/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_Test_FD4 {

extern "C" void ML_Test_FD4_DPolyCalc_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_Test_FD4_DPolyCalc_calc_every != ML_Test_FD4_DPolyCalc_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_dp0","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_dp0.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_dp1","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_dp1.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_dp2","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_dp2.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_dp3","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_dp3.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_dp4","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_dp4.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_dp5","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_dp5.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_dp6","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_dp6.");
  return;
}

static void ML_Test_FD4_DPolyCalc_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_Test_FD4_DPolyCalc,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL p0L CCTK_ATTRIBUTE_UNUSED = p0[index];
    CCTK_REAL p1L CCTK_ATTRIBUTE_UNUSED = p1[index];
    CCTK_REAL p2L CCTK_ATTRIBUTE_UNUSED = p2[index];
    CCTK_REAL p3L CCTK_ATTRIBUTE_UNUSED = p3[index];
    CCTK_REAL p4L CCTK_ATTRIBUTE_UNUSED = p4[index];
    CCTK_REAL p5L CCTK_ATTRIBUTE_UNUSED = p5[index];
    CCTK_REAL p6L CCTK_ATTRIBUTE_UNUSED = p6[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL dp01L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp02L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp03L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp11L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp12L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp13L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp21L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp22L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp23L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp31L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp32L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp33L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp41L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp42L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp43L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp51L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp52L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp53L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp61L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp62L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dp63L CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      dp01L = 0.0833333333333333333333333333333*(J11L*(GFOffset(p0,-2,0,0) - 
        8*GFOffset(p0,-1,0,0) + 8*GFOffset(p0,1,0,0) - 
        GFOffset(p0,2,0,0))*pow(dx,-1) + J21L*(GFOffset(p0,0,-2,0) - 
        8*GFOffset(p0,0,-1,0) + 8*GFOffset(p0,0,1,0) - 
        GFOffset(p0,0,2,0))*pow(dy,-1) + J31L*(GFOffset(p0,0,0,-2) - 
        8*GFOffset(p0,0,0,-1) + 8*GFOffset(p0,0,0,1) - 
        GFOffset(p0,0,0,2))*pow(dz,-1));
      
      dp02L = 0.0833333333333333333333333333333*(J12L*(GFOffset(p0,-2,0,0) - 
        8*GFOffset(p0,-1,0,0) + 8*GFOffset(p0,1,0,0) - 
        GFOffset(p0,2,0,0))*pow(dx,-1) + J22L*(GFOffset(p0,0,-2,0) - 
        8*GFOffset(p0,0,-1,0) + 8*GFOffset(p0,0,1,0) - 
        GFOffset(p0,0,2,0))*pow(dy,-1) + J32L*(GFOffset(p0,0,0,-2) - 
        8*GFOffset(p0,0,0,-1) + 8*GFOffset(p0,0,0,1) - 
        GFOffset(p0,0,0,2))*pow(dz,-1));
      
      dp03L = 0.0833333333333333333333333333333*(J13L*(GFOffset(p0,-2,0,0) - 
        8*GFOffset(p0,-1,0,0) + 8*GFOffset(p0,1,0,0) - 
        GFOffset(p0,2,0,0))*pow(dx,-1) + J23L*(GFOffset(p0,0,-2,0) - 
        8*GFOffset(p0,0,-1,0) + 8*GFOffset(p0,0,1,0) - 
        GFOffset(p0,0,2,0))*pow(dy,-1) + J33L*(GFOffset(p0,0,0,-2) - 
        8*GFOffset(p0,0,0,-1) + 8*GFOffset(p0,0,0,1) - 
        GFOffset(p0,0,0,2))*pow(dz,-1));
      
      dp11L = 0.0833333333333333333333333333333*(J11L*(GFOffset(p1,-2,0,0) - 
        8*GFOffset(p1,-1,0,0) + 8*GFOffset(p1,1,0,0) - 
        GFOffset(p1,2,0,0))*pow(dx,-1) + J21L*(GFOffset(p1,0,-2,0) - 
        8*GFOffset(p1,0,-1,0) + 8*GFOffset(p1,0,1,0) - 
        GFOffset(p1,0,2,0))*pow(dy,-1) + J31L*(GFOffset(p1,0,0,-2) - 
        8*GFOffset(p1,0,0,-1) + 8*GFOffset(p1,0,0,1) - 
        GFOffset(p1,0,0,2))*pow(dz,-1));
      
      dp12L = 0.0833333333333333333333333333333*(J12L*(GFOffset(p1,-2,0,0) - 
        8*GFOffset(p1,-1,0,0) + 8*GFOffset(p1,1,0,0) - 
        GFOffset(p1,2,0,0))*pow(dx,-1) + J22L*(GFOffset(p1,0,-2,0) - 
        8*GFOffset(p1,0,-1,0) + 8*GFOffset(p1,0,1,0) - 
        GFOffset(p1,0,2,0))*pow(dy,-1) + J32L*(GFOffset(p1,0,0,-2) - 
        8*GFOffset(p1,0,0,-1) + 8*GFOffset(p1,0,0,1) - 
        GFOffset(p1,0,0,2))*pow(dz,-1));
      
      dp13L = 0.0833333333333333333333333333333*(J13L*(GFOffset(p1,-2,0,0) - 
        8*GFOffset(p1,-1,0,0) + 8*GFOffset(p1,1,0,0) - 
        GFOffset(p1,2,0,0))*pow(dx,-1) + J23L*(GFOffset(p1,0,-2,0) - 
        8*GFOffset(p1,0,-1,0) + 8*GFOffset(p1,0,1,0) - 
        GFOffset(p1,0,2,0))*pow(dy,-1) + J33L*(GFOffset(p1,0,0,-2) - 
        8*GFOffset(p1,0,0,-1) + 8*GFOffset(p1,0,0,1) - 
        GFOffset(p1,0,0,2))*pow(dz,-1));
      
      dp21L = 0.0833333333333333333333333333333*(J11L*(GFOffset(p2,-2,0,0) - 
        8*GFOffset(p2,-1,0,0) + 8*GFOffset(p2,1,0,0) - 
        GFOffset(p2,2,0,0))*pow(dx,-1) + J21L*(GFOffset(p2,0,-2,0) - 
        8*GFOffset(p2,0,-1,0) + 8*GFOffset(p2,0,1,0) - 
        GFOffset(p2,0,2,0))*pow(dy,-1) + J31L*(GFOffset(p2,0,0,-2) - 
        8*GFOffset(p2,0,0,-1) + 8*GFOffset(p2,0,0,1) - 
        GFOffset(p2,0,0,2))*pow(dz,-1));
      
      dp22L = 0.0833333333333333333333333333333*(J12L*(GFOffset(p2,-2,0,0) - 
        8*GFOffset(p2,-1,0,0) + 8*GFOffset(p2,1,0,0) - 
        GFOffset(p2,2,0,0))*pow(dx,-1) + J22L*(GFOffset(p2,0,-2,0) - 
        8*GFOffset(p2,0,-1,0) + 8*GFOffset(p2,0,1,0) - 
        GFOffset(p2,0,2,0))*pow(dy,-1) + J32L*(GFOffset(p2,0,0,-2) - 
        8*GFOffset(p2,0,0,-1) + 8*GFOffset(p2,0,0,1) - 
        GFOffset(p2,0,0,2))*pow(dz,-1));
      
      dp23L = 0.0833333333333333333333333333333*(J13L*(GFOffset(p2,-2,0,0) - 
        8*GFOffset(p2,-1,0,0) + 8*GFOffset(p2,1,0,0) - 
        GFOffset(p2,2,0,0))*pow(dx,-1) + J23L*(GFOffset(p2,0,-2,0) - 
        8*GFOffset(p2,0,-1,0) + 8*GFOffset(p2,0,1,0) - 
        GFOffset(p2,0,2,0))*pow(dy,-1) + J33L*(GFOffset(p2,0,0,-2) - 
        8*GFOffset(p2,0,0,-1) + 8*GFOffset(p2,0,0,1) - 
        GFOffset(p2,0,0,2))*pow(dz,-1));
      
      dp31L = 0.0833333333333333333333333333333*(J11L*(GFOffset(p3,-2,0,0) - 
        8*GFOffset(p3,-1,0,0) + 8*GFOffset(p3,1,0,0) - 
        GFOffset(p3,2,0,0))*pow(dx,-1) + J21L*(GFOffset(p3,0,-2,0) - 
        8*GFOffset(p3,0,-1,0) + 8*GFOffset(p3,0,1,0) - 
        GFOffset(p3,0,2,0))*pow(dy,-1) + J31L*(GFOffset(p3,0,0,-2) - 
        8*GFOffset(p3,0,0,-1) + 8*GFOffset(p3,0,0,1) - 
        GFOffset(p3,0,0,2))*pow(dz,-1));
      
      dp32L = 0.0833333333333333333333333333333*(J12L*(GFOffset(p3,-2,0,0) - 
        8*GFOffset(p3,-1,0,0) + 8*GFOffset(p3,1,0,0) - 
        GFOffset(p3,2,0,0))*pow(dx,-1) + J22L*(GFOffset(p3,0,-2,0) - 
        8*GFOffset(p3,0,-1,0) + 8*GFOffset(p3,0,1,0) - 
        GFOffset(p3,0,2,0))*pow(dy,-1) + J32L*(GFOffset(p3,0,0,-2) - 
        8*GFOffset(p3,0,0,-1) + 8*GFOffset(p3,0,0,1) - 
        GFOffset(p3,0,0,2))*pow(dz,-1));
      
      dp33L = 0.0833333333333333333333333333333*(J13L*(GFOffset(p3,-2,0,0) - 
        8*GFOffset(p3,-1,0,0) + 8*GFOffset(p3,1,0,0) - 
        GFOffset(p3,2,0,0))*pow(dx,-1) + J23L*(GFOffset(p3,0,-2,0) - 
        8*GFOffset(p3,0,-1,0) + 8*GFOffset(p3,0,1,0) - 
        GFOffset(p3,0,2,0))*pow(dy,-1) + J33L*(GFOffset(p3,0,0,-2) - 
        8*GFOffset(p3,0,0,-1) + 8*GFOffset(p3,0,0,1) - 
        GFOffset(p3,0,0,2))*pow(dz,-1));
      
      dp41L = 0.0833333333333333333333333333333*(J11L*(GFOffset(p4,-2,0,0) - 
        8*GFOffset(p4,-1,0,0) + 8*GFOffset(p4,1,0,0) - 
        GFOffset(p4,2,0,0))*pow(dx,-1) + J21L*(GFOffset(p4,0,-2,0) - 
        8*GFOffset(p4,0,-1,0) + 8*GFOffset(p4,0,1,0) - 
        GFOffset(p4,0,2,0))*pow(dy,-1) + J31L*(GFOffset(p4,0,0,-2) - 
        8*GFOffset(p4,0,0,-1) + 8*GFOffset(p4,0,0,1) - 
        GFOffset(p4,0,0,2))*pow(dz,-1));
      
      dp42L = 0.0833333333333333333333333333333*(J12L*(GFOffset(p4,-2,0,0) - 
        8*GFOffset(p4,-1,0,0) + 8*GFOffset(p4,1,0,0) - 
        GFOffset(p4,2,0,0))*pow(dx,-1) + J22L*(GFOffset(p4,0,-2,0) - 
        8*GFOffset(p4,0,-1,0) + 8*GFOffset(p4,0,1,0) - 
        GFOffset(p4,0,2,0))*pow(dy,-1) + J32L*(GFOffset(p4,0,0,-2) - 
        8*GFOffset(p4,0,0,-1) + 8*GFOffset(p4,0,0,1) - 
        GFOffset(p4,0,0,2))*pow(dz,-1));
      
      dp43L = 0.0833333333333333333333333333333*(J13L*(GFOffset(p4,-2,0,0) - 
        8*GFOffset(p4,-1,0,0) + 8*GFOffset(p4,1,0,0) - 
        GFOffset(p4,2,0,0))*pow(dx,-1) + J23L*(GFOffset(p4,0,-2,0) - 
        8*GFOffset(p4,0,-1,0) + 8*GFOffset(p4,0,1,0) - 
        GFOffset(p4,0,2,0))*pow(dy,-1) + J33L*(GFOffset(p4,0,0,-2) - 
        8*GFOffset(p4,0,0,-1) + 8*GFOffset(p4,0,0,1) - 
        GFOffset(p4,0,0,2))*pow(dz,-1));
      
      dp51L = 0.0833333333333333333333333333333*(J11L*(GFOffset(p5,-2,0,0) - 
        8*GFOffset(p5,-1,0,0) + 8*GFOffset(p5,1,0,0) - 
        GFOffset(p5,2,0,0))*pow(dx,-1) + J21L*(GFOffset(p5,0,-2,0) - 
        8*GFOffset(p5,0,-1,0) + 8*GFOffset(p5,0,1,0) - 
        GFOffset(p5,0,2,0))*pow(dy,-1) + J31L*(GFOffset(p5,0,0,-2) - 
        8*GFOffset(p5,0,0,-1) + 8*GFOffset(p5,0,0,1) - 
        GFOffset(p5,0,0,2))*pow(dz,-1));
      
      dp52L = 0.0833333333333333333333333333333*(J12L*(GFOffset(p5,-2,0,0) - 
        8*GFOffset(p5,-1,0,0) + 8*GFOffset(p5,1,0,0) - 
        GFOffset(p5,2,0,0))*pow(dx,-1) + J22L*(GFOffset(p5,0,-2,0) - 
        8*GFOffset(p5,0,-1,0) + 8*GFOffset(p5,0,1,0) - 
        GFOffset(p5,0,2,0))*pow(dy,-1) + J32L*(GFOffset(p5,0,0,-2) - 
        8*GFOffset(p5,0,0,-1) + 8*GFOffset(p5,0,0,1) - 
        GFOffset(p5,0,0,2))*pow(dz,-1));
      
      dp53L = 0.0833333333333333333333333333333*(J13L*(GFOffset(p5,-2,0,0) - 
        8*GFOffset(p5,-1,0,0) + 8*GFOffset(p5,1,0,0) - 
        GFOffset(p5,2,0,0))*pow(dx,-1) + J23L*(GFOffset(p5,0,-2,0) - 
        8*GFOffset(p5,0,-1,0) + 8*GFOffset(p5,0,1,0) - 
        GFOffset(p5,0,2,0))*pow(dy,-1) + J33L*(GFOffset(p5,0,0,-2) - 
        8*GFOffset(p5,0,0,-1) + 8*GFOffset(p5,0,0,1) - 
        GFOffset(p5,0,0,2))*pow(dz,-1));
      
      dp61L = 0.0833333333333333333333333333333*(J11L*(GFOffset(p6,-2,0,0) - 
        8*GFOffset(p6,-1,0,0) + 8*GFOffset(p6,1,0,0) - 
        GFOffset(p6,2,0,0))*pow(dx,-1) + J21L*(GFOffset(p6,0,-2,0) - 
        8*GFOffset(p6,0,-1,0) + 8*GFOffset(p6,0,1,0) - 
        GFOffset(p6,0,2,0))*pow(dy,-1) + J31L*(GFOffset(p6,0,0,-2) - 
        8*GFOffset(p6,0,0,-1) + 8*GFOffset(p6,0,0,1) - 
        GFOffset(p6,0,0,2))*pow(dz,-1));
      
      dp62L = 0.0833333333333333333333333333333*(J12L*(GFOffset(p6,-2,0,0) - 
        8*GFOffset(p6,-1,0,0) + 8*GFOffset(p6,1,0,0) - 
        GFOffset(p6,2,0,0))*pow(dx,-1) + J22L*(GFOffset(p6,0,-2,0) - 
        8*GFOffset(p6,0,-1,0) + 8*GFOffset(p6,0,1,0) - 
        GFOffset(p6,0,2,0))*pow(dy,-1) + J32L*(GFOffset(p6,0,0,-2) - 
        8*GFOffset(p6,0,0,-1) + 8*GFOffset(p6,0,0,1) - 
        GFOffset(p6,0,0,2))*pow(dz,-1));
      
      dp63L = 0.0833333333333333333333333333333*(J13L*(GFOffset(p6,-2,0,0) - 
        8*GFOffset(p6,-1,0,0) + 8*GFOffset(p6,1,0,0) - 
        GFOffset(p6,2,0,0))*pow(dx,-1) + J23L*(GFOffset(p6,0,-2,0) - 
        8*GFOffset(p6,0,-1,0) + 8*GFOffset(p6,0,1,0) - 
        GFOffset(p6,0,2,0))*pow(dy,-1) + J33L*(GFOffset(p6,0,0,-2) - 
        8*GFOffset(p6,0,0,-1) + 8*GFOffset(p6,0,0,1) - 
        GFOffset(p6,0,0,2))*pow(dz,-1));
    }
    else
    {
      dp01L = 0.0833333333333333333333333333333*(GFOffset(p0,-2,0,0) - 
        8*GFOffset(p0,-1,0,0) + 8*GFOffset(p0,1,0,0) - 
        GFOffset(p0,2,0,0))*pow(dx,-1);
      
      dp02L = 0.0833333333333333333333333333333*(GFOffset(p0,0,-2,0) - 
        8*GFOffset(p0,0,-1,0) + 8*GFOffset(p0,0,1,0) - 
        GFOffset(p0,0,2,0))*pow(dy,-1);
      
      dp03L = 0.0833333333333333333333333333333*(GFOffset(p0,0,0,-2) - 
        8*GFOffset(p0,0,0,-1) + 8*GFOffset(p0,0,0,1) - 
        GFOffset(p0,0,0,2))*pow(dz,-1);
      
      dp11L = 0.0833333333333333333333333333333*(GFOffset(p1,-2,0,0) - 
        8*GFOffset(p1,-1,0,0) + 8*GFOffset(p1,1,0,0) - 
        GFOffset(p1,2,0,0))*pow(dx,-1);
      
      dp12L = 0.0833333333333333333333333333333*(GFOffset(p1,0,-2,0) - 
        8*GFOffset(p1,0,-1,0) + 8*GFOffset(p1,0,1,0) - 
        GFOffset(p1,0,2,0))*pow(dy,-1);
      
      dp13L = 0.0833333333333333333333333333333*(GFOffset(p1,0,0,-2) - 
        8*GFOffset(p1,0,0,-1) + 8*GFOffset(p1,0,0,1) - 
        GFOffset(p1,0,0,2))*pow(dz,-1);
      
      dp21L = 0.0833333333333333333333333333333*(GFOffset(p2,-2,0,0) - 
        8*GFOffset(p2,-1,0,0) + 8*GFOffset(p2,1,0,0) - 
        GFOffset(p2,2,0,0))*pow(dx,-1);
      
      dp22L = 0.0833333333333333333333333333333*(GFOffset(p2,0,-2,0) - 
        8*GFOffset(p2,0,-1,0) + 8*GFOffset(p2,0,1,0) - 
        GFOffset(p2,0,2,0))*pow(dy,-1);
      
      dp23L = 0.0833333333333333333333333333333*(GFOffset(p2,0,0,-2) - 
        8*GFOffset(p2,0,0,-1) + 8*GFOffset(p2,0,0,1) - 
        GFOffset(p2,0,0,2))*pow(dz,-1);
      
      dp31L = 0.0833333333333333333333333333333*(GFOffset(p3,-2,0,0) - 
        8*GFOffset(p3,-1,0,0) + 8*GFOffset(p3,1,0,0) - 
        GFOffset(p3,2,0,0))*pow(dx,-1);
      
      dp32L = 0.0833333333333333333333333333333*(GFOffset(p3,0,-2,0) - 
        8*GFOffset(p3,0,-1,0) + 8*GFOffset(p3,0,1,0) - 
        GFOffset(p3,0,2,0))*pow(dy,-1);
      
      dp33L = 0.0833333333333333333333333333333*(GFOffset(p3,0,0,-2) - 
        8*GFOffset(p3,0,0,-1) + 8*GFOffset(p3,0,0,1) - 
        GFOffset(p3,0,0,2))*pow(dz,-1);
      
      dp41L = 0.0833333333333333333333333333333*(GFOffset(p4,-2,0,0) - 
        8*GFOffset(p4,-1,0,0) + 8*GFOffset(p4,1,0,0) - 
        GFOffset(p4,2,0,0))*pow(dx,-1);
      
      dp42L = 0.0833333333333333333333333333333*(GFOffset(p4,0,-2,0) - 
        8*GFOffset(p4,0,-1,0) + 8*GFOffset(p4,0,1,0) - 
        GFOffset(p4,0,2,0))*pow(dy,-1);
      
      dp43L = 0.0833333333333333333333333333333*(GFOffset(p4,0,0,-2) - 
        8*GFOffset(p4,0,0,-1) + 8*GFOffset(p4,0,0,1) - 
        GFOffset(p4,0,0,2))*pow(dz,-1);
      
      dp51L = 0.0833333333333333333333333333333*(GFOffset(p5,-2,0,0) - 
        8*GFOffset(p5,-1,0,0) + 8*GFOffset(p5,1,0,0) - 
        GFOffset(p5,2,0,0))*pow(dx,-1);
      
      dp52L = 0.0833333333333333333333333333333*(GFOffset(p5,0,-2,0) - 
        8*GFOffset(p5,0,-1,0) + 8*GFOffset(p5,0,1,0) - 
        GFOffset(p5,0,2,0))*pow(dy,-1);
      
      dp53L = 0.0833333333333333333333333333333*(GFOffset(p5,0,0,-2) - 
        8*GFOffset(p5,0,0,-1) + 8*GFOffset(p5,0,0,1) - 
        GFOffset(p5,0,0,2))*pow(dz,-1);
      
      dp61L = 0.0833333333333333333333333333333*(GFOffset(p6,-2,0,0) - 
        8*GFOffset(p6,-1,0,0) + 8*GFOffset(p6,1,0,0) - 
        GFOffset(p6,2,0,0))*pow(dx,-1);
      
      dp62L = 0.0833333333333333333333333333333*(GFOffset(p6,0,-2,0) - 
        8*GFOffset(p6,0,-1,0) + 8*GFOffset(p6,0,1,0) - 
        GFOffset(p6,0,2,0))*pow(dy,-1);
      
      dp63L = 0.0833333333333333333333333333333*(GFOffset(p6,0,0,-2) - 
        8*GFOffset(p6,0,0,-1) + 8*GFOffset(p6,0,0,1) - 
        GFOffset(p6,0,0,2))*pow(dz,-1);
    }
    /* Copy local copies back to grid functions */
    dp01[index] = dp01L;
    dp02[index] = dp02L;
    dp03[index] = dp03L;
    dp11[index] = dp11L;
    dp12[index] = dp12L;
    dp13[index] = dp13L;
    dp21[index] = dp21L;
    dp22[index] = dp22L;
    dp23[index] = dp23L;
    dp31[index] = dp31L;
    dp32[index] = dp32L;
    dp33[index] = dp33L;
    dp41[index] = dp41L;
    dp42[index] = dp42L;
    dp43[index] = dp43L;
    dp51[index] = dp51L;
    dp52[index] = dp52L;
    dp53[index] = dp53L;
    dp61[index] = dp61L;
    dp62[index] = dp62L;
    dp63[index] = dp63L;
  }
  CCTK_ENDLOOP3(ML_Test_FD4_DPolyCalc);
}
extern "C" void ML_Test_FD4_DPolyCalc(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_Test_FD4_DPolyCalc_Body");
  }
  if (cctk_iteration % ML_Test_FD4_DPolyCalc_calc_every != ML_Test_FD4_DPolyCalc_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_Test_FD4::WT_dp0",
    "ML_Test_FD4::WT_dp1",
    "ML_Test_FD4::WT_dp2",
    "ML_Test_FD4::WT_dp3",
    "ML_Test_FD4::WT_dp4",
    "ML_Test_FD4::WT_dp5",
    "ML_Test_FD4::WT_dp6",
    "ML_Test_FD4::WT_p0",
    "ML_Test_FD4::WT_p1",
    "ML_Test_FD4::WT_p2",
    "ML_Test_FD4::WT_p3",
    "ML_Test_FD4::WT_p4",
    "ML_Test_FD4::WT_p5",
    "ML_Test_FD4::WT_p6"};
  AssertGroupStorage(cctkGH, "ML_Test_FD4_DPolyCalc", 14, groups);
  
  
  LoopOverInterior(cctkGH, ML_Test_FD4_DPolyCalc_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_Test_FD4_DPolyCalc_Body");
  }
}

} // namespace ML_Test_FD4
