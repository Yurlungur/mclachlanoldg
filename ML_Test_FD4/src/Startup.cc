/*  File produced by Kranc */

#include "cctk.h"

extern "C" int ML_Test_FD4_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "ML_Test_FD4";
  CCTK_RegisterBanner(banner);
  return 0;
}
