/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_Test_FD4 {

extern "C" void ML_Test_FD4_DDPolyCalc_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_Test_FD4_DDPolyCalc_calc_every != ML_Test_FD4_DDPolyCalc_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_ddp0","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_ddp0.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_ddp1","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_ddp1.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_ddp2","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_ddp2.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_ddp3","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_ddp3.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_ddp4","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_ddp4.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_ddp5","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_ddp5.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_FD4::WT_ddp6","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_FD4::WT_ddp6.");
  return;
}

static void ML_Test_FD4_DDPolyCalc_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_Test_FD4_DDPolyCalc,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL dp01L CCTK_ATTRIBUTE_UNUSED = dp01[index];
    CCTK_REAL dp02L CCTK_ATTRIBUTE_UNUSED = dp02[index];
    CCTK_REAL dp03L CCTK_ATTRIBUTE_UNUSED = dp03[index];
    CCTK_REAL dp11L CCTK_ATTRIBUTE_UNUSED = dp11[index];
    CCTK_REAL dp12L CCTK_ATTRIBUTE_UNUSED = dp12[index];
    CCTK_REAL dp13L CCTK_ATTRIBUTE_UNUSED = dp13[index];
    CCTK_REAL dp21L CCTK_ATTRIBUTE_UNUSED = dp21[index];
    CCTK_REAL dp22L CCTK_ATTRIBUTE_UNUSED = dp22[index];
    CCTK_REAL dp23L CCTK_ATTRIBUTE_UNUSED = dp23[index];
    CCTK_REAL dp31L CCTK_ATTRIBUTE_UNUSED = dp31[index];
    CCTK_REAL dp32L CCTK_ATTRIBUTE_UNUSED = dp32[index];
    CCTK_REAL dp33L CCTK_ATTRIBUTE_UNUSED = dp33[index];
    CCTK_REAL dp41L CCTK_ATTRIBUTE_UNUSED = dp41[index];
    CCTK_REAL dp42L CCTK_ATTRIBUTE_UNUSED = dp42[index];
    CCTK_REAL dp43L CCTK_ATTRIBUTE_UNUSED = dp43[index];
    CCTK_REAL dp51L CCTK_ATTRIBUTE_UNUSED = dp51[index];
    CCTK_REAL dp52L CCTK_ATTRIBUTE_UNUSED = dp52[index];
    CCTK_REAL dp53L CCTK_ATTRIBUTE_UNUSED = dp53[index];
    CCTK_REAL dp61L CCTK_ATTRIBUTE_UNUSED = dp61[index];
    CCTK_REAL dp62L CCTK_ATTRIBUTE_UNUSED = dp62[index];
    CCTK_REAL dp63L CCTK_ATTRIBUTE_UNUSED = dp63[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL ddp011L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp012L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp013L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp022L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp023L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp033L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp111L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp112L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp113L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp122L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp123L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp133L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp211L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp212L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp213L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp222L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp223L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp233L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp311L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp312L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp313L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp322L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp323L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp333L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp411L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp412L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp413L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp422L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp423L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp433L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp511L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp512L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp513L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp522L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp523L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp533L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp611L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp612L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp613L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp622L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp623L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL ddp633L CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      ddp011L = 
        0.0833333333333333333333333333333*(J11L*(GFOffset(dp01,-2,0,0) - 
        8*GFOffset(dp01,-1,0,0) + 8*GFOffset(dp01,1,0,0) - 
        GFOffset(dp01,2,0,0))*pow(dx,-1) + J21L*(GFOffset(dp01,0,-2,0) - 
        8*GFOffset(dp01,0,-1,0) + 8*GFOffset(dp01,0,1,0) - 
        GFOffset(dp01,0,2,0))*pow(dy,-1) + J31L*(GFOffset(dp01,0,0,-2) - 
        8*GFOffset(dp01,0,0,-1) + 8*GFOffset(dp01,0,0,1) - 
        GFOffset(dp01,0,0,2))*pow(dz,-1));
      
      ddp012L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp01,-2,0,0) - 
        8*GFOffset(dp01,-1,0,0) + 8*GFOffset(dp01,1,0,0) - 
        GFOffset(dp01,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp01,0,-2,0) - 
        8*GFOffset(dp01,0,-1,0) + 8*GFOffset(dp01,0,1,0) - 
        GFOffset(dp01,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp01,0,0,-2) - 
        8*GFOffset(dp01,0,0,-1) + 8*GFOffset(dp01,0,0,1) - 
        GFOffset(dp01,0,0,2))*pow(dz,-1));
      
      ddp013L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp01,-2,0,0) - 
        8*GFOffset(dp01,-1,0,0) + 8*GFOffset(dp01,1,0,0) - 
        GFOffset(dp01,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp01,0,-2,0) - 
        8*GFOffset(dp01,0,-1,0) + 8*GFOffset(dp01,0,1,0) - 
        GFOffset(dp01,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp01,0,0,-2) - 
        8*GFOffset(dp01,0,0,-1) + 8*GFOffset(dp01,0,0,1) - 
        GFOffset(dp01,0,0,2))*pow(dz,-1));
      
      ddp022L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp02,-2,0,0) - 
        8*GFOffset(dp02,-1,0,0) + 8*GFOffset(dp02,1,0,0) - 
        GFOffset(dp02,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp02,0,-2,0) - 
        8*GFOffset(dp02,0,-1,0) + 8*GFOffset(dp02,0,1,0) - 
        GFOffset(dp02,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp02,0,0,-2) - 
        8*GFOffset(dp02,0,0,-1) + 8*GFOffset(dp02,0,0,1) - 
        GFOffset(dp02,0,0,2))*pow(dz,-1));
      
      ddp023L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp02,-2,0,0) - 
        8*GFOffset(dp02,-1,0,0) + 8*GFOffset(dp02,1,0,0) - 
        GFOffset(dp02,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp02,0,-2,0) - 
        8*GFOffset(dp02,0,-1,0) + 8*GFOffset(dp02,0,1,0) - 
        GFOffset(dp02,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp02,0,0,-2) - 
        8*GFOffset(dp02,0,0,-1) + 8*GFOffset(dp02,0,0,1) - 
        GFOffset(dp02,0,0,2))*pow(dz,-1));
      
      ddp033L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp03,-2,0,0) - 
        8*GFOffset(dp03,-1,0,0) + 8*GFOffset(dp03,1,0,0) - 
        GFOffset(dp03,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp03,0,-2,0) - 
        8*GFOffset(dp03,0,-1,0) + 8*GFOffset(dp03,0,1,0) - 
        GFOffset(dp03,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp03,0,0,-2) - 
        8*GFOffset(dp03,0,0,-1) + 8*GFOffset(dp03,0,0,1) - 
        GFOffset(dp03,0,0,2))*pow(dz,-1));
      
      ddp111L = 
        0.0833333333333333333333333333333*(J11L*(GFOffset(dp11,-2,0,0) - 
        8*GFOffset(dp11,-1,0,0) + 8*GFOffset(dp11,1,0,0) - 
        GFOffset(dp11,2,0,0))*pow(dx,-1) + J21L*(GFOffset(dp11,0,-2,0) - 
        8*GFOffset(dp11,0,-1,0) + 8*GFOffset(dp11,0,1,0) - 
        GFOffset(dp11,0,2,0))*pow(dy,-1) + J31L*(GFOffset(dp11,0,0,-2) - 
        8*GFOffset(dp11,0,0,-1) + 8*GFOffset(dp11,0,0,1) - 
        GFOffset(dp11,0,0,2))*pow(dz,-1));
      
      ddp112L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp11,-2,0,0) - 
        8*GFOffset(dp11,-1,0,0) + 8*GFOffset(dp11,1,0,0) - 
        GFOffset(dp11,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp11,0,-2,0) - 
        8*GFOffset(dp11,0,-1,0) + 8*GFOffset(dp11,0,1,0) - 
        GFOffset(dp11,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp11,0,0,-2) - 
        8*GFOffset(dp11,0,0,-1) + 8*GFOffset(dp11,0,0,1) - 
        GFOffset(dp11,0,0,2))*pow(dz,-1));
      
      ddp113L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp11,-2,0,0) - 
        8*GFOffset(dp11,-1,0,0) + 8*GFOffset(dp11,1,0,0) - 
        GFOffset(dp11,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp11,0,-2,0) - 
        8*GFOffset(dp11,0,-1,0) + 8*GFOffset(dp11,0,1,0) - 
        GFOffset(dp11,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp11,0,0,-2) - 
        8*GFOffset(dp11,0,0,-1) + 8*GFOffset(dp11,0,0,1) - 
        GFOffset(dp11,0,0,2))*pow(dz,-1));
      
      ddp122L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp12,-2,0,0) - 
        8*GFOffset(dp12,-1,0,0) + 8*GFOffset(dp12,1,0,0) - 
        GFOffset(dp12,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp12,0,-2,0) - 
        8*GFOffset(dp12,0,-1,0) + 8*GFOffset(dp12,0,1,0) - 
        GFOffset(dp12,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp12,0,0,-2) - 
        8*GFOffset(dp12,0,0,-1) + 8*GFOffset(dp12,0,0,1) - 
        GFOffset(dp12,0,0,2))*pow(dz,-1));
      
      ddp123L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp12,-2,0,0) - 
        8*GFOffset(dp12,-1,0,0) + 8*GFOffset(dp12,1,0,0) - 
        GFOffset(dp12,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp12,0,-2,0) - 
        8*GFOffset(dp12,0,-1,0) + 8*GFOffset(dp12,0,1,0) - 
        GFOffset(dp12,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp12,0,0,-2) - 
        8*GFOffset(dp12,0,0,-1) + 8*GFOffset(dp12,0,0,1) - 
        GFOffset(dp12,0,0,2))*pow(dz,-1));
      
      ddp133L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp13,-2,0,0) - 
        8*GFOffset(dp13,-1,0,0) + 8*GFOffset(dp13,1,0,0) - 
        GFOffset(dp13,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp13,0,-2,0) - 
        8*GFOffset(dp13,0,-1,0) + 8*GFOffset(dp13,0,1,0) - 
        GFOffset(dp13,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp13,0,0,-2) - 
        8*GFOffset(dp13,0,0,-1) + 8*GFOffset(dp13,0,0,1) - 
        GFOffset(dp13,0,0,2))*pow(dz,-1));
      
      ddp211L = 
        0.0833333333333333333333333333333*(J11L*(GFOffset(dp21,-2,0,0) - 
        8*GFOffset(dp21,-1,0,0) + 8*GFOffset(dp21,1,0,0) - 
        GFOffset(dp21,2,0,0))*pow(dx,-1) + J21L*(GFOffset(dp21,0,-2,0) - 
        8*GFOffset(dp21,0,-1,0) + 8*GFOffset(dp21,0,1,0) - 
        GFOffset(dp21,0,2,0))*pow(dy,-1) + J31L*(GFOffset(dp21,0,0,-2) - 
        8*GFOffset(dp21,0,0,-1) + 8*GFOffset(dp21,0,0,1) - 
        GFOffset(dp21,0,0,2))*pow(dz,-1));
      
      ddp212L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp21,-2,0,0) - 
        8*GFOffset(dp21,-1,0,0) + 8*GFOffset(dp21,1,0,0) - 
        GFOffset(dp21,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp21,0,-2,0) - 
        8*GFOffset(dp21,0,-1,0) + 8*GFOffset(dp21,0,1,0) - 
        GFOffset(dp21,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp21,0,0,-2) - 
        8*GFOffset(dp21,0,0,-1) + 8*GFOffset(dp21,0,0,1) - 
        GFOffset(dp21,0,0,2))*pow(dz,-1));
      
      ddp213L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp21,-2,0,0) - 
        8*GFOffset(dp21,-1,0,0) + 8*GFOffset(dp21,1,0,0) - 
        GFOffset(dp21,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp21,0,-2,0) - 
        8*GFOffset(dp21,0,-1,0) + 8*GFOffset(dp21,0,1,0) - 
        GFOffset(dp21,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp21,0,0,-2) - 
        8*GFOffset(dp21,0,0,-1) + 8*GFOffset(dp21,0,0,1) - 
        GFOffset(dp21,0,0,2))*pow(dz,-1));
      
      ddp222L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp22,-2,0,0) - 
        8*GFOffset(dp22,-1,0,0) + 8*GFOffset(dp22,1,0,0) - 
        GFOffset(dp22,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp22,0,-2,0) - 
        8*GFOffset(dp22,0,-1,0) + 8*GFOffset(dp22,0,1,0) - 
        GFOffset(dp22,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp22,0,0,-2) - 
        8*GFOffset(dp22,0,0,-1) + 8*GFOffset(dp22,0,0,1) - 
        GFOffset(dp22,0,0,2))*pow(dz,-1));
      
      ddp223L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp22,-2,0,0) - 
        8*GFOffset(dp22,-1,0,0) + 8*GFOffset(dp22,1,0,0) - 
        GFOffset(dp22,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp22,0,-2,0) - 
        8*GFOffset(dp22,0,-1,0) + 8*GFOffset(dp22,0,1,0) - 
        GFOffset(dp22,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp22,0,0,-2) - 
        8*GFOffset(dp22,0,0,-1) + 8*GFOffset(dp22,0,0,1) - 
        GFOffset(dp22,0,0,2))*pow(dz,-1));
      
      ddp233L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp23,-2,0,0) - 
        8*GFOffset(dp23,-1,0,0) + 8*GFOffset(dp23,1,0,0) - 
        GFOffset(dp23,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp23,0,-2,0) - 
        8*GFOffset(dp23,0,-1,0) + 8*GFOffset(dp23,0,1,0) - 
        GFOffset(dp23,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp23,0,0,-2) - 
        8*GFOffset(dp23,0,0,-1) + 8*GFOffset(dp23,0,0,1) - 
        GFOffset(dp23,0,0,2))*pow(dz,-1));
      
      ddp311L = 
        0.0833333333333333333333333333333*(J11L*(GFOffset(dp31,-2,0,0) - 
        8*GFOffset(dp31,-1,0,0) + 8*GFOffset(dp31,1,0,0) - 
        GFOffset(dp31,2,0,0))*pow(dx,-1) + J21L*(GFOffset(dp31,0,-2,0) - 
        8*GFOffset(dp31,0,-1,0) + 8*GFOffset(dp31,0,1,0) - 
        GFOffset(dp31,0,2,0))*pow(dy,-1) + J31L*(GFOffset(dp31,0,0,-2) - 
        8*GFOffset(dp31,0,0,-1) + 8*GFOffset(dp31,0,0,1) - 
        GFOffset(dp31,0,0,2))*pow(dz,-1));
      
      ddp312L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp31,-2,0,0) - 
        8*GFOffset(dp31,-1,0,0) + 8*GFOffset(dp31,1,0,0) - 
        GFOffset(dp31,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp31,0,-2,0) - 
        8*GFOffset(dp31,0,-1,0) + 8*GFOffset(dp31,0,1,0) - 
        GFOffset(dp31,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp31,0,0,-2) - 
        8*GFOffset(dp31,0,0,-1) + 8*GFOffset(dp31,0,0,1) - 
        GFOffset(dp31,0,0,2))*pow(dz,-1));
      
      ddp313L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp31,-2,0,0) - 
        8*GFOffset(dp31,-1,0,0) + 8*GFOffset(dp31,1,0,0) - 
        GFOffset(dp31,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp31,0,-2,0) - 
        8*GFOffset(dp31,0,-1,0) + 8*GFOffset(dp31,0,1,0) - 
        GFOffset(dp31,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp31,0,0,-2) - 
        8*GFOffset(dp31,0,0,-1) + 8*GFOffset(dp31,0,0,1) - 
        GFOffset(dp31,0,0,2))*pow(dz,-1));
      
      ddp322L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp32,-2,0,0) - 
        8*GFOffset(dp32,-1,0,0) + 8*GFOffset(dp32,1,0,0) - 
        GFOffset(dp32,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp32,0,-2,0) - 
        8*GFOffset(dp32,0,-1,0) + 8*GFOffset(dp32,0,1,0) - 
        GFOffset(dp32,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp32,0,0,-2) - 
        8*GFOffset(dp32,0,0,-1) + 8*GFOffset(dp32,0,0,1) - 
        GFOffset(dp32,0,0,2))*pow(dz,-1));
      
      ddp323L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp32,-2,0,0) - 
        8*GFOffset(dp32,-1,0,0) + 8*GFOffset(dp32,1,0,0) - 
        GFOffset(dp32,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp32,0,-2,0) - 
        8*GFOffset(dp32,0,-1,0) + 8*GFOffset(dp32,0,1,0) - 
        GFOffset(dp32,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp32,0,0,-2) - 
        8*GFOffset(dp32,0,0,-1) + 8*GFOffset(dp32,0,0,1) - 
        GFOffset(dp32,0,0,2))*pow(dz,-1));
      
      ddp333L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp33,-2,0,0) - 
        8*GFOffset(dp33,-1,0,0) + 8*GFOffset(dp33,1,0,0) - 
        GFOffset(dp33,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp33,0,-2,0) - 
        8*GFOffset(dp33,0,-1,0) + 8*GFOffset(dp33,0,1,0) - 
        GFOffset(dp33,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp33,0,0,-2) - 
        8*GFOffset(dp33,0,0,-1) + 8*GFOffset(dp33,0,0,1) - 
        GFOffset(dp33,0,0,2))*pow(dz,-1));
      
      ddp411L = 
        0.0833333333333333333333333333333*(J11L*(GFOffset(dp41,-2,0,0) - 
        8*GFOffset(dp41,-1,0,0) + 8*GFOffset(dp41,1,0,0) - 
        GFOffset(dp41,2,0,0))*pow(dx,-1) + J21L*(GFOffset(dp41,0,-2,0) - 
        8*GFOffset(dp41,0,-1,0) + 8*GFOffset(dp41,0,1,0) - 
        GFOffset(dp41,0,2,0))*pow(dy,-1) + J31L*(GFOffset(dp41,0,0,-2) - 
        8*GFOffset(dp41,0,0,-1) + 8*GFOffset(dp41,0,0,1) - 
        GFOffset(dp41,0,0,2))*pow(dz,-1));
      
      ddp412L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp41,-2,0,0) - 
        8*GFOffset(dp41,-1,0,0) + 8*GFOffset(dp41,1,0,0) - 
        GFOffset(dp41,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp41,0,-2,0) - 
        8*GFOffset(dp41,0,-1,0) + 8*GFOffset(dp41,0,1,0) - 
        GFOffset(dp41,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp41,0,0,-2) - 
        8*GFOffset(dp41,0,0,-1) + 8*GFOffset(dp41,0,0,1) - 
        GFOffset(dp41,0,0,2))*pow(dz,-1));
      
      ddp413L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp41,-2,0,0) - 
        8*GFOffset(dp41,-1,0,0) + 8*GFOffset(dp41,1,0,0) - 
        GFOffset(dp41,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp41,0,-2,0) - 
        8*GFOffset(dp41,0,-1,0) + 8*GFOffset(dp41,0,1,0) - 
        GFOffset(dp41,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp41,0,0,-2) - 
        8*GFOffset(dp41,0,0,-1) + 8*GFOffset(dp41,0,0,1) - 
        GFOffset(dp41,0,0,2))*pow(dz,-1));
      
      ddp422L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp42,-2,0,0) - 
        8*GFOffset(dp42,-1,0,0) + 8*GFOffset(dp42,1,0,0) - 
        GFOffset(dp42,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp42,0,-2,0) - 
        8*GFOffset(dp42,0,-1,0) + 8*GFOffset(dp42,0,1,0) - 
        GFOffset(dp42,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp42,0,0,-2) - 
        8*GFOffset(dp42,0,0,-1) + 8*GFOffset(dp42,0,0,1) - 
        GFOffset(dp42,0,0,2))*pow(dz,-1));
      
      ddp423L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp42,-2,0,0) - 
        8*GFOffset(dp42,-1,0,0) + 8*GFOffset(dp42,1,0,0) - 
        GFOffset(dp42,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp42,0,-2,0) - 
        8*GFOffset(dp42,0,-1,0) + 8*GFOffset(dp42,0,1,0) - 
        GFOffset(dp42,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp42,0,0,-2) - 
        8*GFOffset(dp42,0,0,-1) + 8*GFOffset(dp42,0,0,1) - 
        GFOffset(dp42,0,0,2))*pow(dz,-1));
      
      ddp433L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp43,-2,0,0) - 
        8*GFOffset(dp43,-1,0,0) + 8*GFOffset(dp43,1,0,0) - 
        GFOffset(dp43,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp43,0,-2,0) - 
        8*GFOffset(dp43,0,-1,0) + 8*GFOffset(dp43,0,1,0) - 
        GFOffset(dp43,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp43,0,0,-2) - 
        8*GFOffset(dp43,0,0,-1) + 8*GFOffset(dp43,0,0,1) - 
        GFOffset(dp43,0,0,2))*pow(dz,-1));
      
      ddp511L = 
        0.0833333333333333333333333333333*(J11L*(GFOffset(dp51,-2,0,0) - 
        8*GFOffset(dp51,-1,0,0) + 8*GFOffset(dp51,1,0,0) - 
        GFOffset(dp51,2,0,0))*pow(dx,-1) + J21L*(GFOffset(dp51,0,-2,0) - 
        8*GFOffset(dp51,0,-1,0) + 8*GFOffset(dp51,0,1,0) - 
        GFOffset(dp51,0,2,0))*pow(dy,-1) + J31L*(GFOffset(dp51,0,0,-2) - 
        8*GFOffset(dp51,0,0,-1) + 8*GFOffset(dp51,0,0,1) - 
        GFOffset(dp51,0,0,2))*pow(dz,-1));
      
      ddp512L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp51,-2,0,0) - 
        8*GFOffset(dp51,-1,0,0) + 8*GFOffset(dp51,1,0,0) - 
        GFOffset(dp51,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp51,0,-2,0) - 
        8*GFOffset(dp51,0,-1,0) + 8*GFOffset(dp51,0,1,0) - 
        GFOffset(dp51,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp51,0,0,-2) - 
        8*GFOffset(dp51,0,0,-1) + 8*GFOffset(dp51,0,0,1) - 
        GFOffset(dp51,0,0,2))*pow(dz,-1));
      
      ddp513L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp51,-2,0,0) - 
        8*GFOffset(dp51,-1,0,0) + 8*GFOffset(dp51,1,0,0) - 
        GFOffset(dp51,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp51,0,-2,0) - 
        8*GFOffset(dp51,0,-1,0) + 8*GFOffset(dp51,0,1,0) - 
        GFOffset(dp51,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp51,0,0,-2) - 
        8*GFOffset(dp51,0,0,-1) + 8*GFOffset(dp51,0,0,1) - 
        GFOffset(dp51,0,0,2))*pow(dz,-1));
      
      ddp522L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp52,-2,0,0) - 
        8*GFOffset(dp52,-1,0,0) + 8*GFOffset(dp52,1,0,0) - 
        GFOffset(dp52,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp52,0,-2,0) - 
        8*GFOffset(dp52,0,-1,0) + 8*GFOffset(dp52,0,1,0) - 
        GFOffset(dp52,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp52,0,0,-2) - 
        8*GFOffset(dp52,0,0,-1) + 8*GFOffset(dp52,0,0,1) - 
        GFOffset(dp52,0,0,2))*pow(dz,-1));
      
      ddp523L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp52,-2,0,0) - 
        8*GFOffset(dp52,-1,0,0) + 8*GFOffset(dp52,1,0,0) - 
        GFOffset(dp52,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp52,0,-2,0) - 
        8*GFOffset(dp52,0,-1,0) + 8*GFOffset(dp52,0,1,0) - 
        GFOffset(dp52,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp52,0,0,-2) - 
        8*GFOffset(dp52,0,0,-1) + 8*GFOffset(dp52,0,0,1) - 
        GFOffset(dp52,0,0,2))*pow(dz,-1));
      
      ddp533L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp53,-2,0,0) - 
        8*GFOffset(dp53,-1,0,0) + 8*GFOffset(dp53,1,0,0) - 
        GFOffset(dp53,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp53,0,-2,0) - 
        8*GFOffset(dp53,0,-1,0) + 8*GFOffset(dp53,0,1,0) - 
        GFOffset(dp53,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp53,0,0,-2) - 
        8*GFOffset(dp53,0,0,-1) + 8*GFOffset(dp53,0,0,1) - 
        GFOffset(dp53,0,0,2))*pow(dz,-1));
      
      ddp611L = 
        0.0833333333333333333333333333333*(J11L*(GFOffset(dp61,-2,0,0) - 
        8*GFOffset(dp61,-1,0,0) + 8*GFOffset(dp61,1,0,0) - 
        GFOffset(dp61,2,0,0))*pow(dx,-1) + J21L*(GFOffset(dp61,0,-2,0) - 
        8*GFOffset(dp61,0,-1,0) + 8*GFOffset(dp61,0,1,0) - 
        GFOffset(dp61,0,2,0))*pow(dy,-1) + J31L*(GFOffset(dp61,0,0,-2) - 
        8*GFOffset(dp61,0,0,-1) + 8*GFOffset(dp61,0,0,1) - 
        GFOffset(dp61,0,0,2))*pow(dz,-1));
      
      ddp612L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp61,-2,0,0) - 
        8*GFOffset(dp61,-1,0,0) + 8*GFOffset(dp61,1,0,0) - 
        GFOffset(dp61,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp61,0,-2,0) - 
        8*GFOffset(dp61,0,-1,0) + 8*GFOffset(dp61,0,1,0) - 
        GFOffset(dp61,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp61,0,0,-2) - 
        8*GFOffset(dp61,0,0,-1) + 8*GFOffset(dp61,0,0,1) - 
        GFOffset(dp61,0,0,2))*pow(dz,-1));
      
      ddp613L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp61,-2,0,0) - 
        8*GFOffset(dp61,-1,0,0) + 8*GFOffset(dp61,1,0,0) - 
        GFOffset(dp61,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp61,0,-2,0) - 
        8*GFOffset(dp61,0,-1,0) + 8*GFOffset(dp61,0,1,0) - 
        GFOffset(dp61,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp61,0,0,-2) - 
        8*GFOffset(dp61,0,0,-1) + 8*GFOffset(dp61,0,0,1) - 
        GFOffset(dp61,0,0,2))*pow(dz,-1));
      
      ddp622L = 
        0.0833333333333333333333333333333*(J12L*(GFOffset(dp62,-2,0,0) - 
        8*GFOffset(dp62,-1,0,0) + 8*GFOffset(dp62,1,0,0) - 
        GFOffset(dp62,2,0,0))*pow(dx,-1) + J22L*(GFOffset(dp62,0,-2,0) - 
        8*GFOffset(dp62,0,-1,0) + 8*GFOffset(dp62,0,1,0) - 
        GFOffset(dp62,0,2,0))*pow(dy,-1) + J32L*(GFOffset(dp62,0,0,-2) - 
        8*GFOffset(dp62,0,0,-1) + 8*GFOffset(dp62,0,0,1) - 
        GFOffset(dp62,0,0,2))*pow(dz,-1));
      
      ddp623L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp62,-2,0,0) - 
        8*GFOffset(dp62,-1,0,0) + 8*GFOffset(dp62,1,0,0) - 
        GFOffset(dp62,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp62,0,-2,0) - 
        8*GFOffset(dp62,0,-1,0) + 8*GFOffset(dp62,0,1,0) - 
        GFOffset(dp62,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp62,0,0,-2) - 
        8*GFOffset(dp62,0,0,-1) + 8*GFOffset(dp62,0,0,1) - 
        GFOffset(dp62,0,0,2))*pow(dz,-1));
      
      ddp633L = 
        0.0833333333333333333333333333333*(J13L*(GFOffset(dp63,-2,0,0) - 
        8*GFOffset(dp63,-1,0,0) + 8*GFOffset(dp63,1,0,0) - 
        GFOffset(dp63,2,0,0))*pow(dx,-1) + J23L*(GFOffset(dp63,0,-2,0) - 
        8*GFOffset(dp63,0,-1,0) + 8*GFOffset(dp63,0,1,0) - 
        GFOffset(dp63,0,2,0))*pow(dy,-1) + J33L*(GFOffset(dp63,0,0,-2) - 
        8*GFOffset(dp63,0,0,-1) + 8*GFOffset(dp63,0,0,1) - 
        GFOffset(dp63,0,0,2))*pow(dz,-1));
    }
    else
    {
      ddp011L = 0.0833333333333333333333333333333*(GFOffset(dp01,-2,0,0) - 
        8*GFOffset(dp01,-1,0,0) + 8*GFOffset(dp01,1,0,0) - 
        GFOffset(dp01,2,0,0))*pow(dx,-1);
      
      ddp012L = 0.0833333333333333333333333333333*(GFOffset(dp01,0,-2,0) - 
        8*GFOffset(dp01,0,-1,0) + 8*GFOffset(dp01,0,1,0) - 
        GFOffset(dp01,0,2,0))*pow(dy,-1);
      
      ddp013L = 0.0833333333333333333333333333333*(GFOffset(dp01,0,0,-2) - 
        8*GFOffset(dp01,0,0,-1) + 8*GFOffset(dp01,0,0,1) - 
        GFOffset(dp01,0,0,2))*pow(dz,-1);
      
      ddp022L = 0.0833333333333333333333333333333*(GFOffset(dp02,0,-2,0) - 
        8*GFOffset(dp02,0,-1,0) + 8*GFOffset(dp02,0,1,0) - 
        GFOffset(dp02,0,2,0))*pow(dy,-1);
      
      ddp023L = 0.0833333333333333333333333333333*(GFOffset(dp02,0,0,-2) - 
        8*GFOffset(dp02,0,0,-1) + 8*GFOffset(dp02,0,0,1) - 
        GFOffset(dp02,0,0,2))*pow(dz,-1);
      
      ddp033L = 0.0833333333333333333333333333333*(GFOffset(dp03,0,0,-2) - 
        8*GFOffset(dp03,0,0,-1) + 8*GFOffset(dp03,0,0,1) - 
        GFOffset(dp03,0,0,2))*pow(dz,-1);
      
      ddp111L = 0.0833333333333333333333333333333*(GFOffset(dp11,-2,0,0) - 
        8*GFOffset(dp11,-1,0,0) + 8*GFOffset(dp11,1,0,0) - 
        GFOffset(dp11,2,0,0))*pow(dx,-1);
      
      ddp112L = 0.0833333333333333333333333333333*(GFOffset(dp11,0,-2,0) - 
        8*GFOffset(dp11,0,-1,0) + 8*GFOffset(dp11,0,1,0) - 
        GFOffset(dp11,0,2,0))*pow(dy,-1);
      
      ddp113L = 0.0833333333333333333333333333333*(GFOffset(dp11,0,0,-2) - 
        8*GFOffset(dp11,0,0,-1) + 8*GFOffset(dp11,0,0,1) - 
        GFOffset(dp11,0,0,2))*pow(dz,-1);
      
      ddp122L = 0.0833333333333333333333333333333*(GFOffset(dp12,0,-2,0) - 
        8*GFOffset(dp12,0,-1,0) + 8*GFOffset(dp12,0,1,0) - 
        GFOffset(dp12,0,2,0))*pow(dy,-1);
      
      ddp123L = 0.0833333333333333333333333333333*(GFOffset(dp12,0,0,-2) - 
        8*GFOffset(dp12,0,0,-1) + 8*GFOffset(dp12,0,0,1) - 
        GFOffset(dp12,0,0,2))*pow(dz,-1);
      
      ddp133L = 0.0833333333333333333333333333333*(GFOffset(dp13,0,0,-2) - 
        8*GFOffset(dp13,0,0,-1) + 8*GFOffset(dp13,0,0,1) - 
        GFOffset(dp13,0,0,2))*pow(dz,-1);
      
      ddp211L = 0.0833333333333333333333333333333*(GFOffset(dp21,-2,0,0) - 
        8*GFOffset(dp21,-1,0,0) + 8*GFOffset(dp21,1,0,0) - 
        GFOffset(dp21,2,0,0))*pow(dx,-1);
      
      ddp212L = 0.0833333333333333333333333333333*(GFOffset(dp21,0,-2,0) - 
        8*GFOffset(dp21,0,-1,0) + 8*GFOffset(dp21,0,1,0) - 
        GFOffset(dp21,0,2,0))*pow(dy,-1);
      
      ddp213L = 0.0833333333333333333333333333333*(GFOffset(dp21,0,0,-2) - 
        8*GFOffset(dp21,0,0,-1) + 8*GFOffset(dp21,0,0,1) - 
        GFOffset(dp21,0,0,2))*pow(dz,-1);
      
      ddp222L = 0.0833333333333333333333333333333*(GFOffset(dp22,0,-2,0) - 
        8*GFOffset(dp22,0,-1,0) + 8*GFOffset(dp22,0,1,0) - 
        GFOffset(dp22,0,2,0))*pow(dy,-1);
      
      ddp223L = 0.0833333333333333333333333333333*(GFOffset(dp22,0,0,-2) - 
        8*GFOffset(dp22,0,0,-1) + 8*GFOffset(dp22,0,0,1) - 
        GFOffset(dp22,0,0,2))*pow(dz,-1);
      
      ddp233L = 0.0833333333333333333333333333333*(GFOffset(dp23,0,0,-2) - 
        8*GFOffset(dp23,0,0,-1) + 8*GFOffset(dp23,0,0,1) - 
        GFOffset(dp23,0,0,2))*pow(dz,-1);
      
      ddp311L = 0.0833333333333333333333333333333*(GFOffset(dp31,-2,0,0) - 
        8*GFOffset(dp31,-1,0,0) + 8*GFOffset(dp31,1,0,0) - 
        GFOffset(dp31,2,0,0))*pow(dx,-1);
      
      ddp312L = 0.0833333333333333333333333333333*(GFOffset(dp31,0,-2,0) - 
        8*GFOffset(dp31,0,-1,0) + 8*GFOffset(dp31,0,1,0) - 
        GFOffset(dp31,0,2,0))*pow(dy,-1);
      
      ddp313L = 0.0833333333333333333333333333333*(GFOffset(dp31,0,0,-2) - 
        8*GFOffset(dp31,0,0,-1) + 8*GFOffset(dp31,0,0,1) - 
        GFOffset(dp31,0,0,2))*pow(dz,-1);
      
      ddp322L = 0.0833333333333333333333333333333*(GFOffset(dp32,0,-2,0) - 
        8*GFOffset(dp32,0,-1,0) + 8*GFOffset(dp32,0,1,0) - 
        GFOffset(dp32,0,2,0))*pow(dy,-1);
      
      ddp323L = 0.0833333333333333333333333333333*(GFOffset(dp32,0,0,-2) - 
        8*GFOffset(dp32,0,0,-1) + 8*GFOffset(dp32,0,0,1) - 
        GFOffset(dp32,0,0,2))*pow(dz,-1);
      
      ddp333L = 0.0833333333333333333333333333333*(GFOffset(dp33,0,0,-2) - 
        8*GFOffset(dp33,0,0,-1) + 8*GFOffset(dp33,0,0,1) - 
        GFOffset(dp33,0,0,2))*pow(dz,-1);
      
      ddp411L = 0.0833333333333333333333333333333*(GFOffset(dp41,-2,0,0) - 
        8*GFOffset(dp41,-1,0,0) + 8*GFOffset(dp41,1,0,0) - 
        GFOffset(dp41,2,0,0))*pow(dx,-1);
      
      ddp412L = 0.0833333333333333333333333333333*(GFOffset(dp41,0,-2,0) - 
        8*GFOffset(dp41,0,-1,0) + 8*GFOffset(dp41,0,1,0) - 
        GFOffset(dp41,0,2,0))*pow(dy,-1);
      
      ddp413L = 0.0833333333333333333333333333333*(GFOffset(dp41,0,0,-2) - 
        8*GFOffset(dp41,0,0,-1) + 8*GFOffset(dp41,0,0,1) - 
        GFOffset(dp41,0,0,2))*pow(dz,-1);
      
      ddp422L = 0.0833333333333333333333333333333*(GFOffset(dp42,0,-2,0) - 
        8*GFOffset(dp42,0,-1,0) + 8*GFOffset(dp42,0,1,0) - 
        GFOffset(dp42,0,2,0))*pow(dy,-1);
      
      ddp423L = 0.0833333333333333333333333333333*(GFOffset(dp42,0,0,-2) - 
        8*GFOffset(dp42,0,0,-1) + 8*GFOffset(dp42,0,0,1) - 
        GFOffset(dp42,0,0,2))*pow(dz,-1);
      
      ddp433L = 0.0833333333333333333333333333333*(GFOffset(dp43,0,0,-2) - 
        8*GFOffset(dp43,0,0,-1) + 8*GFOffset(dp43,0,0,1) - 
        GFOffset(dp43,0,0,2))*pow(dz,-1);
      
      ddp511L = 0.0833333333333333333333333333333*(GFOffset(dp51,-2,0,0) - 
        8*GFOffset(dp51,-1,0,0) + 8*GFOffset(dp51,1,0,0) - 
        GFOffset(dp51,2,0,0))*pow(dx,-1);
      
      ddp512L = 0.0833333333333333333333333333333*(GFOffset(dp51,0,-2,0) - 
        8*GFOffset(dp51,0,-1,0) + 8*GFOffset(dp51,0,1,0) - 
        GFOffset(dp51,0,2,0))*pow(dy,-1);
      
      ddp513L = 0.0833333333333333333333333333333*(GFOffset(dp51,0,0,-2) - 
        8*GFOffset(dp51,0,0,-1) + 8*GFOffset(dp51,0,0,1) - 
        GFOffset(dp51,0,0,2))*pow(dz,-1);
      
      ddp522L = 0.0833333333333333333333333333333*(GFOffset(dp52,0,-2,0) - 
        8*GFOffset(dp52,0,-1,0) + 8*GFOffset(dp52,0,1,0) - 
        GFOffset(dp52,0,2,0))*pow(dy,-1);
      
      ddp523L = 0.0833333333333333333333333333333*(GFOffset(dp52,0,0,-2) - 
        8*GFOffset(dp52,0,0,-1) + 8*GFOffset(dp52,0,0,1) - 
        GFOffset(dp52,0,0,2))*pow(dz,-1);
      
      ddp533L = 0.0833333333333333333333333333333*(GFOffset(dp53,0,0,-2) - 
        8*GFOffset(dp53,0,0,-1) + 8*GFOffset(dp53,0,0,1) - 
        GFOffset(dp53,0,0,2))*pow(dz,-1);
      
      ddp611L = 0.0833333333333333333333333333333*(GFOffset(dp61,-2,0,0) - 
        8*GFOffset(dp61,-1,0,0) + 8*GFOffset(dp61,1,0,0) - 
        GFOffset(dp61,2,0,0))*pow(dx,-1);
      
      ddp612L = 0.0833333333333333333333333333333*(GFOffset(dp61,0,-2,0) - 
        8*GFOffset(dp61,0,-1,0) + 8*GFOffset(dp61,0,1,0) - 
        GFOffset(dp61,0,2,0))*pow(dy,-1);
      
      ddp613L = 0.0833333333333333333333333333333*(GFOffset(dp61,0,0,-2) - 
        8*GFOffset(dp61,0,0,-1) + 8*GFOffset(dp61,0,0,1) - 
        GFOffset(dp61,0,0,2))*pow(dz,-1);
      
      ddp622L = 0.0833333333333333333333333333333*(GFOffset(dp62,0,-2,0) - 
        8*GFOffset(dp62,0,-1,0) + 8*GFOffset(dp62,0,1,0) - 
        GFOffset(dp62,0,2,0))*pow(dy,-1);
      
      ddp623L = 0.0833333333333333333333333333333*(GFOffset(dp62,0,0,-2) - 
        8*GFOffset(dp62,0,0,-1) + 8*GFOffset(dp62,0,0,1) - 
        GFOffset(dp62,0,0,2))*pow(dz,-1);
      
      ddp633L = 0.0833333333333333333333333333333*(GFOffset(dp63,0,0,-2) - 
        8*GFOffset(dp63,0,0,-1) + 8*GFOffset(dp63,0,0,1) - 
        GFOffset(dp63,0,0,2))*pow(dz,-1);
    }
    /* Copy local copies back to grid functions */
    ddp011[index] = ddp011L;
    ddp012[index] = ddp012L;
    ddp013[index] = ddp013L;
    ddp022[index] = ddp022L;
    ddp023[index] = ddp023L;
    ddp033[index] = ddp033L;
    ddp111[index] = ddp111L;
    ddp112[index] = ddp112L;
    ddp113[index] = ddp113L;
    ddp122[index] = ddp122L;
    ddp123[index] = ddp123L;
    ddp133[index] = ddp133L;
    ddp211[index] = ddp211L;
    ddp212[index] = ddp212L;
    ddp213[index] = ddp213L;
    ddp222[index] = ddp222L;
    ddp223[index] = ddp223L;
    ddp233[index] = ddp233L;
    ddp311[index] = ddp311L;
    ddp312[index] = ddp312L;
    ddp313[index] = ddp313L;
    ddp322[index] = ddp322L;
    ddp323[index] = ddp323L;
    ddp333[index] = ddp333L;
    ddp411[index] = ddp411L;
    ddp412[index] = ddp412L;
    ddp413[index] = ddp413L;
    ddp422[index] = ddp422L;
    ddp423[index] = ddp423L;
    ddp433[index] = ddp433L;
    ddp511[index] = ddp511L;
    ddp512[index] = ddp512L;
    ddp513[index] = ddp513L;
    ddp522[index] = ddp522L;
    ddp523[index] = ddp523L;
    ddp533[index] = ddp533L;
    ddp611[index] = ddp611L;
    ddp612[index] = ddp612L;
    ddp613[index] = ddp613L;
    ddp622[index] = ddp622L;
    ddp623[index] = ddp623L;
    ddp633[index] = ddp633L;
  }
  CCTK_ENDLOOP3(ML_Test_FD4_DDPolyCalc);
}
extern "C" void ML_Test_FD4_DDPolyCalc(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_Test_FD4_DDPolyCalc_Body");
  }
  if (cctk_iteration % ML_Test_FD4_DDPolyCalc_calc_every != ML_Test_FD4_DDPolyCalc_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_Test_FD4::WT_ddp0",
    "ML_Test_FD4::WT_ddp1",
    "ML_Test_FD4::WT_ddp2",
    "ML_Test_FD4::WT_ddp3",
    "ML_Test_FD4::WT_ddp4",
    "ML_Test_FD4::WT_ddp5",
    "ML_Test_FD4::WT_ddp6",
    "ML_Test_FD4::WT_dp0",
    "ML_Test_FD4::WT_dp1",
    "ML_Test_FD4::WT_dp2",
    "ML_Test_FD4::WT_dp3",
    "ML_Test_FD4::WT_dp4",
    "ML_Test_FD4::WT_dp5",
    "ML_Test_FD4::WT_dp6"};
  AssertGroupStorage(cctkGH, "ML_Test_FD4_DDPolyCalc", 14, groups);
  
  
  LoopOverInterior(cctkGH, ML_Test_FD4_DDPolyCalc_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_Test_FD4_DDPolyCalc_Body");
  }
}

} // namespace ML_Test_FD4
