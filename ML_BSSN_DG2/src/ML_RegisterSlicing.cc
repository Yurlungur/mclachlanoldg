#include <Slicing.h>

#include <cctk.h>

extern "C"
int ML_BSSN_DG2_RegisterSlicing(void)
{
  Einstein_RegisterSlicing("ML_BSSN_DG2");
  return 0;
}
