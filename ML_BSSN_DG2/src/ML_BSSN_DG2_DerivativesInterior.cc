/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_BSSN_DG2 {

extern "C" void ML_BSSN_DG2_DerivativesInterior_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_DG2_DerivativesInterior_calc_every != ML_BSSN_DG2_DerivativesInterior_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG2::ML_dconfac","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG2::ML_dconfac.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG2::ML_dlapse","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG2::ML_dlapse.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG2::ML_dmetric","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG2::ML_dmetric.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG2::ML_dshift","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG2::ML_dshift.");
  return;
}

static void ML_BSSN_DG2_DerivativesInterior_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_BSSN_DG2_DerivativesInterior,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL alphaL CCTK_ATTRIBUTE_UNUSED = alpha[index];
    CCTK_REAL beta1L CCTK_ATTRIBUTE_UNUSED = beta1[index];
    CCTK_REAL beta2L CCTK_ATTRIBUTE_UNUSED = beta2[index];
    CCTK_REAL beta3L CCTK_ATTRIBUTE_UNUSED = beta3[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL phiWL CCTK_ATTRIBUTE_UNUSED = phiW[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDphiW1 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(phiW,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(phiW,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(phiW,0,0,0) + 2.*GFOffset(phiW,1,0,0) - 
      0.5*GFOffset(phiW,2,0,0),0) + IfThen(ti == 1,-0.5*GFOffset(phiW,-1,0,0) 
      + 0.5*GFOffset(phiW,1,0,0),0) + IfThen(ti == 
      2,0.5*GFOffset(phiW,-2,0,0) - 2.*GFOffset(phiW,-1,0,0) + 
      1.5*GFOffset(phiW,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(phiW,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(phiW,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDphiW2 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(phiW,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(phiW,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(phiW,0,0,0) + 2.*GFOffset(phiW,0,1,0) - 
      0.5*GFOffset(phiW,0,2,0),0) + IfThen(tj == 1,-0.5*GFOffset(phiW,0,-1,0) 
      + 0.5*GFOffset(phiW,0,1,0),0) + IfThen(tj == 
      2,0.5*GFOffset(phiW,0,-2,0) - 2.*GFOffset(phiW,0,-1,0) + 
      1.5*GFOffset(phiW,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(phiW,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(phiW,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDphiW3 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(phiW,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(phiW,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(phiW,0,0,0) + 2.*GFOffset(phiW,0,0,1) - 
      0.5*GFOffset(phiW,0,0,2),0) + IfThen(tk == 1,-0.5*GFOffset(phiW,0,0,-1) 
      + 0.5*GFOffset(phiW,0,0,1),0) + IfThen(tk == 
      2,0.5*GFOffset(phiW,0,0,-2) - 2.*GFOffset(phiW,0,0,-1) + 
      1.5*GFOffset(phiW,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(phiW,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(phiW,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt111 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(gt11,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(gt11,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(gt11,0,0,0) + 2.*GFOffset(gt11,1,0,0) - 
      0.5*GFOffset(gt11,2,0,0),0) + IfThen(ti == 1,-0.5*GFOffset(gt11,-1,0,0) 
      + 0.5*GFOffset(gt11,1,0,0),0) + IfThen(ti == 
      2,0.5*GFOffset(gt11,-2,0,0) - 2.*GFOffset(gt11,-1,0,0) + 
      1.5*GFOffset(gt11,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(gt11,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(gt11,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt112 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(gt11,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(gt11,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(gt11,0,0,0) + 2.*GFOffset(gt11,0,1,0) - 
      0.5*GFOffset(gt11,0,2,0),0) + IfThen(tj == 1,-0.5*GFOffset(gt11,0,-1,0) 
      + 0.5*GFOffset(gt11,0,1,0),0) + IfThen(tj == 
      2,0.5*GFOffset(gt11,0,-2,0) - 2.*GFOffset(gt11,0,-1,0) + 
      1.5*GFOffset(gt11,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(gt11,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(gt11,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt113 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(gt11,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(gt11,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(gt11,0,0,0) + 2.*GFOffset(gt11,0,0,1) - 
      0.5*GFOffset(gt11,0,0,2),0) + IfThen(tk == 1,-0.5*GFOffset(gt11,0,0,-1) 
      + 0.5*GFOffset(gt11,0,0,1),0) + IfThen(tk == 
      2,0.5*GFOffset(gt11,0,0,-2) - 2.*GFOffset(gt11,0,0,-1) + 
      1.5*GFOffset(gt11,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(gt11,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(gt11,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt121 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(gt12,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(gt12,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(gt12,0,0,0) + 2.*GFOffset(gt12,1,0,0) - 
      0.5*GFOffset(gt12,2,0,0),0) + IfThen(ti == 1,-0.5*GFOffset(gt12,-1,0,0) 
      + 0.5*GFOffset(gt12,1,0,0),0) + IfThen(ti == 
      2,0.5*GFOffset(gt12,-2,0,0) - 2.*GFOffset(gt12,-1,0,0) + 
      1.5*GFOffset(gt12,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(gt12,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(gt12,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt122 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(gt12,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(gt12,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(gt12,0,0,0) + 2.*GFOffset(gt12,0,1,0) - 
      0.5*GFOffset(gt12,0,2,0),0) + IfThen(tj == 1,-0.5*GFOffset(gt12,0,-1,0) 
      + 0.5*GFOffset(gt12,0,1,0),0) + IfThen(tj == 
      2,0.5*GFOffset(gt12,0,-2,0) - 2.*GFOffset(gt12,0,-1,0) + 
      1.5*GFOffset(gt12,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(gt12,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(gt12,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt123 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(gt12,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(gt12,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(gt12,0,0,0) + 2.*GFOffset(gt12,0,0,1) - 
      0.5*GFOffset(gt12,0,0,2),0) + IfThen(tk == 1,-0.5*GFOffset(gt12,0,0,-1) 
      + 0.5*GFOffset(gt12,0,0,1),0) + IfThen(tk == 
      2,0.5*GFOffset(gt12,0,0,-2) - 2.*GFOffset(gt12,0,0,-1) + 
      1.5*GFOffset(gt12,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(gt12,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(gt12,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt131 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(gt13,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(gt13,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(gt13,0,0,0) + 2.*GFOffset(gt13,1,0,0) - 
      0.5*GFOffset(gt13,2,0,0),0) + IfThen(ti == 1,-0.5*GFOffset(gt13,-1,0,0) 
      + 0.5*GFOffset(gt13,1,0,0),0) + IfThen(ti == 
      2,0.5*GFOffset(gt13,-2,0,0) - 2.*GFOffset(gt13,-1,0,0) + 
      1.5*GFOffset(gt13,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(gt13,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(gt13,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt132 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(gt13,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(gt13,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(gt13,0,0,0) + 2.*GFOffset(gt13,0,1,0) - 
      0.5*GFOffset(gt13,0,2,0),0) + IfThen(tj == 1,-0.5*GFOffset(gt13,0,-1,0) 
      + 0.5*GFOffset(gt13,0,1,0),0) + IfThen(tj == 
      2,0.5*GFOffset(gt13,0,-2,0) - 2.*GFOffset(gt13,0,-1,0) + 
      1.5*GFOffset(gt13,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(gt13,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(gt13,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt133 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(gt13,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(gt13,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(gt13,0,0,0) + 2.*GFOffset(gt13,0,0,1) - 
      0.5*GFOffset(gt13,0,0,2),0) + IfThen(tk == 1,-0.5*GFOffset(gt13,0,0,-1) 
      + 0.5*GFOffset(gt13,0,0,1),0) + IfThen(tk == 
      2,0.5*GFOffset(gt13,0,0,-2) - 2.*GFOffset(gt13,0,0,-1) + 
      1.5*GFOffset(gt13,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(gt13,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(gt13,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt221 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(gt22,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(gt22,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(gt22,0,0,0) + 2.*GFOffset(gt22,1,0,0) - 
      0.5*GFOffset(gt22,2,0,0),0) + IfThen(ti == 1,-0.5*GFOffset(gt22,-1,0,0) 
      + 0.5*GFOffset(gt22,1,0,0),0) + IfThen(ti == 
      2,0.5*GFOffset(gt22,-2,0,0) - 2.*GFOffset(gt22,-1,0,0) + 
      1.5*GFOffset(gt22,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(gt22,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(gt22,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt222 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(gt22,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(gt22,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(gt22,0,0,0) + 2.*GFOffset(gt22,0,1,0) - 
      0.5*GFOffset(gt22,0,2,0),0) + IfThen(tj == 1,-0.5*GFOffset(gt22,0,-1,0) 
      + 0.5*GFOffset(gt22,0,1,0),0) + IfThen(tj == 
      2,0.5*GFOffset(gt22,0,-2,0) - 2.*GFOffset(gt22,0,-1,0) + 
      1.5*GFOffset(gt22,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(gt22,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(gt22,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt223 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(gt22,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(gt22,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(gt22,0,0,0) + 2.*GFOffset(gt22,0,0,1) - 
      0.5*GFOffset(gt22,0,0,2),0) + IfThen(tk == 1,-0.5*GFOffset(gt22,0,0,-1) 
      + 0.5*GFOffset(gt22,0,0,1),0) + IfThen(tk == 
      2,0.5*GFOffset(gt22,0,0,-2) - 2.*GFOffset(gt22,0,0,-1) + 
      1.5*GFOffset(gt22,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(gt22,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(gt22,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt231 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(gt23,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(gt23,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(gt23,0,0,0) + 2.*GFOffset(gt23,1,0,0) - 
      0.5*GFOffset(gt23,2,0,0),0) + IfThen(ti == 1,-0.5*GFOffset(gt23,-1,0,0) 
      + 0.5*GFOffset(gt23,1,0,0),0) + IfThen(ti == 
      2,0.5*GFOffset(gt23,-2,0,0) - 2.*GFOffset(gt23,-1,0,0) + 
      1.5*GFOffset(gt23,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(gt23,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(gt23,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt232 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(gt23,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(gt23,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(gt23,0,0,0) + 2.*GFOffset(gt23,0,1,0) - 
      0.5*GFOffset(gt23,0,2,0),0) + IfThen(tj == 1,-0.5*GFOffset(gt23,0,-1,0) 
      + 0.5*GFOffset(gt23,0,1,0),0) + IfThen(tj == 
      2,0.5*GFOffset(gt23,0,-2,0) - 2.*GFOffset(gt23,0,-1,0) + 
      1.5*GFOffset(gt23,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(gt23,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(gt23,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt233 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(gt23,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(gt23,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(gt23,0,0,0) + 2.*GFOffset(gt23,0,0,1) - 
      0.5*GFOffset(gt23,0,0,2),0) + IfThen(tk == 1,-0.5*GFOffset(gt23,0,0,-1) 
      + 0.5*GFOffset(gt23,0,0,1),0) + IfThen(tk == 
      2,0.5*GFOffset(gt23,0,0,-2) - 2.*GFOffset(gt23,0,0,-1) + 
      1.5*GFOffset(gt23,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(gt23,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(gt23,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt331 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(gt33,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(gt33,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(gt33,0,0,0) + 2.*GFOffset(gt33,1,0,0) - 
      0.5*GFOffset(gt33,2,0,0),0) + IfThen(ti == 1,-0.5*GFOffset(gt33,-1,0,0) 
      + 0.5*GFOffset(gt33,1,0,0),0) + IfThen(ti == 
      2,0.5*GFOffset(gt33,-2,0,0) - 2.*GFOffset(gt33,-1,0,0) + 
      1.5*GFOffset(gt33,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(gt33,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(gt33,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt332 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(gt33,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(gt33,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(gt33,0,0,0) + 2.*GFOffset(gt33,0,1,0) - 
      0.5*GFOffset(gt33,0,2,0),0) + IfThen(tj == 1,-0.5*GFOffset(gt33,0,-1,0) 
      + 0.5*GFOffset(gt33,0,1,0),0) + IfThen(tj == 
      2,0.5*GFOffset(gt33,0,-2,0) - 2.*GFOffset(gt33,0,-1,0) + 
      1.5*GFOffset(gt33,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(gt33,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(gt33,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt333 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(gt33,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(gt33,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(gt33,0,0,0) + 2.*GFOffset(gt33,0,0,1) - 
      0.5*GFOffset(gt33,0,0,2),0) + IfThen(tk == 1,-0.5*GFOffset(gt33,0,0,-1) 
      + 0.5*GFOffset(gt33,0,0,1),0) + IfThen(tk == 
      2,0.5*GFOffset(gt33,0,0,-2) - 2.*GFOffset(gt33,0,0,-1) + 
      1.5*GFOffset(gt33,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(gt33,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(gt33,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDalpha1 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(alpha,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(alpha,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(alpha,0,0,0) + 2.*GFOffset(alpha,1,0,0) - 
      0.5*GFOffset(alpha,2,0,0),0) + IfThen(ti == 
      1,-0.5*GFOffset(alpha,-1,0,0) + 0.5*GFOffset(alpha,1,0,0),0) + 
      IfThen(ti == 2,0.5*GFOffset(alpha,-2,0,0) - 2.*GFOffset(alpha,-1,0,0) + 
      1.5*GFOffset(alpha,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(alpha,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(alpha,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDalpha2 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(alpha,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(alpha,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(alpha,0,0,0) + 2.*GFOffset(alpha,0,1,0) - 
      0.5*GFOffset(alpha,0,2,0),0) + IfThen(tj == 
      1,-0.5*GFOffset(alpha,0,-1,0) + 0.5*GFOffset(alpha,0,1,0),0) + 
      IfThen(tj == 2,0.5*GFOffset(alpha,0,-2,0) - 2.*GFOffset(alpha,0,-1,0) + 
      1.5*GFOffset(alpha,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(alpha,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(alpha,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDalpha3 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(alpha,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(alpha,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(alpha,0,0,0) + 2.*GFOffset(alpha,0,0,1) - 
      0.5*GFOffset(alpha,0,0,2),0) + IfThen(tk == 
      1,-0.5*GFOffset(alpha,0,0,-1) + 0.5*GFOffset(alpha,0,0,1),0) + 
      IfThen(tk == 2,0.5*GFOffset(alpha,0,0,-2) - 2.*GFOffset(alpha,0,0,-1) + 
      1.5*GFOffset(alpha,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(alpha,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(alpha,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDbeta11 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(beta1,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(beta1,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(beta1,0,0,0) + 2.*GFOffset(beta1,1,0,0) - 
      0.5*GFOffset(beta1,2,0,0),0) + IfThen(ti == 
      1,-0.5*GFOffset(beta1,-1,0,0) + 0.5*GFOffset(beta1,1,0,0),0) + 
      IfThen(ti == 2,0.5*GFOffset(beta1,-2,0,0) - 2.*GFOffset(beta1,-1,0,0) + 
      1.5*GFOffset(beta1,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(beta1,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(beta1,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDbeta21 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(beta2,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(beta2,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(beta2,0,0,0) + 2.*GFOffset(beta2,1,0,0) - 
      0.5*GFOffset(beta2,2,0,0),0) + IfThen(ti == 
      1,-0.5*GFOffset(beta2,-1,0,0) + 0.5*GFOffset(beta2,1,0,0),0) + 
      IfThen(ti == 2,0.5*GFOffset(beta2,-2,0,0) - 2.*GFOffset(beta2,-1,0,0) + 
      1.5*GFOffset(beta2,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(beta2,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(beta2,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDbeta31 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(beta3,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(beta3,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(beta3,0,0,0) + 2.*GFOffset(beta3,1,0,0) - 
      0.5*GFOffset(beta3,2,0,0),0) + IfThen(ti == 
      1,-0.5*GFOffset(beta3,-1,0,0) + 0.5*GFOffset(beta3,1,0,0),0) + 
      IfThen(ti == 2,0.5*GFOffset(beta3,-2,0,0) - 2.*GFOffset(beta3,-1,0,0) + 
      1.5*GFOffset(beta3,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(beta3,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(beta3,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDbeta12 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(beta1,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(beta1,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(beta1,0,0,0) + 2.*GFOffset(beta1,0,1,0) - 
      0.5*GFOffset(beta1,0,2,0),0) + IfThen(tj == 
      1,-0.5*GFOffset(beta1,0,-1,0) + 0.5*GFOffset(beta1,0,1,0),0) + 
      IfThen(tj == 2,0.5*GFOffset(beta1,0,-2,0) - 2.*GFOffset(beta1,0,-1,0) + 
      1.5*GFOffset(beta1,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(beta1,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(beta1,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDbeta22 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(beta2,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(beta2,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(beta2,0,0,0) + 2.*GFOffset(beta2,0,1,0) - 
      0.5*GFOffset(beta2,0,2,0),0) + IfThen(tj == 
      1,-0.5*GFOffset(beta2,0,-1,0) + 0.5*GFOffset(beta2,0,1,0),0) + 
      IfThen(tj == 2,0.5*GFOffset(beta2,0,-2,0) - 2.*GFOffset(beta2,0,-1,0) + 
      1.5*GFOffset(beta2,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(beta2,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(beta2,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDbeta32 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(beta3,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(beta3,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(beta3,0,0,0) + 2.*GFOffset(beta3,0,1,0) - 
      0.5*GFOffset(beta3,0,2,0),0) + IfThen(tj == 
      1,-0.5*GFOffset(beta3,0,-1,0) + 0.5*GFOffset(beta3,0,1,0),0) + 
      IfThen(tj == 2,0.5*GFOffset(beta3,0,-2,0) - 2.*GFOffset(beta3,0,-1,0) + 
      1.5*GFOffset(beta3,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(beta3,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(beta3,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDbeta13 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(beta1,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(beta1,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(beta1,0,0,0) + 2.*GFOffset(beta1,0,0,1) - 
      0.5*GFOffset(beta1,0,0,2),0) + IfThen(tk == 
      1,-0.5*GFOffset(beta1,0,0,-1) + 0.5*GFOffset(beta1,0,0,1),0) + 
      IfThen(tk == 2,0.5*GFOffset(beta1,0,0,-2) - 2.*GFOffset(beta1,0,0,-1) + 
      1.5*GFOffset(beta1,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(beta1,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(beta1,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDbeta23 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(beta2,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(beta2,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(beta2,0,0,0) + 2.*GFOffset(beta2,0,0,1) - 
      0.5*GFOffset(beta2,0,0,2),0) + IfThen(tk == 
      1,-0.5*GFOffset(beta2,0,0,-1) + 0.5*GFOffset(beta2,0,0,1),0) + 
      IfThen(tk == 2,0.5*GFOffset(beta2,0,0,-2) - 2.*GFOffset(beta2,0,0,-1) + 
      1.5*GFOffset(beta2,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(beta2,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(beta2,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDbeta33 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(beta3,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(beta3,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(beta3,0,0,0) + 2.*GFOffset(beta3,0,0,1) - 
      0.5*GFOffset(beta3,0,0,2),0) + IfThen(tk == 
      1,-0.5*GFOffset(beta3,0,0,-1) + 0.5*GFOffset(beta3,0,0,1),0) + 
      IfThen(tk == 2,0.5*GFOffset(beta3,0,0,-2) - 2.*GFOffset(beta3,0,0,-1) + 
      1.5*GFOffset(beta3,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(beta3,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(beta3,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDalpha1L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDalpha2L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDalpha3L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta11L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta12L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta13L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta21L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta22L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta23L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta31L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta32L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta33L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt111L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt112L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt113L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt121L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt122L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt123L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt131L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt132L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt133L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt221L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt222L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt223L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt231L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt232L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt233L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt331L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt332L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt333L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDphiW1L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDphiW2L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDphiW3L CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDphiW1L = J11L*LDphiW1 + J21L*LDphiW2 + J31L*LDphiW3;
      
      PDphiW2L = J12L*LDphiW1 + J22L*LDphiW2 + J32L*LDphiW3;
      
      PDphiW3L = J13L*LDphiW1 + J23L*LDphiW2 + J33L*LDphiW3;
      
      PDgt111L = J11L*LDgt111 + J21L*LDgt112 + J31L*LDgt113;
      
      PDgt112L = J12L*LDgt111 + J22L*LDgt112 + J32L*LDgt113;
      
      PDgt113L = J13L*LDgt111 + J23L*LDgt112 + J33L*LDgt113;
      
      PDgt121L = J11L*LDgt121 + J21L*LDgt122 + J31L*LDgt123;
      
      PDgt122L = J12L*LDgt121 + J22L*LDgt122 + J32L*LDgt123;
      
      PDgt123L = J13L*LDgt121 + J23L*LDgt122 + J33L*LDgt123;
      
      PDgt131L = J11L*LDgt131 + J21L*LDgt132 + J31L*LDgt133;
      
      PDgt132L = J12L*LDgt131 + J22L*LDgt132 + J32L*LDgt133;
      
      PDgt133L = J13L*LDgt131 + J23L*LDgt132 + J33L*LDgt133;
      
      PDgt221L = J11L*LDgt221 + J21L*LDgt222 + J31L*LDgt223;
      
      PDgt222L = J12L*LDgt221 + J22L*LDgt222 + J32L*LDgt223;
      
      PDgt223L = J13L*LDgt221 + J23L*LDgt222 + J33L*LDgt223;
      
      PDgt231L = J11L*LDgt231 + J21L*LDgt232 + J31L*LDgt233;
      
      PDgt232L = J12L*LDgt231 + J22L*LDgt232 + J32L*LDgt233;
      
      PDgt233L = J13L*LDgt231 + J23L*LDgt232 + J33L*LDgt233;
      
      PDgt331L = J11L*LDgt331 + J21L*LDgt332 + J31L*LDgt333;
      
      PDgt332L = J12L*LDgt331 + J22L*LDgt332 + J32L*LDgt333;
      
      PDgt333L = J13L*LDgt331 + J23L*LDgt332 + J33L*LDgt333;
      
      PDalpha1L = J11L*LDalpha1 + J21L*LDalpha2 + J31L*LDalpha3;
      
      PDalpha2L = J12L*LDalpha1 + J22L*LDalpha2 + J32L*LDalpha3;
      
      PDalpha3L = J13L*LDalpha1 + J23L*LDalpha2 + J33L*LDalpha3;
      
      PDbeta11L = J11L*LDbeta11 + J21L*LDbeta12 + J31L*LDbeta13;
      
      PDbeta21L = J11L*LDbeta21 + J21L*LDbeta22 + J31L*LDbeta23;
      
      PDbeta31L = J11L*LDbeta31 + J21L*LDbeta32 + J31L*LDbeta33;
      
      PDbeta12L = J12L*LDbeta11 + J22L*LDbeta12 + J32L*LDbeta13;
      
      PDbeta22L = J12L*LDbeta21 + J22L*LDbeta22 + J32L*LDbeta23;
      
      PDbeta32L = J12L*LDbeta31 + J22L*LDbeta32 + J32L*LDbeta33;
      
      PDbeta13L = J13L*LDbeta11 + J23L*LDbeta12 + J33L*LDbeta13;
      
      PDbeta23L = J13L*LDbeta21 + J23L*LDbeta22 + J33L*LDbeta23;
      
      PDbeta33L = J13L*LDbeta31 + J23L*LDbeta32 + J33L*LDbeta33;
    }
    else
    {
      PDphiW1L = LDphiW1;
      
      PDphiW2L = LDphiW2;
      
      PDphiW3L = LDphiW3;
      
      PDgt111L = LDgt111;
      
      PDgt112L = LDgt112;
      
      PDgt113L = LDgt113;
      
      PDgt121L = LDgt121;
      
      PDgt122L = LDgt122;
      
      PDgt123L = LDgt123;
      
      PDgt131L = LDgt131;
      
      PDgt132L = LDgt132;
      
      PDgt133L = LDgt133;
      
      PDgt221L = LDgt221;
      
      PDgt222L = LDgt222;
      
      PDgt223L = LDgt223;
      
      PDgt231L = LDgt231;
      
      PDgt232L = LDgt232;
      
      PDgt233L = LDgt233;
      
      PDgt331L = LDgt331;
      
      PDgt332L = LDgt332;
      
      PDgt333L = LDgt333;
      
      PDalpha1L = LDalpha1;
      
      PDalpha2L = LDalpha2;
      
      PDalpha3L = LDalpha3;
      
      PDbeta11L = LDbeta11;
      
      PDbeta21L = LDbeta21;
      
      PDbeta31L = LDbeta31;
      
      PDbeta12L = LDbeta12;
      
      PDbeta22L = LDbeta22;
      
      PDbeta32L = LDbeta32;
      
      PDbeta13L = LDbeta13;
      
      PDbeta23L = LDbeta23;
      
      PDbeta33L = LDbeta33;
    }
    /* Copy local copies back to grid functions */
    PDalpha1[index] = PDalpha1L;
    PDalpha2[index] = PDalpha2L;
    PDalpha3[index] = PDalpha3L;
    PDbeta11[index] = PDbeta11L;
    PDbeta12[index] = PDbeta12L;
    PDbeta13[index] = PDbeta13L;
    PDbeta21[index] = PDbeta21L;
    PDbeta22[index] = PDbeta22L;
    PDbeta23[index] = PDbeta23L;
    PDbeta31[index] = PDbeta31L;
    PDbeta32[index] = PDbeta32L;
    PDbeta33[index] = PDbeta33L;
    PDgt111[index] = PDgt111L;
    PDgt112[index] = PDgt112L;
    PDgt113[index] = PDgt113L;
    PDgt121[index] = PDgt121L;
    PDgt122[index] = PDgt122L;
    PDgt123[index] = PDgt123L;
    PDgt131[index] = PDgt131L;
    PDgt132[index] = PDgt132L;
    PDgt133[index] = PDgt133L;
    PDgt221[index] = PDgt221L;
    PDgt222[index] = PDgt222L;
    PDgt223[index] = PDgt223L;
    PDgt231[index] = PDgt231L;
    PDgt232[index] = PDgt232L;
    PDgt233[index] = PDgt233L;
    PDgt331[index] = PDgt331L;
    PDgt332[index] = PDgt332L;
    PDgt333[index] = PDgt333L;
    PDphiW1[index] = PDphiW1L;
    PDphiW2[index] = PDphiW2L;
    PDphiW3[index] = PDphiW3L;
  }
  CCTK_ENDLOOP3(ML_BSSN_DG2_DerivativesInterior);
}
extern "C" void ML_BSSN_DG2_DerivativesInterior(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_DG2_DerivativesInterior_Body");
  }
  if (cctk_iteration % ML_BSSN_DG2_DerivativesInterior_calc_every != ML_BSSN_DG2_DerivativesInterior_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_BSSN_DG2::ML_confac",
    "ML_BSSN_DG2::ML_dconfac",
    "ML_BSSN_DG2::ML_dlapse",
    "ML_BSSN_DG2::ML_dmetric",
    "ML_BSSN_DG2::ML_dshift",
    "ML_BSSN_DG2::ML_lapse",
    "ML_BSSN_DG2::ML_metric",
    "ML_BSSN_DG2::ML_shift"};
  AssertGroupStorage(cctkGH, "ML_BSSN_DG2_DerivativesInterior", 8, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_BSSN_DG2_DerivativesInterior_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_DG2_DerivativesInterior_Body");
  }
}

} // namespace ML_BSSN_DG2
