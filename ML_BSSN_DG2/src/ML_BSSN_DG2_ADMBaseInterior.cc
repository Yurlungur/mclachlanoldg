/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_BSSN_DG2 {

extern "C" void ML_BSSN_DG2_ADMBaseInterior_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_DG2_ADMBaseInterior_calc_every != ML_BSSN_DG2_ADMBaseInterior_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ADMBase::dtlapse","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ADMBase::dtlapse.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ADMBase::dtshift","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ADMBase::dtshift.");
  return;
}

static void ML_BSSN_DG2_ADMBaseInterior_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_BSSN_DG2_ADMBaseInterior,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL AL CCTK_ATTRIBUTE_UNUSED = A[index];
    CCTK_REAL alphaL CCTK_ATTRIBUTE_UNUSED = alpha[index];
    CCTK_REAL B1L CCTK_ATTRIBUTE_UNUSED = B1[index];
    CCTK_REAL B2L CCTK_ATTRIBUTE_UNUSED = B2[index];
    CCTK_REAL B3L CCTK_ATTRIBUTE_UNUSED = B3[index];
    CCTK_REAL beta1L CCTK_ATTRIBUTE_UNUSED = beta1[index];
    CCTK_REAL beta2L CCTK_ATTRIBUTE_UNUSED = beta2[index];
    CCTK_REAL beta3L CCTK_ATTRIBUTE_UNUSED = beta3[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL PDalpha1L CCTK_ATTRIBUTE_UNUSED = PDalpha1[index];
    CCTK_REAL PDalpha2L CCTK_ATTRIBUTE_UNUSED = PDalpha2[index];
    CCTK_REAL PDalpha3L CCTK_ATTRIBUTE_UNUSED = PDalpha3[index];
    CCTK_REAL PDgt111L CCTK_ATTRIBUTE_UNUSED = PDgt111[index];
    CCTK_REAL PDgt112L CCTK_ATTRIBUTE_UNUSED = PDgt112[index];
    CCTK_REAL PDgt113L CCTK_ATTRIBUTE_UNUSED = PDgt113[index];
    CCTK_REAL PDgt121L CCTK_ATTRIBUTE_UNUSED = PDgt121[index];
    CCTK_REAL PDgt122L CCTK_ATTRIBUTE_UNUSED = PDgt122[index];
    CCTK_REAL PDgt123L CCTK_ATTRIBUTE_UNUSED = PDgt123[index];
    CCTK_REAL PDgt131L CCTK_ATTRIBUTE_UNUSED = PDgt131[index];
    CCTK_REAL PDgt132L CCTK_ATTRIBUTE_UNUSED = PDgt132[index];
    CCTK_REAL PDgt133L CCTK_ATTRIBUTE_UNUSED = PDgt133[index];
    CCTK_REAL PDgt221L CCTK_ATTRIBUTE_UNUSED = PDgt221[index];
    CCTK_REAL PDgt222L CCTK_ATTRIBUTE_UNUSED = PDgt222[index];
    CCTK_REAL PDgt223L CCTK_ATTRIBUTE_UNUSED = PDgt223[index];
    CCTK_REAL PDgt231L CCTK_ATTRIBUTE_UNUSED = PDgt231[index];
    CCTK_REAL PDgt232L CCTK_ATTRIBUTE_UNUSED = PDgt232[index];
    CCTK_REAL PDgt233L CCTK_ATTRIBUTE_UNUSED = PDgt233[index];
    CCTK_REAL PDgt331L CCTK_ATTRIBUTE_UNUSED = PDgt331[index];
    CCTK_REAL PDgt332L CCTK_ATTRIBUTE_UNUSED = PDgt332[index];
    CCTK_REAL PDgt333L CCTK_ATTRIBUTE_UNUSED = PDgt333[index];
    CCTK_REAL PDphiW1L CCTK_ATTRIBUTE_UNUSED = PDphiW1[index];
    CCTK_REAL PDphiW2L CCTK_ATTRIBUTE_UNUSED = PDphiW2[index];
    CCTK_REAL PDphiW3L CCTK_ATTRIBUTE_UNUSED = PDphiW3[index];
    CCTK_REAL phiWL CCTK_ATTRIBUTE_UNUSED = phiW[index];
    CCTK_REAL rL CCTK_ATTRIBUTE_UNUSED = r[index];
    CCTK_REAL trKL CCTK_ATTRIBUTE_UNUSED = trK[index];
    CCTK_REAL Xt1L CCTK_ATTRIBUTE_UNUSED = Xt1[index];
    CCTK_REAL Xt2L CCTK_ATTRIBUTE_UNUSED = Xt2[index];
    CCTK_REAL Xt3L CCTK_ATTRIBUTE_UNUSED = Xt3[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDualpha1 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(alpha,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(alpha,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(alpha,0,0,0) + 2.*GFOffset(alpha,1,0,0) - 
      0.5*GFOffset(alpha,2,0,0),0) + IfThen(ti == 
      1,-0.5*GFOffset(alpha,-1,0,0) + 0.5*GFOffset(alpha,1,0,0),0) + 
      IfThen(ti == 2,0.5*GFOffset(alpha,-2,0,0) - 2.*GFOffset(alpha,-1,0,0) + 
      1.5*GFOffset(alpha,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(alpha,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(alpha,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDualpha2 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(alpha,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(alpha,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(alpha,0,0,0) + 2.*GFOffset(alpha,0,1,0) - 
      0.5*GFOffset(alpha,0,2,0),0) + IfThen(tj == 
      1,-0.5*GFOffset(alpha,0,-1,0) + 0.5*GFOffset(alpha,0,1,0),0) + 
      IfThen(tj == 2,0.5*GFOffset(alpha,0,-2,0) - 2.*GFOffset(alpha,0,-1,0) + 
      1.5*GFOffset(alpha,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(alpha,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(alpha,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDualpha3 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(alpha,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(alpha,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(alpha,0,0,0) + 2.*GFOffset(alpha,0,0,1) - 
      0.5*GFOffset(alpha,0,0,2),0) + IfThen(tk == 
      1,-0.5*GFOffset(alpha,0,0,-1) + 0.5*GFOffset(alpha,0,0,1),0) + 
      IfThen(tk == 2,0.5*GFOffset(alpha,0,0,-2) - 2.*GFOffset(alpha,0,0,-1) + 
      1.5*GFOffset(alpha,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(alpha,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(alpha,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDubeta11 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(beta1,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(beta1,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(beta1,0,0,0) + 2.*GFOffset(beta1,1,0,0) - 
      0.5*GFOffset(beta1,2,0,0),0) + IfThen(ti == 
      1,-0.5*GFOffset(beta1,-1,0,0) + 0.5*GFOffset(beta1,1,0,0),0) + 
      IfThen(ti == 2,0.5*GFOffset(beta1,-2,0,0) - 2.*GFOffset(beta1,-1,0,0) + 
      1.5*GFOffset(beta1,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(beta1,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(beta1,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDubeta21 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(beta2,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(beta2,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(beta2,0,0,0) + 2.*GFOffset(beta2,1,0,0) - 
      0.5*GFOffset(beta2,2,0,0),0) + IfThen(ti == 
      1,-0.5*GFOffset(beta2,-1,0,0) + 0.5*GFOffset(beta2,1,0,0),0) + 
      IfThen(ti == 2,0.5*GFOffset(beta2,-2,0,0) - 2.*GFOffset(beta2,-1,0,0) + 
      1.5*GFOffset(beta2,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(beta2,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(beta2,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDubeta31 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(ti == 
      0,-3.*GFOffset(beta3,0,0,0),0) + IfThen(ti == 
      2,3.*GFOffset(beta3,0,0,0),0))*pow(dx,-1) + 
      0.666666666666666666666666666667*(IfThen(ti == 
      0,-1.5*GFOffset(beta3,0,0,0) + 2.*GFOffset(beta3,1,0,0) - 
      0.5*GFOffset(beta3,2,0,0),0) + IfThen(ti == 
      1,-0.5*GFOffset(beta3,-1,0,0) + 0.5*GFOffset(beta3,1,0,0),0) + 
      IfThen(ti == 2,0.5*GFOffset(beta3,-2,0,0) - 2.*GFOffset(beta3,-1,0,0) + 
      1.5*GFOffset(beta3,0,0,0),0))*pow(dx,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(ti == 
      0,3.*GFOffset(beta3,-1,0,0),0) + IfThen(ti == 
      2,-3.*GFOffset(beta3,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDubeta12 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(beta1,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(beta1,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(beta1,0,0,0) + 2.*GFOffset(beta1,0,1,0) - 
      0.5*GFOffset(beta1,0,2,0),0) + IfThen(tj == 
      1,-0.5*GFOffset(beta1,0,-1,0) + 0.5*GFOffset(beta1,0,1,0),0) + 
      IfThen(tj == 2,0.5*GFOffset(beta1,0,-2,0) - 2.*GFOffset(beta1,0,-1,0) + 
      1.5*GFOffset(beta1,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(beta1,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(beta1,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDubeta22 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(beta2,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(beta2,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(beta2,0,0,0) + 2.*GFOffset(beta2,0,1,0) - 
      0.5*GFOffset(beta2,0,2,0),0) + IfThen(tj == 
      1,-0.5*GFOffset(beta2,0,-1,0) + 0.5*GFOffset(beta2,0,1,0),0) + 
      IfThen(tj == 2,0.5*GFOffset(beta2,0,-2,0) - 2.*GFOffset(beta2,0,-1,0) + 
      1.5*GFOffset(beta2,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(beta2,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(beta2,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDubeta32 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tj == 
      0,-3.*GFOffset(beta3,0,0,0),0) + IfThen(tj == 
      2,3.*GFOffset(beta3,0,0,0),0))*pow(dy,-1) + 
      0.666666666666666666666666666667*(IfThen(tj == 
      0,-1.5*GFOffset(beta3,0,0,0) + 2.*GFOffset(beta3,0,1,0) - 
      0.5*GFOffset(beta3,0,2,0),0) + IfThen(tj == 
      1,-0.5*GFOffset(beta3,0,-1,0) + 0.5*GFOffset(beta3,0,1,0),0) + 
      IfThen(tj == 2,0.5*GFOffset(beta3,0,-2,0) - 2.*GFOffset(beta3,0,-1,0) + 
      1.5*GFOffset(beta3,0,0,0),0))*pow(dy,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tj == 
      0,3.*GFOffset(beta3,0,-1,0),0) + IfThen(tj == 
      2,-3.*GFOffset(beta3,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDubeta13 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(beta1,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(beta1,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(beta1,0,0,0) + 2.*GFOffset(beta1,0,0,1) - 
      0.5*GFOffset(beta1,0,0,2),0) + IfThen(tk == 
      1,-0.5*GFOffset(beta1,0,0,-1) + 0.5*GFOffset(beta1,0,0,1),0) + 
      IfThen(tk == 2,0.5*GFOffset(beta1,0,0,-2) - 2.*GFOffset(beta1,0,0,-1) + 
      1.5*GFOffset(beta1,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(beta1,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(beta1,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDubeta23 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(beta2,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(beta2,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(beta2,0,0,0) + 2.*GFOffset(beta2,0,0,1) - 
      0.5*GFOffset(beta2,0,0,2),0) + IfThen(tk == 
      1,-0.5*GFOffset(beta2,0,0,-1) + 0.5*GFOffset(beta2,0,0,1),0) + 
      IfThen(tk == 2,0.5*GFOffset(beta2,0,0,-2) - 2.*GFOffset(beta2,0,0,-1) + 
      1.5*GFOffset(beta2,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(beta2,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(beta2,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDubeta33 CCTK_ATTRIBUTE_UNUSED = 
      -0.333333333333333333333333333333*(IfThen(tk == 
      0,-3.*GFOffset(beta3,0,0,0),0) + IfThen(tk == 
      2,3.*GFOffset(beta3,0,0,0),0))*pow(dz,-1) + 
      0.666666666666666666666666666667*(IfThen(tk == 
      0,-1.5*GFOffset(beta3,0,0,0) + 2.*GFOffset(beta3,0,0,1) - 
      0.5*GFOffset(beta3,0,0,2),0) + IfThen(tk == 
      1,-0.5*GFOffset(beta3,0,0,-1) + 0.5*GFOffset(beta3,0,0,1),0) + 
      IfThen(tk == 2,0.5*GFOffset(beta3,0,0,-2) - 2.*GFOffset(beta3,0,0,-1) + 
      1.5*GFOffset(beta3,0,0,0),0))*pow(dz,-1) - 
      0.666666666666666666666666666667*alphaDeriv*(IfThen(tk == 
      0,3.*GFOffset(beta3,0,0,-1),0) + IfThen(tk == 
      2,-3.*GFOffset(beta3,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDualpha1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualpha2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualpha3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDualpha1 = J11L*LDualpha1 + J21L*LDualpha2 + J31L*LDualpha3;
      
      PDualpha2 = J12L*LDualpha1 + J22L*LDualpha2 + J32L*LDualpha3;
      
      PDualpha3 = J13L*LDualpha1 + J23L*LDualpha2 + J33L*LDualpha3;
      
      PDubeta11 = J11L*LDubeta11 + J21L*LDubeta12 + J31L*LDubeta13;
      
      PDubeta21 = J11L*LDubeta21 + J21L*LDubeta22 + J31L*LDubeta23;
      
      PDubeta31 = J11L*LDubeta31 + J21L*LDubeta32 + J31L*LDubeta33;
      
      PDubeta12 = J12L*LDubeta11 + J22L*LDubeta12 + J32L*LDubeta13;
      
      PDubeta22 = J12L*LDubeta21 + J22L*LDubeta22 + J32L*LDubeta23;
      
      PDubeta32 = J12L*LDubeta31 + J22L*LDubeta32 + J32L*LDubeta33;
      
      PDubeta13 = J13L*LDubeta11 + J23L*LDubeta12 + J33L*LDubeta13;
      
      PDubeta23 = J13L*LDubeta21 + J23L*LDubeta22 + J33L*LDubeta23;
      
      PDubeta33 = J13L*LDubeta31 + J23L*LDubeta32 + J33L*LDubeta33;
    }
    else
    {
      PDualpha1 = LDualpha1;
      
      PDualpha2 = LDualpha2;
      
      PDualpha3 = LDualpha3;
      
      PDubeta11 = LDubeta11;
      
      PDubeta21 = LDubeta21;
      
      PDubeta31 = LDubeta31;
      
      PDubeta12 = LDubeta12;
      
      PDubeta22 = LDubeta22;
      
      PDubeta32 = LDubeta32;
      
      PDubeta13 = LDubeta13;
      
      PDubeta23 = LDubeta23;
      
      PDubeta33 = LDubeta33;
    }
    
    CCTK_REAL detgt CCTK_ATTRIBUTE_UNUSED = 1;
    
    CCTK_REAL gtu11 CCTK_ATTRIBUTE_UNUSED = (gt22L*gt33L - 
      pow(gt23L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu12 CCTK_ATTRIBUTE_UNUSED = (gt13L*gt23L - 
      gt12L*gt33L)*pow(detgt,-1);
    
    CCTK_REAL gtu13 CCTK_ATTRIBUTE_UNUSED = (-(gt13L*gt22L) + 
      gt12L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu22 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt33L - 
      pow(gt13L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu23 CCTK_ATTRIBUTE_UNUSED = (gt12L*gt13L - 
      gt11L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu33 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt22L - 
      pow(gt12L,2))*pow(detgt,-1);
    
    CCTK_REAL em4phi CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,pow(phiWL,2),exp(-4*phiWL));
    
    CCTK_REAL gu11 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu11;
    
    CCTK_REAL gu12 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu12;
    
    CCTK_REAL gu13 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu13;
    
    CCTK_REAL gu22 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu22;
    
    CCTK_REAL gu23 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu23;
    
    CCTK_REAL gu33 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu33;
    
    CCTK_REAL fac1 CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,-0.5*pow(phiWL,-1),1);
    
    CCTK_REAL cdphi1 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*fac1;
    
    CCTK_REAL cdphi2 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*fac1;
    
    CCTK_REAL cdphi3 CCTK_ATTRIBUTE_UNUSED = PDphiW3L*fac1;
    
    CCTK_REAL dotalpha CCTK_ATTRIBUTE_UNUSED = -(harmonicF*IfThen(evolveA 
      != 0,AL,trKL + (-1 + alphaL)*alphaDriver)*pow(alphaL,harmonicN));
    
    CCTK_REAL betaDriverValue CCTK_ATTRIBUTE_UNUSED = 
      IfThen(useSpatialBetaDriver != 
      0,betaDriver*spatialBetaDriverRadius*pow(fmax(rL,spatialBetaDriverRadius),-1),betaDriver);
    
    CCTK_REAL shiftGammaCoeffValue CCTK_ATTRIBUTE_UNUSED = 
      IfThen(useSpatialShiftGammaCoeff != 0,shiftGammaCoeff*fmin(1,exp(1 - 
      rL*pow(spatialShiftGammaCoeffRadius,-1))),shiftGammaCoeff);
    
    CCTK_REAL ddetgt1 CCTK_ATTRIBUTE_UNUSED = PDgt111L*gtu11 + 
      2*PDgt121L*gtu12 + 2*PDgt131L*gtu13 + PDgt221L*gtu22 + 2*PDgt231L*gtu23 
      + PDgt331L*gtu33;
    
    CCTK_REAL ddetgt2 CCTK_ATTRIBUTE_UNUSED = PDgt112L*gtu11 + 
      2*PDgt122L*gtu12 + 2*PDgt132L*gtu13 + PDgt222L*gtu22 + 2*PDgt232L*gtu23 
      + PDgt332L*gtu33;
    
    CCTK_REAL ddetgt3 CCTK_ATTRIBUTE_UNUSED = PDgt113L*gtu11 + 
      2*PDgt123L*gtu12 + 2*PDgt133L*gtu13 + PDgt223L*gtu22 + 2*PDgt233L*gtu23 
      + PDgt333L*gtu33;
    
    CCTK_REAL dotbeta1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dotbeta2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dotbeta3 CCTK_ATTRIBUTE_UNUSED;
    
    if (shiftFormulation == 0)
    {
      dotbeta1 = shiftGammaCoeffValue*IfThen(evolveB != 0,B1L,Xt1L - 
        beta1L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
      
      dotbeta2 = shiftGammaCoeffValue*IfThen(evolveB != 0,B2L,Xt2L - 
        beta2L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
      
      dotbeta3 = shiftGammaCoeffValue*IfThen(evolveB != 0,B3L,Xt3L - 
        beta3L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
    }
    else
    {
      dotbeta1 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu11 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu12 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu13));
      
      dotbeta2 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu12 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu22 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu23));
      
      dotbeta3 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu13 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu23 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu33));
    }
    
    CCTK_REAL dtalpL CCTK_ATTRIBUTE_UNUSED = dotalpha + IfThen(advectLapse 
      != 0,beta1L*PDualpha1 + beta2L*PDualpha2 + beta3L*PDualpha3,0);
    
    CCTK_REAL dtbetaxL CCTK_ATTRIBUTE_UNUSED = dotbeta1 + 
      IfThen(advectShift != 0,beta1L*PDubeta11 + beta2L*PDubeta12 + 
      beta3L*PDubeta13,0);
    
    CCTK_REAL dtbetayL CCTK_ATTRIBUTE_UNUSED = dotbeta2 + 
      IfThen(advectShift != 0,beta1L*PDubeta21 + beta2L*PDubeta22 + 
      beta3L*PDubeta23,0);
    
    CCTK_REAL dtbetazL CCTK_ATTRIBUTE_UNUSED = dotbeta3 + 
      IfThen(advectShift != 0,beta1L*PDubeta31 + beta2L*PDubeta32 + 
      beta3L*PDubeta33,0);
    /* Copy local copies back to grid functions */
    dtalp[index] = dtalpL;
    dtbetax[index] = dtbetaxL;
    dtbetay[index] = dtbetayL;
    dtbetaz[index] = dtbetazL;
  }
  CCTK_ENDLOOP3(ML_BSSN_DG2_ADMBaseInterior);
}
extern "C" void ML_BSSN_DG2_ADMBaseInterior(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_DG2_ADMBaseInterior_Body");
  }
  if (cctk_iteration % ML_BSSN_DG2_ADMBaseInterior_calc_every != ML_BSSN_DG2_ADMBaseInterior_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ADMBase::dtlapse",
    "ADMBase::dtshift",
    "grid::coordinates",
    "ML_BSSN_DG2::ML_confac",
    "ML_BSSN_DG2::ML_dconfac",
    "ML_BSSN_DG2::ML_dlapse",
    "ML_BSSN_DG2::ML_dmetric",
    "ML_BSSN_DG2::ML_dtlapse",
    "ML_BSSN_DG2::ML_dtshift",
    "ML_BSSN_DG2::ML_Gamma",
    "ML_BSSN_DG2::ML_lapse",
    "ML_BSSN_DG2::ML_metric",
    "ML_BSSN_DG2::ML_shift",
    "ML_BSSN_DG2::ML_trace_curv"};
  AssertGroupStorage(cctkGH, "ML_BSSN_DG2_ADMBaseInterior", 14, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_BSSN_DG2_ADMBaseInterior_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_DG2_ADMBaseInterior_Body");
  }
}

} // namespace ML_BSSN_DG2
