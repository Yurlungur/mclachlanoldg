ActiveThorns = "
   Boundary
   Carpet
   CarpetIOASCII
   CarpetIOBasic
   CarpetIOScalar
   CarpetLib
   CarpetReduce
   CartGrid3D
   CoordBase
   Coordinates
   GenericFD
   IOUtil
   ML_Wavetoy_DG4
   MoL
   NaNChecker
   Periodic
   Slab
   SymBase
   Time
"

#############################################################
# Grid
#############################################################

CartGrid3D::type               = "multipatch"
Coordinates::coordinate_system = "Cartesian"

Coordinates::patch_xmin = 0.0
Coordinates::patch_ymin = 0.0
Coordinates::patch_zmin = 0.0
Coordinates::patch_xmax = 1.0
Coordinates::patch_ymax = 1.0
Coordinates::patch_zmax = 1.0

Coordinates::ncells_x = 5*2   # (order+1)*n
Coordinates::ncells_y = 5*2
Coordinates::ncells_z = 5*2

Coordinates::stagger_patch_boundaries = yes
Coordinates::stagger_outer_boundaries = yes

Coordinates::register_symmetry = no
Periodic::periodic             = yes

Coordinates::store_jacobian            = no
Coordinates::store_inverse_jacobian    = no
Coordinates::store_jacobian_derivative = no
Coordinates::store_volume_form         = no

#############################################################
# Carpet
#############################################################

Carpet::domain_from_multipatch = yes
Carpet::ghost_size             = 1
Carpet::granularity            = 5   # (order+1)*m
Carpet::granularity_boundary   = 1

#############################################################
# Time integration
#############################################################

Cactus::terminate       = "time"
Cactus::cctk_final_time = 0.2

Time::timestep_method = "given"
Time::timestep        = 0.1/2

MethodOfLines::ODE_method             = "RK4"
MethodOfLines::MoL_Intermediate_Steps = 4
MethodOfLines::MoL_Num_Scratch_Levels = 1

MethodOfLines::init_RHS_zero = no

MethodOfLines::MoL_NaN_Check = yes

#############################################################
# ML_WaveToy
#############################################################

ML_WaveToy_DG4::tile_size = 5   # order+1

ML_WaveToy_DG4::initial_data = "sine"
ML_WaveToy_DG4::width        = 1.0

ML_WaveToy_DG4::epsDiss = 0.0

#############################################################
# Output
#############################################################

IO::out_dir       = $parfile
IO::out_fileinfo  = "none"
IO::parfile_write = "no"

IOBasic::outInfo_every      = 1*2
IOBasic::outInfo_reductions = "sum norm2"
IOBasic::outInfo_vars       = "
   ML_WaveToy_DG4::WT_u
   ML_WaveToy_DG4::WT_energy
"

IOScalar::one_file_per_group = yes
IOScalar::out_precision       = 17

IOScalar::outScalar_every = 0*2
IOScalar::outScalar_vars  = "
   ML_WaveToy_DG4::WT_u
   ML_WaveToy_DG4::WT_rho
   ML_WaveToy_DG4::WT_v
   ML_WaveToy_DG4::WT_urhs
   ML_WaveToy_DG4::WT_rhorhs
   ML_WaveToy_DG4::WT_vrhs
   ML_WaveToy_DG4::WT_w
   ML_WaveToy_DG4::WT_energy
"

IOASCII::one_file_per_group = yes
IOASCII::out_precision      = 17

IOASCII::out1D_every = 2*2
IOASCII::out1D_vars  = "
   grid::coordinates
   CarpetReduce::weight
   ML_WaveToy_DG4::WT_u
   ML_WaveToy_DG4::WT_rho
   ML_WaveToy_DG4::WT_v
   ML_WaveToy_DG4::WT_urhs
   ML_WaveToy_DG4::WT_rhorhs
   ML_WaveToy_DG4::WT_vrhs
   ML_WaveToy_DG4::WT_w
   ML_WaveToy_DG4::WT_energy
"
