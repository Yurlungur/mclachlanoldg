/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_BSSN_DG4 {

extern "C" void ML_BSSN_DG4_InitialADMBase2Interior_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_DG4_InitialADMBase2Interior_calc_every != ML_BSSN_DG4_InitialADMBase2Interior_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG4::ML_dmetric","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG4::ML_dmetric.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG4::ML_dtlapse","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG4::ML_dtlapse.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG4::ML_dtshift","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG4::ML_dtshift.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG4::ML_Gamma","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG4::ML_Gamma.");
  return;
}

static void ML_BSSN_DG4_InitialADMBase2Interior_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_BSSN_DG4_InitialADMBase2Interior,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL alpL CCTK_ATTRIBUTE_UNUSED = alp[index];
    CCTK_REAL betaxL CCTK_ATTRIBUTE_UNUSED = betax[index];
    CCTK_REAL betayL CCTK_ATTRIBUTE_UNUSED = betay[index];
    CCTK_REAL betazL CCTK_ATTRIBUTE_UNUSED = betaz[index];
    CCTK_REAL dtalpL CCTK_ATTRIBUTE_UNUSED = dtalp[index];
    CCTK_REAL dtbetaxL CCTK_ATTRIBUTE_UNUSED = dtbetax[index];
    CCTK_REAL dtbetayL CCTK_ATTRIBUTE_UNUSED = dtbetay[index];
    CCTK_REAL dtbetazL CCTK_ATTRIBUTE_UNUSED = dtbetaz[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL gxxL CCTK_ATTRIBUTE_UNUSED = gxx[index];
    CCTK_REAL gxyL CCTK_ATTRIBUTE_UNUSED = gxy[index];
    CCTK_REAL gxzL CCTK_ATTRIBUTE_UNUSED = gxz[index];
    CCTK_REAL gyyL CCTK_ATTRIBUTE_UNUSED = gyy[index];
    CCTK_REAL gyzL CCTK_ATTRIBUTE_UNUSED = gyz[index];
    CCTK_REAL gzzL CCTK_ATTRIBUTE_UNUSED = gzz[index];
    CCTK_REAL PDgt111L CCTK_ATTRIBUTE_UNUSED = PDgt111[index];
    CCTK_REAL PDgt112L CCTK_ATTRIBUTE_UNUSED = PDgt112[index];
    CCTK_REAL PDgt113L CCTK_ATTRIBUTE_UNUSED = PDgt113[index];
    CCTK_REAL PDgt121L CCTK_ATTRIBUTE_UNUSED = PDgt121[index];
    CCTK_REAL PDgt122L CCTK_ATTRIBUTE_UNUSED = PDgt122[index];
    CCTK_REAL PDgt123L CCTK_ATTRIBUTE_UNUSED = PDgt123[index];
    CCTK_REAL PDgt131L CCTK_ATTRIBUTE_UNUSED = PDgt131[index];
    CCTK_REAL PDgt132L CCTK_ATTRIBUTE_UNUSED = PDgt132[index];
    CCTK_REAL PDgt133L CCTK_ATTRIBUTE_UNUSED = PDgt133[index];
    CCTK_REAL PDgt221L CCTK_ATTRIBUTE_UNUSED = PDgt221[index];
    CCTK_REAL PDgt222L CCTK_ATTRIBUTE_UNUSED = PDgt222[index];
    CCTK_REAL PDgt223L CCTK_ATTRIBUTE_UNUSED = PDgt223[index];
    CCTK_REAL PDgt231L CCTK_ATTRIBUTE_UNUSED = PDgt231[index];
    CCTK_REAL PDgt232L CCTK_ATTRIBUTE_UNUSED = PDgt232[index];
    CCTK_REAL PDgt233L CCTK_ATTRIBUTE_UNUSED = PDgt233[index];
    CCTK_REAL PDgt331L CCTK_ATTRIBUTE_UNUSED = PDgt331[index];
    CCTK_REAL PDgt332L CCTK_ATTRIBUTE_UNUSED = PDgt332[index];
    CCTK_REAL PDgt333L CCTK_ATTRIBUTE_UNUSED = PDgt333[index];
    CCTK_REAL phiWL CCTK_ATTRIBUTE_UNUSED = phiW[index];
    CCTK_REAL rL CCTK_ATTRIBUTE_UNUSED = r[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDgt111 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(ti == 
      0,-10.*GFOffset(gt11,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(gt11,0,0,0),0))*pow(dx,-1) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(gt11,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt11,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(gt11,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(gt11,3,0,0) - 
      0.5*GFOffset(gt11,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(gt11,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(gt11,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(gt11,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(gt11,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(gt11,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(gt11,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(gt11,1,0,0) - 
      0.375*GFOffset(gt11,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(gt11,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(gt11,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(gt11,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(gt11,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(gt11,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(gt11,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(gt11,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(gt11,-1,0,0) + 
      5.*GFOffset(gt11,0,0,0),0))*pow(dx,-1) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(gt11,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(gt11,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt112 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tj == 
      0,-10.*GFOffset(gt11,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(gt11,0,0,0),0))*pow(dy,-1) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(gt11,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt11,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(gt11,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(gt11,0,3,0) - 
      0.5*GFOffset(gt11,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(gt11,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(gt11,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(gt11,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(gt11,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(gt11,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(gt11,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(gt11,0,1,0) - 
      0.375*GFOffset(gt11,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(gt11,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(gt11,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(gt11,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(gt11,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(gt11,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(gt11,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(gt11,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(gt11,0,-1,0) + 
      5.*GFOffset(gt11,0,0,0),0))*pow(dy,-1) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(gt11,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(gt11,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt113 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tk == 
      0,-10.*GFOffset(gt11,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(gt11,0,0,0),0))*pow(dz,-1) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(gt11,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt11,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(gt11,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(gt11,0,0,3) - 
      0.5*GFOffset(gt11,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(gt11,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(gt11,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(gt11,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(gt11,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(gt11,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(gt11,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(gt11,0,0,1) - 
      0.375*GFOffset(gt11,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(gt11,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(gt11,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(gt11,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(gt11,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(gt11,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(gt11,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(gt11,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(gt11,0,0,-1) + 
      5.*GFOffset(gt11,0,0,0),0))*pow(dz,-1) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(gt11,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(gt11,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt121 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(ti == 
      0,-10.*GFOffset(gt12,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(gt12,0,0,0),0))*pow(dx,-1) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(gt12,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt12,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(gt12,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(gt12,3,0,0) - 
      0.5*GFOffset(gt12,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(gt12,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(gt12,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(gt12,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(gt12,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(gt12,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(gt12,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(gt12,1,0,0) - 
      0.375*GFOffset(gt12,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(gt12,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(gt12,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(gt12,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(gt12,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(gt12,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(gt12,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(gt12,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(gt12,-1,0,0) + 
      5.*GFOffset(gt12,0,0,0),0))*pow(dx,-1) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(gt12,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(gt12,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt122 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tj == 
      0,-10.*GFOffset(gt12,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(gt12,0,0,0),0))*pow(dy,-1) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(gt12,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt12,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(gt12,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(gt12,0,3,0) - 
      0.5*GFOffset(gt12,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(gt12,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(gt12,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(gt12,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(gt12,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(gt12,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(gt12,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(gt12,0,1,0) - 
      0.375*GFOffset(gt12,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(gt12,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(gt12,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(gt12,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(gt12,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(gt12,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(gt12,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(gt12,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(gt12,0,-1,0) + 
      5.*GFOffset(gt12,0,0,0),0))*pow(dy,-1) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(gt12,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(gt12,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt123 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tk == 
      0,-10.*GFOffset(gt12,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(gt12,0,0,0),0))*pow(dz,-1) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(gt12,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt12,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(gt12,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(gt12,0,0,3) - 
      0.5*GFOffset(gt12,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(gt12,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(gt12,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(gt12,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(gt12,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(gt12,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(gt12,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(gt12,0,0,1) - 
      0.375*GFOffset(gt12,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(gt12,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(gt12,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(gt12,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(gt12,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(gt12,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(gt12,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(gt12,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(gt12,0,0,-1) + 
      5.*GFOffset(gt12,0,0,0),0))*pow(dz,-1) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(gt12,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(gt12,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt131 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(ti == 
      0,-10.*GFOffset(gt13,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(gt13,0,0,0),0))*pow(dx,-1) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(gt13,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt13,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(gt13,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(gt13,3,0,0) - 
      0.5*GFOffset(gt13,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(gt13,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(gt13,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(gt13,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(gt13,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(gt13,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(gt13,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(gt13,1,0,0) - 
      0.375*GFOffset(gt13,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(gt13,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(gt13,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(gt13,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(gt13,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(gt13,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(gt13,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(gt13,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(gt13,-1,0,0) + 
      5.*GFOffset(gt13,0,0,0),0))*pow(dx,-1) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(gt13,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(gt13,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt132 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tj == 
      0,-10.*GFOffset(gt13,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(gt13,0,0,0),0))*pow(dy,-1) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(gt13,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt13,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(gt13,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(gt13,0,3,0) - 
      0.5*GFOffset(gt13,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(gt13,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(gt13,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(gt13,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(gt13,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(gt13,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(gt13,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(gt13,0,1,0) - 
      0.375*GFOffset(gt13,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(gt13,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(gt13,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(gt13,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(gt13,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(gt13,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(gt13,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(gt13,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(gt13,0,-1,0) + 
      5.*GFOffset(gt13,0,0,0),0))*pow(dy,-1) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(gt13,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(gt13,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt133 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tk == 
      0,-10.*GFOffset(gt13,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(gt13,0,0,0),0))*pow(dz,-1) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(gt13,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt13,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(gt13,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(gt13,0,0,3) - 
      0.5*GFOffset(gt13,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(gt13,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(gt13,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(gt13,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(gt13,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(gt13,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(gt13,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(gt13,0,0,1) - 
      0.375*GFOffset(gt13,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(gt13,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(gt13,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(gt13,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(gt13,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(gt13,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(gt13,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(gt13,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(gt13,0,0,-1) + 
      5.*GFOffset(gt13,0,0,0),0))*pow(dz,-1) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(gt13,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(gt13,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt221 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(ti == 
      0,-10.*GFOffset(gt22,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(gt22,0,0,0),0))*pow(dx,-1) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(gt22,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt22,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(gt22,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(gt22,3,0,0) - 
      0.5*GFOffset(gt22,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(gt22,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(gt22,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(gt22,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(gt22,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(gt22,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(gt22,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(gt22,1,0,0) - 
      0.375*GFOffset(gt22,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(gt22,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(gt22,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(gt22,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(gt22,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(gt22,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(gt22,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(gt22,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(gt22,-1,0,0) + 
      5.*GFOffset(gt22,0,0,0),0))*pow(dx,-1) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(gt22,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(gt22,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt222 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tj == 
      0,-10.*GFOffset(gt22,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(gt22,0,0,0),0))*pow(dy,-1) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(gt22,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt22,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(gt22,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(gt22,0,3,0) - 
      0.5*GFOffset(gt22,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(gt22,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(gt22,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(gt22,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(gt22,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(gt22,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(gt22,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(gt22,0,1,0) - 
      0.375*GFOffset(gt22,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(gt22,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(gt22,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(gt22,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(gt22,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(gt22,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(gt22,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(gt22,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(gt22,0,-1,0) + 
      5.*GFOffset(gt22,0,0,0),0))*pow(dy,-1) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(gt22,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(gt22,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt223 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tk == 
      0,-10.*GFOffset(gt22,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(gt22,0,0,0),0))*pow(dz,-1) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(gt22,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt22,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(gt22,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(gt22,0,0,3) - 
      0.5*GFOffset(gt22,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(gt22,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(gt22,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(gt22,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(gt22,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(gt22,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(gt22,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(gt22,0,0,1) - 
      0.375*GFOffset(gt22,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(gt22,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(gt22,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(gt22,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(gt22,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(gt22,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(gt22,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(gt22,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(gt22,0,0,-1) + 
      5.*GFOffset(gt22,0,0,0),0))*pow(dz,-1) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(gt22,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(gt22,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt231 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(ti == 
      0,-10.*GFOffset(gt23,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(gt23,0,0,0),0))*pow(dx,-1) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(gt23,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt23,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(gt23,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(gt23,3,0,0) - 
      0.5*GFOffset(gt23,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(gt23,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(gt23,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(gt23,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(gt23,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(gt23,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(gt23,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(gt23,1,0,0) - 
      0.375*GFOffset(gt23,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(gt23,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(gt23,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(gt23,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(gt23,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(gt23,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(gt23,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(gt23,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(gt23,-1,0,0) + 
      5.*GFOffset(gt23,0,0,0),0))*pow(dx,-1) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(gt23,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(gt23,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt232 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tj == 
      0,-10.*GFOffset(gt23,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(gt23,0,0,0),0))*pow(dy,-1) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(gt23,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt23,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(gt23,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(gt23,0,3,0) - 
      0.5*GFOffset(gt23,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(gt23,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(gt23,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(gt23,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(gt23,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(gt23,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(gt23,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(gt23,0,1,0) - 
      0.375*GFOffset(gt23,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(gt23,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(gt23,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(gt23,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(gt23,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(gt23,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(gt23,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(gt23,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(gt23,0,-1,0) + 
      5.*GFOffset(gt23,0,0,0),0))*pow(dy,-1) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(gt23,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(gt23,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt233 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tk == 
      0,-10.*GFOffset(gt23,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(gt23,0,0,0),0))*pow(dz,-1) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(gt23,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt23,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(gt23,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(gt23,0,0,3) - 
      0.5*GFOffset(gt23,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(gt23,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(gt23,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(gt23,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(gt23,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(gt23,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(gt23,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(gt23,0,0,1) - 
      0.375*GFOffset(gt23,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(gt23,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(gt23,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(gt23,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(gt23,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(gt23,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(gt23,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(gt23,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(gt23,0,0,-1) + 
      5.*GFOffset(gt23,0,0,0),0))*pow(dz,-1) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(gt23,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(gt23,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt331 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(ti == 
      0,-10.*GFOffset(gt33,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(gt33,0,0,0),0))*pow(dx,-1) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(gt33,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt33,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(gt33,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(gt33,3,0,0) - 
      0.5*GFOffset(gt33,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(gt33,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(gt33,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(gt33,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(gt33,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(gt33,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(gt33,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(gt33,1,0,0) - 
      0.375*GFOffset(gt33,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(gt33,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(gt33,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(gt33,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(gt33,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(gt33,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(gt33,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(gt33,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(gt33,-1,0,0) + 
      5.*GFOffset(gt33,0,0,0),0))*pow(dx,-1) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(gt33,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(gt33,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt332 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tj == 
      0,-10.*GFOffset(gt33,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(gt33,0,0,0),0))*pow(dy,-1) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(gt33,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt33,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(gt33,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(gt33,0,3,0) - 
      0.5*GFOffset(gt33,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(gt33,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(gt33,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(gt33,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(gt33,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(gt33,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(gt33,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(gt33,0,1,0) - 
      0.375*GFOffset(gt33,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(gt33,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(gt33,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(gt33,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(gt33,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(gt33,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(gt33,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(gt33,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(gt33,0,-1,0) + 
      5.*GFOffset(gt33,0,0,0),0))*pow(dy,-1) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(gt33,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(gt33,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt333 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tk == 
      0,-10.*GFOffset(gt33,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(gt33,0,0,0),0))*pow(dz,-1) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(gt33,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(gt33,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(gt33,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(gt33,0,0,3) - 
      0.5*GFOffset(gt33,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(gt33,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(gt33,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(gt33,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(gt33,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(gt33,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(gt33,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(gt33,0,0,1) - 
      0.375*GFOffset(gt33,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(gt33,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(gt33,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(gt33,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(gt33,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(gt33,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(gt33,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(gt33,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(gt33,0,0,-1) + 
      5.*GFOffset(gt33,0,0,0),0))*pow(dz,-1) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(gt33,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(gt33,0,0,1),0))*pow(dz,-1);
    
    
    if (usejacobian)
    {
      PDgt111L = J11L*LDgt111 + J21L*LDgt112 + J31L*LDgt113;
      
      PDgt112L = J12L*LDgt111 + J22L*LDgt112 + J32L*LDgt113;
      
      PDgt113L = J13L*LDgt111 + J23L*LDgt112 + J33L*LDgt113;
      
      PDgt121L = J11L*LDgt121 + J21L*LDgt122 + J31L*LDgt123;
      
      PDgt122L = J12L*LDgt121 + J22L*LDgt122 + J32L*LDgt123;
      
      PDgt123L = J13L*LDgt121 + J23L*LDgt122 + J33L*LDgt123;
      
      PDgt131L = J11L*LDgt131 + J21L*LDgt132 + J31L*LDgt133;
      
      PDgt132L = J12L*LDgt131 + J22L*LDgt132 + J32L*LDgt133;
      
      PDgt133L = J13L*LDgt131 + J23L*LDgt132 + J33L*LDgt133;
      
      PDgt221L = J11L*LDgt221 + J21L*LDgt222 + J31L*LDgt223;
      
      PDgt222L = J12L*LDgt221 + J22L*LDgt222 + J32L*LDgt223;
      
      PDgt223L = J13L*LDgt221 + J23L*LDgt222 + J33L*LDgt223;
      
      PDgt231L = J11L*LDgt231 + J21L*LDgt232 + J31L*LDgt233;
      
      PDgt232L = J12L*LDgt231 + J22L*LDgt232 + J32L*LDgt233;
      
      PDgt233L = J13L*LDgt231 + J23L*LDgt232 + J33L*LDgt233;
      
      PDgt331L = J11L*LDgt331 + J21L*LDgt332 + J31L*LDgt333;
      
      PDgt332L = J12L*LDgt331 + J22L*LDgt332 + J32L*LDgt333;
      
      PDgt333L = J13L*LDgt331 + J23L*LDgt332 + J33L*LDgt333;
    }
    else
    {
      PDgt111L = LDgt111;
      
      PDgt112L = LDgt112;
      
      PDgt113L = LDgt113;
      
      PDgt121L = LDgt121;
      
      PDgt122L = LDgt122;
      
      PDgt123L = LDgt123;
      
      PDgt131L = LDgt131;
      
      PDgt132L = LDgt132;
      
      PDgt133L = LDgt133;
      
      PDgt221L = LDgt221;
      
      PDgt222L = LDgt222;
      
      PDgt223L = LDgt223;
      
      PDgt231L = LDgt231;
      
      PDgt232L = LDgt232;
      
      PDgt233L = LDgt233;
      
      PDgt331L = LDgt331;
      
      PDgt332L = LDgt332;
      
      PDgt333L = LDgt333;
    }
    
    CCTK_REAL LDualp1 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(ti == 
      0,-10.*GFOffset(alp,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(alp,0,0,0),0))*pow(dx,-1) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(alp,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(alp,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(alp,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(alp,3,0,0) - 
      0.5*GFOffset(alp,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(alp,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(alp,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(alp,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(alp,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(alp,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(alp,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(alp,1,0,0) - 
      0.375*GFOffset(alp,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(alp,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(alp,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(alp,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(alp,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(alp,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(alp,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(alp,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(alp,-1,0,0) + 
      5.*GFOffset(alp,0,0,0),0))*pow(dx,-1) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(alp,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(alp,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDualp2 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tj == 
      0,-10.*GFOffset(alp,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(alp,0,0,0),0))*pow(dy,-1) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(alp,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(alp,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(alp,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(alp,0,3,0) - 
      0.5*GFOffset(alp,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(alp,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(alp,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(alp,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(alp,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(alp,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(alp,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(alp,0,1,0) - 
      0.375*GFOffset(alp,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(alp,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(alp,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(alp,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(alp,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(alp,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(alp,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(alp,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(alp,0,-1,0) + 
      5.*GFOffset(alp,0,0,0),0))*pow(dy,-1) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(alp,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(alp,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDualp3 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tk == 
      0,-10.*GFOffset(alp,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(alp,0,0,0),0))*pow(dz,-1) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(alp,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(alp,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(alp,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(alp,0,0,3) - 
      0.5*GFOffset(alp,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(alp,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(alp,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(alp,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(alp,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(alp,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(alp,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(alp,0,0,1) - 
      0.375*GFOffset(alp,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(alp,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(alp,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(alp,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(alp,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(alp,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(alp,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(alp,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(alp,0,0,-1) + 
      5.*GFOffset(alp,0,0,0),0))*pow(dz,-1) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(alp,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(alp,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDuadmbeta11 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(ti == 
      0,-10.*GFOffset(betax,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(betax,0,0,0),0))*pow(dx,-1) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(betax,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(betax,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(betax,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(betax,3,0,0) - 
      0.5*GFOffset(betax,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(betax,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(betax,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(betax,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(betax,3,0,0),0) + IfThen(ti 
      == 2,0.375*GFOffset(betax,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(betax,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(betax,1,0,0) - 
      0.375*GFOffset(betax,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(betax,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(betax,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(betax,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(betax,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(betax,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(betax,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(betax,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(betax,-1,0,0) + 
      5.*GFOffset(betax,0,0,0),0))*pow(dx,-1) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(betax,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(betax,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuadmbeta21 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(ti == 
      0,-10.*GFOffset(betay,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(betay,0,0,0),0))*pow(dx,-1) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(betay,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(betay,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(betay,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(betay,3,0,0) - 
      0.5*GFOffset(betay,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(betay,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(betay,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(betay,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(betay,3,0,0),0) + IfThen(ti 
      == 2,0.375*GFOffset(betay,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(betay,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(betay,1,0,0) - 
      0.375*GFOffset(betay,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(betay,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(betay,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(betay,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(betay,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(betay,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(betay,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(betay,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(betay,-1,0,0) + 
      5.*GFOffset(betay,0,0,0),0))*pow(dx,-1) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(betay,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(betay,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuadmbeta31 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(ti == 
      0,-10.*GFOffset(betaz,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(betaz,0,0,0),0))*pow(dx,-1) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(betaz,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(betaz,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(betaz,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(betaz,3,0,0) - 
      0.5*GFOffset(betaz,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(betaz,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(betaz,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(betaz,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(betaz,3,0,0),0) + IfThen(ti 
      == 2,0.375*GFOffset(betaz,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(betaz,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(betaz,1,0,0) - 
      0.375*GFOffset(betaz,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(betaz,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(betaz,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(betaz,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(betaz,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(betaz,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(betaz,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(betaz,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(betaz,-1,0,0) + 
      5.*GFOffset(betaz,0,0,0),0))*pow(dx,-1) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(betaz,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(betaz,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuadmbeta12 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tj == 
      0,-10.*GFOffset(betax,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(betax,0,0,0),0))*pow(dy,-1) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(betax,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(betax,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(betax,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(betax,0,3,0) - 
      0.5*GFOffset(betax,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(betax,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(betax,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(betax,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(betax,0,3,0),0) + IfThen(tj 
      == 2,0.375*GFOffset(betax,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(betax,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(betax,0,1,0) - 
      0.375*GFOffset(betax,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(betax,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(betax,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(betax,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(betax,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(betax,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(betax,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(betax,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(betax,0,-1,0) + 
      5.*GFOffset(betax,0,0,0),0))*pow(dy,-1) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(betax,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(betax,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuadmbeta22 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tj == 
      0,-10.*GFOffset(betay,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(betay,0,0,0),0))*pow(dy,-1) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(betay,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(betay,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(betay,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(betay,0,3,0) - 
      0.5*GFOffset(betay,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(betay,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(betay,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(betay,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(betay,0,3,0),0) + IfThen(tj 
      == 2,0.375*GFOffset(betay,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(betay,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(betay,0,1,0) - 
      0.375*GFOffset(betay,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(betay,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(betay,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(betay,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(betay,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(betay,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(betay,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(betay,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(betay,0,-1,0) + 
      5.*GFOffset(betay,0,0,0),0))*pow(dy,-1) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(betay,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(betay,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuadmbeta32 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tj == 
      0,-10.*GFOffset(betaz,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(betaz,0,0,0),0))*pow(dy,-1) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(betaz,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(betaz,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(betaz,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(betaz,0,3,0) - 
      0.5*GFOffset(betaz,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(betaz,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(betaz,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(betaz,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(betaz,0,3,0),0) + IfThen(tj 
      == 2,0.375*GFOffset(betaz,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(betaz,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(betaz,0,1,0) - 
      0.375*GFOffset(betaz,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(betaz,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(betaz,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(betaz,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(betaz,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(betaz,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(betaz,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(betaz,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(betaz,0,-1,0) + 
      5.*GFOffset(betaz,0,0,0),0))*pow(dy,-1) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(betaz,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(betaz,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuadmbeta13 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tk == 
      0,-10.*GFOffset(betax,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(betax,0,0,0),0))*pow(dz,-1) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(betax,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(betax,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(betax,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(betax,0,0,3) - 
      0.5*GFOffset(betax,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(betax,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(betax,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(betax,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(betax,0,0,3),0) + IfThen(tk 
      == 2,0.375*GFOffset(betax,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(betax,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(betax,0,0,1) - 
      0.375*GFOffset(betax,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(betax,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(betax,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(betax,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(betax,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(betax,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(betax,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(betax,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(betax,0,0,-1) + 
      5.*GFOffset(betax,0,0,0),0))*pow(dz,-1) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(betax,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(betax,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDuadmbeta23 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tk == 
      0,-10.*GFOffset(betay,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(betay,0,0,0),0))*pow(dz,-1) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(betay,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(betay,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(betay,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(betay,0,0,3) - 
      0.5*GFOffset(betay,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(betay,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(betay,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(betay,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(betay,0,0,3),0) + IfThen(tk 
      == 2,0.375*GFOffset(betay,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(betay,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(betay,0,0,1) - 
      0.375*GFOffset(betay,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(betay,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(betay,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(betay,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(betay,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(betay,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(betay,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(betay,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(betay,0,0,-1) + 
      5.*GFOffset(betay,0,0,0),0))*pow(dz,-1) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(betay,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(betay,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDuadmbeta33 CCTK_ATTRIBUTE_UNUSED = -0.2*(IfThen(tk == 
      0,-10.*GFOffset(betaz,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(betaz,0,0,0),0))*pow(dz,-1) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(betaz,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(betaz,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(betaz,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(betaz,0,0,3) - 
      0.5*GFOffset(betaz,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(betaz,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(betaz,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(betaz,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(betaz,0,0,3),0) + IfThen(tk 
      == 2,0.375*GFOffset(betaz,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(betaz,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(betaz,0,0,1) - 
      0.375*GFOffset(betaz,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(betaz,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(betaz,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(betaz,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(betaz,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(betaz,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(betaz,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(betaz,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(betaz,0,0,-1) + 
      5.*GFOffset(betaz,0,0,0),0))*pow(dz,-1) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(betaz,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(betaz,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDuadmbeta11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuadmbeta12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuadmbeta13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuadmbeta21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuadmbeta22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuadmbeta23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuadmbeta31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuadmbeta32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuadmbeta33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualp1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualp2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualp3 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDualp1 = J11L*LDualp1 + J21L*LDualp2 + J31L*LDualp3;
      
      PDualp2 = J12L*LDualp1 + J22L*LDualp2 + J32L*LDualp3;
      
      PDualp3 = J13L*LDualp1 + J23L*LDualp2 + J33L*LDualp3;
      
      PDuadmbeta11 = J11L*LDuadmbeta11 + J21L*LDuadmbeta12 + 
        J31L*LDuadmbeta13;
      
      PDuadmbeta21 = J11L*LDuadmbeta21 + J21L*LDuadmbeta22 + 
        J31L*LDuadmbeta23;
      
      PDuadmbeta31 = J11L*LDuadmbeta31 + J21L*LDuadmbeta32 + 
        J31L*LDuadmbeta33;
      
      PDuadmbeta12 = J12L*LDuadmbeta11 + J22L*LDuadmbeta12 + 
        J32L*LDuadmbeta13;
      
      PDuadmbeta22 = J12L*LDuadmbeta21 + J22L*LDuadmbeta22 + 
        J32L*LDuadmbeta23;
      
      PDuadmbeta32 = J12L*LDuadmbeta31 + J22L*LDuadmbeta32 + 
        J32L*LDuadmbeta33;
      
      PDuadmbeta13 = J13L*LDuadmbeta11 + J23L*LDuadmbeta12 + 
        J33L*LDuadmbeta13;
      
      PDuadmbeta23 = J13L*LDuadmbeta21 + J23L*LDuadmbeta22 + 
        J33L*LDuadmbeta23;
      
      PDuadmbeta33 = J13L*LDuadmbeta31 + J23L*LDuadmbeta32 + 
        J33L*LDuadmbeta33;
    }
    else
    {
      PDualp1 = LDualp1;
      
      PDualp2 = LDualp2;
      
      PDualp3 = LDualp3;
      
      PDuadmbeta11 = LDuadmbeta11;
      
      PDuadmbeta21 = LDuadmbeta21;
      
      PDuadmbeta31 = LDuadmbeta31;
      
      PDuadmbeta12 = LDuadmbeta12;
      
      PDuadmbeta22 = LDuadmbeta22;
      
      PDuadmbeta32 = LDuadmbeta32;
      
      PDuadmbeta13 = LDuadmbeta13;
      
      PDuadmbeta23 = LDuadmbeta23;
      
      PDuadmbeta33 = LDuadmbeta33;
    }
    
    CCTK_REAL g11 CCTK_ATTRIBUTE_UNUSED = gxxL;
    
    CCTK_REAL g12 CCTK_ATTRIBUTE_UNUSED = gxyL;
    
    CCTK_REAL g13 CCTK_ATTRIBUTE_UNUSED = gxzL;
    
    CCTK_REAL g22 CCTK_ATTRIBUTE_UNUSED = gyyL;
    
    CCTK_REAL g23 CCTK_ATTRIBUTE_UNUSED = gyzL;
    
    CCTK_REAL g33 CCTK_ATTRIBUTE_UNUSED = gzzL;
    
    CCTK_REAL detg CCTK_ATTRIBUTE_UNUSED = 2*g12*g13*g23 + g11*g22*g33 - 
      g33*pow(g12,2) - g22*pow(g13,2) - g11*pow(g23,2);
    
    CCTK_REAL gu11 CCTK_ATTRIBUTE_UNUSED = pow(detg,-1)*(g22*g33 - 
      pow(g23,2));
    
    CCTK_REAL gu12 CCTK_ATTRIBUTE_UNUSED = (g13*g23 - 
      g12*g33)*pow(detg,-1);
    
    CCTK_REAL gu13 CCTK_ATTRIBUTE_UNUSED = (-(g13*g22) + 
      g12*g23)*pow(detg,-1);
    
    CCTK_REAL gu22 CCTK_ATTRIBUTE_UNUSED = pow(detg,-1)*(g11*g33 - 
      pow(g13,2));
    
    CCTK_REAL gu23 CCTK_ATTRIBUTE_UNUSED = (g12*g13 - 
      g11*g23)*pow(detg,-1);
    
    CCTK_REAL gu33 CCTK_ATTRIBUTE_UNUSED = pow(detg,-1)*(g11*g22 - 
      pow(g12,2));
    
    CCTK_REAL em4phi CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,pow(phiWL,2),exp(-4*phiWL));
    
    CCTK_REAL gtu11 CCTK_ATTRIBUTE_UNUSED = gu11*pow(em4phi,-1);
    
    CCTK_REAL gtu12 CCTK_ATTRIBUTE_UNUSED = gu12*pow(em4phi,-1);
    
    CCTK_REAL gtu13 CCTK_ATTRIBUTE_UNUSED = gu13*pow(em4phi,-1);
    
    CCTK_REAL gtu22 CCTK_ATTRIBUTE_UNUSED = gu22*pow(em4phi,-1);
    
    CCTK_REAL gtu23 CCTK_ATTRIBUTE_UNUSED = gu23*pow(em4phi,-1);
    
    CCTK_REAL gtu33 CCTK_ATTRIBUTE_UNUSED = gu33*pow(em4phi,-1);
    
    CCTK_REAL Gt111 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt111L*gtu11 + 
      (-PDgt112L + 2*PDgt121L)*gtu12 + (-PDgt113L + 2*PDgt131L)*gtu13);
    
    CCTK_REAL Gt211 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt111L*gtu12 + 
      (-PDgt112L + 2*PDgt121L)*gtu22 + (-PDgt113L + 2*PDgt131L)*gtu23);
    
    CCTK_REAL Gt311 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt111L*gtu13 + 
      (-PDgt112L + 2*PDgt121L)*gtu23 + (-PDgt113L + 2*PDgt131L)*gtu33);
    
    CCTK_REAL Gt112 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt112L*gtu11 + 
      PDgt221L*gtu12 + (-PDgt123L + PDgt132L + PDgt231L)*gtu13);
    
    CCTK_REAL Gt212 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt112L*gtu12 + 
      PDgt221L*gtu22 + (-PDgt123L + PDgt132L + PDgt231L)*gtu23);
    
    CCTK_REAL Gt312 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt112L*gtu13 + 
      PDgt221L*gtu23 + (-PDgt123L + PDgt132L + PDgt231L)*gtu33);
    
    CCTK_REAL Gt113 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt113L*gtu11 + 
      (PDgt123L - PDgt132L + PDgt231L)*gtu12 + PDgt331L*gtu13);
    
    CCTK_REAL Gt213 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt113L*gtu12 + 
      (PDgt123L - PDgt132L + PDgt231L)*gtu22 + PDgt331L*gtu23);
    
    CCTK_REAL Gt313 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt113L*gtu13 + 
      (PDgt123L - PDgt132L + PDgt231L)*gtu23 + PDgt331L*gtu33);
    
    CCTK_REAL Gt122 CCTK_ATTRIBUTE_UNUSED = 0.5*((2*PDgt122L - 
      PDgt221L)*gtu11 + PDgt222L*gtu12 + (-PDgt223L + 2*PDgt232L)*gtu13);
    
    CCTK_REAL Gt222 CCTK_ATTRIBUTE_UNUSED = 0.5*((2*PDgt122L - 
      PDgt221L)*gtu12 + PDgt222L*gtu22 + (-PDgt223L + 2*PDgt232L)*gtu23);
    
    CCTK_REAL Gt322 CCTK_ATTRIBUTE_UNUSED = 0.5*((2*PDgt122L - 
      PDgt221L)*gtu13 + PDgt222L*gtu23 + (-PDgt223L + 2*PDgt232L)*gtu33);
    
    CCTK_REAL Gt123 CCTK_ATTRIBUTE_UNUSED = 0.5*((PDgt123L + PDgt132L - 
      PDgt231L)*gtu11 + PDgt223L*gtu12 + PDgt332L*gtu13);
    
    CCTK_REAL Gt223 CCTK_ATTRIBUTE_UNUSED = 0.5*((PDgt123L + PDgt132L - 
      PDgt231L)*gtu12 + PDgt223L*gtu22 + PDgt332L*gtu23);
    
    CCTK_REAL Gt323 CCTK_ATTRIBUTE_UNUSED = 0.5*((PDgt123L + PDgt132L - 
      PDgt231L)*gtu13 + PDgt223L*gtu23 + PDgt332L*gtu33);
    
    CCTK_REAL Gt133 CCTK_ATTRIBUTE_UNUSED = 0.5*((2*PDgt133L - 
      PDgt331L)*gtu11 + (2*PDgt233L - PDgt332L)*gtu12 + PDgt333L*gtu13);
    
    CCTK_REAL Gt233 CCTK_ATTRIBUTE_UNUSED = 0.5*((2*PDgt133L - 
      PDgt331L)*gtu12 + (2*PDgt233L - PDgt332L)*gtu22 + PDgt333L*gtu23);
    
    CCTK_REAL Gt333 CCTK_ATTRIBUTE_UNUSED = 0.5*((2*PDgt133L - 
      PDgt331L)*gtu13 + (2*PDgt233L - PDgt332L)*gtu23 + PDgt333L*gtu33);
    
    CCTK_REAL Xt1L CCTK_ATTRIBUTE_UNUSED = Gt111*gtu11 + 2*Gt112*gtu12 + 
      2*Gt113*gtu13 + Gt122*gtu22 + 2*Gt123*gtu23 + Gt133*gtu33;
    
    CCTK_REAL Xt2L CCTK_ATTRIBUTE_UNUSED = Gt211*gtu11 + 2*Gt212*gtu12 + 
      2*Gt213*gtu13 + Gt222*gtu22 + 2*Gt223*gtu23 + Gt233*gtu33;
    
    CCTK_REAL Xt3L CCTK_ATTRIBUTE_UNUSED = Gt311*gtu11 + 2*Gt312*gtu12 + 
      2*Gt313*gtu13 + Gt322*gtu22 + 2*Gt323*gtu23 + Gt333*gtu33;
    
    CCTK_REAL AL CCTK_ATTRIBUTE_UNUSED = IfThen(evolveA != 0,-((dtalpL - 
      IfThen(advectLapse != 0,betaxL*PDualp1 + betayL*PDualp2 + 
      betazL*PDualp3,0))*pow(alpL,-harmonicN)*pow(harmonicF,-1)),0);
    
    CCTK_REAL shiftGammaCoeffValue CCTK_ATTRIBUTE_UNUSED = 
      IfThen(useSpatialShiftGammaCoeff != 0,shiftGammaCoeff*fmin(1,exp(1 - 
      rL*pow(spatialShiftGammaCoeffRadius,-1))),shiftGammaCoeff);
    
    CCTK_REAL B1L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL B2L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL B3L CCTK_ATTRIBUTE_UNUSED;
    
    if (evolveB != 0)
    {
      B1L = IfThen(shiftGammaCoeffValue != 0.,(dtbetaxL - IfThen(advectShift 
        != 0,betaxL*PDuadmbeta11 + betayL*PDuadmbeta12 + 
        betazL*PDuadmbeta13,0))*pow(alpL,-shiftAlphaPower)*pow(shiftGammaCoeffValue,-1),0);
      
      B2L = IfThen(shiftGammaCoeffValue != 0.,(dtbetayL - IfThen(advectShift 
        != 0,betaxL*PDuadmbeta21 + betayL*PDuadmbeta22 + 
        betazL*PDuadmbeta23,0))*pow(alpL,-shiftAlphaPower)*pow(shiftGammaCoeffValue,-1),0);
      
      B3L = IfThen(shiftGammaCoeffValue != 0.,(dtbetazL - IfThen(advectShift 
        != 0,betaxL*PDuadmbeta31 + betayL*PDuadmbeta32 + 
        betazL*PDuadmbeta33,0))*pow(alpL,-shiftAlphaPower)*pow(shiftGammaCoeffValue,-1),0);
    }
    else
    {
      B1L = 0;
      
      B2L = 0;
      
      B3L = 0;
    }
    /* Copy local copies back to grid functions */
    A[index] = AL;
    B1[index] = B1L;
    B2[index] = B2L;
    B3[index] = B3L;
    PDgt111[index] = PDgt111L;
    PDgt112[index] = PDgt112L;
    PDgt113[index] = PDgt113L;
    PDgt121[index] = PDgt121L;
    PDgt122[index] = PDgt122L;
    PDgt123[index] = PDgt123L;
    PDgt131[index] = PDgt131L;
    PDgt132[index] = PDgt132L;
    PDgt133[index] = PDgt133L;
    PDgt221[index] = PDgt221L;
    PDgt222[index] = PDgt222L;
    PDgt223[index] = PDgt223L;
    PDgt231[index] = PDgt231L;
    PDgt232[index] = PDgt232L;
    PDgt233[index] = PDgt233L;
    PDgt331[index] = PDgt331L;
    PDgt332[index] = PDgt332L;
    PDgt333[index] = PDgt333L;
    Xt1[index] = Xt1L;
    Xt2[index] = Xt2L;
    Xt3[index] = Xt3L;
  }
  CCTK_ENDLOOP3(ML_BSSN_DG4_InitialADMBase2Interior);
}
extern "C" void ML_BSSN_DG4_InitialADMBase2Interior(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_DG4_InitialADMBase2Interior_Body");
  }
  if (cctk_iteration % ML_BSSN_DG4_InitialADMBase2Interior_calc_every != ML_BSSN_DG4_InitialADMBase2Interior_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ADMBase::dtlapse",
    "ADMBase::dtshift",
    "ADMBase::lapse",
    "ADMBase::metric",
    "ADMBase::shift",
    "grid::coordinates",
    "ML_BSSN_DG4::ML_confac",
    "ML_BSSN_DG4::ML_dmetric",
    "ML_BSSN_DG4::ML_dtlapse",
    "ML_BSSN_DG4::ML_dtshift",
    "ML_BSSN_DG4::ML_Gamma",
    "ML_BSSN_DG4::ML_metric"};
  AssertGroupStorage(cctkGH, "ML_BSSN_DG4_InitialADMBase2Interior", 12, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_BSSN_DG4_InitialADMBase2Interior_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_DG4_InitialADMBase2Interior_Body");
  }
}

} // namespace ML_BSSN_DG4
