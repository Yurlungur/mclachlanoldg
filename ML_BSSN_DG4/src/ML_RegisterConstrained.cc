#include <cctk.h>
#include <cctk_Arguments.h>

#include <cassert>
using namespace std;

namespace {
  void register_constrained(const char* groupname)
  {
    int gi = CCTK_GroupIndex(groupname);
    int ierr = MoLRegisterConstrainedGroup(gi);
    assert(!ierr);
  }
}

extern "C"
void ML_BSSN_DG4_RegisterConstrained(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  
  register_constrained("ADMBase::metric");
  register_constrained("ADMBase::curv");
  register_constrained("ADMBase::lapse");
  register_constrained("ADMBase::shift");
  register_constrained("ADMBase::dtlapse");
  register_constrained("ADMBase::dtshift");
}
