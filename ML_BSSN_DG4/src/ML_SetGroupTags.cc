#include <cctk.h>
#include <cctk_Parameters.h>
#include <util_Table.h>

#include <cassert>

namespace {
  void set_group_tags(bool checkpoint,
                      bool persistent,
                      bool prolongate,
                      const char* groupname)
  {
    assert(groupname);
    
    int gi = CCTK_GroupIndex(groupname);
    assert(gi >= 0);
    
    int table = CCTK_GroupTagsTableI(gi);
    assert(table >= 0);
    
    if (!checkpoint) {
      int ierr = Util_TableSetString(table, "no", "Checkpoint");
      assert(!ierr);
    }
    
    if (!persistent) {
      int ierr = Util_TableSetString(table, "no", "Persistent");
      assert(!ierr);
    }
  }
}

extern "C"
int ML_BSSN_DG4_SetGroupTags(void)
{
  DECLARE_CCTK_PARAMETERS;
  
  // global describes whether these variables are valid on the whole
  // grid hierarchy, or only on the current level
  
  bool global = timelevels > 1;
  set_group_tags(global, global, global, "ADMBase::metric");
  set_group_tags(global, global, global, "ADMBase::curv");
  set_group_tags(global, global, global, "ADMBase::lapse");
  set_group_tags(global, global, global, "ADMBase::shift");
  set_group_tags(global, global, global, "ADMBase::dtlapse");
  set_group_tags(global, global, global, "ADMBase::dtshift");
  
  bool other_global = other_timelevels > 1;
  set_group_tags(other_global, other_global, other_global, "ML_BSSN_DG4::ML_cons_detg");
  set_group_tags(other_global, other_global, other_global, "ML_BSSN_DG4::ML_cons_Gamma");
  set_group_tags(other_global, other_global, other_global, "ML_BSSN_DG4::ML_cons_traceA");
  set_group_tags(other_global, other_global, other_global, "ML_BSSN_DG4::ML_Ham");
  set_group_tags(other_global, other_global, other_global, "ML_BSSN_DG4::ML_mom");
  
  // RHS variables do not have valid boundaries
  bool rhs_global = rhs_timelevels > 1;
  set_group_tags(rhs_global, rhs_global, true, "ML_BSSN_DG4::ML_confacrhs");
  set_group_tags(rhs_global, rhs_global, true, "ML_BSSN_DG4::ML_metricrhs");
  set_group_tags(rhs_global, rhs_global, true, "ML_BSSN_DG4::ML_Gammarhs");
  set_group_tags(rhs_global, rhs_global, true, "ML_BSSN_DG4::ML_trace_curvrhs");
  set_group_tags(rhs_global, rhs_global, true, "ML_BSSN_DG4::ML_curvrhs");
  bool have_Theta = CCTK_VarIndex("ML_BSSN_DG4::ML_Thetarhs") >= 0;
  if (have_Theta) {
    set_group_tags(rhs_global, rhs_global, true, "ML_BSSN_DG4::ML_Thetarhs");
  }
  set_group_tags(rhs_global, rhs_global, true, "ML_BSSN_DG4::ML_lapserhs");
  set_group_tags(rhs_global, rhs_global, true, "ML_BSSN_DG4::ML_dtlapserhs");
  set_group_tags(rhs_global, rhs_global, true, "ML_BSSN_DG4::ML_shiftrhs");
  set_group_tags(rhs_global, rhs_global, true, "ML_BSSN_DG4::ML_dtshiftrhs");
  
  return 0;
}
