#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cassert>

extern "C"
void ML_WaveToy_FD4_RHSSelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int ierr;
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_WaveToy_FD4::WT_urhs", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_WaveToy_FD4::WT_rhorhs", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_WaveToy_FD4::WT_vrhs", "none");
  assert(!ierr);
}
