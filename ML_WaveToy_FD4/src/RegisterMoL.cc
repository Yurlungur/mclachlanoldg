/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void ML_WaveToy_FD4_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_WaveToy_FD4::rho"),  CCTK_VarIndex("ML_WaveToy_FD4::rhorhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_WaveToy_FD4::u"),  CCTK_VarIndex("ML_WaveToy_FD4::urhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_WaveToy_FD4::v1"),  CCTK_VarIndex("ML_WaveToy_FD4::v1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_WaveToy_FD4::v2"),  CCTK_VarIndex("ML_WaveToy_FD4::v2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_WaveToy_FD4::v3"),  CCTK_VarIndex("ML_WaveToy_FD4::v3rhs"));
  /* Register all the evolved Array functions with MoL */
  return;
}
