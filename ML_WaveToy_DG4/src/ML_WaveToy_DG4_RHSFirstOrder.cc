/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_WaveToy_DG4 {

extern "C" void ML_WaveToy_DG4_RHSFirstOrder_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_WaveToy_DG4_RHSFirstOrder_calc_every != ML_WaveToy_DG4_RHSFirstOrder_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG4::WT_rhorhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG4::WT_rhorhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG4::WT_urhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG4::WT_urhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG4::WT_vrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG4::WT_vrhs.");
  return;
}

static void ML_WaveToy_DG4_RHSFirstOrder_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_WaveToy_DG4_RHSFirstOrder,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL myDetJL CCTK_ATTRIBUTE_UNUSED = myDetJ[index];
    CCTK_REAL rhoL CCTK_ATTRIBUTE_UNUSED = rho[index];
    CCTK_REAL uL CCTK_ATTRIBUTE_UNUSED = u[index];
    CCTK_REAL v1L CCTK_ATTRIBUTE_UNUSED = v1[index];
    CCTK_REAL v2L CCTK_ATTRIBUTE_UNUSED = v2[index];
    CCTK_REAL v3L CCTK_ATTRIBUTE_UNUSED = v3[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDrho1 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(ti == 
      0,-10.*GFOffset(rho,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(rho,0,0,0),0)) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(rho,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(rho,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(rho,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(rho,3,0,0) - 
      0.5*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(rho,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(rho,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(rho,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(rho,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(rho,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(rho,1,0,0) - 
      0.375*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(rho,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(rho,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(rho,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(rho,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(rho,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(rho,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(rho,-1,0,0) + 
      5.*GFOffset(rho,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(rho,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(rho,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDrho2 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(tj == 
      0,-10.*GFOffset(rho,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(rho,0,0,0),0)) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(rho,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(rho,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(rho,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(rho,0,3,0) - 
      0.5*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(rho,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(rho,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(rho,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(rho,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(rho,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(rho,0,1,0) - 
      0.375*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(rho,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(rho,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(rho,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(rho,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(rho,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(rho,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(rho,0,-1,0) + 
      5.*GFOffset(rho,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(rho,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(rho,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDrho3 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(tk == 
      0,-10.*GFOffset(rho,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(rho,0,0,0),0)) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(rho,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(rho,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(rho,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(rho,0,0,3) - 
      0.5*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(rho,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(rho,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(rho,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(rho,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(rho,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(rho,0,0,1) - 
      0.375*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(rho,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(rho,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(rho,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(rho,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(rho,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(rho,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(rho,0,0,-1) + 
      5.*GFOffset(rho,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(rho,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(rho,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL LDv11 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(ti == 
      0,-10.*GFOffset(v1,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(v1,0,0,0),0)) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(v1,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(v1,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(v1,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(v1,3,0,0) - 
      0.5*GFOffset(v1,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(v1,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(v1,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(v1,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(v1,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(v1,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(v1,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(v1,1,0,0) - 
      0.375*GFOffset(v1,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(v1,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(v1,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(v1,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(v1,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(v1,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(v1,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(v1,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(v1,-1,0,0) + 
      5.*GFOffset(v1,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(v1,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(v1,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDv12 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(tj == 
      0,-10.*GFOffset(v1,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(v1,0,0,0),0)) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(v1,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(v1,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(v1,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(v1,0,3,0) - 
      0.5*GFOffset(v1,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(v1,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(v1,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(v1,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(v1,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(v1,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(v1,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(v1,0,1,0) - 
      0.375*GFOffset(v1,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(v1,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(v1,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(v1,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(v1,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(v1,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(v1,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(v1,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(v1,0,-1,0) + 
      5.*GFOffset(v1,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(v1,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(v1,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDv13 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(tk == 
      0,-10.*GFOffset(v1,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(v1,0,0,0),0)) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(v1,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(v1,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(v1,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(v1,0,0,3) - 
      0.5*GFOffset(v1,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(v1,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(v1,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(v1,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(v1,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(v1,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(v1,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(v1,0,0,1) - 
      0.375*GFOffset(v1,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(v1,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(v1,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(v1,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(v1,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(v1,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(v1,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(v1,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(v1,0,0,-1) + 
      5.*GFOffset(v1,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(v1,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(v1,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL LDv21 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(ti == 
      0,-10.*GFOffset(v2,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(v2,0,0,0),0)) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(v2,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(v2,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(v2,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(v2,3,0,0) - 
      0.5*GFOffset(v2,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(v2,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(v2,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(v2,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(v2,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(v2,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(v2,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(v2,1,0,0) - 
      0.375*GFOffset(v2,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(v2,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(v2,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(v2,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(v2,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(v2,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(v2,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(v2,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(v2,-1,0,0) + 
      5.*GFOffset(v2,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(v2,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(v2,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDv22 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(tj == 
      0,-10.*GFOffset(v2,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(v2,0,0,0),0)) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(v2,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(v2,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(v2,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(v2,0,3,0) - 
      0.5*GFOffset(v2,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(v2,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(v2,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(v2,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(v2,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(v2,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(v2,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(v2,0,1,0) - 
      0.375*GFOffset(v2,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(v2,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(v2,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(v2,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(v2,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(v2,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(v2,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(v2,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(v2,0,-1,0) + 
      5.*GFOffset(v2,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(v2,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(v2,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDv23 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(tk == 
      0,-10.*GFOffset(v2,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(v2,0,0,0),0)) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(v2,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(v2,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(v2,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(v2,0,0,3) - 
      0.5*GFOffset(v2,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(v2,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(v2,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(v2,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(v2,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(v2,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(v2,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(v2,0,0,1) - 
      0.375*GFOffset(v2,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(v2,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(v2,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(v2,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(v2,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(v2,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(v2,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(v2,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(v2,0,0,-1) + 
      5.*GFOffset(v2,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(v2,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(v2,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL LDv31 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(ti == 
      0,-10.*GFOffset(v3,0,0,0),0) + IfThen(ti == 
      4,10.*GFOffset(v3,0,0,0),0)) + 0.4*(IfThen(ti == 
      0,-5.*GFOffset(v3,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(v3,1,0,0) - 
      2.66666666666666666666666666667*GFOffset(v3,2,0,0) + 
      1.41016417794242666282363913699*GFOffset(v3,3,0,0) - 
      0.5*GFOffset(v3,4,0,0),0) + IfThen(ti == 
      1,-1.24099025303098285784871934219*GFOffset(v3,-1,0,0) + 
      1.74574312188793905012877988332*GFOffset(v3,1,0,0) - 
      0.76376261582597333443134119895*GFOffset(v3,2,0,0) + 
      0.259009746969017142151280657815*GFOffset(v3,3,0,0),0) + IfThen(ti == 
      2,0.375*GFOffset(v3,-2,0,0) - 
      1.33658457769545333525484709817*GFOffset(v3,-1,0,0) + 
      1.33658457769545333525484709817*GFOffset(v3,1,0,0) - 
      0.375*GFOffset(v3,2,0,0),0) + IfThen(ti == 
      3,-0.259009746969017142151280657815*GFOffset(v3,-3,0,0) + 
      0.76376261582597333443134119895*GFOffset(v3,-2,0,0) - 
      1.74574312188793905012877988332*GFOffset(v3,-1,0,0) + 
      1.24099025303098285784871934219*GFOffset(v3,1,0,0),0) + IfThen(ti == 
      4,0.5*GFOffset(v3,-4,0,0) - 
      1.41016417794242666282363913699*GFOffset(v3,-3,0,0) + 
      2.66666666666666666666666666667*GFOffset(v3,-2,0,0) - 
      6.7565024887242400038430275297*GFOffset(v3,-1,0,0) + 
      5.*GFOffset(v3,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(ti == 
      0,10.*GFOffset(v3,-1,0,0),0) + IfThen(ti == 
      4,-10.*GFOffset(v3,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDv32 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(tj == 
      0,-10.*GFOffset(v3,0,0,0),0) + IfThen(tj == 
      4,10.*GFOffset(v3,0,0,0),0)) + 0.4*(IfThen(tj == 
      0,-5.*GFOffset(v3,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(v3,0,1,0) - 
      2.66666666666666666666666666667*GFOffset(v3,0,2,0) + 
      1.41016417794242666282363913699*GFOffset(v3,0,3,0) - 
      0.5*GFOffset(v3,0,4,0),0) + IfThen(tj == 
      1,-1.24099025303098285784871934219*GFOffset(v3,0,-1,0) + 
      1.74574312188793905012877988332*GFOffset(v3,0,1,0) - 
      0.76376261582597333443134119895*GFOffset(v3,0,2,0) + 
      0.259009746969017142151280657815*GFOffset(v3,0,3,0),0) + IfThen(tj == 
      2,0.375*GFOffset(v3,0,-2,0) - 
      1.33658457769545333525484709817*GFOffset(v3,0,-1,0) + 
      1.33658457769545333525484709817*GFOffset(v3,0,1,0) - 
      0.375*GFOffset(v3,0,2,0),0) + IfThen(tj == 
      3,-0.259009746969017142151280657815*GFOffset(v3,0,-3,0) + 
      0.76376261582597333443134119895*GFOffset(v3,0,-2,0) - 
      1.74574312188793905012877988332*GFOffset(v3,0,-1,0) + 
      1.24099025303098285784871934219*GFOffset(v3,0,1,0),0) + IfThen(tj == 
      4,0.5*GFOffset(v3,0,-4,0) - 
      1.41016417794242666282363913699*GFOffset(v3,0,-3,0) + 
      2.66666666666666666666666666667*GFOffset(v3,0,-2,0) - 
      6.7565024887242400038430275297*GFOffset(v3,0,-1,0) + 
      5.*GFOffset(v3,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(tj == 
      0,10.*GFOffset(v3,0,-1,0),0) + IfThen(tj == 
      4,-10.*GFOffset(v3,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDv33 CCTK_ATTRIBUTE_UNUSED = (-0.2*(IfThen(tk == 
      0,-10.*GFOffset(v3,0,0,0),0) + IfThen(tk == 
      4,10.*GFOffset(v3,0,0,0),0)) + 0.4*(IfThen(tk == 
      0,-5.*GFOffset(v3,0,0,0) + 
      6.7565024887242400038430275297*GFOffset(v3,0,0,1) - 
      2.66666666666666666666666666667*GFOffset(v3,0,0,2) + 
      1.41016417794242666282363913699*GFOffset(v3,0,0,3) - 
      0.5*GFOffset(v3,0,0,4),0) + IfThen(tk == 
      1,-1.24099025303098285784871934219*GFOffset(v3,0,0,-1) + 
      1.74574312188793905012877988332*GFOffset(v3,0,0,1) - 
      0.76376261582597333443134119895*GFOffset(v3,0,0,2) + 
      0.259009746969017142151280657815*GFOffset(v3,0,0,3),0) + IfThen(tk == 
      2,0.375*GFOffset(v3,0,0,-2) - 
      1.33658457769545333525484709817*GFOffset(v3,0,0,-1) + 
      1.33658457769545333525484709817*GFOffset(v3,0,0,1) - 
      0.375*GFOffset(v3,0,0,2),0) + IfThen(tk == 
      3,-0.259009746969017142151280657815*GFOffset(v3,0,0,-3) + 
      0.76376261582597333443134119895*GFOffset(v3,0,0,-2) - 
      1.74574312188793905012877988332*GFOffset(v3,0,0,-1) + 
      1.24099025303098285784871934219*GFOffset(v3,0,0,1),0) + IfThen(tk == 
      4,0.5*GFOffset(v3,0,0,-4) - 
      1.41016417794242666282363913699*GFOffset(v3,0,0,-3) + 
      2.66666666666666666666666666667*GFOffset(v3,0,0,-2) - 
      6.7565024887242400038430275297*GFOffset(v3,0,0,-1) + 
      5.*GFOffset(v3,0,0,0),0)) - 0.4*alphaDeriv*(IfThen(tk == 
      0,10.*GFOffset(v3,0,0,-1),0) + IfThen(tk == 
      4,-10.*GFOffset(v3,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL PDrho1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDrho2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDrho3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDv11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDv22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDv33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDrho1 = J11L*LDrho1 + J21L*LDrho2 + J31L*LDrho3;
      
      PDrho2 = J12L*LDrho1 + J22L*LDrho2 + J32L*LDrho3;
      
      PDrho3 = J13L*LDrho1 + J23L*LDrho2 + J33L*LDrho3;
      
      PDv11 = J11L*LDv11 + J21L*LDv12 + J31L*LDv13;
      
      PDv22 = J12L*LDv21 + J22L*LDv22 + J32L*LDv23;
      
      PDv33 = J13L*LDv31 + J23L*LDv32 + J33L*LDv33;
    }
    else
    {
      PDrho1 = LDrho1;
      
      PDrho2 = LDrho2;
      
      PDrho3 = LDrho3;
      
      PDv11 = LDv11;
      
      PDv22 = LDv22;
      
      PDv33 = LDv33;
    }
    
    CCTK_REAL urhsL CCTK_ATTRIBUTE_UNUSED = rhoL + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.200341796875*GFOffset(u,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(u,1,0,0) - 
      0.533333333333333333333333333333*GFOffset(u,2,0,0) + 
      0.466144563316004380207582742019*GFOffset(u,3,0,0) - 
      0.199658203125*GFOffset(u,4,0,0),0) + IfThen(ti == 
      1,0.0858101822480808281251378637108*GFOffset(u,-1,0,0) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(u,1,0,0) - 
      0.199853515625*GFOffset(u,2,0,0) + 
      0.0856183891804906004462907077178*GFOffset(u,3,0,0),0) + IfThen(ti == 
      2,-0.2*GFOffset(u,0,0,0) + 0.175*(GFOffset(u,-1,0,0) + 
      GFOffset(u,1,0,0)) - 0.075*(GFOffset(u,-2,0,0) + GFOffset(u,2,0,0)),0) 
      + IfThen(ti == 3,0.0856183891804906004462907077178*GFOffset(u,-3,0,0) - 
      0.199853515625*GFOffset(u,-2,0,0) + 
      0.228571428571428571428571428571*GFOffset(u,-1,0,0) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(u,1,0,0),0) + IfThen(ti == 
      4,-0.199658203125*GFOffset(u,-4,0,0) + 
      0.466144563316004380207582742019*GFOffset(u,-3,0,0) - 
      0.533333333333333333333333333333*GFOffset(u,-2,0,0) + 
      0.467188770017328953125750591314*GFOffset(u,-1,0,0) - 
      0.200341796875*GFOffset(u,0,0,0),0))*pow(dx,-1) + (IfThen(tj == 
      0,-0.200341796875*GFOffset(u,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(u,0,1,0) - 
      0.533333333333333333333333333333*GFOffset(u,0,2,0) + 
      0.466144563316004380207582742019*GFOffset(u,0,3,0) - 
      0.199658203125*GFOffset(u,0,4,0),0) + IfThen(tj == 
      1,0.0858101822480808281251378637108*GFOffset(u,0,-1,0) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(u,0,1,0) - 
      0.199853515625*GFOffset(u,0,2,0) + 
      0.0856183891804906004462907077178*GFOffset(u,0,3,0),0) + IfThen(tj == 
      2,-0.2*GFOffset(u,0,0,0) + 0.175*(GFOffset(u,0,-1,0) + 
      GFOffset(u,0,1,0)) - 0.075*(GFOffset(u,0,-2,0) + GFOffset(u,0,2,0)),0) 
      + IfThen(tj == 3,0.0856183891804906004462907077178*GFOffset(u,0,-3,0) - 
      0.199853515625*GFOffset(u,0,-2,0) + 
      0.228571428571428571428571428571*GFOffset(u,0,-1,0) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(u,0,1,0),0) + IfThen(tj == 
      4,-0.199658203125*GFOffset(u,0,-4,0) + 
      0.466144563316004380207582742019*GFOffset(u,0,-3,0) - 
      0.533333333333333333333333333333*GFOffset(u,0,-2,0) + 
      0.467188770017328953125750591314*GFOffset(u,0,-1,0) - 
      0.200341796875*GFOffset(u,0,0,0),0))*pow(dy,-1) + (IfThen(tk == 
      0,-0.200341796875*GFOffset(u,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(u,0,0,1) - 
      0.533333333333333333333333333333*GFOffset(u,0,0,2) + 
      0.466144563316004380207582742019*GFOffset(u,0,0,3) - 
      0.199658203125*GFOffset(u,0,0,4),0) + IfThen(tk == 
      1,0.0858101822480808281251378637108*GFOffset(u,0,0,-1) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(u,0,0,1) - 
      0.199853515625*GFOffset(u,0,0,2) + 
      0.0856183891804906004462907077178*GFOffset(u,0,0,3),0) + IfThen(tk == 
      2,-0.2*GFOffset(u,0,0,0) + 0.175*(GFOffset(u,0,0,-1) + 
      GFOffset(u,0,0,1)) - 0.075*(GFOffset(u,0,0,-2) + GFOffset(u,0,0,2)),0) 
      + IfThen(tk == 3,0.0856183891804906004462907077178*GFOffset(u,0,0,-3) - 
      0.199853515625*GFOffset(u,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(u,0,0,-1) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(u,0,0,1),0) + IfThen(tk == 
      4,-0.199658203125*GFOffset(u,0,0,-4) + 
      0.466144563316004380207582742019*GFOffset(u,0,0,-3) - 
      0.533333333333333333333333333333*GFOffset(u,0,0,-2) + 
      0.467188770017328953125750591314*GFOffset(u,0,0,-1) - 
      0.200341796875*GFOffset(u,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti == 
      0,-0.200341796875*GFOffset(u,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(u,1,0,0) - 
      0.533333333333333333333333333333*GFOffset(u,2,0,0) + 
      0.466144563316004380207582742019*GFOffset(u,3,0,0) - 
      0.199658203125*GFOffset(u,4,0,0),0) + IfThen(ti == 
      1,0.0858101822480808281251378637108*GFOffset(u,-1,0,0) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(u,1,0,0) - 
      0.199853515625*GFOffset(u,2,0,0) + 
      0.0856183891804906004462907077178*GFOffset(u,3,0,0),0) + IfThen(ti == 
      2,-0.2*GFOffset(u,0,0,0) + 0.175*(GFOffset(u,-1,0,0) + 
      GFOffset(u,1,0,0)) - 0.075*(GFOffset(u,-2,0,0) + GFOffset(u,2,0,0)),0) 
      + IfThen(ti == 3,0.0856183891804906004462907077178*GFOffset(u,-3,0,0) - 
      0.199853515625*GFOffset(u,-2,0,0) + 
      0.228571428571428571428571428571*GFOffset(u,-1,0,0) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(u,1,0,0),0) + IfThen(ti == 
      4,-0.199658203125*GFOffset(u,-4,0,0) + 
      0.466144563316004380207582742019*GFOffset(u,-3,0,0) - 
      0.533333333333333333333333333333*GFOffset(u,-2,0,0) + 
      0.467188770017328953125750591314*GFOffset(u,-1,0,0) - 
      0.200341796875*GFOffset(u,0,0,0),0))*pow(dx,-1) + (IfThen(tj == 
      0,-0.200341796875*GFOffset(u,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(u,0,1,0) - 
      0.533333333333333333333333333333*GFOffset(u,0,2,0) + 
      0.466144563316004380207582742019*GFOffset(u,0,3,0) - 
      0.199658203125*GFOffset(u,0,4,0),0) + IfThen(tj == 
      1,0.0858101822480808281251378637108*GFOffset(u,0,-1,0) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(u,0,1,0) - 
      0.199853515625*GFOffset(u,0,2,0) + 
      0.0856183891804906004462907077178*GFOffset(u,0,3,0),0) + IfThen(tj == 
      2,-0.2*GFOffset(u,0,0,0) + 0.175*(GFOffset(u,0,-1,0) + 
      GFOffset(u,0,1,0)) - 0.075*(GFOffset(u,0,-2,0) + GFOffset(u,0,2,0)),0) 
      + IfThen(tj == 3,0.0856183891804906004462907077178*GFOffset(u,0,-3,0) - 
      0.199853515625*GFOffset(u,0,-2,0) + 
      0.228571428571428571428571428571*GFOffset(u,0,-1,0) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(u,0,1,0),0) + IfThen(tj == 
      4,-0.199658203125*GFOffset(u,0,-4,0) + 
      0.466144563316004380207582742019*GFOffset(u,0,-3,0) - 
      0.533333333333333333333333333333*GFOffset(u,0,-2,0) + 
      0.467188770017328953125750591314*GFOffset(u,0,-1,0) - 
      0.200341796875*GFOffset(u,0,0,0),0))*pow(dy,-1) + (IfThen(tk == 
      0,-0.200341796875*GFOffset(u,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(u,0,0,1) - 
      0.533333333333333333333333333333*GFOffset(u,0,0,2) + 
      0.466144563316004380207582742019*GFOffset(u,0,0,3) - 
      0.199658203125*GFOffset(u,0,0,4),0) + IfThen(tk == 
      1,0.0858101822480808281251378637108*GFOffset(u,0,0,-1) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(u,0,0,1) - 
      0.199853515625*GFOffset(u,0,0,2) + 
      0.0856183891804906004462907077178*GFOffset(u,0,0,3),0) + IfThen(tk == 
      2,-0.2*GFOffset(u,0,0,0) + 0.175*(GFOffset(u,0,0,-1) + 
      GFOffset(u,0,0,1)) - 0.075*(GFOffset(u,0,0,-2) + GFOffset(u,0,0,2)),0) 
      + IfThen(tk == 3,0.0856183891804906004462907077178*GFOffset(u,0,0,-3) - 
      0.199853515625*GFOffset(u,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(u,0,0,-1) - 
      0.200146484375*GFOffset(u,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(u,0,0,1),0) + IfThen(tk == 
      4,-0.199658203125*GFOffset(u,0,0,-4) + 
      0.466144563316004380207582742019*GFOffset(u,0,0,-3) - 
      0.533333333333333333333333333333*GFOffset(u,0,0,-2) + 
      0.467188770017328953125750591314*GFOffset(u,0,0,-1) - 
      0.200341796875*GFOffset(u,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL rhorhsL CCTK_ATTRIBUTE_UNUSED = PDv11 + PDv22 + PDv33 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.200341796875*GFOffset(rho,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(rho,1,0,0) - 
      0.533333333333333333333333333333*GFOffset(rho,2,0,0) + 
      0.466144563316004380207582742019*GFOffset(rho,3,0,0) - 
      0.199658203125*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      1,0.0858101822480808281251378637108*GFOffset(rho,-1,0,0) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(rho,1,0,0) - 
      0.199853515625*GFOffset(rho,2,0,0) + 
      0.0856183891804906004462907077178*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      2,-0.2*GFOffset(rho,0,0,0) + 0.175*(GFOffset(rho,-1,0,0) + 
      GFOffset(rho,1,0,0)) - 0.075*(GFOffset(rho,-2,0,0) + 
      GFOffset(rho,2,0,0)),0) + IfThen(ti == 
      3,0.0856183891804906004462907077178*GFOffset(rho,-3,0,0) - 
      0.199853515625*GFOffset(rho,-2,0,0) + 
      0.228571428571428571428571428571*GFOffset(rho,-1,0,0) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      4,-0.199658203125*GFOffset(rho,-4,0,0) + 
      0.466144563316004380207582742019*GFOffset(rho,-3,0,0) - 
      0.533333333333333333333333333333*GFOffset(rho,-2,0,0) + 
      0.467188770017328953125750591314*GFOffset(rho,-1,0,0) - 
      0.200341796875*GFOffset(rho,0,0,0),0))*pow(dx,-1) + (IfThen(tj == 
      0,-0.200341796875*GFOffset(rho,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(rho,0,1,0) - 
      0.533333333333333333333333333333*GFOffset(rho,0,2,0) + 
      0.466144563316004380207582742019*GFOffset(rho,0,3,0) - 
      0.199658203125*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      1,0.0858101822480808281251378637108*GFOffset(rho,0,-1,0) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(rho,0,1,0) - 
      0.199853515625*GFOffset(rho,0,2,0) + 
      0.0856183891804906004462907077178*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      2,-0.2*GFOffset(rho,0,0,0) + 0.175*(GFOffset(rho,0,-1,0) + 
      GFOffset(rho,0,1,0)) - 0.075*(GFOffset(rho,0,-2,0) + 
      GFOffset(rho,0,2,0)),0) + IfThen(tj == 
      3,0.0856183891804906004462907077178*GFOffset(rho,0,-3,0) - 
      0.199853515625*GFOffset(rho,0,-2,0) + 
      0.228571428571428571428571428571*GFOffset(rho,0,-1,0) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      4,-0.199658203125*GFOffset(rho,0,-4,0) + 
      0.466144563316004380207582742019*GFOffset(rho,0,-3,0) - 
      0.533333333333333333333333333333*GFOffset(rho,0,-2,0) + 
      0.467188770017328953125750591314*GFOffset(rho,0,-1,0) - 
      0.200341796875*GFOffset(rho,0,0,0),0))*pow(dy,-1) + (IfThen(tk == 
      0,-0.200341796875*GFOffset(rho,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(rho,0,0,1) - 
      0.533333333333333333333333333333*GFOffset(rho,0,0,2) + 
      0.466144563316004380207582742019*GFOffset(rho,0,0,3) - 
      0.199658203125*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      1,0.0858101822480808281251378637108*GFOffset(rho,0,0,-1) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(rho,0,0,1) - 
      0.199853515625*GFOffset(rho,0,0,2) + 
      0.0856183891804906004462907077178*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      2,-0.2*GFOffset(rho,0,0,0) + 0.175*(GFOffset(rho,0,0,-1) + 
      GFOffset(rho,0,0,1)) - 0.075*(GFOffset(rho,0,0,-2) + 
      GFOffset(rho,0,0,2)),0) + IfThen(tk == 
      3,0.0856183891804906004462907077178*GFOffset(rho,0,0,-3) - 
      0.199853515625*GFOffset(rho,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(rho,0,0,-1) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      4,-0.199658203125*GFOffset(rho,0,0,-4) + 
      0.466144563316004380207582742019*GFOffset(rho,0,0,-3) - 
      0.533333333333333333333333333333*GFOffset(rho,0,0,-2) + 
      0.467188770017328953125750591314*GFOffset(rho,0,0,-1) - 
      0.200341796875*GFOffset(rho,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.200341796875*GFOffset(rho,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(rho,1,0,0) - 
      0.533333333333333333333333333333*GFOffset(rho,2,0,0) + 
      0.466144563316004380207582742019*GFOffset(rho,3,0,0) - 
      0.199658203125*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      1,0.0858101822480808281251378637108*GFOffset(rho,-1,0,0) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(rho,1,0,0) - 
      0.199853515625*GFOffset(rho,2,0,0) + 
      0.0856183891804906004462907077178*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      2,-0.2*GFOffset(rho,0,0,0) + 0.175*(GFOffset(rho,-1,0,0) + 
      GFOffset(rho,1,0,0)) - 0.075*(GFOffset(rho,-2,0,0) + 
      GFOffset(rho,2,0,0)),0) + IfThen(ti == 
      3,0.0856183891804906004462907077178*GFOffset(rho,-3,0,0) - 
      0.199853515625*GFOffset(rho,-2,0,0) + 
      0.228571428571428571428571428571*GFOffset(rho,-1,0,0) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      4,-0.199658203125*GFOffset(rho,-4,0,0) + 
      0.466144563316004380207582742019*GFOffset(rho,-3,0,0) - 
      0.533333333333333333333333333333*GFOffset(rho,-2,0,0) + 
      0.467188770017328953125750591314*GFOffset(rho,-1,0,0) - 
      0.200341796875*GFOffset(rho,0,0,0),0))*pow(dx,-1) + (IfThen(tj == 
      0,-0.200341796875*GFOffset(rho,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(rho,0,1,0) - 
      0.533333333333333333333333333333*GFOffset(rho,0,2,0) + 
      0.466144563316004380207582742019*GFOffset(rho,0,3,0) - 
      0.199658203125*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      1,0.0858101822480808281251378637108*GFOffset(rho,0,-1,0) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(rho,0,1,0) - 
      0.199853515625*GFOffset(rho,0,2,0) + 
      0.0856183891804906004462907077178*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      2,-0.2*GFOffset(rho,0,0,0) + 0.175*(GFOffset(rho,0,-1,0) + 
      GFOffset(rho,0,1,0)) - 0.075*(GFOffset(rho,0,-2,0) + 
      GFOffset(rho,0,2,0)),0) + IfThen(tj == 
      3,0.0856183891804906004462907077178*GFOffset(rho,0,-3,0) - 
      0.199853515625*GFOffset(rho,0,-2,0) + 
      0.228571428571428571428571428571*GFOffset(rho,0,-1,0) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      4,-0.199658203125*GFOffset(rho,0,-4,0) + 
      0.466144563316004380207582742019*GFOffset(rho,0,-3,0) - 
      0.533333333333333333333333333333*GFOffset(rho,0,-2,0) + 
      0.467188770017328953125750591314*GFOffset(rho,0,-1,0) - 
      0.200341796875*GFOffset(rho,0,0,0),0))*pow(dy,-1) + (IfThen(tk == 
      0,-0.200341796875*GFOffset(rho,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(rho,0,0,1) - 
      0.533333333333333333333333333333*GFOffset(rho,0,0,2) + 
      0.466144563316004380207582742019*GFOffset(rho,0,0,3) - 
      0.199658203125*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      1,0.0858101822480808281251378637108*GFOffset(rho,0,0,-1) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(rho,0,0,1) - 
      0.199853515625*GFOffset(rho,0,0,2) + 
      0.0856183891804906004462907077178*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      2,-0.2*GFOffset(rho,0,0,0) + 0.175*(GFOffset(rho,0,0,-1) + 
      GFOffset(rho,0,0,1)) - 0.075*(GFOffset(rho,0,0,-2) + 
      GFOffset(rho,0,0,2)),0) + IfThen(tk == 
      3,0.0856183891804906004462907077178*GFOffset(rho,0,0,-3) - 
      0.199853515625*GFOffset(rho,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(rho,0,0,-1) - 
      0.200146484375*GFOffset(rho,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      4,-0.199658203125*GFOffset(rho,0,0,-4) + 
      0.466144563316004380207582742019*GFOffset(rho,0,0,-3) - 
      0.533333333333333333333333333333*GFOffset(rho,0,0,-2) + 
      0.467188770017328953125750591314*GFOffset(rho,0,0,-1) - 
      0.200341796875*GFOffset(rho,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL v1rhsL CCTK_ATTRIBUTE_UNUSED = PDrho1 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.200341796875*GFOffset(v1,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v1,1,0,0) - 
      0.533333333333333333333333333333*GFOffset(v1,2,0,0) + 
      0.466144563316004380207582742019*GFOffset(v1,3,0,0) - 
      0.199658203125*GFOffset(v1,4,0,0),0) + IfThen(ti == 
      1,0.0858101822480808281251378637108*GFOffset(v1,-1,0,0) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v1,1,0,0) - 
      0.199853515625*GFOffset(v1,2,0,0) + 
      0.0856183891804906004462907077178*GFOffset(v1,3,0,0),0) + IfThen(ti == 
      2,-0.2*GFOffset(v1,0,0,0) + 0.175*(GFOffset(v1,-1,0,0) + 
      GFOffset(v1,1,0,0)) - 0.075*(GFOffset(v1,-2,0,0) + 
      GFOffset(v1,2,0,0)),0) + IfThen(ti == 
      3,0.0856183891804906004462907077178*GFOffset(v1,-3,0,0) - 
      0.199853515625*GFOffset(v1,-2,0,0) + 
      0.228571428571428571428571428571*GFOffset(v1,-1,0,0) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v1,1,0,0),0) + IfThen(ti == 
      4,-0.199658203125*GFOffset(v1,-4,0,0) + 
      0.466144563316004380207582742019*GFOffset(v1,-3,0,0) - 
      0.533333333333333333333333333333*GFOffset(v1,-2,0,0) + 
      0.467188770017328953125750591314*GFOffset(v1,-1,0,0) - 
      0.200341796875*GFOffset(v1,0,0,0),0))*pow(dx,-1) + (IfThen(tj == 
      0,-0.200341796875*GFOffset(v1,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v1,0,1,0) - 
      0.533333333333333333333333333333*GFOffset(v1,0,2,0) + 
      0.466144563316004380207582742019*GFOffset(v1,0,3,0) - 
      0.199658203125*GFOffset(v1,0,4,0),0) + IfThen(tj == 
      1,0.0858101822480808281251378637108*GFOffset(v1,0,-1,0) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v1,0,1,0) - 
      0.199853515625*GFOffset(v1,0,2,0) + 
      0.0856183891804906004462907077178*GFOffset(v1,0,3,0),0) + IfThen(tj == 
      2,-0.2*GFOffset(v1,0,0,0) + 0.175*(GFOffset(v1,0,-1,0) + 
      GFOffset(v1,0,1,0)) - 0.075*(GFOffset(v1,0,-2,0) + 
      GFOffset(v1,0,2,0)),0) + IfThen(tj == 
      3,0.0856183891804906004462907077178*GFOffset(v1,0,-3,0) - 
      0.199853515625*GFOffset(v1,0,-2,0) + 
      0.228571428571428571428571428571*GFOffset(v1,0,-1,0) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v1,0,1,0),0) + IfThen(tj == 
      4,-0.199658203125*GFOffset(v1,0,-4,0) + 
      0.466144563316004380207582742019*GFOffset(v1,0,-3,0) - 
      0.533333333333333333333333333333*GFOffset(v1,0,-2,0) + 
      0.467188770017328953125750591314*GFOffset(v1,0,-1,0) - 
      0.200341796875*GFOffset(v1,0,0,0),0))*pow(dy,-1) + (IfThen(tk == 
      0,-0.200341796875*GFOffset(v1,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v1,0,0,1) - 
      0.533333333333333333333333333333*GFOffset(v1,0,0,2) + 
      0.466144563316004380207582742019*GFOffset(v1,0,0,3) - 
      0.199658203125*GFOffset(v1,0,0,4),0) + IfThen(tk == 
      1,0.0858101822480808281251378637108*GFOffset(v1,0,0,-1) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v1,0,0,1) - 
      0.199853515625*GFOffset(v1,0,0,2) + 
      0.0856183891804906004462907077178*GFOffset(v1,0,0,3),0) + IfThen(tk == 
      2,-0.2*GFOffset(v1,0,0,0) + 0.175*(GFOffset(v1,0,0,-1) + 
      GFOffset(v1,0,0,1)) - 0.075*(GFOffset(v1,0,0,-2) + 
      GFOffset(v1,0,0,2)),0) + IfThen(tk == 
      3,0.0856183891804906004462907077178*GFOffset(v1,0,0,-3) - 
      0.199853515625*GFOffset(v1,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(v1,0,0,-1) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v1,0,0,1),0) + IfThen(tk == 
      4,-0.199658203125*GFOffset(v1,0,0,-4) + 
      0.466144563316004380207582742019*GFOffset(v1,0,0,-3) - 
      0.533333333333333333333333333333*GFOffset(v1,0,0,-2) + 
      0.467188770017328953125750591314*GFOffset(v1,0,0,-1) - 
      0.200341796875*GFOffset(v1,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.200341796875*GFOffset(v1,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v1,1,0,0) - 
      0.533333333333333333333333333333*GFOffset(v1,2,0,0) + 
      0.466144563316004380207582742019*GFOffset(v1,3,0,0) - 
      0.199658203125*GFOffset(v1,4,0,0),0) + IfThen(ti == 
      1,0.0858101822480808281251378637108*GFOffset(v1,-1,0,0) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v1,1,0,0) - 
      0.199853515625*GFOffset(v1,2,0,0) + 
      0.0856183891804906004462907077178*GFOffset(v1,3,0,0),0) + IfThen(ti == 
      2,-0.2*GFOffset(v1,0,0,0) + 0.175*(GFOffset(v1,-1,0,0) + 
      GFOffset(v1,1,0,0)) - 0.075*(GFOffset(v1,-2,0,0) + 
      GFOffset(v1,2,0,0)),0) + IfThen(ti == 
      3,0.0856183891804906004462907077178*GFOffset(v1,-3,0,0) - 
      0.199853515625*GFOffset(v1,-2,0,0) + 
      0.228571428571428571428571428571*GFOffset(v1,-1,0,0) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v1,1,0,0),0) + IfThen(ti == 
      4,-0.199658203125*GFOffset(v1,-4,0,0) + 
      0.466144563316004380207582742019*GFOffset(v1,-3,0,0) - 
      0.533333333333333333333333333333*GFOffset(v1,-2,0,0) + 
      0.467188770017328953125750591314*GFOffset(v1,-1,0,0) - 
      0.200341796875*GFOffset(v1,0,0,0),0))*pow(dx,-1) + (IfThen(tj == 
      0,-0.200341796875*GFOffset(v1,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v1,0,1,0) - 
      0.533333333333333333333333333333*GFOffset(v1,0,2,0) + 
      0.466144563316004380207582742019*GFOffset(v1,0,3,0) - 
      0.199658203125*GFOffset(v1,0,4,0),0) + IfThen(tj == 
      1,0.0858101822480808281251378637108*GFOffset(v1,0,-1,0) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v1,0,1,0) - 
      0.199853515625*GFOffset(v1,0,2,0) + 
      0.0856183891804906004462907077178*GFOffset(v1,0,3,0),0) + IfThen(tj == 
      2,-0.2*GFOffset(v1,0,0,0) + 0.175*(GFOffset(v1,0,-1,0) + 
      GFOffset(v1,0,1,0)) - 0.075*(GFOffset(v1,0,-2,0) + 
      GFOffset(v1,0,2,0)),0) + IfThen(tj == 
      3,0.0856183891804906004462907077178*GFOffset(v1,0,-3,0) - 
      0.199853515625*GFOffset(v1,0,-2,0) + 
      0.228571428571428571428571428571*GFOffset(v1,0,-1,0) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v1,0,1,0),0) + IfThen(tj == 
      4,-0.199658203125*GFOffset(v1,0,-4,0) + 
      0.466144563316004380207582742019*GFOffset(v1,0,-3,0) - 
      0.533333333333333333333333333333*GFOffset(v1,0,-2,0) + 
      0.467188770017328953125750591314*GFOffset(v1,0,-1,0) - 
      0.200341796875*GFOffset(v1,0,0,0),0))*pow(dy,-1) + (IfThen(tk == 
      0,-0.200341796875*GFOffset(v1,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v1,0,0,1) - 
      0.533333333333333333333333333333*GFOffset(v1,0,0,2) + 
      0.466144563316004380207582742019*GFOffset(v1,0,0,3) - 
      0.199658203125*GFOffset(v1,0,0,4),0) + IfThen(tk == 
      1,0.0858101822480808281251378637108*GFOffset(v1,0,0,-1) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v1,0,0,1) - 
      0.199853515625*GFOffset(v1,0,0,2) + 
      0.0856183891804906004462907077178*GFOffset(v1,0,0,3),0) + IfThen(tk == 
      2,-0.2*GFOffset(v1,0,0,0) + 0.175*(GFOffset(v1,0,0,-1) + 
      GFOffset(v1,0,0,1)) - 0.075*(GFOffset(v1,0,0,-2) + 
      GFOffset(v1,0,0,2)),0) + IfThen(tk == 
      3,0.0856183891804906004462907077178*GFOffset(v1,0,0,-3) - 
      0.199853515625*GFOffset(v1,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(v1,0,0,-1) - 
      0.200146484375*GFOffset(v1,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v1,0,0,1),0) + IfThen(tk == 
      4,-0.199658203125*GFOffset(v1,0,0,-4) + 
      0.466144563316004380207582742019*GFOffset(v1,0,0,-3) - 
      0.533333333333333333333333333333*GFOffset(v1,0,0,-2) + 
      0.467188770017328953125750591314*GFOffset(v1,0,0,-1) - 
      0.200341796875*GFOffset(v1,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL v2rhsL CCTK_ATTRIBUTE_UNUSED = PDrho2 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.200341796875*GFOffset(v2,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v2,1,0,0) - 
      0.533333333333333333333333333333*GFOffset(v2,2,0,0) + 
      0.466144563316004380207582742019*GFOffset(v2,3,0,0) - 
      0.199658203125*GFOffset(v2,4,0,0),0) + IfThen(ti == 
      1,0.0858101822480808281251378637108*GFOffset(v2,-1,0,0) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v2,1,0,0) - 
      0.199853515625*GFOffset(v2,2,0,0) + 
      0.0856183891804906004462907077178*GFOffset(v2,3,0,0),0) + IfThen(ti == 
      2,-0.2*GFOffset(v2,0,0,0) + 0.175*(GFOffset(v2,-1,0,0) + 
      GFOffset(v2,1,0,0)) - 0.075*(GFOffset(v2,-2,0,0) + 
      GFOffset(v2,2,0,0)),0) + IfThen(ti == 
      3,0.0856183891804906004462907077178*GFOffset(v2,-3,0,0) - 
      0.199853515625*GFOffset(v2,-2,0,0) + 
      0.228571428571428571428571428571*GFOffset(v2,-1,0,0) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v2,1,0,0),0) + IfThen(ti == 
      4,-0.199658203125*GFOffset(v2,-4,0,0) + 
      0.466144563316004380207582742019*GFOffset(v2,-3,0,0) - 
      0.533333333333333333333333333333*GFOffset(v2,-2,0,0) + 
      0.467188770017328953125750591314*GFOffset(v2,-1,0,0) - 
      0.200341796875*GFOffset(v2,0,0,0),0))*pow(dx,-1) + (IfThen(tj == 
      0,-0.200341796875*GFOffset(v2,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v2,0,1,0) - 
      0.533333333333333333333333333333*GFOffset(v2,0,2,0) + 
      0.466144563316004380207582742019*GFOffset(v2,0,3,0) - 
      0.199658203125*GFOffset(v2,0,4,0),0) + IfThen(tj == 
      1,0.0858101822480808281251378637108*GFOffset(v2,0,-1,0) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v2,0,1,0) - 
      0.199853515625*GFOffset(v2,0,2,0) + 
      0.0856183891804906004462907077178*GFOffset(v2,0,3,0),0) + IfThen(tj == 
      2,-0.2*GFOffset(v2,0,0,0) + 0.175*(GFOffset(v2,0,-1,0) + 
      GFOffset(v2,0,1,0)) - 0.075*(GFOffset(v2,0,-2,0) + 
      GFOffset(v2,0,2,0)),0) + IfThen(tj == 
      3,0.0856183891804906004462907077178*GFOffset(v2,0,-3,0) - 
      0.199853515625*GFOffset(v2,0,-2,0) + 
      0.228571428571428571428571428571*GFOffset(v2,0,-1,0) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v2,0,1,0),0) + IfThen(tj == 
      4,-0.199658203125*GFOffset(v2,0,-4,0) + 
      0.466144563316004380207582742019*GFOffset(v2,0,-3,0) - 
      0.533333333333333333333333333333*GFOffset(v2,0,-2,0) + 
      0.467188770017328953125750591314*GFOffset(v2,0,-1,0) - 
      0.200341796875*GFOffset(v2,0,0,0),0))*pow(dy,-1) + (IfThen(tk == 
      0,-0.200341796875*GFOffset(v2,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v2,0,0,1) - 
      0.533333333333333333333333333333*GFOffset(v2,0,0,2) + 
      0.466144563316004380207582742019*GFOffset(v2,0,0,3) - 
      0.199658203125*GFOffset(v2,0,0,4),0) + IfThen(tk == 
      1,0.0858101822480808281251378637108*GFOffset(v2,0,0,-1) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v2,0,0,1) - 
      0.199853515625*GFOffset(v2,0,0,2) + 
      0.0856183891804906004462907077178*GFOffset(v2,0,0,3),0) + IfThen(tk == 
      2,-0.2*GFOffset(v2,0,0,0) + 0.175*(GFOffset(v2,0,0,-1) + 
      GFOffset(v2,0,0,1)) - 0.075*(GFOffset(v2,0,0,-2) + 
      GFOffset(v2,0,0,2)),0) + IfThen(tk == 
      3,0.0856183891804906004462907077178*GFOffset(v2,0,0,-3) - 
      0.199853515625*GFOffset(v2,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(v2,0,0,-1) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v2,0,0,1),0) + IfThen(tk == 
      4,-0.199658203125*GFOffset(v2,0,0,-4) + 
      0.466144563316004380207582742019*GFOffset(v2,0,0,-3) - 
      0.533333333333333333333333333333*GFOffset(v2,0,0,-2) + 
      0.467188770017328953125750591314*GFOffset(v2,0,0,-1) - 
      0.200341796875*GFOffset(v2,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.200341796875*GFOffset(v2,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v2,1,0,0) - 
      0.533333333333333333333333333333*GFOffset(v2,2,0,0) + 
      0.466144563316004380207582742019*GFOffset(v2,3,0,0) - 
      0.199658203125*GFOffset(v2,4,0,0),0) + IfThen(ti == 
      1,0.0858101822480808281251378637108*GFOffset(v2,-1,0,0) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v2,1,0,0) - 
      0.199853515625*GFOffset(v2,2,0,0) + 
      0.0856183891804906004462907077178*GFOffset(v2,3,0,0),0) + IfThen(ti == 
      2,-0.2*GFOffset(v2,0,0,0) + 0.175*(GFOffset(v2,-1,0,0) + 
      GFOffset(v2,1,0,0)) - 0.075*(GFOffset(v2,-2,0,0) + 
      GFOffset(v2,2,0,0)),0) + IfThen(ti == 
      3,0.0856183891804906004462907077178*GFOffset(v2,-3,0,0) - 
      0.199853515625*GFOffset(v2,-2,0,0) + 
      0.228571428571428571428571428571*GFOffset(v2,-1,0,0) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v2,1,0,0),0) + IfThen(ti == 
      4,-0.199658203125*GFOffset(v2,-4,0,0) + 
      0.466144563316004380207582742019*GFOffset(v2,-3,0,0) - 
      0.533333333333333333333333333333*GFOffset(v2,-2,0,0) + 
      0.467188770017328953125750591314*GFOffset(v2,-1,0,0) - 
      0.200341796875*GFOffset(v2,0,0,0),0))*pow(dx,-1) + (IfThen(tj == 
      0,-0.200341796875*GFOffset(v2,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v2,0,1,0) - 
      0.533333333333333333333333333333*GFOffset(v2,0,2,0) + 
      0.466144563316004380207582742019*GFOffset(v2,0,3,0) - 
      0.199658203125*GFOffset(v2,0,4,0),0) + IfThen(tj == 
      1,0.0858101822480808281251378637108*GFOffset(v2,0,-1,0) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v2,0,1,0) - 
      0.199853515625*GFOffset(v2,0,2,0) + 
      0.0856183891804906004462907077178*GFOffset(v2,0,3,0),0) + IfThen(tj == 
      2,-0.2*GFOffset(v2,0,0,0) + 0.175*(GFOffset(v2,0,-1,0) + 
      GFOffset(v2,0,1,0)) - 0.075*(GFOffset(v2,0,-2,0) + 
      GFOffset(v2,0,2,0)),0) + IfThen(tj == 
      3,0.0856183891804906004462907077178*GFOffset(v2,0,-3,0) - 
      0.199853515625*GFOffset(v2,0,-2,0) + 
      0.228571428571428571428571428571*GFOffset(v2,0,-1,0) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v2,0,1,0),0) + IfThen(tj == 
      4,-0.199658203125*GFOffset(v2,0,-4,0) + 
      0.466144563316004380207582742019*GFOffset(v2,0,-3,0) - 
      0.533333333333333333333333333333*GFOffset(v2,0,-2,0) + 
      0.467188770017328953125750591314*GFOffset(v2,0,-1,0) - 
      0.200341796875*GFOffset(v2,0,0,0),0))*pow(dy,-1) + (IfThen(tk == 
      0,-0.200341796875*GFOffset(v2,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v2,0,0,1) - 
      0.533333333333333333333333333333*GFOffset(v2,0,0,2) + 
      0.466144563316004380207582742019*GFOffset(v2,0,0,3) - 
      0.199658203125*GFOffset(v2,0,0,4),0) + IfThen(tk == 
      1,0.0858101822480808281251378637108*GFOffset(v2,0,0,-1) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v2,0,0,1) - 
      0.199853515625*GFOffset(v2,0,0,2) + 
      0.0856183891804906004462907077178*GFOffset(v2,0,0,3),0) + IfThen(tk == 
      2,-0.2*GFOffset(v2,0,0,0) + 0.175*(GFOffset(v2,0,0,-1) + 
      GFOffset(v2,0,0,1)) - 0.075*(GFOffset(v2,0,0,-2) + 
      GFOffset(v2,0,0,2)),0) + IfThen(tk == 
      3,0.0856183891804906004462907077178*GFOffset(v2,0,0,-3) - 
      0.199853515625*GFOffset(v2,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(v2,0,0,-1) - 
      0.200146484375*GFOffset(v2,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v2,0,0,1),0) + IfThen(tk == 
      4,-0.199658203125*GFOffset(v2,0,0,-4) + 
      0.466144563316004380207582742019*GFOffset(v2,0,0,-3) - 
      0.533333333333333333333333333333*GFOffset(v2,0,0,-2) + 
      0.467188770017328953125750591314*GFOffset(v2,0,0,-1) - 
      0.200341796875*GFOffset(v2,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL v3rhsL CCTK_ATTRIBUTE_UNUSED = PDrho3 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.200341796875*GFOffset(v3,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v3,1,0,0) - 
      0.533333333333333333333333333333*GFOffset(v3,2,0,0) + 
      0.466144563316004380207582742019*GFOffset(v3,3,0,0) - 
      0.199658203125*GFOffset(v3,4,0,0),0) + IfThen(ti == 
      1,0.0858101822480808281251378637108*GFOffset(v3,-1,0,0) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v3,1,0,0) - 
      0.199853515625*GFOffset(v3,2,0,0) + 
      0.0856183891804906004462907077178*GFOffset(v3,3,0,0),0) + IfThen(ti == 
      2,-0.2*GFOffset(v3,0,0,0) + 0.175*(GFOffset(v3,-1,0,0) + 
      GFOffset(v3,1,0,0)) - 0.075*(GFOffset(v3,-2,0,0) + 
      GFOffset(v3,2,0,0)),0) + IfThen(ti == 
      3,0.0856183891804906004462907077178*GFOffset(v3,-3,0,0) - 
      0.199853515625*GFOffset(v3,-2,0,0) + 
      0.228571428571428571428571428571*GFOffset(v3,-1,0,0) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v3,1,0,0),0) + IfThen(ti == 
      4,-0.199658203125*GFOffset(v3,-4,0,0) + 
      0.466144563316004380207582742019*GFOffset(v3,-3,0,0) - 
      0.533333333333333333333333333333*GFOffset(v3,-2,0,0) + 
      0.467188770017328953125750591314*GFOffset(v3,-1,0,0) - 
      0.200341796875*GFOffset(v3,0,0,0),0))*pow(dx,-1) + (IfThen(tj == 
      0,-0.200341796875*GFOffset(v3,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v3,0,1,0) - 
      0.533333333333333333333333333333*GFOffset(v3,0,2,0) + 
      0.466144563316004380207582742019*GFOffset(v3,0,3,0) - 
      0.199658203125*GFOffset(v3,0,4,0),0) + IfThen(tj == 
      1,0.0858101822480808281251378637108*GFOffset(v3,0,-1,0) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v3,0,1,0) - 
      0.199853515625*GFOffset(v3,0,2,0) + 
      0.0856183891804906004462907077178*GFOffset(v3,0,3,0),0) + IfThen(tj == 
      2,-0.2*GFOffset(v3,0,0,0) + 0.175*(GFOffset(v3,0,-1,0) + 
      GFOffset(v3,0,1,0)) - 0.075*(GFOffset(v3,0,-2,0) + 
      GFOffset(v3,0,2,0)),0) + IfThen(tj == 
      3,0.0856183891804906004462907077178*GFOffset(v3,0,-3,0) - 
      0.199853515625*GFOffset(v3,0,-2,0) + 
      0.228571428571428571428571428571*GFOffset(v3,0,-1,0) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v3,0,1,0),0) + IfThen(tj == 
      4,-0.199658203125*GFOffset(v3,0,-4,0) + 
      0.466144563316004380207582742019*GFOffset(v3,0,-3,0) - 
      0.533333333333333333333333333333*GFOffset(v3,0,-2,0) + 
      0.467188770017328953125750591314*GFOffset(v3,0,-1,0) - 
      0.200341796875*GFOffset(v3,0,0,0),0))*pow(dy,-1) + (IfThen(tk == 
      0,-0.200341796875*GFOffset(v3,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v3,0,0,1) - 
      0.533333333333333333333333333333*GFOffset(v3,0,0,2) + 
      0.466144563316004380207582742019*GFOffset(v3,0,0,3) - 
      0.199658203125*GFOffset(v3,0,0,4),0) + IfThen(tk == 
      1,0.0858101822480808281251378637108*GFOffset(v3,0,0,-1) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v3,0,0,1) - 
      0.199853515625*GFOffset(v3,0,0,2) + 
      0.0856183891804906004462907077178*GFOffset(v3,0,0,3),0) + IfThen(tk == 
      2,-0.2*GFOffset(v3,0,0,0) + 0.175*(GFOffset(v3,0,0,-1) + 
      GFOffset(v3,0,0,1)) - 0.075*(GFOffset(v3,0,0,-2) + 
      GFOffset(v3,0,0,2)),0) + IfThen(tk == 
      3,0.0856183891804906004462907077178*GFOffset(v3,0,0,-3) - 
      0.199853515625*GFOffset(v3,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(v3,0,0,-1) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v3,0,0,1),0) + IfThen(tk == 
      4,-0.199658203125*GFOffset(v3,0,0,-4) + 
      0.466144563316004380207582742019*GFOffset(v3,0,0,-3) - 
      0.533333333333333333333333333333*GFOffset(v3,0,0,-2) + 
      0.467188770017328953125750591314*GFOffset(v3,0,0,-1) - 
      0.200341796875*GFOffset(v3,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.200341796875*GFOffset(v3,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v3,1,0,0) - 
      0.533333333333333333333333333333*GFOffset(v3,2,0,0) + 
      0.466144563316004380207582742019*GFOffset(v3,3,0,0) - 
      0.199658203125*GFOffset(v3,4,0,0),0) + IfThen(ti == 
      1,0.0858101822480808281251378637108*GFOffset(v3,-1,0,0) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v3,1,0,0) - 
      0.199853515625*GFOffset(v3,2,0,0) + 
      0.0856183891804906004462907077178*GFOffset(v3,3,0,0),0) + IfThen(ti == 
      2,-0.2*GFOffset(v3,0,0,0) + 0.175*(GFOffset(v3,-1,0,0) + 
      GFOffset(v3,1,0,0)) - 0.075*(GFOffset(v3,-2,0,0) + 
      GFOffset(v3,2,0,0)),0) + IfThen(ti == 
      3,0.0856183891804906004462907077178*GFOffset(v3,-3,0,0) - 
      0.199853515625*GFOffset(v3,-2,0,0) + 
      0.228571428571428571428571428571*GFOffset(v3,-1,0,0) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v3,1,0,0),0) + IfThen(ti == 
      4,-0.199658203125*GFOffset(v3,-4,0,0) + 
      0.466144563316004380207582742019*GFOffset(v3,-3,0,0) - 
      0.533333333333333333333333333333*GFOffset(v3,-2,0,0) + 
      0.467188770017328953125750591314*GFOffset(v3,-1,0,0) - 
      0.200341796875*GFOffset(v3,0,0,0),0))*pow(dx,-1) + (IfThen(tj == 
      0,-0.200341796875*GFOffset(v3,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v3,0,1,0) - 
      0.533333333333333333333333333333*GFOffset(v3,0,2,0) + 
      0.466144563316004380207582742019*GFOffset(v3,0,3,0) - 
      0.199658203125*GFOffset(v3,0,4,0),0) + IfThen(tj == 
      1,0.0858101822480808281251378637108*GFOffset(v3,0,-1,0) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v3,0,1,0) - 
      0.199853515625*GFOffset(v3,0,2,0) + 
      0.0856183891804906004462907077178*GFOffset(v3,0,3,0),0) + IfThen(tj == 
      2,-0.2*GFOffset(v3,0,0,0) + 0.175*(GFOffset(v3,0,-1,0) + 
      GFOffset(v3,0,1,0)) - 0.075*(GFOffset(v3,0,-2,0) + 
      GFOffset(v3,0,2,0)),0) + IfThen(tj == 
      3,0.0856183891804906004462907077178*GFOffset(v3,0,-3,0) - 
      0.199853515625*GFOffset(v3,0,-2,0) + 
      0.228571428571428571428571428571*GFOffset(v3,0,-1,0) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v3,0,1,0),0) + IfThen(tj == 
      4,-0.199658203125*GFOffset(v3,0,-4,0) + 
      0.466144563316004380207582742019*GFOffset(v3,0,-3,0) - 
      0.533333333333333333333333333333*GFOffset(v3,0,-2,0) + 
      0.467188770017328953125750591314*GFOffset(v3,0,-1,0) - 
      0.200341796875*GFOffset(v3,0,0,0),0))*pow(dy,-1) + (IfThen(tk == 
      0,-0.200341796875*GFOffset(v3,0,0,0) + 
      0.467188770017328953125750591314*GFOffset(v3,0,0,1) - 
      0.533333333333333333333333333333*GFOffset(v3,0,0,2) + 
      0.466144563316004380207582742019*GFOffset(v3,0,0,3) - 
      0.199658203125*GFOffset(v3,0,0,4),0) + IfThen(tk == 
      1,0.0858101822480808281251378637108*GFOffset(v3,0,0,-1) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(v3,0,0,1) - 
      0.199853515625*GFOffset(v3,0,0,2) + 
      0.0856183891804906004462907077178*GFOffset(v3,0,0,3),0) + IfThen(tk == 
      2,-0.2*GFOffset(v3,0,0,0) + 0.175*(GFOffset(v3,0,0,-1) + 
      GFOffset(v3,0,0,1)) - 0.075*(GFOffset(v3,0,0,-2) + 
      GFOffset(v3,0,0,2)),0) + IfThen(tk == 
      3,0.0856183891804906004462907077178*GFOffset(v3,0,0,-3) - 
      0.199853515625*GFOffset(v3,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(v3,0,0,-1) - 
      0.200146484375*GFOffset(v3,0,0,0) + 
      0.0858101822480808281251378637108*GFOffset(v3,0,0,1),0) + IfThen(tk == 
      4,-0.199658203125*GFOffset(v3,0,0,-4) + 
      0.466144563316004380207582742019*GFOffset(v3,0,0,-3) - 
      0.533333333333333333333333333333*GFOffset(v3,0,0,-2) + 
      0.467188770017328953125750591314*GFOffset(v3,0,0,-1) - 
      0.200341796875*GFOffset(v3,0,0,0),0))*pow(dz,-1)));
    /* Copy local copies back to grid functions */
    rhorhs[index] = rhorhsL;
    urhs[index] = urhsL;
    v1rhs[index] = v1rhsL;
    v2rhs[index] = v2rhsL;
    v3rhs[index] = v3rhsL;
  }
  CCTK_ENDLOOP3(ML_WaveToy_DG4_RHSFirstOrder);
}
extern "C" void ML_WaveToy_DG4_RHSFirstOrder(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_WaveToy_DG4_RHSFirstOrder_Body");
  }
  if (cctk_iteration % ML_WaveToy_DG4_RHSFirstOrder_calc_every != ML_WaveToy_DG4_RHSFirstOrder_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_WaveToy_DG4::WT_detJ",
    "ML_WaveToy_DG4::WT_rho",
    "ML_WaveToy_DG4::WT_rhorhs",
    "ML_WaveToy_DG4::WT_u",
    "ML_WaveToy_DG4::WT_urhs",
    "ML_WaveToy_DG4::WT_v",
    "ML_WaveToy_DG4::WT_vrhs"};
  AssertGroupStorage(cctkGH, "ML_WaveToy_DG4_RHSFirstOrder", 7, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_WaveToy_DG4_RHSFirstOrder_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_WaveToy_DG4_RHSFirstOrder_Body");
  }
}

} // namespace ML_WaveToy_DG4
