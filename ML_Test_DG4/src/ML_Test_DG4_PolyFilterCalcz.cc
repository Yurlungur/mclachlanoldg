/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_Test_DG4 {

extern "C" void ML_Test_DG4_PolyFilterCalcz_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_Test_DG4_PolyFilterCalcz_calc_every != ML_Test_DG4_PolyFilterCalcz_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_p0Trunc","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_p0Trunc.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_p1Trunc","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_p1Trunc.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_p2Trunc","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_p2Trunc.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_p3Trunc","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_p3Trunc.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_p4Trunc","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_p4Trunc.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_p5Trunc","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_p5Trunc.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_p6Trunc","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_p6Trunc.");
  return;
}

static void ML_Test_DG4_PolyFilterCalcz_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_Test_DG4_PolyFilterCalcz,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL p0TempL CCTK_ATTRIBUTE_UNUSED = p0Temp[index];
    CCTK_REAL p1TempL CCTK_ATTRIBUTE_UNUSED = p1Temp[index];
    CCTK_REAL p2TempL CCTK_ATTRIBUTE_UNUSED = p2Temp[index];
    CCTK_REAL p3TempL CCTK_ATTRIBUTE_UNUSED = p3Temp[index];
    CCTK_REAL p4TempL CCTK_ATTRIBUTE_UNUSED = p4Temp[index];
    CCTK_REAL p5TempL CCTK_ATTRIBUTE_UNUSED = p5Temp[index];
    CCTK_REAL p6TempL CCTK_ATTRIBUTE_UNUSED = p6Temp[index];
    
    
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL p0TruncL CCTK_ATTRIBUTE_UNUSED = IfThen(tk == 
      0,0.8*GFOffset(p0Temp,0,0,0) - 
      0.533333333333333333333333333333*GFOffset(p0Temp,0,0,2) + 
      0.466666666666666666666666666667*(GFOffset(p0Temp,0,0,1) + 
      GFOffset(p0Temp,0,0,3)) - 0.2*GFOffset(p0Temp,0,0,4),0) + IfThen(tk == 
      1,0.8*GFOffset(p0Temp,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(p0Temp,0,0,1) - 
      0.2*GFOffset(p0Temp,0,0,2) + 
      0.0857142857142857142857142857143*(GFOffset(p0Temp,0,0,-1) + 
      GFOffset(p0Temp,0,0,3)),0) + IfThen(tk == 2,0.8*GFOffset(p0Temp,0,0,0) 
      + 0.175*(GFOffset(p0Temp,0,0,-1) + GFOffset(p0Temp,0,0,1)) - 
      0.075*(GFOffset(p0Temp,0,0,-2) + GFOffset(p0Temp,0,0,2)),0) + IfThen(tk 
      == 3,-0.2*GFOffset(p0Temp,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(p0Temp,0,0,-1) + 
      0.8*GFOffset(p0Temp,0,0,0) + 
      0.0857142857142857142857142857143*(GFOffset(p0Temp,0,0,-3) + 
      GFOffset(p0Temp,0,0,1)),0) + IfThen(tk == 
      4,-0.2*GFOffset(p0Temp,0,0,-4) - 
      0.533333333333333333333333333333*GFOffset(p0Temp,0,0,-2) + 
      0.466666666666666666666666666667*(GFOffset(p0Temp,0,0,-3) + 
      GFOffset(p0Temp,0,0,-1)) + 0.8*GFOffset(p0Temp,0,0,0),0);
    
    CCTK_REAL p1TruncL CCTK_ATTRIBUTE_UNUSED = IfThen(tk == 
      0,0.8*GFOffset(p1Temp,0,0,0) - 
      0.533333333333333333333333333333*GFOffset(p1Temp,0,0,2) + 
      0.466666666666666666666666666667*(GFOffset(p1Temp,0,0,1) + 
      GFOffset(p1Temp,0,0,3)) - 0.2*GFOffset(p1Temp,0,0,4),0) + IfThen(tk == 
      1,0.8*GFOffset(p1Temp,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(p1Temp,0,0,1) - 
      0.2*GFOffset(p1Temp,0,0,2) + 
      0.0857142857142857142857142857143*(GFOffset(p1Temp,0,0,-1) + 
      GFOffset(p1Temp,0,0,3)),0) + IfThen(tk == 2,0.8*GFOffset(p1Temp,0,0,0) 
      + 0.175*(GFOffset(p1Temp,0,0,-1) + GFOffset(p1Temp,0,0,1)) - 
      0.075*(GFOffset(p1Temp,0,0,-2) + GFOffset(p1Temp,0,0,2)),0) + IfThen(tk 
      == 3,-0.2*GFOffset(p1Temp,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(p1Temp,0,0,-1) + 
      0.8*GFOffset(p1Temp,0,0,0) + 
      0.0857142857142857142857142857143*(GFOffset(p1Temp,0,0,-3) + 
      GFOffset(p1Temp,0,0,1)),0) + IfThen(tk == 
      4,-0.2*GFOffset(p1Temp,0,0,-4) - 
      0.533333333333333333333333333333*GFOffset(p1Temp,0,0,-2) + 
      0.466666666666666666666666666667*(GFOffset(p1Temp,0,0,-3) + 
      GFOffset(p1Temp,0,0,-1)) + 0.8*GFOffset(p1Temp,0,0,0),0);
    
    CCTK_REAL p2TruncL CCTK_ATTRIBUTE_UNUSED = IfThen(tk == 
      0,0.8*GFOffset(p2Temp,0,0,0) - 
      0.533333333333333333333333333333*GFOffset(p2Temp,0,0,2) + 
      0.466666666666666666666666666667*(GFOffset(p2Temp,0,0,1) + 
      GFOffset(p2Temp,0,0,3)) - 0.2*GFOffset(p2Temp,0,0,4),0) + IfThen(tk == 
      1,0.8*GFOffset(p2Temp,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(p2Temp,0,0,1) - 
      0.2*GFOffset(p2Temp,0,0,2) + 
      0.0857142857142857142857142857143*(GFOffset(p2Temp,0,0,-1) + 
      GFOffset(p2Temp,0,0,3)),0) + IfThen(tk == 2,0.8*GFOffset(p2Temp,0,0,0) 
      + 0.175*(GFOffset(p2Temp,0,0,-1) + GFOffset(p2Temp,0,0,1)) - 
      0.075*(GFOffset(p2Temp,0,0,-2) + GFOffset(p2Temp,0,0,2)),0) + IfThen(tk 
      == 3,-0.2*GFOffset(p2Temp,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(p2Temp,0,0,-1) + 
      0.8*GFOffset(p2Temp,0,0,0) + 
      0.0857142857142857142857142857143*(GFOffset(p2Temp,0,0,-3) + 
      GFOffset(p2Temp,0,0,1)),0) + IfThen(tk == 
      4,-0.2*GFOffset(p2Temp,0,0,-4) - 
      0.533333333333333333333333333333*GFOffset(p2Temp,0,0,-2) + 
      0.466666666666666666666666666667*(GFOffset(p2Temp,0,0,-3) + 
      GFOffset(p2Temp,0,0,-1)) + 0.8*GFOffset(p2Temp,0,0,0),0);
    
    CCTK_REAL p3TruncL CCTK_ATTRIBUTE_UNUSED = IfThen(tk == 
      0,0.8*GFOffset(p3Temp,0,0,0) - 
      0.533333333333333333333333333333*GFOffset(p3Temp,0,0,2) + 
      0.466666666666666666666666666667*(GFOffset(p3Temp,0,0,1) + 
      GFOffset(p3Temp,0,0,3)) - 0.2*GFOffset(p3Temp,0,0,4),0) + IfThen(tk == 
      1,0.8*GFOffset(p3Temp,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(p3Temp,0,0,1) - 
      0.2*GFOffset(p3Temp,0,0,2) + 
      0.0857142857142857142857142857143*(GFOffset(p3Temp,0,0,-1) + 
      GFOffset(p3Temp,0,0,3)),0) + IfThen(tk == 2,0.8*GFOffset(p3Temp,0,0,0) 
      + 0.175*(GFOffset(p3Temp,0,0,-1) + GFOffset(p3Temp,0,0,1)) - 
      0.075*(GFOffset(p3Temp,0,0,-2) + GFOffset(p3Temp,0,0,2)),0) + IfThen(tk 
      == 3,-0.2*GFOffset(p3Temp,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(p3Temp,0,0,-1) + 
      0.8*GFOffset(p3Temp,0,0,0) + 
      0.0857142857142857142857142857143*(GFOffset(p3Temp,0,0,-3) + 
      GFOffset(p3Temp,0,0,1)),0) + IfThen(tk == 
      4,-0.2*GFOffset(p3Temp,0,0,-4) - 
      0.533333333333333333333333333333*GFOffset(p3Temp,0,0,-2) + 
      0.466666666666666666666666666667*(GFOffset(p3Temp,0,0,-3) + 
      GFOffset(p3Temp,0,0,-1)) + 0.8*GFOffset(p3Temp,0,0,0),0);
    
    CCTK_REAL p4TruncL CCTK_ATTRIBUTE_UNUSED = IfThen(tk == 
      0,0.8*GFOffset(p4Temp,0,0,0) - 
      0.533333333333333333333333333333*GFOffset(p4Temp,0,0,2) + 
      0.466666666666666666666666666667*(GFOffset(p4Temp,0,0,1) + 
      GFOffset(p4Temp,0,0,3)) - 0.2*GFOffset(p4Temp,0,0,4),0) + IfThen(tk == 
      1,0.8*GFOffset(p4Temp,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(p4Temp,0,0,1) - 
      0.2*GFOffset(p4Temp,0,0,2) + 
      0.0857142857142857142857142857143*(GFOffset(p4Temp,0,0,-1) + 
      GFOffset(p4Temp,0,0,3)),0) + IfThen(tk == 2,0.8*GFOffset(p4Temp,0,0,0) 
      + 0.175*(GFOffset(p4Temp,0,0,-1) + GFOffset(p4Temp,0,0,1)) - 
      0.075*(GFOffset(p4Temp,0,0,-2) + GFOffset(p4Temp,0,0,2)),0) + IfThen(tk 
      == 3,-0.2*GFOffset(p4Temp,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(p4Temp,0,0,-1) + 
      0.8*GFOffset(p4Temp,0,0,0) + 
      0.0857142857142857142857142857143*(GFOffset(p4Temp,0,0,-3) + 
      GFOffset(p4Temp,0,0,1)),0) + IfThen(tk == 
      4,-0.2*GFOffset(p4Temp,0,0,-4) - 
      0.533333333333333333333333333333*GFOffset(p4Temp,0,0,-2) + 
      0.466666666666666666666666666667*(GFOffset(p4Temp,0,0,-3) + 
      GFOffset(p4Temp,0,0,-1)) + 0.8*GFOffset(p4Temp,0,0,0),0);
    
    CCTK_REAL p5TruncL CCTK_ATTRIBUTE_UNUSED = IfThen(tk == 
      0,0.8*GFOffset(p5Temp,0,0,0) - 
      0.533333333333333333333333333333*GFOffset(p5Temp,0,0,2) + 
      0.466666666666666666666666666667*(GFOffset(p5Temp,0,0,1) + 
      GFOffset(p5Temp,0,0,3)) - 0.2*GFOffset(p5Temp,0,0,4),0) + IfThen(tk == 
      1,0.8*GFOffset(p5Temp,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(p5Temp,0,0,1) - 
      0.2*GFOffset(p5Temp,0,0,2) + 
      0.0857142857142857142857142857143*(GFOffset(p5Temp,0,0,-1) + 
      GFOffset(p5Temp,0,0,3)),0) + IfThen(tk == 2,0.8*GFOffset(p5Temp,0,0,0) 
      + 0.175*(GFOffset(p5Temp,0,0,-1) + GFOffset(p5Temp,0,0,1)) - 
      0.075*(GFOffset(p5Temp,0,0,-2) + GFOffset(p5Temp,0,0,2)),0) + IfThen(tk 
      == 3,-0.2*GFOffset(p5Temp,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(p5Temp,0,0,-1) + 
      0.8*GFOffset(p5Temp,0,0,0) + 
      0.0857142857142857142857142857143*(GFOffset(p5Temp,0,0,-3) + 
      GFOffset(p5Temp,0,0,1)),0) + IfThen(tk == 
      4,-0.2*GFOffset(p5Temp,0,0,-4) - 
      0.533333333333333333333333333333*GFOffset(p5Temp,0,0,-2) + 
      0.466666666666666666666666666667*(GFOffset(p5Temp,0,0,-3) + 
      GFOffset(p5Temp,0,0,-1)) + 0.8*GFOffset(p5Temp,0,0,0),0);
    
    CCTK_REAL p6TruncL CCTK_ATTRIBUTE_UNUSED = IfThen(tk == 
      0,0.8*GFOffset(p6Temp,0,0,0) - 
      0.533333333333333333333333333333*GFOffset(p6Temp,0,0,2) + 
      0.466666666666666666666666666667*(GFOffset(p6Temp,0,0,1) + 
      GFOffset(p6Temp,0,0,3)) - 0.2*GFOffset(p6Temp,0,0,4),0) + IfThen(tk == 
      1,0.8*GFOffset(p6Temp,0,0,0) + 
      0.228571428571428571428571428571*GFOffset(p6Temp,0,0,1) - 
      0.2*GFOffset(p6Temp,0,0,2) + 
      0.0857142857142857142857142857143*(GFOffset(p6Temp,0,0,-1) + 
      GFOffset(p6Temp,0,0,3)),0) + IfThen(tk == 2,0.8*GFOffset(p6Temp,0,0,0) 
      + 0.175*(GFOffset(p6Temp,0,0,-1) + GFOffset(p6Temp,0,0,1)) - 
      0.075*(GFOffset(p6Temp,0,0,-2) + GFOffset(p6Temp,0,0,2)),0) + IfThen(tk 
      == 3,-0.2*GFOffset(p6Temp,0,0,-2) + 
      0.228571428571428571428571428571*GFOffset(p6Temp,0,0,-1) + 
      0.8*GFOffset(p6Temp,0,0,0) + 
      0.0857142857142857142857142857143*(GFOffset(p6Temp,0,0,-3) + 
      GFOffset(p6Temp,0,0,1)),0) + IfThen(tk == 
      4,-0.2*GFOffset(p6Temp,0,0,-4) - 
      0.533333333333333333333333333333*GFOffset(p6Temp,0,0,-2) + 
      0.466666666666666666666666666667*(GFOffset(p6Temp,0,0,-3) + 
      GFOffset(p6Temp,0,0,-1)) + 0.8*GFOffset(p6Temp,0,0,0),0);
    /* Copy local copies back to grid functions */
    p0Trunc[index] = p0TruncL;
    p1Trunc[index] = p1TruncL;
    p2Trunc[index] = p2TruncL;
    p3Trunc[index] = p3TruncL;
    p4Trunc[index] = p4TruncL;
    p5Trunc[index] = p5TruncL;
    p6Trunc[index] = p6TruncL;
  }
  CCTK_ENDLOOP3(ML_Test_DG4_PolyFilterCalcz);
}
extern "C" void ML_Test_DG4_PolyFilterCalcz(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_Test_DG4_PolyFilterCalcz_Body");
  }
  if (cctk_iteration % ML_Test_DG4_PolyFilterCalcz_calc_every != ML_Test_DG4_PolyFilterCalcz_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_Test_DG4::WT_p0Temp",
    "ML_Test_DG4::WT_p0Trunc",
    "ML_Test_DG4::WT_p1Temp",
    "ML_Test_DG4::WT_p1Trunc",
    "ML_Test_DG4::WT_p2Temp",
    "ML_Test_DG4::WT_p2Trunc",
    "ML_Test_DG4::WT_p3Temp",
    "ML_Test_DG4::WT_p3Trunc",
    "ML_Test_DG4::WT_p4Temp",
    "ML_Test_DG4::WT_p4Trunc",
    "ML_Test_DG4::WT_p5Temp",
    "ML_Test_DG4::WT_p5Trunc",
    "ML_Test_DG4::WT_p6Temp",
    "ML_Test_DG4::WT_p6Trunc"};
  AssertGroupStorage(cctkGH, "ML_Test_DG4_PolyFilterCalcz", 14, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_Test_DG4_PolyFilterCalcz_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_Test_DG4_PolyFilterCalcz_Body");
  }
}

} // namespace ML_Test_DG4
