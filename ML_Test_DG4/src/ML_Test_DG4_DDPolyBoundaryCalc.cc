/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_Test_DG4 {

extern "C" void ML_Test_DG4_DDPolyBoundaryCalc_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_Test_DG4_DDPolyBoundaryCalc_calc_every != ML_Test_DG4_DDPolyBoundaryCalc_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_ddp0","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_ddp0.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_ddp1","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_ddp1.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_ddp2","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_ddp2.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_ddp3","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_ddp3.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_ddp4","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_ddp4.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_ddp5","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_ddp5.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Test_DG4::WT_ddp6","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Test_DG4::WT_ddp6.");
  return;
}

static void ML_Test_DG4_DDPolyBoundaryCalc_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_Test_DG4_DDPolyBoundaryCalc,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    
    
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL ddp011L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp012L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp013L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp022L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp023L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp033L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp111L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp112L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp113L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp122L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp123L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp133L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp211L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp212L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp213L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp222L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp223L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp233L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp311L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp312L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp313L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp322L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp323L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp333L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp411L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp412L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp413L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp422L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp423L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp433L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp511L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp512L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp513L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp522L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp523L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp533L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp611L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp612L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp613L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp622L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp623L CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL ddp633L CCTK_ATTRIBUTE_UNUSED = 0;
    /* Copy local copies back to grid functions */
    ddp011[index] = ddp011L;
    ddp012[index] = ddp012L;
    ddp013[index] = ddp013L;
    ddp022[index] = ddp022L;
    ddp023[index] = ddp023L;
    ddp033[index] = ddp033L;
    ddp111[index] = ddp111L;
    ddp112[index] = ddp112L;
    ddp113[index] = ddp113L;
    ddp122[index] = ddp122L;
    ddp123[index] = ddp123L;
    ddp133[index] = ddp133L;
    ddp211[index] = ddp211L;
    ddp212[index] = ddp212L;
    ddp213[index] = ddp213L;
    ddp222[index] = ddp222L;
    ddp223[index] = ddp223L;
    ddp233[index] = ddp233L;
    ddp311[index] = ddp311L;
    ddp312[index] = ddp312L;
    ddp313[index] = ddp313L;
    ddp322[index] = ddp322L;
    ddp323[index] = ddp323L;
    ddp333[index] = ddp333L;
    ddp411[index] = ddp411L;
    ddp412[index] = ddp412L;
    ddp413[index] = ddp413L;
    ddp422[index] = ddp422L;
    ddp423[index] = ddp423L;
    ddp433[index] = ddp433L;
    ddp511[index] = ddp511L;
    ddp512[index] = ddp512L;
    ddp513[index] = ddp513L;
    ddp522[index] = ddp522L;
    ddp523[index] = ddp523L;
    ddp533[index] = ddp533L;
    ddp611[index] = ddp611L;
    ddp612[index] = ddp612L;
    ddp613[index] = ddp613L;
    ddp622[index] = ddp622L;
    ddp623[index] = ddp623L;
    ddp633[index] = ddp633L;
  }
  CCTK_ENDLOOP3(ML_Test_DG4_DDPolyBoundaryCalc);
}
extern "C" void ML_Test_DG4_DDPolyBoundaryCalc(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_Test_DG4_DDPolyBoundaryCalc_Body");
  }
  if (cctk_iteration % ML_Test_DG4_DDPolyBoundaryCalc_calc_every != ML_Test_DG4_DDPolyBoundaryCalc_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_Test_DG4::WT_ddp0",
    "ML_Test_DG4::WT_ddp1",
    "ML_Test_DG4::WT_ddp2",
    "ML_Test_DG4::WT_ddp3",
    "ML_Test_DG4::WT_ddp4",
    "ML_Test_DG4::WT_ddp5",
    "ML_Test_DG4::WT_ddp6"};
  AssertGroupStorage(cctkGH, "ML_Test_DG4_DDPolyBoundaryCalc", 7, groups);
  
  
  LoopOverBoundary(cctkGH, ML_Test_DG4_DDPolyBoundaryCalc_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_Test_DG4_DDPolyBoundaryCalc_Body");
  }
}

} // namespace ML_Test_DG4
