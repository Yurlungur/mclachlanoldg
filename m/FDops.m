(* FD Derivatives *)

(* TODO: check that "order" is set and integer and positive and even *)

GFOffsetDir[u_, a_, i_] := ReplacePart[GFOffset[u,0,0,0], {a+1->i}];

diffOpFromStencil[op_, u_] :=
  Simplify[Expand[op GFOffset[u,0,0,0]
                  /. {shift[a_]^i_ :> GFOffsetDir[u,a,i]}
                  /. {shift[a_] :> GFOffsetDir[u,a,1]}]
           //. {GFOffset[u,i_,j_,k_] GFOffset[u,l_,m_,n_] :>
                GFOffset[u,i+l,j+m,k+n]}
           /. {spacing[a_] :> {dx,dy,dz}[[a]]}];

SCDO = StandardCenteredDifferenceOperator;



Node[u_, a_Integer] :=
  ({cctkOriginSpace1,cctkOriginSpace2,cctkOriginSpace3}[[a]] +
   {dx,dy,dz}[[a]] * {cctkLbnd1+i,cctkLbnd2+j,cctkLbnd3+k}[[a]]);

Weight[u_, a_] = 1;
Weight[u_] = Weight[u,1] Weight[u,2] Weight[u,3];

(* use Deriv instead of PD since PD is special in Kranc *)
Deriv[u_, a_Integer] := diffOpFromStencil[SCDO[1, order/2, a], u];
Deriv[u_, a_Integer, a_Integer] := diffOpFromStencil[SCDO[2, order/2, a], u];
Deriv[u_, a_Integer, b_Integer] := diffOpFromStencil[SCDO[1, order/2, a]
                                                     SCDO[1, order/2, b], u];


(* order2 = p+1, where p is dissipation order *)
(* Should be equivalent to Kreiss-Oliger dissipation*)
(*
   formula should be:
   (-1)^((p+3)/2) epsDiss (1/2)^(p+1) h^p (d^(p+1)/d x^(p+1)) u

   TODO: Is this the most efficient way of writing this operator?
 *)
order2 = (Floor[order, 2]+2);

Diss[u_] :=
    ((-1)^(order2/2+1) (epsDiss/(2^order2))
     (dx^(order2-1) diffOpFromStencil[SCDO[order2,order2/2,1],u] +
      dy^(order2-1) diffOpFromStencil[SCDO[order2,order2/2,2],u] +
      dz^(order2-1) diffOpFromStencil[SCDO[order2,order2/2,3],u]));

(*
   This is Erik's implementation. I had trouble with it. ~JM
   
Diss[u_] :=
    ((-1)^(order2/2+1) (epsDiss/(2^order2))
     diffOpFromStencil[spacing[1]^(order2-1) SCDO[order2,order2/2,1] +
		       spacing[2]^(order2-1) SCDO[order2,order2/2,2] +
		       spacing[3]^(order2-1) SCDO[order2,order2/2,3], u]);
*)

(* A truncation operator. No truncation is performed. *)
(*
   TODO: Optimize away truncation operation
   when doing FD calculations
 *)
Trunc[u_,a_Integer] := u;
Trunc[u_,a_Integer,b_Integer] := u;
Trunc[u_,a_Integer, b_Integer, c_Integer] := u;
Trunc[u_] := u;