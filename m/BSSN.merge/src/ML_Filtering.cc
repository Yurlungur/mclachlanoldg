#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cassert>

extern "C"
void @THORN_NAME@_ConjugateFilterSelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int ierr;
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "@THORN_NAME@::ML_dtlapse", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "@THORN_NAME@::ML_dtshift", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "@THORN_NAME@::ML_trace_curv", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "@THORN_NAME@::ML_cons_Gamma", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "@THORN_NAME@::ML_curv", "none");
  assert(!ierr);
}
