#include <Slicing.h>

#include <cctk.h>

extern "C"
int @THORN_NAME@_RegisterSlicing(void)
{
  Einstein_RegisterSlicing("@THORN_NAME@");
  return 0;
}
