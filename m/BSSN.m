(* Authors:
   Jonah Miller <jmiller@perimeterinstitute.ca>
   Erik Schnetter <eschnetter@perimeterinstitute.ca>
   *)

(* Boilerplate setup *)

SetDebugLevel[InfoFull];
SetEnhancedTimes[False];
SetSourceLanguage["C"];

(******************************************************************************)
(* Various Mathematica definitions that may depend on Kranc, but which
   are independent of McLachlan *)
(******************************************************************************)

(* Define a tensor and declare its symmetries *)
(* Note: We don't want to use (or write down) an expression such as
   g[la,lb] before g has been declared as tensor, since g[la,lb] is
   then apparently not interpreted correctly. Therefore, tensor name
   and index list are passed separately. *)
(* Note: syms is a list of symmetries, where each symmetry is a list
   of two indices. *)
(* Example call: DefineTensor1[g, {la,lb}, {{la,lb}}] *)
DefineTensor1[name_, indices_:{}, syms_:{}] :=
  Module[{tensor},
         DefineTensor[name];
         tensor = If[indices=={}, name, name[Sequence@@indices]];
         AssertSymmetricIncreasing[tensor, Sequence@@#]& /@ syms;
         tensor];

(* Define a group and declare its name *)
DefineGroup1[name_, tensor_, timelevels_:1] :=
  Module[{group},
         group = CreateGroupFromTensor[tensor];
         group = AddGroupExtra[group, Timelevels -> timelevels];
         group = SetGroupName[group, name];
         group];

(* Split a calculation *)
(* calc:    calculation
   suffix:  suffix to be added to the original name
   updates: keyword-value pairs that should be added to or replaced in
            the calculation
   vars:    left hand sides of the equations that should be kept
            (together with their dependencies) *)
PartialCalculation[calc_, suffix_, updates_, vars_] :=
  Module[
    {name, calc1, replacements, calc2, vars1, patterns, eqs, calc3},
    (* Add suffix to name *)
    name  = lookupDefault[calc, Name, ""] <> suffix;
    calc1 = mapReplaceAdd[calc, Name, name];
    (* Replace some entries in the calculation *)
    replacements = updates //. (lhs_ -> rhs_) -> (mapReplaceAdd[#, lhs, rhs]&);
    calc2        = calc1 // Composition@@replacements;
    (* Remove unnecessary equations *)
    vars1    = Join[vars, lookupDefault[calc2, Shorthands, {}]];
    patterns = Replace[vars1, {Tensor[n_,__]      ->     Tensor[n,__] ,
                               dot[Tensor[n_,__]] -> dot[Tensor[n,__]]}, 1];
    eqs      = FilterRules[lookup[calc, Equations], patterns];
    calc3    = mapReplace[calc2, Equations, eqs];
    calc3];

(******************************************************************************)
(* Choose code to generate *)
(******************************************************************************)

Switch[Environment["ML_DERIVATIVES"],
       "FD", derivs=FD,
       "DG", derivs=DG,
       _, ThrowError["Environment variable ML_DERIVATIVES has wrong value '" <>
                     Environment["ML_DERIVATIVES"] <> "'"]];

(* Polynomial order *)
order = ToExpression[Environment["ML_ORDER"]];
(* TODO: Check that order is a positive integer *)

prefix = "ML_";
name = Environment["ML_CODE"];
thorn = prefix <> name <> "_" <> ToString[derivs] <> ToString[order];

(* default settings *)
useOpenCL  = False;
useVectors = False;

evolveAKranc = evolveA;
evolveBKranc = evolveB;

addMatterKranc = 1;

addDissipationKranc = 1;

maxTimelevels = 3;

(* NOTE: Variables named "xxxKranc" ared used by Kranc at build time.
   They can either be set to a particular value, and Kranc will then
   generate specialized code. They can also be set to "xxx" where
   "xxx" needs to be a parameter declared below; in this case, Kranc
   will generate generic code that makes this choice at run time. *)
Switch[name,
       
       (* Standard: a generic BSSN implementation *)
       "BSSN",
       formulationKranc = fBSSN;
       conformalMethodKranc = conformalMethod,
       
       (* Benchmark: A highly optimized version of BSSN *)
       "BSSN_bench",
       formulationKranc = fBSSN;
       conformalMethodKranc = conformalMethod;
       evolveAKranc = 0;
       evolveBKranc = 1,
       
       (* OpenCL: OpenCL code is optimized at run time depending on
          parameter settings; we therefore choose very generic
          parameter settings *)
       "BSSN_CL",
       formulationKranc = formulation;
       conformalMethodKranc = conformalMethod;
       useOpenCL = True,
       
       (* No Vectorization: This is mostly for debugging. Test as much
          as possible, by choosing generic parameter settings *)
       "BSSN_NV",
       formulationKranc = formulation;
       conformalMethodKranc = conformalMethod;
       useVectors = False,
       
       "CCZ4",
       formulationKranc = fCCZ4;
       (* TODO: is this correct? see e.g. Ricci tensor *)
       conformalMethodKranc = cmW];

(******************************************************************************)
(* Derivatives *)
(******************************************************************************)

Switch[derivs,
       FD, Get["FDops.m"],
       DG, Get["DGops.m"]];

(*
   In the DG formulation we have two notions of derivative, which
   are applied to variables depending on how they correspond to
   variables in the second order wave equation

   du/dt = rho
   drho/dt = d^2u/dx^2

   spatial derivatives of rho (i.e., drho/dx) are calculated using
   DerivTrunc

   spatial derivatives of all other terms (i.e., du/dx)
   are calculated using Deriv.

   Analogously, the following BSSN derivatices are calculated
   using DerivTrunc:
   A, B, trK, Kt, Xt, Theta
 *)
(*If[derivs == FD,
   TruncDeriv = Deriv];
*)
(* Upwind derivatives: Currently same as non-upwind derivatives *)
DerivUpw = Deriv;
(*TruncDerivUpw = TruncDeriv;*)


LDname[u_Symbol] := Symbol["LD"<>ToString[u]];
LD[Tensor[u_Symbol,is___],il_] := Tensor[LDname[u],is,il];
LD[u_Symbol,il_] := Tensor[LDname[u],il];

LDuname[u_Symbol] := Symbol["LDu"<>ToString[u]];
LDu[Tensor[u_Symbol,is___],il_] := Tensor[LDuname[u],is,il];
LDu[u_Symbol,il_] := Tensor[LDuname[u],il];

LDissName[u_Symbol] := Symbol["LDiss" <> ToString[u]];
LDiss[Tensor[u_Symbol, is___]] := Tensor[LDissName[u], is];
LDiss[u_Symbol] := LDissName[u];

(* Convert a local to a global derivative *)
LD2PD[u_,il_] := IfThen[usejacobian, J[uq,il] LD[u,lq], KD[uq,il] LD[u,lq]];
LDu2PDu[u_,il_] := IfThen[usejacobian, J[uq,il] LDu[u,lq], KD[uq,il] LDu[u,lq]];
LDiss2GDiss[u_] := IfThen[usejacobian, myDetJ LDiss[u], LDiss[u]];

PDname[u_Symbol] := Symbol["PD"<>ToString[u]];
PD[Tensor[u_Symbol,is___],il_] := Tensor[PDname[u],is,il];
PD[u_Symbol,il_] := Tensor[PDname[u],il];
PD[u_,il_,jl_] := PD[PD[u,il],jl];

PDuname[u_Symbol] := Symbol["PDu"<>ToString[u]];
PDu[Tensor[u_Symbol,is___],il_] := Tensor[PDuname[u],is,il];
PDu[u_Symbol,il_] := Tensor[PDuname[u],il];

GDissName[u_Symbol] := Symbol["GDiss" <> ToString[u]];
GDiss[Tensor[u_Symbol, is___]] := Tensor[GDissName[u], is];
GDiss[u_Symbol] := GDissName[u];

(* Upwind derivatives: Currently same as non-upwind derivatives *)
PDua[u_,il_] := PDu[u,il];
PDus[u_,il_] := 0;

Upwind[dir_, u_,il_] := dir PDua[u,il] + Abs[dir] PDus[u,il];

(*
   LDiss = dissipation without volume form
   GDiss = dissipation with volume form
 *)
Dissipation[u_] := Diss[u];



(******************************************************************************)
(* Global choices (e.g. for formulations) *)
(******************************************************************************)

(* Global choices are evaluated either at Kranc time, or at run time
   via a Cactus parameter. They cannot differ for different grid
   points. Mathematica variables with the suffix "Kranc" make
   Kranc-time choices. These variables are either set to a small
   integer, determining the choice at Kranc time, or are set to a
   Cactus parameter name, delaying the choice until run time. *)

(* In the If expressions below, the Unevaluated empty Sequence is even
   "less empty" than Null (the empty argument). This is convenient for
   eating a preceding comma: The expression
      { phi->0, IfCCZ4[Theta->0] }
   becomes either
      { phi->0, Theta->0 }
   or
      { phi->0 }
   (note that there is no trailing comma). *)

(* Choices that can be either Kranc time or run time *)

(* Formulation (BSSN or CCZ4) *)
fBSSN=0; fCCZ4=1;
IfCCZ4[expr_] :=
  Which[formulationKranc === fBSSN, Unevaluated[Sequence[]],
        True, expr];
IfCCZ4[expr_, else_] :=
  Which[formulationKranc === fCCZ4, expr,
        formulationKranc === fBSSN, else,
        True, IfThen[formulationKranc != fBSSN, expr, else]];

(* Conformal treatment (phi or W) *)
(* This could be made a run-time choice *)
cmPhi=0; cmW=1;
IfW[expr_, else_] :=
  Which[conformalMethodKranc === cmW, expr,
        conformalMethodKranc === cmPhi, else,
        True, IfThen[conformalMethodKranc != cmPhi, expr, else]];

(* Include matter terms? *)
IfMatter[expr_] :=
  Which[addMatterKranc === 0, Unevaluated[Sequence[]],
        True, expr];
IfMatter[expr_, else_] :=
  Which[addMatterKranc === 1, expr,
        addMatterKranc === 0, else,
        True, IfThen[addMatterKranc != 0, expr, else]];

(* Is A (time derivative of lapse) evolved? *)
IfA[expr_] :=
  Which[evolveAKranc === 0, Unevaluated[Sequence[]],
        True, expr];
IfA[expr_, else_] :=
  Which[evolveAKranc === 1, expr,
        evolveAKranc === 0, else,
        True, IfThen[evolveAKranc != 0, expr, else]];

(* Is B^i (time derivative of shift) evolved? *)
IfB[expr_] :=
  Which[evolveBKranc === 0, Unevaluated[Sequence[]],
        True, expr];
IfB[expr_, else_] :=
  Which[evolveBKranc === 1, expr,
        evolveBKranc === 0, else,
        True, IfThen[evolveBKranc != 0, expr, else]];

(* Is dissipation added? *)
IfDiss[expr_, else_] :=
  Which[addDissipationKranc === 0, else,
        True, expr];

(* Shift formulation *)
shiftGammaDriver=0; shiftHarmonic=1;
(*
ShiftChoice[exprGammaDriver_, exprHarmonic_] :=
  IfThen[shiftFormulation == shiftGammaDriver, exprGammaDriver, exprHarmonic];
*)

(******************************************************************************)
(* Declare tensors *)
(******************************************************************************)

(* Rename some Cactus grid functions *)

(* CartGrid3D coordinates *)
x1=x; x2=y; x3=z;

(* ADMBase variables *)
admg11=gxx; admg12=gxy; admg22=gyy; admg13=gxz; admg23=gyz; admg33=gzz;
admK11=kxx; admK12=kxy; admK22=kyy; admK13=kxz; admK23=kyz; admK33=kzz;
admalpha=alp;
admdtalpha=dtalp;
admbeta1=betax; admbeta2=betay; admbeta3=betaz;
admdtbeta1=dtbetax; admdtbeta2=dtbetay; admdtbeta3=dtbetaz;

(* TmunuBase variables *)
T00=eTtt;
T01=eTtx; T02=eTty; T03=eTtz;
T11=eTxx; T12=eTxy; T22=eTyy; T13=eTxz; T23=eTyz; T33=eTzz;

DefineTensor1[dir, {ua}];

(* The weight function for CarpetReduce. The CarpetReduce weight must
   be modified by the integration weights given by the Gauss-Lobatto
   quadrature points. *)
DefineTensor1[weight]

(* BSSN/CCZ4 state vector *)
DefineTensor1[phiW];
DefineTensor1[gt, {la,lb}, {{la,lb}}];
DefineTensor1[Xt, {ua}];
DefineTensor1[trK];
DefineTensor1[At, {la,lb}, {{la,lb}}];
DefineTensor1[Theta];

DefineTensor1[alpha];
DefineTensor1[A];
DefineTensor1[beta, {ua}];
DefineTensor1[B, {ua}];

(* local and partial derivatives *)
DefineTensor1[LDname[phiW], {lq}];
DefineTensor1[LDname[gt], {la,lb,lq}, {{la,lb}}];
DefineTensor1[LDname[Xt], {ua,lq}];
DefineTensor1[LDname[trK], {lq}];
DefineTensor1[LDname[At], {la,lb,lq}];
DefineTensor1[LDname[alpha], {lq}];
DefineTensor1[LDname[beta], {ua,lq}];
DefineTensor1[PDname[phiW], {lq}];
DefineTensor1[PDname[gt], {la,lb,lc}, {{la,lb}}];
DefineTensor1[PDname[Xt], {ua,lb}];
DefineTensor1[PDname[trK], {la}];
DefineTensor1[LDname[At], {la,lb,lc}];
DefineTensor1[PDname[alpha], {la}];
DefineTensor1[PDname[beta], {ua,lb}];

DefineTensor1[phiWTmp];
DefineTensor1[gtTmp, {la,lb}, {{la,lb}}];
DefineTensor1[alphaTmp];
DefineTensor1[betaTmp, {ua}];
DefineTensor1[PDname[phiWTmp], {lq}];
DefineTensor1[PDname[gtTmp], {la,lb,lc}, {{la,lb}}];
DefineTensor1[PDname[alphaTmp], {la}];
DefineTensor1[PDname[betaTmp], {ua,lb}];

DefineTensor1[LDname[PD[phiW,la]], {lq}];
DefineTensor1[LDname[PD[gt[la,lb],lc]], {lq}];
DefineTensor1[LDname[PD[alpha,la]], {lq}];
DefineTensor1[LDname[PD[beta[ua],lb]], {lq}];
DefineTensor1[PDname[PD[phiW,la]], {lb}, {{la,lb}}];
DefineTensor1[PDname[PD[gt[la,lb],lc]], {ld}, {{la,lb}, {lc,ld}}];
DefineTensor1[PDname[PD[alpha,la]], {lb}, {{la,lb}}];
DefineTensor1[PDname[PD[beta[ua],lb]], {lc}, {{lb,lc}}];

(* local and partial intermediate derivatives *)
DefineTensor1[LDuname[phiW], {lq}];
DefineTensor1[LDuname[gt], {la,lb,lq}, {{la,lb}}];
DefineTensor1[LDuname[Xt], {ua,lq}];
DefineTensor1[LDuname[trK], {lq}];
DefineTensor1[LDuname[At], {la,lb,lq}, {{la,lb}}];
DefineTensor1[LDuname[Theta], {lq}];
DefineTensor1[LDuname[alpha], {lq}];
DefineTensor1[LDuname[A], {lq}];
DefineTensor1[LDuname[beta], {ua,lq}];
DefineTensor1[LDuname[B], {ua,lq}];
DefineTensor1[PDuname[phiW], {la}];
DefineTensor1[PDuname[gt], {la,lb,lc}, {{la,lb}}];
DefineTensor1[PDuname[Xt], {ua,lb}];
DefineTensor1[PDuname[trK], {la}];
DefineTensor1[PDuname[At], {la,lb,lc}, {{la,lb}}];
DefineTensor1[PDuname[Theta], {la}];
DefineTensor1[PDuname[alpha], {la}];
DefineTensor1[PDuname[A], {la}];
DefineTensor1[PDuname[beta], {ua,lb}];
DefineTensor1[PDuname[B], {ua,lb}];

(* local and global dissipation *)

DefineTensor1[LDissName[phiW]];
DefineTensor1[GDissName[phiW]];
DefineTensor1[LDissName[gt],{la,lb},{{la,lb}}];
DefineTensor1[GDissName[gt],{la,lb},{{la,lb}}];
DefineTensor1[LDissName[Xt],{ua}];
DefineTensor1[GDissName[Xt],{ua}];
DefineTensor1[LDissName[Theta]];
DefineTensor1[GDissName[Theta]];
DefineTensor1[LDissName[Trk]];
DefineTensor1[GDissName[Trk]];
DefineTensor1[LDissName[At],{la,lb},{{la,lb}}];
DefineTensor1[GDissName[At],{la,lb},{{la,lb}}];
DefineTensor1[LDissName[alpha]];
DefineTensor1[GDissName[alpha]];
DefineTensor1[LDissName[A]];
DefineTensor1[GDissName[A]];
DefineTensor1[LDissName[beta],{ua}];
DefineTensor1[GDissName[beta],{ua}];
DefineTensor1[LDissName[B],{ua}];
DefineTensor1[GDissName[B],{ua}];

Print["LDissName[At],{la,lb},{lla,lb}} = ",
      LDissName[At][la,b]];
Print["LDissName[At],{la,lb},{lla,lb}} 2 = "LDiss[At[la,lb]]];
Print["GDissName[At],{la,lb},{lla,lb}} = ",
      GDissName[At][la,b]];
Print["GDissName[At],{la,lb},{lla,lb}} 2 = "GDiss[At[la,lb]]];

(* Filtered variables *)
(* -------------------------------------------- *)

(*
   Strategy:
   In numerical experiments with the wave equation,
   du/dt = rho
   drho/dt = d^2 u/dx^2

   we found that truncating rho to demand it
   be accurate to order p-1, where
   p is the order of u, made convergence much
   cleaner.

   Inspired by this, we attempt to do something
   analogous for the BSSN equations.

   In the wave equation, rho is the canonically
   conjugate momentum to u. So in the BSSN
   equations we truncate
   (or filter. TODO: figure out which is better)
   the variables canonically conjugate to the
   quantities we are interested in (which are the
   3+1 split components of the four-metric).

   Therefore, we truncate/filter:
   A, B, trK, K,
   here denoted:
   A, B, trK, At,

   We denote the filtered variables, 'Temp'
 *)
DefineTensor1[ATemp];
DefineTensor1[BTemp,{ua}];
DefineTensor1[trKTemp];
DefineTensor1[AtTemp,{la,lb},{{la,lb}}];
(* -------------------------------------------- *)

(* conformal Christoffel and curvature variables *)
DefineTensor1[em4phi];
DefineTensor1[gtu, {ua,ub}, {{ua,ub}}];
DefineTensor1[Gtl, {la,lb,lc}, {{lb,lc}}];
DefineTensor1[Gtlu, {la,lb,uc}];
DefineTensor1[Gt, {ua,lb,lc}, {{lb,lc}}];
DefineTensor1[Xtn, {ua}];
DefineTensor1[Zl, {la}];
DefineTensor1[Z, {ua}];
DefineTensor1[cdphi, {la}];
DefineTensor1[cdphi2, {la,lb}];
DefineTensor1[Atm, {ua,lb}];
DefineTensor1[Atu, {ua,ub}, {{ua,ub}}];
DefineTensor1[Rt, {la,lb}, {{la,lb}}];
DefineTensor1[Rphi, {la,lb}, {{la,lb}}];
DefineTensor1[Ats, {la,lb}, {{la,lb}}];
DefineTensor1[trAts];
DefineTensor1[dotXt, {ua}];
DefineTensor1[dotTheta];
DefineTensor1[dottrK];
DefineTensor1[dotalpha];
DefineTensor1[dotbeta, {ua}];
DefineTensor1[ddetgt, {la}];

(* Matter variables *)
DefineTensor1[rho];
DefineTensor1[S, {la}];
DefineTensor1[trS];
DefineTensor1[T00];
DefineTensor1[T0, {la}];
DefineTensor1[T, {la,lb}, {{la,lb}}];

(* constraints from BSSN decomposition *)
DefineTensor1[cS];
DefineTensor1[cXt, {ua}];
DefineTensor1[cA];

(* ADM quantities *)
DefineTensor1[g, {la,lb}, {{la,lb}}];
DefineTensor1[K, {la,lb}, {{la,lb}}];

DefineTensor1[gu, {ua,ub}, {{ua,ub}}];
DefineTensor1[R, {la,lb}, {{la,lb}}];
DefineTensor1[trR];

(* ADM constraints *)
DefineTensor1[H];
DefineTensor1[M, {la}];

(* ADMBase variables *)
DefineTensor1[admg, {la,lb}, {{la,lb}}];
DefineTensor1[admK, {la,lb}, {{la,lb}}];
DefineTensor1[admalpha];
DefineTensor1[admdtalpha];
DefineTensor1[admbeta, {ua}];
DefineTensor1[admdtbeta, {ua}];

DefineTensor1[LDuname[admalpha], {lq}];
DefineTensor1[LDuname[admbeta], {ua,lq}];
DefineTensor1[PDuname[admalpha], {la}];
DefineTensor1[PDuname[admbeta], {ua,lb}];

(* These weights are probably unused *)
SetTensorAttribute[phiW, TensorWeight, +1/6];
SetTensorAttribute[gt,   TensorWeight, -2/3];
SetTensorAttribute[Xt,   TensorWeight, +2/3];
SetTensorAttribute[At,   TensorWeight, -2/3];
SetTensorAttribute[cS,   TensorWeight, +2  ];
SetTensorAttribute[cXt,  TensorWeight, +2/3];

(* Note: J[ua,li] = dx^a/dx^i   where a: local, i: global *)
DefineTensor1[J, {ua,li}];
DefineTensor1[myDetJ];
DefineTensor1[sqrtDetJJ];

DefineConnection[CD, PD, G];
DefineConnection[CDt, PD, Gt];

(******************************************************************************)
(* Groups *)
(******************************************************************************)

evolvedGroups = {
  DefineGroup1[prefix<>"confac"    , phiW     , maxTimelevels],
  DefineGroup1[prefix<>"metric"    , gt[la,lb], maxTimelevels],
  DefineGroup1[prefix<>"Gamma"     , Xt[ua]   , maxTimelevels],
  DefineGroup1[prefix<>"trace_curv", trK      , maxTimelevels],
  DefineGroup1[prefix<>"curv"      , At[la,lb], maxTimelevels],
  DefineGroup1[prefix<>"Theta"     , Theta    , maxTimelevels] // IfCCZ4,
  DefineGroup1[prefix<>"lapse"     , alpha    , maxTimelevels],
  DefineGroup1[prefix<>"dtlapse"   , A        , maxTimelevels] // IfA,
  DefineGroup1[prefix<>"shift"     , beta[ua] , maxTimelevels],
  DefineGroup1[prefix<>"dtshift"   , B[ua]    , maxTimelevels] // IfB};

(* TODO: isn't there a Theta constraint as well? *)
(* TODO: make the evaluated groups use other_timelevels *)
evaluatedGroups = {
  DefineGroup1[prefix<>"weight"              , weight              , 1],
  DefineGroup1[prefix<>"volume_form"         , myDetJ              , 1],
  DefineGroup1[prefix<>"dconfac"             , PD[phiW,la]         , 1],
  DefineGroup1[prefix<>"dmetric"             , PD[gt[la,lb],lc]    , 1],
  DefineGroup1[prefix<>"dlapse"              , PD[alpha,la]        , 1],
  DefineGroup1[prefix<>"dshift"              , PD[beta[ua],lb]     , 1],
  DefineGroup1[prefix<>"dconfac_tmp"         , PD[phiWTmp,la]      , 1],
  DefineGroup1[prefix<>"dmetric_tmp"         , PD[gtTmp[la,lb],lc] , 1],
  DefineGroup1[prefix<>"dlapse_tmp"          , PD[alphaTmp,la]     , 1],
  DefineGroup1[prefix<>"dshift_tmp"          , PD[betaTmp[ua],lb]  , 1],
  DefineGroup1[prefix<>"trace_curv_filtered" , trKTemp             , 1],
  DefineGroup1[prefix<>"dtlapse_filtered"    , ATemp               , 1],
  DefineGroup1[prefix<>"dtshift_filtered"    , BTemp[ua]           , 1],
  DefineGroup1[prefix<>"curv_filtered"       , AtTemp[la,lb]       , 1],
  DefineGroup1[prefix<>"Ham"                 , H              , maxTimelevels],
  DefineGroup1[prefix<>"mom"                 , M[la]          , maxTimelevels],
  DefineGroup1[prefix<>"cons_detg"           , cS             , maxTimelevels],
  DefineGroup1[prefix<>"cons_Gamma"          , cXt[ua]        , maxTimelevels],
  DefineGroup1[prefix<>"cons_traceA"         , cA             , maxTimelevels]};

declaredGroups = Join[evolvedGroups, evaluatedGroups];
declaredGroupNames = groupName /@ declaredGroups;

extraGroups =
  {{"grid::coordinates", {x, y, z, r}},
   {"ADMBase::metric",  {gxx, gxy, gxz, gyy, gyz, gzz}},
   {"ADMBase::curv",    {kxx, kxy, kxz, kyy, kyz, kzz}},
   {"ADMBase::lapse",   {alp}},
   {"ADMBase::dtlapse", {dtalp}},
   {"ADMBase::shift",   {betax, betay, betaz}},
   {"ADMBase::dtshift", {dtbetax, dtbetay, dtbetaz}},
   {"TmunuBase::stress_energy_scalar", {eTtt}},
   {"TmunuBase::stress_energy_vector", {eTtx, eTty, eTtz}},
   {"TmunuBase::stress_energy_tensor", {eTxx, eTxy, eTxz, eTyy, eTyz, eTzz}}};

groups = Join[declaredGroups, extraGroups];

inheritedImplementations =
  {"ADMBase", "CoordGauge", IfMatter["TmunuBase"]};

(******************************************************************************)
(* Grid setup *)
(******************************************************************************)

setupCoordinates = {
  Name      -> thorn<>"_SetupCoordinates",
  Schedule  -> {"IN "<>thorn<>"_CalcCoordinatesGroup"},
  NoSimplify -> True,
  Equations -> {
    x -> Node[0,1],
    y -> Node[0,2],
    z -> Node[0,3],
    r -> Sqrt[x^2 + y^2 + z^2]}};

(* TODO: Figure out how to use detJ from Llama *)
setupWeights = {
  Name       -> thorn<>"_SetupWeights",
  Schedule   -> {"AS DGFE_SetupWeights IN SetupMask"},
  Shorthands -> {sqrtDetJ},
  Equations  -> {
    myDetJ   -> IfThen[usejacobian,
                       Abs[Det[MatrixOfComponents[J[uq,lb]]]],
                       1],
    sqrtDetJ -> Sqrt[myDetJ],
    weight   -> Weight[1] / sqrtDetJ}};

(******************************************************************************)


(* Cactus Parameters *)
(******************************************************************************)

extendedKeywordParameters = {
  {
    Name -> "ADMBase::evolution_method",
    AllowedValues -> {thorn}
  },
  {
    Name -> "ADMBase::lapse_evolution_method",
    AllowedValues -> {thorn}
  },
  {
    Name -> "ADMBase::shift_evolution_method",
    AllowedValues -> {thorn}
  },
  {
    Name -> "ADMBase::dtlapse_evolution_method",
    AllowedValues -> {thorn}
  },
  {
    Name -> "ADMBase::dtshift_evolution_method",
    AllowedValues -> {thorn}
  }};

keywordParameters = {
  {
    Name -> "initial_boundary_condition",
    Description -> "Boundary condition for initial condition for some of the BSSN variables",
    AllowedValues -> {{Value -> "none"},
                      {Value -> "Minkowski"},
                      {Value -> "extrapolate"}},
    Default -> "extrapolate"
  },
  {
    Name -> "boundary_condition",
    Description -> "Boundary condition for BSSN variables",
    AllowedValues -> {{Value -> "none"},
                      {Value -> "extrapolate"}},
    Default -> "none"
  },
  {
    Name -> "derivatives_boundary_condition",
    Description -> "Boundary condition for first spatialderivatives",
    AllowedValues -> {{Value -> "none"},
                      {Value -> "extrapolate"}},
    Default -> "none"
  },
  {
    Name -> "rhs_boundary_condition",
    Description -> "Boundary condition for BSSN RHS and some of the ADMBase variables",
    AllowedValues -> {{Value -> "none"},
                      {Value -> "zero"},
                      {Value -> "newrad"}},
    Default -> "newrad"
  },
  {
    Name -> "rhs_evaluation",
    Description -> "Whether and how the RHS routine should be split to improve performance",
    AllowedValues -> {{Value -> "combined", Description -> "use a single routine (probably slow)"},
                      {Value -> "split3", Description -> "split into 3 routines via Kranc"}},
    Default -> "combined"
  }};

intParameters = {
  If[!NumericQ[formulationKranc],
     {
       Name -> formulation,
       Description -> "Formulation",
       AllowedValues -> {{Value -> fBSSN, Description -> "BSSN"},
                         {Value -> fCCZ4, Description -> "CCZ4"}},
       Default -> fBSSN
     },
     Unevaluated[Sequence[]]],
  If[!NumericQ[conformalMethodKranc],
     {
       Name -> conformalMethod,
       Description -> "Treatment of conformal factor",
       AllowedValues -> {{Value -> cmPhi, Description -> "phi method"},
                         {Value -> cmW,   Description -> "W method"}},
       Default -> cmPhi
     },
     (* We need this parameter all the time since ML_BSSN_Helper accesses it *)
     {
       Name -> conformalMethod,
       Description -> "Treatment of conformal factor",
       AllowedValues -> {{Value -> conformalMethodKranc}},
       Default -> conformalMethodKranc
     }],
  If[!NumericQ[evolveAKranc],
     {
       Name -> evolveA,
       Description -> "Evolve time derivative of lapse A? (former LapseACoeff)",
       AllowedValues -> {{Value -> 0, Description -> "off"},
                         {Value -> 1, Description -> "on"}},
       Default -> 0
     },
     Unevaluated[Sequence[]]],
  If[!NumericQ[evolveBKranc],
     {
       Name -> evolveB,
       Description -> "Evolve time derivative of shift B^i? (former ShiftBCoeff)",
       AllowedValues -> {{Value -> 0, Description -> "off"},
                         {Value -> 1, Description -> "on"}},
       Default -> 1
     },
     Unevaluated[Sequence[]]],
  If[!NumericQ[addMatterKranc],
     {
       Name -> addMatter,
       Description -> "Add matter terms?",
       AllowedValues -> {{Value -> 0, Description -> "off"},
                         {Value -> 1, Description -> "on"}},
       Default -> 1
     },
     Unevaluated[Sequence[]]],
  {
    Name -> harmonicN,
    Description -> "d/dt alpha = - f alpha^n K  (harmonic: n=2, 1+log: n=1)",
    Default -> 2
  },
  {
    Name -> shiftFormulation,
    Description -> "shift formulation",
    AllowedValues -> {{Value -> shiftGammaDriver, Description -> "Gamma driver"},
                      {Value -> shiftHarmonic, Description -> "harmonic"}},
    Default -> shiftGammaDriver
  },
  {
    Name -> useSpatialBetaDriver,
    Description -> "Enable spatially varying betaDriver",
    AllowedValues -> {{Value -> 0, Description -> "off"},
                      {Value -> 1, Description -> "on"}},
    Default -> 0
  },
  {
    Name -> useSpatialShiftGammaCoeff,
    Description -> "Enable spatially varying shiftGammaCoeff",
    AllowedValues -> {{Value -> 0, Description -> "off"},
                      {Value -> 1, Description -> "on"}},
    Default -> 0
  },
  {
    Name -> advectLapse,
    Description -> "Advect lapse? (former LapseAdvectionCoeff)",
    AllowedValues -> {{Value -> 0, Description -> "off"},
                      {Value -> 1, Description -> "on"}},
    Default -> 1
  },
  {
    Name -> advectShift,
    Description -> "Advect shift? (former ShiftAdvectionCoeff)",
    AllowedValues -> {{Value -> 0, Description -> "off"},
                      {Value -> 1, Description -> "on"}},
    Default -> 1
  },
  {
    Name -> fixAdvectionTerms,
    Description -> "Modify driver and advection terms to work better?",
    AllowedValues -> {{Value -> 0, Description -> "off"},
                      {Value -> 1, Description -> "on"}},
    Default -> 0
  }};

realParameters = {
  IfCCZ4[
    {
      Name -> GammaShift,
      Description -> "CCZ4 dovariant shift term in Gamma",
      Default -> 0.5
    }],
  IfCCZ4[
    {
      Name -> dampk1,
      Description -> "CCZ4 damping term 1 for Theta and Z",
      Default -> 0
    }],
  IfCCZ4[
    {
      Name -> dampk2,
      Description -> "CCZ4 damping term 2 for Theta and Z",
      Default -> 0
    }],
  {
    Name -> harmonicF,
    Description -> "d/dt alpha = - f alpha^n K   (harmonic: f=1, 1+log: f=2)",
    Default -> 1
  },
  {
    Name -> alphaDriver,
    Description -> "d/dt alpha = ... - alphaDriver (alpha - 1)   (use 1/M (?))",
    Default -> 0 (* TODO: change default? *)
  },
  {
    Name -> shiftGammaCoeff,
    Description -> "d/dt beta^i = C Xt^i   (use C=0.75/M)",
    Default -> 0 (* TODO: change default? *)
  },
  {
    Name -> betaDriver,
    Description -> "d/dt beta^i = ... - betaDriver alpha^shiftAlphaPower beta^i   (use 1/M (?))",
    Default -> 0 (* TODO: change default? *)
  },
  {
    Name -> shiftAlphaPower,
    Description -> "d/dt beta^i = ... - betaDriver alpha^shiftAlphaPower beta^i   (use 0 (?))",
    Default -> 0
  },
  {
    Name -> spatialBetaDriverRadius,
    Description -> "Radius at which the betaDriver starts to be reduced",
    AllowedValues -> {{Value -> "(0:*", Description -> "positive"}},
    Default -> 10^12
  },
  {
    Name -> spatialShiftGammaCoeffRadius,
    Description -> "Radius at which shiftGammaCoeff starts to be reduced",
    AllowedValues -> {{Value -> "(0:*", Description -> "positive"}},
    Default -> 10^12
  },
  {
    Name -> minimumLapse,
    Description -> "Enforced minimum of the lapse function",
    AllowedValues -> {{Value -> "0:*", Description -> "non-negative"}},
    Default -> 0
  },
  {
    Name        -> alphaDeriv,
    Description -> "Jump term (penalty) strength in derivatives",
    Default     -> 0.5
  },
  {
    Name        -> epsDiss,
    Description -> "Dissipation strength",
    Default     -> 0.0
  },
  {
    Name        -> epsJump,
    Description -> "Penalty strength in dissipation",
    Default     -> 0.0
  },
  {
    Name        -> epsFilter,
    Description -> "Filter strength for first derivatives",
    Default     -> 0.0
  }};

(******************************************************************************)
(* BSSN-specific Mathematica definition *)
(******************************************************************************)

(* Various Mathematica definitions that involve tensors *)

KD = KroneckerDelta;

detgExpr = Det[MatrixOfComponents[g[la,lb]]];
detgtExpr = Det[MatrixOfComponents[gt[la,lb]]];

betaDriverFunc[r_] :=
  IfThen[useSpatialBetaDriver!=0,
         spatialBetaDriverRadius / Max[r, spatialBetaDriverRadius] betaDriver,
         betaDriver];
shiftGammaCoeffFunc[r_] :=
  IfThen[useSpatialShiftGammaCoeff!=0,
         Min[Exp[1 - r / spatialShiftGammaCoeffRadius], 1] shiftGammaCoeff,
         shiftGammaCoeff];

(******************************************************************************)
(* Master calculations *)
(******************************************************************************)

BSSNFromADMBaseCalc = {
  Shorthands -> {dir[ua],
                 LD[gt[la,lb],lq],
                 LDu[admalpha,lq], LDu[admbeta[ua],lq],
                 PDu[admalpha,la], PDu[admbeta[ua],lb],
                 g[la,lb], detg, gu[ua,ub],
                 em4phi,
                 gtu[ua,ub], Gt[ua,lb,lc],
                 shiftGammaCoeffValue},
  Equations -> {
    dir[ua] -> Sign[admbeta[ua]],
    
    LD[gt[la,lb],lq] -> Deriv[gt[la,lb],lq],
    PD[gt[la,lb],lc] -> LD2PD[gt[la,lb],lc],
    
    LDu[admalpha,lq]    -> DerivUpw[admalpha,lq],
    LDu[admbeta[ua],lq] -> DerivUpw[admbeta[ua],lq],
    PDu[admalpha,la]    -> LDu2PDu[admalpha,la],
    PDu[admbeta[ua],lb] -> LDu2PDu[admbeta[ua],lb],
    
    g[la,lb]     -> admg[la,lb],
    detg         -> detgExpr,
    gu[ua,ub]    -> detgExpr/detg MatrixInverse[g[ua,ub]],
    
    phiW         -> IfW[detg^(-1/6), Log[detg]/12],
    em4phi       -> IfW[phiW^2, Exp[-4 phiW]],
    gt[la,lb]    -> em4phi g[la,lb],
    
    gtu[ua,ub]   -> 1/em4phi gu[ua,ub],
    Gt[ua,lb,lc] -> (1/2 gtu[ua,ud]
                     (PD[gt[lb,ld],lc] + PD[gt[lc,ld],lb] - PD[gt[lb,lc],ld])),
    Xt[ua]       -> gtu[ub,uc] Gt[ua,lb,lc],
    
    trK          -> gu[ua,ub] admK[la,lb],
    At[la,lb]    -> em4phi * (admK[la,lb] - 1/3 g[la,lb] trK),
    
    Theta        -> 0,
    
    alpha        -> admalpha,
    (* See RHS *)
    A            -> IfA[1 / (-harmonicF admalpha^harmonicN)
                        (+ admdtalpha
                         - IfThen[advectLapse!=0,
                                  Upwind[admbeta[ua], admalpha, la],
                                  0]),
                        0],
    
    shiftGammaCoeffValue -> shiftGammaCoeffFunc[r],
    
    beta[ua]     -> admbeta[ua],
    (* See RHS *)
    (* Note: cannot have (shiftFormulation == shiftharmonic && evolveB) *)
    (* TODO: check this *)
    B[ua]        -> IfB[IfThen[shiftGammaCoeffValue!=0.0,
			       1 / (shiftGammaCoeffValue
				    admalpha^shiftAlphaPower)
			       (+ admdtbeta[ua]
				- IfThen[advectShift!=0,
					 Upwind[admbeta[ub], admbeta[ua] ,lb],
					 0]),
			       0],
                        0]}};



BSSNToBSSNCalc = {
  Shorthands -> {detgt, gtu[ua,ub], Atm[ua,lb], trAt},
  Equations -> {
    detgt     -> 1 (* TODO COMPATIBILITY detgtExpr *),
    gt[la,lb] -> detgt^(-1/3) gt[la,lb],
    
    detgt      -> 1 (* detgtExpr *),
    gtu[ua,ub] -> detgtExpr/detgt MatrixInverse[gt[ua,ub]],
    Atm[ua,lb] -> gtu[ua,uc] At[lc,lb],
    trAt       -> Atm[ua,la],
    At[la,lb]  -> At[la,lb] - 1/3 gt[la,lb] trAt,
    
    alpha -> Max[alpha, minimumLapse]}};


BSSNDerivativesCalc = {
  Shorthands -> {LD[phiW,lq], LD[gt[la,lb],lq], LD[alpha,lq], LD[beta[ua],lq]},
  Equations -> {
    LD[phiW,lq]      -> Deriv[phiW,lq],
    LD[gt[la,lb],lq] -> Deriv[gt[la,lb],lq],
    LD[alpha,lq]     -> Deriv[alpha,lq],
    LD[beta[ua],lq]  -> Deriv[beta[ua],lq],
    PD[phiW,la]      -> LD2PD[phiW,la],
    PD[gt[la,lb],lc] -> LD2PD[gt[la,lb],lc],
    PD[alpha,la]     -> LD2PD[alpha,la],
    PD[beta[ua],lb]  -> LD2PD[beta[ua],lb]}};

FromBSSNCalc = {
  Shorthands -> {dir[ua],
                 LD[Xt[ua],lq], LD[trK,lq], LD[At[la,lb],lq],
                 PD[Xt[ua],lb], PD[trK,la], PD[At[la,lb],lc],
                 LD[PD[phiW,la],lq], LD[PD[gt[la,lb],lc],lq],
                 LD[PD[alpha,la],lq], LD[PD[beta[ua],lb],lq],
                 PD[phiW,la,lb], PD[gt[la,lb],lc,ld],
                 PD[alpha,la,lb], PD[beta[ua],lb,lc],
                 LDu[phiW,lq], LDu[gt[la,lb],lq], LDu[Xt[ua],lq],
                 LDu[trK,lq], LDu[At[la,lb],lq], LDu[Theta,lq],
                 LDu[alpha,lq], LDu[A,lq], LDu[beta[ua],lq], LDu[B[ua],lq],
                 PDu[phiW,la], PDu[gt[la,lb],lc], PDu[Xt[ua],lb],
                 PDu[trK,la], PDu[At[la,lb],lc], PDu[Theta,la],
                 PDu[alpha,la], PDu[A,la], PDu[beta[ua],lb], PDu[B[ua],lb],


		 LDiss[phiW], GDiss[phiW],
		 LDiss[gt[la,lb]], GDiss[gt[la,lb]],
		 LDiss[Xt[ua]], GDiss[Xt[ua]],
		 LDiss[Theta], GDiss[Theta],
		 LDiss[trK], GDiss[trK],
		 LDiss[At[la,lb]], GDiss[At[la,lb]],
		 LDiss[alpha], GDiss[alpha],
		 LDiss[A], GDiss[A],
		 LDiss[beta[ua]], GDiss[beta[ua]],
		 LDiss[B[ua]], GDiss[B[ua]],


                 detgt, gtu[ua,ub], Gtl[la,lb,lc], Gtlu[la,lb,uc], Gt[ua,lb,lc],
                 Xtn[ua],
                 e4phi, em4phi, g[la,lb], gu[ua,ub],
                 Zl[la], Z[ua],
                 fac1, cdphi[la], fac2, cdphi2[la,lb],
                 Rt[la,lb], Rphi[la,lb], R[la,lb], trR,
                 Atm[ua,lb], Atu[ua,ub], trAt,
                 rho, S[la], trS,
                 Ats[la,lb], trAts,
                 betaDriverValue, shiftGammaCoeffValue,
                 dotXt[ua], dotTheta, dottrK, dotalpha, dotbeta[ua],
                 ddetgt[la]},
  Equations -> {
    dir[ua] -> Sign[beta[ua]],
    
    LD[Xt[ua],lq]    -> Deriv[Xt[ua],lq],
    LD[trK,lq]       -> Deriv[trK,lq],
    LD[At[la,lb],lq] -> Deriv[At[la,lb],lq],
    PD[Xt[ua],lb]    -> LD2PD[Xt[ua],lb],
    PD[trK,la]       -> LD2PD[trK,la],
    PD[At[la,lb],lc] -> LD2PD[At[la,lb],lc],
    
    LD[PD[phiW,la],lq]      -> Deriv[PD[phiW,la],lq],
    LD[PD[gt[la,lb],lc],lq] -> Deriv[PD[gt[la,lb],lc],lq],
    LD[PD[alpha,la],lq]     -> Deriv[PD[alpha,la],lq],
    LD[PD[beta[ua],lb],lq]  -> Deriv[PD[beta[ua],lb],lq],
    PD[phiW,la,lb]          -> LD2PD[PD[phiW,la],lb],
    PD[gt[la,lb],lc,ld]     -> LD2PD[PD[gt[la,lb],lc],ld],
    PD[alpha,la,lb]         -> LD2PD[PD[alpha,la],lb],
    PD[beta[ua],lb,lc]      -> LD2PD[PD[beta[ua],lb],lc],
    
    LDu[phiW,lq]      -> DerivUpw[phiW,lq],
    LDu[gt[la,lb],lq] -> DerivUpw[gt[la,lb],lq],
    LDu[Xt[ua],lq]    -> DerivUpw[Xt[ua],lq],
    LDu[trK,lq]       -> DerivUpw[trK,lq],
    LDu[At[la,lb],lq] -> DerivUpw[At[la,lb],lq],
    LDu[Theta,lq]     -> DerivUpw[Theta,lq],
    LDu[alpha,lq]     -> DerivUpw[alpha,lq],
    LDu[A,lq]         -> DerivUpw[A,lq],
    LDu[beta[ua],lq]  -> DerivUpw[beta[ua],lq],
    LDu[B[ua],lq]     -> DerivUpw[B[ua],lq],
    PDu[phiW,la]      -> LDu2PDu[phiW,la],
    PDu[gt[la,lb],lc] -> LDu2PDu[gt[la,lb],lc],
    PDu[Xt[ua],lb]    -> LDu2PDu[Xt[ua],lb],
    PDu[trK,la]       -> LDu2PDu[trK,la],
    PDu[At[la,lb],lc] -> LDu2PDu[At[la,lb],lc],
    PDu[Theta,la]     -> LDu2PDu[Theta,la],
    PDu[alpha,la]     -> LDu2PDu[alpha,la],
    PDu[A,la]         -> LDu2PDu[A,la],
    PDu[beta[ua],lb]  -> LDu2PDu[beta[ua],lb],
    PDu[B[ua],lb]     -> LDu2PDu[B[ua],lb],

    LDiss[phiW]       -> Dissipation[phiW],
    GDiss[phiW]       -> LDiss2GDiss[phiW],
    LDiss[gt[la,lb]]  -> Dissipation[gt[la,lb]],
    GDiss[gt[la,lb]]  -> LDiss2GDiss[gt[la,lb]],
    LDiss[Xt[ua]]     -> Dissipation[Xt[ua]],
    GDiss[Xt[ua]]     -> LDiss2GDiss[Xt[ua]],
    LDiss[Theta]      -> Dissipation[Theta],
    GDiss[Theta]      -> LDiss2GDiss[Theta],
    LDiss[trK]        -> Dissipation[trK],
    GDiss[trK]        -> LDiss2GDiss[trK],
    LDiss[At[la,lb]]  -> Dissipation[At[la,lb]],
    GDiss[At[la,lb]]  -> LDiss2GDiss[At[la,lb]],
    LDiss[alpha]      -> Dissipation[alpha],
    GDiss[alpha]      -> LDiss2GDiss[alpha],
    LDiss[A]          -> Dissipation[A],
    GDiss[A]          -> LDiss2GDiss[A],
    LDiss[beta[ua]]   -> Dissipation[beta[ua]],
    GDiss[beta[ua]]   -> LDiss2GDiss[beta[ua]],
    LDiss[B[ua]]      -> Dissipation[B[ua]],
    GDiss[B[ua]]      -> LDiss2GDiss[B[ua]],

    (* Connection coefficients *)
    detgt          -> 1 (* detgtExpr *),
    (* This leads to simpler code *)
    gtu[ua,ub]     -> detgtExpr/detgt MatrixInverse[gt[ua,ub]],
    Gtl[la,lb,lc]  -> (1/2 (+ PD[gt[lb,la],lc] + PD[gt[lc,la],lb]
                            - PD[gt[lb,lc],la])),
    Gtlu[la,lb,uc] -> gtu[uc,ud] Gtl[la,lb,ld],
    Gt[ua,lb,lc]   -> gtu[ua,ud] Gtl[ld,lb,lc],
    
    em4phi    -> IfW[phiW^2, Exp[-4 phiW]],
    e4phi     -> 1 / em4phi,
    g[la,lb]  -> e4phi gt[la,lb],
    (* detg      -> detgExpr, *)
    gu[ua,ub] -> em4phi gtu[ua,ub],
    
    
    
    (* These conformal connection coefficients are calculated from the
       conformal metric, and are used instead of Xt where no
       derivatives of Xt are taken *)
    Xtn[ua] -> gtu[ub,uc] Gt[ua,lb,lc],
    
    (* Z quantities *)
    (* gr-qc:1106.2254 (2011), eqn. (23) *)
    Zl[la] -> 1/2 (- PD[gt[la,lb],lc] gtu[ub,uc] + gt[la,lc] Xt[uc]),
    Z[ua]  -> gu[ua,ub] Zl[lb],
    
    
    
    (* Curvature *)
    
    (* PRD 62 044034 (2000), eqn. (18) *)
    (* CCZ4: Adding Z term by changing Xtn to Xt *)
    Rt[la,lb] -> (- 1/2 gtu[uc,ud] PD[gt[la,lb],lc,ld]
                  + 1/2 gt[lc,la] PD[Xt[uc],lb]
                  + 1/2 gt[lc,lb] PD[Xt[uc],la]
                  + 1/2 Xtn[uc] Gtl[la,lb,lc]
                  + 1/2 Xtn[uc] Gtl[lb,la,lc]
                  + (+ Gt[uc,la,ld] Gtlu[lb,lc,ud]
                     + Gt[uc,lb,ld] Gtlu[la,lc,ud]
                     + Gt[uc,la,ld] Gtlu[lc,lb,ud])),
    
    fac1          -> IfW[-1/(2 phiW), 1],
    cdphi[la]     -> fac1 CDt[phiW,la],
    fac2          -> IfW[1/(2 phiW^2), 0],
    cdphi2[la,lb] -> fac1 CDt[phiW,la,lb] + fac2 CDt[phiW,la] CDt[phiW,lb],
    
    (* PRD 62 044034 (2000), eqn. (15) *)
    Rphi[la,lb] -> (- 2 cdphi2[lb,la]
                    - 2 gt[la,lb] gtu[uc,ud] cdphi2[lc,ld]
                    + 4 cdphi[la] cdphi[lb]
                    - 4 gt[la,lb] gtu[uc,ud] cdphi[lc] cdphi[ld]),
    
    Atm[ua,lb] -> gtu[ua,uc] At[lc,lb],
    Atu[ua,ub] -> gtu[ub,uc] Atm[ua,lc],
    
    R[la,lb] -> (+ Rt[la,lb] + Rphi[la,lb]
                 + IfCCZ4[+ 2/phiW * (+ g[la,lc] Z[uc] PD[phiW,lb]
                                      + g[lb,lc] Z[uc] PD[phiW,la]
                                      - g[la,lb] Z[uc] PD[phiW,lc])
                          + e4phi Z[uc] PD[gt[la,lb],lc],
                          0]),
    
    
    
    (* Matter *)
    
    (* rho = n^a n^b T_ab *)
    rho -> IfMatter[1/alpha^2
                    (T00 - 2 beta[ua] T0[la] + beta[ua] beta[ub] T[la,lb]),
                    0],
    
    (* S_i = -p^a_i n^b T_ab, where p^a_i = delta^a_i + n^a n_i *)
    S[la] -> IfMatter[-1/alpha * (T0[la] - beta[ub] T[la,lb]), 0],
    
    (* trS = gamma^ij T_ij  *)
    trS -> IfMatter[gu[ua,ub] T[la,lb], 0],
    
    
    
    (* Calculate RHS terms *)
    
    (* PRD 62 044034 (2000), eqn. (10) *)
    (* PRD 67 084023 (2003), eqns. (16) and (23) *)
    dot[phiW] -> (+ IfW[1/3 phiW, -1/6] (alpha trK - PD[beta[ua],la])
                  + Upwind[beta[ua], phiW, la]
                  + GDiss[phiW]),
    
    (* PRD 62, 044034 (2000), eqn. (9) *)
    (* gr-qc:1106.2254 (2011), eqn. (14) *)
    (* removing trA from Aij ensures that dot[detgt] = 0 *)
    trAt           -> Atm[ua,la],
    dot[gt[la,lb]] -> (- 2 alpha * (+ At[la,lb]
                                    - IfCCZ4[1/3 gt[la,lb] trAt, 0])
                       + gt[la,lc] PD[beta[uc],lb]
                       + gt[lb,lc] PD[beta[uc],la]
                       - 2/3 gt[la,lb] PD[beta[uc],lc]
                       + Upwind[beta[uc], gt[la,lb], lc]
                       + GDiss[gt[la,lb]]),
    
    (* PRD 62 044034 (2000), eqn. (20) *)
    (* PRD 67 084023 (2003), eqn. (26) *)
    (* gr-qc:1106.2254 (2011), eqn. (19) *)
    (* Equation (4.28) in Baumgarte & Shapiro (Phys. Rept. 376 (2003) 41-131) *)
    (* CCZ4: Adding Z terms by changing Xtn to Xt, also adding extra Z
       and Theta terms *)
    dotXt[ua] -> (- 2 Atu[ua,ub] PD[alpha,lb]
                  + 2 alpha * (+ Gt[ua,lb,lc] Atu[ub,uc]
                               - 2/3 gtu[ua,ub] PD[trK,lb]
                               + 6 Atu[ua,ub] cdphi[lb])
                  + gtu[ub,uc] PD[beta[ua],lb,lc]
                  + 1/3 gtu[ua,ub] PD[beta[uc],lb,lc]
                  - Xtn[ub] PD[beta[ua],lb] 
                  + 2/3 Xtn[ua] PD[beta[ub],lb]
                  + IfCCZ4[+ GammaShift 2 e4phi (- Z[ub] PD[beta[ua],lb]
                                                 + 2/3 Z[ua] PD[beta[ub],lb])
                           - 4/3 alpha e4phi Z[ua] trK
                           + 2 gtu[ua,ub] (+ alpha PD[Theta,lb]
                                           - Theta PD[alpha,lb])
                           - 2 alpha e4phi dampk1 Z[ua],
                           0]
                  + IfMatter[- 16 Pi alpha gtu[ua,ub] S[lb]]),
    dot[Xt[ua]] -> (+ dotXt[ua]
                    + Upwind[beta[ub], Xt[ua], lb]
                    + GDiss[Xt[ua]]),
    
    (* gr-qc:1106.2254 (2011), eqn. (18) *)
    trR      -> gu[ua,ub] R[la,lb],
    dotTheta -> (- PD[alpha,la] Z[ua]
                 - dampk1 (2 + dampk2) alpha Theta
                 + 1/2 alpha * (+ trR
                                - Atm[ua,lb] Atm[ub,la]
                                + 2/3 trK^2
                                - 2 trK Theta)
                 + IfMatter[- 8 Pi alpha rho]),
    dot[Theta] -> IfCCZ4[+ dotTheta
                         + Upwind[beta[ua], Theta, la]
                         + GDiss[Theta],
                         0],
    
    (* PRD 62 044034 (2000), eqn. (11) *)
    (* gr-qc:1106.2254 (2011), eqn. (17) *)
    (* Equation (4.21) in Baumgarte & Shapiro (Phys. Rept. 376 (2003) 41-131) *)
    (* CCZ4: Adding the RHS of Theta to K, because K_Z4 = K_BSSN + 2
       Theta. Also adding the Z term, as it has to cancel with the one
       in Theta. *)
    dottrK -> (- em4phi * (+ gtu[ua,ub] (+ PD[alpha,la,lb]
                                         + 2 cdphi[la] PD[alpha,lb])
                           - Xtn[ua] PD[alpha,la])
               + alpha * (Atm[ua,lb] Atm[ub,la] + 1/3 trK^2)
               + IfCCZ4[+ 2 dotTheta + 2 PD[alpha,la] Z[ua]
                        + dampk1 (1 - dampk2) alpha Theta,
                        0]
               + IfMatter[4 Pi alpha * (rho + trS)]),
    dot[trK] -> (+ dottrK
                 + Upwind[beta[ua], trK, la]
                 + GDiss[trK]),
    
    (* PRD 62 044034 (2000), eqn. (12) *)
    (* TODO: Should we use the Hamiltonian constraint to make Rij tracefree? *)
    (* gr-qc:1106.2254 (2011), eqn. (15) *)
    (* Equation (4.23) in Baumgarte & Shapiro (Phys. Rept. 376 (2003) 41-131) *)
    (* CCZ4: Adding Z terms in the Ricci and Theta terms *)
    Ats[la,lb]     -> (- CDt[alpha,la,lb] +
                       + 2 (PD[alpha,la] cdphi[lb] + PD[alpha,lb] cdphi[la])
                       + alpha R[la,lb]),
    trAts          -> gu[ua,ub] Ats[la,lb],
    dot[At[la,lb]] -> (+ em4phi * (Ats[la,lb] - 1/3 g[la,lb] trAts)
                       + alpha * (+ (trK - IfCCZ4[2 Theta, 0]) At[la,lb]
                                  - 2 At[la,lc] Atm[uc,lb])
                       + At[la,lc] PD[beta[uc],lb]
                       + At[lb,lc] PD[beta[uc],la]
                       - 2/3 At[la,lb] PD[beta[uc],lc]
                       + Upwind[beta[uc], At[la,lb], lc]
                       + GDiss[At[la,lb]]
                       + IfMatter[- em4phi alpha 8 Pi
                                  (T[la,lb] - 1/3 g[la,lb] trS)]),
    
    dotalpha   -> - (harmonicF alpha^harmonicN
                     IfA[+ A,
                         + trK - IfCCZ4[2 Theta, 0]
                         + alphaDriver (alpha - 1)]),
    dot[alpha] -> (+ dotalpha
                   + IfThen[advectLapse!=0, Upwind[beta[ua], alpha, la], 0]
                   + GDiss[alpha]),
    
    dot[A] -> IfA[+ dottrK - IfCCZ4[2 dot[Theta], 0]
                  + IfThen[fixAdvectionTerms!=0, Upwind[beta[ua], trK, la], 0]
                  - (alphaDriver
                     (+ A
                      + IfThen[fixAdvectionTerms!=0 && advectLapse!=0,
                               Upwind[beta[ua], alpha, la] /
                               (- harmonicF alpha^harmonicN),
                               0]))
                  + IfThen[fixAdvectionTerms==0 && advectLapse!=0,
                           Upwind[beta[ua], A, la],
                           0]
                  + GDiss[A],
                  0],
    
    betaDriverValue      -> betaDriverFunc[r],
    shiftGammaCoeffValue -> shiftGammaCoeffFunc[r],
    
    (* TODO COMPATIBILITY: this is zero analytically *)
    ddetgt[la] -> gtu[ub,uc] PD[gt[lb,lc],la],
    
    dotbeta[ua] -> IfThen[shiftFormulation == shiftGammaDriver,
                          (* Gamma driver *)
                          shiftGammaCoeffValue alpha^shiftAlphaPower
                          IfB[B[ua], Xt[ua] - betaDriverValue beta[ua]],
                          (* harmonic *)
                          - (gu[ua,ub] alpha
                             (+ 2 alpha cdphi[lb] + PD[alpha,lb]
                              + 1/2 alpha ddetgt[lb]
                              - alpha gtu[uc,ud] PD[gt[lb,lc],ld]))],
    
    (* TODO: add not only advection terms, but full Lie derivatives! *)
    dot[beta[ua]] -> (+ dotbeta[ua]
                      + IfThen[advectShift!=0,
                               Upwind[beta[ub], beta[ua], lb],
                               0]
                      + GDiss[beta[ua]]),
    
    dot[B[ua]] -> IfB[+ dotXt[ua]
                      + IfThen[fixAdvectionTerms!=0,
                               Upwind[beta[ub], Xt[ua], lb],
                               0]
                      - (betaDriverValue
                         (+ B[ua]
                          + IfThen[fixAdvectionTerms!=0 && advectShift!=0 &&
				   shiftGammaCoeffValue!=0.0,
                                   Upwind[beta[ub], beta[ua], lb] /
                                   (shiftGammaCoeffValue alpha^shiftAlphaPower),
                                   0]))
                      + IfThen[fixAdvectionTerms==0 && advectShift!=0,
                               Upwind[beta[ub], B[ua], lb],
                               0]
                      + GDiss[B[ua]],
                      0],
    
    
    
    (* Calculate ADMBase variables *)
    admg[la,lb]   -> g[la,lb],
    admK[la,lb]   -> e4phi At[la,lb] + 1/3 g[la,lb] trK,
    admalpha      -> alpha,
    admdtalpha    -> (+ dotalpha
                      + IfThen[advectLapse!=0, Upwind[beta[ua], alpha, la], 0]),
    admbeta[ua]   -> beta[ua],
    admdtbeta[ua] -> (+ dotbeta[ua]
                      + IfThen[advectShift!=0,
                               Upwind[beta[ub], beta[ua], lb],
                               0]),
    
    
    
    (* Calculate constraints *)
    
    (* det gamma-tilde *)
    cS -> 0 (* TODO COMPATIBILITY detgtExpr - 1 *),
    
    (* Gamma constraint *)
    cXt[ua] -> Xtn[ua] - Xt[ua],
    
    (* trace A-tilde *)
    cA -> trAt,
    
    (* H -> trR - Km[ua,lb] Km[ub,la] + trK^2 *)
    (* PRD 67, 084023 (2003), eqn. (19) *)
    H -> trR - Atm[ua,lb] Atm[ub,la] + 2/3 trK^2 - IfMatter[16 Pi rho],
    
    (* TODO: use PRD 67, 084023 (2003), eqn. (20) *)
    M[la] -> (+ gtu[ub,uc] (CDt[At[la,lb],lc] + 6 At[la,lb] cdphi[lc])
              - 2/3 PD[trK,la]
              - IfMatter[8 Pi S[la]])}};



(******************************************************************************)
(* Actual calculations *)
(******************************************************************************)

(* Initial conditions *)

initialADMBase1EverywhereCalc = PartialCalculation[
  BSSNFromADMBaseCalc, "",
  {
    Name                  -> thorn<>"_InitialADMBase1Everywhere",
    Schedule              -> {"IN "<>thorn<>"_CalcInitialGroup"},
    ConditionalOnKeywords -> {{"evolution_method", thorn}},
    NoSimplify            -> True
  },
  {phiW, gt[la,lb], trK, At[la,lb], IfCCZ4[Theta], alpha, beta[ua]}];

initialADMBase2InteriorCalc = PartialCalculation[
  BSSNFromADMBaseCalc, "",
  {
    Name                  -> thorn<>"_InitialADMBase2Interior",
    Schedule              -> {"IN "<>thorn<>"_CalcInitialGroup AFTER "<>thorn<>"_InitialADMBase1Everywhere"},
    ConditionalOnKeywords -> {{"evolution_method", thorn}},
    Where                 -> InteriorNoSync,
    NoSimplify            -> True
  },
  {PD[gt[la,lb],lc],   (* Note: needed for Xt[ua] *)
   Xt[ua], IfA[A], IfB[B[ua]]}];

initialBoundaryMinkowskiCalc = {
  Name                  -> thorn<>"_InitialBoundaryMinkowski",
  Schedule              -> {"IN "<>thorn<>"_CalcInitialGroup"},
  ConditionalOnKeywords -> {{"evolution_method", thorn},
                            {"initial_boundary_condition", "Minkowski"}},
  Where                 -> Boundary,
  NoSimplify            -> True,
  Equations             -> {
    PD[gt[la,lb],lc] -> 0,
    Xt[ua]           -> 0,
    A                -> 0 // IfA,
    B[ua]            -> 0 // IfB}};



(* Extrapolating boundary conditions *)
extrap[u_] := GFOffset[u, -Sign[normal1], -Sign[normal2], -Sign[normal3]];
boundaryExtrapolateCalc = {
  Name                  -> thorn<>"_BoundaryExtrapolate",
  Schedule              -> {"IN "<>thorn<>"_CalcBoundaryGroup"},
  ConditionalOnKeywords -> {{"evolution_method", thorn},
                            {"boundary_condition", "extrapolate"}},
  Where                 -> Boundary,
  NoSimplify            -> True,
  Equations             -> {
    phiW      -> extrap[phiW]     ,
    gt[la,lb] -> extrap[gt[la,lb]],
    Xt[ua]    -> extrap[Xt[ua]]   ,
    trK       -> extrap[trK]      ,
    At[la,lb] -> extrap[At[la,lb]],
    Theta     -> extrap[Theta]     // IfCCZ4,
    alpha     -> extrap[alpha]    ,
    A         -> extrap[A]         // IfA,
    beta[ua]  -> extrap[beta[ua]] ,
    B[ua]     -> extrap[B[ua]]     // IfB}};



(* Enforce algebraic BSSN constraints *)

enforceEverywhereCalc = PartialCalculation[
  BSSNToBSSNCalc, "",
  {
    Name                  -> thorn<>"_EnforceEverywhere",
    Schedule              -> {"IN "<>thorn<>"_CalcEnforceGroup"},
    ConditionalOnKeywords -> {{"evolution_method", thorn}},
    NoSimplify            -> True
  },
  {gt[la,lb], At[la,lb], alpha}];



(* Calculate first spatial derivatives *)

derivativesInteriorCalc = PartialCalculation[
  BSSNDerivativesCalc, "",
  {
    Name                  -> thorn<>"_DerivativesInterior", 
   Schedule              -> {"IN "<>thorn<>"_CalcDerivativesGroup"},
    ConditionalOnKeywords -> {{"evolution_method", thorn}},
    Where                 -> Interior,
    NoSimplify            -> True
  },
  {PD[phiW,la], PD[gt[la,lb],lc], PD[alpha,la], PD[beta[ua],lb]}];

derivativesBoundaryExtrapolateCalc = {
  Name                  -> thorn<>"_DerivativesBoundaryExtrapolate",
  Schedule              -> {"IN "<>thorn<>"_CalcDerivativesGroup AFTER "<>thorn<>"_DerivativesInterior"},
  ConditionalOnKeywords -> {{"evolution_method", thorn},
                            {"derivatives_boundary_condition", "extrapolate"}},
  Where                 -> Boundary,
  NoSimplify            -> True,
  Equations             -> {
    PD[phiW,la]      -> extrap[PD[phiW,la]],
    PD[gt[la,lb],lc] -> extrap[PD[gt[la,lb],lc]],
    PD[alpha,la]     -> extrap[PD[alpha,la]],
    PD[beta[ua],lb]  -> extrap[PD[beta[ua],lb]]}};


(*
   This is the calculation that filters the canonically conjugate
   variables described above.
 *)
conjugateFilterCalcx = {
  Name                  -> thorn<>"_CalcConjugateFilterGroupx",
  Schedule              -> {"IN "<>thorn<>"_CalcConjugateFilterGroup"},
  ConditionalOnKeywords -> {{"evolution_method",thorn}},
  Where                 -> InteriorNoSync, (* RHS is not synced *)
  NoSimplify            -> True,
  Equations             -> {
    ATemp         -> Trunc[A,1],
    BTemp[ua]     -> Trunc[B[ua],1],
    trKTemp       -> Trunc[trK,1],
    AtTemp[la,lb] -> Trunc[At[la,lb],1]}};

conjugateFilterCalcy = {
  Name                  -> thorn<>"_CalcConjugateFilterGroupy",
  Schedule              -> {"IN "<>thorn<>"_CalcConjugateFilterGroup AFTER " <>thorn<>"_CalcConjugateFilterGroupx"},
  ConditionalOnKeywords -> {{"evolution_method",thorn}},
  Where                 -> InteriorNoSync, (* RHS is not synced *)
  NoSimplify            -> True,
  Equations             -> {
    A                   -> Trunc[ATemp,2],
    B[ua]               -> Trunc[BTemp[ua],2],
    trK                 -> Trunc[trKTemp,2],
    At[la,lb]           -> Trunc[AtTemp[la,lb],2]}};

conjugateFilterCalcz = {
  Name                  -> thorn<>"_CalcConjugateFilterGroup",
  Schedule              -> {"IN "<>thorn<>"_CalcConjugateFilterGroup AFTER "<>thorn<>"_CalcConjugateFilterGroupy"},
  ConditionalOnKeywords -> {{"evolution_method",thorn}},
  Where                 -> InteriorNoSync, (* RHS is not synced *)
  NoSimplify            -> True,
  Equations             -> {
    ATemp         -> Trunc[A,3],
    BTemp[ua]     -> Trunc[B[ua],3],
    trKTemp       -> Trunc[trK,3],
    AtTemp[la,lb] -> Trunc[At[la,lb],3]}};

conjugateMapCalc = {
  Name                  -> thorn<>"_CalcConjugateMapGroup",
  Schedule              -> {"IN "<>thorn<>"_CalcConjugateMapGroup"},
  ConditionalOnKeywords -> {{"evolution_method",thorn}},
  Where                 -> InteriorNoSync, (* RHS is not synced *)
  NoSimplify            -> True,
  Equations             -> {
    A         -> ATemp,
    B[ua]     -> BTemp[ua],
    trK       -> trKTemp,
    At[la,lb] -> AtTemp[la,lb]}};



(* Filter first spatial derivatives *)

(*
derivativesFilterInterior1Calc = PartialCalculation[
  BSSNDerivativesFilterCalc, "",
  {
    Name                  -> thorn<>"_DerivativesFilterInterior1",
    Schedule              -> {"IN "<>thorn<>"_CalcDerivativesFilterGroup"},
    ConditionalOnKeywords -> {{"evolution_method", thorn}},
    Where                 -> Interior
  },
  {PD[phiWTmp,la], PD[gtTmp[la,lb],lc], PD[alphaTmp,la], PD[betaTmp[ua],lb]}];

derivativesFilterInterior2Calc = PartialCalculation[
  BSSNDerivativesFilterCalc, "",
  {
    Name                  -> thorn<>"_DerivativesFilterInterior2",
    Schedule              -> {"IN "<>thorn<>"_CalcDerivativesFilterGroup AFTER "<>thorn<>"_DerivativesFilterInterior1"},
    ConditionalOnKeywords -> {{"evolution_method", thorn}},
    Where                 -> Interior
  },
  {PD[phiW,la], PD[gt[la,lb],lc], PD[alpha,la], PD[beta[ua],lb]}];

derivativesFilterBoundaryExtrapolateCalc = {
  Name                  -> thorn<>"_DerivativesFilterBoundaryExtrapolate",
  Schedule              -> {"IN "<>thorn<>"_CalcDerivativesFilterGroup AFTER "<>thorn<>"_DerivativesFilterInterior2"},
  ConditionalOnKeywords -> {{"evolution_method", thorn},
                            {"derivatives_boundary_condition", "extrapolate"}},
  Where                 -> Boundary,
  Equations             -> {
    PD[phiW,la]      -> extrap[PD[phiW,la]],
    PD[gt[la,lb],lc] -> extrap[PD[gt[la,lb],lc]],
    PD[alpha,la]     -> extrap[PD[alpha,la]],
    PD[beta[ua],lb]  -> extrap[PD[beta[ua],lb]]}};
*)



(* Calculate ADMBase variables *)

ADMBaseEverywhereCalc = PartialCalculation[
  FromBSSNCalc, "",
  {
    Name                  -> thorn<>"_ADMBaseEverywhere",
    Schedule              -> {"IN "<>thorn<>"_CalcADMBaseGroup"},
    ConditionalOnKeywords -> {{"evolution_method", thorn}},
    NoSimplify            -> True
  },
  {admg[la,lb], admK[la,lb], admalpha, admbeta[ua]}];

ADMBaseInteriorCalc = PartialCalculation[
  FromBSSNCalc, "",
  {
    Name                  -> thorn<>"_ADMBaseInterior",
    Schedule              -> {"IN "<>thorn<>"_CalcADMBaseGroup"},
    ConditionalOnKeywords -> {{"evolution_method", thorn}},
    Where                 -> InteriorNoSync,
    NoSimplify            -> True
  },
  {admdtalpha, admdtbeta[ua]}];



(* Calculate RHS variables *)

(* Set RHS to zero everywhere to avoid uninitialized values on
   non-interior poitns. There is no way to only loop over non-interior
   points, so we initialize all points and ensure this happens before
   the RHS is calculated. *)
evolutionInitializeCalc = {
  Name                  -> thorn<>"_EvolutionInitialize",
  Schedule              -> {"IN "<>thorn<>"_CalcRHSGroup"},
  ConditionalOnKeywords -> {{"evolution_method", thorn}},
  Where                 -> Everywhere,
  NoSimplify            -> True,
  Equations             -> {
    dot[phiW]      -> 0,
    dot[gt[la,lb]] -> 0,
    dot[Xt[ua]]    -> 0,
    dot[trK]       -> 0,
    dot[At[la,lb]] -> 0,
    dot[Theta]     -> 0 // IfCCZ4,
    dot[alpha]     -> 0,
    dot[A]         -> 0 // IfA,
    dot[beta[ua]]  -> 0,
    dot[B[ua]]     -> 0 // IfB}};

evolutionInteriorCalc = PartialCalculation[
  FromBSSNCalc, "",
  {
    Name                  -> thorn<>"_EvolutionInterior",
    Schedule              -> {"IN "<>thorn<>"_CalcRHSGroup AFTER "<>thorn<>"_EvolutionInitialize"},
    ConditionalOnKeywords -> {{"evolution_method", thorn},
                              {"rhs_evaluation", "combined"}},
    NoSimplify            -> True,
    Where                 -> InteriorNoSync (* RHS is not synced *)
  },
  {dot[phiW], dot[gt[la,lb]], dot[Xt[ua]], dot[trK], dot[At[la,lb]],
   IfCCZ4[dot[Theta]],
   dot[alpha], IfA[dot[A]], dot[beta[ua]], IfB[dot[B[ua]]]}];

evolutionInteriorCalcSplit3 = PartialCalculation[
  FromBSSNCalc, "",
  {
    Name                  -> thorn<>"_EvolutionInteriorSplit3",
    Schedule              -> {"IN "<>thorn<>"_CalcRHSGroup AFTER "<>thorn<>"_EvolutionInitialize"},
    ConditionalOnKeywords -> {{"evolution_method", thorn},
                              {"rhs_evaluation", "split3"}},
    NoSimplify            -> True,
    Where                 -> InteriorNoSync, (* RHS is not synced *)
    SplitBy -> {
      {dot[phiW], dot[gt[la,lb]], dot[alpha], IfA[dot[A]]},
      {dot[Xt[ua]], dot[trK], IfCCZ4[dot[Theta]],
       dot[beta[ua]], IfB[dot[B[ua]]]},
      {dot[At[la,lb]]}}
  },
  {dot[phiW], dot[gt[la,lb]], dot[Xt[ua]], dot[trK], dot[At[la,lb]],
   IfCCZ4[dot[Theta]],
   dot[alpha], IfA[dot[A]], dot[beta[ua]], IfB[dot[B[ua]]]}];

evolutionBoundaryCalc = {
  Name                  -> thorn<>"_EvolutionBoundaryZero",
  Schedule              -> {"IN "<>thorn<>"_CalcRHSGroup AFTER "<>thorn<>"_EvolutionInitialize"},
  ConditionalOnKeywords -> {{"evolution_method", thorn},
                            {"rhs_boundary_condition", "zero"}},
  NoSimplify            -> True,
  Where                 -> Boundary,
  Equations             -> {
    dot[phiW]      -> 0,
    dot[gt[la,lb]] -> 0,
    dot[Xt[ua]]    -> 0,
    dot[trK]       -> 0,
    dot[At[la,lb]] -> 0,
    dot[Theta]     -> 0 // IfCCZ4,
    dot[alpha]     -> 0,
    dot[A]         -> 0 // IfA,
    dot[beta[ua]]  -> 0,
    dot[B[ua]]     -> 0 // IfB}};



(* Calculate constraints *)

constraintsEverywhereCalc = PartialCalculation[
  FromBSSNCalc, "",
  {
    Name                  -> thorn<>"_ConstraintsEverywhere",
    Schedule              -> {"IN "<>thorn<>"_CalcConstraintsGroup"},
    ConditionalOnKeywords -> {{"evolution_method", thorn}},
    NoSimplify            -> True,
    Where                 -> Interior (* TODO COMPATIBILITY Everywhere; also SelectBCs! *)
  },
  {cS, cA}];

constraintsInteriorCalc = PartialCalculation[
  FromBSSNCalc, "",
  {
    Name                  -> thorn<>"_ConstraintsInterior",
    Schedule              -> {"IN "<>thorn<>"_CalcConstraintsGroup"},
    ConditionalOnKeywords -> {{"evolution_method", thorn}},
    NoSimplify            -> True,
    Where                 -> Interior
  },
  {cXt[ua], H, M[la]}];



(******************************************************************************)
(* Construct the thorn *)
(******************************************************************************)

calculations = {
  (* coordinates *)
  setupCoordinates,
  (* carpet reduce *)
  setupWeights,
  (* initial conditions *)
  initialADMBase1EverywhereCalc,
  initialADMBase2InteriorCalc,
  initialBoundaryMinkowskiCalc,
  (* boundary conditions *)
  boundaryExtrapolateCalc,
  (* enforce constraints*)
  enforceEverywhereCalc,
  (* calculate derivatives *)
  derivativesInteriorCalc,
  derivativesBoundaryExtrapolateCalc,
  (* Filter/Truncate Variables *)
  conjugateFilterCalcx,
  conjugateFilterCalcy,
  conjugateFilterCalcz,
  conjugateMapCalc,
  (* filter derivatives *)
  (* derivativesFilterInterior1Calc, *)
  (* derivativesFilterInterior2Calc, *)
  (* derivativesFilterBoundaryExtrapolateCalc, *)
  (* calculate ADMBase variables *)
  ADMBaseEverywhereCalc,
  ADMBaseInteriorCalc,
  (* evaluate RHS *)
  evolutionInitializeCalc,
  evolutionInteriorCalc,
  evolutionInteriorCalcSplit3,
  evolutionBoundaryCalc,
  (* evaluate constraints *)
  constraintsEverywhereCalc,
  constraintsInteriorCalc};

CreateKrancThornTT[
  groups, ".", thorn,
  Calculations              -> calculations,
  DeclaredGroups            -> declaredGroupNames,
  InheritedImplementations  -> inheritedImplementations,
  ExtendedKeywordParameters -> extendedKeywordParameters,
  KeywordParameters         -> keywordParameters,
  IntParameters             -> intParameters,
  RealParameters            -> realParameters,
  MergeFiles                -> "BSSN.merge",
  (* CountOperations           -> True, *)
  EvolutionTimelevels       -> maxTimelevels,
  Tile                      -> derivs===DG,
  UseJacobian               -> True,
  UseLoopControl            -> derivs===FD,
  UseOpenCL                 -> useOpenCL,
  UseVectors                -> derivs===FD && useVectors];
