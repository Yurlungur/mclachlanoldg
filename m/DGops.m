(* DGFE Derivatives *)
(* Authors:
   Jonah Miller <jmiller@perimeterinstitute.ca>
   Erik Schnetter <eschnetter@perimeterinstitute.ca>
   *)

(* TODO: check that "order" is set and integer and positive *)

(* A utility function for combining wide and narrow stencils *)
makeWide[narrow_] := Module[{transpose,length},
			    transpose = Transpose[narrow];
			    length = Length[transpose[[1]]];
			    Transpose[Join[{Table[0,{i,1,length}]},
					   transpose,
					   {Table[0,{i,1,length}]}]]];

(* Choose polynomial basis *)
xmin = -1; xmax = +1;

(* Note: Chebyshev polynomials don't admit simple SBP operators! *)
poly[n_, x_] = LegendreP[n, x]; intw[x_] = 1;
(* poly[n_, x_] = ChebyshevT[n, x]; intw[x_] = 1/Sqrt[1-x^2]; *)

accuracy = 30;                  (* digits *)

(* Collocation point locations: extrema and interval end points *)
nodes = Chop[Sort[Join[{xmin, xmax}, (x /. #1 & ) /@ 
                       NSolve[D[poly[order, x], x] == 0, x, Reals,
                              WorkingPrecision -> accuracy+10]]]];
Print["Nodes: ", nodes];

(* Conversion from spectral coefficients to configuration spaces and
   back *)
s2c = Table[poly[i, nodes[[j + 1]]], {j, 0, order}, {i, 0, order}];
c2s = Inverse[s2c];

(* Integration weights *)
weights =
  Chop[Table[w[i], {i, 0, order}] /.
       NSolve[Table[Integrate[intw[x] poly[n, x], {x, xmin, xmax}] ==
                    Plus @@ Table[w[i] poly[n, nodes[[i + 1]]],
                                  {i, 0, order}],
                    {n, 0, order}],
              Table[w[i], {i, 0, order}],
              WorkingPrecision -> accuracy][[1]]];
Print["Weights: ", weights];
wcoeffs = DiagonalMatrix[weights];

(* Derivative coefficients *)
dcoeffs =
  Chop[Table[Table[w[i], {i, 0, order}] /. 
             NSolve[Table[(D[poly[n, x], x] /. x -> nodes[[j + 1]]) == 
                          Plus @@ Table[w[i] poly[n, nodes[[i + 1]]],
                                        {i, 0, order}], 
                          {n, 0, order}],
                    Table[w[i], {i, 0, order}], 
                    WorkingPrecision -> accuracy][[1]],
             {j, 0, order}]];

(* Boundary coefficients *)
bcoeffs =
  Chop[Inverse[wcoeffs] . (wcoeffs.dcoeffs + Transpose[wcoeffs.dcoeffs])];

(* Jump coefficients *)
jcoeffs =
  Table[Switch[j,
               0,     (bcoeffs[[1,1]]
                       Switch[i, -1, -1, 0, +1, _, 0]),
               order, (bcoeffs[[order+1,order+1]]
                       Switch[i, order, +1, order+1, -1, _, 0]),
               _,     0],
        {j, 0, order}, {i, -1, order+1}];

(* Jump coefficients excluding boundary coefficients *)
(* Goal: D - 1/2 B - 1/2 K has only zeros on the diagonal *)
kcoeffs =
  Table[Switch[j,
               0,     (bcoeffs[[1,1]]
                       Switch[i, -1, -1, _, 0]),
               order, (bcoeffs[[order+1,order+1]]
                       Switch[i, order+1, -1, _, 0]),
               _,     0],
        {j, 0, order}, {i, -1, order+1}];

(* Penalty jump coefficients *)
pcoeffs =
  Table[Switch[j,
               0,     Switch[i, -1, +1, 0, -1, _, 0],
               order, Switch[i, order, -1, order+1, +1, _, 0],
               _,     0],
        {j, 0, order}, {i, -1, order+1}];

(* Exponential filter (Hesthaven, 5.16) *)
(* Tadmor's Spectral Viscosity Formulation,
   as described by Gottlieb and Hesthaven
   'Spectral Methods for Hyperbolic Problems'
   DOI: 10.1016/S0377-0427(00)00510-0

   Also described in Meister, Ortlieb, Sonar
   'On Spectral Filtering for Discontinuous Galerkin Methods on
   Unstructured Triangular Grids'

   We add a term ot the rhs that causes modes to be filtered in time.
   See section 3.2 Meister, Ortlieb, Sonar

   See Vendeven 'Family of Spectral Filters for Discontinuous Problems'
   for convergence results.

   Tadmor suggests that epsDiss be set to approximately the maximum
   of the quantity to be filtered, to the filterP power.

   But experimentation is required.
*)

(* TODO: Make these parameters *)
filterAlpha = -Log[$MachineEpsilon];
filterOrder = Ceiling[order+1, 2];

filterCutoff = 1/2;

filterP = order+1;
epsDissBase=1; (* TODO: normalize this to FD dissipation *)
(*filterEpsilonN=(epsDissBase/((order+1)^(2*filterP-1)));*)

eta[k_] = k/order;
filterCk[k_] = -epsDissBase*
    If[eta[k] <= filterCutoff, 0,
       (( eta[k] - filterCutoff ) / (1 - filterCutoff))^(2*filterP)];
rhsfcoeffs =
    Chop[N[s2c . DiagonalMatrix[Table[filterCk[k],{k,0,order}]] . c2s,
	   accuracy]];

sigma[eta_] =
  If[eta <= filterCutoff,
     1,
     Exp[-filterAlpha ((eta - filterCutoff)/(1 - filterCutoff))^filterOrder]];
fcoeffs =
  Chop[N[s2c . DiagonalMatrix[Table[sigma[i/(order + 1)], {i, 0, order}]] . c2s,
         accuracy]];
(* rhsfcoeffs = Chop[fcoeffs - IdentityMatrix[order+1]];*)


(* Dissipation operator *)

order2 = Floor[order, 2];
ecoeffs = (-1)^(order2/2+1) Nest[#.dcoeffs&, IdentityMatrix[order+1], order2];

(* Truncation operator to use on auxiliary variables. *)
trunccoeffs =
    Chop[N[s2c . DiagonalMatrix[Table[If[i == order, 0, 1],{i,0,order}]] . c2s,
				accuracy]];

(* A special derivative used for auxiliary variables, where the
   highest-order mode is truncated away after differentiation. *)

(* for the interior derivative *)
dtrunccoeffs = Chop[trunccoeffs . makeWide[dcoeffs - (1/2) bcoeffs]];
(* for the jump term *)
jumptrunccoeffs = Chop[trunccoeffs . kcoeffs];

(* Helper functions *)

GFOffsetDir[u_, a_, i_] := ReplacePart[GFOffset[u,0,0,0], {a+1->i}];
GFOffsetDir[u_, a_, i_, a_, j_] := GFOffsetDir[u, a, i+j];
GFOffsetDir[u_, a_, i_, b_, j_] :=
  ReplacePart[GFOffset[u,0,0,0], {a+1->i, b+1->j}];
GFOffsetDir[u_, a_, i_, a_, j_, a_, k_] := GFOffsetDir[u,a,i+j+k];
GFOffsetDir[u_, a_, i_, a_, j_, b_, k_] := GFOffsetDir[u,a,i+j,b,k];
GFOffsetDir[u_, a_, i_, b_, j_, a_, k_] := GFOffsetDir[u,a,i+k,b,j];
GFOffsetDir[u_, a_, i_, b_, j_, b_, k_] := GFOffsetDir[u,a,i,b,j+k];
GFOffsetDir[u_, a_, i_, b_, j_, c_, k_] :=
    ReplacePart[GFOffset[u,0,0,0], {a+1->i, b+1->j, c+1->k}];

MkIfThen[choice_, minval_, maxval_, expr_] :=
  Plus@@Table[IfThen[choice == val, expr[val], 0], {val, minval, maxval}];

MkStencil[u_, coeffs_, a_Integer] :=
  MkIfThen[{ti,tj,tk}[[a]], 0, order,
           Plus@@Table[coeffs[[#+1,i+1]] GFOffsetDir[u, a, i-#],
                       {i,0,order}]&];

MkStencilWide[u_, coeffs_, a_Integer] :=
  MkIfThen[{ti,tj,tk}[[a]], 0, order,
           Plus@@Table[coeffs[[#+1,i+2]] GFOffsetDir[u, a, i-#],
                       {i,-1,order+1}]&];

MkStencil[u_, coeffs1_, a_Integer, coeffs2_, b_Integer] :=
  MkIfThen[{ti,tj,tk}[[a]], 0, order,
           Function[{ii},
                    MkIfThen[{ti,tj,tk}[[b]], 0, order,
                             Function[{jj},
                                      Plus@@Plus@@Table[
                                        coeffs1[[ii+1,i+1]] coeffs2[[jj+1,j+1]]
                                        GFOffsetDir[u, a, i-ii, b, j-jj],
                                        {j,0,order}, {i,0,order}]]]]];

(* TODO: Get Erik to check this: *)
MkStencil[u_, coeffs1_, a_Integer, coeffs2_, b_Integer, coeffs3_, c_Integer] :=
    MkIfThen[
	{ti,tj,tk}[[a]], 0, order,
	Function[{ii},
		 MkIfThen[
		     {ti,tj,tk}[[b]], 0, order,
		     Function[{jj},
			      MkIfThen[
				  {ti,tj,tk}[[c]], 0, order,
				  Function[{kk},
					   Plus@@Plus@@Plus@@Table[
					       coeffs1[[ii+1,i+1]]*
					       coeffs2[[jj+1,j+1]]*
					       coeffs3[[kk+1,k+1]]*
					       GFOffsetDir[u,a,i-ii,b,j-jj,c,k-kk],
					       {k,0,order},
					       {j,0,order},
					       {i,0,order}]]]]]]];
							      


(* Derivative API *)

(* Note: We need the dummy argument u (that is unused) to pacify Kranc *)
Node[u_, a_Integer] :=
  ({cctkOriginSpace1,cctkOriginSpace2,cctkOriginSpace3}[[a]] +
   {dx,dy,dz}[[a]] *
   ({cctkLbnd1+i,cctkLbnd2+j,cctkLbnd3+k}[[a]] - ({ti,tj,tk}[[a]] + 1/2) +
    (MkIfThen[{ti,tj,tk}[[a]], 0, order, nodes[[#+1]]&] - xmin) /
    (xmax - xmin) * (order+1)));

(* Note: The average weight of a grid point must be 1 *)
Weight[u_, a_Integer] :=
  (xmax-xmin) / (order+1)^0 *
  MkIfThen[{ti,tj,tk}[[a]], 0, order, weights[[#+1]]&];

PDelem[u_, a_Integer] :=
  (xmax-xmin) / ((order+1) {dx,dy,dz}[[a]]) MkStencil[u, dcoeffs, a];
PDelem[u_, a_Integer, b_Integer] :=
  (xmax-xmin) / ((order+1) {dx,dy,dz}[[a]] {dx,dy,dz}[[b]]) *
  MkStencil[u, dcoeffs, a, dcoeffs, b];

Boundary[u_, a_Integer] :=
  (xmax-xmin) / ((order+1) {dx,dy,dz}[[a]]) MkStencil[u, bcoeffs, a];

Jump[u_, a_Integer] :=
  (xmax-xmin) / ((order+1) {dx,dy,dz}[[a]]) MkStencilWide[u, jcoeffs, a];

DJump[u_, a_Integer, b_Integer] :=
  (xmax-xmin) / ((order+1) {dx,dy,dz}[[a]] {dx,dy,dz}[[b]]) *
  MkStencilWide[u, jcoeffs, a, dcoeffs, b];

JumpExtra[u_, a_Integer] :=
  (xmax-xmin) / ((order+1) {dx,dy,dz}[[a]]) MkStencilWide[u, kcoeffs, a];

(* The truncated derivative terms operators *)
DTrunc[u_,a_Integer] :=
    (xmax-xmin) / ((order+1) {dx,dy,dz}[[a]]) MkStencilWide[u,dtrunccoeffs,a];
JumpTrunc[u_,a_Integer] :=
    (xmax-xmin) / ((order+1) {dx,dy,dz}[[a]]) MkStencilWide[u,jumptrunccoeffs,a];

(* TODO: Only expand this if u is a tensor component, not if u is
   still a tensor *)
Filter[u_, a_Integer] := (xmax-xmin) / (order+1) MkStencil[u, fcoeffs, a];
(*
RHSFilter[u_, a_Integer] :=
  ((xmax-xmin) / ((order+1) {dx,dy,dz}[[a]]) epsDiss *
   MkStencil[u, rhsfcoeffs, a] +
   (xmax-xmin) / ((order+1) {dx,dy,dz}[[a]]) epsJump *
   MkStencilWide[u, pcoeffs, a]);
*)
RHSFilter[u_, a_Integer] :=
    (1/{dx,dy,dz}[[a]])*epsDiss*MkStencil[u, rhsfcoeffs, a];

(* NOTE: These dissipation operators are not SBP. Instead, construct
   some as a filter, and apply to the RHS.

   These operators not in use.
*)
(*
Diss[u_, a_Integer] :=
  (((xmax-xmin) / (order+1))^order2 / {dx,dy,dz}[[a]] epsDiss *
   MkStencil[u, ecoeffs, a] +
   (xmax-xmin) / ((order+1) {dx,dy,dz}[[a]]) epsJump *
   MkStencilWide[u, pcoeffs, a]);
*)

(* use Deriv instead of PD since PD is special in Kranc *)
(* Deriv[u_, a_] := PDelem[u,a] - alphaDeriv Jump[u,a]; *)
Deriv[u_, a_] := PDelem[u,a] - (1/2) Boundary[u,a] - alphaDeriv JumpExtra[u,a]; 

(* Trunaction operator *)
(* trunccoeffs = Table[0,{i,0,order},{j,0,order}];*)
Trunc[u_,a_Integer] := MkStencil[u,trunccoeffs,a];
Trunc[u_,a_Integer,b_Integer] := MkStencil[u,
					   trunccoeffs, a,
					   trunccoeffs, b];
Trunc[u_,a_Integer,b_Integer,c_Integer] :=
    MkStencil[u,
	      trunccoeffs, a,
	      trunccoeffs, b,
	      trunccoeffs, c];
(* Trunc[u_] := Trunc[u,1,2,3]; *)
Trunc[u_] := Trunc[u,1]; (* Truncation is along x-axis only *)

(* A special derivative used for auxiliary variables, where the
   highest-order mode is truncated away after differentiation. *)
TruncDeriv[u_,a_] := DTrunc[u,a] - alphaDeriv JumpTrunc[u,a];

Weight[u_] = Weight[u,1] Weight[u,2] Weight[u,3];

(* Diss[u_] = Diss[u,1] + Diss[u,2] + Diss[u,3]; *)
Diss[u_] = RHSFilter[u,1] + RHSFilter[u,2] + RHSFilter[u,3];
