(* Authors:
   Jonah Miller <jmiller@perimeterinstitute.ca>
   Erik Schnetter <eschnetter@perimeterinstitute.ca>
   *)

(* Boilerplate setup *)
SetDebugLevel[InfoFull];
SetEnhancedTimes[False];
SetSourceLanguage["C"];

(******************************************************************************)
(* Prelude *)
(******************************************************************************)

KD = KroneckerDelta;
dropNull[list_] := Cases[list, Except[Null]];

(******************************************************************************)
(* User choices *)
(******************************************************************************)

Switch[Environment["ML_DERIVATIVES"],
       "FD", derivs=FD,
       "DG", derivs=DG,
       _, ThrowError["Environment variable ML_DERIVATIVES has wrong value '" <>
                     Environment["ML_DERIVATIVES"] <> "'"]];

(* Polynomial order *)
order = ToExpression[Environment["ML_ORDER"]];
(* TODO: Check that order is a positive integer *)

thorn = "ML_WaveToy_" <> ToString[derivs] <> ToString[order];

(******************************************************************************)
(* Derivatives *)
(******************************************************************************)

Switch[derivs,
       FD, Get["FDops.m"],
       DG, Get["DGops.m"]];

LD = Deriv;

(*
   If we are using DG methods, we have a separate operator for
   derivatives that generate u and derivatives that generate rho = du/dt.
   LDTrunc is for derivatives that generate rho
 *)

Switch[derivs,
       FD,LDTrunc=Deriv,
       DG,LDTrunc=TruncDeriv];

(******************************************************************************)
(* Tensors *)
(******************************************************************************)

(* Register the tensor quantities with the TensorTools package *)
Map[DefineTensor, {u, rho, v, du,
                   ua0, rhoa, va,
                   LDu, LDrho, LDv, LDdu, PDu, PDrho, PDv, PDdu,
                   w,
                   energy,
                   erru, errenergy,
                   J, myDetJ,sqrtDetJ,
		   weight}];

(* Note: J[ua,li] = dx^a/dx^i   where a: local, i: global *)

(******************************************************************************)
(* Groups *)
(******************************************************************************)

evolvedGroups = {
  SetGroupName[CreateGroupFromTensor[u    ], "WT_u"  ],
  SetGroupName[CreateGroupFromTensor[rho  ], "WT_rho"],
  SetGroupName[CreateGroupFromTensor[v[la]], "WT_v"  ]};
evaluatedGroups = {
  SetGroupName[CreateGroupFromTensor[du[la]      ], "WT_du"       ],
  SetGroupName[CreateGroupFromTensor[w[la]       ], "WT_w"        ],
  SetGroupName[CreateGroupFromTensor[erru        ], "WT_erru"     ],
  SetGroupName[CreateGroupFromTensor[weight      ], "WT_weight"   ],
  SetGroupName[CreateGroupFromTensor[myDetJ      ], "WT_detJ"     ],
  SetGroupName[CreateGroupFromTensor[errenergy   ], "WT_errenergy"],
  SetGroupName[CreateGroupFromTensor[energy      ], "WT_energy"   ]};

declaredGroups = Join[evolvedGroups, evaluatedGroups];
declaredGroupNames = Map[First, declaredGroups];

groups = declaredGroups;

(******************************************************************************)
(* Grid setup *)
(******************************************************************************)

setupCoordinates = {
  Name      -> thorn<>"_SetupCoordinates",
  Schedule  -> {"IN "<>thorn<>"_CalcCoordinatesGroup"},
  Equations -> {
    x -> Node[0,1],
    y -> Node[0,2],
    z -> Node[0,3],
    r -> Sqrt[x^2 + y^2 + z^2]}};

setupWeights = {
  Name       -> thorn<>"_SetupWeights",
  Schedule   -> {"AS DGFE_SetupWeights IN SetupMask"},
  Shorthands -> {sqrtDetJ},
  Equations  -> {
    myDetJ   -> IfThen[usejacobian,
                       Abs[Det[MatrixOfComponents[J[uq,lb]]]],
                       1],
    sqrtDetJ -> Sqrt[myDetJ],
    weight    -> Weight[1] / sqrtDetJ}};

(******************************************************************************)
(* Initial data *)
(******************************************************************************)

eps = $MachineEpsilon;
mkSeries[expr_, var_, var0_, order_] :=
  IfThen[Abs[var-var0] < eps^(1/(order+1)),
         Normal[Series[expr, {var, var0, order}]],
         expr];

xyz2r = x^2 + y^2 + z^2 -> r^2;
initialGaussian[r_] := amplitude Exp[-1/2 r^2/width^2];
initialGaussian[t_, r_] :=
  ((r+t) initialGaussian[r+t] + (r-t) initialGaussian[r-t]) / (2 r);
initialGaussian[t_, x_, y_, z_] := initialGaussian[t+radius, Sqrt[x^2+y^2+z^2]];

initialGaussianU =
  mkSeries[initialGaussian[t,x,y,z] /. xyz2r // PowerExpand, r, 0, 2];
initialGaussianRho =
  mkSeries[D[initialGaussian[t,x,y,z],t] /. xyz2r // PowerExpand, r, 0, 2];
initialGaussianV1 =
  mkSeries[D[initialGaussian[t,x,y,z],x] /. xyz2r // PowerExpand, r, 0, 2];
initialGaussianV2 =
  mkSeries[D[initialGaussian[t,x,y,z],y] /. xyz2r // PowerExpand, r, 0, 2];
initialGaussianV3 =
  mkSeries[D[initialGaussian[t,x,y,z],z] /. xyz2r // PowerExpand, r, 0, 2];

initialGaussianCalc = {
  Name                 -> thorn<>"_InitialGaussian",
  Schedule             -> {"IN "<>thorn<>"_CalcInitialGroup"},
  ConditionalOnKeyword -> {"initial_data", "Gaussian"},
  Equations            -> {
    u   -> initialGaussianU,
    rho -> initialGaussianRho,
    v1  -> initialGaussianV1,
    v2  -> initialGaussianV2,
    v3  -> initialGaussianV3}};

boundaryGaussianCalc = {
  Name                 -> thorn<>"_BoundaryGaussian",
  Schedule             -> {"IN "<>thorn<>"_CalcBoundaryGroup"},
  ConditionalOnKeyword -> {"boundary_condition", "Gaussian"},
  Where                -> Boundary,
  Equations            -> {
    u   -> initialGaussianU,
    rho -> initialGaussianRho,
    v1  -> initialGaussianV1,
    v2  -> initialGaussianV2,
    v3  -> initialGaussianV3}};

errorGaussianCalc = {
  Name                 -> thorn<>"_ErrorGaussian",
  Schedule             -> {"AT analysis"},
  ConditionalOnKeyword -> {"initial_data", "Gaussian"},
  Where                -> Everywhere,
  Shorthands           -> {ua0, rhoa, va[la]},
  Equations            -> {
    ua0  -> initialGaussianU,
    rhoa -> initialGaussianRho,
    va1  -> initialGaussianV1,
    va2  -> initialGaussianV2,
    va3  -> initialGaussianV3,
    erru      -> u - ua0,
    errenergy -> 1/2 ((rho-rhoa)^2 +
                      KD[ua,ub] (v[la] - va[la]) (v[lb] - va[lb]))}};

initialStanding[t_, x_, y_, z_] :=
  amplitude Cos[k x] Cos[k y] Cos[k z] Cos[omega t];
initialStandingCalc = {
  Name                 -> thorn<>"_InitialStanding",
  Schedule             -> {"IN "<>thorn<>"_CalcInitialGroup"},
  ConditionalOnKeyword -> {"initial_data", "standing"},
  Shorthands           -> {k, omega},
  Equations            -> {
    k     -> 2 Pi / width,
    omega -> Sqrt[3 k^2],
    u     -> initialStanding[t,x,y,z],
    rho   -> D[initialStanding[t,x,y,z],t],
    v1    -> D[initialStanding[t,x,y,z],x],
    v2    -> D[initialStanding[t,x,y,z],y],
    v3    -> D[initialStanding[t,x,y,z],z]},
  (* otherwise, this takes a very long time *)
  NoSimplify -> True};

boundaryStandingCalc = {
  Name                 -> thorn<>"_BoundaryStanding",
  Schedule             -> {"IN "<>thorn<>"_CalcBoundaryGroup"},
  ConditionalOnKeyword -> {"boundary_condition", "standing"},
  Where                -> Boundary,
  Shorthands           -> {k, omega},
  Equations            -> {
    k     -> 2 Pi / width,
    omega -> Sqrt[3 k^2],
    u     -> initialStanding[t,x,y,z],
    rho   -> D[initialStanding[t,x,y,z],t],
    v1    -> D[initialStanding[t,x,y,z],x],
    v2    -> D[initialStanding[t,x,y,z],y],
    v3    -> D[initialStanding[t,x,y,z],z]},
  (* otherwise, this takes a very long time *)
  NoSimplify -> True};

RHSBoundaryStandingCalc = {
  Name                 -> thorn<>"_RHSBoundaryStanding",
  Schedule             -> {"IN "<>thorn<>"_CalcRHSGroup"},
  ConditionalOnKeyword -> {"rhs_boundary_condition", "standing"},
  Where                -> Boundary,
  Shorthands           -> {k, omega},
  Equations            -> {
    k        -> 2 Pi / width,
    omega    -> Sqrt[3 k^2],
    dot[u]   -> D[initialStanding[t,x,y,z],t],
    dot[rho] -> D[initialStanding[t,x,y,z],t,t],
    dot[v1]  -> D[initialStanding[t,x,y,z],t,x],
    dot[v2]  -> D[initialStanding[t,x,y,z],t,y],
    dot[v3]  -> D[initialStanding[t,x,y,z],t,z]},
  (* otherwise, this takes a very long time *)
  NoSimplify -> True};

initialSine[t_, x_, y_, z_] := amplitude Sin[k x + k y + k z - omega t];
initialSineCalc = {
  Name                 -> thorn<>"_InitialSine",
  Schedule             -> {"IN "<>thorn<>"_CalcInitialGroup"},
  ConditionalOnKeyword -> {"initial_data", "sine"},
  Shorthands           -> {k, omega},
  Equations            -> {
    k     -> 2 Pi / width,
    omega -> Sqrt[3 k^2],
    u     -> initialSine[t,x,y,z],
    rho   -> D[initialSine[t,x,y,z],t],
    v1    -> D[initialSine[t,x,y,z],x],
    v2    -> D[initialSine[t,x,y,z],y],
    v3    -> D[initialSine[t,x,y,z],z]},
  (* otherwise, this takes a very long time *)
  NoSimplify -> True};

initialSineX[t_, x_, y_, z_] := amplitude Sin[kx x - omega t];
initialSineXCalc = {
  Name                 -> thorn<>"_InitialSineX",
  Schedule             -> {"IN "<>thorn<>"_CalcInitialGroup"},
  ConditionalOnKeyword -> {"initial_data", "sinex"},
  Shorthands           -> {kx, omega},
  Equations            -> {
    kx    -> 2 Pi / width,
    omega -> Sqrt[kx^2],
    u     -> initialSineX[t,x,y,z],
    rho   -> D[initialSineX[t,x,y,z],t],
    v1    -> D[initialSineX[t,x,y,z],x],
    v2    -> D[initialSineX[t,x,y,z],y],
    v3    -> D[initialSineX[t,x,y,z],z]},
  (* otherwise, this takes a very long time *)
  NoSimplify -> True};

boundarySineXCalc = {
  Name                 -> thorn<>"_BoundarySineX",
  Schedule             -> {"IN "<>thorn<>"_CalcBoundaryGroup"},
  ConditionalOnKeyword -> {"boundary_condition", "sinex"},
  Where                -> Boundary,
  Shorthands           -> {kx, omega},
  Equations            -> {
    kx     -> 2 Pi / width,
    omega  -> Sqrt[kx^2],
    u      -> initialSineX[t,x,y,z],
    rho    -> D[initialSineX[t,x,y,z],t],
    v1     -> D[initialSineX[t,x,y,z],x],
    v2     -> D[initialSineX[t,x,y,z],y],
    v3     -> D[initialSineX[t,x,y,z],z]},
  (* otherwise, this takes a very long time *)
  NoSimplify -> True};

dependentBoundarySineXCalc = {
  Name                 -> thorn<>"_DependentBoundarySineX",
  Schedule             -> {"IN "<>thorn<>"_CalcDependentGroup"},
  ConditionalOnKeyword -> {"dependent_boundary_condition", "sinex"},
  Where                -> Boundary,
  Shorthands           -> {kx, omega},
  Equations            -> {
    kx    -> 2 Pi / width,
    omega -> Sqrt[kx^2],
    du1   -> D[initialSineX[t,x,y,z],x],
    du2   -> D[initialSineX[t,x,y,z],y],
    du3   -> D[initialSineX[t,x,y,z],z]},
  (* otherwise, this takes a very long time *)
  NoSimplify -> True};

errorSineXCalc = {
  Name                 -> thorn<>"_ErrorSineX",
  Schedule             -> {"AT analysis"},
  ConditionalOnKeyword -> {"initial_data", "sinex"},
  Where                -> Everywhere,
  Shorthands           -> {kx, omega, ua0, rhoa, va[la]},
  Equations            -> {
    kx        -> 2 Pi / width,
    omega     -> Sqrt[kx^2],
    ua0       -> initialSineX[t,x,y,z],
    rhoa      -> D[initialSineX[t,x,y,z],t],
    va1       -> D[initialSineX[t,x,y,z],x],
    va2       -> D[initialSineX[t,x,y,z],y],
    va3       -> D[initialSineX[t,x,y,z],z],
    erru      -> u - ua0,
    errenergy -> 1/2 ((rho-rhoa)^2 +
                      KD[ua,ub] (v[la] - va[la]) (v[lb] - va[lb]))}};

(******************************************************************************)
(* Boundary conditions *)
(******************************************************************************)

boundaryZeroCalc = {
  Name                 -> thorn<>"_BoundaryZero",
  Schedule             -> {"IN "<>thorn<>"_CalcBoundaryGroup"},
  ConditionalOnKeyword -> {"boundary_condition", "zero"},
  Where                -> Boundary,
  Equations            -> {
    u     -> 0,
    rho   -> 0,
    v[la] -> 0}};

(******************************************************************************)
(* Dependent variables *)
(******************************************************************************)

dependentFirstOrderCalc = {
  Name                 -> thorn<>"_DependentFirstOrder",
  Schedule             -> {"IN "<>thorn<>"_CalcDependentGroup"},
  ConditionalOnKeyword -> {"formulation", "first_order"},
  Where                -> Interior,
  Equations            -> {
    du[la] -> 0}};

dependentSecondOrderCalc = {
  Name                 -> thorn<>"_DependentSecondOrder",
  Schedule             -> {"IN "<>thorn<>"_CalcDependentGroup"},
  ConditionalOnKeyword -> {"formulation", "second_order"},
  Where                -> Interior,
  Shorthands           -> {LDu[lq], PDu[la]},
  Equations            -> {
    LDu[lq] -> LD[u,lq],
    PDu[la] -> IfThen[usejacobian, J[uq,la] LDu[lq], KD[uq,la] LDu[lq]],
    du[la]  -> PDu[la]},
  NoSimplify -> True};

(******************************************************************************)
(* Dependent boundary conditions *)
(******************************************************************************)

dependentSecondOrderBoundaryCalc = {
  Name                 -> thorn<>"_DependentSecondOrderBoundary",
  Schedule             -> {"IN "<>thorn<>"_CalcDependentGroup"},
  ConditionalOnKeyword -> {"dependent_boundary_condition", "zero"},
  Where                -> Boundary,
  Equations            -> {
    du[la] -> 0}};

(******************************************************************************)
(* Evolution equations *)
(******************************************************************************)

RHSFirstOrderCalc = {
  Name                 -> thorn<>"_RHSFirstOrder",
  Schedule             -> {"IN "<>thorn<>"_CalcRHSGroup"},
  ConditionalOnKeyword -> {"formulation", "first_order"},
  Where                -> Interior,
  Shorthands           -> {LDrho[lq], LDv[la,lq], PDrho[la], PDv[la,lb]},
  Equations            -> {
    LDrho[lq]  -> LD[rho,lq],
    LDv[la,lq] -> LD[v[la],lq],
    PDrho[la]  -> IfThen[usejacobian, J[uq,la] LDrho[lq], KD[uq,la] LDrho[lq]],
    PDv[la,lb] -> IfThen[usejacobian,
                         J[uq,lb] LDv[la,lq],
                         KD[uq,lb] LDv[la,lq]],
    dot[u]     -> rho + IfThen[usejacobian,
			       myDetJ Diss[u],
			       Diss[u]],
    dot[rho]   -> KD[ua,ub] PDv[la,lb] + IfThen[usejacobian,
						myDetJ Diss[rho],
						Diss[rho]],
    dot[v[la]] -> PDrho[la] + IfThen[usejacobian,
				     myDetJ Diss[v[la]],
				     Diss[v[la]]]}};

RHSSecondOrderCalc = {
  Name                 -> thorn<>"_RHSSecondOrder",
  Schedule             -> {"IN "<>thorn<>"_CalcRHSGroup"},
  ConditionalOnKeyword -> {"formulation", "second_order"},
  Where                -> Interior,
  Shorthands           -> {LDdu[la,lq], PDdu[la,lb]},
  Equations            -> {
    LDdu[la,lq] -> LDTrunc[du[la],lq],
    PDdu[la,lb] -> IfThen[usejacobian,
                          J[uq,lb] LDdu[la,lq],
                          KD[uq,lb] LDdu[la,lq]],
    dot[u]     -> rho + IfThen[usejacobian,
			       myDetJ Diss[u],
			       Diss[u]],
    dot[rho]   -> KD[ua,ub] PDdu[la,lb] + IfThen[usejacobian,
						 myDetJ Diss[rho],
						 Diss[rho]],
    dot[v[la]] -> 0},
  NoSimplify -> True};

(******************************************************************************)
(* RHS boundary conditions *)
(******************************************************************************)

RHSBoundaryZeroCalc = {
  Name                 -> thorn<>"_RHSBoundaryZero",
  Schedule             -> {"IN "<>thorn<>"_CalcRHSGroup"},
  ConditionalOnKeyword -> {"rhs_boundary_condition", "zero"},
  Where                -> Boundary,
  Equations            -> {
    dot[u]     -> 0,
    dot[rho]   -> 0,
    dot[v[la]] -> 0}};

(******************************************************************************)
(* Constraint equations *)
(******************************************************************************)

constraintsFirstOrderCalc = {
  Name                 -> thorn<>"_ConstraintsFirstOrder",
  Schedule             -> {"IN "<>thorn<>"_CalcConstraintsGroup"},
  ConditionalOnKeyword -> {"formulation", "first_order"},
  Where                -> Interior,
  Shorthands           -> {LDv[la,lq], PDv[la,lb]},
  Equations            -> {
    LDv[la,lq] -> LD[v[la],lq],
    PDv[la,lb] -> IfThen[usejacobian,
                         J[uq,lb] LDv[la,lq],
                         KD[uq,lb] LDv[la,lq]],
    w[la] -> Eps[la,ub,uc] PDv[lb,lc]}};

constraintsSecondOrderCalc = {
  Name                 -> thorn<>"_ConstraintsSecondOrder",
  Schedule             -> {"IN "<>thorn<>"_CalcConstraintsGroup"},
  ConditionalOnKeyword -> {"formulation", "second_order"},
  Where                -> Interior,
  Shorthands           -> {LDdu[la,lq], PDdu[la,lb]},
  Equations            -> {
    LDdu[la,lq] -> LD[du[la],lq],
    PDdu[la,lb] -> IfThen[usejacobian,
                          J[uq,lb] LDdu[la,lq],
                          KD[uq,lb] LDdu[la,lq]],
    w[la] -> Eps[la,ub,uc] PDdu[lb,lc]}};

constraintsBoundaryCalc = {
  Name      -> thorn<>"_ConstraintsBoundary",
  Schedule  -> {"IN "<>thorn<>"_CalcConstraintsGroup"},
  Where     -> Boundary,
  Equations -> {
    w[la] -> 0}};

(******************************************************************************)
(* Analysis routines *)
(******************************************************************************)

energyFirstOrderCalc = {
  Name                 -> thorn<>"_EnergyFirstOrder",
  Schedule             -> {"IN "<>thorn<>"_CalcEnergyGroup"},
  ConditionalOnKeyword -> {"formulation", "first_order"},
  Where                -> Interior,
  Equations            -> {
    energy   -> 1/2 (rho^2 + KD[ua,ub] v[la] v[lb])}};

energySecondOrderCalc = {
  Name                 -> thorn<>"_EnergySecondOrder",
  Schedule             -> {"IN "<>thorn<>"_CalcEnergyGroup"},
  ConditionalOnKeyword -> {"formulation", "second_order"},
  Where                -> Interior,
  Equations            -> {
    energy   -> 1/2 (rho^2 + KD[ua,ub] du[la] du[lb])}};

energyBoundaryCalc = {
  Name      -> thorn<>"_EnergyBoundary",
  Schedule  -> {"IN "<>thorn<>"_CalcEnergyGroup"},
  Where     -> Boundary,
  Equations -> {
    energy -> 0}};

(******************************************************************************)
(* Parameters *)
(******************************************************************************)

keywordParameters = {
  {
    Name          -> "initial_data",
    AllowedValues -> {"Gaussian", "standing", "sine", "sinex"},
    Default       -> "Gaussian"},
  {
    Name          -> "formulation",
    AllowedValues -> {"first_order", "second_order"},
    Default       -> "first_order"},
  {
    Name          -> "boundary_condition",
    AllowedValues -> {"none", "zero", "Gaussian", "standing", "sinex"},
    Default       -> "none"},
  {
    Name          -> "dependent_boundary_condition",
    AllowedValues -> {"none", "zero", "sinex"},
    Default       -> "none"},
  {
    Name          -> "rhs_boundary_condition",
    AllowedValues -> {"standing", "zero"},
    Default       -> "zero"}};

intParameters = {};

realParameters = {
  {
    Name        -> amplitude,
    Description -> "Amplitude of initial data",
    Default     -> 1.0},
  {
    Name        -> radius,
    Description -> "Radius of initial data",
    Default     -> 0.0},
  {
    Name        -> width,
    Description -> "Width of initial data",
    Default     -> 1.0},
  {
    Name        -> alphaDeriv,
    Description -> "Jump term (penalty) strength in flux derivatives",
    Default     -> 0.5},
  {
    Name        -> epsDiss,
    Description -> "Dissipation strength",
    Default     -> 0.0},
  {
    Name        -> epsJump,
    Description -> "Penalty strength in dissipation",
    Default     -> 0.0}};

(******************************************************************************)
(* Construct the thorns *)
(******************************************************************************)

calculations = {
  setupCoordinates, setupWeights,
  initialGaussianCalc, boundaryGaussianCalc, errorGaussianCalc,
  initialStandingCalc, boundaryStandingCalc, RHSBoundaryStandingCalc,
  initialSineCalc,
  initialSineXCalc, boundarySineXCalc, dependentBoundarySineXCalc,
  errorSineXCalc,
  boundaryZeroCalc, 
  dependentFirstOrderCalc, dependentSecondOrderCalc,
  dependentSecondOrderBoundaryCalc,
  RHSFirstOrderCalc, RHSSecondOrderCalc, RHSBoundaryZeroCalc,
  constraintsFirstOrderCalc, constraintsSecondOrderCalc,
  constraintsBoundaryCalc,
  energyFirstOrderCalc, energySecondOrderCalc, energyBoundaryCalc} // dropNull;

CreateKrancThornTT[
  groups, ".", thorn,
  Calculations             -> calculations,
  DeclaredGroups           -> declaredGroupNames,
  KeywordParameters        -> keywordParameters,
  IntParameters            -> intParameters,
  RealParameters           -> realParameters,
  MergeFiles               -> "WaveToy.merge",
  CountOperations          -> True,
  Tile                     -> derivs===DG,
  UseJacobian              -> True,
  UseLoopControl           -> False,
  UseVectors               -> False];
