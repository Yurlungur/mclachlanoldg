#! /bin/bash

# Abort on errors
set -e

script=$1

if test -z "$script"; then
    echo "Usage:"
    echo "$0 <script.m>"
    exit 2
fi

outfile=$2

if test -z "$outfile"; then
    outfile=$(basename $script .m)
fi

error=$outfile.err
output=$outfile.out

rm -f $output

# Run Kranc to regenerate the code
../../../repos/Kranc/Bin/kranc $script | tee $error
[ $PIPESTATUS -eq 0 ] || exit $PIPESTATUS

mv $error $output
