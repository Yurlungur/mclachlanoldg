(* Boilerplate setup *)

SetDebugLevel[InfoFull];
SetEnhancedTimes[False];
SetSourceLanguage["C"];

(******************************************************************************)
(* Prelude *)
(******************************************************************************)

KD = KroneckerDelta;

(******************************************************************************)
(* User choices *)
(******************************************************************************)

Switch[Environment["ML_DERIVATIVES"],
       "FD", derivs=FD,
       "DG", derivs=DG,
       _, ThrowError["Environment variable ML_DERIVATIVES has wrong value '" <>
                     Environment["ML_DERIVATIVES"] <> "'"]];

(* Polynomial order *)
order = ToExpression[Environment["ML_ORDER"]];
(* TODO: Check that order is a positive integer *)

thorn = "ML_Test_" <> ToString[derivs] <> ToString[order];

(******************************************************************************)
(* Derivatives *)
(******************************************************************************)

Switch[derivs,
       FD, Get["FDops.m"],
       DG, Get["DGops.m"]];

LD = Deriv;
Dissipation[u_] := IfThen[usejacobian, Abs[myDetJ] Diss[u], Diss[u]];

(******************************************************************************)
(* Tensors *)
(******************************************************************************)

(* Register the tensor quantities with the TensorTools package *)
Map[DefineTensor, {weightgf, weight,
                   p0, p1, p2, p3, p4, p5, p6,p7,p8,
                   dp0, dp1, dp2, dp3, dp4, dp5, dp6,
		   p0Trunc,p1Trunc,p2Trunc,p3Trunc,
		   p4Trunc,p5Trunc,p6Trunc,
  		   p0Temp,p1Temp,p2Temp,p3Temp,
		   p4Temp,p5Temp,p6Temp,
                   ddp0, ddp1, ddp2, ddp3, ddp4, ddp5, ddp6,
		   dissp5,dissp7,dissp8,
                   J,myDetJ}];

AssertSymmetricIncreasing[ddp0[la,lb], la,lb];
AssertSymmetricIncreasing[ddp1[la,lb], la,lb];
AssertSymmetricIncreasing[ddp2[la,lb], la,lb];
AssertSymmetricIncreasing[ddp3[la,lb], la,lb];
AssertSymmetricIncreasing[ddp4[la,lb], la,lb];
AssertSymmetricIncreasing[ddp5[la,lb], la,lb];
AssertSymmetricIncreasing[ddp6[la,lb], la,lb];

(* Note: J[ua,li] = dx^a/dx^i   where a: local, i: global *)

(******************************************************************************)
(* Groups *)
(******************************************************************************)

evolvedGroups = {};
evaluatedGroups = {
  SetGroupName[CreateGroupFromTensor[weightgf[la]], "WT_weightgf" ],
  SetGroupName[CreateGroupFromTensor[weight      ], "WT_weight"   ],
  SetGroupName[CreateGroupFromTensor[myDetJ      ], "WT_detJ"     ],
  SetGroupName[CreateGroupFromTensor[p0          ], "WT_p0"       ],
  SetGroupName[CreateGroupFromTensor[p1          ], "WT_p1"       ],
  SetGroupName[CreateGroupFromTensor[p2          ], "WT_p2"       ],
  SetGroupName[CreateGroupFromTensor[p3          ], "WT_p3"       ],
  SetGroupName[CreateGroupFromTensor[p4          ], "WT_p4"       ],
  SetGroupName[CreateGroupFromTensor[p5          ], "WT_p5"       ],
  SetGroupName[CreateGroupFromTensor[p6          ], "WT_p6"       ],
  SetGroupName[CreateGroupFromTensor[p7          ], "WT_p7"       ],
  SetGroupName[CreateGroupFromTensor[p8          ], "WT_p8"       ],
  SetGroupName[CreateGroupFromTensor[dp0[la]     ], "WT_dp0"      ],
  SetGroupName[CreateGroupFromTensor[dp1[la]     ], "WT_dp1"      ],
  SetGroupName[CreateGroupFromTensor[dp2[la]     ], "WT_dp2"      ],
  SetGroupName[CreateGroupFromTensor[dp3[la]     ], "WT_dp3"      ],
  SetGroupName[CreateGroupFromTensor[dp4[la]     ], "WT_dp4"      ],
  SetGroupName[CreateGroupFromTensor[dp5[la]     ], "WT_dp5"      ],
  SetGroupName[CreateGroupFromTensor[dp6[la]     ], "WT_dp6"      ],
  SetGroupName[CreateGroupFromTensor[p0Trunc     ], "WT_p0Trunc"  ],
  SetGroupName[CreateGroupFromTensor[p1Trunc     ], "WT_p1Trunc"  ],
  SetGroupName[CreateGroupFromTensor[p2Trunc     ], "WT_p2Trunc"  ],
  SetGroupName[CreateGroupFromTensor[p3Trunc     ], "WT_p3Trunc"  ],
  SetGroupName[CreateGroupFromTensor[p4Trunc     ], "WT_p4Trunc"  ],
  SetGroupName[CreateGroupFromTensor[p5Trunc     ], "WT_p5Trunc"  ],
  SetGroupName[CreateGroupFromTensor[p6Trunc     ], "WT_p6Trunc"  ],
  SetGroupName[CreateGroupFromTensor[p0Temp      ], "WT_p0Temp"   ],
  SetGroupName[CreateGroupFromTensor[p1Temp      ], "WT_p1Temp"   ],
  SetGroupName[CreateGroupFromTensor[p2Temp      ], "WT_p2Temp"   ],
  SetGroupName[CreateGroupFromTensor[p3Temp      ], "WT_p3Temp"   ],
  SetGroupName[CreateGroupFromTensor[p4Temp      ], "WT_p4Temp"   ],
  SetGroupName[CreateGroupFromTensor[p5Temp      ], "WT_p5Temp"   ],
  SetGroupName[CreateGroupFromTensor[p6Temp      ], "WT_p6Temp"   ],
  SetGroupName[CreateGroupFromTensor[ddp0[la,lb] ], "WT_ddp0"     ],
  SetGroupName[CreateGroupFromTensor[ddp1[la,lb] ], "WT_ddp1"     ],
  SetGroupName[CreateGroupFromTensor[ddp2[la,lb] ], "WT_ddp2"     ],
  SetGroupName[CreateGroupFromTensor[ddp3[la,lb] ], "WT_ddp3"     ],
  SetGroupName[CreateGroupFromTensor[ddp4[la,lb] ], "WT_ddp4"     ],
  SetGroupName[CreateGroupFromTensor[ddp5[la,lb] ], "WT_ddp5"     ],
  SetGroupName[CreateGroupFromTensor[ddp6[la,lb] ], "WT_ddp6"     ],
  SetGroupName[CreateGroupFromTensor[dissp5      ], "WT_dissp5"   ],
  SetGroupName[CreateGroupFromTensor[dissp7      ], "WT_dissp7"   ],
  SetGroupName[CreateGroupFromTensor[dissp8      ], "WT_dissp8"   ]};

extraGroups = {{"grid::coordinates", {x,y,z,r}}}

declaredGroups = Join[evolvedGroups, evaluatedGroups];
declaredGroupNames = Map[First, declaredGroups];

groups = Join[declaredGroups,extraGroups];

(******************************************************************************)
(* Parameters *)
(******************************************************************************)

realParameters = {
  {
    Name        -> alphaDeriv,
    Description -> "Jump term (penalty) strength in derivatives",
    Default     -> 0.5
  },
  {
    Name        -> epsDiss,
    Description -> "Dissipation strength",
    Default     -> 0.2
  }};

(******************************************************************************)
(* Grid setup *)
(******************************************************************************)

setupCoordinates = {
  Name      -> thorn<>"_SetupCoordinates",
  Schedule  -> {"IN "<>thorn<>"_CalcCoordinatesGroup"},
  Equations -> {
    x        -> Node[0,1],
    y        -> Node[0,2],
    z        -> Node[0,3],
    r        -> Sqrt[x^2 + y^2 + z^2]}};

(*
   TODO: use the volume form provided Llama.
   Kranc won't let us use over-write the detJ
   variable, but accessing detJ results in a
   segfault.

   I suspect I'm missing something simple,
   but I have no idea where to look to figure
   it out.
 *)
setupDetJ = {
  Name       -> thorn<>"_SetupDetJ",
  Schedule   -> {"IN "<>thorn<>"_CalcCoordinatesGroup AFTER "<>thorn<>"_SetupCoordinates"},
  Shorthands -> {sqrtDetJ},
  Equations  -> {

      myDetJ   -> IfThen[usejacobian,
			 Det[MatrixOfComponents[J[uq,lb]]],
			 1]}};

(******************************************************************************)
(* Test routines *)
(******************************************************************************)

weightgfCalc = {
  Name      -> thorn<>"_WeightgfCalc",
  Schedule  -> {"AT initial"},
  Where     -> Everywhere,
  Equations -> {
    weightgf[la] -> Weight[1,la]}};

polyCalc = {
  Name      -> thorn<>"_PolyCalc",
  Schedule  -> {"AT initial"},
  Where     -> Everywhere,
  Equations -> {
    p0 -> x^0,
    p1 -> x^1,
    p2 -> x^2,
    p3 -> x^3,
    p4 -> x^4,
    p5 -> x^5,
    p6 -> x^6,
    p7 -> x^7,
    p8 -> x^8}};

dPolyCalc = {
  Name      -> thorn<>"_DPolyCalc",
  Schedule  -> {"AT initial AFTER "<>thorn<>"_PolyCalc"},
  Where     -> Interior,
  Equations -> {
    dp0[la] -> IfThen[usejacobian, J[uq,la] LD[p0, lq], KD[uq,la] LD[p0, lq]],
    dp1[la] -> IfThen[usejacobian, J[uq,la] LD[p1, lq], KD[uq,la] LD[p1, lq]],
    dp2[la] -> IfThen[usejacobian, J[uq,la] LD[p2, lq], KD[uq,la] LD[p2, lq]],
    dp3[la] -> IfThen[usejacobian, J[uq,la] LD[p3, lq], KD[uq,la] LD[p3, lq]],
    dp4[la] -> IfThen[usejacobian, J[uq,la] LD[p4, lq], KD[uq,la] LD[p4, lq]],
    dp5[la] -> IfThen[usejacobian, J[uq,la] LD[p5, lq], KD[uq,la] LD[p5, lq]],
    dp6[la] -> IfThen[usejacobian, J[uq,la] LD[p6, lq], KD[uq,la] LD[p6, lq]]}};

dPolyBoundaryCalc = {
  Name      -> thorn<>"_DPolyBoundaryCalc",
  Schedule  -> {"AT initial"},
  Where     -> Boundary,
  Equations -> {
    dp0[la] -> 0,
    dp1[la] -> 0,
    dp2[la] -> 0,
    dp3[la] -> 0,
    dp4[la] -> 0,
    dp5[la] -> 0,
    dp6[la] -> 0}};

PolyFilterCalcx = {
  Name      -> thorn<>"_PolyFilterCalcx",
  Schedule  -> {"AT initial AFTER "<>thorn<>"_PolyCalc"},
  Where     -> Interior,
  Equations -> {
    p0Trunc -> Trunc[p0,1],
    p1Trunc -> Trunc[p1,1],
    p2Trunc -> Trunc[p2,1],
    p3Trunc -> Trunc[p3,1],
    p4Trunc -> Trunc[p4,1],
    p5Trunc -> Trunc[p5,1],
    p6Trunc -> Trunc[p6,1]}};

PolyFilterCalcy = {
  Name      -> thorn<>"_PolyFilterCalcy",
  Schedule  -> {"AT initial AFTER "<>thorn<>"_PolyFilterCalcx"},
  Where     -> Interior,
  Equations -> {
    p0Temp -> Trunc[p0Trunc,2],
    p1Temp -> Trunc[p1Trunc,2],
    p2Temp -> Trunc[p2Trunc,2],
    p3Temp -> Trunc[p3Trunc,2],
    p4Temp -> Trunc[p4Trunc,2],
    p5Temp -> Trunc[p5Trunc,2],
    p6Temp -> Trunc[p6Trunc,2]}};

PolyFilterCalcz = {
  Name      -> thorn<>"_PolyFilterCalcz",
  Schedule  -> {"AT initial AFTER "<>thorn<>"_PolyFilterCalcy"},
  Where     -> Interior,
  Equations -> {
    p0Trunc -> Trunc[p0Temp,3],
    p1Trunc -> Trunc[p1Temp,3],
    p2Trunc -> Trunc[p2Temp,3],
    p3Trunc -> Trunc[p3Temp,3],
    p4Trunc -> Trunc[p4Temp,3],
    p5Trunc -> Trunc[p5Temp,3],
    p6Trunc -> Trunc[p6Temp,3]}};

PolyFilterBoundaryCalc = {
  Name      -> thorn<>"_PolyFilterBoundaryCalc",
  Schedule  -> {"AT initial"},
  Where     -> Boundary,
  Equations -> {
    p0Trunc -> 0,
    p1Trunc -> 0,
    p2Trunc -> 0,
    p3Trunc -> 0,
    p4Trunc -> 0,
    p5Trunc -> 0,
    p6Trunc -> 0,
    p0Temp  -> 0,
    p1Temp  -> 0,
    p2Temp  -> 0,
    p3Temp  -> 0,
    p4Temp  -> 0,
    p5Temp  -> 0,
    p6Temp  -> 0}};

ddPolyCalc = {
  Name      -> thorn<>"_DDPolyCalc",
  Schedule  -> {"AT initial AFTER "<>thorn<>"_DPolyCalc"},
  Where     -> Interior,
  Equations -> {
    ddp0[la,lb] -> IfThen[usejacobian,
                          J[uq,lb] LD[dp0[la], lq],
                          KD[uq,lb] LD[dp0[la], lq]],
    ddp1[la,lb] -> IfThen[usejacobian,
                          J[uq,lb] LD[dp1[la], lq],
                          KD[uq,lb] LD[dp1[la], lq]],
    ddp2[la,lb] -> IfThen[usejacobian,
                          J[uq,lb] LD[dp2[la], lq],
                          KD[uq,lb] LD[dp2[la], lq]],
    ddp3[la,lb] -> IfThen[usejacobian,
                          J[uq,lb] LD[dp3[la], lq],
                          KD[uq,lb] LD[dp3[la], lq]],
    ddp4[la,lb] -> IfThen[usejacobian,
                          J[uq,lb] LD[dp4[la], lq],
                          KD[uq,lb] LD[dp4[la], lq]],
    ddp5[la,lb] -> IfThen[usejacobian,
                          J[uq,lb] LD[dp5[la], lq],
                          KD[uq,lb] LD[dp5[la], lq]],
    ddp6[la,lb] -> IfThen[usejacobian,
                          J[uq,lb] LD[dp6[la], lq],
                          KD[uq,lb] LD[dp6[la], lq]]}};

ddPolyBoundaryCalc = {
  Name      -> thorn<>"_DDPolyBoundaryCalc",
  Schedule  -> {"AT initial"},
  Where     -> Boundary,
  Equations -> {
    ddp0[la,lb] -> 0,
    ddp1[la,lb] -> 0,
    ddp2[la,lb] -> 0,
    ddp3[la,lb] -> 0,
    ddp4[la,lb] -> 0,
    ddp5[la,lb] -> 0,
    ddp6[la,lb] -> 0}};

dissPolyCalc = {
    Name      -> thorn<>"_DissPolyCalc",
    Schedule  -> {"AT initial AFTER "<>thorn<>"_ddPolyCalc"},
    Where     -> Interior,
    Equations -> {
	dissp5 -> Dissipation[p5],
	dissp7 -> Dissipation[p7],
	dissp8 -> Dissipation[p8]}};

dissPolyBoundaryCalc = {
    Name       -> thorn<>"_DissPolyBoundaryCalc",
    Schedule   -> {"AT initial"},
    Where      -> Boundary,
    Equations  -> {
	dissp5 -> 0,
	dissp7 -> 0,
	dissp8 -> 0}};

(******************************************************************************)
(* Construct the thorns *)
(******************************************************************************)

calculations = {
  setupCoordinates,
  setupDetJ,
  weightgfCalc,
  polyCalc,
  dPolyCalc, dPolyBoundaryCalc,
  PolyFilterCalcx,PolyFilterCalcy,PolyFilterCalcz,
  PolyFilterBoundaryCalc,
  ddPolyCalc, ddPolyBoundaryCalc,
  dissPolyCalc,dissPolyBoundaryCalc};

CreateKrancThornTT[
  groups, ".", thorn,
  Calculations   -> calculations,
  DeclaredGroups -> declaredGroupNames,
  RealParameters -> realParameters,
  MergeFiles     -> "Test.merge",
  Tile           -> derivs===DG,
  UseJacobian    -> True,
  UseLoopControl -> False,
  UseVectors     -> False];
