/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_WaveToy_DG2 {


static void ML_WaveToy_DG2_ErrorGaussian_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_WaveToy_DG2_ErrorGaussian,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL rL CCTK_ATTRIBUTE_UNUSED = r[index];
    CCTK_REAL rhoL CCTK_ATTRIBUTE_UNUSED = rho[index];
    CCTK_REAL uL CCTK_ATTRIBUTE_UNUSED = u[index];
    CCTK_REAL v1L CCTK_ATTRIBUTE_UNUSED = v1[index];
    CCTK_REAL v2L CCTK_ATTRIBUTE_UNUSED = v2[index];
    CCTK_REAL v3L CCTK_ATTRIBUTE_UNUSED = v3[index];
    CCTK_REAL xL CCTK_ATTRIBUTE_UNUSED = x[index];
    CCTK_REAL yL CCTK_ATTRIBUTE_UNUSED = y[index];
    CCTK_REAL zL CCTK_ATTRIBUTE_UNUSED = z[index];
    
    
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL ua0 CCTK_ATTRIBUTE_UNUSED = IfThen(rL < 
      6.055454452393343e-6,-0.166666666666666666666666666667*amplitude*exp(-0.5*pow(radius 
      + t,2)*pow(width,-2))*pow(width,-6)*(6*(2*radius*t + pow(radius,2) + 
      pow(t,2) - pow(width,2))*pow(width,4) + pow(rL,2)*(pow(radius,4) + 
      pow(t,4) + 6*pow(radius,2)*(pow(t,2) - pow(width,2)) - 
      6*pow(t,2)*pow(width,2) + 4*(t*pow(radius,3) + radius*(pow(t,3) - 
      3*t*pow(width,2))) + 3*pow(width,4))),0.5*amplitude*((rL - radius - 
      t)*exp(-0.5*pow(-rL + radius + t,2)*pow(width,-2)) + (rL + radius + 
      t)*exp(-0.5*pow(rL + radius + t,2)*pow(width,-2)))*pow(rL,-1));
    
    CCTK_REAL rhoa CCTK_ATTRIBUTE_UNUSED = IfThen(rL < 
      6.055454452393343e-6,0.166666666666666666666666666667*amplitude*(radius 
      + t)*exp(-0.5*pow(radius + 
      t,2)*pow(width,-2))*pow(width,-8)*(6*(2*radius*t + pow(radius,2) + 
      pow(t,2) - 3*pow(width,2))*pow(width,4) + pow(rL,2)*(pow(radius,4) + 
      pow(t,4) + 2*pow(radius,2)*(3*pow(t,2) - 5*pow(width,2)) - 
      10*pow(t,2)*pow(width,2) + 4*(t*pow(radius,3) + radius*(pow(t,3) - 
      5*t*pow(width,2))) + 
      15*pow(width,4))),0.5*amplitude*pow(rL,-1)*(exp(-0.5*pow(rL + radius + 
      t,2)*pow(width,-2)) - exp(-0.5*pow(rL + radius + 
      t,2)*pow(width,-2))*pow(rL + radius + t,2)*pow(width,-2) + 
      exp(-0.5*pow(-rL + radius + t,2)*pow(width,-2))*(-1 + pow(-rL + radius 
      + t,2)*pow(width,-2))));
    
    CCTK_REAL va1 CCTK_ATTRIBUTE_UNUSED = IfThen(rL < 
      6.055454452393343e-6,-0.0333333333333333333333333333333*xL*amplitude*exp(-0.5*pow(radius 
      + t,2)*pow(width,-2))*pow(width,-10)*(10*pow(width,4)*(pow(radius,4) + 
      pow(t,4) + 6*pow(radius,2)*(pow(t,2) - pow(width,2)) - 
      6*pow(t,2)*pow(width,2) + 4*(t*pow(radius,3) + radius*(pow(t,3) - 
      3*t*pow(width,2))) + 3*pow(width,4)) + pow(rL,2)*(pow(radius,6) + 
      pow(t,6) + 20*pow(radius,3)*(pow(t,3) - 3*t*pow(width,2)) + 
      45*pow(t,2)*pow(width,4) + 15*(pow(radius,4)*(pow(t,2) - pow(width,2)) 
      + pow(radius,2)*(pow(t,4) - 6*pow(t,2)*pow(width,2) + 3*pow(width,4))) 
      + 6*(t*pow(radius,5) + radius*(pow(t,5) - 10*pow(t,3)*pow(width,2) + 
      15*t*pow(width,4))) - 15*(pow(t,4)*pow(width,2) + 
      pow(width,6)))),0.5*xL*pow(rL,-3)*(amplitude*(-((rL - radius - 
      t)*exp(-0.5*pow(-rL + radius + t,2)*pow(width,-2))) - (rL + radius + 
      t)*exp(-0.5*pow(rL + radius + t,2)*pow(width,-2))) + 
      rL*amplitude*(exp(-0.5*pow(-rL + radius + t,2)*pow(width,-2)) + 
      exp(-0.5*pow(rL + radius + t,2)*pow(width,-2)) + (-(exp(-0.5*pow(-rL + 
      radius + t,2)*pow(width,-2))*pow(-rL + radius + t,2)) - exp(-0.5*pow(rL 
      + radius + t,2)*pow(width,-2))*pow(rL + radius + 
      t,2))*pow(width,-2))));
    
    CCTK_REAL va2 CCTK_ATTRIBUTE_UNUSED = IfThen(rL < 
      6.055454452393343e-6,-0.0333333333333333333333333333333*yL*amplitude*exp(-0.5*pow(radius 
      + t,2)*pow(width,-2))*pow(width,-10)*(10*pow(width,4)*(pow(radius,4) + 
      pow(t,4) + 6*pow(radius,2)*(pow(t,2) - pow(width,2)) - 
      6*pow(t,2)*pow(width,2) + 4*(t*pow(radius,3) + radius*(pow(t,3) - 
      3*t*pow(width,2))) + 3*pow(width,4)) + pow(rL,2)*(pow(radius,6) + 
      pow(t,6) + 20*pow(radius,3)*(pow(t,3) - 3*t*pow(width,2)) + 
      45*pow(t,2)*pow(width,4) + 15*(pow(radius,4)*(pow(t,2) - pow(width,2)) 
      + pow(radius,2)*(pow(t,4) - 6*pow(t,2)*pow(width,2) + 3*pow(width,4))) 
      + 6*(t*pow(radius,5) + radius*(pow(t,5) - 10*pow(t,3)*pow(width,2) + 
      15*t*pow(width,4))) - 15*(pow(t,4)*pow(width,2) + 
      pow(width,6)))),0.5*yL*pow(rL,-3)*(amplitude*(-((rL - radius - 
      t)*exp(-0.5*pow(-rL + radius + t,2)*pow(width,-2))) - (rL + radius + 
      t)*exp(-0.5*pow(rL + radius + t,2)*pow(width,-2))) + 
      rL*amplitude*(exp(-0.5*pow(-rL + radius + t,2)*pow(width,-2)) + 
      exp(-0.5*pow(rL + radius + t,2)*pow(width,-2)) + (-(exp(-0.5*pow(-rL + 
      radius + t,2)*pow(width,-2))*pow(-rL + radius + t,2)) - exp(-0.5*pow(rL 
      + radius + t,2)*pow(width,-2))*pow(rL + radius + 
      t,2))*pow(width,-2))));
    
    CCTK_REAL va3 CCTK_ATTRIBUTE_UNUSED = IfThen(rL < 
      6.055454452393343e-6,-0.0333333333333333333333333333333*zL*amplitude*exp(-0.5*pow(radius 
      + t,2)*pow(width,-2))*pow(width,-10)*(10*pow(width,4)*(pow(radius,4) + 
      pow(t,4) + 6*pow(radius,2)*(pow(t,2) - pow(width,2)) - 
      6*pow(t,2)*pow(width,2) + 4*(t*pow(radius,3) + radius*(pow(t,3) - 
      3*t*pow(width,2))) + 3*pow(width,4)) + pow(rL,2)*(pow(radius,6) + 
      pow(t,6) + 20*pow(radius,3)*(pow(t,3) - 3*t*pow(width,2)) + 
      45*pow(t,2)*pow(width,4) + 15*(pow(radius,4)*(pow(t,2) - pow(width,2)) 
      + pow(radius,2)*(pow(t,4) - 6*pow(t,2)*pow(width,2) + 3*pow(width,4))) 
      + 6*(t*pow(radius,5) + radius*(pow(t,5) - 10*pow(t,3)*pow(width,2) + 
      15*t*pow(width,4))) - 15*(pow(t,4)*pow(width,2) + 
      pow(width,6)))),0.5*zL*pow(rL,-3)*(amplitude*(-((rL - radius - 
      t)*exp(-0.5*pow(-rL + radius + t,2)*pow(width,-2))) - (rL + radius + 
      t)*exp(-0.5*pow(rL + radius + t,2)*pow(width,-2))) + 
      rL*amplitude*(exp(-0.5*pow(-rL + radius + t,2)*pow(width,-2)) + 
      exp(-0.5*pow(rL + radius + t,2)*pow(width,-2)) + (-(exp(-0.5*pow(-rL + 
      radius + t,2)*pow(width,-2))*pow(-rL + radius + t,2)) - exp(-0.5*pow(rL 
      + radius + t,2)*pow(width,-2))*pow(rL + radius + 
      t,2))*pow(width,-2))));
    
    CCTK_REAL erruL CCTK_ATTRIBUTE_UNUSED = uL - ua0;
    
    CCTK_REAL errenergyL CCTK_ATTRIBUTE_UNUSED = 0.5*(pow(rhoL - rhoa,2) + 
      pow(v1L - va1,2) + pow(v2L - va2,2) + pow(v3L - va3,2));
    /* Copy local copies back to grid functions */
    errenergy[index] = errenergyL;
    erru[index] = erruL;
  }
  CCTK_ENDLOOP3(ML_WaveToy_DG2_ErrorGaussian);
}
extern "C" void ML_WaveToy_DG2_ErrorGaussian(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_WaveToy_DG2_ErrorGaussian_Body");
  }
  if (cctk_iteration % ML_WaveToy_DG2_ErrorGaussian_calc_every != ML_WaveToy_DG2_ErrorGaussian_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "grid::coordinates",
    "ML_WaveToy_DG2::WT_errenergy",
    "ML_WaveToy_DG2::WT_erru",
    "ML_WaveToy_DG2::WT_rho",
    "ML_WaveToy_DG2::WT_u",
    "ML_WaveToy_DG2::WT_v"};
  AssertGroupStorage(cctkGH, "ML_WaveToy_DG2_ErrorGaussian", 6, groups);
  
  
  TiledLoopOverEverything(cctkGH, ML_WaveToy_DG2_ErrorGaussian_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_WaveToy_DG2_ErrorGaussian_Body");
  }
}

} // namespace ML_WaveToy_DG2
