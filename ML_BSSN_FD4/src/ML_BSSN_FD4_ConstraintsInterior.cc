/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"
#include "loopcontrol.h"

namespace ML_BSSN_FD4 {

extern "C" void ML_BSSN_FD4_ConstraintsInterior_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_FD4_ConstraintsInterior_calc_every != ML_BSSN_FD4_ConstraintsInterior_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_cons_Gamma","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_cons_Gamma.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_Ham","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_Ham.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_mom","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_mom.");
  return;
}

static void ML_BSSN_FD4_ConstraintsInterior_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_BSSN_FD4_ConstraintsInterior,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL alphaL CCTK_ATTRIBUTE_UNUSED = alpha[index];
    CCTK_REAL At11L CCTK_ATTRIBUTE_UNUSED = At11[index];
    CCTK_REAL At12L CCTK_ATTRIBUTE_UNUSED = At12[index];
    CCTK_REAL At13L CCTK_ATTRIBUTE_UNUSED = At13[index];
    CCTK_REAL At22L CCTK_ATTRIBUTE_UNUSED = At22[index];
    CCTK_REAL At23L CCTK_ATTRIBUTE_UNUSED = At23[index];
    CCTK_REAL At33L CCTK_ATTRIBUTE_UNUSED = At33[index];
    CCTK_REAL beta1L CCTK_ATTRIBUTE_UNUSED = beta1[index];
    CCTK_REAL beta2L CCTK_ATTRIBUTE_UNUSED = beta2[index];
    CCTK_REAL beta3L CCTK_ATTRIBUTE_UNUSED = beta3[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL PDgt111L CCTK_ATTRIBUTE_UNUSED = PDgt111[index];
    CCTK_REAL PDgt112L CCTK_ATTRIBUTE_UNUSED = PDgt112[index];
    CCTK_REAL PDgt113L CCTK_ATTRIBUTE_UNUSED = PDgt113[index];
    CCTK_REAL PDgt121L CCTK_ATTRIBUTE_UNUSED = PDgt121[index];
    CCTK_REAL PDgt122L CCTK_ATTRIBUTE_UNUSED = PDgt122[index];
    CCTK_REAL PDgt123L CCTK_ATTRIBUTE_UNUSED = PDgt123[index];
    CCTK_REAL PDgt131L CCTK_ATTRIBUTE_UNUSED = PDgt131[index];
    CCTK_REAL PDgt132L CCTK_ATTRIBUTE_UNUSED = PDgt132[index];
    CCTK_REAL PDgt133L CCTK_ATTRIBUTE_UNUSED = PDgt133[index];
    CCTK_REAL PDgt221L CCTK_ATTRIBUTE_UNUSED = PDgt221[index];
    CCTK_REAL PDgt222L CCTK_ATTRIBUTE_UNUSED = PDgt222[index];
    CCTK_REAL PDgt223L CCTK_ATTRIBUTE_UNUSED = PDgt223[index];
    CCTK_REAL PDgt231L CCTK_ATTRIBUTE_UNUSED = PDgt231[index];
    CCTK_REAL PDgt232L CCTK_ATTRIBUTE_UNUSED = PDgt232[index];
    CCTK_REAL PDgt233L CCTK_ATTRIBUTE_UNUSED = PDgt233[index];
    CCTK_REAL PDgt331L CCTK_ATTRIBUTE_UNUSED = PDgt331[index];
    CCTK_REAL PDgt332L CCTK_ATTRIBUTE_UNUSED = PDgt332[index];
    CCTK_REAL PDgt333L CCTK_ATTRIBUTE_UNUSED = PDgt333[index];
    CCTK_REAL PDphiW1L CCTK_ATTRIBUTE_UNUSED = PDphiW1[index];
    CCTK_REAL PDphiW2L CCTK_ATTRIBUTE_UNUSED = PDphiW2[index];
    CCTK_REAL PDphiW3L CCTK_ATTRIBUTE_UNUSED = PDphiW3[index];
    CCTK_REAL phiWL CCTK_ATTRIBUTE_UNUSED = phiW[index];
    CCTK_REAL trKL CCTK_ATTRIBUTE_UNUSED = trK[index];
    CCTK_REAL Xt1L CCTK_ATTRIBUTE_UNUSED = Xt1[index];
    CCTK_REAL Xt2L CCTK_ATTRIBUTE_UNUSED = Xt2[index];
    CCTK_REAL Xt3L CCTK_ATTRIBUTE_UNUSED = Xt3[index];
    
    CCTK_REAL eTttL, eTtxL, eTtyL, eTtzL, eTxxL, eTxyL, eTxzL, eTyyL, eTyzL, eTzzL CCTK_ATTRIBUTE_UNUSED ;
    
    if (assume_stress_energy_state>=0 ? assume_stress_energy_state : *stress_energy_state)
    {
      eTttL = eTtt[index];
      eTtxL = eTtx[index];
      eTtyL = eTty[index];
      eTtzL = eTtz[index];
      eTxxL = eTxx[index];
      eTxyL = eTxy[index];
      eTxzL = eTxz[index];
      eTyyL = eTyy[index];
      eTyzL = eTyz[index];
      eTzzL = eTzz[index];
    }
    else
    {
      eTttL = 0.;
      eTtxL = 0.;
      eTtyL = 0.;
      eTtzL = 0.;
      eTxxL = 0.;
      eTxyL = 0.;
      eTxzL = 0.;
      eTyyL = 0.;
      eTyzL = 0.;
      eTzzL = 0.;
    }
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDXt11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,-2,0,0) - 
      8*GFOffset(Xt1,-1,0,0) + 8*GFOffset(Xt1,1,0,0) - 
      GFOffset(Xt1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDXt21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,-2,0,0) - 
      8*GFOffset(Xt2,-1,0,0) + 8*GFOffset(Xt2,1,0,0) - 
      GFOffset(Xt2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDXt31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,-2,0,0) - 
      8*GFOffset(Xt3,-1,0,0) + 8*GFOffset(Xt3,1,0,0) - 
      GFOffset(Xt3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDXt12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,0,-2,0) - 
      8*GFOffset(Xt1,0,-1,0) + 8*GFOffset(Xt1,0,1,0) - 
      GFOffset(Xt1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDXt22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,0,-2,0) - 
      8*GFOffset(Xt2,0,-1,0) + 8*GFOffset(Xt2,0,1,0) - 
      GFOffset(Xt2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDXt32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,0,-2,0) - 
      8*GFOffset(Xt3,0,-1,0) + 8*GFOffset(Xt3,0,1,0) - 
      GFOffset(Xt3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDXt13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,0,0,-2) - 
      8*GFOffset(Xt1,0,0,-1) + 8*GFOffset(Xt1,0,0,1) - 
      GFOffset(Xt1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDXt23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,0,0,-2) - 
      8*GFOffset(Xt2,0,0,-1) + 8*GFOffset(Xt2,0,0,1) - 
      GFOffset(Xt2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDXt33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,0,0,-2) - 
      8*GFOffset(Xt3,0,0,-1) + 8*GFOffset(Xt3,0,0,1) - 
      GFOffset(Xt3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDtrK1 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,-2,0,0) - 
      8*GFOffset(trK,-1,0,0) + 8*GFOffset(trK,1,0,0) - 
      GFOffset(trK,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDtrK2 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,0,-2,0) - 
      8*GFOffset(trK,0,-1,0) + 8*GFOffset(trK,0,1,0) - 
      GFOffset(trK,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDtrK3 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,0,0,-2) - 
      8*GFOffset(trK,0,0,-1) + 8*GFOffset(trK,0,0,1) - 
      GFOffset(trK,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDAt111 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At11,-2,0,0) - 
      8*GFOffset(At11,-1,0,0) + 8*GFOffset(At11,1,0,0) - 
      GFOffset(At11,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDAt112 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At11,0,-2,0) - 
      8*GFOffset(At11,0,-1,0) + 8*GFOffset(At11,0,1,0) - 
      GFOffset(At11,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDAt113 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At11,0,0,-2) - 
      8*GFOffset(At11,0,0,-1) + 8*GFOffset(At11,0,0,1) - 
      GFOffset(At11,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDAt121 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At12,-2,0,0) - 
      8*GFOffset(At12,-1,0,0) + 8*GFOffset(At12,1,0,0) - 
      GFOffset(At12,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDAt122 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At12,0,-2,0) - 
      8*GFOffset(At12,0,-1,0) + 8*GFOffset(At12,0,1,0) - 
      GFOffset(At12,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDAt123 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At12,0,0,-2) - 
      8*GFOffset(At12,0,0,-1) + 8*GFOffset(At12,0,0,1) - 
      GFOffset(At12,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDAt131 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At13,-2,0,0) - 
      8*GFOffset(At13,-1,0,0) + 8*GFOffset(At13,1,0,0) - 
      GFOffset(At13,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDAt132 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At13,0,-2,0) - 
      8*GFOffset(At13,0,-1,0) + 8*GFOffset(At13,0,1,0) - 
      GFOffset(At13,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDAt133 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At13,0,0,-2) - 
      8*GFOffset(At13,0,0,-1) + 8*GFOffset(At13,0,0,1) - 
      GFOffset(At13,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDAt211 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At12,-2,0,0) - 
      8*GFOffset(At12,-1,0,0) + 8*GFOffset(At12,1,0,0) - 
      GFOffset(At12,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDAt212 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At12,0,-2,0) - 
      8*GFOffset(At12,0,-1,0) + 8*GFOffset(At12,0,1,0) - 
      GFOffset(At12,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDAt213 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At12,0,0,-2) - 
      8*GFOffset(At12,0,0,-1) + 8*GFOffset(At12,0,0,1) - 
      GFOffset(At12,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDAt221 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At22,-2,0,0) - 
      8*GFOffset(At22,-1,0,0) + 8*GFOffset(At22,1,0,0) - 
      GFOffset(At22,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDAt222 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At22,0,-2,0) - 
      8*GFOffset(At22,0,-1,0) + 8*GFOffset(At22,0,1,0) - 
      GFOffset(At22,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDAt223 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At22,0,0,-2) - 
      8*GFOffset(At22,0,0,-1) + 8*GFOffset(At22,0,0,1) - 
      GFOffset(At22,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDAt231 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At23,-2,0,0) - 
      8*GFOffset(At23,-1,0,0) + 8*GFOffset(At23,1,0,0) - 
      GFOffset(At23,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDAt232 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At23,0,-2,0) - 
      8*GFOffset(At23,0,-1,0) + 8*GFOffset(At23,0,1,0) - 
      GFOffset(At23,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDAt233 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At23,0,0,-2) - 
      8*GFOffset(At23,0,0,-1) + 8*GFOffset(At23,0,0,1) - 
      GFOffset(At23,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDAt311 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At13,-2,0,0) - 
      8*GFOffset(At13,-1,0,0) + 8*GFOffset(At13,1,0,0) - 
      GFOffset(At13,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDAt312 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At13,0,-2,0) - 
      8*GFOffset(At13,0,-1,0) + 8*GFOffset(At13,0,1,0) - 
      GFOffset(At13,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDAt313 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At13,0,0,-2) - 
      8*GFOffset(At13,0,0,-1) + 8*GFOffset(At13,0,0,1) - 
      GFOffset(At13,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDAt321 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At23,-2,0,0) - 
      8*GFOffset(At23,-1,0,0) + 8*GFOffset(At23,1,0,0) - 
      GFOffset(At23,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDAt322 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At23,0,-2,0) - 
      8*GFOffset(At23,0,-1,0) + 8*GFOffset(At23,0,1,0) - 
      GFOffset(At23,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDAt323 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At23,0,0,-2) - 
      8*GFOffset(At23,0,0,-1) + 8*GFOffset(At23,0,0,1) - 
      GFOffset(At23,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDAt331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At33,-2,0,0) - 
      8*GFOffset(At33,-1,0,0) + 8*GFOffset(At33,1,0,0) - 
      GFOffset(At33,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDAt332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At33,0,-2,0) - 
      8*GFOffset(At33,0,-1,0) + 8*GFOffset(At33,0,1,0) - 
      GFOffset(At33,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDAt333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At33,0,0,-2) - 
      8*GFOffset(At33,0,0,-1) + 8*GFOffset(At33,0,0,1) - 
      GFOffset(At33,0,0,2))*pow(dz,-1);
    
    CCTK_REAL PDAt111 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt112 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt113 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt121 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt122 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt123 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt131 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt132 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt133 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt211 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt212 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt213 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDAt333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDtrK1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDtrK2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDtrK3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDXt11 = J11L*LDXt11 + J21L*LDXt12 + J31L*LDXt13;
      
      PDXt21 = J11L*LDXt21 + J21L*LDXt22 + J31L*LDXt23;
      
      PDXt31 = J11L*LDXt31 + J21L*LDXt32 + J31L*LDXt33;
      
      PDXt12 = J12L*LDXt11 + J22L*LDXt12 + J32L*LDXt13;
      
      PDXt22 = J12L*LDXt21 + J22L*LDXt22 + J32L*LDXt23;
      
      PDXt32 = J12L*LDXt31 + J22L*LDXt32 + J32L*LDXt33;
      
      PDXt13 = J13L*LDXt11 + J23L*LDXt12 + J33L*LDXt13;
      
      PDXt23 = J13L*LDXt21 + J23L*LDXt22 + J33L*LDXt23;
      
      PDXt33 = J13L*LDXt31 + J23L*LDXt32 + J33L*LDXt33;
      
      PDtrK1 = J11L*LDtrK1 + J21L*LDtrK2 + J31L*LDtrK3;
      
      PDtrK2 = J12L*LDtrK1 + J22L*LDtrK2 + J32L*LDtrK3;
      
      PDtrK3 = J13L*LDtrK1 + J23L*LDtrK2 + J33L*LDtrK3;
      
      PDAt111 = J11L*LDAt111 + J21L*LDAt112 + J31L*LDAt113;
      
      PDAt112 = J12L*LDAt111 + J22L*LDAt112 + J32L*LDAt113;
      
      PDAt113 = J13L*LDAt111 + J23L*LDAt112 + J33L*LDAt113;
      
      PDAt121 = J11L*LDAt121 + J21L*LDAt122 + J31L*LDAt123;
      
      PDAt122 = J12L*LDAt121 + J22L*LDAt122 + J32L*LDAt123;
      
      PDAt123 = J13L*LDAt121 + J23L*LDAt122 + J33L*LDAt123;
      
      PDAt131 = J11L*LDAt131 + J21L*LDAt132 + J31L*LDAt133;
      
      PDAt132 = J12L*LDAt131 + J22L*LDAt132 + J32L*LDAt133;
      
      PDAt133 = J13L*LDAt131 + J23L*LDAt132 + J33L*LDAt133;
      
      PDAt211 = J11L*LDAt211 + J21L*LDAt212 + J31L*LDAt213;
      
      PDAt212 = J12L*LDAt211 + J22L*LDAt212 + J32L*LDAt213;
      
      PDAt213 = J13L*LDAt211 + J23L*LDAt212 + J33L*LDAt213;
      
      PDAt221 = J11L*LDAt221 + J21L*LDAt222 + J31L*LDAt223;
      
      PDAt222 = J12L*LDAt221 + J22L*LDAt222 + J32L*LDAt223;
      
      PDAt223 = J13L*LDAt221 + J23L*LDAt222 + J33L*LDAt223;
      
      PDAt231 = J11L*LDAt231 + J21L*LDAt232 + J31L*LDAt233;
      
      PDAt232 = J12L*LDAt231 + J22L*LDAt232 + J32L*LDAt233;
      
      PDAt233 = J13L*LDAt231 + J23L*LDAt232 + J33L*LDAt233;
      
      PDAt311 = J11L*LDAt311 + J21L*LDAt312 + J31L*LDAt313;
      
      PDAt312 = J12L*LDAt311 + J22L*LDAt312 + J32L*LDAt313;
      
      PDAt313 = J13L*LDAt311 + J23L*LDAt312 + J33L*LDAt313;
      
      PDAt321 = J11L*LDAt321 + J21L*LDAt322 + J31L*LDAt323;
      
      PDAt322 = J12L*LDAt321 + J22L*LDAt322 + J32L*LDAt323;
      
      PDAt323 = J13L*LDAt321 + J23L*LDAt322 + J33L*LDAt323;
      
      PDAt331 = J11L*LDAt331 + J21L*LDAt332 + J31L*LDAt333;
      
      PDAt332 = J12L*LDAt331 + J22L*LDAt332 + J32L*LDAt333;
      
      PDAt333 = J13L*LDAt331 + J23L*LDAt332 + J33L*LDAt333;
    }
    else
    {
      PDXt11 = LDXt11;
      
      PDXt21 = LDXt21;
      
      PDXt31 = LDXt31;
      
      PDXt12 = LDXt12;
      
      PDXt22 = LDXt22;
      
      PDXt32 = LDXt32;
      
      PDXt13 = LDXt13;
      
      PDXt23 = LDXt23;
      
      PDXt33 = LDXt33;
      
      PDtrK1 = LDtrK1;
      
      PDtrK2 = LDtrK2;
      
      PDtrK3 = LDtrK3;
      
      PDAt111 = LDAt111;
      
      PDAt112 = LDAt112;
      
      PDAt113 = LDAt113;
      
      PDAt121 = LDAt121;
      
      PDAt122 = LDAt122;
      
      PDAt123 = LDAt123;
      
      PDAt131 = LDAt131;
      
      PDAt132 = LDAt132;
      
      PDAt133 = LDAt133;
      
      PDAt211 = LDAt211;
      
      PDAt212 = LDAt212;
      
      PDAt213 = LDAt213;
      
      PDAt221 = LDAt221;
      
      PDAt222 = LDAt222;
      
      PDAt223 = LDAt223;
      
      PDAt231 = LDAt231;
      
      PDAt232 = LDAt232;
      
      PDAt233 = LDAt233;
      
      PDAt311 = LDAt311;
      
      PDAt312 = LDAt312;
      
      PDAt313 = LDAt313;
      
      PDAt321 = LDAt321;
      
      PDAt322 = LDAt322;
      
      PDAt323 = LDAt323;
      
      PDAt331 = LDAt331;
      
      PDAt332 = LDAt332;
      
      PDAt333 = LDAt333;
    }
    
    CCTK_REAL LDPDphiW11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW1,-2,0,0) - 
      8*GFOffset(PDphiW1,-1,0,0) + 8*GFOffset(PDphiW1,1,0,0) - 
      GFOffset(PDphiW1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDphiW12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW1,0,-2,0) - 
      8*GFOffset(PDphiW1,0,-1,0) + 8*GFOffset(PDphiW1,0,1,0) - 
      GFOffset(PDphiW1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDphiW13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW1,0,0,-2) - 
      8*GFOffset(PDphiW1,0,0,-1) + 8*GFOffset(PDphiW1,0,0,1) - 
      GFOffset(PDphiW1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDphiW21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW2,-2,0,0) - 
      8*GFOffset(PDphiW2,-1,0,0) + 8*GFOffset(PDphiW2,1,0,0) - 
      GFOffset(PDphiW2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDphiW22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW2,0,-2,0) - 
      8*GFOffset(PDphiW2,0,-1,0) + 8*GFOffset(PDphiW2,0,1,0) - 
      GFOffset(PDphiW2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDphiW23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW2,0,0,-2) - 
      8*GFOffset(PDphiW2,0,0,-1) + 8*GFOffset(PDphiW2,0,0,1) - 
      GFOffset(PDphiW2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDphiW31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW3,-2,0,0) - 
      8*GFOffset(PDphiW3,-1,0,0) + 8*GFOffset(PDphiW3,1,0,0) - 
      GFOffset(PDphiW3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDphiW32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW3,0,-2,0) - 
      8*GFOffset(PDphiW3,0,-1,0) + 8*GFOffset(PDphiW3,0,1,0) - 
      GFOffset(PDphiW3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDphiW33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW3,0,0,-2) - 
      8*GFOffset(PDphiW3,0,0,-1) + 8*GFOffset(PDphiW3,0,0,1) - 
      GFOffset(PDphiW3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1111 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt111,-2,0,0) - 
      8*GFOffset(PDgt111,-1,0,0) + 8*GFOffset(PDgt111,1,0,0) - 
      GFOffset(PDgt111,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1112 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt111,0,-2,0) - 
      8*GFOffset(PDgt111,0,-1,0) + 8*GFOffset(PDgt111,0,1,0) - 
      GFOffset(PDgt111,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1113 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt111,0,0,-2) - 
      8*GFOffset(PDgt111,0,0,-1) + 8*GFOffset(PDgt111,0,0,1) - 
      GFOffset(PDgt111,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1121 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt112,-2,0,0) - 
      8*GFOffset(PDgt112,-1,0,0) + 8*GFOffset(PDgt112,1,0,0) - 
      GFOffset(PDgt112,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1122 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt112,0,-2,0) - 
      8*GFOffset(PDgt112,0,-1,0) + 8*GFOffset(PDgt112,0,1,0) - 
      GFOffset(PDgt112,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1123 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt112,0,0,-2) - 
      8*GFOffset(PDgt112,0,0,-1) + 8*GFOffset(PDgt112,0,0,1) - 
      GFOffset(PDgt112,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1131 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt113,-2,0,0) - 
      8*GFOffset(PDgt113,-1,0,0) + 8*GFOffset(PDgt113,1,0,0) - 
      GFOffset(PDgt113,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1132 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt113,0,-2,0) - 
      8*GFOffset(PDgt113,0,-1,0) + 8*GFOffset(PDgt113,0,1,0) - 
      GFOffset(PDgt113,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1133 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt113,0,0,-2) - 
      8*GFOffset(PDgt113,0,0,-1) + 8*GFOffset(PDgt113,0,0,1) - 
      GFOffset(PDgt113,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1211 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt121,-2,0,0) - 
      8*GFOffset(PDgt121,-1,0,0) + 8*GFOffset(PDgt121,1,0,0) - 
      GFOffset(PDgt121,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1212 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt121,0,-2,0) - 
      8*GFOffset(PDgt121,0,-1,0) + 8*GFOffset(PDgt121,0,1,0) - 
      GFOffset(PDgt121,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1213 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt121,0,0,-2) - 
      8*GFOffset(PDgt121,0,0,-1) + 8*GFOffset(PDgt121,0,0,1) - 
      GFOffset(PDgt121,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1221 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt122,-2,0,0) - 
      8*GFOffset(PDgt122,-1,0,0) + 8*GFOffset(PDgt122,1,0,0) - 
      GFOffset(PDgt122,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1222 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt122,0,-2,0) - 
      8*GFOffset(PDgt122,0,-1,0) + 8*GFOffset(PDgt122,0,1,0) - 
      GFOffset(PDgt122,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1223 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt122,0,0,-2) - 
      8*GFOffset(PDgt122,0,0,-1) + 8*GFOffset(PDgt122,0,0,1) - 
      GFOffset(PDgt122,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1231 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt123,-2,0,0) - 
      8*GFOffset(PDgt123,-1,0,0) + 8*GFOffset(PDgt123,1,0,0) - 
      GFOffset(PDgt123,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1232 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt123,0,-2,0) - 
      8*GFOffset(PDgt123,0,-1,0) + 8*GFOffset(PDgt123,0,1,0) - 
      GFOffset(PDgt123,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1233 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt123,0,0,-2) - 
      8*GFOffset(PDgt123,0,0,-1) + 8*GFOffset(PDgt123,0,0,1) - 
      GFOffset(PDgt123,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1311 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt131,-2,0,0) - 
      8*GFOffset(PDgt131,-1,0,0) + 8*GFOffset(PDgt131,1,0,0) - 
      GFOffset(PDgt131,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1312 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt131,0,-2,0) - 
      8*GFOffset(PDgt131,0,-1,0) + 8*GFOffset(PDgt131,0,1,0) - 
      GFOffset(PDgt131,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1313 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt131,0,0,-2) - 
      8*GFOffset(PDgt131,0,0,-1) + 8*GFOffset(PDgt131,0,0,1) - 
      GFOffset(PDgt131,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1321 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt132,-2,0,0) - 
      8*GFOffset(PDgt132,-1,0,0) + 8*GFOffset(PDgt132,1,0,0) - 
      GFOffset(PDgt132,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1322 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt132,0,-2,0) - 
      8*GFOffset(PDgt132,0,-1,0) + 8*GFOffset(PDgt132,0,1,0) - 
      GFOffset(PDgt132,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1323 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt132,0,0,-2) - 
      8*GFOffset(PDgt132,0,0,-1) + 8*GFOffset(PDgt132,0,0,1) - 
      GFOffset(PDgt132,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt133,-2,0,0) - 
      8*GFOffset(PDgt133,-1,0,0) + 8*GFOffset(PDgt133,1,0,0) - 
      GFOffset(PDgt133,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt133,0,-2,0) - 
      8*GFOffset(PDgt133,0,-1,0) + 8*GFOffset(PDgt133,0,1,0) - 
      GFOffset(PDgt133,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt133,0,0,-2) - 
      8*GFOffset(PDgt133,0,0,-1) + 8*GFOffset(PDgt133,0,0,1) - 
      GFOffset(PDgt133,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2211 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt221,-2,0,0) - 
      8*GFOffset(PDgt221,-1,0,0) + 8*GFOffset(PDgt221,1,0,0) - 
      GFOffset(PDgt221,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2212 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt221,0,-2,0) - 
      8*GFOffset(PDgt221,0,-1,0) + 8*GFOffset(PDgt221,0,1,0) - 
      GFOffset(PDgt221,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2213 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt221,0,0,-2) - 
      8*GFOffset(PDgt221,0,0,-1) + 8*GFOffset(PDgt221,0,0,1) - 
      GFOffset(PDgt221,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2221 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt222,-2,0,0) - 
      8*GFOffset(PDgt222,-1,0,0) + 8*GFOffset(PDgt222,1,0,0) - 
      GFOffset(PDgt222,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2222 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt222,0,-2,0) - 
      8*GFOffset(PDgt222,0,-1,0) + 8*GFOffset(PDgt222,0,1,0) - 
      GFOffset(PDgt222,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2223 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt222,0,0,-2) - 
      8*GFOffset(PDgt222,0,0,-1) + 8*GFOffset(PDgt222,0,0,1) - 
      GFOffset(PDgt222,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2231 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt223,-2,0,0) - 
      8*GFOffset(PDgt223,-1,0,0) + 8*GFOffset(PDgt223,1,0,0) - 
      GFOffset(PDgt223,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2232 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt223,0,-2,0) - 
      8*GFOffset(PDgt223,0,-1,0) + 8*GFOffset(PDgt223,0,1,0) - 
      GFOffset(PDgt223,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2233 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt223,0,0,-2) - 
      8*GFOffset(PDgt223,0,0,-1) + 8*GFOffset(PDgt223,0,0,1) - 
      GFOffset(PDgt223,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2311 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt231,-2,0,0) - 
      8*GFOffset(PDgt231,-1,0,0) + 8*GFOffset(PDgt231,1,0,0) - 
      GFOffset(PDgt231,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2312 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt231,0,-2,0) - 
      8*GFOffset(PDgt231,0,-1,0) + 8*GFOffset(PDgt231,0,1,0) - 
      GFOffset(PDgt231,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2313 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt231,0,0,-2) - 
      8*GFOffset(PDgt231,0,0,-1) + 8*GFOffset(PDgt231,0,0,1) - 
      GFOffset(PDgt231,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2321 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt232,-2,0,0) - 
      8*GFOffset(PDgt232,-1,0,0) + 8*GFOffset(PDgt232,1,0,0) - 
      GFOffset(PDgt232,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2322 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt232,0,-2,0) - 
      8*GFOffset(PDgt232,0,-1,0) + 8*GFOffset(PDgt232,0,1,0) - 
      GFOffset(PDgt232,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2323 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt232,0,0,-2) - 
      8*GFOffset(PDgt232,0,0,-1) + 8*GFOffset(PDgt232,0,0,1) - 
      GFOffset(PDgt232,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt233,-2,0,0) - 
      8*GFOffset(PDgt233,-1,0,0) + 8*GFOffset(PDgt233,1,0,0) - 
      GFOffset(PDgt233,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt233,0,-2,0) - 
      8*GFOffset(PDgt233,0,-1,0) + 8*GFOffset(PDgt233,0,1,0) - 
      GFOffset(PDgt233,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt233,0,0,-2) - 
      8*GFOffset(PDgt233,0,0,-1) + 8*GFOffset(PDgt233,0,0,1) - 
      GFOffset(PDgt233,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt3311 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt331,-2,0,0) - 
      8*GFOffset(PDgt331,-1,0,0) + 8*GFOffset(PDgt331,1,0,0) - 
      GFOffset(PDgt331,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt3312 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt331,0,-2,0) - 
      8*GFOffset(PDgt331,0,-1,0) + 8*GFOffset(PDgt331,0,1,0) - 
      GFOffset(PDgt331,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt3313 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt331,0,0,-2) - 
      8*GFOffset(PDgt331,0,0,-1) + 8*GFOffset(PDgt331,0,0,1) - 
      GFOffset(PDgt331,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt3321 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt332,-2,0,0) - 
      8*GFOffset(PDgt332,-1,0,0) + 8*GFOffset(PDgt332,1,0,0) - 
      GFOffset(PDgt332,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt3322 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt332,0,-2,0) - 
      8*GFOffset(PDgt332,0,-1,0) + 8*GFOffset(PDgt332,0,1,0) - 
      GFOffset(PDgt332,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt3323 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt332,0,0,-2) - 
      8*GFOffset(PDgt332,0,0,-1) + 8*GFOffset(PDgt332,0,0,1) - 
      GFOffset(PDgt332,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt3331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt333,-2,0,0) - 
      8*GFOffset(PDgt333,-1,0,0) + 8*GFOffset(PDgt333,1,0,0) - 
      GFOffset(PDgt333,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt3332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt333,0,-2,0) - 
      8*GFOffset(PDgt333,0,-1,0) + 8*GFOffset(PDgt333,0,1,0) - 
      GFOffset(PDgt333,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt3333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt333,0,0,-2) - 
      8*GFOffset(PDgt333,0,0,-1) + 8*GFOffset(PDgt333,0,0,1) - 
      GFOffset(PDgt333,0,0,2))*pow(dz,-1);
    
    CCTK_REAL PDPDgt1111 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1112 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1113 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1121 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1122 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1123 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1131 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1132 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1133 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1211 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1212 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1213 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2211 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2212 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2213 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDPDphiW11 = J11L*LDPDphiW11 + J21L*LDPDphiW12 + J31L*LDPDphiW13;
      
      PDPDphiW12 = J12L*LDPDphiW11 + J22L*LDPDphiW12 + J32L*LDPDphiW13;
      
      PDPDphiW13 = J13L*LDPDphiW11 + J23L*LDPDphiW12 + J33L*LDPDphiW13;
      
      PDPDphiW21 = J11L*LDPDphiW21 + J21L*LDPDphiW22 + J31L*LDPDphiW23;
      
      PDPDphiW22 = J12L*LDPDphiW21 + J22L*LDPDphiW22 + J32L*LDPDphiW23;
      
      PDPDphiW23 = J13L*LDPDphiW21 + J23L*LDPDphiW22 + J33L*LDPDphiW23;
      
      PDPDphiW31 = J11L*LDPDphiW31 + J21L*LDPDphiW32 + J31L*LDPDphiW33;
      
      PDPDphiW32 = J12L*LDPDphiW31 + J22L*LDPDphiW32 + J32L*LDPDphiW33;
      
      PDPDphiW33 = J13L*LDPDphiW31 + J23L*LDPDphiW32 + J33L*LDPDphiW33;
      
      PDPDgt1111 = J11L*LDPDgt1111 + J21L*LDPDgt1112 + J31L*LDPDgt1113;
      
      PDPDgt1112 = J12L*LDPDgt1111 + J22L*LDPDgt1112 + J32L*LDPDgt1113;
      
      PDPDgt1113 = J13L*LDPDgt1111 + J23L*LDPDgt1112 + J33L*LDPDgt1113;
      
      PDPDgt1121 = J11L*LDPDgt1121 + J21L*LDPDgt1122 + J31L*LDPDgt1123;
      
      PDPDgt1122 = J12L*LDPDgt1121 + J22L*LDPDgt1122 + J32L*LDPDgt1123;
      
      PDPDgt1123 = J13L*LDPDgt1121 + J23L*LDPDgt1122 + J33L*LDPDgt1123;
      
      PDPDgt1131 = J11L*LDPDgt1131 + J21L*LDPDgt1132 + J31L*LDPDgt1133;
      
      PDPDgt1132 = J12L*LDPDgt1131 + J22L*LDPDgt1132 + J32L*LDPDgt1133;
      
      PDPDgt1133 = J13L*LDPDgt1131 + J23L*LDPDgt1132 + J33L*LDPDgt1133;
      
      PDPDgt1211 = J11L*LDPDgt1211 + J21L*LDPDgt1212 + J31L*LDPDgt1213;
      
      PDPDgt1212 = J12L*LDPDgt1211 + J22L*LDPDgt1212 + J32L*LDPDgt1213;
      
      PDPDgt1213 = J13L*LDPDgt1211 + J23L*LDPDgt1212 + J33L*LDPDgt1213;
      
      PDPDgt1221 = J11L*LDPDgt1221 + J21L*LDPDgt1222 + J31L*LDPDgt1223;
      
      PDPDgt1222 = J12L*LDPDgt1221 + J22L*LDPDgt1222 + J32L*LDPDgt1223;
      
      PDPDgt1223 = J13L*LDPDgt1221 + J23L*LDPDgt1222 + J33L*LDPDgt1223;
      
      PDPDgt1231 = J11L*LDPDgt1231 + J21L*LDPDgt1232 + J31L*LDPDgt1233;
      
      PDPDgt1232 = J12L*LDPDgt1231 + J22L*LDPDgt1232 + J32L*LDPDgt1233;
      
      PDPDgt1233 = J13L*LDPDgt1231 + J23L*LDPDgt1232 + J33L*LDPDgt1233;
      
      PDPDgt1311 = J11L*LDPDgt1311 + J21L*LDPDgt1312 + J31L*LDPDgt1313;
      
      PDPDgt1312 = J12L*LDPDgt1311 + J22L*LDPDgt1312 + J32L*LDPDgt1313;
      
      PDPDgt1313 = J13L*LDPDgt1311 + J23L*LDPDgt1312 + J33L*LDPDgt1313;
      
      PDPDgt1321 = J11L*LDPDgt1321 + J21L*LDPDgt1322 + J31L*LDPDgt1323;
      
      PDPDgt1322 = J12L*LDPDgt1321 + J22L*LDPDgt1322 + J32L*LDPDgt1323;
      
      PDPDgt1323 = J13L*LDPDgt1321 + J23L*LDPDgt1322 + J33L*LDPDgt1323;
      
      PDPDgt1331 = J11L*LDPDgt1331 + J21L*LDPDgt1332 + J31L*LDPDgt1333;
      
      PDPDgt1332 = J12L*LDPDgt1331 + J22L*LDPDgt1332 + J32L*LDPDgt1333;
      
      PDPDgt1333 = J13L*LDPDgt1331 + J23L*LDPDgt1332 + J33L*LDPDgt1333;
      
      PDPDgt2211 = J11L*LDPDgt2211 + J21L*LDPDgt2212 + J31L*LDPDgt2213;
      
      PDPDgt2212 = J12L*LDPDgt2211 + J22L*LDPDgt2212 + J32L*LDPDgt2213;
      
      PDPDgt2213 = J13L*LDPDgt2211 + J23L*LDPDgt2212 + J33L*LDPDgt2213;
      
      PDPDgt2221 = J11L*LDPDgt2221 + J21L*LDPDgt2222 + J31L*LDPDgt2223;
      
      PDPDgt2222 = J12L*LDPDgt2221 + J22L*LDPDgt2222 + J32L*LDPDgt2223;
      
      PDPDgt2223 = J13L*LDPDgt2221 + J23L*LDPDgt2222 + J33L*LDPDgt2223;
      
      PDPDgt2231 = J11L*LDPDgt2231 + J21L*LDPDgt2232 + J31L*LDPDgt2233;
      
      PDPDgt2232 = J12L*LDPDgt2231 + J22L*LDPDgt2232 + J32L*LDPDgt2233;
      
      PDPDgt2233 = J13L*LDPDgt2231 + J23L*LDPDgt2232 + J33L*LDPDgt2233;
      
      PDPDgt2311 = J11L*LDPDgt2311 + J21L*LDPDgt2312 + J31L*LDPDgt2313;
      
      PDPDgt2312 = J12L*LDPDgt2311 + J22L*LDPDgt2312 + J32L*LDPDgt2313;
      
      PDPDgt2313 = J13L*LDPDgt2311 + J23L*LDPDgt2312 + J33L*LDPDgt2313;
      
      PDPDgt2321 = J11L*LDPDgt2321 + J21L*LDPDgt2322 + J31L*LDPDgt2323;
      
      PDPDgt2322 = J12L*LDPDgt2321 + J22L*LDPDgt2322 + J32L*LDPDgt2323;
      
      PDPDgt2323 = J13L*LDPDgt2321 + J23L*LDPDgt2322 + J33L*LDPDgt2323;
      
      PDPDgt2331 = J11L*LDPDgt2331 + J21L*LDPDgt2332 + J31L*LDPDgt2333;
      
      PDPDgt2332 = J12L*LDPDgt2331 + J22L*LDPDgt2332 + J32L*LDPDgt2333;
      
      PDPDgt2333 = J13L*LDPDgt2331 + J23L*LDPDgt2332 + J33L*LDPDgt2333;
      
      PDPDgt3311 = J11L*LDPDgt3311 + J21L*LDPDgt3312 + J31L*LDPDgt3313;
      
      PDPDgt3312 = J12L*LDPDgt3311 + J22L*LDPDgt3312 + J32L*LDPDgt3313;
      
      PDPDgt3313 = J13L*LDPDgt3311 + J23L*LDPDgt3312 + J33L*LDPDgt3313;
      
      PDPDgt3321 = J11L*LDPDgt3321 + J21L*LDPDgt3322 + J31L*LDPDgt3323;
      
      PDPDgt3322 = J12L*LDPDgt3321 + J22L*LDPDgt3322 + J32L*LDPDgt3323;
      
      PDPDgt3323 = J13L*LDPDgt3321 + J23L*LDPDgt3322 + J33L*LDPDgt3323;
      
      PDPDgt3331 = J11L*LDPDgt3331 + J21L*LDPDgt3332 + J31L*LDPDgt3333;
      
      PDPDgt3332 = J12L*LDPDgt3331 + J22L*LDPDgt3332 + J32L*LDPDgt3333;
      
      PDPDgt3333 = J13L*LDPDgt3331 + J23L*LDPDgt3332 + J33L*LDPDgt3333;
    }
    else
    {
      PDPDphiW11 = LDPDphiW11;
      
      PDPDphiW12 = LDPDphiW12;
      
      PDPDphiW13 = LDPDphiW13;
      
      PDPDphiW21 = LDPDphiW21;
      
      PDPDphiW22 = LDPDphiW22;
      
      PDPDphiW23 = LDPDphiW23;
      
      PDPDphiW31 = LDPDphiW31;
      
      PDPDphiW32 = LDPDphiW32;
      
      PDPDphiW33 = LDPDphiW33;
      
      PDPDgt1111 = LDPDgt1111;
      
      PDPDgt1112 = LDPDgt1112;
      
      PDPDgt1113 = LDPDgt1113;
      
      PDPDgt1121 = LDPDgt1121;
      
      PDPDgt1122 = LDPDgt1122;
      
      PDPDgt1123 = LDPDgt1123;
      
      PDPDgt1131 = LDPDgt1131;
      
      PDPDgt1132 = LDPDgt1132;
      
      PDPDgt1133 = LDPDgt1133;
      
      PDPDgt1211 = LDPDgt1211;
      
      PDPDgt1212 = LDPDgt1212;
      
      PDPDgt1213 = LDPDgt1213;
      
      PDPDgt1221 = LDPDgt1221;
      
      PDPDgt1222 = LDPDgt1222;
      
      PDPDgt1223 = LDPDgt1223;
      
      PDPDgt1231 = LDPDgt1231;
      
      PDPDgt1232 = LDPDgt1232;
      
      PDPDgt1233 = LDPDgt1233;
      
      PDPDgt1311 = LDPDgt1311;
      
      PDPDgt1312 = LDPDgt1312;
      
      PDPDgt1313 = LDPDgt1313;
      
      PDPDgt1321 = LDPDgt1321;
      
      PDPDgt1322 = LDPDgt1322;
      
      PDPDgt1323 = LDPDgt1323;
      
      PDPDgt1331 = LDPDgt1331;
      
      PDPDgt1332 = LDPDgt1332;
      
      PDPDgt1333 = LDPDgt1333;
      
      PDPDgt2211 = LDPDgt2211;
      
      PDPDgt2212 = LDPDgt2212;
      
      PDPDgt2213 = LDPDgt2213;
      
      PDPDgt2221 = LDPDgt2221;
      
      PDPDgt2222 = LDPDgt2222;
      
      PDPDgt2223 = LDPDgt2223;
      
      PDPDgt2231 = LDPDgt2231;
      
      PDPDgt2232 = LDPDgt2232;
      
      PDPDgt2233 = LDPDgt2233;
      
      PDPDgt2311 = LDPDgt2311;
      
      PDPDgt2312 = LDPDgt2312;
      
      PDPDgt2313 = LDPDgt2313;
      
      PDPDgt2321 = LDPDgt2321;
      
      PDPDgt2322 = LDPDgt2322;
      
      PDPDgt2323 = LDPDgt2323;
      
      PDPDgt2331 = LDPDgt2331;
      
      PDPDgt2332 = LDPDgt2332;
      
      PDPDgt2333 = LDPDgt2333;
      
      PDPDgt3311 = LDPDgt3311;
      
      PDPDgt3312 = LDPDgt3312;
      
      PDPDgt3313 = LDPDgt3313;
      
      PDPDgt3321 = LDPDgt3321;
      
      PDPDgt3322 = LDPDgt3322;
      
      PDPDgt3323 = LDPDgt3323;
      
      PDPDgt3331 = LDPDgt3331;
      
      PDPDgt3332 = LDPDgt3332;
      
      PDPDgt3333 = LDPDgt3333;
    }
    
    CCTK_REAL detgt CCTK_ATTRIBUTE_UNUSED = 1;
    
    CCTK_REAL gtu11 CCTK_ATTRIBUTE_UNUSED = (gt22L*gt33L - 
      pow(gt23L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu12 CCTK_ATTRIBUTE_UNUSED = (gt13L*gt23L - 
      gt12L*gt33L)*pow(detgt,-1);
    
    CCTK_REAL gtu13 CCTK_ATTRIBUTE_UNUSED = (-(gt13L*gt22L) + 
      gt12L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu22 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt33L - 
      pow(gt13L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu23 CCTK_ATTRIBUTE_UNUSED = (gt12L*gt13L - 
      gt11L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu33 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt22L - 
      pow(gt12L,2))*pow(detgt,-1);
    
    CCTK_REAL Gtl111 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt111L;
    
    CCTK_REAL Gtl112 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt112L;
    
    CCTK_REAL Gtl113 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt113L;
    
    CCTK_REAL Gtl122 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt122L - PDgt221L);
    
    CCTK_REAL Gtl123 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt123L + PDgt132L - 
      PDgt231L);
    
    CCTK_REAL Gtl133 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt133L - PDgt331L);
    
    CCTK_REAL Gtl211 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt112L + 2*PDgt121L);
    
    CCTK_REAL Gtl212 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt221L;
    
    CCTK_REAL Gtl213 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt123L - PDgt132L + 
      PDgt231L);
    
    CCTK_REAL Gtl222 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt222L;
    
    CCTK_REAL Gtl223 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt223L;
    
    CCTK_REAL Gtl233 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt233L - PDgt332L);
    
    CCTK_REAL Gtl311 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt113L + 2*PDgt131L);
    
    CCTK_REAL Gtl312 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt123L + PDgt132L + 
      PDgt231L);
    
    CCTK_REAL Gtl313 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt331L;
    
    CCTK_REAL Gtl322 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt223L + 2*PDgt232L);
    
    CCTK_REAL Gtl323 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt332L;
    
    CCTK_REAL Gtl333 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt333L;
    
    CCTK_REAL Gtlu111 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu11 + Gtl112*gtu12 
      + Gtl113*gtu13;
    
    CCTK_REAL Gtlu112 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu12 + Gtl112*gtu22 
      + Gtl113*gtu23;
    
    CCTK_REAL Gtlu113 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu13 + Gtl112*gtu23 
      + Gtl113*gtu33;
    
    CCTK_REAL Gtlu121 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu11 + Gtl122*gtu12 
      + Gtl123*gtu13;
    
    CCTK_REAL Gtlu122 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu12 + Gtl122*gtu22 
      + Gtl123*gtu23;
    
    CCTK_REAL Gtlu123 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu13 + Gtl122*gtu23 
      + Gtl123*gtu33;
    
    CCTK_REAL Gtlu131 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu11 + Gtl123*gtu12 
      + Gtl133*gtu13;
    
    CCTK_REAL Gtlu132 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu12 + Gtl123*gtu22 
      + Gtl133*gtu23;
    
    CCTK_REAL Gtlu133 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu13 + Gtl123*gtu23 
      + Gtl133*gtu33;
    
    CCTK_REAL Gtlu211 CCTK_ATTRIBUTE_UNUSED = Gtl211*gtu11 + Gtl212*gtu12 
      + Gtl213*gtu13;
    
    CCTK_REAL Gtlu212 CCTK_ATTRIBUTE_UNUSED = Gtl211*gtu12 + Gtl212*gtu22 
      + Gtl213*gtu23;
    
    CCTK_REAL Gtlu213 CCTK_ATTRIBUTE_UNUSED = Gtl211*gtu13 + Gtl212*gtu23 
      + Gtl213*gtu33;
    
    CCTK_REAL Gtlu221 CCTK_ATTRIBUTE_UNUSED = Gtl212*gtu11 + Gtl222*gtu12 
      + Gtl223*gtu13;
    
    CCTK_REAL Gtlu222 CCTK_ATTRIBUTE_UNUSED = Gtl212*gtu12 + Gtl222*gtu22 
      + Gtl223*gtu23;
    
    CCTK_REAL Gtlu223 CCTK_ATTRIBUTE_UNUSED = Gtl212*gtu13 + Gtl222*gtu23 
      + Gtl223*gtu33;
    
    CCTK_REAL Gtlu231 CCTK_ATTRIBUTE_UNUSED = Gtl213*gtu11 + Gtl223*gtu12 
      + Gtl233*gtu13;
    
    CCTK_REAL Gtlu232 CCTK_ATTRIBUTE_UNUSED = Gtl213*gtu12 + Gtl223*gtu22 
      + Gtl233*gtu23;
    
    CCTK_REAL Gtlu233 CCTK_ATTRIBUTE_UNUSED = Gtl213*gtu13 + Gtl223*gtu23 
      + Gtl233*gtu33;
    
    CCTK_REAL Gtlu311 CCTK_ATTRIBUTE_UNUSED = Gtl311*gtu11 + Gtl312*gtu12 
      + Gtl313*gtu13;
    
    CCTK_REAL Gtlu312 CCTK_ATTRIBUTE_UNUSED = Gtl311*gtu12 + Gtl312*gtu22 
      + Gtl313*gtu23;
    
    CCTK_REAL Gtlu313 CCTK_ATTRIBUTE_UNUSED = Gtl311*gtu13 + Gtl312*gtu23 
      + Gtl313*gtu33;
    
    CCTK_REAL Gtlu321 CCTK_ATTRIBUTE_UNUSED = Gtl312*gtu11 + Gtl322*gtu12 
      + Gtl323*gtu13;
    
    CCTK_REAL Gtlu322 CCTK_ATTRIBUTE_UNUSED = Gtl312*gtu12 + Gtl322*gtu22 
      + Gtl323*gtu23;
    
    CCTK_REAL Gtlu323 CCTK_ATTRIBUTE_UNUSED = Gtl312*gtu13 + Gtl322*gtu23 
      + Gtl323*gtu33;
    
    CCTK_REAL Gtlu331 CCTK_ATTRIBUTE_UNUSED = Gtl313*gtu11 + Gtl323*gtu12 
      + Gtl333*gtu13;
    
    CCTK_REAL Gtlu332 CCTK_ATTRIBUTE_UNUSED = Gtl313*gtu12 + Gtl323*gtu22 
      + Gtl333*gtu23;
    
    CCTK_REAL Gtlu333 CCTK_ATTRIBUTE_UNUSED = Gtl313*gtu13 + Gtl323*gtu23 
      + Gtl333*gtu33;
    
    CCTK_REAL Gt111 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu11 + Gtl211*gtu12 + 
      Gtl311*gtu13;
    
    CCTK_REAL Gt211 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu12 + Gtl211*gtu22 + 
      Gtl311*gtu23;
    
    CCTK_REAL Gt311 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu13 + Gtl211*gtu23 + 
      Gtl311*gtu33;
    
    CCTK_REAL Gt112 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu11 + Gtl212*gtu12 + 
      Gtl312*gtu13;
    
    CCTK_REAL Gt212 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu12 + Gtl212*gtu22 + 
      Gtl312*gtu23;
    
    CCTK_REAL Gt312 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu13 + Gtl212*gtu23 + 
      Gtl312*gtu33;
    
    CCTK_REAL Gt113 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu11 + Gtl213*gtu12 + 
      Gtl313*gtu13;
    
    CCTK_REAL Gt213 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu12 + Gtl213*gtu22 + 
      Gtl313*gtu23;
    
    CCTK_REAL Gt313 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu13 + Gtl213*gtu23 + 
      Gtl313*gtu33;
    
    CCTK_REAL Gt122 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu11 + Gtl222*gtu12 + 
      Gtl322*gtu13;
    
    CCTK_REAL Gt222 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu12 + Gtl222*gtu22 + 
      Gtl322*gtu23;
    
    CCTK_REAL Gt322 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu13 + Gtl222*gtu23 + 
      Gtl322*gtu33;
    
    CCTK_REAL Gt123 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu11 + Gtl223*gtu12 + 
      Gtl323*gtu13;
    
    CCTK_REAL Gt223 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu12 + Gtl223*gtu22 + 
      Gtl323*gtu23;
    
    CCTK_REAL Gt323 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu13 + Gtl223*gtu23 + 
      Gtl323*gtu33;
    
    CCTK_REAL Gt133 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu11 + Gtl233*gtu12 + 
      Gtl333*gtu13;
    
    CCTK_REAL Gt233 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu12 + Gtl233*gtu22 + 
      Gtl333*gtu23;
    
    CCTK_REAL Gt333 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu13 + Gtl233*gtu23 + 
      Gtl333*gtu33;
    
    CCTK_REAL em4phi CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,pow(phiWL,2),exp(-4*phiWL));
    
    CCTK_REAL gu11 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu11;
    
    CCTK_REAL gu12 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu12;
    
    CCTK_REAL gu13 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu13;
    
    CCTK_REAL gu22 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu22;
    
    CCTK_REAL gu23 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu23;
    
    CCTK_REAL gu33 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu33;
    
    CCTK_REAL Xtn1 CCTK_ATTRIBUTE_UNUSED = Gt111*gtu11 + 2*Gt112*gtu12 + 
      2*Gt113*gtu13 + Gt122*gtu22 + 2*Gt123*gtu23 + Gt133*gtu33;
    
    CCTK_REAL Xtn2 CCTK_ATTRIBUTE_UNUSED = Gt211*gtu11 + 2*Gt212*gtu12 + 
      2*Gt213*gtu13 + Gt222*gtu22 + 2*Gt223*gtu23 + Gt233*gtu33;
    
    CCTK_REAL Xtn3 CCTK_ATTRIBUTE_UNUSED = Gt311*gtu11 + 2*Gt312*gtu12 + 
      2*Gt313*gtu13 + Gt322*gtu22 + 2*Gt323*gtu23 + Gt333*gtu33;
    
    CCTK_REAL Rt11 CCTK_ATTRIBUTE_UNUSED = 3*Gt111*Gtlu111 + 
      3*Gt112*Gtlu112 + 3*Gt113*Gtlu113 + 2*Gt211*Gtlu121 + 2*Gt212*Gtlu122 + 
      2*Gt213*Gtlu123 + 2*Gt311*Gtlu131 + 2*Gt312*Gtlu132 + 2*Gt313*Gtlu133 + 
      Gt211*Gtlu211 + Gt212*Gtlu212 + Gt213*Gtlu213 + Gt311*Gtlu311 + 
      Gt312*Gtlu312 + Gt313*Gtlu313 + 0.5*(-(gtu11*PDPDgt1111) - 
      gtu12*PDPDgt1112 - gtu13*PDPDgt1113 - gtu12*PDPDgt1121 - 
      gtu22*PDPDgt1122 - gtu23*PDPDgt1123 - gtu13*PDPDgt1131 - 
      gtu23*PDPDgt1132 - gtu33*PDPDgt1133) + gt11L*PDXt11 + gt12L*PDXt21 + 
      gt13L*PDXt31 + Gtl111*Xtn1 + Gtl112*Xtn2 + Gtl113*Xtn3;
    
    CCTK_REAL Rt12 CCTK_ATTRIBUTE_UNUSED = Gt112*Gtlu111 + Gt122*Gtlu112 + 
      Gt123*Gtlu113 + Gt111*Gtlu121 + Gt212*Gtlu121 + Gt112*Gtlu122 + 
      Gt222*Gtlu122 + Gt113*Gtlu123 + Gt223*Gtlu123 + Gt312*Gtlu131 + 
      Gt322*Gtlu132 + Gt323*Gtlu133 + Gt111*Gtlu211 + Gt112*Gtlu212 + 
      Gt113*Gtlu213 + 2*Gt211*Gtlu221 + 2*Gt212*Gtlu222 + 2*Gt213*Gtlu223 + 
      Gt311*Gtlu231 + Gt312*Gtlu232 + Gt313*Gtlu233 + Gt311*Gtlu321 + 
      Gt312*Gtlu322 + Gt313*Gtlu323 + 0.5*(-(gtu11*PDPDgt1211) - 
      gtu12*PDPDgt1212 - gtu13*PDPDgt1213 - gtu12*PDPDgt1221 - 
      gtu22*PDPDgt1222 - gtu23*PDPDgt1223 - gtu13*PDPDgt1231 - 
      gtu23*PDPDgt1232 - gtu33*PDPDgt1233) + 0.5*(gt12L*PDXt11 + gt22L*PDXt21 
      + gt23L*PDXt31) + 0.5*(gt11L*PDXt12 + gt12L*PDXt22 + gt13L*PDXt32) + 
      0.5*(Gtl112*Xtn1 + Gtl122*Xtn2 + Gtl123*Xtn3) + 0.5*(Gtl211*Xtn1 + 
      Gtl212*Xtn2 + Gtl213*Xtn3);
    
    CCTK_REAL Rt13 CCTK_ATTRIBUTE_UNUSED = Gt113*Gtlu111 + Gt123*Gtlu112 + 
      Gt133*Gtlu113 + Gt213*Gtlu121 + Gt223*Gtlu122 + Gt233*Gtlu123 + 
      Gt111*Gtlu131 + Gt313*Gtlu131 + Gt112*Gtlu132 + Gt323*Gtlu132 + 
      Gt113*Gtlu133 + Gt333*Gtlu133 + Gt211*Gtlu231 + Gt212*Gtlu232 + 
      Gt213*Gtlu233 + Gt111*Gtlu311 + Gt112*Gtlu312 + Gt113*Gtlu313 + 
      Gt211*Gtlu321 + Gt212*Gtlu322 + Gt213*Gtlu323 + 2*Gt311*Gtlu331 + 
      2*Gt312*Gtlu332 + 2*Gt313*Gtlu333 + 0.5*(-(gtu11*PDPDgt1311) - 
      gtu12*PDPDgt1312 - gtu13*PDPDgt1313 - gtu12*PDPDgt1321 - 
      gtu22*PDPDgt1322 - gtu23*PDPDgt1323 - gtu13*PDPDgt1331 - 
      gtu23*PDPDgt1332 - gtu33*PDPDgt1333) + 0.5*(gt13L*PDXt11 + gt23L*PDXt21 
      + gt33L*PDXt31) + 0.5*(gt11L*PDXt13 + gt12L*PDXt23 + gt13L*PDXt33) + 
      0.5*(Gtl113*Xtn1 + Gtl123*Xtn2 + Gtl133*Xtn3) + 0.5*(Gtl311*Xtn1 + 
      Gtl312*Xtn2 + Gtl313*Xtn3);
    
    CCTK_REAL Rt22 CCTK_ATTRIBUTE_UNUSED = Gt112*Gtlu121 + Gt122*Gtlu122 + 
      Gt123*Gtlu123 + 2*Gt112*Gtlu211 + 2*Gt122*Gtlu212 + 2*Gt123*Gtlu213 + 
      3*Gt212*Gtlu221 + 3*Gt222*Gtlu222 + 3*Gt223*Gtlu223 + 2*Gt312*Gtlu231 + 
      2*Gt322*Gtlu232 + 2*Gt323*Gtlu233 + Gt312*Gtlu321 + Gt322*Gtlu322 + 
      Gt323*Gtlu323 + 0.5*(-(gtu11*PDPDgt2211) - gtu12*PDPDgt2212 - 
      gtu13*PDPDgt2213 - gtu12*PDPDgt2221 - gtu22*PDPDgt2222 - 
      gtu23*PDPDgt2223 - gtu13*PDPDgt2231 - gtu23*PDPDgt2232 - 
      gtu33*PDPDgt2233) + gt12L*PDXt12 + gt22L*PDXt22 + gt23L*PDXt32 + 
      Gtl212*Xtn1 + Gtl222*Xtn2 + Gtl223*Xtn3;
    
    CCTK_REAL Rt23 CCTK_ATTRIBUTE_UNUSED = Gt112*Gtlu131 + Gt122*Gtlu132 + 
      Gt123*Gtlu133 + Gt113*Gtlu211 + Gt123*Gtlu212 + Gt133*Gtlu213 + 
      Gt213*Gtlu221 + Gt223*Gtlu222 + Gt233*Gtlu223 + Gt212*Gtlu231 + 
      Gt313*Gtlu231 + Gt222*Gtlu232 + Gt323*Gtlu232 + Gt223*Gtlu233 + 
      Gt333*Gtlu233 + Gt112*Gtlu311 + Gt122*Gtlu312 + Gt123*Gtlu313 + 
      Gt212*Gtlu321 + Gt222*Gtlu322 + Gt223*Gtlu323 + 2*Gt312*Gtlu331 + 
      2*Gt322*Gtlu332 + 2*Gt323*Gtlu333 + 0.5*(-(gtu11*PDPDgt2311) - 
      gtu12*PDPDgt2312 - gtu13*PDPDgt2313 - gtu12*PDPDgt2321 - 
      gtu22*PDPDgt2322 - gtu23*PDPDgt2323 - gtu13*PDPDgt2331 - 
      gtu23*PDPDgt2332 - gtu33*PDPDgt2333) + 0.5*(gt13L*PDXt12 + gt23L*PDXt22 
      + gt33L*PDXt32) + 0.5*(gt12L*PDXt13 + gt22L*PDXt23 + gt23L*PDXt33) + 
      0.5*(Gtl213*Xtn1 + Gtl223*Xtn2 + Gtl233*Xtn3) + 0.5*(Gtl312*Xtn1 + 
      Gtl322*Xtn2 + Gtl323*Xtn3);
    
    CCTK_REAL Rt33 CCTK_ATTRIBUTE_UNUSED = Gt113*Gtlu131 + Gt123*Gtlu132 + 
      Gt133*Gtlu133 + Gt213*Gtlu231 + Gt223*Gtlu232 + Gt233*Gtlu233 + 
      2*Gt113*Gtlu311 + 2*Gt123*Gtlu312 + 2*Gt133*Gtlu313 + 2*Gt213*Gtlu321 + 
      2*Gt223*Gtlu322 + 2*Gt233*Gtlu323 + 3*Gt313*Gtlu331 + 3*Gt323*Gtlu332 + 
      3*Gt333*Gtlu333 + 0.5*(-(gtu11*PDPDgt3311) - gtu12*PDPDgt3312 - 
      gtu13*PDPDgt3313 - gtu12*PDPDgt3321 - gtu22*PDPDgt3322 - 
      gtu23*PDPDgt3323 - gtu13*PDPDgt3331 - gtu23*PDPDgt3332 - 
      gtu33*PDPDgt3333) + gt13L*PDXt13 + gt23L*PDXt23 + gt33L*PDXt33 + 
      Gtl313*Xtn1 + Gtl323*Xtn2 + Gtl333*Xtn3;
    
    CCTK_REAL fac1 CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,-0.5*pow(phiWL,-1),1);
    
    CCTK_REAL cdphi1 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*fac1;
    
    CCTK_REAL cdphi2 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*fac1;
    
    CCTK_REAL cdphi3 CCTK_ATTRIBUTE_UNUSED = PDphiW3L*fac1;
    
    CCTK_REAL fac2 CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,0.5*pow(phiWL,-2),0);
    
    CCTK_REAL cdphi211 CCTK_ATTRIBUTE_UNUSED = fac1*(-(PDphiW1L*Gt111) - 
      PDphiW2L*Gt211 - PDphiW3L*Gt311 + PDPDphiW11) + fac2*pow(PDphiW1L,2);
    
    CCTK_REAL cdphi212 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW2L*fac2 + 
      fac1*(-(PDphiW1L*Gt112) - PDphiW2L*Gt212 - PDphiW3L*Gt312 + 
      PDPDphiW12);
    
    CCTK_REAL cdphi213 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt113) - PDphiW2L*Gt213 - PDphiW3L*Gt313 + 
      PDPDphiW13);
    
    CCTK_REAL cdphi221 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW2L*fac2 + 
      fac1*(-(PDphiW1L*Gt112) - PDphiW2L*Gt212 - PDphiW3L*Gt312 + 
      PDPDphiW21);
    
    CCTK_REAL cdphi222 CCTK_ATTRIBUTE_UNUSED = fac1*(-(PDphiW1L*Gt122) - 
      PDphiW2L*Gt222 - PDphiW3L*Gt322 + PDPDphiW22) + fac2*pow(PDphiW2L,2);
    
    CCTK_REAL cdphi223 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt123) - PDphiW2L*Gt223 - PDphiW3L*Gt323 + 
      PDPDphiW23);
    
    CCTK_REAL cdphi231 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt113) - PDphiW2L*Gt213 - PDphiW3L*Gt313 + 
      PDPDphiW31);
    
    CCTK_REAL cdphi232 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt123) - PDphiW2L*Gt223 - PDphiW3L*Gt323 + 
      PDPDphiW32);
    
    CCTK_REAL cdphi233 CCTK_ATTRIBUTE_UNUSED = fac1*(-(PDphiW1L*Gt133) - 
      PDphiW2L*Gt233 - PDphiW3L*Gt333 + PDPDphiW33) + fac2*pow(PDphiW3L,2);
    
    CCTK_REAL Rphi11 CCTK_ATTRIBUTE_UNUSED = -2*cdphi211 - 
      2*gt11L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt11L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33)) + 
      4*pow(cdphi1,2);
    
    CCTK_REAL Rphi12 CCTK_ATTRIBUTE_UNUSED = 4*cdphi1*cdphi2 - 2*cdphi221 
      - 2*gt12L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt12L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33));
    
    CCTK_REAL Rphi13 CCTK_ATTRIBUTE_UNUSED = -2*cdphi231 + 4*cdphi1*cdphi3 
      - 2*gt13L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt13L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33));
    
    CCTK_REAL Rphi22 CCTK_ATTRIBUTE_UNUSED = -2*cdphi222 - 
      2*gt22L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt22L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33)) + 
      4*pow(cdphi2,2);
    
    CCTK_REAL Rphi23 CCTK_ATTRIBUTE_UNUSED = -2*cdphi232 + 4*cdphi2*cdphi3 
      - 2*gt23L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt23L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33));
    
    CCTK_REAL Rphi33 CCTK_ATTRIBUTE_UNUSED = -2*cdphi233 - 
      2*gt33L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt33L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33)) + 
      4*pow(cdphi3,2);
    
    CCTK_REAL Atm11 CCTK_ATTRIBUTE_UNUSED = At11L*gtu11 + At12L*gtu12 + 
      At13L*gtu13;
    
    CCTK_REAL Atm21 CCTK_ATTRIBUTE_UNUSED = At11L*gtu12 + At12L*gtu22 + 
      At13L*gtu23;
    
    CCTK_REAL Atm31 CCTK_ATTRIBUTE_UNUSED = At11L*gtu13 + At12L*gtu23 + 
      At13L*gtu33;
    
    CCTK_REAL Atm12 CCTK_ATTRIBUTE_UNUSED = At12L*gtu11 + At22L*gtu12 + 
      At23L*gtu13;
    
    CCTK_REAL Atm22 CCTK_ATTRIBUTE_UNUSED = At12L*gtu12 + At22L*gtu22 + 
      At23L*gtu23;
    
    CCTK_REAL Atm32 CCTK_ATTRIBUTE_UNUSED = At12L*gtu13 + At22L*gtu23 + 
      At23L*gtu33;
    
    CCTK_REAL Atm13 CCTK_ATTRIBUTE_UNUSED = At13L*gtu11 + At23L*gtu12 + 
      At33L*gtu13;
    
    CCTK_REAL Atm23 CCTK_ATTRIBUTE_UNUSED = At13L*gtu12 + At23L*gtu22 + 
      At33L*gtu23;
    
    CCTK_REAL Atm33 CCTK_ATTRIBUTE_UNUSED = At13L*gtu13 + At23L*gtu23 + 
      At33L*gtu33;
    
    CCTK_REAL R11 CCTK_ATTRIBUTE_UNUSED = Rphi11 + Rt11;
    
    CCTK_REAL R12 CCTK_ATTRIBUTE_UNUSED = Rphi12 + Rt12;
    
    CCTK_REAL R13 CCTK_ATTRIBUTE_UNUSED = Rphi13 + Rt13;
    
    CCTK_REAL R22 CCTK_ATTRIBUTE_UNUSED = Rphi22 + Rt22;
    
    CCTK_REAL R23 CCTK_ATTRIBUTE_UNUSED = Rphi23 + Rt23;
    
    CCTK_REAL R33 CCTK_ATTRIBUTE_UNUSED = Rphi33 + Rt33;
    
    CCTK_REAL rho CCTK_ATTRIBUTE_UNUSED = (eTttL - 2*(beta1L*eTtxL + 
      beta2L*eTtyL + beta3L*eTtzL) + beta1L*(beta1L*eTxxL + beta2L*eTxyL + 
      beta3L*eTxzL) + beta2L*(beta1L*eTxyL + beta2L*eTyyL + beta3L*eTyzL) + 
      beta3L*(beta1L*eTxzL + beta2L*eTyzL + beta3L*eTzzL))*pow(alphaL,-2);
    
    CCTK_REAL S1 CCTK_ATTRIBUTE_UNUSED = -((eTtxL - beta1L*eTxxL - 
      beta2L*eTxyL - beta3L*eTxzL)*pow(alphaL,-1));
    
    CCTK_REAL S2 CCTK_ATTRIBUTE_UNUSED = -((eTtyL - beta1L*eTxyL - 
      beta2L*eTyyL - beta3L*eTyzL)*pow(alphaL,-1));
    
    CCTK_REAL S3 CCTK_ATTRIBUTE_UNUSED = -((eTtzL - beta1L*eTxzL - 
      beta2L*eTyzL - beta3L*eTzzL)*pow(alphaL,-1));
    
    CCTK_REAL trR CCTK_ATTRIBUTE_UNUSED = gu11*R11 + 2*gu12*R12 + 
      2*gu13*R13 + gu22*R22 + 2*gu23*R23 + gu33*R33;
    
    CCTK_REAL cXt1L CCTK_ATTRIBUTE_UNUSED = -Xt1L + Xtn1;
    
    CCTK_REAL cXt2L CCTK_ATTRIBUTE_UNUSED = -Xt2L + Xtn2;
    
    CCTK_REAL cXt3L CCTK_ATTRIBUTE_UNUSED = -Xt3L + Xtn3;
    
    CCTK_REAL HL CCTK_ATTRIBUTE_UNUSED = -2*Atm12*Atm21 - 2*Atm13*Atm31 - 
      2*Atm23*Atm32 - 16*Pi*rho + trR + 
      0.666666666666666666666666666667*pow(trKL,2) - pow(Atm11,2) - 
      pow(Atm22,2) - pow(Atm33,2);
    
    CCTK_REAL M1L CCTK_ATTRIBUTE_UNUSED = gtu11*(6*At11L*cdphi1 - 
      2*At11L*Gt111 - 2*At12L*Gt211 - 2*At13L*Gt311 + PDAt111) + 
      gtu12*(6*At11L*cdphi2 - 2*At11L*Gt112 - 2*At12L*Gt212 - 2*At13L*Gt312 + 
      PDAt112) + gtu13*(6*At11L*cdphi3 - 2*At11L*Gt113 - 2*At12L*Gt213 - 
      2*At13L*Gt313 + PDAt113) + gtu12*(6*At12L*cdphi1 - At12L*Gt111 - 
      At11L*Gt112 - At22L*Gt211 - At12L*Gt212 - At23L*Gt311 - At13L*Gt312 + 
      PDAt121) + gtu22*(6*At12L*cdphi2 - At12L*Gt112 - At11L*Gt122 - 
      At22L*Gt212 - At12L*Gt222 - At23L*Gt312 - At13L*Gt322 + PDAt122) + 
      gtu23*(6*At12L*cdphi3 - At12L*Gt113 - At11L*Gt123 - At22L*Gt213 - 
      At12L*Gt223 - At23L*Gt313 - At13L*Gt323 + PDAt123) + 
      gtu13*(6*At13L*cdphi1 - At13L*Gt111 - At11L*Gt113 - At23L*Gt211 - 
      At12L*Gt213 - At33L*Gt311 - At13L*Gt313 + PDAt131) + 
      gtu23*(6*At13L*cdphi2 - At13L*Gt112 - At11L*Gt123 - At23L*Gt212 - 
      At12L*Gt223 - At33L*Gt312 - At13L*Gt323 + PDAt132) + 
      gtu33*(6*At13L*cdphi3 - At13L*Gt113 - At11L*Gt133 - At23L*Gt213 - 
      At12L*Gt233 - At33L*Gt313 - At13L*Gt333 + PDAt133) - 
      0.666666666666666666666666666667*PDtrK1 - 8*Pi*S1;
    
    CCTK_REAL M2L CCTK_ATTRIBUTE_UNUSED = gtu11*(6*At12L*cdphi1 - 
      At12L*Gt111 - At11L*Gt112 - At22L*Gt211 - At12L*Gt212 - At23L*Gt311 - 
      At13L*Gt312 + PDAt211) + gtu12*(6*At12L*cdphi2 - At12L*Gt112 - 
      At11L*Gt122 - At22L*Gt212 - At12L*Gt222 - At23L*Gt312 - At13L*Gt322 + 
      PDAt212) + gtu13*(6*At12L*cdphi3 - At12L*Gt113 - At11L*Gt123 - 
      At22L*Gt213 - At12L*Gt223 - At23L*Gt313 - At13L*Gt323 + PDAt213) + 
      gtu12*(6*At22L*cdphi1 - 2*At12L*Gt112 - 2*At22L*Gt212 - 2*At23L*Gt312 + 
      PDAt221) + gtu22*(6*At22L*cdphi2 - 2*At12L*Gt122 - 2*At22L*Gt222 - 
      2*At23L*Gt322 + PDAt222) + gtu23*(6*At22L*cdphi3 - 2*At12L*Gt123 - 
      2*At22L*Gt223 - 2*At23L*Gt323 + PDAt223) + gtu13*(6*At23L*cdphi1 - 
      At13L*Gt112 - At12L*Gt113 - At23L*Gt212 - At22L*Gt213 - At33L*Gt312 - 
      At23L*Gt313 + PDAt231) + gtu23*(6*At23L*cdphi2 - At13L*Gt122 - 
      At12L*Gt123 - At23L*Gt222 - At22L*Gt223 - At33L*Gt322 - At23L*Gt323 + 
      PDAt232) + gtu33*(6*At23L*cdphi3 - At13L*Gt123 - At12L*Gt133 - 
      At23L*Gt223 - At22L*Gt233 - At33L*Gt323 - At23L*Gt333 + PDAt233) - 
      0.666666666666666666666666666667*PDtrK2 - 8*Pi*S2;
    
    CCTK_REAL M3L CCTK_ATTRIBUTE_UNUSED = gtu11*(6*At13L*cdphi1 - 
      At13L*Gt111 - At11L*Gt113 - At23L*Gt211 - At12L*Gt213 - At33L*Gt311 - 
      At13L*Gt313 + PDAt311) + gtu12*(6*At13L*cdphi2 - At13L*Gt112 - 
      At11L*Gt123 - At23L*Gt212 - At12L*Gt223 - At33L*Gt312 - At13L*Gt323 + 
      PDAt312) + gtu13*(6*At13L*cdphi3 - At13L*Gt113 - At11L*Gt133 - 
      At23L*Gt213 - At12L*Gt233 - At33L*Gt313 - At13L*Gt333 + PDAt313) + 
      gtu12*(6*At23L*cdphi1 - At13L*Gt112 - At12L*Gt113 - At23L*Gt212 - 
      At22L*Gt213 - At33L*Gt312 - At23L*Gt313 + PDAt321) + 
      gtu22*(6*At23L*cdphi2 - At13L*Gt122 - At12L*Gt123 - At23L*Gt222 - 
      At22L*Gt223 - At33L*Gt322 - At23L*Gt323 + PDAt322) + 
      gtu23*(6*At23L*cdphi3 - At13L*Gt123 - At12L*Gt133 - At23L*Gt223 - 
      At22L*Gt233 - At33L*Gt323 - At23L*Gt333 + PDAt323) + 
      gtu13*(6*At33L*cdphi1 - 2*At13L*Gt113 - 2*At23L*Gt213 - 2*At33L*Gt313 + 
      PDAt331) + gtu23*(6*At33L*cdphi2 - 2*At13L*Gt123 - 2*At23L*Gt223 - 
      2*At33L*Gt323 + PDAt332) + gtu33*(6*At33L*cdphi3 - 2*At13L*Gt133 - 
      2*At23L*Gt233 - 2*At33L*Gt333 + PDAt333) - 
      0.666666666666666666666666666667*PDtrK3 - 8*Pi*S3;
    /* Copy local copies back to grid functions */
    cXt1[index] = cXt1L;
    cXt2[index] = cXt2L;
    cXt3[index] = cXt3L;
    H[index] = HL;
    M1[index] = M1L;
    M2[index] = M2L;
    M3[index] = M3L;
  }
  CCTK_ENDLOOP3(ML_BSSN_FD4_ConstraintsInterior);
}
extern "C" void ML_BSSN_FD4_ConstraintsInterior(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_FD4_ConstraintsInterior_Body");
  }
  if (cctk_iteration % ML_BSSN_FD4_ConstraintsInterior_calc_every != ML_BSSN_FD4_ConstraintsInterior_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_BSSN_FD4::ML_confac",
    "ML_BSSN_FD4::ML_cons_Gamma",
    "ML_BSSN_FD4::ML_curv",
    "ML_BSSN_FD4::ML_dconfac",
    "ML_BSSN_FD4::ML_dmetric",
    "ML_BSSN_FD4::ML_Gamma",
    "ML_BSSN_FD4::ML_Ham",
    "ML_BSSN_FD4::ML_lapse",
    "ML_BSSN_FD4::ML_metric",
    "ML_BSSN_FD4::ML_mom",
    "ML_BSSN_FD4::ML_shift",
    "ML_BSSN_FD4::ML_trace_curv"};
  AssertGroupStorage(cctkGH, "ML_BSSN_FD4_ConstraintsInterior", 12, groups);
  
  
  LoopOverInterior(cctkGH, ML_BSSN_FD4_ConstraintsInterior_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_FD4_ConstraintsInterior_Body");
  }
}

} // namespace ML_BSSN_FD4
