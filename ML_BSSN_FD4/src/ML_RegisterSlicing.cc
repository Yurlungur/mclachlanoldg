#include <Slicing.h>

#include <cctk.h>

extern "C"
int ML_BSSN_FD4_RegisterSlicing(void)
{
  Einstein_RegisterSlicing("ML_BSSN_FD4");
  return 0;
}
