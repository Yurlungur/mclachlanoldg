#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cassert>
#include <cmath>
#include <cstring>
#include <cstring>
#include <vector>
using namespace std;

extern "C"
void ML_BSSN_FD4_ConvertCoordinates(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const bool use_jacobian1 =
    (!CCTK_IsFunctionAliased("MultiPatch_GetMap") ||
     MultiPatch_GetMap(cctkGH) != jacobian_identity_map) &&
    strlen(jacobian_group) > 0;
  const bool use_jacobian =
    assume_use_jacobian>=0 ? assume_use_jacobian : use_jacobian1;
  if (!use_jacobian) return;
  
  const int map = MultiPatch_GetMap(cctkGH);
  assert(map >= 0);
  
  const int npoints = cctk_ash[0] * cctk_ash[1] * cctk_ash[2];
  
  vector<CCTK_INT> patch(npoints, map);
  vector<CCTK_REAL> localx(npoints), localy(npoints), localz(npoints);
  for (int i=0; i<npoints; ++i) {
    localx[i] = x[i];
    localy[i] = y[i];
    localz[i] = z[i];
  }
  vector<const CCTK_REAL*> localcoords(3);
  localcoords[0] = &localx[0];
  localcoords[1] = &localy[0];
  localcoords[2] = &localz[0];
  
  vector<CCTK_REAL*> globalcoords(3);
  globalcoords[0] = x;
  globalcoords[1] = y;
  globalcoords[2] = z;
  
  vector<CCTK_REAL*> dadx(9);
  const int gi = CCTK_GroupIndex(jacobian_group);
  assert(gi >= 0);
  const int v0 = CCTK_FirstVarIndexI(gi);
  assert(v0 >= 0);
  const int nv = CCTK_NumVarsInGroupI(gi);
  assert(nv == 9);
  for (int d=0; d<9; ++d) {
    dadx[d] = static_cast<CCTK_REAL*>(CCTK_VarDataPtrI(cctkGH, 0, v0 + d));
  }
  
  const int ierr = MultiPatch_LocalToGlobal
    (cctkGH, 3, npoints,
     &patch[0], &localcoords[0],
     &globalcoords[0], 0, 0, &dadx[0], 0, 0, 0);
  assert(!ierr);
  
#pragma omp parallel
  CCTK_LOOP3_ALL(ML_BSSN_FD4_ConvertCoordinates_r, cctkGH, i,j,k) {
    int const ind = CCTK_GFINDEX3D(cctkGH, i,j,k);
    r[ind] = sqrt(pow(x[ind],2) + pow(y[ind],2) + pow(z[ind],2));
  } CCTK_ENDLOOP3_ALL(ML_BSSN_FD4_ConvertCoordinates_r);
}
