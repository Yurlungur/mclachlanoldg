/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"
#include "loopcontrol.h"

namespace ML_BSSN_FD4 {

extern "C" void ML_BSSN_FD4_EvolutionInteriorSplit32_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_FD4_EvolutionInteriorSplit32_calc_every != ML_BSSN_FD4_EvolutionInteriorSplit32_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_dtshiftrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_dtshiftrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_Gammarhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_Gammarhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_shiftrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_shiftrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_trace_curvrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_trace_curvrhs.");
  return;
}

static void ML_BSSN_FD4_EvolutionInteriorSplit32_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_BSSN_FD4_EvolutionInteriorSplit32,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL alphaL CCTK_ATTRIBUTE_UNUSED = alpha[index];
    CCTK_REAL At11L CCTK_ATTRIBUTE_UNUSED = At11[index];
    CCTK_REAL At12L CCTK_ATTRIBUTE_UNUSED = At12[index];
    CCTK_REAL At13L CCTK_ATTRIBUTE_UNUSED = At13[index];
    CCTK_REAL At22L CCTK_ATTRIBUTE_UNUSED = At22[index];
    CCTK_REAL At23L CCTK_ATTRIBUTE_UNUSED = At23[index];
    CCTK_REAL At33L CCTK_ATTRIBUTE_UNUSED = At33[index];
    CCTK_REAL B1L CCTK_ATTRIBUTE_UNUSED = B1[index];
    CCTK_REAL B2L CCTK_ATTRIBUTE_UNUSED = B2[index];
    CCTK_REAL B3L CCTK_ATTRIBUTE_UNUSED = B3[index];
    CCTK_REAL beta1L CCTK_ATTRIBUTE_UNUSED = beta1[index];
    CCTK_REAL beta2L CCTK_ATTRIBUTE_UNUSED = beta2[index];
    CCTK_REAL beta3L CCTK_ATTRIBUTE_UNUSED = beta3[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL myDetJL CCTK_ATTRIBUTE_UNUSED = myDetJ[index];
    CCTK_REAL PDalpha1L CCTK_ATTRIBUTE_UNUSED = PDalpha1[index];
    CCTK_REAL PDalpha2L CCTK_ATTRIBUTE_UNUSED = PDalpha2[index];
    CCTK_REAL PDalpha3L CCTK_ATTRIBUTE_UNUSED = PDalpha3[index];
    CCTK_REAL PDbeta11L CCTK_ATTRIBUTE_UNUSED = PDbeta11[index];
    CCTK_REAL PDbeta12L CCTK_ATTRIBUTE_UNUSED = PDbeta12[index];
    CCTK_REAL PDbeta13L CCTK_ATTRIBUTE_UNUSED = PDbeta13[index];
    CCTK_REAL PDbeta21L CCTK_ATTRIBUTE_UNUSED = PDbeta21[index];
    CCTK_REAL PDbeta22L CCTK_ATTRIBUTE_UNUSED = PDbeta22[index];
    CCTK_REAL PDbeta23L CCTK_ATTRIBUTE_UNUSED = PDbeta23[index];
    CCTK_REAL PDbeta31L CCTK_ATTRIBUTE_UNUSED = PDbeta31[index];
    CCTK_REAL PDbeta32L CCTK_ATTRIBUTE_UNUSED = PDbeta32[index];
    CCTK_REAL PDbeta33L CCTK_ATTRIBUTE_UNUSED = PDbeta33[index];
    CCTK_REAL PDgt111L CCTK_ATTRIBUTE_UNUSED = PDgt111[index];
    CCTK_REAL PDgt112L CCTK_ATTRIBUTE_UNUSED = PDgt112[index];
    CCTK_REAL PDgt113L CCTK_ATTRIBUTE_UNUSED = PDgt113[index];
    CCTK_REAL PDgt121L CCTK_ATTRIBUTE_UNUSED = PDgt121[index];
    CCTK_REAL PDgt122L CCTK_ATTRIBUTE_UNUSED = PDgt122[index];
    CCTK_REAL PDgt123L CCTK_ATTRIBUTE_UNUSED = PDgt123[index];
    CCTK_REAL PDgt131L CCTK_ATTRIBUTE_UNUSED = PDgt131[index];
    CCTK_REAL PDgt132L CCTK_ATTRIBUTE_UNUSED = PDgt132[index];
    CCTK_REAL PDgt133L CCTK_ATTRIBUTE_UNUSED = PDgt133[index];
    CCTK_REAL PDgt221L CCTK_ATTRIBUTE_UNUSED = PDgt221[index];
    CCTK_REAL PDgt222L CCTK_ATTRIBUTE_UNUSED = PDgt222[index];
    CCTK_REAL PDgt223L CCTK_ATTRIBUTE_UNUSED = PDgt223[index];
    CCTK_REAL PDgt231L CCTK_ATTRIBUTE_UNUSED = PDgt231[index];
    CCTK_REAL PDgt232L CCTK_ATTRIBUTE_UNUSED = PDgt232[index];
    CCTK_REAL PDgt233L CCTK_ATTRIBUTE_UNUSED = PDgt233[index];
    CCTK_REAL PDgt331L CCTK_ATTRIBUTE_UNUSED = PDgt331[index];
    CCTK_REAL PDgt332L CCTK_ATTRIBUTE_UNUSED = PDgt332[index];
    CCTK_REAL PDgt333L CCTK_ATTRIBUTE_UNUSED = PDgt333[index];
    CCTK_REAL PDphiW1L CCTK_ATTRIBUTE_UNUSED = PDphiW1[index];
    CCTK_REAL PDphiW2L CCTK_ATTRIBUTE_UNUSED = PDphiW2[index];
    CCTK_REAL PDphiW3L CCTK_ATTRIBUTE_UNUSED = PDphiW3[index];
    CCTK_REAL phiWL CCTK_ATTRIBUTE_UNUSED = phiW[index];
    CCTK_REAL rL CCTK_ATTRIBUTE_UNUSED = r[index];
    CCTK_REAL trKL CCTK_ATTRIBUTE_UNUSED = trK[index];
    CCTK_REAL Xt1L CCTK_ATTRIBUTE_UNUSED = Xt1[index];
    CCTK_REAL Xt2L CCTK_ATTRIBUTE_UNUSED = Xt2[index];
    CCTK_REAL Xt3L CCTK_ATTRIBUTE_UNUSED = Xt3[index];
    
    CCTK_REAL eTttL, eTtxL, eTtyL, eTtzL, eTxxL, eTxyL, eTxzL, eTyyL, eTyzL, eTzzL CCTK_ATTRIBUTE_UNUSED ;
    
    if (assume_stress_energy_state>=0 ? assume_stress_energy_state : *stress_energy_state)
    {
      eTttL = eTtt[index];
      eTtxL = eTtx[index];
      eTtyL = eTty[index];
      eTtzL = eTtz[index];
      eTxxL = eTxx[index];
      eTxyL = eTxy[index];
      eTxzL = eTxz[index];
      eTyyL = eTyy[index];
      eTyzL = eTyz[index];
      eTzzL = eTzz[index];
    }
    else
    {
      eTttL = 0.;
      eTtxL = 0.;
      eTtyL = 0.;
      eTtzL = 0.;
      eTxxL = 0.;
      eTxyL = 0.;
      eTxzL = 0.;
      eTyyL = 0.;
      eTyzL = 0.;
      eTzzL = 0.;
    }
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDtrK1 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,-2,0,0) - 
      8*GFOffset(trK,-1,0,0) + 8*GFOffset(trK,1,0,0) - 
      GFOffset(trK,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDtrK2 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,0,-2,0) - 
      8*GFOffset(trK,0,-1,0) + 8*GFOffset(trK,0,1,0) - 
      GFOffset(trK,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDtrK3 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,0,0,-2) - 
      8*GFOffset(trK,0,0,-1) + 8*GFOffset(trK,0,0,1) - 
      GFOffset(trK,0,0,2))*pow(dz,-1);
    
    CCTK_REAL PDtrK1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDtrK2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDtrK3 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDtrK1 = J11L*LDtrK1 + J21L*LDtrK2 + J31L*LDtrK3;
      
      PDtrK2 = J12L*LDtrK1 + J22L*LDtrK2 + J32L*LDtrK3;
      
      PDtrK3 = J13L*LDtrK1 + J23L*LDtrK2 + J33L*LDtrK3;
    }
    else
    {
      PDtrK1 = LDtrK1;
      
      PDtrK2 = LDtrK2;
      
      PDtrK3 = LDtrK3;
    }
    
    CCTK_REAL LDPDalpha11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha1,-2,0,0) - 
      8*GFOffset(PDalpha1,-1,0,0) + 8*GFOffset(PDalpha1,1,0,0) - 
      GFOffset(PDalpha1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha1,0,-2,0) - 
      8*GFOffset(PDalpha1,0,-1,0) + 8*GFOffset(PDalpha1,0,1,0) - 
      GFOffset(PDalpha1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha1,0,0,-2) - 
      8*GFOffset(PDalpha1,0,0,-1) + 8*GFOffset(PDalpha1,0,0,1) - 
      GFOffset(PDalpha1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDalpha21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha2,-2,0,0) - 
      8*GFOffset(PDalpha2,-1,0,0) + 8*GFOffset(PDalpha2,1,0,0) - 
      GFOffset(PDalpha2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha2,0,-2,0) - 
      8*GFOffset(PDalpha2,0,-1,0) + 8*GFOffset(PDalpha2,0,1,0) - 
      GFOffset(PDalpha2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha2,0,0,-2) - 
      8*GFOffset(PDalpha2,0,0,-1) + 8*GFOffset(PDalpha2,0,0,1) - 
      GFOffset(PDalpha2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDalpha31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha3,-2,0,0) - 
      8*GFOffset(PDalpha3,-1,0,0) + 8*GFOffset(PDalpha3,1,0,0) - 
      GFOffset(PDalpha3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha3,0,-2,0) - 
      8*GFOffset(PDalpha3,0,-1,0) + 8*GFOffset(PDalpha3,0,1,0) - 
      GFOffset(PDalpha3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha3,0,0,-2) - 
      8*GFOffset(PDalpha3,0,0,-1) + 8*GFOffset(PDalpha3,0,0,1) - 
      GFOffset(PDalpha3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta111 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta11,-2,0,0) - 
      8*GFOffset(PDbeta11,-1,0,0) + 8*GFOffset(PDbeta11,1,0,0) - 
      GFOffset(PDbeta11,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta211 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta21,-2,0,0) - 
      8*GFOffset(PDbeta21,-1,0,0) + 8*GFOffset(PDbeta21,1,0,0) - 
      GFOffset(PDbeta21,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta311 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta31,-2,0,0) - 
      8*GFOffset(PDbeta31,-1,0,0) + 8*GFOffset(PDbeta31,1,0,0) - 
      GFOffset(PDbeta31,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta112 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta11,0,-2,0) - 
      8*GFOffset(PDbeta11,0,-1,0) + 8*GFOffset(PDbeta11,0,1,0) - 
      GFOffset(PDbeta11,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta212 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta21,0,-2,0) - 
      8*GFOffset(PDbeta21,0,-1,0) + 8*GFOffset(PDbeta21,0,1,0) - 
      GFOffset(PDbeta21,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta312 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta31,0,-2,0) - 
      8*GFOffset(PDbeta31,0,-1,0) + 8*GFOffset(PDbeta31,0,1,0) - 
      GFOffset(PDbeta31,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta113 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta11,0,0,-2) - 
      8*GFOffset(PDbeta11,0,0,-1) + 8*GFOffset(PDbeta11,0,0,1) - 
      GFOffset(PDbeta11,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta213 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta21,0,0,-2) - 
      8*GFOffset(PDbeta21,0,0,-1) + 8*GFOffset(PDbeta21,0,0,1) - 
      GFOffset(PDbeta21,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta313 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta31,0,0,-2) - 
      8*GFOffset(PDbeta31,0,0,-1) + 8*GFOffset(PDbeta31,0,0,1) - 
      GFOffset(PDbeta31,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta121 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta12,-2,0,0) - 
      8*GFOffset(PDbeta12,-1,0,0) + 8*GFOffset(PDbeta12,1,0,0) - 
      GFOffset(PDbeta12,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta221 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta22,-2,0,0) - 
      8*GFOffset(PDbeta22,-1,0,0) + 8*GFOffset(PDbeta22,1,0,0) - 
      GFOffset(PDbeta22,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta321 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta32,-2,0,0) - 
      8*GFOffset(PDbeta32,-1,0,0) + 8*GFOffset(PDbeta32,1,0,0) - 
      GFOffset(PDbeta32,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta122 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta12,0,-2,0) - 
      8*GFOffset(PDbeta12,0,-1,0) + 8*GFOffset(PDbeta12,0,1,0) - 
      GFOffset(PDbeta12,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta222 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta22,0,-2,0) - 
      8*GFOffset(PDbeta22,0,-1,0) + 8*GFOffset(PDbeta22,0,1,0) - 
      GFOffset(PDbeta22,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta322 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta32,0,-2,0) - 
      8*GFOffset(PDbeta32,0,-1,0) + 8*GFOffset(PDbeta32,0,1,0) - 
      GFOffset(PDbeta32,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta123 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta12,0,0,-2) - 
      8*GFOffset(PDbeta12,0,0,-1) + 8*GFOffset(PDbeta12,0,0,1) - 
      GFOffset(PDbeta12,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta223 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta22,0,0,-2) - 
      8*GFOffset(PDbeta22,0,0,-1) + 8*GFOffset(PDbeta22,0,0,1) - 
      GFOffset(PDbeta22,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta323 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta32,0,0,-2) - 
      8*GFOffset(PDbeta32,0,0,-1) + 8*GFOffset(PDbeta32,0,0,1) - 
      GFOffset(PDbeta32,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta131 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta13,-2,0,0) - 
      8*GFOffset(PDbeta13,-1,0,0) + 8*GFOffset(PDbeta13,1,0,0) - 
      GFOffset(PDbeta13,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta231 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta23,-2,0,0) - 
      8*GFOffset(PDbeta23,-1,0,0) + 8*GFOffset(PDbeta23,1,0,0) - 
      GFOffset(PDbeta23,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta33,-2,0,0) - 
      8*GFOffset(PDbeta33,-1,0,0) + 8*GFOffset(PDbeta33,1,0,0) - 
      GFOffset(PDbeta33,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta132 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta13,0,-2,0) - 
      8*GFOffset(PDbeta13,0,-1,0) + 8*GFOffset(PDbeta13,0,1,0) - 
      GFOffset(PDbeta13,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta232 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta23,0,-2,0) - 
      8*GFOffset(PDbeta23,0,-1,0) + 8*GFOffset(PDbeta23,0,1,0) - 
      GFOffset(PDbeta23,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta33,0,-2,0) - 
      8*GFOffset(PDbeta33,0,-1,0) + 8*GFOffset(PDbeta33,0,1,0) - 
      GFOffset(PDbeta33,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta133 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta13,0,0,-2) - 
      8*GFOffset(PDbeta13,0,0,-1) + 8*GFOffset(PDbeta13,0,0,1) - 
      GFOffset(PDbeta13,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta233 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta23,0,0,-2) - 
      8*GFOffset(PDbeta23,0,0,-1) + 8*GFOffset(PDbeta23,0,0,1) - 
      GFOffset(PDbeta23,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta33,0,0,-2) - 
      8*GFOffset(PDbeta33,0,0,-1) + 8*GFOffset(PDbeta33,0,0,1) - 
      GFOffset(PDbeta33,0,0,2))*pow(dz,-1);
    
    CCTK_REAL PDPDalpha11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta111 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta112 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta113 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta121 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta122 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta123 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta131 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta132 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta133 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta211 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta212 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta213 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta333 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDPDalpha11 = J11L*LDPDalpha11 + J21L*LDPDalpha12 + J31L*LDPDalpha13;
      
      PDPDalpha12 = J12L*LDPDalpha11 + J22L*LDPDalpha12 + J32L*LDPDalpha13;
      
      PDPDalpha13 = J13L*LDPDalpha11 + J23L*LDPDalpha12 + J33L*LDPDalpha13;
      
      PDPDalpha21 = J11L*LDPDalpha21 + J21L*LDPDalpha22 + J31L*LDPDalpha23;
      
      PDPDalpha22 = J12L*LDPDalpha21 + J22L*LDPDalpha22 + J32L*LDPDalpha23;
      
      PDPDalpha23 = J13L*LDPDalpha21 + J23L*LDPDalpha22 + J33L*LDPDalpha23;
      
      PDPDalpha31 = J11L*LDPDalpha31 + J21L*LDPDalpha32 + J31L*LDPDalpha33;
      
      PDPDalpha32 = J12L*LDPDalpha31 + J22L*LDPDalpha32 + J32L*LDPDalpha33;
      
      PDPDalpha33 = J13L*LDPDalpha31 + J23L*LDPDalpha32 + J33L*LDPDalpha33;
      
      PDPDbeta111 = J11L*LDPDbeta111 + J21L*LDPDbeta112 + J31L*LDPDbeta113;
      
      PDPDbeta211 = J11L*LDPDbeta211 + J21L*LDPDbeta212 + J31L*LDPDbeta213;
      
      PDPDbeta311 = J11L*LDPDbeta311 + J21L*LDPDbeta312 + J31L*LDPDbeta313;
      
      PDPDbeta112 = J12L*LDPDbeta111 + J22L*LDPDbeta112 + J32L*LDPDbeta113;
      
      PDPDbeta212 = J12L*LDPDbeta211 + J22L*LDPDbeta212 + J32L*LDPDbeta213;
      
      PDPDbeta312 = J12L*LDPDbeta311 + J22L*LDPDbeta312 + J32L*LDPDbeta313;
      
      PDPDbeta113 = J13L*LDPDbeta111 + J23L*LDPDbeta112 + J33L*LDPDbeta113;
      
      PDPDbeta213 = J13L*LDPDbeta211 + J23L*LDPDbeta212 + J33L*LDPDbeta213;
      
      PDPDbeta313 = J13L*LDPDbeta311 + J23L*LDPDbeta312 + J33L*LDPDbeta313;
      
      PDPDbeta121 = J11L*LDPDbeta121 + J21L*LDPDbeta122 + J31L*LDPDbeta123;
      
      PDPDbeta221 = J11L*LDPDbeta221 + J21L*LDPDbeta222 + J31L*LDPDbeta223;
      
      PDPDbeta321 = J11L*LDPDbeta321 + J21L*LDPDbeta322 + J31L*LDPDbeta323;
      
      PDPDbeta122 = J12L*LDPDbeta121 + J22L*LDPDbeta122 + J32L*LDPDbeta123;
      
      PDPDbeta222 = J12L*LDPDbeta221 + J22L*LDPDbeta222 + J32L*LDPDbeta223;
      
      PDPDbeta322 = J12L*LDPDbeta321 + J22L*LDPDbeta322 + J32L*LDPDbeta323;
      
      PDPDbeta123 = J13L*LDPDbeta121 + J23L*LDPDbeta122 + J33L*LDPDbeta123;
      
      PDPDbeta223 = J13L*LDPDbeta221 + J23L*LDPDbeta222 + J33L*LDPDbeta223;
      
      PDPDbeta323 = J13L*LDPDbeta321 + J23L*LDPDbeta322 + J33L*LDPDbeta323;
      
      PDPDbeta131 = J11L*LDPDbeta131 + J21L*LDPDbeta132 + J31L*LDPDbeta133;
      
      PDPDbeta231 = J11L*LDPDbeta231 + J21L*LDPDbeta232 + J31L*LDPDbeta233;
      
      PDPDbeta331 = J11L*LDPDbeta331 + J21L*LDPDbeta332 + J31L*LDPDbeta333;
      
      PDPDbeta132 = J12L*LDPDbeta131 + J22L*LDPDbeta132 + J32L*LDPDbeta133;
      
      PDPDbeta232 = J12L*LDPDbeta231 + J22L*LDPDbeta232 + J32L*LDPDbeta233;
      
      PDPDbeta332 = J12L*LDPDbeta331 + J22L*LDPDbeta332 + J32L*LDPDbeta333;
      
      PDPDbeta133 = J13L*LDPDbeta131 + J23L*LDPDbeta132 + J33L*LDPDbeta133;
      
      PDPDbeta233 = J13L*LDPDbeta231 + J23L*LDPDbeta232 + J33L*LDPDbeta233;
      
      PDPDbeta333 = J13L*LDPDbeta331 + J23L*LDPDbeta332 + J33L*LDPDbeta333;
    }
    else
    {
      PDPDalpha11 = LDPDalpha11;
      
      PDPDalpha12 = LDPDalpha12;
      
      PDPDalpha13 = LDPDalpha13;
      
      PDPDalpha21 = LDPDalpha21;
      
      PDPDalpha22 = LDPDalpha22;
      
      PDPDalpha23 = LDPDalpha23;
      
      PDPDalpha31 = LDPDalpha31;
      
      PDPDalpha32 = LDPDalpha32;
      
      PDPDalpha33 = LDPDalpha33;
      
      PDPDbeta111 = LDPDbeta111;
      
      PDPDbeta211 = LDPDbeta211;
      
      PDPDbeta311 = LDPDbeta311;
      
      PDPDbeta112 = LDPDbeta112;
      
      PDPDbeta212 = LDPDbeta212;
      
      PDPDbeta312 = LDPDbeta312;
      
      PDPDbeta113 = LDPDbeta113;
      
      PDPDbeta213 = LDPDbeta213;
      
      PDPDbeta313 = LDPDbeta313;
      
      PDPDbeta121 = LDPDbeta121;
      
      PDPDbeta221 = LDPDbeta221;
      
      PDPDbeta321 = LDPDbeta321;
      
      PDPDbeta122 = LDPDbeta122;
      
      PDPDbeta222 = LDPDbeta222;
      
      PDPDbeta322 = LDPDbeta322;
      
      PDPDbeta123 = LDPDbeta123;
      
      PDPDbeta223 = LDPDbeta223;
      
      PDPDbeta323 = LDPDbeta323;
      
      PDPDbeta131 = LDPDbeta131;
      
      PDPDbeta231 = LDPDbeta231;
      
      PDPDbeta331 = LDPDbeta331;
      
      PDPDbeta132 = LDPDbeta132;
      
      PDPDbeta232 = LDPDbeta232;
      
      PDPDbeta332 = LDPDbeta332;
      
      PDPDbeta133 = LDPDbeta133;
      
      PDPDbeta233 = LDPDbeta233;
      
      PDPDbeta333 = LDPDbeta333;
    }
    
    CCTK_REAL LDuXt11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,-2,0,0) - 
      8*GFOffset(Xt1,-1,0,0) + 8*GFOffset(Xt1,1,0,0) - 
      GFOffset(Xt1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuXt21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,-2,0,0) - 
      8*GFOffset(Xt2,-1,0,0) + 8*GFOffset(Xt2,1,0,0) - 
      GFOffset(Xt2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuXt31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,-2,0,0) - 
      8*GFOffset(Xt3,-1,0,0) + 8*GFOffset(Xt3,1,0,0) - 
      GFOffset(Xt3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuXt12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,0,-2,0) - 
      8*GFOffset(Xt1,0,-1,0) + 8*GFOffset(Xt1,0,1,0) - 
      GFOffset(Xt1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuXt22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,0,-2,0) - 
      8*GFOffset(Xt2,0,-1,0) + 8*GFOffset(Xt2,0,1,0) - 
      GFOffset(Xt2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuXt32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,0,-2,0) - 
      8*GFOffset(Xt3,0,-1,0) + 8*GFOffset(Xt3,0,1,0) - 
      GFOffset(Xt3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuXt13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,0,0,-2) - 
      8*GFOffset(Xt1,0,0,-1) + 8*GFOffset(Xt1,0,0,1) - 
      GFOffset(Xt1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuXt23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,0,0,-2) - 
      8*GFOffset(Xt2,0,0,-1) + 8*GFOffset(Xt2,0,0,1) - 
      GFOffset(Xt2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuXt33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,0,0,-2) - 
      8*GFOffset(Xt3,0,0,-1) + 8*GFOffset(Xt3,0,0,1) - 
      GFOffset(Xt3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDutrK1 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,-2,0,0) - 
      8*GFOffset(trK,-1,0,0) + 8*GFOffset(trK,1,0,0) - 
      GFOffset(trK,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDutrK2 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,0,-2,0) - 
      8*GFOffset(trK,0,-1,0) + 8*GFOffset(trK,0,1,0) - 
      GFOffset(trK,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDutrK3 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,0,0,-2) - 
      8*GFOffset(trK,0,0,-1) + 8*GFOffset(trK,0,0,1) - 
      GFOffset(trK,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDubeta11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta1,-2,0,0) - 
      8*GFOffset(beta1,-1,0,0) + 8*GFOffset(beta1,1,0,0) - 
      GFOffset(beta1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDubeta21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta2,-2,0,0) - 
      8*GFOffset(beta2,-1,0,0) + 8*GFOffset(beta2,1,0,0) - 
      GFOffset(beta2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDubeta31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta3,-2,0,0) - 
      8*GFOffset(beta3,-1,0,0) + 8*GFOffset(beta3,1,0,0) - 
      GFOffset(beta3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDubeta12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta1,0,-2,0) - 
      8*GFOffset(beta1,0,-1,0) + 8*GFOffset(beta1,0,1,0) - 
      GFOffset(beta1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDubeta22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta2,0,-2,0) - 
      8*GFOffset(beta2,0,-1,0) + 8*GFOffset(beta2,0,1,0) - 
      GFOffset(beta2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDubeta32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta3,0,-2,0) - 
      8*GFOffset(beta3,0,-1,0) + 8*GFOffset(beta3,0,1,0) - 
      GFOffset(beta3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDubeta13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta1,0,0,-2) - 
      8*GFOffset(beta1,0,0,-1) + 8*GFOffset(beta1,0,0,1) - 
      GFOffset(beta1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDubeta23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta2,0,0,-2) - 
      8*GFOffset(beta2,0,0,-1) + 8*GFOffset(beta2,0,0,1) - 
      GFOffset(beta2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDubeta33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta3,0,0,-2) - 
      8*GFOffset(beta3,0,0,-1) + 8*GFOffset(beta3,0,0,1) - 
      GFOffset(beta3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuB11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B1,-2,0,0) - 
      8*GFOffset(B1,-1,0,0) + 8*GFOffset(B1,1,0,0) - 
      GFOffset(B1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuB21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B2,-2,0,0) - 
      8*GFOffset(B2,-1,0,0) + 8*GFOffset(B2,1,0,0) - 
      GFOffset(B2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuB31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B3,-2,0,0) - 
      8*GFOffset(B3,-1,0,0) + 8*GFOffset(B3,1,0,0) - 
      GFOffset(B3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuB12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B1,0,-2,0) - 
      8*GFOffset(B1,0,-1,0) + 8*GFOffset(B1,0,1,0) - 
      GFOffset(B1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuB22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B2,0,-2,0) - 
      8*GFOffset(B2,0,-1,0) + 8*GFOffset(B2,0,1,0) - 
      GFOffset(B2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuB32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B3,0,-2,0) - 
      8*GFOffset(B3,0,-1,0) + 8*GFOffset(B3,0,1,0) - 
      GFOffset(B3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuB13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B1,0,0,-2) - 
      8*GFOffset(B1,0,0,-1) + 8*GFOffset(B1,0,0,1) - 
      GFOffset(B1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuB23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B2,0,0,-2) - 
      8*GFOffset(B2,0,0,-1) + 8*GFOffset(B2,0,0,1) - 
      GFOffset(B2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuB33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B3,0,0,-2) - 
      8*GFOffset(B3,0,0,-1) + 8*GFOffset(B3,0,0,1) - 
      GFOffset(B3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL PDuB11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDutrK1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDutrK2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDutrK3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDuXt11 = J11L*LDuXt11 + J21L*LDuXt12 + J31L*LDuXt13;
      
      PDuXt21 = J11L*LDuXt21 + J21L*LDuXt22 + J31L*LDuXt23;
      
      PDuXt31 = J11L*LDuXt31 + J21L*LDuXt32 + J31L*LDuXt33;
      
      PDuXt12 = J12L*LDuXt11 + J22L*LDuXt12 + J32L*LDuXt13;
      
      PDuXt22 = J12L*LDuXt21 + J22L*LDuXt22 + J32L*LDuXt23;
      
      PDuXt32 = J12L*LDuXt31 + J22L*LDuXt32 + J32L*LDuXt33;
      
      PDuXt13 = J13L*LDuXt11 + J23L*LDuXt12 + J33L*LDuXt13;
      
      PDuXt23 = J13L*LDuXt21 + J23L*LDuXt22 + J33L*LDuXt23;
      
      PDuXt33 = J13L*LDuXt31 + J23L*LDuXt32 + J33L*LDuXt33;
      
      PDutrK1 = J11L*LDutrK1 + J21L*LDutrK2 + J31L*LDutrK3;
      
      PDutrK2 = J12L*LDutrK1 + J22L*LDutrK2 + J32L*LDutrK3;
      
      PDutrK3 = J13L*LDutrK1 + J23L*LDutrK2 + J33L*LDutrK3;
      
      PDubeta11 = J11L*LDubeta11 + J21L*LDubeta12 + J31L*LDubeta13;
      
      PDubeta21 = J11L*LDubeta21 + J21L*LDubeta22 + J31L*LDubeta23;
      
      PDubeta31 = J11L*LDubeta31 + J21L*LDubeta32 + J31L*LDubeta33;
      
      PDubeta12 = J12L*LDubeta11 + J22L*LDubeta12 + J32L*LDubeta13;
      
      PDubeta22 = J12L*LDubeta21 + J22L*LDubeta22 + J32L*LDubeta23;
      
      PDubeta32 = J12L*LDubeta31 + J22L*LDubeta32 + J32L*LDubeta33;
      
      PDubeta13 = J13L*LDubeta11 + J23L*LDubeta12 + J33L*LDubeta13;
      
      PDubeta23 = J13L*LDubeta21 + J23L*LDubeta22 + J33L*LDubeta23;
      
      PDubeta33 = J13L*LDubeta31 + J23L*LDubeta32 + J33L*LDubeta33;
      
      PDuB11 = J11L*LDuB11 + J21L*LDuB12 + J31L*LDuB13;
      
      PDuB21 = J11L*LDuB21 + J21L*LDuB22 + J31L*LDuB23;
      
      PDuB31 = J11L*LDuB31 + J21L*LDuB32 + J31L*LDuB33;
      
      PDuB12 = J12L*LDuB11 + J22L*LDuB12 + J32L*LDuB13;
      
      PDuB22 = J12L*LDuB21 + J22L*LDuB22 + J32L*LDuB23;
      
      PDuB32 = J12L*LDuB31 + J22L*LDuB32 + J32L*LDuB33;
      
      PDuB13 = J13L*LDuB11 + J23L*LDuB12 + J33L*LDuB13;
      
      PDuB23 = J13L*LDuB21 + J23L*LDuB22 + J33L*LDuB23;
      
      PDuB33 = J13L*LDuB31 + J23L*LDuB32 + J33L*LDuB33;
    }
    else
    {
      PDuXt11 = LDuXt11;
      
      PDuXt21 = LDuXt21;
      
      PDuXt31 = LDuXt31;
      
      PDuXt12 = LDuXt12;
      
      PDuXt22 = LDuXt22;
      
      PDuXt32 = LDuXt32;
      
      PDuXt13 = LDuXt13;
      
      PDuXt23 = LDuXt23;
      
      PDuXt33 = LDuXt33;
      
      PDutrK1 = LDutrK1;
      
      PDutrK2 = LDutrK2;
      
      PDutrK3 = LDutrK3;
      
      PDubeta11 = LDubeta11;
      
      PDubeta21 = LDubeta21;
      
      PDubeta31 = LDubeta31;
      
      PDubeta12 = LDubeta12;
      
      PDubeta22 = LDubeta22;
      
      PDubeta32 = LDubeta32;
      
      PDubeta13 = LDubeta13;
      
      PDubeta23 = LDubeta23;
      
      PDubeta33 = LDubeta33;
      
      PDuB11 = LDuB11;
      
      PDuB21 = LDuB21;
      
      PDuB31 = LDuB31;
      
      PDuB12 = LDuB12;
      
      PDuB22 = LDuB22;
      
      PDuB32 = LDuB32;
      
      PDuB13 = LDuB13;
      
      PDuB23 = LDuB23;
      
      PDuB33 = LDuB33;
    }
    
    CCTK_REAL LDissXt1 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(Xt1,-3,0,0) - 6*GFOffset(Xt1,-2,0,0) + 
      15*GFOffset(Xt1,-1,0,0) - 20*GFOffset(Xt1,0,0,0) + 
      15*GFOffset(Xt1,1,0,0) - 6*GFOffset(Xt1,2,0,0) + 
      GFOffset(Xt1,3,0,0))*pow(dx,-1) + (GFOffset(Xt1,0,-3,0) - 
      6*GFOffset(Xt1,0,-2,0) + 15*GFOffset(Xt1,0,-1,0) - 
      20*GFOffset(Xt1,0,0,0) + 15*GFOffset(Xt1,0,1,0) - 6*GFOffset(Xt1,0,2,0) 
      + GFOffset(Xt1,0,3,0))*pow(dy,-1) + (GFOffset(Xt1,0,0,-3) - 
      6*GFOffset(Xt1,0,0,-2) + 15*GFOffset(Xt1,0,0,-1) - 
      20*GFOffset(Xt1,0,0,0) + 15*GFOffset(Xt1,0,0,1) - 6*GFOffset(Xt1,0,0,2) 
      + GFOffset(Xt1,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissXt2 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(Xt2,-3,0,0) - 6*GFOffset(Xt2,-2,0,0) + 
      15*GFOffset(Xt2,-1,0,0) - 20*GFOffset(Xt2,0,0,0) + 
      15*GFOffset(Xt2,1,0,0) - 6*GFOffset(Xt2,2,0,0) + 
      GFOffset(Xt2,3,0,0))*pow(dx,-1) + (GFOffset(Xt2,0,-3,0) - 
      6*GFOffset(Xt2,0,-2,0) + 15*GFOffset(Xt2,0,-1,0) - 
      20*GFOffset(Xt2,0,0,0) + 15*GFOffset(Xt2,0,1,0) - 6*GFOffset(Xt2,0,2,0) 
      + GFOffset(Xt2,0,3,0))*pow(dy,-1) + (GFOffset(Xt2,0,0,-3) - 
      6*GFOffset(Xt2,0,0,-2) + 15*GFOffset(Xt2,0,0,-1) - 
      20*GFOffset(Xt2,0,0,0) + 15*GFOffset(Xt2,0,0,1) - 6*GFOffset(Xt2,0,0,2) 
      + GFOffset(Xt2,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissXt3 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(Xt3,-3,0,0) - 6*GFOffset(Xt3,-2,0,0) + 
      15*GFOffset(Xt3,-1,0,0) - 20*GFOffset(Xt3,0,0,0) + 
      15*GFOffset(Xt3,1,0,0) - 6*GFOffset(Xt3,2,0,0) + 
      GFOffset(Xt3,3,0,0))*pow(dx,-1) + (GFOffset(Xt3,0,-3,0) - 
      6*GFOffset(Xt3,0,-2,0) + 15*GFOffset(Xt3,0,-1,0) - 
      20*GFOffset(Xt3,0,0,0) + 15*GFOffset(Xt3,0,1,0) - 6*GFOffset(Xt3,0,2,0) 
      + GFOffset(Xt3,0,3,0))*pow(dy,-1) + (GFOffset(Xt3,0,0,-3) - 
      6*GFOffset(Xt3,0,0,-2) + 15*GFOffset(Xt3,0,0,-1) - 
      20*GFOffset(Xt3,0,0,0) + 15*GFOffset(Xt3,0,0,1) - 6*GFOffset(Xt3,0,0,2) 
      + GFOffset(Xt3,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissXt1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissXt2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissXt3 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      GDissXt1 = myDetJL*LDissXt1;
      
      GDissXt2 = myDetJL*LDissXt2;
      
      GDissXt3 = myDetJL*LDissXt3;
    }
    else
    {
      GDissXt1 = LDissXt1;
      
      GDissXt2 = LDissXt2;
      
      GDissXt3 = LDissXt3;
    }
    
    CCTK_REAL LDisstrK CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(trK,-3,0,0) - 6*GFOffset(trK,-2,0,0) + 
      15*GFOffset(trK,-1,0,0) - 20*GFOffset(trK,0,0,0) + 
      15*GFOffset(trK,1,0,0) - 6*GFOffset(trK,2,0,0) + 
      GFOffset(trK,3,0,0))*pow(dx,-1) + (GFOffset(trK,0,-3,0) - 
      6*GFOffset(trK,0,-2,0) + 15*GFOffset(trK,0,-1,0) - 
      20*GFOffset(trK,0,0,0) + 15*GFOffset(trK,0,1,0) - 6*GFOffset(trK,0,2,0) 
      + GFOffset(trK,0,3,0))*pow(dy,-1) + (GFOffset(trK,0,0,-3) - 
      6*GFOffset(trK,0,0,-2) + 15*GFOffset(trK,0,0,-1) - 
      20*GFOffset(trK,0,0,0) + 15*GFOffset(trK,0,0,1) - 6*GFOffset(trK,0,0,2) 
      + GFOffset(trK,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDisstrK CCTK_ATTRIBUTE_UNUSED = 
      IfThen(usejacobian,myDetJL*LDisstrK,LDisstrK);
    
    CCTK_REAL LDissbeta1 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(beta1,-3,0,0) - 6*GFOffset(beta1,-2,0,0) + 
      15*GFOffset(beta1,-1,0,0) - 20*GFOffset(beta1,0,0,0) + 
      15*GFOffset(beta1,1,0,0) - 6*GFOffset(beta1,2,0,0) + 
      GFOffset(beta1,3,0,0))*pow(dx,-1) + (GFOffset(beta1,0,-3,0) - 
      6*GFOffset(beta1,0,-2,0) + 15*GFOffset(beta1,0,-1,0) - 
      20*GFOffset(beta1,0,0,0) + 15*GFOffset(beta1,0,1,0) - 
      6*GFOffset(beta1,0,2,0) + GFOffset(beta1,0,3,0))*pow(dy,-1) + 
      (GFOffset(beta1,0,0,-3) - 6*GFOffset(beta1,0,0,-2) + 
      15*GFOffset(beta1,0,0,-1) - 20*GFOffset(beta1,0,0,0) + 
      15*GFOffset(beta1,0,0,1) - 6*GFOffset(beta1,0,0,2) + 
      GFOffset(beta1,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissbeta2 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(beta2,-3,0,0) - 6*GFOffset(beta2,-2,0,0) + 
      15*GFOffset(beta2,-1,0,0) - 20*GFOffset(beta2,0,0,0) + 
      15*GFOffset(beta2,1,0,0) - 6*GFOffset(beta2,2,0,0) + 
      GFOffset(beta2,3,0,0))*pow(dx,-1) + (GFOffset(beta2,0,-3,0) - 
      6*GFOffset(beta2,0,-2,0) + 15*GFOffset(beta2,0,-1,0) - 
      20*GFOffset(beta2,0,0,0) + 15*GFOffset(beta2,0,1,0) - 
      6*GFOffset(beta2,0,2,0) + GFOffset(beta2,0,3,0))*pow(dy,-1) + 
      (GFOffset(beta2,0,0,-3) - 6*GFOffset(beta2,0,0,-2) + 
      15*GFOffset(beta2,0,0,-1) - 20*GFOffset(beta2,0,0,0) + 
      15*GFOffset(beta2,0,0,1) - 6*GFOffset(beta2,0,0,2) + 
      GFOffset(beta2,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissbeta3 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(beta3,-3,0,0) - 6*GFOffset(beta3,-2,0,0) + 
      15*GFOffset(beta3,-1,0,0) - 20*GFOffset(beta3,0,0,0) + 
      15*GFOffset(beta3,1,0,0) - 6*GFOffset(beta3,2,0,0) + 
      GFOffset(beta3,3,0,0))*pow(dx,-1) + (GFOffset(beta3,0,-3,0) - 
      6*GFOffset(beta3,0,-2,0) + 15*GFOffset(beta3,0,-1,0) - 
      20*GFOffset(beta3,0,0,0) + 15*GFOffset(beta3,0,1,0) - 
      6*GFOffset(beta3,0,2,0) + GFOffset(beta3,0,3,0))*pow(dy,-1) + 
      (GFOffset(beta3,0,0,-3) - 6*GFOffset(beta3,0,0,-2) + 
      15*GFOffset(beta3,0,0,-1) - 20*GFOffset(beta3,0,0,0) + 
      15*GFOffset(beta3,0,0,1) - 6*GFOffset(beta3,0,0,2) + 
      GFOffset(beta3,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissbeta1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissbeta2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissbeta3 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      GDissbeta1 = myDetJL*LDissbeta1;
      
      GDissbeta2 = myDetJL*LDissbeta2;
      
      GDissbeta3 = myDetJL*LDissbeta3;
    }
    else
    {
      GDissbeta1 = LDissbeta1;
      
      GDissbeta2 = LDissbeta2;
      
      GDissbeta3 = LDissbeta3;
    }
    
    CCTK_REAL LDissB1 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(B1,-3,0,0) - 6*GFOffset(B1,-2,0,0) + 
      15*GFOffset(B1,-1,0,0) - 20*GFOffset(B1,0,0,0) + 15*GFOffset(B1,1,0,0) 
      - 6*GFOffset(B1,2,0,0) + GFOffset(B1,3,0,0))*pow(dx,-1) + 
      (GFOffset(B1,0,-3,0) - 6*GFOffset(B1,0,-2,0) + 15*GFOffset(B1,0,-1,0) - 
      20*GFOffset(B1,0,0,0) + 15*GFOffset(B1,0,1,0) - 6*GFOffset(B1,0,2,0) + 
      GFOffset(B1,0,3,0))*pow(dy,-1) + (GFOffset(B1,0,0,-3) - 
      6*GFOffset(B1,0,0,-2) + 15*GFOffset(B1,0,0,-1) - 20*GFOffset(B1,0,0,0) 
      + 15*GFOffset(B1,0,0,1) - 6*GFOffset(B1,0,0,2) + 
      GFOffset(B1,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissB2 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(B2,-3,0,0) - 6*GFOffset(B2,-2,0,0) + 
      15*GFOffset(B2,-1,0,0) - 20*GFOffset(B2,0,0,0) + 15*GFOffset(B2,1,0,0) 
      - 6*GFOffset(B2,2,0,0) + GFOffset(B2,3,0,0))*pow(dx,-1) + 
      (GFOffset(B2,0,-3,0) - 6*GFOffset(B2,0,-2,0) + 15*GFOffset(B2,0,-1,0) - 
      20*GFOffset(B2,0,0,0) + 15*GFOffset(B2,0,1,0) - 6*GFOffset(B2,0,2,0) + 
      GFOffset(B2,0,3,0))*pow(dy,-1) + (GFOffset(B2,0,0,-3) - 
      6*GFOffset(B2,0,0,-2) + 15*GFOffset(B2,0,0,-1) - 20*GFOffset(B2,0,0,0) 
      + 15*GFOffset(B2,0,0,1) - 6*GFOffset(B2,0,0,2) + 
      GFOffset(B2,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissB3 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(B3,-3,0,0) - 6*GFOffset(B3,-2,0,0) + 
      15*GFOffset(B3,-1,0,0) - 20*GFOffset(B3,0,0,0) + 15*GFOffset(B3,1,0,0) 
      - 6*GFOffset(B3,2,0,0) + GFOffset(B3,3,0,0))*pow(dx,-1) + 
      (GFOffset(B3,0,-3,0) - 6*GFOffset(B3,0,-2,0) + 15*GFOffset(B3,0,-1,0) - 
      20*GFOffset(B3,0,0,0) + 15*GFOffset(B3,0,1,0) - 6*GFOffset(B3,0,2,0) + 
      GFOffset(B3,0,3,0))*pow(dy,-1) + (GFOffset(B3,0,0,-3) - 
      6*GFOffset(B3,0,0,-2) + 15*GFOffset(B3,0,0,-1) - 20*GFOffset(B3,0,0,0) 
      + 15*GFOffset(B3,0,0,1) - 6*GFOffset(B3,0,0,2) + 
      GFOffset(B3,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissB1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissB2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissB3 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      GDissB1 = myDetJL*LDissB1;
      
      GDissB2 = myDetJL*LDissB2;
      
      GDissB3 = myDetJL*LDissB3;
    }
    else
    {
      GDissB1 = LDissB1;
      
      GDissB2 = LDissB2;
      
      GDissB3 = LDissB3;
    }
    
    CCTK_REAL detgt CCTK_ATTRIBUTE_UNUSED = 1;
    
    CCTK_REAL gtu11 CCTK_ATTRIBUTE_UNUSED = (gt22L*gt33L - 
      pow(gt23L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu12 CCTK_ATTRIBUTE_UNUSED = (gt13L*gt23L - 
      gt12L*gt33L)*pow(detgt,-1);
    
    CCTK_REAL gtu13 CCTK_ATTRIBUTE_UNUSED = (-(gt13L*gt22L) + 
      gt12L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu22 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt33L - 
      pow(gt13L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu23 CCTK_ATTRIBUTE_UNUSED = (gt12L*gt13L - 
      gt11L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu33 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt22L - 
      pow(gt12L,2))*pow(detgt,-1);
    
    CCTK_REAL Gtl111 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt111L;
    
    CCTK_REAL Gtl112 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt112L;
    
    CCTK_REAL Gtl113 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt113L;
    
    CCTK_REAL Gtl122 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt122L - PDgt221L);
    
    CCTK_REAL Gtl123 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt123L + PDgt132L - 
      PDgt231L);
    
    CCTK_REAL Gtl133 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt133L - PDgt331L);
    
    CCTK_REAL Gtl211 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt112L + 2*PDgt121L);
    
    CCTK_REAL Gtl212 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt221L;
    
    CCTK_REAL Gtl213 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt123L - PDgt132L + 
      PDgt231L);
    
    CCTK_REAL Gtl222 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt222L;
    
    CCTK_REAL Gtl223 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt223L;
    
    CCTK_REAL Gtl233 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt233L - PDgt332L);
    
    CCTK_REAL Gtl311 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt113L + 2*PDgt131L);
    
    CCTK_REAL Gtl312 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt123L + PDgt132L + 
      PDgt231L);
    
    CCTK_REAL Gtl313 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt331L;
    
    CCTK_REAL Gtl322 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt223L + 2*PDgt232L);
    
    CCTK_REAL Gtl323 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt332L;
    
    CCTK_REAL Gtl333 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt333L;
    
    CCTK_REAL Gt111 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu11 + Gtl211*gtu12 + 
      Gtl311*gtu13;
    
    CCTK_REAL Gt211 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu12 + Gtl211*gtu22 + 
      Gtl311*gtu23;
    
    CCTK_REAL Gt311 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu13 + Gtl211*gtu23 + 
      Gtl311*gtu33;
    
    CCTK_REAL Gt112 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu11 + Gtl212*gtu12 + 
      Gtl312*gtu13;
    
    CCTK_REAL Gt212 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu12 + Gtl212*gtu22 + 
      Gtl312*gtu23;
    
    CCTK_REAL Gt312 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu13 + Gtl212*gtu23 + 
      Gtl312*gtu33;
    
    CCTK_REAL Gt113 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu11 + Gtl213*gtu12 + 
      Gtl313*gtu13;
    
    CCTK_REAL Gt213 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu12 + Gtl213*gtu22 + 
      Gtl313*gtu23;
    
    CCTK_REAL Gt313 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu13 + Gtl213*gtu23 + 
      Gtl313*gtu33;
    
    CCTK_REAL Gt122 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu11 + Gtl222*gtu12 + 
      Gtl322*gtu13;
    
    CCTK_REAL Gt222 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu12 + Gtl222*gtu22 + 
      Gtl322*gtu23;
    
    CCTK_REAL Gt322 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu13 + Gtl222*gtu23 + 
      Gtl322*gtu33;
    
    CCTK_REAL Gt123 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu11 + Gtl223*gtu12 + 
      Gtl323*gtu13;
    
    CCTK_REAL Gt223 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu12 + Gtl223*gtu22 + 
      Gtl323*gtu23;
    
    CCTK_REAL Gt323 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu13 + Gtl223*gtu23 + 
      Gtl323*gtu33;
    
    CCTK_REAL Gt133 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu11 + Gtl233*gtu12 + 
      Gtl333*gtu13;
    
    CCTK_REAL Gt233 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu12 + Gtl233*gtu22 + 
      Gtl333*gtu23;
    
    CCTK_REAL Gt333 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu13 + Gtl233*gtu23 + 
      Gtl333*gtu33;
    
    CCTK_REAL em4phi CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,pow(phiWL,2),exp(-4*phiWL));
    
    CCTK_REAL gu11 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu11;
    
    CCTK_REAL gu12 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu12;
    
    CCTK_REAL gu13 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu13;
    
    CCTK_REAL gu22 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu22;
    
    CCTK_REAL gu23 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu23;
    
    CCTK_REAL gu33 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu33;
    
    CCTK_REAL Xtn1 CCTK_ATTRIBUTE_UNUSED = Gt111*gtu11 + 2*Gt112*gtu12 + 
      2*Gt113*gtu13 + Gt122*gtu22 + 2*Gt123*gtu23 + Gt133*gtu33;
    
    CCTK_REAL Xtn2 CCTK_ATTRIBUTE_UNUSED = Gt211*gtu11 + 2*Gt212*gtu12 + 
      2*Gt213*gtu13 + Gt222*gtu22 + 2*Gt223*gtu23 + Gt233*gtu33;
    
    CCTK_REAL Xtn3 CCTK_ATTRIBUTE_UNUSED = Gt311*gtu11 + 2*Gt312*gtu12 + 
      2*Gt313*gtu13 + Gt322*gtu22 + 2*Gt323*gtu23 + Gt333*gtu33;
    
    CCTK_REAL fac1 CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,-0.5*pow(phiWL,-1),1);
    
    CCTK_REAL cdphi1 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*fac1;
    
    CCTK_REAL cdphi2 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*fac1;
    
    CCTK_REAL cdphi3 CCTK_ATTRIBUTE_UNUSED = PDphiW3L*fac1;
    
    CCTK_REAL Atm11 CCTK_ATTRIBUTE_UNUSED = At11L*gtu11 + At12L*gtu12 + 
      At13L*gtu13;
    
    CCTK_REAL Atm21 CCTK_ATTRIBUTE_UNUSED = At11L*gtu12 + At12L*gtu22 + 
      At13L*gtu23;
    
    CCTK_REAL Atm31 CCTK_ATTRIBUTE_UNUSED = At11L*gtu13 + At12L*gtu23 + 
      At13L*gtu33;
    
    CCTK_REAL Atm12 CCTK_ATTRIBUTE_UNUSED = At12L*gtu11 + At22L*gtu12 + 
      At23L*gtu13;
    
    CCTK_REAL Atm22 CCTK_ATTRIBUTE_UNUSED = At12L*gtu12 + At22L*gtu22 + 
      At23L*gtu23;
    
    CCTK_REAL Atm32 CCTK_ATTRIBUTE_UNUSED = At12L*gtu13 + At22L*gtu23 + 
      At23L*gtu33;
    
    CCTK_REAL Atm13 CCTK_ATTRIBUTE_UNUSED = At13L*gtu11 + At23L*gtu12 + 
      At33L*gtu13;
    
    CCTK_REAL Atm23 CCTK_ATTRIBUTE_UNUSED = At13L*gtu12 + At23L*gtu22 + 
      At33L*gtu23;
    
    CCTK_REAL Atm33 CCTK_ATTRIBUTE_UNUSED = At13L*gtu13 + At23L*gtu23 + 
      At33L*gtu33;
    
    CCTK_REAL Atu11 CCTK_ATTRIBUTE_UNUSED = Atm11*gtu11 + Atm12*gtu12 + 
      Atm13*gtu13;
    
    CCTK_REAL Atu12 CCTK_ATTRIBUTE_UNUSED = Atm11*gtu12 + Atm12*gtu22 + 
      Atm13*gtu23;
    
    CCTK_REAL Atu13 CCTK_ATTRIBUTE_UNUSED = Atm11*gtu13 + Atm12*gtu23 + 
      Atm13*gtu33;
    
    CCTK_REAL Atu22 CCTK_ATTRIBUTE_UNUSED = Atm21*gtu12 + Atm22*gtu22 + 
      Atm23*gtu23;
    
    CCTK_REAL Atu23 CCTK_ATTRIBUTE_UNUSED = Atm21*gtu13 + Atm22*gtu23 + 
      Atm23*gtu33;
    
    CCTK_REAL Atu33 CCTK_ATTRIBUTE_UNUSED = Atm31*gtu13 + Atm32*gtu23 + 
      Atm33*gtu33;
    
    CCTK_REAL rho CCTK_ATTRIBUTE_UNUSED = (eTttL - 2*(beta1L*eTtxL + 
      beta2L*eTtyL + beta3L*eTtzL) + beta1L*(beta1L*eTxxL + beta2L*eTxyL + 
      beta3L*eTxzL) + beta2L*(beta1L*eTxyL + beta2L*eTyyL + beta3L*eTyzL) + 
      beta3L*(beta1L*eTxzL + beta2L*eTyzL + beta3L*eTzzL))*pow(alphaL,-2);
    
    CCTK_REAL S1 CCTK_ATTRIBUTE_UNUSED = -((eTtxL - beta1L*eTxxL - 
      beta2L*eTxyL - beta3L*eTxzL)*pow(alphaL,-1));
    
    CCTK_REAL S2 CCTK_ATTRIBUTE_UNUSED = -((eTtyL - beta1L*eTxyL - 
      beta2L*eTyyL - beta3L*eTyzL)*pow(alphaL,-1));
    
    CCTK_REAL S3 CCTK_ATTRIBUTE_UNUSED = -((eTtzL - beta1L*eTxzL - 
      beta2L*eTyzL - beta3L*eTzzL)*pow(alphaL,-1));
    
    CCTK_REAL trS CCTK_ATTRIBUTE_UNUSED = eTxxL*gu11 + 2*eTxyL*gu12 + 
      2*eTxzL*gu13 + eTyyL*gu22 + 2*eTyzL*gu23 + eTzzL*gu33;
    
    CCTK_REAL dotXt1 CCTK_ATTRIBUTE_UNUSED = -2*(PDalpha1L*Atu11 + 
      PDalpha2L*Atu12 + PDalpha3L*Atu13) + gtu11*PDPDbeta111 + 
      gtu12*PDPDbeta112 + gtu13*PDPDbeta113 + gtu12*PDPDbeta121 + 
      gtu22*PDPDbeta122 + gtu23*PDPDbeta123 + gtu13*PDPDbeta131 + 
      gtu23*PDPDbeta132 + gtu33*PDPDbeta133 + 
      0.333333333333333333333333333333*(gtu11*(PDPDbeta111 + PDPDbeta212 + 
      PDPDbeta313) + gtu12*(PDPDbeta121 + PDPDbeta222 + PDPDbeta323) + 
      gtu13*(PDPDbeta131 + PDPDbeta232 + PDPDbeta333)) + 
      2*alphaL*(6*(Atu11*cdphi1 + Atu12*cdphi2 + Atu13*cdphi3) + Atu11*Gt111 
      + 2*Atu12*Gt112 + 2*Atu13*Gt113 + Atu22*Gt122 + 2*Atu23*Gt123 + 
      Atu33*Gt133 - 0.666666666666666666666666666667*(gtu11*PDtrK1 + 
      gtu12*PDtrK2 + gtu13*PDtrK3)) - 16*alphaL*Pi*(gtu11*S1 + gtu12*S2 + 
      gtu13*S3) - PDbeta11L*Xtn1 + 
      0.666666666666666666666666666667*(PDbeta11L + PDbeta22L + 
      PDbeta33L)*Xtn1 - PDbeta12L*Xtn2 - PDbeta13L*Xtn3;
    
    CCTK_REAL dotXt2 CCTK_ATTRIBUTE_UNUSED = -2*(PDalpha1L*Atu12 + 
      PDalpha2L*Atu22 + PDalpha3L*Atu23) + gtu11*PDPDbeta211 + 
      gtu12*PDPDbeta212 + gtu13*PDPDbeta213 + gtu12*PDPDbeta221 + 
      gtu22*PDPDbeta222 + gtu23*PDPDbeta223 + gtu13*PDPDbeta231 + 
      gtu23*PDPDbeta232 + gtu33*PDPDbeta233 + 
      0.333333333333333333333333333333*(gtu12*(PDPDbeta111 + PDPDbeta212 + 
      PDPDbeta313) + gtu22*(PDPDbeta121 + PDPDbeta222 + PDPDbeta323) + 
      gtu23*(PDPDbeta131 + PDPDbeta232 + PDPDbeta333)) + 
      2*alphaL*(6*(Atu12*cdphi1 + Atu22*cdphi2 + Atu23*cdphi3) + Atu11*Gt211 
      + 2*Atu12*Gt212 + 2*Atu13*Gt213 + Atu22*Gt222 + 2*Atu23*Gt223 + 
      Atu33*Gt233 - 0.666666666666666666666666666667*(gtu12*PDtrK1 + 
      gtu22*PDtrK2 + gtu23*PDtrK3)) - 16*alphaL*Pi*(gtu12*S1 + gtu22*S2 + 
      gtu23*S3) - PDbeta21L*Xtn1 - PDbeta22L*Xtn2 + 
      0.666666666666666666666666666667*(PDbeta11L + PDbeta22L + 
      PDbeta33L)*Xtn2 - PDbeta23L*Xtn3;
    
    CCTK_REAL dotXt3 CCTK_ATTRIBUTE_UNUSED = -2*(PDalpha1L*Atu13 + 
      PDalpha2L*Atu23 + PDalpha3L*Atu33) + gtu11*PDPDbeta311 + 
      gtu12*PDPDbeta312 + gtu13*PDPDbeta313 + gtu12*PDPDbeta321 + 
      gtu22*PDPDbeta322 + gtu23*PDPDbeta323 + gtu13*PDPDbeta331 + 
      gtu23*PDPDbeta332 + gtu33*PDPDbeta333 + 
      0.333333333333333333333333333333*(gtu13*(PDPDbeta111 + PDPDbeta212 + 
      PDPDbeta313) + gtu23*(PDPDbeta121 + PDPDbeta222 + PDPDbeta323) + 
      gtu33*(PDPDbeta131 + PDPDbeta232 + PDPDbeta333)) + 
      2*alphaL*(6*(Atu13*cdphi1 + Atu23*cdphi2 + Atu33*cdphi3) + Atu11*Gt311 
      + 2*Atu12*Gt312 + 2*Atu13*Gt313 + Atu22*Gt322 + 2*Atu23*Gt323 + 
      Atu33*Gt333 - 0.666666666666666666666666666667*(gtu13*PDtrK1 + 
      gtu23*PDtrK2 + gtu33*PDtrK3)) - 16*alphaL*Pi*(gtu13*S1 + gtu23*S2 + 
      gtu33*S3) - PDbeta31L*Xtn1 - PDbeta32L*Xtn2 - PDbeta33L*Xtn3 + 
      0.666666666666666666666666666667*(PDbeta11L + PDbeta22L + 
      PDbeta33L)*Xtn3;
    
    CCTK_REAL Xt1rhsL CCTK_ATTRIBUTE_UNUSED = dotXt1 + GDissXt1 + 
      beta1L*PDuXt11 + beta2L*PDuXt12 + beta3L*PDuXt13;
    
    CCTK_REAL Xt2rhsL CCTK_ATTRIBUTE_UNUSED = dotXt2 + GDissXt2 + 
      beta1L*PDuXt21 + beta2L*PDuXt22 + beta3L*PDuXt23;
    
    CCTK_REAL Xt3rhsL CCTK_ATTRIBUTE_UNUSED = dotXt3 + GDissXt3 + 
      beta1L*PDuXt31 + beta2L*PDuXt32 + beta3L*PDuXt33;
    
    CCTK_REAL dottrK CCTK_ATTRIBUTE_UNUSED = 4*alphaL*Pi*(rho + trS) - 
      em4phi*(gtu11*(2*PDalpha1L*cdphi1 + PDPDalpha11) + 
      gtu12*(2*PDalpha2L*cdphi1 + PDPDalpha12) + gtu13*(2*PDalpha3L*cdphi1 + 
      PDPDalpha13) + gtu12*(2*PDalpha1L*cdphi2 + PDPDalpha21) + 
      gtu22*(2*PDalpha2L*cdphi2 + PDPDalpha22) + gtu23*(2*PDalpha3L*cdphi2 + 
      PDPDalpha23) + gtu13*(2*PDalpha1L*cdphi3 + PDPDalpha31) + 
      gtu23*(2*PDalpha2L*cdphi3 + PDPDalpha32) + gtu33*(2*PDalpha3L*cdphi3 + 
      PDPDalpha33) - PDalpha1L*Xtn1 - PDalpha2L*Xtn2 - PDalpha3L*Xtn3) + 
      alphaL*(2*Atm12*Atm21 + 2*Atm13*Atm31 + 2*Atm23*Atm32 + 
      0.333333333333333333333333333333*pow(trKL,2) + pow(Atm11,2) + 
      pow(Atm22,2) + pow(Atm33,2));
    
    CCTK_REAL trKrhsL CCTK_ATTRIBUTE_UNUSED = dottrK + GDisstrK + 
      beta1L*PDutrK1 + beta2L*PDutrK2 + beta3L*PDutrK3;
    
    CCTK_REAL betaDriverValue CCTK_ATTRIBUTE_UNUSED = 
      IfThen(useSpatialBetaDriver != 
      0,betaDriver*spatialBetaDriverRadius*pow(fmax(rL,spatialBetaDriverRadius),-1),betaDriver);
    
    CCTK_REAL shiftGammaCoeffValue CCTK_ATTRIBUTE_UNUSED = 
      IfThen(useSpatialShiftGammaCoeff != 0,shiftGammaCoeff*fmin(1,exp(1 - 
      rL*pow(spatialShiftGammaCoeffRadius,-1))),shiftGammaCoeff);
    
    CCTK_REAL ddetgt1 CCTK_ATTRIBUTE_UNUSED = PDgt111L*gtu11 + 
      2*PDgt121L*gtu12 + 2*PDgt131L*gtu13 + PDgt221L*gtu22 + 2*PDgt231L*gtu23 
      + PDgt331L*gtu33;
    
    CCTK_REAL ddetgt2 CCTK_ATTRIBUTE_UNUSED = PDgt112L*gtu11 + 
      2*PDgt122L*gtu12 + 2*PDgt132L*gtu13 + PDgt222L*gtu22 + 2*PDgt232L*gtu23 
      + PDgt332L*gtu33;
    
    CCTK_REAL ddetgt3 CCTK_ATTRIBUTE_UNUSED = PDgt113L*gtu11 + 
      2*PDgt123L*gtu12 + 2*PDgt133L*gtu13 + PDgt223L*gtu22 + 2*PDgt233L*gtu23 
      + PDgt333L*gtu33;
    
    CCTK_REAL dotbeta1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dotbeta2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dotbeta3 CCTK_ATTRIBUTE_UNUSED;
    
    if (shiftFormulation == 0)
    {
      dotbeta1 = shiftGammaCoeffValue*IfThen(evolveB != 0,B1L,Xt1L - 
        beta1L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
      
      dotbeta2 = shiftGammaCoeffValue*IfThen(evolveB != 0,B2L,Xt2L - 
        beta2L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
      
      dotbeta3 = shiftGammaCoeffValue*IfThen(evolveB != 0,B3L,Xt3L - 
        beta3L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
    }
    else
    {
      dotbeta1 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu11 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu12 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu13));
      
      dotbeta2 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu12 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu22 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu23));
      
      dotbeta3 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu13 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu23 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu33));
    }
    
    CCTK_REAL beta1rhsL CCTK_ATTRIBUTE_UNUSED = dotbeta1 + GDissbeta1 + 
      IfThen(advectShift != 0,beta1L*PDubeta11 + beta2L*PDubeta12 + 
      beta3L*PDubeta13,0);
    
    CCTK_REAL beta2rhsL CCTK_ATTRIBUTE_UNUSED = dotbeta2 + GDissbeta2 + 
      IfThen(advectShift != 0,beta1L*PDubeta21 + beta2L*PDubeta22 + 
      beta3L*PDubeta23,0);
    
    CCTK_REAL beta3rhsL CCTK_ATTRIBUTE_UNUSED = dotbeta3 + GDissbeta3 + 
      IfThen(advectShift != 0,beta1L*PDubeta31 + beta2L*PDubeta32 + 
      beta3L*PDubeta33,0);
    
    CCTK_REAL B1rhsL CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL B2rhsL CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL B3rhsL CCTK_ATTRIBUTE_UNUSED;
    
    if (evolveB != 0)
    {
      B1rhsL = dotXt1 + GDissB1 + IfThen(fixAdvectionTerms == 0 && 
        advectShift != 0,beta1L*PDuB11 + beta2L*PDuB12 + beta3L*PDuB13,0) - 
        betaDriverValue*(B1L + IfThen(fixAdvectionTerms != 0 && advectShift != 
        0 && shiftGammaCoeffValue != 0.,(beta1L*PDubeta11 + beta2L*PDubeta12 + 
        beta3L*PDubeta13)*pow(alphaL,-shiftAlphaPower)*pow(shiftGammaCoeffValue,-1),0)) 
        + IfThen(fixAdvectionTerms != 0,beta1L*PDuXt11 + beta2L*PDuXt12 + 
        beta3L*PDuXt13,0);
      
      B2rhsL = dotXt2 + GDissB2 + IfThen(fixAdvectionTerms == 0 && 
        advectShift != 0,beta1L*PDuB21 + beta2L*PDuB22 + beta3L*PDuB23,0) - 
        betaDriverValue*(B2L + IfThen(fixAdvectionTerms != 0 && advectShift != 
        0 && shiftGammaCoeffValue != 0.,(beta1L*PDubeta21 + beta2L*PDubeta22 + 
        beta3L*PDubeta23)*pow(alphaL,-shiftAlphaPower)*pow(shiftGammaCoeffValue,-1),0)) 
        + IfThen(fixAdvectionTerms != 0,beta1L*PDuXt21 + beta2L*PDuXt22 + 
        beta3L*PDuXt23,0);
      
      B3rhsL = dotXt3 + GDissB3 + IfThen(fixAdvectionTerms == 0 && 
        advectShift != 0,beta1L*PDuB31 + beta2L*PDuB32 + beta3L*PDuB33,0) - 
        betaDriverValue*(B3L + IfThen(fixAdvectionTerms != 0 && advectShift != 
        0 && shiftGammaCoeffValue != 0.,(beta1L*PDubeta31 + beta2L*PDubeta32 + 
        beta3L*PDubeta33)*pow(alphaL,-shiftAlphaPower)*pow(shiftGammaCoeffValue,-1),0)) 
        + IfThen(fixAdvectionTerms != 0,beta1L*PDuXt31 + beta2L*PDuXt32 + 
        beta3L*PDuXt33,0);
    }
    else
    {
      B1rhsL = 0;
      
      B2rhsL = 0;
      
      B3rhsL = 0;
    }
    /* Copy local copies back to grid functions */
    B1rhs[index] = B1rhsL;
    B2rhs[index] = B2rhsL;
    B3rhs[index] = B3rhsL;
    beta1rhs[index] = beta1rhsL;
    beta2rhs[index] = beta2rhsL;
    beta3rhs[index] = beta3rhsL;
    trKrhs[index] = trKrhsL;
    Xt1rhs[index] = Xt1rhsL;
    Xt2rhs[index] = Xt2rhsL;
    Xt3rhs[index] = Xt3rhsL;
  }
  CCTK_ENDLOOP3(ML_BSSN_FD4_EvolutionInteriorSplit32);
}
extern "C" void ML_BSSN_FD4_EvolutionInteriorSplit32(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_FD4_EvolutionInteriorSplit32_Body");
  }
  if (cctk_iteration % ML_BSSN_FD4_EvolutionInteriorSplit32_calc_every != ML_BSSN_FD4_EvolutionInteriorSplit32_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "grid::coordinates",
    "ML_BSSN_FD4::ML_confac",
    "ML_BSSN_FD4::ML_curv",
    "ML_BSSN_FD4::ML_dconfac",
    "ML_BSSN_FD4::ML_dlapse",
    "ML_BSSN_FD4::ML_dmetric",
    "ML_BSSN_FD4::ML_dshift",
    "ML_BSSN_FD4::ML_dtshift",
    "ML_BSSN_FD4::ML_dtshiftrhs",
    "ML_BSSN_FD4::ML_Gamma",
    "ML_BSSN_FD4::ML_Gammarhs",
    "ML_BSSN_FD4::ML_lapse",
    "ML_BSSN_FD4::ML_metric",
    "ML_BSSN_FD4::ML_shift",
    "ML_BSSN_FD4::ML_shiftrhs",
    "ML_BSSN_FD4::ML_trace_curv",
    "ML_BSSN_FD4::ML_trace_curvrhs",
    "ML_BSSN_FD4::ML_volume_form"};
  AssertGroupStorage(cctkGH, "ML_BSSN_FD4_EvolutionInteriorSplit32", 18, groups);
  
  
  LoopOverInterior(cctkGH, ML_BSSN_FD4_EvolutionInteriorSplit32_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_FD4_EvolutionInteriorSplit32_Body");
  }
}

} // namespace ML_BSSN_FD4
