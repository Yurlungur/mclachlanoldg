/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"
#include "loopcontrol.h"

namespace ML_BSSN_FD4 {

extern "C" void ML_BSSN_FD4_EvolutionInterior_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_FD4_EvolutionInterior_calc_every != ML_BSSN_FD4_EvolutionInterior_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_confacrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_confacrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_curvrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_curvrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_dtlapserhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_dtlapserhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_dtshiftrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_dtshiftrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_Gammarhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_Gammarhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_lapserhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_lapserhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_metricrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_metricrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_shiftrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_shiftrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_FD4::ML_trace_curvrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_FD4::ML_trace_curvrhs.");
  return;
}

static void ML_BSSN_FD4_EvolutionInterior_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_BSSN_FD4_EvolutionInterior,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    /* Assign local copies of grid functions */
    
    CCTK_REAL AL CCTK_ATTRIBUTE_UNUSED = A[index];
    CCTK_REAL alphaL CCTK_ATTRIBUTE_UNUSED = alpha[index];
    CCTK_REAL At11L CCTK_ATTRIBUTE_UNUSED = At11[index];
    CCTK_REAL At12L CCTK_ATTRIBUTE_UNUSED = At12[index];
    CCTK_REAL At13L CCTK_ATTRIBUTE_UNUSED = At13[index];
    CCTK_REAL At22L CCTK_ATTRIBUTE_UNUSED = At22[index];
    CCTK_REAL At23L CCTK_ATTRIBUTE_UNUSED = At23[index];
    CCTK_REAL At33L CCTK_ATTRIBUTE_UNUSED = At33[index];
    CCTK_REAL B1L CCTK_ATTRIBUTE_UNUSED = B1[index];
    CCTK_REAL B2L CCTK_ATTRIBUTE_UNUSED = B2[index];
    CCTK_REAL B3L CCTK_ATTRIBUTE_UNUSED = B3[index];
    CCTK_REAL beta1L CCTK_ATTRIBUTE_UNUSED = beta1[index];
    CCTK_REAL beta2L CCTK_ATTRIBUTE_UNUSED = beta2[index];
    CCTK_REAL beta3L CCTK_ATTRIBUTE_UNUSED = beta3[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL myDetJL CCTK_ATTRIBUTE_UNUSED = myDetJ[index];
    CCTK_REAL PDalpha1L CCTK_ATTRIBUTE_UNUSED = PDalpha1[index];
    CCTK_REAL PDalpha2L CCTK_ATTRIBUTE_UNUSED = PDalpha2[index];
    CCTK_REAL PDalpha3L CCTK_ATTRIBUTE_UNUSED = PDalpha3[index];
    CCTK_REAL PDbeta11L CCTK_ATTRIBUTE_UNUSED = PDbeta11[index];
    CCTK_REAL PDbeta12L CCTK_ATTRIBUTE_UNUSED = PDbeta12[index];
    CCTK_REAL PDbeta13L CCTK_ATTRIBUTE_UNUSED = PDbeta13[index];
    CCTK_REAL PDbeta21L CCTK_ATTRIBUTE_UNUSED = PDbeta21[index];
    CCTK_REAL PDbeta22L CCTK_ATTRIBUTE_UNUSED = PDbeta22[index];
    CCTK_REAL PDbeta23L CCTK_ATTRIBUTE_UNUSED = PDbeta23[index];
    CCTK_REAL PDbeta31L CCTK_ATTRIBUTE_UNUSED = PDbeta31[index];
    CCTK_REAL PDbeta32L CCTK_ATTRIBUTE_UNUSED = PDbeta32[index];
    CCTK_REAL PDbeta33L CCTK_ATTRIBUTE_UNUSED = PDbeta33[index];
    CCTK_REAL PDgt111L CCTK_ATTRIBUTE_UNUSED = PDgt111[index];
    CCTK_REAL PDgt112L CCTK_ATTRIBUTE_UNUSED = PDgt112[index];
    CCTK_REAL PDgt113L CCTK_ATTRIBUTE_UNUSED = PDgt113[index];
    CCTK_REAL PDgt121L CCTK_ATTRIBUTE_UNUSED = PDgt121[index];
    CCTK_REAL PDgt122L CCTK_ATTRIBUTE_UNUSED = PDgt122[index];
    CCTK_REAL PDgt123L CCTK_ATTRIBUTE_UNUSED = PDgt123[index];
    CCTK_REAL PDgt131L CCTK_ATTRIBUTE_UNUSED = PDgt131[index];
    CCTK_REAL PDgt132L CCTK_ATTRIBUTE_UNUSED = PDgt132[index];
    CCTK_REAL PDgt133L CCTK_ATTRIBUTE_UNUSED = PDgt133[index];
    CCTK_REAL PDgt221L CCTK_ATTRIBUTE_UNUSED = PDgt221[index];
    CCTK_REAL PDgt222L CCTK_ATTRIBUTE_UNUSED = PDgt222[index];
    CCTK_REAL PDgt223L CCTK_ATTRIBUTE_UNUSED = PDgt223[index];
    CCTK_REAL PDgt231L CCTK_ATTRIBUTE_UNUSED = PDgt231[index];
    CCTK_REAL PDgt232L CCTK_ATTRIBUTE_UNUSED = PDgt232[index];
    CCTK_REAL PDgt233L CCTK_ATTRIBUTE_UNUSED = PDgt233[index];
    CCTK_REAL PDgt331L CCTK_ATTRIBUTE_UNUSED = PDgt331[index];
    CCTK_REAL PDgt332L CCTK_ATTRIBUTE_UNUSED = PDgt332[index];
    CCTK_REAL PDgt333L CCTK_ATTRIBUTE_UNUSED = PDgt333[index];
    CCTK_REAL PDphiW1L CCTK_ATTRIBUTE_UNUSED = PDphiW1[index];
    CCTK_REAL PDphiW2L CCTK_ATTRIBUTE_UNUSED = PDphiW2[index];
    CCTK_REAL PDphiW3L CCTK_ATTRIBUTE_UNUSED = PDphiW3[index];
    CCTK_REAL phiWL CCTK_ATTRIBUTE_UNUSED = phiW[index];
    CCTK_REAL rL CCTK_ATTRIBUTE_UNUSED = r[index];
    CCTK_REAL trKL CCTK_ATTRIBUTE_UNUSED = trK[index];
    CCTK_REAL Xt1L CCTK_ATTRIBUTE_UNUSED = Xt1[index];
    CCTK_REAL Xt2L CCTK_ATTRIBUTE_UNUSED = Xt2[index];
    CCTK_REAL Xt3L CCTK_ATTRIBUTE_UNUSED = Xt3[index];
    
    CCTK_REAL eTttL, eTtxL, eTtyL, eTtzL, eTxxL, eTxyL, eTxzL, eTyyL, eTyzL, eTzzL CCTK_ATTRIBUTE_UNUSED ;
    
    if (assume_stress_energy_state>=0 ? assume_stress_energy_state : *stress_energy_state)
    {
      eTttL = eTtt[index];
      eTtxL = eTtx[index];
      eTtyL = eTty[index];
      eTtzL = eTtz[index];
      eTxxL = eTxx[index];
      eTxyL = eTxy[index];
      eTxzL = eTxz[index];
      eTyyL = eTyy[index];
      eTyzL = eTyz[index];
      eTzzL = eTzz[index];
    }
    else
    {
      eTttL = 0.;
      eTtxL = 0.;
      eTtyL = 0.;
      eTtzL = 0.;
      eTxxL = 0.;
      eTxyL = 0.;
      eTxzL = 0.;
      eTyyL = 0.;
      eTyzL = 0.;
      eTzzL = 0.;
    }
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDXt11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,-2,0,0) - 
      8*GFOffset(Xt1,-1,0,0) + 8*GFOffset(Xt1,1,0,0) - 
      GFOffset(Xt1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDXt21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,-2,0,0) - 
      8*GFOffset(Xt2,-1,0,0) + 8*GFOffset(Xt2,1,0,0) - 
      GFOffset(Xt2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDXt31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,-2,0,0) - 
      8*GFOffset(Xt3,-1,0,0) + 8*GFOffset(Xt3,1,0,0) - 
      GFOffset(Xt3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDXt12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,0,-2,0) - 
      8*GFOffset(Xt1,0,-1,0) + 8*GFOffset(Xt1,0,1,0) - 
      GFOffset(Xt1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDXt22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,0,-2,0) - 
      8*GFOffset(Xt2,0,-1,0) + 8*GFOffset(Xt2,0,1,0) - 
      GFOffset(Xt2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDXt32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,0,-2,0) - 
      8*GFOffset(Xt3,0,-1,0) + 8*GFOffset(Xt3,0,1,0) - 
      GFOffset(Xt3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDXt13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,0,0,-2) - 
      8*GFOffset(Xt1,0,0,-1) + 8*GFOffset(Xt1,0,0,1) - 
      GFOffset(Xt1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDXt23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,0,0,-2) - 
      8*GFOffset(Xt2,0,0,-1) + 8*GFOffset(Xt2,0,0,1) - 
      GFOffset(Xt2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDXt33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,0,0,-2) - 
      8*GFOffset(Xt3,0,0,-1) + 8*GFOffset(Xt3,0,0,1) - 
      GFOffset(Xt3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDtrK1 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,-2,0,0) - 
      8*GFOffset(trK,-1,0,0) + 8*GFOffset(trK,1,0,0) - 
      GFOffset(trK,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDtrK2 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,0,-2,0) - 
      8*GFOffset(trK,0,-1,0) + 8*GFOffset(trK,0,1,0) - 
      GFOffset(trK,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDtrK3 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,0,0,-2) - 
      8*GFOffset(trK,0,0,-1) + 8*GFOffset(trK,0,0,1) - 
      GFOffset(trK,0,0,2))*pow(dz,-1);
    
    CCTK_REAL PDtrK1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDtrK2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDtrK3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDXt11 = J11L*LDXt11 + J21L*LDXt12 + J31L*LDXt13;
      
      PDXt21 = J11L*LDXt21 + J21L*LDXt22 + J31L*LDXt23;
      
      PDXt31 = J11L*LDXt31 + J21L*LDXt32 + J31L*LDXt33;
      
      PDXt12 = J12L*LDXt11 + J22L*LDXt12 + J32L*LDXt13;
      
      PDXt22 = J12L*LDXt21 + J22L*LDXt22 + J32L*LDXt23;
      
      PDXt32 = J12L*LDXt31 + J22L*LDXt32 + J32L*LDXt33;
      
      PDXt13 = J13L*LDXt11 + J23L*LDXt12 + J33L*LDXt13;
      
      PDXt23 = J13L*LDXt21 + J23L*LDXt22 + J33L*LDXt23;
      
      PDXt33 = J13L*LDXt31 + J23L*LDXt32 + J33L*LDXt33;
      
      PDtrK1 = J11L*LDtrK1 + J21L*LDtrK2 + J31L*LDtrK3;
      
      PDtrK2 = J12L*LDtrK1 + J22L*LDtrK2 + J32L*LDtrK3;
      
      PDtrK3 = J13L*LDtrK1 + J23L*LDtrK2 + J33L*LDtrK3;
    }
    else
    {
      PDXt11 = LDXt11;
      
      PDXt21 = LDXt21;
      
      PDXt31 = LDXt31;
      
      PDXt12 = LDXt12;
      
      PDXt22 = LDXt22;
      
      PDXt32 = LDXt32;
      
      PDXt13 = LDXt13;
      
      PDXt23 = LDXt23;
      
      PDXt33 = LDXt33;
      
      PDtrK1 = LDtrK1;
      
      PDtrK2 = LDtrK2;
      
      PDtrK3 = LDtrK3;
    }
    
    CCTK_REAL LDPDphiW11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW1,-2,0,0) - 
      8*GFOffset(PDphiW1,-1,0,0) + 8*GFOffset(PDphiW1,1,0,0) - 
      GFOffset(PDphiW1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDphiW12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW1,0,-2,0) - 
      8*GFOffset(PDphiW1,0,-1,0) + 8*GFOffset(PDphiW1,0,1,0) - 
      GFOffset(PDphiW1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDphiW13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW1,0,0,-2) - 
      8*GFOffset(PDphiW1,0,0,-1) + 8*GFOffset(PDphiW1,0,0,1) - 
      GFOffset(PDphiW1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDphiW21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW2,-2,0,0) - 
      8*GFOffset(PDphiW2,-1,0,0) + 8*GFOffset(PDphiW2,1,0,0) - 
      GFOffset(PDphiW2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDphiW22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW2,0,-2,0) - 
      8*GFOffset(PDphiW2,0,-1,0) + 8*GFOffset(PDphiW2,0,1,0) - 
      GFOffset(PDphiW2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDphiW23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW2,0,0,-2) - 
      8*GFOffset(PDphiW2,0,0,-1) + 8*GFOffset(PDphiW2,0,0,1) - 
      GFOffset(PDphiW2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDphiW31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW3,-2,0,0) - 
      8*GFOffset(PDphiW3,-1,0,0) + 8*GFOffset(PDphiW3,1,0,0) - 
      GFOffset(PDphiW3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDphiW32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW3,0,-2,0) - 
      8*GFOffset(PDphiW3,0,-1,0) + 8*GFOffset(PDphiW3,0,1,0) - 
      GFOffset(PDphiW3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDphiW33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDphiW3,0,0,-2) - 
      8*GFOffset(PDphiW3,0,0,-1) + 8*GFOffset(PDphiW3,0,0,1) - 
      GFOffset(PDphiW3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1111 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt111,-2,0,0) - 
      8*GFOffset(PDgt111,-1,0,0) + 8*GFOffset(PDgt111,1,0,0) - 
      GFOffset(PDgt111,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1112 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt111,0,-2,0) - 
      8*GFOffset(PDgt111,0,-1,0) + 8*GFOffset(PDgt111,0,1,0) - 
      GFOffset(PDgt111,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1113 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt111,0,0,-2) - 
      8*GFOffset(PDgt111,0,0,-1) + 8*GFOffset(PDgt111,0,0,1) - 
      GFOffset(PDgt111,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1121 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt112,-2,0,0) - 
      8*GFOffset(PDgt112,-1,0,0) + 8*GFOffset(PDgt112,1,0,0) - 
      GFOffset(PDgt112,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1122 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt112,0,-2,0) - 
      8*GFOffset(PDgt112,0,-1,0) + 8*GFOffset(PDgt112,0,1,0) - 
      GFOffset(PDgt112,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1123 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt112,0,0,-2) - 
      8*GFOffset(PDgt112,0,0,-1) + 8*GFOffset(PDgt112,0,0,1) - 
      GFOffset(PDgt112,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1131 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt113,-2,0,0) - 
      8*GFOffset(PDgt113,-1,0,0) + 8*GFOffset(PDgt113,1,0,0) - 
      GFOffset(PDgt113,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1132 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt113,0,-2,0) - 
      8*GFOffset(PDgt113,0,-1,0) + 8*GFOffset(PDgt113,0,1,0) - 
      GFOffset(PDgt113,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1133 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt113,0,0,-2) - 
      8*GFOffset(PDgt113,0,0,-1) + 8*GFOffset(PDgt113,0,0,1) - 
      GFOffset(PDgt113,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1211 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt121,-2,0,0) - 
      8*GFOffset(PDgt121,-1,0,0) + 8*GFOffset(PDgt121,1,0,0) - 
      GFOffset(PDgt121,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1212 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt121,0,-2,0) - 
      8*GFOffset(PDgt121,0,-1,0) + 8*GFOffset(PDgt121,0,1,0) - 
      GFOffset(PDgt121,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1213 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt121,0,0,-2) - 
      8*GFOffset(PDgt121,0,0,-1) + 8*GFOffset(PDgt121,0,0,1) - 
      GFOffset(PDgt121,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1221 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt122,-2,0,0) - 
      8*GFOffset(PDgt122,-1,0,0) + 8*GFOffset(PDgt122,1,0,0) - 
      GFOffset(PDgt122,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1222 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt122,0,-2,0) - 
      8*GFOffset(PDgt122,0,-1,0) + 8*GFOffset(PDgt122,0,1,0) - 
      GFOffset(PDgt122,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1223 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt122,0,0,-2) - 
      8*GFOffset(PDgt122,0,0,-1) + 8*GFOffset(PDgt122,0,0,1) - 
      GFOffset(PDgt122,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1231 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt123,-2,0,0) - 
      8*GFOffset(PDgt123,-1,0,0) + 8*GFOffset(PDgt123,1,0,0) - 
      GFOffset(PDgt123,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1232 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt123,0,-2,0) - 
      8*GFOffset(PDgt123,0,-1,0) + 8*GFOffset(PDgt123,0,1,0) - 
      GFOffset(PDgt123,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1233 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt123,0,0,-2) - 
      8*GFOffset(PDgt123,0,0,-1) + 8*GFOffset(PDgt123,0,0,1) - 
      GFOffset(PDgt123,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1311 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt131,-2,0,0) - 
      8*GFOffset(PDgt131,-1,0,0) + 8*GFOffset(PDgt131,1,0,0) - 
      GFOffset(PDgt131,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1312 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt131,0,-2,0) - 
      8*GFOffset(PDgt131,0,-1,0) + 8*GFOffset(PDgt131,0,1,0) - 
      GFOffset(PDgt131,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1313 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt131,0,0,-2) - 
      8*GFOffset(PDgt131,0,0,-1) + 8*GFOffset(PDgt131,0,0,1) - 
      GFOffset(PDgt131,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1321 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt132,-2,0,0) - 
      8*GFOffset(PDgt132,-1,0,0) + 8*GFOffset(PDgt132,1,0,0) - 
      GFOffset(PDgt132,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1322 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt132,0,-2,0) - 
      8*GFOffset(PDgt132,0,-1,0) + 8*GFOffset(PDgt132,0,1,0) - 
      GFOffset(PDgt132,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1323 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt132,0,0,-2) - 
      8*GFOffset(PDgt132,0,0,-1) + 8*GFOffset(PDgt132,0,0,1) - 
      GFOffset(PDgt132,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt133,-2,0,0) - 
      8*GFOffset(PDgt133,-1,0,0) + 8*GFOffset(PDgt133,1,0,0) - 
      GFOffset(PDgt133,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt133,0,-2,0) - 
      8*GFOffset(PDgt133,0,-1,0) + 8*GFOffset(PDgt133,0,1,0) - 
      GFOffset(PDgt133,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt133,0,0,-2) - 
      8*GFOffset(PDgt133,0,0,-1) + 8*GFOffset(PDgt133,0,0,1) - 
      GFOffset(PDgt133,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2211 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt221,-2,0,0) - 
      8*GFOffset(PDgt221,-1,0,0) + 8*GFOffset(PDgt221,1,0,0) - 
      GFOffset(PDgt221,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2212 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt221,0,-2,0) - 
      8*GFOffset(PDgt221,0,-1,0) + 8*GFOffset(PDgt221,0,1,0) - 
      GFOffset(PDgt221,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2213 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt221,0,0,-2) - 
      8*GFOffset(PDgt221,0,0,-1) + 8*GFOffset(PDgt221,0,0,1) - 
      GFOffset(PDgt221,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2221 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt222,-2,0,0) - 
      8*GFOffset(PDgt222,-1,0,0) + 8*GFOffset(PDgt222,1,0,0) - 
      GFOffset(PDgt222,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2222 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt222,0,-2,0) - 
      8*GFOffset(PDgt222,0,-1,0) + 8*GFOffset(PDgt222,0,1,0) - 
      GFOffset(PDgt222,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2223 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt222,0,0,-2) - 
      8*GFOffset(PDgt222,0,0,-1) + 8*GFOffset(PDgt222,0,0,1) - 
      GFOffset(PDgt222,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2231 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt223,-2,0,0) - 
      8*GFOffset(PDgt223,-1,0,0) + 8*GFOffset(PDgt223,1,0,0) - 
      GFOffset(PDgt223,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2232 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt223,0,-2,0) - 
      8*GFOffset(PDgt223,0,-1,0) + 8*GFOffset(PDgt223,0,1,0) - 
      GFOffset(PDgt223,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2233 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt223,0,0,-2) - 
      8*GFOffset(PDgt223,0,0,-1) + 8*GFOffset(PDgt223,0,0,1) - 
      GFOffset(PDgt223,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2311 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt231,-2,0,0) - 
      8*GFOffset(PDgt231,-1,0,0) + 8*GFOffset(PDgt231,1,0,0) - 
      GFOffset(PDgt231,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2312 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt231,0,-2,0) - 
      8*GFOffset(PDgt231,0,-1,0) + 8*GFOffset(PDgt231,0,1,0) - 
      GFOffset(PDgt231,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2313 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt231,0,0,-2) - 
      8*GFOffset(PDgt231,0,0,-1) + 8*GFOffset(PDgt231,0,0,1) - 
      GFOffset(PDgt231,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2321 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt232,-2,0,0) - 
      8*GFOffset(PDgt232,-1,0,0) + 8*GFOffset(PDgt232,1,0,0) - 
      GFOffset(PDgt232,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2322 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt232,0,-2,0) - 
      8*GFOffset(PDgt232,0,-1,0) + 8*GFOffset(PDgt232,0,1,0) - 
      GFOffset(PDgt232,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2323 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt232,0,0,-2) - 
      8*GFOffset(PDgt232,0,0,-1) + 8*GFOffset(PDgt232,0,0,1) - 
      GFOffset(PDgt232,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt233,-2,0,0) - 
      8*GFOffset(PDgt233,-1,0,0) + 8*GFOffset(PDgt233,1,0,0) - 
      GFOffset(PDgt233,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt233,0,-2,0) - 
      8*GFOffset(PDgt233,0,-1,0) + 8*GFOffset(PDgt233,0,1,0) - 
      GFOffset(PDgt233,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt233,0,0,-2) - 
      8*GFOffset(PDgt233,0,0,-1) + 8*GFOffset(PDgt233,0,0,1) - 
      GFOffset(PDgt233,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt3311 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt331,-2,0,0) - 
      8*GFOffset(PDgt331,-1,0,0) + 8*GFOffset(PDgt331,1,0,0) - 
      GFOffset(PDgt331,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt3312 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt331,0,-2,0) - 
      8*GFOffset(PDgt331,0,-1,0) + 8*GFOffset(PDgt331,0,1,0) - 
      GFOffset(PDgt331,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt3313 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt331,0,0,-2) - 
      8*GFOffset(PDgt331,0,0,-1) + 8*GFOffset(PDgt331,0,0,1) - 
      GFOffset(PDgt331,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt3321 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt332,-2,0,0) - 
      8*GFOffset(PDgt332,-1,0,0) + 8*GFOffset(PDgt332,1,0,0) - 
      GFOffset(PDgt332,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt3322 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt332,0,-2,0) - 
      8*GFOffset(PDgt332,0,-1,0) + 8*GFOffset(PDgt332,0,1,0) - 
      GFOffset(PDgt332,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt3323 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt332,0,0,-2) - 
      8*GFOffset(PDgt332,0,0,-1) + 8*GFOffset(PDgt332,0,0,1) - 
      GFOffset(PDgt332,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDgt3331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt333,-2,0,0) - 
      8*GFOffset(PDgt333,-1,0,0) + 8*GFOffset(PDgt333,1,0,0) - 
      GFOffset(PDgt333,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt3332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt333,0,-2,0) - 
      8*GFOffset(PDgt333,0,-1,0) + 8*GFOffset(PDgt333,0,1,0) - 
      GFOffset(PDgt333,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt3333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDgt333,0,0,-2) - 
      8*GFOffset(PDgt333,0,0,-1) + 8*GFOffset(PDgt333,0,0,1) - 
      GFOffset(PDgt333,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDalpha11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha1,-2,0,0) - 
      8*GFOffset(PDalpha1,-1,0,0) + 8*GFOffset(PDalpha1,1,0,0) - 
      GFOffset(PDalpha1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha1,0,-2,0) - 
      8*GFOffset(PDalpha1,0,-1,0) + 8*GFOffset(PDalpha1,0,1,0) - 
      GFOffset(PDalpha1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha1,0,0,-2) - 
      8*GFOffset(PDalpha1,0,0,-1) + 8*GFOffset(PDalpha1,0,0,1) - 
      GFOffset(PDalpha1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDalpha21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha2,-2,0,0) - 
      8*GFOffset(PDalpha2,-1,0,0) + 8*GFOffset(PDalpha2,1,0,0) - 
      GFOffset(PDalpha2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha2,0,-2,0) - 
      8*GFOffset(PDalpha2,0,-1,0) + 8*GFOffset(PDalpha2,0,1,0) - 
      GFOffset(PDalpha2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha2,0,0,-2) - 
      8*GFOffset(PDalpha2,0,0,-1) + 8*GFOffset(PDalpha2,0,0,1) - 
      GFOffset(PDalpha2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDalpha31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha3,-2,0,0) - 
      8*GFOffset(PDalpha3,-1,0,0) + 8*GFOffset(PDalpha3,1,0,0) - 
      GFOffset(PDalpha3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha3,0,-2,0) - 
      8*GFOffset(PDalpha3,0,-1,0) + 8*GFOffset(PDalpha3,0,1,0) - 
      GFOffset(PDalpha3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDalpha3,0,0,-2) - 
      8*GFOffset(PDalpha3,0,0,-1) + 8*GFOffset(PDalpha3,0,0,1) - 
      GFOffset(PDalpha3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta111 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta11,-2,0,0) - 
      8*GFOffset(PDbeta11,-1,0,0) + 8*GFOffset(PDbeta11,1,0,0) - 
      GFOffset(PDbeta11,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta211 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta21,-2,0,0) - 
      8*GFOffset(PDbeta21,-1,0,0) + 8*GFOffset(PDbeta21,1,0,0) - 
      GFOffset(PDbeta21,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta311 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta31,-2,0,0) - 
      8*GFOffset(PDbeta31,-1,0,0) + 8*GFOffset(PDbeta31,1,0,0) - 
      GFOffset(PDbeta31,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta112 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta11,0,-2,0) - 
      8*GFOffset(PDbeta11,0,-1,0) + 8*GFOffset(PDbeta11,0,1,0) - 
      GFOffset(PDbeta11,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta212 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta21,0,-2,0) - 
      8*GFOffset(PDbeta21,0,-1,0) + 8*GFOffset(PDbeta21,0,1,0) - 
      GFOffset(PDbeta21,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta312 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta31,0,-2,0) - 
      8*GFOffset(PDbeta31,0,-1,0) + 8*GFOffset(PDbeta31,0,1,0) - 
      GFOffset(PDbeta31,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta113 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta11,0,0,-2) - 
      8*GFOffset(PDbeta11,0,0,-1) + 8*GFOffset(PDbeta11,0,0,1) - 
      GFOffset(PDbeta11,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta213 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta21,0,0,-2) - 
      8*GFOffset(PDbeta21,0,0,-1) + 8*GFOffset(PDbeta21,0,0,1) - 
      GFOffset(PDbeta21,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta313 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta31,0,0,-2) - 
      8*GFOffset(PDbeta31,0,0,-1) + 8*GFOffset(PDbeta31,0,0,1) - 
      GFOffset(PDbeta31,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta121 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta12,-2,0,0) - 
      8*GFOffset(PDbeta12,-1,0,0) + 8*GFOffset(PDbeta12,1,0,0) - 
      GFOffset(PDbeta12,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta221 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta22,-2,0,0) - 
      8*GFOffset(PDbeta22,-1,0,0) + 8*GFOffset(PDbeta22,1,0,0) - 
      GFOffset(PDbeta22,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta321 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta32,-2,0,0) - 
      8*GFOffset(PDbeta32,-1,0,0) + 8*GFOffset(PDbeta32,1,0,0) - 
      GFOffset(PDbeta32,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta122 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta12,0,-2,0) - 
      8*GFOffset(PDbeta12,0,-1,0) + 8*GFOffset(PDbeta12,0,1,0) - 
      GFOffset(PDbeta12,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta222 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta22,0,-2,0) - 
      8*GFOffset(PDbeta22,0,-1,0) + 8*GFOffset(PDbeta22,0,1,0) - 
      GFOffset(PDbeta22,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta322 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta32,0,-2,0) - 
      8*GFOffset(PDbeta32,0,-1,0) + 8*GFOffset(PDbeta32,0,1,0) - 
      GFOffset(PDbeta32,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta123 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta12,0,0,-2) - 
      8*GFOffset(PDbeta12,0,0,-1) + 8*GFOffset(PDbeta12,0,0,1) - 
      GFOffset(PDbeta12,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta223 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta22,0,0,-2) - 
      8*GFOffset(PDbeta22,0,0,-1) + 8*GFOffset(PDbeta22,0,0,1) - 
      GFOffset(PDbeta22,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta323 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta32,0,0,-2) - 
      8*GFOffset(PDbeta32,0,0,-1) + 8*GFOffset(PDbeta32,0,0,1) - 
      GFOffset(PDbeta32,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta131 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta13,-2,0,0) - 
      8*GFOffset(PDbeta13,-1,0,0) + 8*GFOffset(PDbeta13,1,0,0) - 
      GFOffset(PDbeta13,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta231 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta23,-2,0,0) - 
      8*GFOffset(PDbeta23,-1,0,0) + 8*GFOffset(PDbeta23,1,0,0) - 
      GFOffset(PDbeta23,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta33,-2,0,0) - 
      8*GFOffset(PDbeta33,-1,0,0) + 8*GFOffset(PDbeta33,1,0,0) - 
      GFOffset(PDbeta33,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDPDbeta132 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta13,0,-2,0) - 
      8*GFOffset(PDbeta13,0,-1,0) + 8*GFOffset(PDbeta13,0,1,0) - 
      GFOffset(PDbeta13,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta232 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta23,0,-2,0) - 
      8*GFOffset(PDbeta23,0,-1,0) + 8*GFOffset(PDbeta23,0,1,0) - 
      GFOffset(PDbeta23,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta33,0,-2,0) - 
      8*GFOffset(PDbeta33,0,-1,0) + 8*GFOffset(PDbeta33,0,1,0) - 
      GFOffset(PDbeta33,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDPDbeta133 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta13,0,0,-2) - 
      8*GFOffset(PDbeta13,0,0,-1) + 8*GFOffset(PDbeta13,0,0,1) - 
      GFOffset(PDbeta13,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta233 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta23,0,0,-2) - 
      8*GFOffset(PDbeta23,0,0,-1) + 8*GFOffset(PDbeta23,0,0,1) - 
      GFOffset(PDbeta23,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDPDbeta333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(PDbeta33,0,0,-2) - 
      8*GFOffset(PDbeta33,0,0,-1) + 8*GFOffset(PDbeta33,0,0,1) - 
      GFOffset(PDbeta33,0,0,2))*pow(dz,-1);
    
    CCTK_REAL PDPDalpha11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta111 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta112 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta113 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta121 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta122 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta123 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta131 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta132 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta133 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta211 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta212 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta213 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDbeta333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1111 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1112 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1113 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1121 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1122 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1123 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1131 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1132 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1133 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1211 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1212 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1213 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2211 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2212 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2213 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDPDphiW11 = J11L*LDPDphiW11 + J21L*LDPDphiW12 + J31L*LDPDphiW13;
      
      PDPDphiW12 = J12L*LDPDphiW11 + J22L*LDPDphiW12 + J32L*LDPDphiW13;
      
      PDPDphiW13 = J13L*LDPDphiW11 + J23L*LDPDphiW12 + J33L*LDPDphiW13;
      
      PDPDphiW21 = J11L*LDPDphiW21 + J21L*LDPDphiW22 + J31L*LDPDphiW23;
      
      PDPDphiW22 = J12L*LDPDphiW21 + J22L*LDPDphiW22 + J32L*LDPDphiW23;
      
      PDPDphiW23 = J13L*LDPDphiW21 + J23L*LDPDphiW22 + J33L*LDPDphiW23;
      
      PDPDphiW31 = J11L*LDPDphiW31 + J21L*LDPDphiW32 + J31L*LDPDphiW33;
      
      PDPDphiW32 = J12L*LDPDphiW31 + J22L*LDPDphiW32 + J32L*LDPDphiW33;
      
      PDPDphiW33 = J13L*LDPDphiW31 + J23L*LDPDphiW32 + J33L*LDPDphiW33;
      
      PDPDgt1111 = J11L*LDPDgt1111 + J21L*LDPDgt1112 + J31L*LDPDgt1113;
      
      PDPDgt1112 = J12L*LDPDgt1111 + J22L*LDPDgt1112 + J32L*LDPDgt1113;
      
      PDPDgt1113 = J13L*LDPDgt1111 + J23L*LDPDgt1112 + J33L*LDPDgt1113;
      
      PDPDgt1121 = J11L*LDPDgt1121 + J21L*LDPDgt1122 + J31L*LDPDgt1123;
      
      PDPDgt1122 = J12L*LDPDgt1121 + J22L*LDPDgt1122 + J32L*LDPDgt1123;
      
      PDPDgt1123 = J13L*LDPDgt1121 + J23L*LDPDgt1122 + J33L*LDPDgt1123;
      
      PDPDgt1131 = J11L*LDPDgt1131 + J21L*LDPDgt1132 + J31L*LDPDgt1133;
      
      PDPDgt1132 = J12L*LDPDgt1131 + J22L*LDPDgt1132 + J32L*LDPDgt1133;
      
      PDPDgt1133 = J13L*LDPDgt1131 + J23L*LDPDgt1132 + J33L*LDPDgt1133;
      
      PDPDgt1211 = J11L*LDPDgt1211 + J21L*LDPDgt1212 + J31L*LDPDgt1213;
      
      PDPDgt1212 = J12L*LDPDgt1211 + J22L*LDPDgt1212 + J32L*LDPDgt1213;
      
      PDPDgt1213 = J13L*LDPDgt1211 + J23L*LDPDgt1212 + J33L*LDPDgt1213;
      
      PDPDgt1221 = J11L*LDPDgt1221 + J21L*LDPDgt1222 + J31L*LDPDgt1223;
      
      PDPDgt1222 = J12L*LDPDgt1221 + J22L*LDPDgt1222 + J32L*LDPDgt1223;
      
      PDPDgt1223 = J13L*LDPDgt1221 + J23L*LDPDgt1222 + J33L*LDPDgt1223;
      
      PDPDgt1231 = J11L*LDPDgt1231 + J21L*LDPDgt1232 + J31L*LDPDgt1233;
      
      PDPDgt1232 = J12L*LDPDgt1231 + J22L*LDPDgt1232 + J32L*LDPDgt1233;
      
      PDPDgt1233 = J13L*LDPDgt1231 + J23L*LDPDgt1232 + J33L*LDPDgt1233;
      
      PDPDgt1311 = J11L*LDPDgt1311 + J21L*LDPDgt1312 + J31L*LDPDgt1313;
      
      PDPDgt1312 = J12L*LDPDgt1311 + J22L*LDPDgt1312 + J32L*LDPDgt1313;
      
      PDPDgt1313 = J13L*LDPDgt1311 + J23L*LDPDgt1312 + J33L*LDPDgt1313;
      
      PDPDgt1321 = J11L*LDPDgt1321 + J21L*LDPDgt1322 + J31L*LDPDgt1323;
      
      PDPDgt1322 = J12L*LDPDgt1321 + J22L*LDPDgt1322 + J32L*LDPDgt1323;
      
      PDPDgt1323 = J13L*LDPDgt1321 + J23L*LDPDgt1322 + J33L*LDPDgt1323;
      
      PDPDgt1331 = J11L*LDPDgt1331 + J21L*LDPDgt1332 + J31L*LDPDgt1333;
      
      PDPDgt1332 = J12L*LDPDgt1331 + J22L*LDPDgt1332 + J32L*LDPDgt1333;
      
      PDPDgt1333 = J13L*LDPDgt1331 + J23L*LDPDgt1332 + J33L*LDPDgt1333;
      
      PDPDgt2211 = J11L*LDPDgt2211 + J21L*LDPDgt2212 + J31L*LDPDgt2213;
      
      PDPDgt2212 = J12L*LDPDgt2211 + J22L*LDPDgt2212 + J32L*LDPDgt2213;
      
      PDPDgt2213 = J13L*LDPDgt2211 + J23L*LDPDgt2212 + J33L*LDPDgt2213;
      
      PDPDgt2221 = J11L*LDPDgt2221 + J21L*LDPDgt2222 + J31L*LDPDgt2223;
      
      PDPDgt2222 = J12L*LDPDgt2221 + J22L*LDPDgt2222 + J32L*LDPDgt2223;
      
      PDPDgt2223 = J13L*LDPDgt2221 + J23L*LDPDgt2222 + J33L*LDPDgt2223;
      
      PDPDgt2231 = J11L*LDPDgt2231 + J21L*LDPDgt2232 + J31L*LDPDgt2233;
      
      PDPDgt2232 = J12L*LDPDgt2231 + J22L*LDPDgt2232 + J32L*LDPDgt2233;
      
      PDPDgt2233 = J13L*LDPDgt2231 + J23L*LDPDgt2232 + J33L*LDPDgt2233;
      
      PDPDgt2311 = J11L*LDPDgt2311 + J21L*LDPDgt2312 + J31L*LDPDgt2313;
      
      PDPDgt2312 = J12L*LDPDgt2311 + J22L*LDPDgt2312 + J32L*LDPDgt2313;
      
      PDPDgt2313 = J13L*LDPDgt2311 + J23L*LDPDgt2312 + J33L*LDPDgt2313;
      
      PDPDgt2321 = J11L*LDPDgt2321 + J21L*LDPDgt2322 + J31L*LDPDgt2323;
      
      PDPDgt2322 = J12L*LDPDgt2321 + J22L*LDPDgt2322 + J32L*LDPDgt2323;
      
      PDPDgt2323 = J13L*LDPDgt2321 + J23L*LDPDgt2322 + J33L*LDPDgt2323;
      
      PDPDgt2331 = J11L*LDPDgt2331 + J21L*LDPDgt2332 + J31L*LDPDgt2333;
      
      PDPDgt2332 = J12L*LDPDgt2331 + J22L*LDPDgt2332 + J32L*LDPDgt2333;
      
      PDPDgt2333 = J13L*LDPDgt2331 + J23L*LDPDgt2332 + J33L*LDPDgt2333;
      
      PDPDgt3311 = J11L*LDPDgt3311 + J21L*LDPDgt3312 + J31L*LDPDgt3313;
      
      PDPDgt3312 = J12L*LDPDgt3311 + J22L*LDPDgt3312 + J32L*LDPDgt3313;
      
      PDPDgt3313 = J13L*LDPDgt3311 + J23L*LDPDgt3312 + J33L*LDPDgt3313;
      
      PDPDgt3321 = J11L*LDPDgt3321 + J21L*LDPDgt3322 + J31L*LDPDgt3323;
      
      PDPDgt3322 = J12L*LDPDgt3321 + J22L*LDPDgt3322 + J32L*LDPDgt3323;
      
      PDPDgt3323 = J13L*LDPDgt3321 + J23L*LDPDgt3322 + J33L*LDPDgt3323;
      
      PDPDgt3331 = J11L*LDPDgt3331 + J21L*LDPDgt3332 + J31L*LDPDgt3333;
      
      PDPDgt3332 = J12L*LDPDgt3331 + J22L*LDPDgt3332 + J32L*LDPDgt3333;
      
      PDPDgt3333 = J13L*LDPDgt3331 + J23L*LDPDgt3332 + J33L*LDPDgt3333;
      
      PDPDalpha11 = J11L*LDPDalpha11 + J21L*LDPDalpha12 + J31L*LDPDalpha13;
      
      PDPDalpha12 = J12L*LDPDalpha11 + J22L*LDPDalpha12 + J32L*LDPDalpha13;
      
      PDPDalpha13 = J13L*LDPDalpha11 + J23L*LDPDalpha12 + J33L*LDPDalpha13;
      
      PDPDalpha21 = J11L*LDPDalpha21 + J21L*LDPDalpha22 + J31L*LDPDalpha23;
      
      PDPDalpha22 = J12L*LDPDalpha21 + J22L*LDPDalpha22 + J32L*LDPDalpha23;
      
      PDPDalpha23 = J13L*LDPDalpha21 + J23L*LDPDalpha22 + J33L*LDPDalpha23;
      
      PDPDalpha31 = J11L*LDPDalpha31 + J21L*LDPDalpha32 + J31L*LDPDalpha33;
      
      PDPDalpha32 = J12L*LDPDalpha31 + J22L*LDPDalpha32 + J32L*LDPDalpha33;
      
      PDPDalpha33 = J13L*LDPDalpha31 + J23L*LDPDalpha32 + J33L*LDPDalpha33;
      
      PDPDbeta111 = J11L*LDPDbeta111 + J21L*LDPDbeta112 + J31L*LDPDbeta113;
      
      PDPDbeta211 = J11L*LDPDbeta211 + J21L*LDPDbeta212 + J31L*LDPDbeta213;
      
      PDPDbeta311 = J11L*LDPDbeta311 + J21L*LDPDbeta312 + J31L*LDPDbeta313;
      
      PDPDbeta112 = J12L*LDPDbeta111 + J22L*LDPDbeta112 + J32L*LDPDbeta113;
      
      PDPDbeta212 = J12L*LDPDbeta211 + J22L*LDPDbeta212 + J32L*LDPDbeta213;
      
      PDPDbeta312 = J12L*LDPDbeta311 + J22L*LDPDbeta312 + J32L*LDPDbeta313;
      
      PDPDbeta113 = J13L*LDPDbeta111 + J23L*LDPDbeta112 + J33L*LDPDbeta113;
      
      PDPDbeta213 = J13L*LDPDbeta211 + J23L*LDPDbeta212 + J33L*LDPDbeta213;
      
      PDPDbeta313 = J13L*LDPDbeta311 + J23L*LDPDbeta312 + J33L*LDPDbeta313;
      
      PDPDbeta121 = J11L*LDPDbeta121 + J21L*LDPDbeta122 + J31L*LDPDbeta123;
      
      PDPDbeta221 = J11L*LDPDbeta221 + J21L*LDPDbeta222 + J31L*LDPDbeta223;
      
      PDPDbeta321 = J11L*LDPDbeta321 + J21L*LDPDbeta322 + J31L*LDPDbeta323;
      
      PDPDbeta122 = J12L*LDPDbeta121 + J22L*LDPDbeta122 + J32L*LDPDbeta123;
      
      PDPDbeta222 = J12L*LDPDbeta221 + J22L*LDPDbeta222 + J32L*LDPDbeta223;
      
      PDPDbeta322 = J12L*LDPDbeta321 + J22L*LDPDbeta322 + J32L*LDPDbeta323;
      
      PDPDbeta123 = J13L*LDPDbeta121 + J23L*LDPDbeta122 + J33L*LDPDbeta123;
      
      PDPDbeta223 = J13L*LDPDbeta221 + J23L*LDPDbeta222 + J33L*LDPDbeta223;
      
      PDPDbeta323 = J13L*LDPDbeta321 + J23L*LDPDbeta322 + J33L*LDPDbeta323;
      
      PDPDbeta131 = J11L*LDPDbeta131 + J21L*LDPDbeta132 + J31L*LDPDbeta133;
      
      PDPDbeta231 = J11L*LDPDbeta231 + J21L*LDPDbeta232 + J31L*LDPDbeta233;
      
      PDPDbeta331 = J11L*LDPDbeta331 + J21L*LDPDbeta332 + J31L*LDPDbeta333;
      
      PDPDbeta132 = J12L*LDPDbeta131 + J22L*LDPDbeta132 + J32L*LDPDbeta133;
      
      PDPDbeta232 = J12L*LDPDbeta231 + J22L*LDPDbeta232 + J32L*LDPDbeta233;
      
      PDPDbeta332 = J12L*LDPDbeta331 + J22L*LDPDbeta332 + J32L*LDPDbeta333;
      
      PDPDbeta133 = J13L*LDPDbeta131 + J23L*LDPDbeta132 + J33L*LDPDbeta133;
      
      PDPDbeta233 = J13L*LDPDbeta231 + J23L*LDPDbeta232 + J33L*LDPDbeta233;
      
      PDPDbeta333 = J13L*LDPDbeta331 + J23L*LDPDbeta332 + J33L*LDPDbeta333;
    }
    else
    {
      PDPDphiW11 = LDPDphiW11;
      
      PDPDphiW12 = LDPDphiW12;
      
      PDPDphiW13 = LDPDphiW13;
      
      PDPDphiW21 = LDPDphiW21;
      
      PDPDphiW22 = LDPDphiW22;
      
      PDPDphiW23 = LDPDphiW23;
      
      PDPDphiW31 = LDPDphiW31;
      
      PDPDphiW32 = LDPDphiW32;
      
      PDPDphiW33 = LDPDphiW33;
      
      PDPDgt1111 = LDPDgt1111;
      
      PDPDgt1112 = LDPDgt1112;
      
      PDPDgt1113 = LDPDgt1113;
      
      PDPDgt1121 = LDPDgt1121;
      
      PDPDgt1122 = LDPDgt1122;
      
      PDPDgt1123 = LDPDgt1123;
      
      PDPDgt1131 = LDPDgt1131;
      
      PDPDgt1132 = LDPDgt1132;
      
      PDPDgt1133 = LDPDgt1133;
      
      PDPDgt1211 = LDPDgt1211;
      
      PDPDgt1212 = LDPDgt1212;
      
      PDPDgt1213 = LDPDgt1213;
      
      PDPDgt1221 = LDPDgt1221;
      
      PDPDgt1222 = LDPDgt1222;
      
      PDPDgt1223 = LDPDgt1223;
      
      PDPDgt1231 = LDPDgt1231;
      
      PDPDgt1232 = LDPDgt1232;
      
      PDPDgt1233 = LDPDgt1233;
      
      PDPDgt1311 = LDPDgt1311;
      
      PDPDgt1312 = LDPDgt1312;
      
      PDPDgt1313 = LDPDgt1313;
      
      PDPDgt1321 = LDPDgt1321;
      
      PDPDgt1322 = LDPDgt1322;
      
      PDPDgt1323 = LDPDgt1323;
      
      PDPDgt1331 = LDPDgt1331;
      
      PDPDgt1332 = LDPDgt1332;
      
      PDPDgt1333 = LDPDgt1333;
      
      PDPDgt2211 = LDPDgt2211;
      
      PDPDgt2212 = LDPDgt2212;
      
      PDPDgt2213 = LDPDgt2213;
      
      PDPDgt2221 = LDPDgt2221;
      
      PDPDgt2222 = LDPDgt2222;
      
      PDPDgt2223 = LDPDgt2223;
      
      PDPDgt2231 = LDPDgt2231;
      
      PDPDgt2232 = LDPDgt2232;
      
      PDPDgt2233 = LDPDgt2233;
      
      PDPDgt2311 = LDPDgt2311;
      
      PDPDgt2312 = LDPDgt2312;
      
      PDPDgt2313 = LDPDgt2313;
      
      PDPDgt2321 = LDPDgt2321;
      
      PDPDgt2322 = LDPDgt2322;
      
      PDPDgt2323 = LDPDgt2323;
      
      PDPDgt2331 = LDPDgt2331;
      
      PDPDgt2332 = LDPDgt2332;
      
      PDPDgt2333 = LDPDgt2333;
      
      PDPDgt3311 = LDPDgt3311;
      
      PDPDgt3312 = LDPDgt3312;
      
      PDPDgt3313 = LDPDgt3313;
      
      PDPDgt3321 = LDPDgt3321;
      
      PDPDgt3322 = LDPDgt3322;
      
      PDPDgt3323 = LDPDgt3323;
      
      PDPDgt3331 = LDPDgt3331;
      
      PDPDgt3332 = LDPDgt3332;
      
      PDPDgt3333 = LDPDgt3333;
      
      PDPDalpha11 = LDPDalpha11;
      
      PDPDalpha12 = LDPDalpha12;
      
      PDPDalpha13 = LDPDalpha13;
      
      PDPDalpha21 = LDPDalpha21;
      
      PDPDalpha22 = LDPDalpha22;
      
      PDPDalpha23 = LDPDalpha23;
      
      PDPDalpha31 = LDPDalpha31;
      
      PDPDalpha32 = LDPDalpha32;
      
      PDPDalpha33 = LDPDalpha33;
      
      PDPDbeta111 = LDPDbeta111;
      
      PDPDbeta211 = LDPDbeta211;
      
      PDPDbeta311 = LDPDbeta311;
      
      PDPDbeta112 = LDPDbeta112;
      
      PDPDbeta212 = LDPDbeta212;
      
      PDPDbeta312 = LDPDbeta312;
      
      PDPDbeta113 = LDPDbeta113;
      
      PDPDbeta213 = LDPDbeta213;
      
      PDPDbeta313 = LDPDbeta313;
      
      PDPDbeta121 = LDPDbeta121;
      
      PDPDbeta221 = LDPDbeta221;
      
      PDPDbeta321 = LDPDbeta321;
      
      PDPDbeta122 = LDPDbeta122;
      
      PDPDbeta222 = LDPDbeta222;
      
      PDPDbeta322 = LDPDbeta322;
      
      PDPDbeta123 = LDPDbeta123;
      
      PDPDbeta223 = LDPDbeta223;
      
      PDPDbeta323 = LDPDbeta323;
      
      PDPDbeta131 = LDPDbeta131;
      
      PDPDbeta231 = LDPDbeta231;
      
      PDPDbeta331 = LDPDbeta331;
      
      PDPDbeta132 = LDPDbeta132;
      
      PDPDbeta232 = LDPDbeta232;
      
      PDPDbeta332 = LDPDbeta332;
      
      PDPDbeta133 = LDPDbeta133;
      
      PDPDbeta233 = LDPDbeta233;
      
      PDPDbeta333 = LDPDbeta333;
    }
    
    CCTK_REAL LDuphiW1 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(phiW,-2,0,0) - 
      8*GFOffset(phiW,-1,0,0) + 8*GFOffset(phiW,1,0,0) - 
      GFOffset(phiW,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuphiW2 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(phiW,0,-2,0) - 
      8*GFOffset(phiW,0,-1,0) + 8*GFOffset(phiW,0,1,0) - 
      GFOffset(phiW,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuphiW3 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(phiW,0,0,-2) - 
      8*GFOffset(phiW,0,0,-1) + 8*GFOffset(phiW,0,0,1) - 
      GFOffset(phiW,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDugt111 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt11,-2,0,0) - 
      8*GFOffset(gt11,-1,0,0) + 8*GFOffset(gt11,1,0,0) - 
      GFOffset(gt11,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDugt112 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt11,0,-2,0) - 
      8*GFOffset(gt11,0,-1,0) + 8*GFOffset(gt11,0,1,0) - 
      GFOffset(gt11,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDugt113 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt11,0,0,-2) - 
      8*GFOffset(gt11,0,0,-1) + 8*GFOffset(gt11,0,0,1) - 
      GFOffset(gt11,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDugt121 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt12,-2,0,0) - 
      8*GFOffset(gt12,-1,0,0) + 8*GFOffset(gt12,1,0,0) - 
      GFOffset(gt12,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDugt122 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt12,0,-2,0) - 
      8*GFOffset(gt12,0,-1,0) + 8*GFOffset(gt12,0,1,0) - 
      GFOffset(gt12,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDugt123 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt12,0,0,-2) - 
      8*GFOffset(gt12,0,0,-1) + 8*GFOffset(gt12,0,0,1) - 
      GFOffset(gt12,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDugt131 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt13,-2,0,0) - 
      8*GFOffset(gt13,-1,0,0) + 8*GFOffset(gt13,1,0,0) - 
      GFOffset(gt13,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDugt132 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt13,0,-2,0) - 
      8*GFOffset(gt13,0,-1,0) + 8*GFOffset(gt13,0,1,0) - 
      GFOffset(gt13,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDugt133 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt13,0,0,-2) - 
      8*GFOffset(gt13,0,0,-1) + 8*GFOffset(gt13,0,0,1) - 
      GFOffset(gt13,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDugt221 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt22,-2,0,0) - 
      8*GFOffset(gt22,-1,0,0) + 8*GFOffset(gt22,1,0,0) - 
      GFOffset(gt22,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDugt222 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt22,0,-2,0) - 
      8*GFOffset(gt22,0,-1,0) + 8*GFOffset(gt22,0,1,0) - 
      GFOffset(gt22,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDugt223 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt22,0,0,-2) - 
      8*GFOffset(gt22,0,0,-1) + 8*GFOffset(gt22,0,0,1) - 
      GFOffset(gt22,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDugt231 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt23,-2,0,0) - 
      8*GFOffset(gt23,-1,0,0) + 8*GFOffset(gt23,1,0,0) - 
      GFOffset(gt23,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDugt232 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt23,0,-2,0) - 
      8*GFOffset(gt23,0,-1,0) + 8*GFOffset(gt23,0,1,0) - 
      GFOffset(gt23,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDugt233 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt23,0,0,-2) - 
      8*GFOffset(gt23,0,0,-1) + 8*GFOffset(gt23,0,0,1) - 
      GFOffset(gt23,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDugt331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt33,-2,0,0) - 
      8*GFOffset(gt33,-1,0,0) + 8*GFOffset(gt33,1,0,0) - 
      GFOffset(gt33,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDugt332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt33,0,-2,0) - 
      8*GFOffset(gt33,0,-1,0) + 8*GFOffset(gt33,0,1,0) - 
      GFOffset(gt33,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDugt333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(gt33,0,0,-2) - 
      8*GFOffset(gt33,0,0,-1) + 8*GFOffset(gt33,0,0,1) - 
      GFOffset(gt33,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuXt11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,-2,0,0) - 
      8*GFOffset(Xt1,-1,0,0) + 8*GFOffset(Xt1,1,0,0) - 
      GFOffset(Xt1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuXt21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,-2,0,0) - 
      8*GFOffset(Xt2,-1,0,0) + 8*GFOffset(Xt2,1,0,0) - 
      GFOffset(Xt2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuXt31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,-2,0,0) - 
      8*GFOffset(Xt3,-1,0,0) + 8*GFOffset(Xt3,1,0,0) - 
      GFOffset(Xt3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuXt12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,0,-2,0) - 
      8*GFOffset(Xt1,0,-1,0) + 8*GFOffset(Xt1,0,1,0) - 
      GFOffset(Xt1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuXt22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,0,-2,0) - 
      8*GFOffset(Xt2,0,-1,0) + 8*GFOffset(Xt2,0,1,0) - 
      GFOffset(Xt2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuXt32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,0,-2,0) - 
      8*GFOffset(Xt3,0,-1,0) + 8*GFOffset(Xt3,0,1,0) - 
      GFOffset(Xt3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuXt13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt1,0,0,-2) - 
      8*GFOffset(Xt1,0,0,-1) + 8*GFOffset(Xt1,0,0,1) - 
      GFOffset(Xt1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuXt23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt2,0,0,-2) - 
      8*GFOffset(Xt2,0,0,-1) + 8*GFOffset(Xt2,0,0,1) - 
      GFOffset(Xt2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuXt33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(Xt3,0,0,-2) - 
      8*GFOffset(Xt3,0,0,-1) + 8*GFOffset(Xt3,0,0,1) - 
      GFOffset(Xt3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDutrK1 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,-2,0,0) - 
      8*GFOffset(trK,-1,0,0) + 8*GFOffset(trK,1,0,0) - 
      GFOffset(trK,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDutrK2 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,0,-2,0) - 
      8*GFOffset(trK,0,-1,0) + 8*GFOffset(trK,0,1,0) - 
      GFOffset(trK,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDutrK3 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(trK,0,0,-2) - 
      8*GFOffset(trK,0,0,-1) + 8*GFOffset(trK,0,0,1) - 
      GFOffset(trK,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuAt111 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At11,-2,0,0) - 
      8*GFOffset(At11,-1,0,0) + 8*GFOffset(At11,1,0,0) - 
      GFOffset(At11,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuAt112 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At11,0,-2,0) - 
      8*GFOffset(At11,0,-1,0) + 8*GFOffset(At11,0,1,0) - 
      GFOffset(At11,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuAt113 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At11,0,0,-2) - 
      8*GFOffset(At11,0,0,-1) + 8*GFOffset(At11,0,0,1) - 
      GFOffset(At11,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuAt121 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At12,-2,0,0) - 
      8*GFOffset(At12,-1,0,0) + 8*GFOffset(At12,1,0,0) - 
      GFOffset(At12,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuAt122 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At12,0,-2,0) - 
      8*GFOffset(At12,0,-1,0) + 8*GFOffset(At12,0,1,0) - 
      GFOffset(At12,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuAt123 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At12,0,0,-2) - 
      8*GFOffset(At12,0,0,-1) + 8*GFOffset(At12,0,0,1) - 
      GFOffset(At12,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuAt131 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At13,-2,0,0) - 
      8*GFOffset(At13,-1,0,0) + 8*GFOffset(At13,1,0,0) - 
      GFOffset(At13,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuAt132 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At13,0,-2,0) - 
      8*GFOffset(At13,0,-1,0) + 8*GFOffset(At13,0,1,0) - 
      GFOffset(At13,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuAt133 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At13,0,0,-2) - 
      8*GFOffset(At13,0,0,-1) + 8*GFOffset(At13,0,0,1) - 
      GFOffset(At13,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuAt221 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At22,-2,0,0) - 
      8*GFOffset(At22,-1,0,0) + 8*GFOffset(At22,1,0,0) - 
      GFOffset(At22,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuAt222 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At22,0,-2,0) - 
      8*GFOffset(At22,0,-1,0) + 8*GFOffset(At22,0,1,0) - 
      GFOffset(At22,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuAt223 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At22,0,0,-2) - 
      8*GFOffset(At22,0,0,-1) + 8*GFOffset(At22,0,0,1) - 
      GFOffset(At22,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuAt231 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At23,-2,0,0) - 
      8*GFOffset(At23,-1,0,0) + 8*GFOffset(At23,1,0,0) - 
      GFOffset(At23,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuAt232 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At23,0,-2,0) - 
      8*GFOffset(At23,0,-1,0) + 8*GFOffset(At23,0,1,0) - 
      GFOffset(At23,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuAt233 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At23,0,0,-2) - 
      8*GFOffset(At23,0,0,-1) + 8*GFOffset(At23,0,0,1) - 
      GFOffset(At23,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuAt331 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At33,-2,0,0) - 
      8*GFOffset(At33,-1,0,0) + 8*GFOffset(At33,1,0,0) - 
      GFOffset(At33,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuAt332 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At33,0,-2,0) - 
      8*GFOffset(At33,0,-1,0) + 8*GFOffset(At33,0,1,0) - 
      GFOffset(At33,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuAt333 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(At33,0,0,-2) - 
      8*GFOffset(At33,0,0,-1) + 8*GFOffset(At33,0,0,1) - 
      GFOffset(At33,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDualpha1 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(alpha,-2,0,0) - 
      8*GFOffset(alpha,-1,0,0) + 8*GFOffset(alpha,1,0,0) - 
      GFOffset(alpha,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDualpha2 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(alpha,0,-2,0) - 
      8*GFOffset(alpha,0,-1,0) + 8*GFOffset(alpha,0,1,0) - 
      GFOffset(alpha,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDualpha3 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(alpha,0,0,-2) - 
      8*GFOffset(alpha,0,0,-1) + 8*GFOffset(alpha,0,0,1) - 
      GFOffset(alpha,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuA1 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(A,-2,0,0) - 
      8*GFOffset(A,-1,0,0) + 8*GFOffset(A,1,0,0) - 
      GFOffset(A,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuA2 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(A,0,-2,0) - 
      8*GFOffset(A,0,-1,0) + 8*GFOffset(A,0,1,0) - 
      GFOffset(A,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuA3 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(A,0,0,-2) - 
      8*GFOffset(A,0,0,-1) + 8*GFOffset(A,0,0,1) - 
      GFOffset(A,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDubeta11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta1,-2,0,0) - 
      8*GFOffset(beta1,-1,0,0) + 8*GFOffset(beta1,1,0,0) - 
      GFOffset(beta1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDubeta21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta2,-2,0,0) - 
      8*GFOffset(beta2,-1,0,0) + 8*GFOffset(beta2,1,0,0) - 
      GFOffset(beta2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDubeta31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta3,-2,0,0) - 
      8*GFOffset(beta3,-1,0,0) + 8*GFOffset(beta3,1,0,0) - 
      GFOffset(beta3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDubeta12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta1,0,-2,0) - 
      8*GFOffset(beta1,0,-1,0) + 8*GFOffset(beta1,0,1,0) - 
      GFOffset(beta1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDubeta22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta2,0,-2,0) - 
      8*GFOffset(beta2,0,-1,0) + 8*GFOffset(beta2,0,1,0) - 
      GFOffset(beta2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDubeta32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta3,0,-2,0) - 
      8*GFOffset(beta3,0,-1,0) + 8*GFOffset(beta3,0,1,0) - 
      GFOffset(beta3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDubeta13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta1,0,0,-2) - 
      8*GFOffset(beta1,0,0,-1) + 8*GFOffset(beta1,0,0,1) - 
      GFOffset(beta1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDubeta23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta2,0,0,-2) - 
      8*GFOffset(beta2,0,0,-1) + 8*GFOffset(beta2,0,0,1) - 
      GFOffset(beta2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDubeta33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(beta3,0,0,-2) - 
      8*GFOffset(beta3,0,0,-1) + 8*GFOffset(beta3,0,0,1) - 
      GFOffset(beta3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuB11 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B1,-2,0,0) - 
      8*GFOffset(B1,-1,0,0) + 8*GFOffset(B1,1,0,0) - 
      GFOffset(B1,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuB21 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B2,-2,0,0) - 
      8*GFOffset(B2,-1,0,0) + 8*GFOffset(B2,1,0,0) - 
      GFOffset(B2,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuB31 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B3,-2,0,0) - 
      8*GFOffset(B3,-1,0,0) + 8*GFOffset(B3,1,0,0) - 
      GFOffset(B3,2,0,0))*pow(dx,-1);
    
    CCTK_REAL LDuB12 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B1,0,-2,0) - 
      8*GFOffset(B1,0,-1,0) + 8*GFOffset(B1,0,1,0) - 
      GFOffset(B1,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuB22 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B2,0,-2,0) - 
      8*GFOffset(B2,0,-1,0) + 8*GFOffset(B2,0,1,0) - 
      GFOffset(B2,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuB32 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B3,0,-2,0) - 
      8*GFOffset(B3,0,-1,0) + 8*GFOffset(B3,0,1,0) - 
      GFOffset(B3,0,2,0))*pow(dy,-1);
    
    CCTK_REAL LDuB13 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B1,0,0,-2) - 
      8*GFOffset(B1,0,0,-1) + 8*GFOffset(B1,0,0,1) - 
      GFOffset(B1,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuB23 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B2,0,0,-2) - 
      8*GFOffset(B2,0,0,-1) + 8*GFOffset(B2,0,0,1) - 
      GFOffset(B2,0,0,2))*pow(dz,-1);
    
    CCTK_REAL LDuB33 CCTK_ATTRIBUTE_UNUSED = 
      0.0833333333333333333333333333333*(GFOffset(B3,0,0,-2) - 
      8*GFOffset(B3,0,0,-1) + 8*GFOffset(B3,0,0,1) - 
      GFOffset(B3,0,0,2))*pow(dz,-1);
    
    CCTK_REAL PDuA1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuA2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuA3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualpha1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualpha2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualpha3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt111 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt112 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt113 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt121 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt122 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt123 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt131 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt132 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt133 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuB33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt111 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt112 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt113 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt121 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt122 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt123 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt131 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt132 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt133 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuphiW1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuphiW2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuphiW3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDutrK1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDutrK2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDutrK3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuXt33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDuphiW1 = J11L*LDuphiW1 + J21L*LDuphiW2 + J31L*LDuphiW3;
      
      PDuphiW2 = J12L*LDuphiW1 + J22L*LDuphiW2 + J32L*LDuphiW3;
      
      PDuphiW3 = J13L*LDuphiW1 + J23L*LDuphiW2 + J33L*LDuphiW3;
      
      PDugt111 = J11L*LDugt111 + J21L*LDugt112 + J31L*LDugt113;
      
      PDugt112 = J12L*LDugt111 + J22L*LDugt112 + J32L*LDugt113;
      
      PDugt113 = J13L*LDugt111 + J23L*LDugt112 + J33L*LDugt113;
      
      PDugt121 = J11L*LDugt121 + J21L*LDugt122 + J31L*LDugt123;
      
      PDugt122 = J12L*LDugt121 + J22L*LDugt122 + J32L*LDugt123;
      
      PDugt123 = J13L*LDugt121 + J23L*LDugt122 + J33L*LDugt123;
      
      PDugt131 = J11L*LDugt131 + J21L*LDugt132 + J31L*LDugt133;
      
      PDugt132 = J12L*LDugt131 + J22L*LDugt132 + J32L*LDugt133;
      
      PDugt133 = J13L*LDugt131 + J23L*LDugt132 + J33L*LDugt133;
      
      PDugt221 = J11L*LDugt221 + J21L*LDugt222 + J31L*LDugt223;
      
      PDugt222 = J12L*LDugt221 + J22L*LDugt222 + J32L*LDugt223;
      
      PDugt223 = J13L*LDugt221 + J23L*LDugt222 + J33L*LDugt223;
      
      PDugt231 = J11L*LDugt231 + J21L*LDugt232 + J31L*LDugt233;
      
      PDugt232 = J12L*LDugt231 + J22L*LDugt232 + J32L*LDugt233;
      
      PDugt233 = J13L*LDugt231 + J23L*LDugt232 + J33L*LDugt233;
      
      PDugt331 = J11L*LDugt331 + J21L*LDugt332 + J31L*LDugt333;
      
      PDugt332 = J12L*LDugt331 + J22L*LDugt332 + J32L*LDugt333;
      
      PDugt333 = J13L*LDugt331 + J23L*LDugt332 + J33L*LDugt333;
      
      PDuXt11 = J11L*LDuXt11 + J21L*LDuXt12 + J31L*LDuXt13;
      
      PDuXt21 = J11L*LDuXt21 + J21L*LDuXt22 + J31L*LDuXt23;
      
      PDuXt31 = J11L*LDuXt31 + J21L*LDuXt32 + J31L*LDuXt33;
      
      PDuXt12 = J12L*LDuXt11 + J22L*LDuXt12 + J32L*LDuXt13;
      
      PDuXt22 = J12L*LDuXt21 + J22L*LDuXt22 + J32L*LDuXt23;
      
      PDuXt32 = J12L*LDuXt31 + J22L*LDuXt32 + J32L*LDuXt33;
      
      PDuXt13 = J13L*LDuXt11 + J23L*LDuXt12 + J33L*LDuXt13;
      
      PDuXt23 = J13L*LDuXt21 + J23L*LDuXt22 + J33L*LDuXt23;
      
      PDuXt33 = J13L*LDuXt31 + J23L*LDuXt32 + J33L*LDuXt33;
      
      PDutrK1 = J11L*LDutrK1 + J21L*LDutrK2 + J31L*LDutrK3;
      
      PDutrK2 = J12L*LDutrK1 + J22L*LDutrK2 + J32L*LDutrK3;
      
      PDutrK3 = J13L*LDutrK1 + J23L*LDutrK2 + J33L*LDutrK3;
      
      PDuAt111 = J11L*LDuAt111 + J21L*LDuAt112 + J31L*LDuAt113;
      
      PDuAt112 = J12L*LDuAt111 + J22L*LDuAt112 + J32L*LDuAt113;
      
      PDuAt113 = J13L*LDuAt111 + J23L*LDuAt112 + J33L*LDuAt113;
      
      PDuAt121 = J11L*LDuAt121 + J21L*LDuAt122 + J31L*LDuAt123;
      
      PDuAt122 = J12L*LDuAt121 + J22L*LDuAt122 + J32L*LDuAt123;
      
      PDuAt123 = J13L*LDuAt121 + J23L*LDuAt122 + J33L*LDuAt123;
      
      PDuAt131 = J11L*LDuAt131 + J21L*LDuAt132 + J31L*LDuAt133;
      
      PDuAt132 = J12L*LDuAt131 + J22L*LDuAt132 + J32L*LDuAt133;
      
      PDuAt133 = J13L*LDuAt131 + J23L*LDuAt132 + J33L*LDuAt133;
      
      PDuAt221 = J11L*LDuAt221 + J21L*LDuAt222 + J31L*LDuAt223;
      
      PDuAt222 = J12L*LDuAt221 + J22L*LDuAt222 + J32L*LDuAt223;
      
      PDuAt223 = J13L*LDuAt221 + J23L*LDuAt222 + J33L*LDuAt223;
      
      PDuAt231 = J11L*LDuAt231 + J21L*LDuAt232 + J31L*LDuAt233;
      
      PDuAt232 = J12L*LDuAt231 + J22L*LDuAt232 + J32L*LDuAt233;
      
      PDuAt233 = J13L*LDuAt231 + J23L*LDuAt232 + J33L*LDuAt233;
      
      PDuAt331 = J11L*LDuAt331 + J21L*LDuAt332 + J31L*LDuAt333;
      
      PDuAt332 = J12L*LDuAt331 + J22L*LDuAt332 + J32L*LDuAt333;
      
      PDuAt333 = J13L*LDuAt331 + J23L*LDuAt332 + J33L*LDuAt333;
      
      PDualpha1 = J11L*LDualpha1 + J21L*LDualpha2 + J31L*LDualpha3;
      
      PDualpha2 = J12L*LDualpha1 + J22L*LDualpha2 + J32L*LDualpha3;
      
      PDualpha3 = J13L*LDualpha1 + J23L*LDualpha2 + J33L*LDualpha3;
      
      PDuA1 = J11L*LDuA1 + J21L*LDuA2 + J31L*LDuA3;
      
      PDuA2 = J12L*LDuA1 + J22L*LDuA2 + J32L*LDuA3;
      
      PDuA3 = J13L*LDuA1 + J23L*LDuA2 + J33L*LDuA3;
      
      PDubeta11 = J11L*LDubeta11 + J21L*LDubeta12 + J31L*LDubeta13;
      
      PDubeta21 = J11L*LDubeta21 + J21L*LDubeta22 + J31L*LDubeta23;
      
      PDubeta31 = J11L*LDubeta31 + J21L*LDubeta32 + J31L*LDubeta33;
      
      PDubeta12 = J12L*LDubeta11 + J22L*LDubeta12 + J32L*LDubeta13;
      
      PDubeta22 = J12L*LDubeta21 + J22L*LDubeta22 + J32L*LDubeta23;
      
      PDubeta32 = J12L*LDubeta31 + J22L*LDubeta32 + J32L*LDubeta33;
      
      PDubeta13 = J13L*LDubeta11 + J23L*LDubeta12 + J33L*LDubeta13;
      
      PDubeta23 = J13L*LDubeta21 + J23L*LDubeta22 + J33L*LDubeta23;
      
      PDubeta33 = J13L*LDubeta31 + J23L*LDubeta32 + J33L*LDubeta33;
      
      PDuB11 = J11L*LDuB11 + J21L*LDuB12 + J31L*LDuB13;
      
      PDuB21 = J11L*LDuB21 + J21L*LDuB22 + J31L*LDuB23;
      
      PDuB31 = J11L*LDuB31 + J21L*LDuB32 + J31L*LDuB33;
      
      PDuB12 = J12L*LDuB11 + J22L*LDuB12 + J32L*LDuB13;
      
      PDuB22 = J12L*LDuB21 + J22L*LDuB22 + J32L*LDuB23;
      
      PDuB32 = J12L*LDuB31 + J22L*LDuB32 + J32L*LDuB33;
      
      PDuB13 = J13L*LDuB11 + J23L*LDuB12 + J33L*LDuB13;
      
      PDuB23 = J13L*LDuB21 + J23L*LDuB22 + J33L*LDuB23;
      
      PDuB33 = J13L*LDuB31 + J23L*LDuB32 + J33L*LDuB33;
    }
    else
    {
      PDuphiW1 = LDuphiW1;
      
      PDuphiW2 = LDuphiW2;
      
      PDuphiW3 = LDuphiW3;
      
      PDugt111 = LDugt111;
      
      PDugt112 = LDugt112;
      
      PDugt113 = LDugt113;
      
      PDugt121 = LDugt121;
      
      PDugt122 = LDugt122;
      
      PDugt123 = LDugt123;
      
      PDugt131 = LDugt131;
      
      PDugt132 = LDugt132;
      
      PDugt133 = LDugt133;
      
      PDugt221 = LDugt221;
      
      PDugt222 = LDugt222;
      
      PDugt223 = LDugt223;
      
      PDugt231 = LDugt231;
      
      PDugt232 = LDugt232;
      
      PDugt233 = LDugt233;
      
      PDugt331 = LDugt331;
      
      PDugt332 = LDugt332;
      
      PDugt333 = LDugt333;
      
      PDuXt11 = LDuXt11;
      
      PDuXt21 = LDuXt21;
      
      PDuXt31 = LDuXt31;
      
      PDuXt12 = LDuXt12;
      
      PDuXt22 = LDuXt22;
      
      PDuXt32 = LDuXt32;
      
      PDuXt13 = LDuXt13;
      
      PDuXt23 = LDuXt23;
      
      PDuXt33 = LDuXt33;
      
      PDutrK1 = LDutrK1;
      
      PDutrK2 = LDutrK2;
      
      PDutrK3 = LDutrK3;
      
      PDuAt111 = LDuAt111;
      
      PDuAt112 = LDuAt112;
      
      PDuAt113 = LDuAt113;
      
      PDuAt121 = LDuAt121;
      
      PDuAt122 = LDuAt122;
      
      PDuAt123 = LDuAt123;
      
      PDuAt131 = LDuAt131;
      
      PDuAt132 = LDuAt132;
      
      PDuAt133 = LDuAt133;
      
      PDuAt221 = LDuAt221;
      
      PDuAt222 = LDuAt222;
      
      PDuAt223 = LDuAt223;
      
      PDuAt231 = LDuAt231;
      
      PDuAt232 = LDuAt232;
      
      PDuAt233 = LDuAt233;
      
      PDuAt331 = LDuAt331;
      
      PDuAt332 = LDuAt332;
      
      PDuAt333 = LDuAt333;
      
      PDualpha1 = LDualpha1;
      
      PDualpha2 = LDualpha2;
      
      PDualpha3 = LDualpha3;
      
      PDuA1 = LDuA1;
      
      PDuA2 = LDuA2;
      
      PDuA3 = LDuA3;
      
      PDubeta11 = LDubeta11;
      
      PDubeta21 = LDubeta21;
      
      PDubeta31 = LDubeta31;
      
      PDubeta12 = LDubeta12;
      
      PDubeta22 = LDubeta22;
      
      PDubeta32 = LDubeta32;
      
      PDubeta13 = LDubeta13;
      
      PDubeta23 = LDubeta23;
      
      PDubeta33 = LDubeta33;
      
      PDuB11 = LDuB11;
      
      PDuB21 = LDuB21;
      
      PDuB31 = LDuB31;
      
      PDuB12 = LDuB12;
      
      PDuB22 = LDuB22;
      
      PDuB32 = LDuB32;
      
      PDuB13 = LDuB13;
      
      PDuB23 = LDuB23;
      
      PDuB33 = LDuB33;
    }
    
    CCTK_REAL LDissphiW CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(phiW,-3,0,0) - 6*GFOffset(phiW,-2,0,0) + 
      15*GFOffset(phiW,-1,0,0) - 20*GFOffset(phiW,0,0,0) + 
      15*GFOffset(phiW,1,0,0) - 6*GFOffset(phiW,2,0,0) + 
      GFOffset(phiW,3,0,0))*pow(dx,-1) + (GFOffset(phiW,0,-3,0) - 
      6*GFOffset(phiW,0,-2,0) + 15*GFOffset(phiW,0,-1,0) - 
      20*GFOffset(phiW,0,0,0) + 15*GFOffset(phiW,0,1,0) - 
      6*GFOffset(phiW,0,2,0) + GFOffset(phiW,0,3,0))*pow(dy,-1) + 
      (GFOffset(phiW,0,0,-3) - 6*GFOffset(phiW,0,0,-2) + 
      15*GFOffset(phiW,0,0,-1) - 20*GFOffset(phiW,0,0,0) + 
      15*GFOffset(phiW,0,0,1) - 6*GFOffset(phiW,0,0,2) + 
      GFOffset(phiW,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissphiW CCTK_ATTRIBUTE_UNUSED = 
      IfThen(usejacobian,myDetJL*LDissphiW,LDissphiW);
    
    CCTK_REAL LDissgt11 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(gt11,-3,0,0) - 6*GFOffset(gt11,-2,0,0) + 
      15*GFOffset(gt11,-1,0,0) - 20*GFOffset(gt11,0,0,0) + 
      15*GFOffset(gt11,1,0,0) - 6*GFOffset(gt11,2,0,0) + 
      GFOffset(gt11,3,0,0))*pow(dx,-1) + (GFOffset(gt11,0,-3,0) - 
      6*GFOffset(gt11,0,-2,0) + 15*GFOffset(gt11,0,-1,0) - 
      20*GFOffset(gt11,0,0,0) + 15*GFOffset(gt11,0,1,0) - 
      6*GFOffset(gt11,0,2,0) + GFOffset(gt11,0,3,0))*pow(dy,-1) + 
      (GFOffset(gt11,0,0,-3) - 6*GFOffset(gt11,0,0,-2) + 
      15*GFOffset(gt11,0,0,-1) - 20*GFOffset(gt11,0,0,0) + 
      15*GFOffset(gt11,0,0,1) - 6*GFOffset(gt11,0,0,2) + 
      GFOffset(gt11,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissgt12 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(gt12,-3,0,0) - 6*GFOffset(gt12,-2,0,0) + 
      15*GFOffset(gt12,-1,0,0) - 20*GFOffset(gt12,0,0,0) + 
      15*GFOffset(gt12,1,0,0) - 6*GFOffset(gt12,2,0,0) + 
      GFOffset(gt12,3,0,0))*pow(dx,-1) + (GFOffset(gt12,0,-3,0) - 
      6*GFOffset(gt12,0,-2,0) + 15*GFOffset(gt12,0,-1,0) - 
      20*GFOffset(gt12,0,0,0) + 15*GFOffset(gt12,0,1,0) - 
      6*GFOffset(gt12,0,2,0) + GFOffset(gt12,0,3,0))*pow(dy,-1) + 
      (GFOffset(gt12,0,0,-3) - 6*GFOffset(gt12,0,0,-2) + 
      15*GFOffset(gt12,0,0,-1) - 20*GFOffset(gt12,0,0,0) + 
      15*GFOffset(gt12,0,0,1) - 6*GFOffset(gt12,0,0,2) + 
      GFOffset(gt12,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissgt13 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(gt13,-3,0,0) - 6*GFOffset(gt13,-2,0,0) + 
      15*GFOffset(gt13,-1,0,0) - 20*GFOffset(gt13,0,0,0) + 
      15*GFOffset(gt13,1,0,0) - 6*GFOffset(gt13,2,0,0) + 
      GFOffset(gt13,3,0,0))*pow(dx,-1) + (GFOffset(gt13,0,-3,0) - 
      6*GFOffset(gt13,0,-2,0) + 15*GFOffset(gt13,0,-1,0) - 
      20*GFOffset(gt13,0,0,0) + 15*GFOffset(gt13,0,1,0) - 
      6*GFOffset(gt13,0,2,0) + GFOffset(gt13,0,3,0))*pow(dy,-1) + 
      (GFOffset(gt13,0,0,-3) - 6*GFOffset(gt13,0,0,-2) + 
      15*GFOffset(gt13,0,0,-1) - 20*GFOffset(gt13,0,0,0) + 
      15*GFOffset(gt13,0,0,1) - 6*GFOffset(gt13,0,0,2) + 
      GFOffset(gt13,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissgt22 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(gt22,-3,0,0) - 6*GFOffset(gt22,-2,0,0) + 
      15*GFOffset(gt22,-1,0,0) - 20*GFOffset(gt22,0,0,0) + 
      15*GFOffset(gt22,1,0,0) - 6*GFOffset(gt22,2,0,0) + 
      GFOffset(gt22,3,0,0))*pow(dx,-1) + (GFOffset(gt22,0,-3,0) - 
      6*GFOffset(gt22,0,-2,0) + 15*GFOffset(gt22,0,-1,0) - 
      20*GFOffset(gt22,0,0,0) + 15*GFOffset(gt22,0,1,0) - 
      6*GFOffset(gt22,0,2,0) + GFOffset(gt22,0,3,0))*pow(dy,-1) + 
      (GFOffset(gt22,0,0,-3) - 6*GFOffset(gt22,0,0,-2) + 
      15*GFOffset(gt22,0,0,-1) - 20*GFOffset(gt22,0,0,0) + 
      15*GFOffset(gt22,0,0,1) - 6*GFOffset(gt22,0,0,2) + 
      GFOffset(gt22,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissgt23 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(gt23,-3,0,0) - 6*GFOffset(gt23,-2,0,0) + 
      15*GFOffset(gt23,-1,0,0) - 20*GFOffset(gt23,0,0,0) + 
      15*GFOffset(gt23,1,0,0) - 6*GFOffset(gt23,2,0,0) + 
      GFOffset(gt23,3,0,0))*pow(dx,-1) + (GFOffset(gt23,0,-3,0) - 
      6*GFOffset(gt23,0,-2,0) + 15*GFOffset(gt23,0,-1,0) - 
      20*GFOffset(gt23,0,0,0) + 15*GFOffset(gt23,0,1,0) - 
      6*GFOffset(gt23,0,2,0) + GFOffset(gt23,0,3,0))*pow(dy,-1) + 
      (GFOffset(gt23,0,0,-3) - 6*GFOffset(gt23,0,0,-2) + 
      15*GFOffset(gt23,0,0,-1) - 20*GFOffset(gt23,0,0,0) + 
      15*GFOffset(gt23,0,0,1) - 6*GFOffset(gt23,0,0,2) + 
      GFOffset(gt23,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissgt33 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(gt33,-3,0,0) - 6*GFOffset(gt33,-2,0,0) + 
      15*GFOffset(gt33,-1,0,0) - 20*GFOffset(gt33,0,0,0) + 
      15*GFOffset(gt33,1,0,0) - 6*GFOffset(gt33,2,0,0) + 
      GFOffset(gt33,3,0,0))*pow(dx,-1) + (GFOffset(gt33,0,-3,0) - 
      6*GFOffset(gt33,0,-2,0) + 15*GFOffset(gt33,0,-1,0) - 
      20*GFOffset(gt33,0,0,0) + 15*GFOffset(gt33,0,1,0) - 
      6*GFOffset(gt33,0,2,0) + GFOffset(gt33,0,3,0))*pow(dy,-1) + 
      (GFOffset(gt33,0,0,-3) - 6*GFOffset(gt33,0,0,-2) + 
      15*GFOffset(gt33,0,0,-1) - 20*GFOffset(gt33,0,0,0) + 
      15*GFOffset(gt33,0,0,1) - 6*GFOffset(gt33,0,0,2) + 
      GFOffset(gt33,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissgt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissgt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissgt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissgt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissgt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissgt33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      GDissgt11 = myDetJL*LDissgt11;
      
      GDissgt12 = myDetJL*LDissgt12;
      
      GDissgt13 = myDetJL*LDissgt13;
      
      GDissgt22 = myDetJL*LDissgt22;
      
      GDissgt23 = myDetJL*LDissgt23;
      
      GDissgt33 = myDetJL*LDissgt33;
    }
    else
    {
      GDissgt11 = LDissgt11;
      
      GDissgt12 = LDissgt12;
      
      GDissgt13 = LDissgt13;
      
      GDissgt22 = LDissgt22;
      
      GDissgt23 = LDissgt23;
      
      GDissgt33 = LDissgt33;
    }
    
    CCTK_REAL LDissXt1 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(Xt1,-3,0,0) - 6*GFOffset(Xt1,-2,0,0) + 
      15*GFOffset(Xt1,-1,0,0) - 20*GFOffset(Xt1,0,0,0) + 
      15*GFOffset(Xt1,1,0,0) - 6*GFOffset(Xt1,2,0,0) + 
      GFOffset(Xt1,3,0,0))*pow(dx,-1) + (GFOffset(Xt1,0,-3,0) - 
      6*GFOffset(Xt1,0,-2,0) + 15*GFOffset(Xt1,0,-1,0) - 
      20*GFOffset(Xt1,0,0,0) + 15*GFOffset(Xt1,0,1,0) - 6*GFOffset(Xt1,0,2,0) 
      + GFOffset(Xt1,0,3,0))*pow(dy,-1) + (GFOffset(Xt1,0,0,-3) - 
      6*GFOffset(Xt1,0,0,-2) + 15*GFOffset(Xt1,0,0,-1) - 
      20*GFOffset(Xt1,0,0,0) + 15*GFOffset(Xt1,0,0,1) - 6*GFOffset(Xt1,0,0,2) 
      + GFOffset(Xt1,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissXt2 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(Xt2,-3,0,0) - 6*GFOffset(Xt2,-2,0,0) + 
      15*GFOffset(Xt2,-1,0,0) - 20*GFOffset(Xt2,0,0,0) + 
      15*GFOffset(Xt2,1,0,0) - 6*GFOffset(Xt2,2,0,0) + 
      GFOffset(Xt2,3,0,0))*pow(dx,-1) + (GFOffset(Xt2,0,-3,0) - 
      6*GFOffset(Xt2,0,-2,0) + 15*GFOffset(Xt2,0,-1,0) - 
      20*GFOffset(Xt2,0,0,0) + 15*GFOffset(Xt2,0,1,0) - 6*GFOffset(Xt2,0,2,0) 
      + GFOffset(Xt2,0,3,0))*pow(dy,-1) + (GFOffset(Xt2,0,0,-3) - 
      6*GFOffset(Xt2,0,0,-2) + 15*GFOffset(Xt2,0,0,-1) - 
      20*GFOffset(Xt2,0,0,0) + 15*GFOffset(Xt2,0,0,1) - 6*GFOffset(Xt2,0,0,2) 
      + GFOffset(Xt2,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissXt3 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(Xt3,-3,0,0) - 6*GFOffset(Xt3,-2,0,0) + 
      15*GFOffset(Xt3,-1,0,0) - 20*GFOffset(Xt3,0,0,0) + 
      15*GFOffset(Xt3,1,0,0) - 6*GFOffset(Xt3,2,0,0) + 
      GFOffset(Xt3,3,0,0))*pow(dx,-1) + (GFOffset(Xt3,0,-3,0) - 
      6*GFOffset(Xt3,0,-2,0) + 15*GFOffset(Xt3,0,-1,0) - 
      20*GFOffset(Xt3,0,0,0) + 15*GFOffset(Xt3,0,1,0) - 6*GFOffset(Xt3,0,2,0) 
      + GFOffset(Xt3,0,3,0))*pow(dy,-1) + (GFOffset(Xt3,0,0,-3) - 
      6*GFOffset(Xt3,0,0,-2) + 15*GFOffset(Xt3,0,0,-1) - 
      20*GFOffset(Xt3,0,0,0) + 15*GFOffset(Xt3,0,0,1) - 6*GFOffset(Xt3,0,0,2) 
      + GFOffset(Xt3,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissXt1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissXt2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissXt3 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      GDissXt1 = myDetJL*LDissXt1;
      
      GDissXt2 = myDetJL*LDissXt2;
      
      GDissXt3 = myDetJL*LDissXt3;
    }
    else
    {
      GDissXt1 = LDissXt1;
      
      GDissXt2 = LDissXt2;
      
      GDissXt3 = LDissXt3;
    }
    
    CCTK_REAL LDisstrK CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(trK,-3,0,0) - 6*GFOffset(trK,-2,0,0) + 
      15*GFOffset(trK,-1,0,0) - 20*GFOffset(trK,0,0,0) + 
      15*GFOffset(trK,1,0,0) - 6*GFOffset(trK,2,0,0) + 
      GFOffset(trK,3,0,0))*pow(dx,-1) + (GFOffset(trK,0,-3,0) - 
      6*GFOffset(trK,0,-2,0) + 15*GFOffset(trK,0,-1,0) - 
      20*GFOffset(trK,0,0,0) + 15*GFOffset(trK,0,1,0) - 6*GFOffset(trK,0,2,0) 
      + GFOffset(trK,0,3,0))*pow(dy,-1) + (GFOffset(trK,0,0,-3) - 
      6*GFOffset(trK,0,0,-2) + 15*GFOffset(trK,0,0,-1) - 
      20*GFOffset(trK,0,0,0) + 15*GFOffset(trK,0,0,1) - 6*GFOffset(trK,0,0,2) 
      + GFOffset(trK,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDisstrK CCTK_ATTRIBUTE_UNUSED = 
      IfThen(usejacobian,myDetJL*LDisstrK,LDisstrK);
    
    CCTK_REAL LDissAt11 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(At11,-3,0,0) - 6*GFOffset(At11,-2,0,0) + 
      15*GFOffset(At11,-1,0,0) - 20*GFOffset(At11,0,0,0) + 
      15*GFOffset(At11,1,0,0) - 6*GFOffset(At11,2,0,0) + 
      GFOffset(At11,3,0,0))*pow(dx,-1) + (GFOffset(At11,0,-3,0) - 
      6*GFOffset(At11,0,-2,0) + 15*GFOffset(At11,0,-1,0) - 
      20*GFOffset(At11,0,0,0) + 15*GFOffset(At11,0,1,0) - 
      6*GFOffset(At11,0,2,0) + GFOffset(At11,0,3,0))*pow(dy,-1) + 
      (GFOffset(At11,0,0,-3) - 6*GFOffset(At11,0,0,-2) + 
      15*GFOffset(At11,0,0,-1) - 20*GFOffset(At11,0,0,0) + 
      15*GFOffset(At11,0,0,1) - 6*GFOffset(At11,0,0,2) + 
      GFOffset(At11,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissAt12 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(At12,-3,0,0) - 6*GFOffset(At12,-2,0,0) + 
      15*GFOffset(At12,-1,0,0) - 20*GFOffset(At12,0,0,0) + 
      15*GFOffset(At12,1,0,0) - 6*GFOffset(At12,2,0,0) + 
      GFOffset(At12,3,0,0))*pow(dx,-1) + (GFOffset(At12,0,-3,0) - 
      6*GFOffset(At12,0,-2,0) + 15*GFOffset(At12,0,-1,0) - 
      20*GFOffset(At12,0,0,0) + 15*GFOffset(At12,0,1,0) - 
      6*GFOffset(At12,0,2,0) + GFOffset(At12,0,3,0))*pow(dy,-1) + 
      (GFOffset(At12,0,0,-3) - 6*GFOffset(At12,0,0,-2) + 
      15*GFOffset(At12,0,0,-1) - 20*GFOffset(At12,0,0,0) + 
      15*GFOffset(At12,0,0,1) - 6*GFOffset(At12,0,0,2) + 
      GFOffset(At12,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissAt13 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(At13,-3,0,0) - 6*GFOffset(At13,-2,0,0) + 
      15*GFOffset(At13,-1,0,0) - 20*GFOffset(At13,0,0,0) + 
      15*GFOffset(At13,1,0,0) - 6*GFOffset(At13,2,0,0) + 
      GFOffset(At13,3,0,0))*pow(dx,-1) + (GFOffset(At13,0,-3,0) - 
      6*GFOffset(At13,0,-2,0) + 15*GFOffset(At13,0,-1,0) - 
      20*GFOffset(At13,0,0,0) + 15*GFOffset(At13,0,1,0) - 
      6*GFOffset(At13,0,2,0) + GFOffset(At13,0,3,0))*pow(dy,-1) + 
      (GFOffset(At13,0,0,-3) - 6*GFOffset(At13,0,0,-2) + 
      15*GFOffset(At13,0,0,-1) - 20*GFOffset(At13,0,0,0) + 
      15*GFOffset(At13,0,0,1) - 6*GFOffset(At13,0,0,2) + 
      GFOffset(At13,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissAt22 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(At22,-3,0,0) - 6*GFOffset(At22,-2,0,0) + 
      15*GFOffset(At22,-1,0,0) - 20*GFOffset(At22,0,0,0) + 
      15*GFOffset(At22,1,0,0) - 6*GFOffset(At22,2,0,0) + 
      GFOffset(At22,3,0,0))*pow(dx,-1) + (GFOffset(At22,0,-3,0) - 
      6*GFOffset(At22,0,-2,0) + 15*GFOffset(At22,0,-1,0) - 
      20*GFOffset(At22,0,0,0) + 15*GFOffset(At22,0,1,0) - 
      6*GFOffset(At22,0,2,0) + GFOffset(At22,0,3,0))*pow(dy,-1) + 
      (GFOffset(At22,0,0,-3) - 6*GFOffset(At22,0,0,-2) + 
      15*GFOffset(At22,0,0,-1) - 20*GFOffset(At22,0,0,0) + 
      15*GFOffset(At22,0,0,1) - 6*GFOffset(At22,0,0,2) + 
      GFOffset(At22,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissAt23 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(At23,-3,0,0) - 6*GFOffset(At23,-2,0,0) + 
      15*GFOffset(At23,-1,0,0) - 20*GFOffset(At23,0,0,0) + 
      15*GFOffset(At23,1,0,0) - 6*GFOffset(At23,2,0,0) + 
      GFOffset(At23,3,0,0))*pow(dx,-1) + (GFOffset(At23,0,-3,0) - 
      6*GFOffset(At23,0,-2,0) + 15*GFOffset(At23,0,-1,0) - 
      20*GFOffset(At23,0,0,0) + 15*GFOffset(At23,0,1,0) - 
      6*GFOffset(At23,0,2,0) + GFOffset(At23,0,3,0))*pow(dy,-1) + 
      (GFOffset(At23,0,0,-3) - 6*GFOffset(At23,0,0,-2) + 
      15*GFOffset(At23,0,0,-1) - 20*GFOffset(At23,0,0,0) + 
      15*GFOffset(At23,0,0,1) - 6*GFOffset(At23,0,0,2) + 
      GFOffset(At23,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissAt33 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(At33,-3,0,0) - 6*GFOffset(At33,-2,0,0) + 
      15*GFOffset(At33,-1,0,0) - 20*GFOffset(At33,0,0,0) + 
      15*GFOffset(At33,1,0,0) - 6*GFOffset(At33,2,0,0) + 
      GFOffset(At33,3,0,0))*pow(dx,-1) + (GFOffset(At33,0,-3,0) - 
      6*GFOffset(At33,0,-2,0) + 15*GFOffset(At33,0,-1,0) - 
      20*GFOffset(At33,0,0,0) + 15*GFOffset(At33,0,1,0) - 
      6*GFOffset(At33,0,2,0) + GFOffset(At33,0,3,0))*pow(dy,-1) + 
      (GFOffset(At33,0,0,-3) - 6*GFOffset(At33,0,0,-2) + 
      15*GFOffset(At33,0,0,-1) - 20*GFOffset(At33,0,0,0) + 
      15*GFOffset(At33,0,0,1) - 6*GFOffset(At33,0,0,2) + 
      GFOffset(At33,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissAt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissAt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissAt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissAt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissAt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissAt33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      GDissAt11 = myDetJL*LDissAt11;
      
      GDissAt12 = myDetJL*LDissAt12;
      
      GDissAt13 = myDetJL*LDissAt13;
      
      GDissAt22 = myDetJL*LDissAt22;
      
      GDissAt23 = myDetJL*LDissAt23;
      
      GDissAt33 = myDetJL*LDissAt33;
    }
    else
    {
      GDissAt11 = LDissAt11;
      
      GDissAt12 = LDissAt12;
      
      GDissAt13 = LDissAt13;
      
      GDissAt22 = LDissAt22;
      
      GDissAt23 = LDissAt23;
      
      GDissAt33 = LDissAt33;
    }
    
    CCTK_REAL LDissalpha CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(alpha,-3,0,0) - 6*GFOffset(alpha,-2,0,0) + 
      15*GFOffset(alpha,-1,0,0) - 20*GFOffset(alpha,0,0,0) + 
      15*GFOffset(alpha,1,0,0) - 6*GFOffset(alpha,2,0,0) + 
      GFOffset(alpha,3,0,0))*pow(dx,-1) + (GFOffset(alpha,0,-3,0) - 
      6*GFOffset(alpha,0,-2,0) + 15*GFOffset(alpha,0,-1,0) - 
      20*GFOffset(alpha,0,0,0) + 15*GFOffset(alpha,0,1,0) - 
      6*GFOffset(alpha,0,2,0) + GFOffset(alpha,0,3,0))*pow(dy,-1) + 
      (GFOffset(alpha,0,0,-3) - 6*GFOffset(alpha,0,0,-2) + 
      15*GFOffset(alpha,0,0,-1) - 20*GFOffset(alpha,0,0,0) + 
      15*GFOffset(alpha,0,0,1) - 6*GFOffset(alpha,0,0,2) + 
      GFOffset(alpha,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissalpha CCTK_ATTRIBUTE_UNUSED = 
      IfThen(usejacobian,myDetJL*LDissalpha,LDissalpha);
    
    CCTK_REAL LDissA CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(A,-3,0,0) - 6*GFOffset(A,-2,0,0) + 
      15*GFOffset(A,-1,0,0) - 20*GFOffset(A,0,0,0) + 15*GFOffset(A,1,0,0) - 
      6*GFOffset(A,2,0,0) + GFOffset(A,3,0,0))*pow(dx,-1) + 
      (GFOffset(A,0,-3,0) - 6*GFOffset(A,0,-2,0) + 15*GFOffset(A,0,-1,0) - 
      20*GFOffset(A,0,0,0) + 15*GFOffset(A,0,1,0) - 6*GFOffset(A,0,2,0) + 
      GFOffset(A,0,3,0))*pow(dy,-1) + (GFOffset(A,0,0,-3) - 
      6*GFOffset(A,0,0,-2) + 15*GFOffset(A,0,0,-1) - 20*GFOffset(A,0,0,0) + 
      15*GFOffset(A,0,0,1) - 6*GFOffset(A,0,0,2) + 
      GFOffset(A,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissA CCTK_ATTRIBUTE_UNUSED = 
      IfThen(usejacobian,myDetJL*LDissA,LDissA);
    
    CCTK_REAL LDissbeta1 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(beta1,-3,0,0) - 6*GFOffset(beta1,-2,0,0) + 
      15*GFOffset(beta1,-1,0,0) - 20*GFOffset(beta1,0,0,0) + 
      15*GFOffset(beta1,1,0,0) - 6*GFOffset(beta1,2,0,0) + 
      GFOffset(beta1,3,0,0))*pow(dx,-1) + (GFOffset(beta1,0,-3,0) - 
      6*GFOffset(beta1,0,-2,0) + 15*GFOffset(beta1,0,-1,0) - 
      20*GFOffset(beta1,0,0,0) + 15*GFOffset(beta1,0,1,0) - 
      6*GFOffset(beta1,0,2,0) + GFOffset(beta1,0,3,0))*pow(dy,-1) + 
      (GFOffset(beta1,0,0,-3) - 6*GFOffset(beta1,0,0,-2) + 
      15*GFOffset(beta1,0,0,-1) - 20*GFOffset(beta1,0,0,0) + 
      15*GFOffset(beta1,0,0,1) - 6*GFOffset(beta1,0,0,2) + 
      GFOffset(beta1,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissbeta2 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(beta2,-3,0,0) - 6*GFOffset(beta2,-2,0,0) + 
      15*GFOffset(beta2,-1,0,0) - 20*GFOffset(beta2,0,0,0) + 
      15*GFOffset(beta2,1,0,0) - 6*GFOffset(beta2,2,0,0) + 
      GFOffset(beta2,3,0,0))*pow(dx,-1) + (GFOffset(beta2,0,-3,0) - 
      6*GFOffset(beta2,0,-2,0) + 15*GFOffset(beta2,0,-1,0) - 
      20*GFOffset(beta2,0,0,0) + 15*GFOffset(beta2,0,1,0) - 
      6*GFOffset(beta2,0,2,0) + GFOffset(beta2,0,3,0))*pow(dy,-1) + 
      (GFOffset(beta2,0,0,-3) - 6*GFOffset(beta2,0,0,-2) + 
      15*GFOffset(beta2,0,0,-1) - 20*GFOffset(beta2,0,0,0) + 
      15*GFOffset(beta2,0,0,1) - 6*GFOffset(beta2,0,0,2) + 
      GFOffset(beta2,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissbeta3 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(beta3,-3,0,0) - 6*GFOffset(beta3,-2,0,0) + 
      15*GFOffset(beta3,-1,0,0) - 20*GFOffset(beta3,0,0,0) + 
      15*GFOffset(beta3,1,0,0) - 6*GFOffset(beta3,2,0,0) + 
      GFOffset(beta3,3,0,0))*pow(dx,-1) + (GFOffset(beta3,0,-3,0) - 
      6*GFOffset(beta3,0,-2,0) + 15*GFOffset(beta3,0,-1,0) - 
      20*GFOffset(beta3,0,0,0) + 15*GFOffset(beta3,0,1,0) - 
      6*GFOffset(beta3,0,2,0) + GFOffset(beta3,0,3,0))*pow(dy,-1) + 
      (GFOffset(beta3,0,0,-3) - 6*GFOffset(beta3,0,0,-2) + 
      15*GFOffset(beta3,0,0,-1) - 20*GFOffset(beta3,0,0,0) + 
      15*GFOffset(beta3,0,0,1) - 6*GFOffset(beta3,0,0,2) + 
      GFOffset(beta3,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissbeta1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissbeta2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissbeta3 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      GDissbeta1 = myDetJL*LDissbeta1;
      
      GDissbeta2 = myDetJL*LDissbeta2;
      
      GDissbeta3 = myDetJL*LDissbeta3;
    }
    else
    {
      GDissbeta1 = LDissbeta1;
      
      GDissbeta2 = LDissbeta2;
      
      GDissbeta3 = LDissbeta3;
    }
    
    CCTK_REAL LDissB1 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(B1,-3,0,0) - 6*GFOffset(B1,-2,0,0) + 
      15*GFOffset(B1,-1,0,0) - 20*GFOffset(B1,0,0,0) + 15*GFOffset(B1,1,0,0) 
      - 6*GFOffset(B1,2,0,0) + GFOffset(B1,3,0,0))*pow(dx,-1) + 
      (GFOffset(B1,0,-3,0) - 6*GFOffset(B1,0,-2,0) + 15*GFOffset(B1,0,-1,0) - 
      20*GFOffset(B1,0,0,0) + 15*GFOffset(B1,0,1,0) - 6*GFOffset(B1,0,2,0) + 
      GFOffset(B1,0,3,0))*pow(dy,-1) + (GFOffset(B1,0,0,-3) - 
      6*GFOffset(B1,0,0,-2) + 15*GFOffset(B1,0,0,-1) - 20*GFOffset(B1,0,0,0) 
      + 15*GFOffset(B1,0,0,1) - 6*GFOffset(B1,0,0,2) + 
      GFOffset(B1,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissB2 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(B2,-3,0,0) - 6*GFOffset(B2,-2,0,0) + 
      15*GFOffset(B2,-1,0,0) - 20*GFOffset(B2,0,0,0) + 15*GFOffset(B2,1,0,0) 
      - 6*GFOffset(B2,2,0,0) + GFOffset(B2,3,0,0))*pow(dx,-1) + 
      (GFOffset(B2,0,-3,0) - 6*GFOffset(B2,0,-2,0) + 15*GFOffset(B2,0,-1,0) - 
      20*GFOffset(B2,0,0,0) + 15*GFOffset(B2,0,1,0) - 6*GFOffset(B2,0,2,0) + 
      GFOffset(B2,0,3,0))*pow(dy,-1) + (GFOffset(B2,0,0,-3) - 
      6*GFOffset(B2,0,0,-2) + 15*GFOffset(B2,0,0,-1) - 20*GFOffset(B2,0,0,0) 
      + 15*GFOffset(B2,0,0,1) - 6*GFOffset(B2,0,0,2) + 
      GFOffset(B2,0,0,3))*pow(dz,-1));
    
    CCTK_REAL LDissB3 CCTK_ATTRIBUTE_UNUSED = 
      0.015625*epsDiss*((GFOffset(B3,-3,0,0) - 6*GFOffset(B3,-2,0,0) + 
      15*GFOffset(B3,-1,0,0) - 20*GFOffset(B3,0,0,0) + 15*GFOffset(B3,1,0,0) 
      - 6*GFOffset(B3,2,0,0) + GFOffset(B3,3,0,0))*pow(dx,-1) + 
      (GFOffset(B3,0,-3,0) - 6*GFOffset(B3,0,-2,0) + 15*GFOffset(B3,0,-1,0) - 
      20*GFOffset(B3,0,0,0) + 15*GFOffset(B3,0,1,0) - 6*GFOffset(B3,0,2,0) + 
      GFOffset(B3,0,3,0))*pow(dy,-1) + (GFOffset(B3,0,0,-3) - 
      6*GFOffset(B3,0,0,-2) + 15*GFOffset(B3,0,0,-1) - 20*GFOffset(B3,0,0,0) 
      + 15*GFOffset(B3,0,0,1) - 6*GFOffset(B3,0,0,2) + 
      GFOffset(B3,0,0,3))*pow(dz,-1));
    
    CCTK_REAL GDissB1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissB2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissB3 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      GDissB1 = myDetJL*LDissB1;
      
      GDissB2 = myDetJL*LDissB2;
      
      GDissB3 = myDetJL*LDissB3;
    }
    else
    {
      GDissB1 = LDissB1;
      
      GDissB2 = LDissB2;
      
      GDissB3 = LDissB3;
    }
    
    CCTK_REAL detgt CCTK_ATTRIBUTE_UNUSED = 1;
    
    CCTK_REAL gtu11 CCTK_ATTRIBUTE_UNUSED = (gt22L*gt33L - 
      pow(gt23L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu12 CCTK_ATTRIBUTE_UNUSED = (gt13L*gt23L - 
      gt12L*gt33L)*pow(detgt,-1);
    
    CCTK_REAL gtu13 CCTK_ATTRIBUTE_UNUSED = (-(gt13L*gt22L) + 
      gt12L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu22 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt33L - 
      pow(gt13L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu23 CCTK_ATTRIBUTE_UNUSED = (gt12L*gt13L - 
      gt11L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu33 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt22L - 
      pow(gt12L,2))*pow(detgt,-1);
    
    CCTK_REAL Gtl111 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt111L;
    
    CCTK_REAL Gtl112 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt112L;
    
    CCTK_REAL Gtl113 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt113L;
    
    CCTK_REAL Gtl122 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt122L - PDgt221L);
    
    CCTK_REAL Gtl123 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt123L + PDgt132L - 
      PDgt231L);
    
    CCTK_REAL Gtl133 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt133L - PDgt331L);
    
    CCTK_REAL Gtl211 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt112L + 2*PDgt121L);
    
    CCTK_REAL Gtl212 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt221L;
    
    CCTK_REAL Gtl213 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt123L - PDgt132L + 
      PDgt231L);
    
    CCTK_REAL Gtl222 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt222L;
    
    CCTK_REAL Gtl223 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt223L;
    
    CCTK_REAL Gtl233 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt233L - PDgt332L);
    
    CCTK_REAL Gtl311 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt113L + 2*PDgt131L);
    
    CCTK_REAL Gtl312 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt123L + PDgt132L + 
      PDgt231L);
    
    CCTK_REAL Gtl313 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt331L;
    
    CCTK_REAL Gtl322 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt223L + 2*PDgt232L);
    
    CCTK_REAL Gtl323 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt332L;
    
    CCTK_REAL Gtl333 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt333L;
    
    CCTK_REAL Gtlu111 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu11 + Gtl112*gtu12 
      + Gtl113*gtu13;
    
    CCTK_REAL Gtlu112 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu12 + Gtl112*gtu22 
      + Gtl113*gtu23;
    
    CCTK_REAL Gtlu113 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu13 + Gtl112*gtu23 
      + Gtl113*gtu33;
    
    CCTK_REAL Gtlu121 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu11 + Gtl122*gtu12 
      + Gtl123*gtu13;
    
    CCTK_REAL Gtlu122 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu12 + Gtl122*gtu22 
      + Gtl123*gtu23;
    
    CCTK_REAL Gtlu123 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu13 + Gtl122*gtu23 
      + Gtl123*gtu33;
    
    CCTK_REAL Gtlu131 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu11 + Gtl123*gtu12 
      + Gtl133*gtu13;
    
    CCTK_REAL Gtlu132 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu12 + Gtl123*gtu22 
      + Gtl133*gtu23;
    
    CCTK_REAL Gtlu133 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu13 + Gtl123*gtu23 
      + Gtl133*gtu33;
    
    CCTK_REAL Gtlu211 CCTK_ATTRIBUTE_UNUSED = Gtl211*gtu11 + Gtl212*gtu12 
      + Gtl213*gtu13;
    
    CCTK_REAL Gtlu212 CCTK_ATTRIBUTE_UNUSED = Gtl211*gtu12 + Gtl212*gtu22 
      + Gtl213*gtu23;
    
    CCTK_REAL Gtlu213 CCTK_ATTRIBUTE_UNUSED = Gtl211*gtu13 + Gtl212*gtu23 
      + Gtl213*gtu33;
    
    CCTK_REAL Gtlu221 CCTK_ATTRIBUTE_UNUSED = Gtl212*gtu11 + Gtl222*gtu12 
      + Gtl223*gtu13;
    
    CCTK_REAL Gtlu222 CCTK_ATTRIBUTE_UNUSED = Gtl212*gtu12 + Gtl222*gtu22 
      + Gtl223*gtu23;
    
    CCTK_REAL Gtlu223 CCTK_ATTRIBUTE_UNUSED = Gtl212*gtu13 + Gtl222*gtu23 
      + Gtl223*gtu33;
    
    CCTK_REAL Gtlu231 CCTK_ATTRIBUTE_UNUSED = Gtl213*gtu11 + Gtl223*gtu12 
      + Gtl233*gtu13;
    
    CCTK_REAL Gtlu232 CCTK_ATTRIBUTE_UNUSED = Gtl213*gtu12 + Gtl223*gtu22 
      + Gtl233*gtu23;
    
    CCTK_REAL Gtlu233 CCTK_ATTRIBUTE_UNUSED = Gtl213*gtu13 + Gtl223*gtu23 
      + Gtl233*gtu33;
    
    CCTK_REAL Gtlu311 CCTK_ATTRIBUTE_UNUSED = Gtl311*gtu11 + Gtl312*gtu12 
      + Gtl313*gtu13;
    
    CCTK_REAL Gtlu312 CCTK_ATTRIBUTE_UNUSED = Gtl311*gtu12 + Gtl312*gtu22 
      + Gtl313*gtu23;
    
    CCTK_REAL Gtlu313 CCTK_ATTRIBUTE_UNUSED = Gtl311*gtu13 + Gtl312*gtu23 
      + Gtl313*gtu33;
    
    CCTK_REAL Gtlu321 CCTK_ATTRIBUTE_UNUSED = Gtl312*gtu11 + Gtl322*gtu12 
      + Gtl323*gtu13;
    
    CCTK_REAL Gtlu322 CCTK_ATTRIBUTE_UNUSED = Gtl312*gtu12 + Gtl322*gtu22 
      + Gtl323*gtu23;
    
    CCTK_REAL Gtlu323 CCTK_ATTRIBUTE_UNUSED = Gtl312*gtu13 + Gtl322*gtu23 
      + Gtl323*gtu33;
    
    CCTK_REAL Gtlu331 CCTK_ATTRIBUTE_UNUSED = Gtl313*gtu11 + Gtl323*gtu12 
      + Gtl333*gtu13;
    
    CCTK_REAL Gtlu332 CCTK_ATTRIBUTE_UNUSED = Gtl313*gtu12 + Gtl323*gtu22 
      + Gtl333*gtu23;
    
    CCTK_REAL Gtlu333 CCTK_ATTRIBUTE_UNUSED = Gtl313*gtu13 + Gtl323*gtu23 
      + Gtl333*gtu33;
    
    CCTK_REAL Gt111 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu11 + Gtl211*gtu12 + 
      Gtl311*gtu13;
    
    CCTK_REAL Gt211 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu12 + Gtl211*gtu22 + 
      Gtl311*gtu23;
    
    CCTK_REAL Gt311 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu13 + Gtl211*gtu23 + 
      Gtl311*gtu33;
    
    CCTK_REAL Gt112 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu11 + Gtl212*gtu12 + 
      Gtl312*gtu13;
    
    CCTK_REAL Gt212 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu12 + Gtl212*gtu22 + 
      Gtl312*gtu23;
    
    CCTK_REAL Gt312 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu13 + Gtl212*gtu23 + 
      Gtl312*gtu33;
    
    CCTK_REAL Gt113 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu11 + Gtl213*gtu12 + 
      Gtl313*gtu13;
    
    CCTK_REAL Gt213 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu12 + Gtl213*gtu22 + 
      Gtl313*gtu23;
    
    CCTK_REAL Gt313 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu13 + Gtl213*gtu23 + 
      Gtl313*gtu33;
    
    CCTK_REAL Gt122 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu11 + Gtl222*gtu12 + 
      Gtl322*gtu13;
    
    CCTK_REAL Gt222 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu12 + Gtl222*gtu22 + 
      Gtl322*gtu23;
    
    CCTK_REAL Gt322 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu13 + Gtl222*gtu23 + 
      Gtl322*gtu33;
    
    CCTK_REAL Gt123 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu11 + Gtl223*gtu12 + 
      Gtl323*gtu13;
    
    CCTK_REAL Gt223 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu12 + Gtl223*gtu22 + 
      Gtl323*gtu23;
    
    CCTK_REAL Gt323 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu13 + Gtl223*gtu23 + 
      Gtl323*gtu33;
    
    CCTK_REAL Gt133 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu11 + Gtl233*gtu12 + 
      Gtl333*gtu13;
    
    CCTK_REAL Gt233 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu12 + Gtl233*gtu22 + 
      Gtl333*gtu23;
    
    CCTK_REAL Gt333 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu13 + Gtl233*gtu23 + 
      Gtl333*gtu33;
    
    CCTK_REAL em4phi CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,pow(phiWL,2),exp(-4*phiWL));
    
    CCTK_REAL e4phi CCTK_ATTRIBUTE_UNUSED = pow(em4phi,-1);
    
    CCTK_REAL g11 CCTK_ATTRIBUTE_UNUSED = gt11L*e4phi;
    
    CCTK_REAL g12 CCTK_ATTRIBUTE_UNUSED = gt12L*e4phi;
    
    CCTK_REAL g13 CCTK_ATTRIBUTE_UNUSED = gt13L*e4phi;
    
    CCTK_REAL g22 CCTK_ATTRIBUTE_UNUSED = gt22L*e4phi;
    
    CCTK_REAL g23 CCTK_ATTRIBUTE_UNUSED = gt23L*e4phi;
    
    CCTK_REAL g33 CCTK_ATTRIBUTE_UNUSED = gt33L*e4phi;
    
    CCTK_REAL gu11 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu11;
    
    CCTK_REAL gu12 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu12;
    
    CCTK_REAL gu13 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu13;
    
    CCTK_REAL gu22 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu22;
    
    CCTK_REAL gu23 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu23;
    
    CCTK_REAL gu33 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu33;
    
    CCTK_REAL Xtn1 CCTK_ATTRIBUTE_UNUSED = Gt111*gtu11 + 2*Gt112*gtu12 + 
      2*Gt113*gtu13 + Gt122*gtu22 + 2*Gt123*gtu23 + Gt133*gtu33;
    
    CCTK_REAL Xtn2 CCTK_ATTRIBUTE_UNUSED = Gt211*gtu11 + 2*Gt212*gtu12 + 
      2*Gt213*gtu13 + Gt222*gtu22 + 2*Gt223*gtu23 + Gt233*gtu33;
    
    CCTK_REAL Xtn3 CCTK_ATTRIBUTE_UNUSED = Gt311*gtu11 + 2*Gt312*gtu12 + 
      2*Gt313*gtu13 + Gt322*gtu22 + 2*Gt323*gtu23 + Gt333*gtu33;
    
    CCTK_REAL Rt11 CCTK_ATTRIBUTE_UNUSED = 3*Gt111*Gtlu111 + 
      3*Gt112*Gtlu112 + 3*Gt113*Gtlu113 + 2*Gt211*Gtlu121 + 2*Gt212*Gtlu122 + 
      2*Gt213*Gtlu123 + 2*Gt311*Gtlu131 + 2*Gt312*Gtlu132 + 2*Gt313*Gtlu133 + 
      Gt211*Gtlu211 + Gt212*Gtlu212 + Gt213*Gtlu213 + Gt311*Gtlu311 + 
      Gt312*Gtlu312 + Gt313*Gtlu313 + 0.5*(-(gtu11*PDPDgt1111) - 
      gtu12*PDPDgt1112 - gtu13*PDPDgt1113 - gtu12*PDPDgt1121 - 
      gtu22*PDPDgt1122 - gtu23*PDPDgt1123 - gtu13*PDPDgt1131 - 
      gtu23*PDPDgt1132 - gtu33*PDPDgt1133) + gt11L*PDXt11 + gt12L*PDXt21 + 
      gt13L*PDXt31 + Gtl111*Xtn1 + Gtl112*Xtn2 + Gtl113*Xtn3;
    
    CCTK_REAL Rt12 CCTK_ATTRIBUTE_UNUSED = Gt112*Gtlu111 + Gt122*Gtlu112 + 
      Gt123*Gtlu113 + Gt111*Gtlu121 + Gt212*Gtlu121 + Gt112*Gtlu122 + 
      Gt222*Gtlu122 + Gt113*Gtlu123 + Gt223*Gtlu123 + Gt312*Gtlu131 + 
      Gt322*Gtlu132 + Gt323*Gtlu133 + Gt111*Gtlu211 + Gt112*Gtlu212 + 
      Gt113*Gtlu213 + 2*Gt211*Gtlu221 + 2*Gt212*Gtlu222 + 2*Gt213*Gtlu223 + 
      Gt311*Gtlu231 + Gt312*Gtlu232 + Gt313*Gtlu233 + Gt311*Gtlu321 + 
      Gt312*Gtlu322 + Gt313*Gtlu323 + 0.5*(-(gtu11*PDPDgt1211) - 
      gtu12*PDPDgt1212 - gtu13*PDPDgt1213 - gtu12*PDPDgt1221 - 
      gtu22*PDPDgt1222 - gtu23*PDPDgt1223 - gtu13*PDPDgt1231 - 
      gtu23*PDPDgt1232 - gtu33*PDPDgt1233) + 0.5*(gt12L*PDXt11 + gt22L*PDXt21 
      + gt23L*PDXt31) + 0.5*(gt11L*PDXt12 + gt12L*PDXt22 + gt13L*PDXt32) + 
      0.5*(Gtl112*Xtn1 + Gtl122*Xtn2 + Gtl123*Xtn3) + 0.5*(Gtl211*Xtn1 + 
      Gtl212*Xtn2 + Gtl213*Xtn3);
    
    CCTK_REAL Rt13 CCTK_ATTRIBUTE_UNUSED = Gt113*Gtlu111 + Gt123*Gtlu112 + 
      Gt133*Gtlu113 + Gt213*Gtlu121 + Gt223*Gtlu122 + Gt233*Gtlu123 + 
      Gt111*Gtlu131 + Gt313*Gtlu131 + Gt112*Gtlu132 + Gt323*Gtlu132 + 
      Gt113*Gtlu133 + Gt333*Gtlu133 + Gt211*Gtlu231 + Gt212*Gtlu232 + 
      Gt213*Gtlu233 + Gt111*Gtlu311 + Gt112*Gtlu312 + Gt113*Gtlu313 + 
      Gt211*Gtlu321 + Gt212*Gtlu322 + Gt213*Gtlu323 + 2*Gt311*Gtlu331 + 
      2*Gt312*Gtlu332 + 2*Gt313*Gtlu333 + 0.5*(-(gtu11*PDPDgt1311) - 
      gtu12*PDPDgt1312 - gtu13*PDPDgt1313 - gtu12*PDPDgt1321 - 
      gtu22*PDPDgt1322 - gtu23*PDPDgt1323 - gtu13*PDPDgt1331 - 
      gtu23*PDPDgt1332 - gtu33*PDPDgt1333) + 0.5*(gt13L*PDXt11 + gt23L*PDXt21 
      + gt33L*PDXt31) + 0.5*(gt11L*PDXt13 + gt12L*PDXt23 + gt13L*PDXt33) + 
      0.5*(Gtl113*Xtn1 + Gtl123*Xtn2 + Gtl133*Xtn3) + 0.5*(Gtl311*Xtn1 + 
      Gtl312*Xtn2 + Gtl313*Xtn3);
    
    CCTK_REAL Rt22 CCTK_ATTRIBUTE_UNUSED = Gt112*Gtlu121 + Gt122*Gtlu122 + 
      Gt123*Gtlu123 + 2*Gt112*Gtlu211 + 2*Gt122*Gtlu212 + 2*Gt123*Gtlu213 + 
      3*Gt212*Gtlu221 + 3*Gt222*Gtlu222 + 3*Gt223*Gtlu223 + 2*Gt312*Gtlu231 + 
      2*Gt322*Gtlu232 + 2*Gt323*Gtlu233 + Gt312*Gtlu321 + Gt322*Gtlu322 + 
      Gt323*Gtlu323 + 0.5*(-(gtu11*PDPDgt2211) - gtu12*PDPDgt2212 - 
      gtu13*PDPDgt2213 - gtu12*PDPDgt2221 - gtu22*PDPDgt2222 - 
      gtu23*PDPDgt2223 - gtu13*PDPDgt2231 - gtu23*PDPDgt2232 - 
      gtu33*PDPDgt2233) + gt12L*PDXt12 + gt22L*PDXt22 + gt23L*PDXt32 + 
      Gtl212*Xtn1 + Gtl222*Xtn2 + Gtl223*Xtn3;
    
    CCTK_REAL Rt23 CCTK_ATTRIBUTE_UNUSED = Gt112*Gtlu131 + Gt122*Gtlu132 + 
      Gt123*Gtlu133 + Gt113*Gtlu211 + Gt123*Gtlu212 + Gt133*Gtlu213 + 
      Gt213*Gtlu221 + Gt223*Gtlu222 + Gt233*Gtlu223 + Gt212*Gtlu231 + 
      Gt313*Gtlu231 + Gt222*Gtlu232 + Gt323*Gtlu232 + Gt223*Gtlu233 + 
      Gt333*Gtlu233 + Gt112*Gtlu311 + Gt122*Gtlu312 + Gt123*Gtlu313 + 
      Gt212*Gtlu321 + Gt222*Gtlu322 + Gt223*Gtlu323 + 2*Gt312*Gtlu331 + 
      2*Gt322*Gtlu332 + 2*Gt323*Gtlu333 + 0.5*(-(gtu11*PDPDgt2311) - 
      gtu12*PDPDgt2312 - gtu13*PDPDgt2313 - gtu12*PDPDgt2321 - 
      gtu22*PDPDgt2322 - gtu23*PDPDgt2323 - gtu13*PDPDgt2331 - 
      gtu23*PDPDgt2332 - gtu33*PDPDgt2333) + 0.5*(gt13L*PDXt12 + gt23L*PDXt22 
      + gt33L*PDXt32) + 0.5*(gt12L*PDXt13 + gt22L*PDXt23 + gt23L*PDXt33) + 
      0.5*(Gtl213*Xtn1 + Gtl223*Xtn2 + Gtl233*Xtn3) + 0.5*(Gtl312*Xtn1 + 
      Gtl322*Xtn2 + Gtl323*Xtn3);
    
    CCTK_REAL Rt33 CCTK_ATTRIBUTE_UNUSED = Gt113*Gtlu131 + Gt123*Gtlu132 + 
      Gt133*Gtlu133 + Gt213*Gtlu231 + Gt223*Gtlu232 + Gt233*Gtlu233 + 
      2*Gt113*Gtlu311 + 2*Gt123*Gtlu312 + 2*Gt133*Gtlu313 + 2*Gt213*Gtlu321 + 
      2*Gt223*Gtlu322 + 2*Gt233*Gtlu323 + 3*Gt313*Gtlu331 + 3*Gt323*Gtlu332 + 
      3*Gt333*Gtlu333 + 0.5*(-(gtu11*PDPDgt3311) - gtu12*PDPDgt3312 - 
      gtu13*PDPDgt3313 - gtu12*PDPDgt3321 - gtu22*PDPDgt3322 - 
      gtu23*PDPDgt3323 - gtu13*PDPDgt3331 - gtu23*PDPDgt3332 - 
      gtu33*PDPDgt3333) + gt13L*PDXt13 + gt23L*PDXt23 + gt33L*PDXt33 + 
      Gtl313*Xtn1 + Gtl323*Xtn2 + Gtl333*Xtn3;
    
    CCTK_REAL fac1 CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,-0.5*pow(phiWL,-1),1);
    
    CCTK_REAL cdphi1 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*fac1;
    
    CCTK_REAL cdphi2 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*fac1;
    
    CCTK_REAL cdphi3 CCTK_ATTRIBUTE_UNUSED = PDphiW3L*fac1;
    
    CCTK_REAL fac2 CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,0.5*pow(phiWL,-2),0);
    
    CCTK_REAL cdphi211 CCTK_ATTRIBUTE_UNUSED = fac1*(-(PDphiW1L*Gt111) - 
      PDphiW2L*Gt211 - PDphiW3L*Gt311 + PDPDphiW11) + fac2*pow(PDphiW1L,2);
    
    CCTK_REAL cdphi212 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW2L*fac2 + 
      fac1*(-(PDphiW1L*Gt112) - PDphiW2L*Gt212 - PDphiW3L*Gt312 + 
      PDPDphiW12);
    
    CCTK_REAL cdphi213 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt113) - PDphiW2L*Gt213 - PDphiW3L*Gt313 + 
      PDPDphiW13);
    
    CCTK_REAL cdphi221 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW2L*fac2 + 
      fac1*(-(PDphiW1L*Gt112) - PDphiW2L*Gt212 - PDphiW3L*Gt312 + 
      PDPDphiW21);
    
    CCTK_REAL cdphi222 CCTK_ATTRIBUTE_UNUSED = fac1*(-(PDphiW1L*Gt122) - 
      PDphiW2L*Gt222 - PDphiW3L*Gt322 + PDPDphiW22) + fac2*pow(PDphiW2L,2);
    
    CCTK_REAL cdphi223 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt123) - PDphiW2L*Gt223 - PDphiW3L*Gt323 + 
      PDPDphiW23);
    
    CCTK_REAL cdphi231 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt113) - PDphiW2L*Gt213 - PDphiW3L*Gt313 + 
      PDPDphiW31);
    
    CCTK_REAL cdphi232 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt123) - PDphiW2L*Gt223 - PDphiW3L*Gt323 + 
      PDPDphiW32);
    
    CCTK_REAL cdphi233 CCTK_ATTRIBUTE_UNUSED = fac1*(-(PDphiW1L*Gt133) - 
      PDphiW2L*Gt233 - PDphiW3L*Gt333 + PDPDphiW33) + fac2*pow(PDphiW3L,2);
    
    CCTK_REAL Rphi11 CCTK_ATTRIBUTE_UNUSED = -2*cdphi211 - 
      2*gt11L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt11L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33)) + 
      4*pow(cdphi1,2);
    
    CCTK_REAL Rphi12 CCTK_ATTRIBUTE_UNUSED = 4*cdphi1*cdphi2 - 2*cdphi221 
      - 2*gt12L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt12L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33));
    
    CCTK_REAL Rphi13 CCTK_ATTRIBUTE_UNUSED = -2*cdphi231 + 4*cdphi1*cdphi3 
      - 2*gt13L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt13L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33));
    
    CCTK_REAL Rphi22 CCTK_ATTRIBUTE_UNUSED = -2*cdphi222 - 
      2*gt22L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt22L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33)) + 
      4*pow(cdphi2,2);
    
    CCTK_REAL Rphi23 CCTK_ATTRIBUTE_UNUSED = -2*cdphi232 + 4*cdphi2*cdphi3 
      - 2*gt23L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt23L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33));
    
    CCTK_REAL Rphi33 CCTK_ATTRIBUTE_UNUSED = -2*cdphi233 - 
      2*gt33L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt33L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33)) + 
      4*pow(cdphi3,2);
    
    CCTK_REAL Atm11 CCTK_ATTRIBUTE_UNUSED = At11L*gtu11 + At12L*gtu12 + 
      At13L*gtu13;
    
    CCTK_REAL Atm21 CCTK_ATTRIBUTE_UNUSED = At11L*gtu12 + At12L*gtu22 + 
      At13L*gtu23;
    
    CCTK_REAL Atm31 CCTK_ATTRIBUTE_UNUSED = At11L*gtu13 + At12L*gtu23 + 
      At13L*gtu33;
    
    CCTK_REAL Atm12 CCTK_ATTRIBUTE_UNUSED = At12L*gtu11 + At22L*gtu12 + 
      At23L*gtu13;
    
    CCTK_REAL Atm22 CCTK_ATTRIBUTE_UNUSED = At12L*gtu12 + At22L*gtu22 + 
      At23L*gtu23;
    
    CCTK_REAL Atm32 CCTK_ATTRIBUTE_UNUSED = At12L*gtu13 + At22L*gtu23 + 
      At23L*gtu33;
    
    CCTK_REAL Atm13 CCTK_ATTRIBUTE_UNUSED = At13L*gtu11 + At23L*gtu12 + 
      At33L*gtu13;
    
    CCTK_REAL Atm23 CCTK_ATTRIBUTE_UNUSED = At13L*gtu12 + At23L*gtu22 + 
      At33L*gtu23;
    
    CCTK_REAL Atm33 CCTK_ATTRIBUTE_UNUSED = At13L*gtu13 + At23L*gtu23 + 
      At33L*gtu33;
    
    CCTK_REAL Atu11 CCTK_ATTRIBUTE_UNUSED = Atm11*gtu11 + Atm12*gtu12 + 
      Atm13*gtu13;
    
    CCTK_REAL Atu12 CCTK_ATTRIBUTE_UNUSED = Atm11*gtu12 + Atm12*gtu22 + 
      Atm13*gtu23;
    
    CCTK_REAL Atu13 CCTK_ATTRIBUTE_UNUSED = Atm11*gtu13 + Atm12*gtu23 + 
      Atm13*gtu33;
    
    CCTK_REAL Atu22 CCTK_ATTRIBUTE_UNUSED = Atm21*gtu12 + Atm22*gtu22 + 
      Atm23*gtu23;
    
    CCTK_REAL Atu23 CCTK_ATTRIBUTE_UNUSED = Atm21*gtu13 + Atm22*gtu23 + 
      Atm23*gtu33;
    
    CCTK_REAL Atu33 CCTK_ATTRIBUTE_UNUSED = Atm31*gtu13 + Atm32*gtu23 + 
      Atm33*gtu33;
    
    CCTK_REAL R11 CCTK_ATTRIBUTE_UNUSED = Rphi11 + Rt11;
    
    CCTK_REAL R12 CCTK_ATTRIBUTE_UNUSED = Rphi12 + Rt12;
    
    CCTK_REAL R13 CCTK_ATTRIBUTE_UNUSED = Rphi13 + Rt13;
    
    CCTK_REAL R22 CCTK_ATTRIBUTE_UNUSED = Rphi22 + Rt22;
    
    CCTK_REAL R23 CCTK_ATTRIBUTE_UNUSED = Rphi23 + Rt23;
    
    CCTK_REAL R33 CCTK_ATTRIBUTE_UNUSED = Rphi33 + Rt33;
    
    CCTK_REAL rho CCTK_ATTRIBUTE_UNUSED = (eTttL - 2*(beta1L*eTtxL + 
      beta2L*eTtyL + beta3L*eTtzL) + beta1L*(beta1L*eTxxL + beta2L*eTxyL + 
      beta3L*eTxzL) + beta2L*(beta1L*eTxyL + beta2L*eTyyL + beta3L*eTyzL) + 
      beta3L*(beta1L*eTxzL + beta2L*eTyzL + beta3L*eTzzL))*pow(alphaL,-2);
    
    CCTK_REAL S1 CCTK_ATTRIBUTE_UNUSED = -((eTtxL - beta1L*eTxxL - 
      beta2L*eTxyL - beta3L*eTxzL)*pow(alphaL,-1));
    
    CCTK_REAL S2 CCTK_ATTRIBUTE_UNUSED = -((eTtyL - beta1L*eTxyL - 
      beta2L*eTyyL - beta3L*eTyzL)*pow(alphaL,-1));
    
    CCTK_REAL S3 CCTK_ATTRIBUTE_UNUSED = -((eTtzL - beta1L*eTxzL - 
      beta2L*eTyzL - beta3L*eTzzL)*pow(alphaL,-1));
    
    CCTK_REAL trS CCTK_ATTRIBUTE_UNUSED = eTxxL*gu11 + 2*eTxyL*gu12 + 
      2*eTxzL*gu13 + eTyyL*gu22 + 2*eTyzL*gu23 + eTzzL*gu33;
    
    CCTK_REAL phiWrhsL CCTK_ATTRIBUTE_UNUSED = GDissphiW + beta1L*PDuphiW1 
      + beta2L*PDuphiW2 + beta3L*PDuphiW3 + (-PDbeta11L - PDbeta22L - 
      PDbeta33L + alphaL*trKL)*IfThen(conformalMethod != 
      0,0.333333333333333333333333333333*phiWL,-0.166666666666666666666666666667);
    
    CCTK_REAL gt11rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At11L + 
      2*gt11L*PDbeta11L + 2*gt12L*PDbeta21L + 2*gt13L*PDbeta31L - 
      0.666666666666666666666666666667*gt11L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt11 + beta1L*PDugt111 + beta2L*PDugt112 + 
      beta3L*PDugt113;
    
    CCTK_REAL gt12rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At12L + 
      gt12L*PDbeta11L + gt11L*PDbeta12L + gt22L*PDbeta21L + gt12L*PDbeta22L + 
      gt23L*PDbeta31L + gt13L*PDbeta32L - 
      0.666666666666666666666666666667*gt12L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt12 + beta1L*PDugt121 + beta2L*PDugt122 + 
      beta3L*PDugt123;
    
    CCTK_REAL gt13rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At13L + 
      gt13L*PDbeta11L + gt11L*PDbeta13L + gt23L*PDbeta21L + gt12L*PDbeta23L + 
      gt33L*PDbeta31L + gt13L*PDbeta33L - 
      0.666666666666666666666666666667*gt13L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt13 + beta1L*PDugt131 + beta2L*PDugt132 + 
      beta3L*PDugt133;
    
    CCTK_REAL gt22rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At22L + 
      2*gt12L*PDbeta12L + 2*gt22L*PDbeta22L + 2*gt23L*PDbeta32L - 
      0.666666666666666666666666666667*gt22L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt22 + beta1L*PDugt221 + beta2L*PDugt222 + 
      beta3L*PDugt223;
    
    CCTK_REAL gt23rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At23L + 
      gt13L*PDbeta12L + gt12L*PDbeta13L + gt23L*PDbeta22L + gt22L*PDbeta23L + 
      gt33L*PDbeta32L + gt23L*PDbeta33L - 
      0.666666666666666666666666666667*gt23L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt23 + beta1L*PDugt231 + beta2L*PDugt232 + 
      beta3L*PDugt233;
    
    CCTK_REAL gt33rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At33L + 
      2*gt13L*PDbeta13L + 2*gt23L*PDbeta23L + 2*gt33L*PDbeta33L - 
      0.666666666666666666666666666667*gt33L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt33 + beta1L*PDugt331 + beta2L*PDugt332 + 
      beta3L*PDugt333;
    
    CCTK_REAL dotXt1 CCTK_ATTRIBUTE_UNUSED = -2*(PDalpha1L*Atu11 + 
      PDalpha2L*Atu12 + PDalpha3L*Atu13) + gtu11*PDPDbeta111 + 
      gtu12*PDPDbeta112 + gtu13*PDPDbeta113 + gtu12*PDPDbeta121 + 
      gtu22*PDPDbeta122 + gtu23*PDPDbeta123 + gtu13*PDPDbeta131 + 
      gtu23*PDPDbeta132 + gtu33*PDPDbeta133 + 
      0.333333333333333333333333333333*(gtu11*(PDPDbeta111 + PDPDbeta212 + 
      PDPDbeta313) + gtu12*(PDPDbeta121 + PDPDbeta222 + PDPDbeta323) + 
      gtu13*(PDPDbeta131 + PDPDbeta232 + PDPDbeta333)) + 
      2*alphaL*(6*(Atu11*cdphi1 + Atu12*cdphi2 + Atu13*cdphi3) + Atu11*Gt111 
      + 2*Atu12*Gt112 + 2*Atu13*Gt113 + Atu22*Gt122 + 2*Atu23*Gt123 + 
      Atu33*Gt133 - 0.666666666666666666666666666667*(gtu11*PDtrK1 + 
      gtu12*PDtrK2 + gtu13*PDtrK3)) - 16*alphaL*Pi*(gtu11*S1 + gtu12*S2 + 
      gtu13*S3) - PDbeta11L*Xtn1 + 
      0.666666666666666666666666666667*(PDbeta11L + PDbeta22L + 
      PDbeta33L)*Xtn1 - PDbeta12L*Xtn2 - PDbeta13L*Xtn3;
    
    CCTK_REAL dotXt2 CCTK_ATTRIBUTE_UNUSED = -2*(PDalpha1L*Atu12 + 
      PDalpha2L*Atu22 + PDalpha3L*Atu23) + gtu11*PDPDbeta211 + 
      gtu12*PDPDbeta212 + gtu13*PDPDbeta213 + gtu12*PDPDbeta221 + 
      gtu22*PDPDbeta222 + gtu23*PDPDbeta223 + gtu13*PDPDbeta231 + 
      gtu23*PDPDbeta232 + gtu33*PDPDbeta233 + 
      0.333333333333333333333333333333*(gtu12*(PDPDbeta111 + PDPDbeta212 + 
      PDPDbeta313) + gtu22*(PDPDbeta121 + PDPDbeta222 + PDPDbeta323) + 
      gtu23*(PDPDbeta131 + PDPDbeta232 + PDPDbeta333)) + 
      2*alphaL*(6*(Atu12*cdphi1 + Atu22*cdphi2 + Atu23*cdphi3) + Atu11*Gt211 
      + 2*Atu12*Gt212 + 2*Atu13*Gt213 + Atu22*Gt222 + 2*Atu23*Gt223 + 
      Atu33*Gt233 - 0.666666666666666666666666666667*(gtu12*PDtrK1 + 
      gtu22*PDtrK2 + gtu23*PDtrK3)) - 16*alphaL*Pi*(gtu12*S1 + gtu22*S2 + 
      gtu23*S3) - PDbeta21L*Xtn1 - PDbeta22L*Xtn2 + 
      0.666666666666666666666666666667*(PDbeta11L + PDbeta22L + 
      PDbeta33L)*Xtn2 - PDbeta23L*Xtn3;
    
    CCTK_REAL dotXt3 CCTK_ATTRIBUTE_UNUSED = -2*(PDalpha1L*Atu13 + 
      PDalpha2L*Atu23 + PDalpha3L*Atu33) + gtu11*PDPDbeta311 + 
      gtu12*PDPDbeta312 + gtu13*PDPDbeta313 + gtu12*PDPDbeta321 + 
      gtu22*PDPDbeta322 + gtu23*PDPDbeta323 + gtu13*PDPDbeta331 + 
      gtu23*PDPDbeta332 + gtu33*PDPDbeta333 + 
      0.333333333333333333333333333333*(gtu13*(PDPDbeta111 + PDPDbeta212 + 
      PDPDbeta313) + gtu23*(PDPDbeta121 + PDPDbeta222 + PDPDbeta323) + 
      gtu33*(PDPDbeta131 + PDPDbeta232 + PDPDbeta333)) + 
      2*alphaL*(6*(Atu13*cdphi1 + Atu23*cdphi2 + Atu33*cdphi3) + Atu11*Gt311 
      + 2*Atu12*Gt312 + 2*Atu13*Gt313 + Atu22*Gt322 + 2*Atu23*Gt323 + 
      Atu33*Gt333 - 0.666666666666666666666666666667*(gtu13*PDtrK1 + 
      gtu23*PDtrK2 + gtu33*PDtrK3)) - 16*alphaL*Pi*(gtu13*S1 + gtu23*S2 + 
      gtu33*S3) - PDbeta31L*Xtn1 - PDbeta32L*Xtn2 - PDbeta33L*Xtn3 + 
      0.666666666666666666666666666667*(PDbeta11L + PDbeta22L + 
      PDbeta33L)*Xtn3;
    
    CCTK_REAL Xt1rhsL CCTK_ATTRIBUTE_UNUSED = dotXt1 + GDissXt1 + 
      beta1L*PDuXt11 + beta2L*PDuXt12 + beta3L*PDuXt13;
    
    CCTK_REAL Xt2rhsL CCTK_ATTRIBUTE_UNUSED = dotXt2 + GDissXt2 + 
      beta1L*PDuXt21 + beta2L*PDuXt22 + beta3L*PDuXt23;
    
    CCTK_REAL Xt3rhsL CCTK_ATTRIBUTE_UNUSED = dotXt3 + GDissXt3 + 
      beta1L*PDuXt31 + beta2L*PDuXt32 + beta3L*PDuXt33;
    
    CCTK_REAL dottrK CCTK_ATTRIBUTE_UNUSED = 4*alphaL*Pi*(rho + trS) - 
      em4phi*(gtu11*(2*PDalpha1L*cdphi1 + PDPDalpha11) + 
      gtu12*(2*PDalpha2L*cdphi1 + PDPDalpha12) + gtu13*(2*PDalpha3L*cdphi1 + 
      PDPDalpha13) + gtu12*(2*PDalpha1L*cdphi2 + PDPDalpha21) + 
      gtu22*(2*PDalpha2L*cdphi2 + PDPDalpha22) + gtu23*(2*PDalpha3L*cdphi2 + 
      PDPDalpha23) + gtu13*(2*PDalpha1L*cdphi3 + PDPDalpha31) + 
      gtu23*(2*PDalpha2L*cdphi3 + PDPDalpha32) + gtu33*(2*PDalpha3L*cdphi3 + 
      PDPDalpha33) - PDalpha1L*Xtn1 - PDalpha2L*Xtn2 - PDalpha3L*Xtn3) + 
      alphaL*(2*Atm12*Atm21 + 2*Atm13*Atm31 + 2*Atm23*Atm32 + 
      0.333333333333333333333333333333*pow(trKL,2) + pow(Atm11,2) + 
      pow(Atm22,2) + pow(Atm33,2));
    
    CCTK_REAL trKrhsL CCTK_ATTRIBUTE_UNUSED = dottrK + GDisstrK + 
      beta1L*PDutrK1 + beta2L*PDutrK2 + beta3L*PDutrK3;
    
    CCTK_REAL Ats11 CCTK_ATTRIBUTE_UNUSED = 4*PDalpha1L*cdphi1 + 
      PDalpha1L*Gt111 + PDalpha2L*Gt211 + PDalpha3L*Gt311 - PDPDalpha11 + 
      alphaL*R11;
    
    CCTK_REAL Ats12 CCTK_ATTRIBUTE_UNUSED = 2*(PDalpha2L*cdphi1 + 
      PDalpha1L*cdphi2) + PDalpha1L*Gt112 + PDalpha2L*Gt212 + PDalpha3L*Gt312 
      - PDPDalpha12 + alphaL*R12;
    
    CCTK_REAL Ats13 CCTK_ATTRIBUTE_UNUSED = 2*(PDalpha3L*cdphi1 + 
      PDalpha1L*cdphi3) + PDalpha1L*Gt113 + PDalpha2L*Gt213 + PDalpha3L*Gt313 
      - PDPDalpha13 + alphaL*R13;
    
    CCTK_REAL Ats22 CCTK_ATTRIBUTE_UNUSED = 4*PDalpha2L*cdphi2 + 
      PDalpha1L*Gt122 + PDalpha2L*Gt222 + PDalpha3L*Gt322 - PDPDalpha22 + 
      alphaL*R22;
    
    CCTK_REAL Ats23 CCTK_ATTRIBUTE_UNUSED = 2*(PDalpha3L*cdphi2 + 
      PDalpha2L*cdphi3) + PDalpha1L*Gt123 + PDalpha2L*Gt223 + PDalpha3L*Gt323 
      - PDPDalpha23 + alphaL*R23;
    
    CCTK_REAL Ats33 CCTK_ATTRIBUTE_UNUSED = 4*PDalpha3L*cdphi3 + 
      PDalpha1L*Gt133 + PDalpha2L*Gt233 + PDalpha3L*Gt333 - PDPDalpha33 + 
      alphaL*R33;
    
    CCTK_REAL trAts CCTK_ATTRIBUTE_UNUSED = Ats11*gu11 + 2*Ats12*gu12 + 
      2*Ats13*gu13 + Ats22*gu22 + 2*Ats23*gu23 + Ats33*gu33;
    
    CCTK_REAL At11rhsL CCTK_ATTRIBUTE_UNUSED = 2*At11L*PDbeta11L + 
      2*At12L*PDbeta21L + 2*At13L*PDbeta31L - 
      0.666666666666666666666666666667*At11L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + alphaL*(At11L*trKL - 2*(At11L*Atm11 + At12L*Atm21 + 
      At13L*Atm31)) + GDissAt11 + beta1L*PDuAt111 + beta2L*PDuAt112 + 
      beta3L*PDuAt113 + em4phi*(Ats11 - 
      0.333333333333333333333333333333*g11*trAts) - 8*alphaL*em4phi*Pi*(eTxxL 
      - 0.333333333333333333333333333333*g11*trS);
    
    CCTK_REAL At12rhsL CCTK_ATTRIBUTE_UNUSED = At12L*PDbeta11L + 
      At11L*PDbeta12L + At22L*PDbeta21L + At12L*PDbeta22L + At23L*PDbeta31L + 
      At13L*PDbeta32L - 0.666666666666666666666666666667*At12L*(PDbeta11L + 
      PDbeta22L + PDbeta33L) + alphaL*(At12L*trKL - 2*(At11L*Atm12 + 
      At12L*Atm22 + At13L*Atm32)) + GDissAt12 + beta1L*PDuAt121 + 
      beta2L*PDuAt122 + beta3L*PDuAt123 + em4phi*(Ats12 - 
      0.333333333333333333333333333333*g12*trAts) - 8*alphaL*em4phi*Pi*(eTxyL 
      - 0.333333333333333333333333333333*g12*trS);
    
    CCTK_REAL At13rhsL CCTK_ATTRIBUTE_UNUSED = At13L*PDbeta11L + 
      At11L*PDbeta13L + At23L*PDbeta21L + At12L*PDbeta23L + At33L*PDbeta31L + 
      At13L*PDbeta33L - 0.666666666666666666666666666667*At13L*(PDbeta11L + 
      PDbeta22L + PDbeta33L) + alphaL*(At13L*trKL - 2*(At11L*Atm13 + 
      At12L*Atm23 + At13L*Atm33)) + GDissAt13 + beta1L*PDuAt131 + 
      beta2L*PDuAt132 + beta3L*PDuAt133 + em4phi*(Ats13 - 
      0.333333333333333333333333333333*g13*trAts) - 8*alphaL*em4phi*Pi*(eTxzL 
      - 0.333333333333333333333333333333*g13*trS);
    
    CCTK_REAL At22rhsL CCTK_ATTRIBUTE_UNUSED = 2*At12L*PDbeta12L + 
      2*At22L*PDbeta22L + 2*At23L*PDbeta32L - 
      0.666666666666666666666666666667*At22L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + alphaL*(At22L*trKL - 2*(At12L*Atm12 + At22L*Atm22 + 
      At23L*Atm32)) + GDissAt22 + beta1L*PDuAt221 + beta2L*PDuAt222 + 
      beta3L*PDuAt223 + em4phi*(Ats22 - 
      0.333333333333333333333333333333*g22*trAts) - 8*alphaL*em4phi*Pi*(eTyyL 
      - 0.333333333333333333333333333333*g22*trS);
    
    CCTK_REAL At23rhsL CCTK_ATTRIBUTE_UNUSED = At13L*PDbeta12L + 
      At12L*PDbeta13L + At23L*PDbeta22L + At22L*PDbeta23L + At33L*PDbeta32L + 
      At23L*PDbeta33L - 0.666666666666666666666666666667*At23L*(PDbeta11L + 
      PDbeta22L + PDbeta33L) + alphaL*(At23L*trKL - 2*(At12L*Atm13 + 
      At22L*Atm23 + At23L*Atm33)) + GDissAt23 + beta1L*PDuAt231 + 
      beta2L*PDuAt232 + beta3L*PDuAt233 + em4phi*(Ats23 - 
      0.333333333333333333333333333333*g23*trAts) - 8*alphaL*em4phi*Pi*(eTyzL 
      - 0.333333333333333333333333333333*g23*trS);
    
    CCTK_REAL At33rhsL CCTK_ATTRIBUTE_UNUSED = 2*At13L*PDbeta13L + 
      2*At23L*PDbeta23L + 2*At33L*PDbeta33L - 
      0.666666666666666666666666666667*At33L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + alphaL*(At33L*trKL - 2*(At13L*Atm13 + At23L*Atm23 + 
      At33L*Atm33)) + GDissAt33 + beta1L*PDuAt331 + beta2L*PDuAt332 + 
      beta3L*PDuAt333 + em4phi*(Ats33 - 
      0.333333333333333333333333333333*g33*trAts) - 8*alphaL*em4phi*Pi*(eTzzL 
      - 0.333333333333333333333333333333*g33*trS);
    
    CCTK_REAL dotalpha CCTK_ATTRIBUTE_UNUSED = -(harmonicF*IfThen(evolveA 
      != 0,AL,trKL + (-1 + alphaL)*alphaDriver)*pow(alphaL,harmonicN));
    
    CCTK_REAL alpharhsL CCTK_ATTRIBUTE_UNUSED = dotalpha + GDissalpha + 
      IfThen(advectLapse != 0,beta1L*PDualpha1 + beta2L*PDualpha2 + 
      beta3L*PDualpha3,0);
    
    CCTK_REAL ArhsL CCTK_ATTRIBUTE_UNUSED = IfThen(evolveA != 0,dottrK + 
      GDissA + IfThen(fixAdvectionTerms == 0 && advectLapse != 0,beta1L*PDuA1 
      + beta2L*PDuA2 + beta3L*PDuA3,0) - alphaDriver*(AL + 
      IfThen(fixAdvectionTerms != 0 && advectLapse != 0,-((beta1L*PDualpha1 + 
      beta2L*PDualpha2 + 
      beta3L*PDualpha3)*pow(alphaL,-harmonicN)*pow(harmonicF,-1)),0)) + 
      IfThen(fixAdvectionTerms != 0,beta1L*PDutrK1 + beta2L*PDutrK2 + 
      beta3L*PDutrK3,0),0);
    
    CCTK_REAL betaDriverValue CCTK_ATTRIBUTE_UNUSED = 
      IfThen(useSpatialBetaDriver != 
      0,betaDriver*spatialBetaDriverRadius*pow(fmax(rL,spatialBetaDriverRadius),-1),betaDriver);
    
    CCTK_REAL shiftGammaCoeffValue CCTK_ATTRIBUTE_UNUSED = 
      IfThen(useSpatialShiftGammaCoeff != 0,shiftGammaCoeff*fmin(1,exp(1 - 
      rL*pow(spatialShiftGammaCoeffRadius,-1))),shiftGammaCoeff);
    
    CCTK_REAL ddetgt1 CCTK_ATTRIBUTE_UNUSED = PDgt111L*gtu11 + 
      2*PDgt121L*gtu12 + 2*PDgt131L*gtu13 + PDgt221L*gtu22 + 2*PDgt231L*gtu23 
      + PDgt331L*gtu33;
    
    CCTK_REAL ddetgt2 CCTK_ATTRIBUTE_UNUSED = PDgt112L*gtu11 + 
      2*PDgt122L*gtu12 + 2*PDgt132L*gtu13 + PDgt222L*gtu22 + 2*PDgt232L*gtu23 
      + PDgt332L*gtu33;
    
    CCTK_REAL ddetgt3 CCTK_ATTRIBUTE_UNUSED = PDgt113L*gtu11 + 
      2*PDgt123L*gtu12 + 2*PDgt133L*gtu13 + PDgt223L*gtu22 + 2*PDgt233L*gtu23 
      + PDgt333L*gtu33;
    
    CCTK_REAL dotbeta1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dotbeta2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dotbeta3 CCTK_ATTRIBUTE_UNUSED;
    
    if (shiftFormulation == 0)
    {
      dotbeta1 = shiftGammaCoeffValue*IfThen(evolveB != 0,B1L,Xt1L - 
        beta1L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
      
      dotbeta2 = shiftGammaCoeffValue*IfThen(evolveB != 0,B2L,Xt2L - 
        beta2L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
      
      dotbeta3 = shiftGammaCoeffValue*IfThen(evolveB != 0,B3L,Xt3L - 
        beta3L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
    }
    else
    {
      dotbeta1 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu11 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu12 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu13));
      
      dotbeta2 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu12 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu22 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu23));
      
      dotbeta3 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu13 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu23 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu33));
    }
    
    CCTK_REAL beta1rhsL CCTK_ATTRIBUTE_UNUSED = dotbeta1 + GDissbeta1 + 
      IfThen(advectShift != 0,beta1L*PDubeta11 + beta2L*PDubeta12 + 
      beta3L*PDubeta13,0);
    
    CCTK_REAL beta2rhsL CCTK_ATTRIBUTE_UNUSED = dotbeta2 + GDissbeta2 + 
      IfThen(advectShift != 0,beta1L*PDubeta21 + beta2L*PDubeta22 + 
      beta3L*PDubeta23,0);
    
    CCTK_REAL beta3rhsL CCTK_ATTRIBUTE_UNUSED = dotbeta3 + GDissbeta3 + 
      IfThen(advectShift != 0,beta1L*PDubeta31 + beta2L*PDubeta32 + 
      beta3L*PDubeta33,0);
    
    CCTK_REAL B1rhsL CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL B2rhsL CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL B3rhsL CCTK_ATTRIBUTE_UNUSED;
    
    if (evolveB != 0)
    {
      B1rhsL = dotXt1 + GDissB1 + IfThen(fixAdvectionTerms == 0 && 
        advectShift != 0,beta1L*PDuB11 + beta2L*PDuB12 + beta3L*PDuB13,0) - 
        betaDriverValue*(B1L + IfThen(fixAdvectionTerms != 0 && advectShift != 
        0 && shiftGammaCoeffValue != 0.,(beta1L*PDubeta11 + beta2L*PDubeta12 + 
        beta3L*PDubeta13)*pow(alphaL,-shiftAlphaPower)*pow(shiftGammaCoeffValue,-1),0)) 
        + IfThen(fixAdvectionTerms != 0,beta1L*PDuXt11 + beta2L*PDuXt12 + 
        beta3L*PDuXt13,0);
      
      B2rhsL = dotXt2 + GDissB2 + IfThen(fixAdvectionTerms == 0 && 
        advectShift != 0,beta1L*PDuB21 + beta2L*PDuB22 + beta3L*PDuB23,0) - 
        betaDriverValue*(B2L + IfThen(fixAdvectionTerms != 0 && advectShift != 
        0 && shiftGammaCoeffValue != 0.,(beta1L*PDubeta21 + beta2L*PDubeta22 + 
        beta3L*PDubeta23)*pow(alphaL,-shiftAlphaPower)*pow(shiftGammaCoeffValue,-1),0)) 
        + IfThen(fixAdvectionTerms != 0,beta1L*PDuXt21 + beta2L*PDuXt22 + 
        beta3L*PDuXt23,0);
      
      B3rhsL = dotXt3 + GDissB3 + IfThen(fixAdvectionTerms == 0 && 
        advectShift != 0,beta1L*PDuB31 + beta2L*PDuB32 + beta3L*PDuB33,0) - 
        betaDriverValue*(B3L + IfThen(fixAdvectionTerms != 0 && advectShift != 
        0 && shiftGammaCoeffValue != 0.,(beta1L*PDubeta31 + beta2L*PDubeta32 + 
        beta3L*PDubeta33)*pow(alphaL,-shiftAlphaPower)*pow(shiftGammaCoeffValue,-1),0)) 
        + IfThen(fixAdvectionTerms != 0,beta1L*PDuXt31 + beta2L*PDuXt32 + 
        beta3L*PDuXt33,0);
    }
    else
    {
      B1rhsL = 0;
      
      B2rhsL = 0;
      
      B3rhsL = 0;
    }
    /* Copy local copies back to grid functions */
    alpharhs[index] = alpharhsL;
    Arhs[index] = ArhsL;
    At11rhs[index] = At11rhsL;
    At12rhs[index] = At12rhsL;
    At13rhs[index] = At13rhsL;
    At22rhs[index] = At22rhsL;
    At23rhs[index] = At23rhsL;
    At33rhs[index] = At33rhsL;
    B1rhs[index] = B1rhsL;
    B2rhs[index] = B2rhsL;
    B3rhs[index] = B3rhsL;
    beta1rhs[index] = beta1rhsL;
    beta2rhs[index] = beta2rhsL;
    beta3rhs[index] = beta3rhsL;
    gt11rhs[index] = gt11rhsL;
    gt12rhs[index] = gt12rhsL;
    gt13rhs[index] = gt13rhsL;
    gt22rhs[index] = gt22rhsL;
    gt23rhs[index] = gt23rhsL;
    gt33rhs[index] = gt33rhsL;
    phiWrhs[index] = phiWrhsL;
    trKrhs[index] = trKrhsL;
    Xt1rhs[index] = Xt1rhsL;
    Xt2rhs[index] = Xt2rhsL;
    Xt3rhs[index] = Xt3rhsL;
  }
  CCTK_ENDLOOP3(ML_BSSN_FD4_EvolutionInterior);
}
extern "C" void ML_BSSN_FD4_EvolutionInterior(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_FD4_EvolutionInterior_Body");
  }
  if (cctk_iteration % ML_BSSN_FD4_EvolutionInterior_calc_every != ML_BSSN_FD4_EvolutionInterior_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "grid::coordinates",
    "ML_BSSN_FD4::ML_confac",
    "ML_BSSN_FD4::ML_confacrhs",
    "ML_BSSN_FD4::ML_curv",
    "ML_BSSN_FD4::ML_curvrhs",
    "ML_BSSN_FD4::ML_dconfac",
    "ML_BSSN_FD4::ML_dlapse",
    "ML_BSSN_FD4::ML_dmetric",
    "ML_BSSN_FD4::ML_dshift",
    "ML_BSSN_FD4::ML_dtlapse",
    "ML_BSSN_FD4::ML_dtlapserhs",
    "ML_BSSN_FD4::ML_dtshift",
    "ML_BSSN_FD4::ML_dtshiftrhs",
    "ML_BSSN_FD4::ML_Gamma",
    "ML_BSSN_FD4::ML_Gammarhs",
    "ML_BSSN_FD4::ML_lapse",
    "ML_BSSN_FD4::ML_lapserhs",
    "ML_BSSN_FD4::ML_metric",
    "ML_BSSN_FD4::ML_metricrhs",
    "ML_BSSN_FD4::ML_shift",
    "ML_BSSN_FD4::ML_shiftrhs",
    "ML_BSSN_FD4::ML_trace_curv",
    "ML_BSSN_FD4::ML_trace_curvrhs",
    "ML_BSSN_FD4::ML_volume_form"};
  AssertGroupStorage(cctkGH, "ML_BSSN_FD4_EvolutionInterior", 24, groups);
  
  
  LoopOverInterior(cctkGH, ML_BSSN_FD4_EvolutionInterior_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_FD4_EvolutionInterior_Body");
  }
}

} // namespace ML_BSSN_FD4
