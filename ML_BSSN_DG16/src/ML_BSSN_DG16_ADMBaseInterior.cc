/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_BSSN_DG16 {

extern "C" void ML_BSSN_DG16_ADMBaseInterior_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_DG16_ADMBaseInterior_calc_every != ML_BSSN_DG16_ADMBaseInterior_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ADMBase::dtlapse","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ADMBase::dtlapse.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ADMBase::dtshift","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ADMBase::dtshift.");
  return;
}

static void ML_BSSN_DG16_ADMBaseInterior_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_BSSN_DG16_ADMBaseInterior,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL AL CCTK_ATTRIBUTE_UNUSED = A[index];
    CCTK_REAL alphaL CCTK_ATTRIBUTE_UNUSED = alpha[index];
    CCTK_REAL B1L CCTK_ATTRIBUTE_UNUSED = B1[index];
    CCTK_REAL B2L CCTK_ATTRIBUTE_UNUSED = B2[index];
    CCTK_REAL B3L CCTK_ATTRIBUTE_UNUSED = B3[index];
    CCTK_REAL beta1L CCTK_ATTRIBUTE_UNUSED = beta1[index];
    CCTK_REAL beta2L CCTK_ATTRIBUTE_UNUSED = beta2[index];
    CCTK_REAL beta3L CCTK_ATTRIBUTE_UNUSED = beta3[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL PDalpha1L CCTK_ATTRIBUTE_UNUSED = PDalpha1[index];
    CCTK_REAL PDalpha2L CCTK_ATTRIBUTE_UNUSED = PDalpha2[index];
    CCTK_REAL PDalpha3L CCTK_ATTRIBUTE_UNUSED = PDalpha3[index];
    CCTK_REAL PDgt111L CCTK_ATTRIBUTE_UNUSED = PDgt111[index];
    CCTK_REAL PDgt112L CCTK_ATTRIBUTE_UNUSED = PDgt112[index];
    CCTK_REAL PDgt113L CCTK_ATTRIBUTE_UNUSED = PDgt113[index];
    CCTK_REAL PDgt121L CCTK_ATTRIBUTE_UNUSED = PDgt121[index];
    CCTK_REAL PDgt122L CCTK_ATTRIBUTE_UNUSED = PDgt122[index];
    CCTK_REAL PDgt123L CCTK_ATTRIBUTE_UNUSED = PDgt123[index];
    CCTK_REAL PDgt131L CCTK_ATTRIBUTE_UNUSED = PDgt131[index];
    CCTK_REAL PDgt132L CCTK_ATTRIBUTE_UNUSED = PDgt132[index];
    CCTK_REAL PDgt133L CCTK_ATTRIBUTE_UNUSED = PDgt133[index];
    CCTK_REAL PDgt221L CCTK_ATTRIBUTE_UNUSED = PDgt221[index];
    CCTK_REAL PDgt222L CCTK_ATTRIBUTE_UNUSED = PDgt222[index];
    CCTK_REAL PDgt223L CCTK_ATTRIBUTE_UNUSED = PDgt223[index];
    CCTK_REAL PDgt231L CCTK_ATTRIBUTE_UNUSED = PDgt231[index];
    CCTK_REAL PDgt232L CCTK_ATTRIBUTE_UNUSED = PDgt232[index];
    CCTK_REAL PDgt233L CCTK_ATTRIBUTE_UNUSED = PDgt233[index];
    CCTK_REAL PDgt331L CCTK_ATTRIBUTE_UNUSED = PDgt331[index];
    CCTK_REAL PDgt332L CCTK_ATTRIBUTE_UNUSED = PDgt332[index];
    CCTK_REAL PDgt333L CCTK_ATTRIBUTE_UNUSED = PDgt333[index];
    CCTK_REAL PDphiW1L CCTK_ATTRIBUTE_UNUSED = PDphiW1[index];
    CCTK_REAL PDphiW2L CCTK_ATTRIBUTE_UNUSED = PDphiW2[index];
    CCTK_REAL PDphiW3L CCTK_ATTRIBUTE_UNUSED = PDphiW3[index];
    CCTK_REAL phiWL CCTK_ATTRIBUTE_UNUSED = phiW[index];
    CCTK_REAL rL CCTK_ATTRIBUTE_UNUSED = r[index];
    CCTK_REAL trKL CCTK_ATTRIBUTE_UNUSED = trK[index];
    CCTK_REAL Xt1L CCTK_ATTRIBUTE_UNUSED = Xt1[index];
    CCTK_REAL Xt2L CCTK_ATTRIBUTE_UNUSED = Xt2[index];
    CCTK_REAL Xt3L CCTK_ATTRIBUTE_UNUSED = Xt3[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDualpha1 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(alpha,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(alpha,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(alpha,0,0,0) + 
      91.995423705517011185543249491*GFOffset(alpha,1,0,0) - 
      36.825792804916105238285776948*GFOffset(alpha,2,0,0) + 
      21.042577052349419249515496693*GFOffset(alpha,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(alpha,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(alpha,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(alpha,6,0,0) + 
      6.225793703001792996013745096*GFOffset(alpha,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(alpha,8,0,0) + 
      4.242018040981331373618358215*GFOffset(alpha,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(alpha,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(alpha,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(alpha,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(alpha,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(alpha,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(alpha,15,0,0) - 
      0.5*GFOffset(alpha,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(alpha,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(alpha,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(alpha,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(alpha,3,0,0) - 
      4.376597384264969120661066707*GFOffset(alpha,4,0,0) + 
      3.303076725656509756145262224*GFOffset(alpha,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(alpha,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(alpha,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(alpha,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(alpha,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(alpha,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(alpha,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(alpha,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(alpha,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(alpha,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(alpha,15,0,0),0) + IfThen(ti 
      == 2,3.4189873913829435992478256345*GFOffset(alpha,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(alpha,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(alpha,1,0,0) - 
      6.003906719792868453304381935*GFOffset(alpha,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(alpha,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(alpha,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(alpha,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(alpha,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(alpha,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(alpha,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(alpha,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(alpha,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(alpha,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(alpha,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(alpha,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(alpha,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(alpha,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(alpha,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(alpha,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(alpha,1,0,0) - 
      4.390240639181825578641202719*GFOffset(alpha,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(alpha,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(alpha,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(alpha,5,0,0) - 
      1.29435140870084806656280919*GFOffset(alpha,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(alpha,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(alpha,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(alpha,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(alpha,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(alpha,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(alpha,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(alpha,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(alpha,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(alpha,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(alpha,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(alpha,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(alpha,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(alpha,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(alpha,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(alpha,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(alpha,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(alpha,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(alpha,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(alpha,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(alpha,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(alpha,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(alpha,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(alpha,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(alpha,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(alpha,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(alpha,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(alpha,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(alpha,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(alpha,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(alpha,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(alpha,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(alpha,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(alpha,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(alpha,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(alpha,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(alpha,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(alpha,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(alpha,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(alpha,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(alpha,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(alpha,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(alpha,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(alpha,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(alpha,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(alpha,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(alpha,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(alpha,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(alpha,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(alpha,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(alpha,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(alpha,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(alpha,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(alpha,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(alpha,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(alpha,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(alpha,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(alpha,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(alpha,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(alpha,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(alpha,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(alpha,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(alpha,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(alpha,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(alpha,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(alpha,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(alpha,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(alpha,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(alpha,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(alpha,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(alpha,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(alpha,9,0,0),0) + IfThen(ti 
      == 8,0.196380615234375*GFOffset(alpha,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(alpha,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(alpha,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(alpha,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(alpha,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(alpha,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(alpha,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(alpha,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(alpha,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(alpha,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(alpha,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(alpha,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(alpha,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(alpha,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(alpha,7,0,0) - 
      0.196380615234375*GFOffset(alpha,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(alpha,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(alpha,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(alpha,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(alpha,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(alpha,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(alpha,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(alpha,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(alpha,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(alpha,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(alpha,1,0,0) - 
      2.630489733142368322167942483*GFOffset(alpha,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(alpha,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(alpha,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(alpha,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(alpha,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(alpha,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(alpha,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(alpha,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(alpha,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(alpha,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(alpha,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(alpha,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(alpha,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(alpha,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(alpha,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(alpha,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(alpha,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(alpha,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(alpha,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(alpha,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(alpha,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(alpha,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(alpha,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(alpha,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(alpha,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(alpha,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(alpha,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(alpha,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(alpha,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(alpha,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(alpha,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(alpha,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(alpha,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(alpha,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(alpha,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(alpha,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(alpha,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(alpha,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(alpha,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(alpha,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(alpha,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(alpha,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(alpha,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(alpha,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(alpha,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(alpha,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(alpha,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(alpha,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(alpha,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(alpha,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(alpha,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(alpha,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(alpha,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(alpha,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(alpha,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(alpha,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(alpha,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(alpha,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(alpha,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(alpha,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(alpha,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(alpha,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(alpha,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(alpha,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(alpha,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(alpha,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(alpha,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(alpha,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(alpha,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(alpha,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(alpha,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(alpha,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(alpha,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(alpha,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(alpha,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(alpha,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(alpha,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(alpha,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(alpha,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(alpha,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(alpha,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(alpha,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(alpha,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(alpha,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(alpha,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(alpha,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(alpha,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(alpha,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(alpha,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(alpha,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(alpha,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(alpha,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(alpha,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(alpha,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(alpha,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(alpha,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(alpha,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(alpha,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(alpha,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(alpha,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(alpha,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(alpha,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(alpha,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(alpha,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(alpha,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(alpha,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(alpha,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(alpha,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(alpha,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(alpha,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(alpha,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(alpha,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(alpha,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(alpha,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(alpha,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(alpha,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(alpha,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(alpha,-1,0,0) + 
      68.*GFOffset(alpha,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(alpha,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(alpha,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDualpha2 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(alpha,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(alpha,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(alpha,0,0,0) + 
      91.995423705517011185543249491*GFOffset(alpha,0,1,0) - 
      36.825792804916105238285776948*GFOffset(alpha,0,2,0) + 
      21.042577052349419249515496693*GFOffset(alpha,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(alpha,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(alpha,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(alpha,0,6,0) + 
      6.225793703001792996013745096*GFOffset(alpha,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(alpha,0,8,0) + 
      4.242018040981331373618358215*GFOffset(alpha,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(alpha,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(alpha,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(alpha,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(alpha,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(alpha,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(alpha,0,15,0) - 
      0.5*GFOffset(alpha,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(alpha,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(alpha,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(alpha,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(alpha,0,3,0) - 
      4.376597384264969120661066707*GFOffset(alpha,0,4,0) + 
      3.303076725656509756145262224*GFOffset(alpha,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(alpha,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(alpha,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(alpha,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(alpha,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(alpha,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(alpha,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(alpha,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(alpha,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(alpha,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(alpha,0,15,0),0) + IfThen(tj 
      == 2,3.4189873913829435992478256345*GFOffset(alpha,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(alpha,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(alpha,0,1,0) - 
      6.003906719792868453304381935*GFOffset(alpha,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(alpha,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(alpha,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(alpha,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(alpha,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(alpha,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(alpha,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(alpha,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(alpha,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(alpha,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(alpha,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(alpha,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(alpha,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(alpha,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(alpha,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(alpha,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(alpha,0,1,0) - 
      4.390240639181825578641202719*GFOffset(alpha,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(alpha,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(alpha,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(alpha,0,5,0) - 
      1.29435140870084806656280919*GFOffset(alpha,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(alpha,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(alpha,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(alpha,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(alpha,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(alpha,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(alpha,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(alpha,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(alpha,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(alpha,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(alpha,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(alpha,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(alpha,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(alpha,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(alpha,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(alpha,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(alpha,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(alpha,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(alpha,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(alpha,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(alpha,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(alpha,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(alpha,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(alpha,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(alpha,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(alpha,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(alpha,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(alpha,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(alpha,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(alpha,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(alpha,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(alpha,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(alpha,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(alpha,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(alpha,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(alpha,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(alpha,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(alpha,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(alpha,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(alpha,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(alpha,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(alpha,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(alpha,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(alpha,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(alpha,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(alpha,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(alpha,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(alpha,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(alpha,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(alpha,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(alpha,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(alpha,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(alpha,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(alpha,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(alpha,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(alpha,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(alpha,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(alpha,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(alpha,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(alpha,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(alpha,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(alpha,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(alpha,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(alpha,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(alpha,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(alpha,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(alpha,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(alpha,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(alpha,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(alpha,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(alpha,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(alpha,0,9,0),0) + IfThen(tj 
      == 8,0.196380615234375*GFOffset(alpha,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(alpha,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(alpha,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(alpha,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(alpha,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(alpha,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(alpha,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(alpha,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(alpha,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(alpha,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(alpha,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(alpha,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(alpha,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(alpha,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(alpha,0,7,0) - 
      0.196380615234375*GFOffset(alpha,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(alpha,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(alpha,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(alpha,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(alpha,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(alpha,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(alpha,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(alpha,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(alpha,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(alpha,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(alpha,0,1,0) - 
      2.630489733142368322167942483*GFOffset(alpha,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(alpha,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(alpha,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(alpha,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(alpha,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(alpha,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(alpha,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(alpha,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(alpha,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(alpha,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(alpha,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(alpha,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(alpha,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(alpha,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(alpha,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(alpha,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(alpha,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(alpha,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(alpha,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(alpha,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(alpha,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(alpha,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(alpha,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(alpha,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(alpha,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(alpha,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(alpha,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(alpha,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(alpha,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(alpha,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(alpha,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(alpha,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(alpha,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(alpha,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(alpha,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(alpha,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(alpha,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(alpha,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(alpha,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(alpha,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(alpha,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(alpha,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(alpha,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(alpha,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(alpha,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(alpha,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(alpha,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(alpha,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(alpha,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(alpha,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(alpha,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(alpha,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(alpha,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(alpha,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(alpha,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(alpha,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(alpha,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(alpha,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(alpha,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(alpha,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(alpha,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(alpha,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(alpha,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(alpha,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(alpha,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(alpha,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(alpha,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(alpha,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(alpha,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(alpha,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(alpha,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(alpha,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(alpha,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(alpha,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(alpha,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(alpha,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(alpha,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(alpha,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(alpha,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(alpha,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(alpha,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(alpha,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(alpha,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(alpha,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(alpha,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(alpha,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(alpha,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(alpha,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(alpha,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(alpha,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(alpha,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(alpha,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(alpha,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(alpha,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(alpha,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(alpha,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(alpha,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(alpha,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(alpha,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(alpha,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(alpha,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(alpha,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(alpha,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(alpha,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(alpha,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(alpha,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(alpha,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(alpha,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(alpha,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(alpha,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(alpha,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(alpha,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(alpha,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(alpha,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(alpha,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(alpha,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(alpha,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(alpha,0,-1,0) + 
      68.*GFOffset(alpha,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(alpha,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(alpha,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDualpha3 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(alpha,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(alpha,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(alpha,0,0,0) + 
      91.995423705517011185543249491*GFOffset(alpha,0,0,1) - 
      36.825792804916105238285776948*GFOffset(alpha,0,0,2) + 
      21.042577052349419249515496693*GFOffset(alpha,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(alpha,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(alpha,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(alpha,0,0,6) + 
      6.225793703001792996013745096*GFOffset(alpha,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(alpha,0,0,8) + 
      4.242018040981331373618358215*GFOffset(alpha,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(alpha,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(alpha,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(alpha,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(alpha,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(alpha,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(alpha,0,0,15) - 
      0.5*GFOffset(alpha,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(alpha,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(alpha,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(alpha,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(alpha,0,0,3) - 
      4.376597384264969120661066707*GFOffset(alpha,0,0,4) + 
      3.303076725656509756145262224*GFOffset(alpha,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(alpha,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(alpha,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(alpha,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(alpha,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(alpha,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(alpha,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(alpha,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(alpha,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(alpha,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(alpha,0,0,15),0) + IfThen(tk 
      == 2,3.4189873913829435992478256345*GFOffset(alpha,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(alpha,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(alpha,0,0,1) - 
      6.003906719792868453304381935*GFOffset(alpha,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(alpha,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(alpha,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(alpha,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(alpha,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(alpha,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(alpha,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(alpha,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(alpha,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(alpha,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(alpha,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(alpha,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(alpha,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(alpha,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(alpha,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(alpha,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(alpha,0,0,1) - 
      4.390240639181825578641202719*GFOffset(alpha,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(alpha,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(alpha,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(alpha,0,0,5) - 
      1.29435140870084806656280919*GFOffset(alpha,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(alpha,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(alpha,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(alpha,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(alpha,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(alpha,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(alpha,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(alpha,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(alpha,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(alpha,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(alpha,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(alpha,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(alpha,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(alpha,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(alpha,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(alpha,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(alpha,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(alpha,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(alpha,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(alpha,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(alpha,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(alpha,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(alpha,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(alpha,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(alpha,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(alpha,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(alpha,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(alpha,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(alpha,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(alpha,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(alpha,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(alpha,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(alpha,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(alpha,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(alpha,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(alpha,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(alpha,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(alpha,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(alpha,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(alpha,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(alpha,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(alpha,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(alpha,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(alpha,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(alpha,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(alpha,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(alpha,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(alpha,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(alpha,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(alpha,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(alpha,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(alpha,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(alpha,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(alpha,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(alpha,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(alpha,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(alpha,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(alpha,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(alpha,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(alpha,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(alpha,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(alpha,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(alpha,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(alpha,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(alpha,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(alpha,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(alpha,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(alpha,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(alpha,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(alpha,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(alpha,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(alpha,0,0,9),0) + IfThen(tk 
      == 8,0.196380615234375*GFOffset(alpha,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(alpha,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(alpha,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(alpha,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(alpha,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(alpha,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(alpha,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(alpha,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(alpha,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(alpha,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(alpha,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(alpha,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(alpha,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(alpha,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(alpha,0,0,7) - 
      0.196380615234375*GFOffset(alpha,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(alpha,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(alpha,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(alpha,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(alpha,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(alpha,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(alpha,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(alpha,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(alpha,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(alpha,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(alpha,0,0,1) - 
      2.630489733142368322167942483*GFOffset(alpha,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(alpha,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(alpha,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(alpha,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(alpha,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(alpha,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(alpha,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(alpha,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(alpha,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(alpha,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(alpha,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(alpha,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(alpha,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(alpha,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(alpha,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(alpha,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(alpha,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(alpha,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(alpha,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(alpha,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(alpha,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(alpha,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(alpha,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(alpha,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(alpha,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(alpha,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(alpha,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(alpha,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(alpha,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(alpha,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(alpha,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(alpha,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(alpha,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(alpha,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(alpha,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(alpha,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(alpha,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(alpha,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(alpha,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(alpha,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(alpha,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(alpha,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(alpha,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(alpha,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(alpha,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(alpha,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(alpha,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(alpha,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(alpha,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(alpha,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(alpha,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(alpha,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(alpha,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(alpha,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(alpha,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(alpha,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(alpha,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(alpha,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(alpha,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(alpha,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(alpha,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(alpha,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(alpha,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(alpha,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(alpha,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(alpha,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(alpha,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(alpha,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(alpha,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(alpha,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(alpha,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(alpha,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(alpha,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(alpha,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(alpha,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(alpha,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(alpha,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(alpha,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(alpha,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(alpha,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(alpha,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(alpha,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(alpha,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(alpha,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(alpha,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(alpha,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(alpha,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(alpha,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(alpha,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(alpha,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(alpha,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(alpha,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(alpha,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(alpha,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(alpha,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(alpha,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(alpha,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(alpha,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(alpha,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(alpha,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(alpha,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(alpha,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(alpha,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(alpha,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(alpha,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(alpha,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(alpha,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(alpha,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(alpha,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(alpha,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(alpha,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(alpha,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(alpha,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(alpha,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(alpha,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(alpha,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(alpha,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(alpha,0,0,-1) + 
      68.*GFOffset(alpha,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(alpha,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(alpha,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDubeta11 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(beta1,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(beta1,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(beta1,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta1,1,0,0) - 
      36.825792804916105238285776948*GFOffset(beta1,2,0,0) + 
      21.042577052349419249515496693*GFOffset(beta1,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(beta1,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(beta1,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(beta1,6,0,0) + 
      6.225793703001792996013745096*GFOffset(beta1,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(beta1,8,0,0) + 
      4.242018040981331373618358215*GFOffset(beta1,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(beta1,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(beta1,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(beta1,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(beta1,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(beta1,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(beta1,15,0,0) - 
      0.5*GFOffset(beta1,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(beta1,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(beta1,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(beta1,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(beta1,3,0,0) - 
      4.376597384264969120661066707*GFOffset(beta1,4,0,0) + 
      3.303076725656509756145262224*GFOffset(beta1,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(beta1,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(beta1,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(beta1,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(beta1,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(beta1,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(beta1,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(beta1,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(beta1,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(beta1,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(beta1,15,0,0),0) + IfThen(ti 
      == 2,3.4189873913829435992478256345*GFOffset(beta1,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(beta1,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(beta1,1,0,0) - 
      6.003906719792868453304381935*GFOffset(beta1,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(beta1,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(beta1,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(beta1,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(beta1,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(beta1,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(beta1,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(beta1,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(beta1,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(beta1,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(beta1,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(beta1,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(beta1,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(beta1,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(beta1,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(beta1,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(beta1,1,0,0) - 
      4.390240639181825578641202719*GFOffset(beta1,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(beta1,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(beta1,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(beta1,5,0,0) - 
      1.29435140870084806656280919*GFOffset(beta1,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(beta1,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(beta1,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(beta1,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(beta1,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(beta1,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(beta1,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(beta1,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(beta1,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(beta1,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(beta1,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(beta1,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(beta1,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(beta1,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(beta1,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(beta1,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(beta1,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(beta1,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(beta1,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(beta1,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(beta1,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(beta1,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(beta1,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(beta1,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(beta1,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(beta1,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(beta1,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(beta1,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(beta1,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(beta1,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(beta1,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(beta1,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(beta1,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(beta1,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(beta1,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(beta1,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(beta1,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(beta1,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(beta1,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(beta1,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(beta1,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(beta1,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(beta1,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(beta1,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(beta1,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(beta1,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(beta1,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(beta1,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(beta1,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(beta1,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(beta1,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(beta1,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(beta1,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(beta1,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(beta1,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(beta1,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(beta1,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(beta1,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(beta1,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(beta1,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(beta1,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(beta1,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(beta1,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(beta1,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(beta1,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(beta1,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(beta1,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(beta1,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(beta1,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(beta1,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(beta1,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(beta1,9,0,0),0) + IfThen(ti 
      == 8,0.196380615234375*GFOffset(beta1,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(beta1,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(beta1,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(beta1,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(beta1,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(beta1,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(beta1,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(beta1,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(beta1,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(beta1,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(beta1,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(beta1,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(beta1,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(beta1,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(beta1,7,0,0) - 
      0.196380615234375*GFOffset(beta1,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(beta1,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(beta1,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(beta1,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(beta1,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(beta1,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(beta1,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(beta1,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(beta1,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(beta1,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(beta1,1,0,0) - 
      2.630489733142368322167942483*GFOffset(beta1,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(beta1,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(beta1,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(beta1,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(beta1,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(beta1,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(beta1,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(beta1,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(beta1,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(beta1,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(beta1,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(beta1,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(beta1,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(beta1,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(beta1,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(beta1,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(beta1,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(beta1,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(beta1,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(beta1,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(beta1,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(beta1,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(beta1,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(beta1,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(beta1,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(beta1,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(beta1,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(beta1,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(beta1,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(beta1,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(beta1,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(beta1,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(beta1,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(beta1,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(beta1,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(beta1,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(beta1,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(beta1,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(beta1,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(beta1,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(beta1,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(beta1,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(beta1,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(beta1,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(beta1,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(beta1,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(beta1,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(beta1,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(beta1,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(beta1,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(beta1,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(beta1,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(beta1,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(beta1,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(beta1,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(beta1,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(beta1,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(beta1,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(beta1,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(beta1,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(beta1,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(beta1,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(beta1,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(beta1,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(beta1,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(beta1,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(beta1,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(beta1,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(beta1,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(beta1,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(beta1,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(beta1,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(beta1,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(beta1,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(beta1,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(beta1,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(beta1,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(beta1,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(beta1,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(beta1,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(beta1,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(beta1,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(beta1,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(beta1,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(beta1,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(beta1,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(beta1,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(beta1,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(beta1,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(beta1,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(beta1,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(beta1,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(beta1,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(beta1,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(beta1,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(beta1,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(beta1,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(beta1,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(beta1,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(beta1,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(beta1,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(beta1,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(beta1,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(beta1,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(beta1,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(beta1,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(beta1,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(beta1,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(beta1,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(beta1,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(beta1,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(beta1,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(beta1,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(beta1,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(beta1,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(beta1,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(beta1,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(beta1,-1,0,0) + 
      68.*GFOffset(beta1,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(beta1,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(beta1,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDubeta21 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(beta2,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(beta2,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(beta2,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta2,1,0,0) - 
      36.825792804916105238285776948*GFOffset(beta2,2,0,0) + 
      21.042577052349419249515496693*GFOffset(beta2,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(beta2,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(beta2,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(beta2,6,0,0) + 
      6.225793703001792996013745096*GFOffset(beta2,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(beta2,8,0,0) + 
      4.242018040981331373618358215*GFOffset(beta2,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(beta2,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(beta2,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(beta2,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(beta2,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(beta2,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(beta2,15,0,0) - 
      0.5*GFOffset(beta2,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(beta2,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(beta2,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(beta2,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(beta2,3,0,0) - 
      4.376597384264969120661066707*GFOffset(beta2,4,0,0) + 
      3.303076725656509756145262224*GFOffset(beta2,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(beta2,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(beta2,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(beta2,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(beta2,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(beta2,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(beta2,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(beta2,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(beta2,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(beta2,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(beta2,15,0,0),0) + IfThen(ti 
      == 2,3.4189873913829435992478256345*GFOffset(beta2,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(beta2,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(beta2,1,0,0) - 
      6.003906719792868453304381935*GFOffset(beta2,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(beta2,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(beta2,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(beta2,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(beta2,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(beta2,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(beta2,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(beta2,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(beta2,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(beta2,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(beta2,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(beta2,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(beta2,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(beta2,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(beta2,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(beta2,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(beta2,1,0,0) - 
      4.390240639181825578641202719*GFOffset(beta2,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(beta2,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(beta2,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(beta2,5,0,0) - 
      1.29435140870084806656280919*GFOffset(beta2,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(beta2,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(beta2,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(beta2,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(beta2,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(beta2,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(beta2,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(beta2,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(beta2,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(beta2,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(beta2,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(beta2,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(beta2,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(beta2,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(beta2,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(beta2,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(beta2,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(beta2,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(beta2,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(beta2,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(beta2,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(beta2,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(beta2,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(beta2,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(beta2,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(beta2,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(beta2,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(beta2,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(beta2,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(beta2,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(beta2,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(beta2,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(beta2,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(beta2,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(beta2,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(beta2,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(beta2,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(beta2,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(beta2,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(beta2,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(beta2,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(beta2,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(beta2,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(beta2,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(beta2,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(beta2,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(beta2,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(beta2,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(beta2,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(beta2,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(beta2,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(beta2,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(beta2,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(beta2,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(beta2,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(beta2,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(beta2,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(beta2,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(beta2,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(beta2,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(beta2,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(beta2,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(beta2,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(beta2,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(beta2,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(beta2,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(beta2,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(beta2,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(beta2,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(beta2,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(beta2,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(beta2,9,0,0),0) + IfThen(ti 
      == 8,0.196380615234375*GFOffset(beta2,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(beta2,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(beta2,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(beta2,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(beta2,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(beta2,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(beta2,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(beta2,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(beta2,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(beta2,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(beta2,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(beta2,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(beta2,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(beta2,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(beta2,7,0,0) - 
      0.196380615234375*GFOffset(beta2,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(beta2,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(beta2,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(beta2,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(beta2,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(beta2,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(beta2,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(beta2,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(beta2,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(beta2,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(beta2,1,0,0) - 
      2.630489733142368322167942483*GFOffset(beta2,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(beta2,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(beta2,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(beta2,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(beta2,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(beta2,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(beta2,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(beta2,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(beta2,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(beta2,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(beta2,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(beta2,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(beta2,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(beta2,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(beta2,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(beta2,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(beta2,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(beta2,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(beta2,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(beta2,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(beta2,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(beta2,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(beta2,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(beta2,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(beta2,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(beta2,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(beta2,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(beta2,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(beta2,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(beta2,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(beta2,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(beta2,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(beta2,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(beta2,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(beta2,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(beta2,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(beta2,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(beta2,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(beta2,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(beta2,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(beta2,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(beta2,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(beta2,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(beta2,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(beta2,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(beta2,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(beta2,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(beta2,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(beta2,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(beta2,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(beta2,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(beta2,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(beta2,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(beta2,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(beta2,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(beta2,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(beta2,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(beta2,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(beta2,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(beta2,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(beta2,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(beta2,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(beta2,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(beta2,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(beta2,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(beta2,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(beta2,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(beta2,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(beta2,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(beta2,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(beta2,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(beta2,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(beta2,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(beta2,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(beta2,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(beta2,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(beta2,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(beta2,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(beta2,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(beta2,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(beta2,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(beta2,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(beta2,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(beta2,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(beta2,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(beta2,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(beta2,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(beta2,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(beta2,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(beta2,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(beta2,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(beta2,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(beta2,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(beta2,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(beta2,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(beta2,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(beta2,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(beta2,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(beta2,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(beta2,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(beta2,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(beta2,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(beta2,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(beta2,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(beta2,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(beta2,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(beta2,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(beta2,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(beta2,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(beta2,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(beta2,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(beta2,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(beta2,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(beta2,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(beta2,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(beta2,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(beta2,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(beta2,-1,0,0) + 
      68.*GFOffset(beta2,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(beta2,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(beta2,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDubeta31 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(beta3,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(beta3,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(beta3,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta3,1,0,0) - 
      36.825792804916105238285776948*GFOffset(beta3,2,0,0) + 
      21.042577052349419249515496693*GFOffset(beta3,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(beta3,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(beta3,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(beta3,6,0,0) + 
      6.225793703001792996013745096*GFOffset(beta3,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(beta3,8,0,0) + 
      4.242018040981331373618358215*GFOffset(beta3,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(beta3,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(beta3,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(beta3,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(beta3,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(beta3,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(beta3,15,0,0) - 
      0.5*GFOffset(beta3,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(beta3,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(beta3,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(beta3,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(beta3,3,0,0) - 
      4.376597384264969120661066707*GFOffset(beta3,4,0,0) + 
      3.303076725656509756145262224*GFOffset(beta3,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(beta3,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(beta3,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(beta3,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(beta3,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(beta3,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(beta3,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(beta3,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(beta3,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(beta3,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(beta3,15,0,0),0) + IfThen(ti 
      == 2,3.4189873913829435992478256345*GFOffset(beta3,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(beta3,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(beta3,1,0,0) - 
      6.003906719792868453304381935*GFOffset(beta3,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(beta3,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(beta3,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(beta3,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(beta3,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(beta3,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(beta3,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(beta3,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(beta3,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(beta3,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(beta3,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(beta3,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(beta3,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(beta3,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(beta3,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(beta3,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(beta3,1,0,0) - 
      4.390240639181825578641202719*GFOffset(beta3,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(beta3,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(beta3,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(beta3,5,0,0) - 
      1.29435140870084806656280919*GFOffset(beta3,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(beta3,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(beta3,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(beta3,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(beta3,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(beta3,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(beta3,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(beta3,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(beta3,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(beta3,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(beta3,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(beta3,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(beta3,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(beta3,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(beta3,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(beta3,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(beta3,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(beta3,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(beta3,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(beta3,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(beta3,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(beta3,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(beta3,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(beta3,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(beta3,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(beta3,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(beta3,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(beta3,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(beta3,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(beta3,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(beta3,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(beta3,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(beta3,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(beta3,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(beta3,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(beta3,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(beta3,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(beta3,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(beta3,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(beta3,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(beta3,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(beta3,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(beta3,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(beta3,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(beta3,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(beta3,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(beta3,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(beta3,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(beta3,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(beta3,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(beta3,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(beta3,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(beta3,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(beta3,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(beta3,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(beta3,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(beta3,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(beta3,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(beta3,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(beta3,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(beta3,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(beta3,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(beta3,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(beta3,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(beta3,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(beta3,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(beta3,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(beta3,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(beta3,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(beta3,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(beta3,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(beta3,9,0,0),0) + IfThen(ti 
      == 8,0.196380615234375*GFOffset(beta3,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(beta3,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(beta3,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(beta3,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(beta3,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(beta3,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(beta3,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(beta3,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(beta3,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(beta3,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(beta3,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(beta3,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(beta3,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(beta3,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(beta3,7,0,0) - 
      0.196380615234375*GFOffset(beta3,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(beta3,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(beta3,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(beta3,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(beta3,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(beta3,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(beta3,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(beta3,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(beta3,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(beta3,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(beta3,1,0,0) - 
      2.630489733142368322167942483*GFOffset(beta3,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(beta3,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(beta3,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(beta3,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(beta3,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(beta3,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(beta3,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(beta3,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(beta3,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(beta3,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(beta3,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(beta3,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(beta3,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(beta3,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(beta3,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(beta3,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(beta3,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(beta3,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(beta3,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(beta3,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(beta3,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(beta3,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(beta3,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(beta3,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(beta3,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(beta3,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(beta3,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(beta3,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(beta3,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(beta3,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(beta3,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(beta3,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(beta3,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(beta3,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(beta3,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(beta3,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(beta3,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(beta3,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(beta3,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(beta3,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(beta3,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(beta3,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(beta3,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(beta3,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(beta3,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(beta3,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(beta3,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(beta3,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(beta3,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(beta3,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(beta3,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(beta3,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(beta3,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(beta3,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(beta3,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(beta3,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(beta3,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(beta3,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(beta3,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(beta3,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(beta3,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(beta3,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(beta3,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(beta3,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(beta3,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(beta3,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(beta3,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(beta3,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(beta3,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(beta3,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(beta3,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(beta3,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(beta3,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(beta3,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(beta3,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(beta3,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(beta3,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(beta3,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(beta3,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(beta3,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(beta3,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(beta3,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(beta3,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(beta3,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(beta3,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(beta3,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(beta3,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(beta3,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(beta3,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(beta3,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(beta3,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(beta3,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(beta3,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(beta3,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(beta3,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(beta3,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(beta3,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(beta3,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(beta3,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(beta3,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(beta3,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(beta3,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(beta3,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(beta3,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(beta3,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(beta3,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(beta3,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(beta3,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(beta3,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(beta3,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(beta3,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(beta3,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(beta3,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(beta3,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(beta3,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(beta3,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(beta3,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(beta3,-1,0,0) + 
      68.*GFOffset(beta3,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(beta3,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(beta3,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDubeta12 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(beta1,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(beta1,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(beta1,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta1,0,1,0) - 
      36.825792804916105238285776948*GFOffset(beta1,0,2,0) + 
      21.042577052349419249515496693*GFOffset(beta1,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(beta1,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(beta1,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(beta1,0,6,0) + 
      6.225793703001792996013745096*GFOffset(beta1,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(beta1,0,8,0) + 
      4.242018040981331373618358215*GFOffset(beta1,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(beta1,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(beta1,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(beta1,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(beta1,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(beta1,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(beta1,0,15,0) - 
      0.5*GFOffset(beta1,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(beta1,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(beta1,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(beta1,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(beta1,0,3,0) - 
      4.376597384264969120661066707*GFOffset(beta1,0,4,0) + 
      3.303076725656509756145262224*GFOffset(beta1,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(beta1,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(beta1,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(beta1,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(beta1,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(beta1,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(beta1,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(beta1,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(beta1,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(beta1,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(beta1,0,15,0),0) + IfThen(tj 
      == 2,3.4189873913829435992478256345*GFOffset(beta1,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(beta1,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(beta1,0,1,0) - 
      6.003906719792868453304381935*GFOffset(beta1,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(beta1,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(beta1,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(beta1,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(beta1,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(beta1,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(beta1,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(beta1,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(beta1,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(beta1,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(beta1,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(beta1,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(beta1,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(beta1,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(beta1,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(beta1,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(beta1,0,1,0) - 
      4.390240639181825578641202719*GFOffset(beta1,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(beta1,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(beta1,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(beta1,0,5,0) - 
      1.29435140870084806656280919*GFOffset(beta1,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(beta1,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(beta1,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(beta1,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(beta1,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(beta1,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(beta1,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(beta1,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(beta1,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(beta1,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(beta1,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(beta1,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(beta1,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(beta1,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(beta1,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(beta1,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(beta1,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(beta1,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(beta1,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(beta1,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(beta1,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(beta1,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(beta1,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(beta1,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(beta1,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(beta1,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(beta1,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(beta1,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(beta1,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(beta1,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(beta1,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(beta1,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(beta1,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(beta1,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(beta1,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(beta1,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(beta1,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(beta1,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(beta1,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(beta1,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(beta1,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(beta1,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(beta1,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(beta1,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(beta1,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(beta1,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(beta1,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(beta1,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(beta1,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(beta1,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(beta1,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(beta1,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(beta1,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(beta1,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(beta1,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(beta1,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(beta1,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(beta1,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(beta1,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(beta1,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(beta1,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(beta1,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(beta1,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(beta1,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(beta1,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(beta1,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(beta1,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(beta1,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(beta1,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(beta1,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(beta1,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(beta1,0,9,0),0) + IfThen(tj 
      == 8,0.196380615234375*GFOffset(beta1,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(beta1,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(beta1,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(beta1,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(beta1,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(beta1,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(beta1,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(beta1,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(beta1,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(beta1,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(beta1,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(beta1,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(beta1,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(beta1,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(beta1,0,7,0) - 
      0.196380615234375*GFOffset(beta1,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(beta1,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(beta1,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(beta1,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(beta1,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(beta1,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(beta1,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(beta1,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(beta1,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(beta1,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(beta1,0,1,0) - 
      2.630489733142368322167942483*GFOffset(beta1,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(beta1,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(beta1,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(beta1,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(beta1,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(beta1,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(beta1,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(beta1,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(beta1,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(beta1,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(beta1,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(beta1,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(beta1,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(beta1,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(beta1,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(beta1,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(beta1,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(beta1,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(beta1,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(beta1,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(beta1,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(beta1,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(beta1,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(beta1,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(beta1,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(beta1,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(beta1,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(beta1,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(beta1,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(beta1,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(beta1,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(beta1,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(beta1,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(beta1,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(beta1,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(beta1,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(beta1,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(beta1,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(beta1,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(beta1,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(beta1,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(beta1,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(beta1,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(beta1,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(beta1,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(beta1,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(beta1,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(beta1,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(beta1,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(beta1,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(beta1,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(beta1,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(beta1,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(beta1,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(beta1,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(beta1,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(beta1,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(beta1,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(beta1,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(beta1,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(beta1,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(beta1,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(beta1,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(beta1,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(beta1,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(beta1,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(beta1,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(beta1,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(beta1,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(beta1,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(beta1,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(beta1,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(beta1,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(beta1,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(beta1,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(beta1,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(beta1,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(beta1,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(beta1,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(beta1,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(beta1,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(beta1,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(beta1,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(beta1,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(beta1,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(beta1,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(beta1,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(beta1,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(beta1,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(beta1,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(beta1,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(beta1,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(beta1,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(beta1,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(beta1,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(beta1,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(beta1,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(beta1,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(beta1,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(beta1,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(beta1,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(beta1,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(beta1,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(beta1,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(beta1,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(beta1,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(beta1,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(beta1,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(beta1,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(beta1,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(beta1,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(beta1,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(beta1,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(beta1,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(beta1,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(beta1,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(beta1,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(beta1,0,-1,0) + 
      68.*GFOffset(beta1,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(beta1,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(beta1,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDubeta22 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(beta2,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(beta2,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(beta2,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta2,0,1,0) - 
      36.825792804916105238285776948*GFOffset(beta2,0,2,0) + 
      21.042577052349419249515496693*GFOffset(beta2,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(beta2,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(beta2,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(beta2,0,6,0) + 
      6.225793703001792996013745096*GFOffset(beta2,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(beta2,0,8,0) + 
      4.242018040981331373618358215*GFOffset(beta2,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(beta2,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(beta2,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(beta2,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(beta2,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(beta2,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(beta2,0,15,0) - 
      0.5*GFOffset(beta2,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(beta2,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(beta2,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(beta2,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(beta2,0,3,0) - 
      4.376597384264969120661066707*GFOffset(beta2,0,4,0) + 
      3.303076725656509756145262224*GFOffset(beta2,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(beta2,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(beta2,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(beta2,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(beta2,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(beta2,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(beta2,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(beta2,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(beta2,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(beta2,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(beta2,0,15,0),0) + IfThen(tj 
      == 2,3.4189873913829435992478256345*GFOffset(beta2,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(beta2,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(beta2,0,1,0) - 
      6.003906719792868453304381935*GFOffset(beta2,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(beta2,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(beta2,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(beta2,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(beta2,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(beta2,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(beta2,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(beta2,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(beta2,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(beta2,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(beta2,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(beta2,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(beta2,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(beta2,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(beta2,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(beta2,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(beta2,0,1,0) - 
      4.390240639181825578641202719*GFOffset(beta2,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(beta2,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(beta2,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(beta2,0,5,0) - 
      1.29435140870084806656280919*GFOffset(beta2,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(beta2,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(beta2,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(beta2,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(beta2,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(beta2,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(beta2,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(beta2,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(beta2,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(beta2,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(beta2,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(beta2,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(beta2,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(beta2,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(beta2,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(beta2,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(beta2,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(beta2,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(beta2,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(beta2,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(beta2,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(beta2,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(beta2,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(beta2,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(beta2,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(beta2,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(beta2,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(beta2,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(beta2,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(beta2,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(beta2,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(beta2,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(beta2,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(beta2,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(beta2,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(beta2,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(beta2,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(beta2,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(beta2,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(beta2,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(beta2,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(beta2,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(beta2,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(beta2,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(beta2,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(beta2,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(beta2,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(beta2,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(beta2,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(beta2,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(beta2,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(beta2,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(beta2,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(beta2,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(beta2,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(beta2,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(beta2,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(beta2,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(beta2,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(beta2,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(beta2,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(beta2,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(beta2,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(beta2,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(beta2,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(beta2,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(beta2,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(beta2,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(beta2,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(beta2,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(beta2,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(beta2,0,9,0),0) + IfThen(tj 
      == 8,0.196380615234375*GFOffset(beta2,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(beta2,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(beta2,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(beta2,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(beta2,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(beta2,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(beta2,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(beta2,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(beta2,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(beta2,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(beta2,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(beta2,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(beta2,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(beta2,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(beta2,0,7,0) - 
      0.196380615234375*GFOffset(beta2,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(beta2,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(beta2,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(beta2,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(beta2,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(beta2,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(beta2,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(beta2,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(beta2,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(beta2,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(beta2,0,1,0) - 
      2.630489733142368322167942483*GFOffset(beta2,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(beta2,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(beta2,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(beta2,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(beta2,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(beta2,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(beta2,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(beta2,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(beta2,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(beta2,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(beta2,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(beta2,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(beta2,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(beta2,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(beta2,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(beta2,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(beta2,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(beta2,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(beta2,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(beta2,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(beta2,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(beta2,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(beta2,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(beta2,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(beta2,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(beta2,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(beta2,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(beta2,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(beta2,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(beta2,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(beta2,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(beta2,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(beta2,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(beta2,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(beta2,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(beta2,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(beta2,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(beta2,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(beta2,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(beta2,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(beta2,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(beta2,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(beta2,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(beta2,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(beta2,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(beta2,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(beta2,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(beta2,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(beta2,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(beta2,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(beta2,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(beta2,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(beta2,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(beta2,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(beta2,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(beta2,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(beta2,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(beta2,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(beta2,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(beta2,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(beta2,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(beta2,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(beta2,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(beta2,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(beta2,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(beta2,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(beta2,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(beta2,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(beta2,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(beta2,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(beta2,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(beta2,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(beta2,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(beta2,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(beta2,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(beta2,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(beta2,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(beta2,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(beta2,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(beta2,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(beta2,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(beta2,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(beta2,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(beta2,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(beta2,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(beta2,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(beta2,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(beta2,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(beta2,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(beta2,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(beta2,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(beta2,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(beta2,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(beta2,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(beta2,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(beta2,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(beta2,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(beta2,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(beta2,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(beta2,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(beta2,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(beta2,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(beta2,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(beta2,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(beta2,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(beta2,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(beta2,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(beta2,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(beta2,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(beta2,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(beta2,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(beta2,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(beta2,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(beta2,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(beta2,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(beta2,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(beta2,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(beta2,0,-1,0) + 
      68.*GFOffset(beta2,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(beta2,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(beta2,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDubeta32 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(beta3,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(beta3,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(beta3,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta3,0,1,0) - 
      36.825792804916105238285776948*GFOffset(beta3,0,2,0) + 
      21.042577052349419249515496693*GFOffset(beta3,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(beta3,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(beta3,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(beta3,0,6,0) + 
      6.225793703001792996013745096*GFOffset(beta3,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(beta3,0,8,0) + 
      4.242018040981331373618358215*GFOffset(beta3,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(beta3,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(beta3,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(beta3,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(beta3,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(beta3,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(beta3,0,15,0) - 
      0.5*GFOffset(beta3,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(beta3,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(beta3,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(beta3,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(beta3,0,3,0) - 
      4.376597384264969120661066707*GFOffset(beta3,0,4,0) + 
      3.303076725656509756145262224*GFOffset(beta3,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(beta3,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(beta3,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(beta3,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(beta3,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(beta3,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(beta3,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(beta3,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(beta3,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(beta3,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(beta3,0,15,0),0) + IfThen(tj 
      == 2,3.4189873913829435992478256345*GFOffset(beta3,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(beta3,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(beta3,0,1,0) - 
      6.003906719792868453304381935*GFOffset(beta3,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(beta3,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(beta3,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(beta3,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(beta3,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(beta3,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(beta3,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(beta3,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(beta3,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(beta3,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(beta3,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(beta3,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(beta3,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(beta3,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(beta3,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(beta3,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(beta3,0,1,0) - 
      4.390240639181825578641202719*GFOffset(beta3,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(beta3,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(beta3,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(beta3,0,5,0) - 
      1.29435140870084806656280919*GFOffset(beta3,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(beta3,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(beta3,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(beta3,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(beta3,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(beta3,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(beta3,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(beta3,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(beta3,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(beta3,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(beta3,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(beta3,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(beta3,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(beta3,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(beta3,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(beta3,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(beta3,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(beta3,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(beta3,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(beta3,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(beta3,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(beta3,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(beta3,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(beta3,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(beta3,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(beta3,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(beta3,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(beta3,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(beta3,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(beta3,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(beta3,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(beta3,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(beta3,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(beta3,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(beta3,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(beta3,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(beta3,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(beta3,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(beta3,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(beta3,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(beta3,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(beta3,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(beta3,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(beta3,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(beta3,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(beta3,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(beta3,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(beta3,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(beta3,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(beta3,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(beta3,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(beta3,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(beta3,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(beta3,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(beta3,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(beta3,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(beta3,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(beta3,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(beta3,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(beta3,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(beta3,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(beta3,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(beta3,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(beta3,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(beta3,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(beta3,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(beta3,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(beta3,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(beta3,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(beta3,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(beta3,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(beta3,0,9,0),0) + IfThen(tj 
      == 8,0.196380615234375*GFOffset(beta3,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(beta3,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(beta3,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(beta3,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(beta3,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(beta3,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(beta3,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(beta3,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(beta3,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(beta3,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(beta3,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(beta3,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(beta3,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(beta3,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(beta3,0,7,0) - 
      0.196380615234375*GFOffset(beta3,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(beta3,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(beta3,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(beta3,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(beta3,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(beta3,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(beta3,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(beta3,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(beta3,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(beta3,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(beta3,0,1,0) - 
      2.630489733142368322167942483*GFOffset(beta3,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(beta3,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(beta3,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(beta3,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(beta3,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(beta3,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(beta3,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(beta3,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(beta3,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(beta3,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(beta3,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(beta3,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(beta3,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(beta3,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(beta3,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(beta3,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(beta3,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(beta3,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(beta3,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(beta3,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(beta3,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(beta3,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(beta3,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(beta3,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(beta3,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(beta3,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(beta3,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(beta3,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(beta3,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(beta3,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(beta3,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(beta3,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(beta3,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(beta3,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(beta3,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(beta3,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(beta3,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(beta3,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(beta3,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(beta3,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(beta3,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(beta3,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(beta3,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(beta3,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(beta3,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(beta3,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(beta3,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(beta3,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(beta3,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(beta3,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(beta3,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(beta3,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(beta3,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(beta3,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(beta3,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(beta3,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(beta3,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(beta3,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(beta3,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(beta3,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(beta3,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(beta3,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(beta3,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(beta3,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(beta3,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(beta3,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(beta3,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(beta3,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(beta3,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(beta3,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(beta3,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(beta3,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(beta3,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(beta3,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(beta3,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(beta3,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(beta3,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(beta3,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(beta3,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(beta3,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(beta3,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(beta3,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(beta3,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(beta3,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(beta3,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(beta3,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(beta3,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(beta3,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(beta3,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(beta3,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(beta3,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(beta3,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(beta3,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(beta3,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(beta3,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(beta3,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(beta3,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(beta3,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(beta3,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(beta3,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(beta3,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(beta3,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(beta3,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(beta3,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(beta3,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(beta3,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(beta3,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(beta3,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(beta3,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(beta3,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(beta3,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(beta3,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(beta3,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(beta3,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(beta3,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(beta3,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(beta3,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(beta3,0,-1,0) + 
      68.*GFOffset(beta3,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(beta3,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(beta3,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDubeta13 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(beta1,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(beta1,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(beta1,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta1,0,0,1) - 
      36.825792804916105238285776948*GFOffset(beta1,0,0,2) + 
      21.042577052349419249515496693*GFOffset(beta1,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(beta1,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(beta1,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(beta1,0,0,6) + 
      6.225793703001792996013745096*GFOffset(beta1,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(beta1,0,0,8) + 
      4.242018040981331373618358215*GFOffset(beta1,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(beta1,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(beta1,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(beta1,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(beta1,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(beta1,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(beta1,0,0,15) - 
      0.5*GFOffset(beta1,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(beta1,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(beta1,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(beta1,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(beta1,0,0,3) - 
      4.376597384264969120661066707*GFOffset(beta1,0,0,4) + 
      3.303076725656509756145262224*GFOffset(beta1,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(beta1,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(beta1,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(beta1,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(beta1,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(beta1,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(beta1,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(beta1,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(beta1,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(beta1,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(beta1,0,0,15),0) + IfThen(tk 
      == 2,3.4189873913829435992478256345*GFOffset(beta1,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(beta1,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(beta1,0,0,1) - 
      6.003906719792868453304381935*GFOffset(beta1,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(beta1,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(beta1,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(beta1,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(beta1,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(beta1,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(beta1,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(beta1,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(beta1,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(beta1,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(beta1,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(beta1,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(beta1,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(beta1,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(beta1,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(beta1,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(beta1,0,0,1) - 
      4.390240639181825578641202719*GFOffset(beta1,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(beta1,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(beta1,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(beta1,0,0,5) - 
      1.29435140870084806656280919*GFOffset(beta1,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(beta1,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(beta1,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(beta1,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(beta1,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(beta1,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(beta1,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(beta1,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(beta1,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(beta1,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(beta1,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(beta1,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(beta1,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(beta1,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(beta1,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(beta1,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(beta1,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(beta1,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(beta1,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(beta1,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(beta1,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(beta1,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(beta1,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(beta1,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(beta1,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(beta1,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(beta1,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(beta1,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(beta1,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(beta1,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(beta1,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(beta1,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(beta1,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(beta1,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(beta1,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(beta1,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(beta1,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(beta1,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(beta1,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(beta1,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(beta1,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(beta1,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(beta1,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(beta1,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(beta1,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(beta1,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(beta1,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(beta1,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(beta1,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(beta1,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(beta1,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(beta1,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(beta1,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(beta1,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(beta1,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(beta1,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(beta1,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(beta1,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(beta1,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(beta1,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(beta1,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(beta1,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(beta1,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(beta1,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(beta1,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(beta1,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(beta1,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(beta1,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(beta1,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(beta1,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(beta1,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(beta1,0,0,9),0) + IfThen(tk 
      == 8,0.196380615234375*GFOffset(beta1,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(beta1,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(beta1,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(beta1,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(beta1,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(beta1,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(beta1,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(beta1,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(beta1,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(beta1,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(beta1,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(beta1,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(beta1,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(beta1,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(beta1,0,0,7) - 
      0.196380615234375*GFOffset(beta1,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(beta1,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(beta1,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(beta1,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(beta1,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(beta1,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(beta1,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(beta1,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(beta1,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(beta1,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(beta1,0,0,1) - 
      2.630489733142368322167942483*GFOffset(beta1,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(beta1,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(beta1,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(beta1,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(beta1,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(beta1,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(beta1,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(beta1,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(beta1,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(beta1,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(beta1,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(beta1,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(beta1,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(beta1,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(beta1,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(beta1,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(beta1,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(beta1,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(beta1,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(beta1,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(beta1,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(beta1,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(beta1,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(beta1,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(beta1,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(beta1,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(beta1,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(beta1,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(beta1,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(beta1,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(beta1,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(beta1,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(beta1,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(beta1,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(beta1,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(beta1,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(beta1,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(beta1,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(beta1,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(beta1,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(beta1,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(beta1,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(beta1,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(beta1,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(beta1,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(beta1,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(beta1,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(beta1,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(beta1,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(beta1,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(beta1,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(beta1,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(beta1,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(beta1,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(beta1,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(beta1,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(beta1,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(beta1,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(beta1,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(beta1,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(beta1,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(beta1,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(beta1,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(beta1,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(beta1,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(beta1,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(beta1,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(beta1,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(beta1,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(beta1,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(beta1,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(beta1,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(beta1,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(beta1,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(beta1,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(beta1,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(beta1,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(beta1,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(beta1,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(beta1,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(beta1,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(beta1,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(beta1,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(beta1,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(beta1,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(beta1,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(beta1,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(beta1,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(beta1,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(beta1,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(beta1,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(beta1,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(beta1,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(beta1,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(beta1,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(beta1,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(beta1,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(beta1,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(beta1,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(beta1,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(beta1,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(beta1,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(beta1,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(beta1,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(beta1,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(beta1,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(beta1,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(beta1,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(beta1,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(beta1,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(beta1,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(beta1,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(beta1,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(beta1,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(beta1,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(beta1,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(beta1,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(beta1,0,0,-1) + 
      68.*GFOffset(beta1,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(beta1,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(beta1,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDubeta23 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(beta2,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(beta2,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(beta2,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta2,0,0,1) - 
      36.825792804916105238285776948*GFOffset(beta2,0,0,2) + 
      21.042577052349419249515496693*GFOffset(beta2,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(beta2,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(beta2,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(beta2,0,0,6) + 
      6.225793703001792996013745096*GFOffset(beta2,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(beta2,0,0,8) + 
      4.242018040981331373618358215*GFOffset(beta2,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(beta2,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(beta2,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(beta2,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(beta2,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(beta2,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(beta2,0,0,15) - 
      0.5*GFOffset(beta2,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(beta2,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(beta2,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(beta2,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(beta2,0,0,3) - 
      4.376597384264969120661066707*GFOffset(beta2,0,0,4) + 
      3.303076725656509756145262224*GFOffset(beta2,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(beta2,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(beta2,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(beta2,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(beta2,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(beta2,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(beta2,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(beta2,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(beta2,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(beta2,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(beta2,0,0,15),0) + IfThen(tk 
      == 2,3.4189873913829435992478256345*GFOffset(beta2,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(beta2,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(beta2,0,0,1) - 
      6.003906719792868453304381935*GFOffset(beta2,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(beta2,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(beta2,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(beta2,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(beta2,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(beta2,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(beta2,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(beta2,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(beta2,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(beta2,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(beta2,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(beta2,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(beta2,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(beta2,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(beta2,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(beta2,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(beta2,0,0,1) - 
      4.390240639181825578641202719*GFOffset(beta2,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(beta2,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(beta2,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(beta2,0,0,5) - 
      1.29435140870084806656280919*GFOffset(beta2,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(beta2,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(beta2,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(beta2,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(beta2,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(beta2,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(beta2,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(beta2,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(beta2,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(beta2,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(beta2,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(beta2,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(beta2,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(beta2,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(beta2,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(beta2,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(beta2,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(beta2,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(beta2,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(beta2,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(beta2,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(beta2,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(beta2,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(beta2,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(beta2,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(beta2,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(beta2,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(beta2,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(beta2,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(beta2,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(beta2,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(beta2,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(beta2,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(beta2,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(beta2,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(beta2,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(beta2,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(beta2,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(beta2,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(beta2,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(beta2,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(beta2,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(beta2,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(beta2,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(beta2,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(beta2,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(beta2,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(beta2,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(beta2,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(beta2,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(beta2,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(beta2,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(beta2,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(beta2,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(beta2,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(beta2,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(beta2,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(beta2,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(beta2,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(beta2,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(beta2,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(beta2,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(beta2,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(beta2,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(beta2,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(beta2,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(beta2,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(beta2,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(beta2,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(beta2,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(beta2,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(beta2,0,0,9),0) + IfThen(tk 
      == 8,0.196380615234375*GFOffset(beta2,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(beta2,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(beta2,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(beta2,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(beta2,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(beta2,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(beta2,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(beta2,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(beta2,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(beta2,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(beta2,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(beta2,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(beta2,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(beta2,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(beta2,0,0,7) - 
      0.196380615234375*GFOffset(beta2,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(beta2,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(beta2,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(beta2,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(beta2,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(beta2,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(beta2,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(beta2,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(beta2,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(beta2,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(beta2,0,0,1) - 
      2.630489733142368322167942483*GFOffset(beta2,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(beta2,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(beta2,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(beta2,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(beta2,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(beta2,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(beta2,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(beta2,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(beta2,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(beta2,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(beta2,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(beta2,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(beta2,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(beta2,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(beta2,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(beta2,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(beta2,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(beta2,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(beta2,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(beta2,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(beta2,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(beta2,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(beta2,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(beta2,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(beta2,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(beta2,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(beta2,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(beta2,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(beta2,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(beta2,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(beta2,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(beta2,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(beta2,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(beta2,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(beta2,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(beta2,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(beta2,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(beta2,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(beta2,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(beta2,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(beta2,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(beta2,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(beta2,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(beta2,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(beta2,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(beta2,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(beta2,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(beta2,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(beta2,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(beta2,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(beta2,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(beta2,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(beta2,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(beta2,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(beta2,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(beta2,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(beta2,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(beta2,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(beta2,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(beta2,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(beta2,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(beta2,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(beta2,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(beta2,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(beta2,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(beta2,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(beta2,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(beta2,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(beta2,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(beta2,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(beta2,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(beta2,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(beta2,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(beta2,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(beta2,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(beta2,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(beta2,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(beta2,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(beta2,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(beta2,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(beta2,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(beta2,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(beta2,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(beta2,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(beta2,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(beta2,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(beta2,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(beta2,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(beta2,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(beta2,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(beta2,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(beta2,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(beta2,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(beta2,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(beta2,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(beta2,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(beta2,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(beta2,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(beta2,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(beta2,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(beta2,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(beta2,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(beta2,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(beta2,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(beta2,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(beta2,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(beta2,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(beta2,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(beta2,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(beta2,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(beta2,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(beta2,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(beta2,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(beta2,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(beta2,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(beta2,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(beta2,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(beta2,0,0,-1) + 
      68.*GFOffset(beta2,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(beta2,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(beta2,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDubeta33 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(beta3,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(beta3,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(beta3,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta3,0,0,1) - 
      36.825792804916105238285776948*GFOffset(beta3,0,0,2) + 
      21.042577052349419249515496693*GFOffset(beta3,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(beta3,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(beta3,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(beta3,0,0,6) + 
      6.225793703001792996013745096*GFOffset(beta3,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(beta3,0,0,8) + 
      4.242018040981331373618358215*GFOffset(beta3,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(beta3,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(beta3,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(beta3,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(beta3,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(beta3,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(beta3,0,0,15) - 
      0.5*GFOffset(beta3,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(beta3,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(beta3,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(beta3,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(beta3,0,0,3) - 
      4.376597384264969120661066707*GFOffset(beta3,0,0,4) + 
      3.303076725656509756145262224*GFOffset(beta3,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(beta3,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(beta3,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(beta3,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(beta3,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(beta3,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(beta3,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(beta3,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(beta3,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(beta3,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(beta3,0,0,15),0) + IfThen(tk 
      == 2,3.4189873913829435992478256345*GFOffset(beta3,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(beta3,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(beta3,0,0,1) - 
      6.003906719792868453304381935*GFOffset(beta3,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(beta3,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(beta3,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(beta3,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(beta3,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(beta3,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(beta3,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(beta3,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(beta3,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(beta3,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(beta3,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(beta3,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(beta3,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(beta3,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(beta3,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(beta3,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(beta3,0,0,1) - 
      4.390240639181825578641202719*GFOffset(beta3,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(beta3,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(beta3,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(beta3,0,0,5) - 
      1.29435140870084806656280919*GFOffset(beta3,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(beta3,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(beta3,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(beta3,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(beta3,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(beta3,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(beta3,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(beta3,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(beta3,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(beta3,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(beta3,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(beta3,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(beta3,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(beta3,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(beta3,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(beta3,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(beta3,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(beta3,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(beta3,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(beta3,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(beta3,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(beta3,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(beta3,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(beta3,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(beta3,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(beta3,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(beta3,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(beta3,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(beta3,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(beta3,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(beta3,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(beta3,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(beta3,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(beta3,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(beta3,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(beta3,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(beta3,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(beta3,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(beta3,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(beta3,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(beta3,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(beta3,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(beta3,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(beta3,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(beta3,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(beta3,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(beta3,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(beta3,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(beta3,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(beta3,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(beta3,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(beta3,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(beta3,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(beta3,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(beta3,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(beta3,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(beta3,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(beta3,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(beta3,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(beta3,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(beta3,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(beta3,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(beta3,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(beta3,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(beta3,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(beta3,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(beta3,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(beta3,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(beta3,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(beta3,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(beta3,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(beta3,0,0,9),0) + IfThen(tk 
      == 8,0.196380615234375*GFOffset(beta3,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(beta3,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(beta3,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(beta3,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(beta3,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(beta3,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(beta3,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(beta3,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(beta3,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(beta3,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(beta3,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(beta3,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(beta3,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(beta3,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(beta3,0,0,7) - 
      0.196380615234375*GFOffset(beta3,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(beta3,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(beta3,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(beta3,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(beta3,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(beta3,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(beta3,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(beta3,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(beta3,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(beta3,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(beta3,0,0,1) - 
      2.630489733142368322167942483*GFOffset(beta3,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(beta3,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(beta3,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(beta3,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(beta3,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(beta3,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(beta3,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(beta3,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(beta3,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(beta3,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(beta3,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(beta3,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(beta3,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(beta3,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(beta3,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(beta3,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(beta3,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(beta3,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(beta3,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(beta3,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(beta3,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(beta3,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(beta3,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(beta3,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(beta3,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(beta3,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(beta3,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(beta3,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(beta3,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(beta3,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(beta3,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(beta3,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(beta3,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(beta3,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(beta3,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(beta3,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(beta3,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(beta3,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(beta3,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(beta3,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(beta3,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(beta3,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(beta3,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(beta3,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(beta3,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(beta3,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(beta3,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(beta3,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(beta3,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(beta3,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(beta3,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(beta3,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(beta3,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(beta3,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(beta3,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(beta3,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(beta3,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(beta3,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(beta3,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(beta3,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(beta3,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(beta3,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(beta3,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(beta3,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(beta3,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(beta3,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(beta3,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(beta3,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(beta3,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(beta3,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(beta3,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(beta3,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(beta3,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(beta3,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(beta3,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(beta3,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(beta3,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(beta3,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(beta3,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(beta3,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(beta3,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(beta3,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(beta3,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(beta3,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(beta3,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(beta3,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(beta3,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(beta3,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(beta3,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(beta3,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(beta3,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(beta3,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(beta3,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(beta3,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(beta3,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(beta3,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(beta3,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(beta3,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(beta3,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(beta3,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(beta3,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(beta3,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(beta3,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(beta3,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(beta3,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(beta3,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(beta3,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(beta3,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(beta3,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(beta3,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(beta3,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(beta3,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(beta3,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(beta3,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(beta3,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(beta3,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(beta3,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(beta3,0,0,-1) + 
      68.*GFOffset(beta3,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(beta3,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(beta3,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDualpha1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualpha2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualpha3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDubeta33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDualpha1 = J11L*LDualpha1 + J21L*LDualpha2 + J31L*LDualpha3;
      
      PDualpha2 = J12L*LDualpha1 + J22L*LDualpha2 + J32L*LDualpha3;
      
      PDualpha3 = J13L*LDualpha1 + J23L*LDualpha2 + J33L*LDualpha3;
      
      PDubeta11 = J11L*LDubeta11 + J21L*LDubeta12 + J31L*LDubeta13;
      
      PDubeta21 = J11L*LDubeta21 + J21L*LDubeta22 + J31L*LDubeta23;
      
      PDubeta31 = J11L*LDubeta31 + J21L*LDubeta32 + J31L*LDubeta33;
      
      PDubeta12 = J12L*LDubeta11 + J22L*LDubeta12 + J32L*LDubeta13;
      
      PDubeta22 = J12L*LDubeta21 + J22L*LDubeta22 + J32L*LDubeta23;
      
      PDubeta32 = J12L*LDubeta31 + J22L*LDubeta32 + J32L*LDubeta33;
      
      PDubeta13 = J13L*LDubeta11 + J23L*LDubeta12 + J33L*LDubeta13;
      
      PDubeta23 = J13L*LDubeta21 + J23L*LDubeta22 + J33L*LDubeta23;
      
      PDubeta33 = J13L*LDubeta31 + J23L*LDubeta32 + J33L*LDubeta33;
    }
    else
    {
      PDualpha1 = LDualpha1;
      
      PDualpha2 = LDualpha2;
      
      PDualpha3 = LDualpha3;
      
      PDubeta11 = LDubeta11;
      
      PDubeta21 = LDubeta21;
      
      PDubeta31 = LDubeta31;
      
      PDubeta12 = LDubeta12;
      
      PDubeta22 = LDubeta22;
      
      PDubeta32 = LDubeta32;
      
      PDubeta13 = LDubeta13;
      
      PDubeta23 = LDubeta23;
      
      PDubeta33 = LDubeta33;
    }
    
    CCTK_REAL detgt CCTK_ATTRIBUTE_UNUSED = 1;
    
    CCTK_REAL gtu11 CCTK_ATTRIBUTE_UNUSED = (gt22L*gt33L - 
      pow(gt23L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu12 CCTK_ATTRIBUTE_UNUSED = (gt13L*gt23L - 
      gt12L*gt33L)*pow(detgt,-1);
    
    CCTK_REAL gtu13 CCTK_ATTRIBUTE_UNUSED = (-(gt13L*gt22L) + 
      gt12L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu22 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt33L - 
      pow(gt13L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu23 CCTK_ATTRIBUTE_UNUSED = (gt12L*gt13L - 
      gt11L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu33 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt22L - 
      pow(gt12L,2))*pow(detgt,-1);
    
    CCTK_REAL em4phi CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,pow(phiWL,2),exp(-4*phiWL));
    
    CCTK_REAL gu11 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu11;
    
    CCTK_REAL gu12 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu12;
    
    CCTK_REAL gu13 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu13;
    
    CCTK_REAL gu22 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu22;
    
    CCTK_REAL gu23 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu23;
    
    CCTK_REAL gu33 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu33;
    
    CCTK_REAL fac1 CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,-0.5*pow(phiWL,-1),1);
    
    CCTK_REAL cdphi1 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*fac1;
    
    CCTK_REAL cdphi2 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*fac1;
    
    CCTK_REAL cdphi3 CCTK_ATTRIBUTE_UNUSED = PDphiW3L*fac1;
    
    CCTK_REAL dotalpha CCTK_ATTRIBUTE_UNUSED = -(harmonicF*IfThen(evolveA 
      != 0,AL,trKL + (-1 + alphaL)*alphaDriver)*pow(alphaL,harmonicN));
    
    CCTK_REAL betaDriverValue CCTK_ATTRIBUTE_UNUSED = 
      IfThen(useSpatialBetaDriver != 
      0,betaDriver*spatialBetaDriverRadius*pow(fmax(rL,spatialBetaDriverRadius),-1),betaDriver);
    
    CCTK_REAL shiftGammaCoeffValue CCTK_ATTRIBUTE_UNUSED = 
      IfThen(useSpatialShiftGammaCoeff != 0,shiftGammaCoeff*fmin(1,exp(1 - 
      rL*pow(spatialShiftGammaCoeffRadius,-1))),shiftGammaCoeff);
    
    CCTK_REAL ddetgt1 CCTK_ATTRIBUTE_UNUSED = PDgt111L*gtu11 + 
      2*PDgt121L*gtu12 + 2*PDgt131L*gtu13 + PDgt221L*gtu22 + 2*PDgt231L*gtu23 
      + PDgt331L*gtu33;
    
    CCTK_REAL ddetgt2 CCTK_ATTRIBUTE_UNUSED = PDgt112L*gtu11 + 
      2*PDgt122L*gtu12 + 2*PDgt132L*gtu13 + PDgt222L*gtu22 + 2*PDgt232L*gtu23 
      + PDgt332L*gtu33;
    
    CCTK_REAL ddetgt3 CCTK_ATTRIBUTE_UNUSED = PDgt113L*gtu11 + 
      2*PDgt123L*gtu12 + 2*PDgt133L*gtu13 + PDgt223L*gtu22 + 2*PDgt233L*gtu23 
      + PDgt333L*gtu33;
    
    CCTK_REAL dotbeta1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dotbeta2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL dotbeta3 CCTK_ATTRIBUTE_UNUSED;
    
    if (shiftFormulation == 0)
    {
      dotbeta1 = shiftGammaCoeffValue*IfThen(evolveB != 0,B1L,Xt1L - 
        beta1L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
      
      dotbeta2 = shiftGammaCoeffValue*IfThen(evolveB != 0,B2L,Xt2L - 
        beta2L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
      
      dotbeta3 = shiftGammaCoeffValue*IfThen(evolveB != 0,B3L,Xt3L - 
        beta3L*betaDriverValue)*pow(alphaL,shiftAlphaPower);
    }
    else
    {
      dotbeta1 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu11 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu12 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu13));
      
      dotbeta2 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu12 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu22 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu23));
      
      dotbeta3 = -(alphaL*((PDalpha1L + 2*alphaL*cdphi1 + 0.5*alphaL*ddetgt1 
        - alphaL*(PDgt111L*gtu11 + PDgt112L*gtu12 + PDgt121L*gtu12 + 
        PDgt113L*gtu13 + PDgt131L*gtu13 + PDgt122L*gtu22 + PDgt123L*gtu23 + 
        PDgt132L*gtu23 + PDgt133L*gtu33))*gu13 + (PDalpha2L + 2*alphaL*cdphi2 + 
        0.5*alphaL*ddetgt2 - alphaL*(PDgt121L*gtu11 + PDgt122L*gtu12 + 
        PDgt221L*gtu12 + PDgt123L*gtu13 + PDgt231L*gtu13 + PDgt222L*gtu22 + 
        PDgt223L*gtu23 + PDgt232L*gtu23 + PDgt233L*gtu33))*gu23 + (PDalpha3L + 
        2*alphaL*cdphi3 + 0.5*alphaL*ddetgt3 - alphaL*(PDgt131L*gtu11 + 
        PDgt132L*gtu12 + PDgt231L*gtu12 + PDgt133L*gtu13 + PDgt331L*gtu13 + 
        PDgt232L*gtu22 + PDgt233L*gtu23 + PDgt332L*gtu23 + 
        PDgt333L*gtu33))*gu33));
    }
    
    CCTK_REAL dtalpL CCTK_ATTRIBUTE_UNUSED = dotalpha + IfThen(advectLapse 
      != 0,beta1L*PDualpha1 + beta2L*PDualpha2 + beta3L*PDualpha3,0);
    
    CCTK_REAL dtbetaxL CCTK_ATTRIBUTE_UNUSED = dotbeta1 + 
      IfThen(advectShift != 0,beta1L*PDubeta11 + beta2L*PDubeta12 + 
      beta3L*PDubeta13,0);
    
    CCTK_REAL dtbetayL CCTK_ATTRIBUTE_UNUSED = dotbeta2 + 
      IfThen(advectShift != 0,beta1L*PDubeta21 + beta2L*PDubeta22 + 
      beta3L*PDubeta23,0);
    
    CCTK_REAL dtbetazL CCTK_ATTRIBUTE_UNUSED = dotbeta3 + 
      IfThen(advectShift != 0,beta1L*PDubeta31 + beta2L*PDubeta32 + 
      beta3L*PDubeta33,0);
    /* Copy local copies back to grid functions */
    dtalp[index] = dtalpL;
    dtbetax[index] = dtbetaxL;
    dtbetay[index] = dtbetayL;
    dtbetaz[index] = dtbetazL;
  }
  CCTK_ENDLOOP3(ML_BSSN_DG16_ADMBaseInterior);
}
extern "C" void ML_BSSN_DG16_ADMBaseInterior(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_DG16_ADMBaseInterior_Body");
  }
  if (cctk_iteration % ML_BSSN_DG16_ADMBaseInterior_calc_every != ML_BSSN_DG16_ADMBaseInterior_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ADMBase::dtlapse",
    "ADMBase::dtshift",
    "grid::coordinates",
    "ML_BSSN_DG16::ML_confac",
    "ML_BSSN_DG16::ML_dconfac",
    "ML_BSSN_DG16::ML_dlapse",
    "ML_BSSN_DG16::ML_dmetric",
    "ML_BSSN_DG16::ML_dtlapse",
    "ML_BSSN_DG16::ML_dtshift",
    "ML_BSSN_DG16::ML_Gamma",
    "ML_BSSN_DG16::ML_lapse",
    "ML_BSSN_DG16::ML_metric",
    "ML_BSSN_DG16::ML_shift",
    "ML_BSSN_DG16::ML_trace_curv"};
  AssertGroupStorage(cctkGH, "ML_BSSN_DG16_ADMBaseInterior", 14, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_BSSN_DG16_ADMBaseInterior_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_DG16_ADMBaseInterior_Body");
  }
}

} // namespace ML_BSSN_DG16
