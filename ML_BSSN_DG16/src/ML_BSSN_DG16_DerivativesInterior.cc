/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_BSSN_DG16 {

extern "C" void ML_BSSN_DG16_DerivativesInterior_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_DG16_DerivativesInterior_calc_every != ML_BSSN_DG16_DerivativesInterior_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG16::ML_dconfac","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG16::ML_dconfac.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG16::ML_dlapse","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG16::ML_dlapse.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG16::ML_dmetric","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG16::ML_dmetric.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG16::ML_dshift","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG16::ML_dshift.");
  return;
}

static void ML_BSSN_DG16_DerivativesInterior_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_BSSN_DG16_DerivativesInterior,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL alphaL CCTK_ATTRIBUTE_UNUSED = alpha[index];
    CCTK_REAL beta1L CCTK_ATTRIBUTE_UNUSED = beta1[index];
    CCTK_REAL beta2L CCTK_ATTRIBUTE_UNUSED = beta2[index];
    CCTK_REAL beta3L CCTK_ATTRIBUTE_UNUSED = beta3[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL phiWL CCTK_ATTRIBUTE_UNUSED = phiW[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDphiW1 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(phiW,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(phiW,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(phiW,0,0,0) + 
      91.995423705517011185543249491*GFOffset(phiW,1,0,0) - 
      36.825792804916105238285776948*GFOffset(phiW,2,0,0) + 
      21.042577052349419249515496693*GFOffset(phiW,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(phiW,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(phiW,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(phiW,6,0,0) + 
      6.225793703001792996013745096*GFOffset(phiW,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(phiW,8,0,0) + 
      4.242018040981331373618358215*GFOffset(phiW,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(phiW,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(phiW,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(phiW,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(phiW,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(phiW,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(phiW,15,0,0) - 
      0.5*GFOffset(phiW,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(phiW,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(phiW,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(phiW,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(phiW,3,0,0) - 
      4.376597384264969120661066707*GFOffset(phiW,4,0,0) + 
      3.303076725656509756145262224*GFOffset(phiW,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(phiW,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(phiW,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(phiW,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(phiW,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(phiW,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(phiW,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(phiW,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(phiW,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(phiW,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(phiW,15,0,0),0) + IfThen(ti == 
      2,3.4189873913829435992478256345*GFOffset(phiW,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(phiW,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(phiW,1,0,0) - 
      6.003906719792868453304381935*GFOffset(phiW,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(phiW,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(phiW,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(phiW,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(phiW,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(phiW,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(phiW,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(phiW,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(phiW,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(phiW,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(phiW,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(phiW,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(phiW,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(phiW,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(phiW,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(phiW,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(phiW,1,0,0) - 
      4.390240639181825578641202719*GFOffset(phiW,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(phiW,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(phiW,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(phiW,5,0,0) - 
      1.29435140870084806656280919*GFOffset(phiW,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(phiW,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(phiW,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(phiW,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(phiW,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(phiW,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(phiW,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(phiW,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(phiW,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(phiW,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(phiW,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(phiW,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(phiW,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(phiW,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(phiW,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(phiW,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(phiW,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(phiW,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(phiW,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(phiW,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(phiW,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(phiW,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(phiW,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(phiW,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(phiW,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(phiW,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(phiW,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(phiW,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(phiW,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(phiW,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(phiW,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(phiW,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(phiW,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(phiW,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(phiW,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(phiW,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(phiW,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(phiW,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(phiW,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(phiW,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(phiW,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(phiW,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(phiW,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(phiW,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(phiW,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(phiW,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(phiW,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(phiW,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(phiW,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(phiW,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(phiW,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(phiW,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(phiW,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(phiW,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(phiW,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(phiW,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(phiW,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(phiW,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(phiW,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(phiW,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(phiW,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(phiW,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(phiW,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(phiW,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(phiW,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(phiW,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(phiW,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(phiW,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(phiW,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(phiW,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(phiW,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(phiW,9,0,0),0) + IfThen(ti == 
      8,0.196380615234375*GFOffset(phiW,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(phiW,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(phiW,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(phiW,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(phiW,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(phiW,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(phiW,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(phiW,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(phiW,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(phiW,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(phiW,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(phiW,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(phiW,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(phiW,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(phiW,7,0,0) - 
      0.196380615234375*GFOffset(phiW,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(phiW,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(phiW,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(phiW,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(phiW,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(phiW,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(phiW,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(phiW,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(phiW,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(phiW,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(phiW,1,0,0) - 
      2.630489733142368322167942483*GFOffset(phiW,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(phiW,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(phiW,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(phiW,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(phiW,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(phiW,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(phiW,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(phiW,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(phiW,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(phiW,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(phiW,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(phiW,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(phiW,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(phiW,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(phiW,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(phiW,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(phiW,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(phiW,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(phiW,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(phiW,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(phiW,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(phiW,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(phiW,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(phiW,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(phiW,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(phiW,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(phiW,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(phiW,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(phiW,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(phiW,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(phiW,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(phiW,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(phiW,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(phiW,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(phiW,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(phiW,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(phiW,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(phiW,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(phiW,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(phiW,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(phiW,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(phiW,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(phiW,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(phiW,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(phiW,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(phiW,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(phiW,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(phiW,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(phiW,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(phiW,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(phiW,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(phiW,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(phiW,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(phiW,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(phiW,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(phiW,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(phiW,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(phiW,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(phiW,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(phiW,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(phiW,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(phiW,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(phiW,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(phiW,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(phiW,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(phiW,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(phiW,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(phiW,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(phiW,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(phiW,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(phiW,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(phiW,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(phiW,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(phiW,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(phiW,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(phiW,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(phiW,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(phiW,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(phiW,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(phiW,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(phiW,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(phiW,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(phiW,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(phiW,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(phiW,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(phiW,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(phiW,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(phiW,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(phiW,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(phiW,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(phiW,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(phiW,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(phiW,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(phiW,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(phiW,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(phiW,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(phiW,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(phiW,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(phiW,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(phiW,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(phiW,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(phiW,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(phiW,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(phiW,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(phiW,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(phiW,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(phiW,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(phiW,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(phiW,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(phiW,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(phiW,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(phiW,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(phiW,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(phiW,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(phiW,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(phiW,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(phiW,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(phiW,-1,0,0) + 
      68.*GFOffset(phiW,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(phiW,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(phiW,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDphiW2 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(phiW,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(phiW,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(phiW,0,0,0) + 
      91.995423705517011185543249491*GFOffset(phiW,0,1,0) - 
      36.825792804916105238285776948*GFOffset(phiW,0,2,0) + 
      21.042577052349419249515496693*GFOffset(phiW,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(phiW,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(phiW,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(phiW,0,6,0) + 
      6.225793703001792996013745096*GFOffset(phiW,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(phiW,0,8,0) + 
      4.242018040981331373618358215*GFOffset(phiW,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(phiW,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(phiW,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(phiW,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(phiW,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(phiW,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(phiW,0,15,0) - 
      0.5*GFOffset(phiW,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(phiW,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(phiW,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(phiW,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(phiW,0,3,0) - 
      4.376597384264969120661066707*GFOffset(phiW,0,4,0) + 
      3.303076725656509756145262224*GFOffset(phiW,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(phiW,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(phiW,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(phiW,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(phiW,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(phiW,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(phiW,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(phiW,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(phiW,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(phiW,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(phiW,0,15,0),0) + IfThen(tj == 
      2,3.4189873913829435992478256345*GFOffset(phiW,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(phiW,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(phiW,0,1,0) - 
      6.003906719792868453304381935*GFOffset(phiW,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(phiW,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(phiW,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(phiW,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(phiW,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(phiW,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(phiW,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(phiW,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(phiW,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(phiW,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(phiW,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(phiW,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(phiW,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(phiW,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(phiW,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(phiW,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(phiW,0,1,0) - 
      4.390240639181825578641202719*GFOffset(phiW,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(phiW,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(phiW,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(phiW,0,5,0) - 
      1.29435140870084806656280919*GFOffset(phiW,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(phiW,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(phiW,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(phiW,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(phiW,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(phiW,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(phiW,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(phiW,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(phiW,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(phiW,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(phiW,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(phiW,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(phiW,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(phiW,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(phiW,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(phiW,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(phiW,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(phiW,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(phiW,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(phiW,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(phiW,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(phiW,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(phiW,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(phiW,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(phiW,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(phiW,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(phiW,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(phiW,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(phiW,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(phiW,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(phiW,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(phiW,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(phiW,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(phiW,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(phiW,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(phiW,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(phiW,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(phiW,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(phiW,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(phiW,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(phiW,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(phiW,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(phiW,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(phiW,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(phiW,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(phiW,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(phiW,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(phiW,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(phiW,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(phiW,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(phiW,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(phiW,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(phiW,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(phiW,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(phiW,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(phiW,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(phiW,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(phiW,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(phiW,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(phiW,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(phiW,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(phiW,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(phiW,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(phiW,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(phiW,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(phiW,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(phiW,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(phiW,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(phiW,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(phiW,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(phiW,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(phiW,0,9,0),0) + IfThen(tj == 
      8,0.196380615234375*GFOffset(phiW,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(phiW,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(phiW,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(phiW,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(phiW,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(phiW,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(phiW,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(phiW,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(phiW,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(phiW,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(phiW,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(phiW,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(phiW,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(phiW,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(phiW,0,7,0) - 
      0.196380615234375*GFOffset(phiW,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(phiW,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(phiW,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(phiW,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(phiW,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(phiW,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(phiW,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(phiW,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(phiW,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(phiW,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(phiW,0,1,0) - 
      2.630489733142368322167942483*GFOffset(phiW,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(phiW,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(phiW,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(phiW,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(phiW,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(phiW,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(phiW,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(phiW,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(phiW,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(phiW,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(phiW,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(phiW,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(phiW,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(phiW,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(phiW,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(phiW,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(phiW,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(phiW,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(phiW,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(phiW,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(phiW,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(phiW,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(phiW,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(phiW,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(phiW,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(phiW,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(phiW,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(phiW,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(phiW,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(phiW,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(phiW,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(phiW,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(phiW,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(phiW,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(phiW,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(phiW,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(phiW,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(phiW,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(phiW,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(phiW,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(phiW,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(phiW,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(phiW,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(phiW,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(phiW,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(phiW,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(phiW,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(phiW,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(phiW,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(phiW,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(phiW,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(phiW,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(phiW,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(phiW,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(phiW,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(phiW,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(phiW,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(phiW,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(phiW,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(phiW,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(phiW,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(phiW,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(phiW,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(phiW,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(phiW,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(phiW,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(phiW,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(phiW,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(phiW,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(phiW,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(phiW,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(phiW,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(phiW,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(phiW,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(phiW,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(phiW,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(phiW,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(phiW,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(phiW,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(phiW,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(phiW,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(phiW,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(phiW,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(phiW,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(phiW,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(phiW,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(phiW,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(phiW,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(phiW,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(phiW,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(phiW,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(phiW,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(phiW,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(phiW,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(phiW,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(phiW,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(phiW,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(phiW,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(phiW,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(phiW,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(phiW,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(phiW,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(phiW,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(phiW,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(phiW,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(phiW,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(phiW,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(phiW,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(phiW,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(phiW,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(phiW,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(phiW,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(phiW,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(phiW,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(phiW,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(phiW,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(phiW,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(phiW,0,-1,0) + 
      68.*GFOffset(phiW,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(phiW,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(phiW,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDphiW3 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(phiW,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(phiW,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(phiW,0,0,0) + 
      91.995423705517011185543249491*GFOffset(phiW,0,0,1) - 
      36.825792804916105238285776948*GFOffset(phiW,0,0,2) + 
      21.042577052349419249515496693*GFOffset(phiW,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(phiW,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(phiW,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(phiW,0,0,6) + 
      6.225793703001792996013745096*GFOffset(phiW,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(phiW,0,0,8) + 
      4.242018040981331373618358215*GFOffset(phiW,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(phiW,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(phiW,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(phiW,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(phiW,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(phiW,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(phiW,0,0,15) - 
      0.5*GFOffset(phiW,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(phiW,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(phiW,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(phiW,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(phiW,0,0,3) - 
      4.376597384264969120661066707*GFOffset(phiW,0,0,4) + 
      3.303076725656509756145262224*GFOffset(phiW,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(phiW,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(phiW,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(phiW,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(phiW,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(phiW,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(phiW,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(phiW,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(phiW,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(phiW,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(phiW,0,0,15),0) + IfThen(tk == 
      2,3.4189873913829435992478256345*GFOffset(phiW,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(phiW,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(phiW,0,0,1) - 
      6.003906719792868453304381935*GFOffset(phiW,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(phiW,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(phiW,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(phiW,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(phiW,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(phiW,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(phiW,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(phiW,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(phiW,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(phiW,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(phiW,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(phiW,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(phiW,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(phiW,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(phiW,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(phiW,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(phiW,0,0,1) - 
      4.390240639181825578641202719*GFOffset(phiW,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(phiW,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(phiW,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(phiW,0,0,5) - 
      1.29435140870084806656280919*GFOffset(phiW,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(phiW,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(phiW,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(phiW,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(phiW,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(phiW,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(phiW,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(phiW,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(phiW,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(phiW,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(phiW,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(phiW,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(phiW,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(phiW,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(phiW,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(phiW,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(phiW,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(phiW,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(phiW,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(phiW,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(phiW,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(phiW,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(phiW,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(phiW,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(phiW,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(phiW,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(phiW,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(phiW,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(phiW,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(phiW,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(phiW,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(phiW,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(phiW,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(phiW,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(phiW,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(phiW,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(phiW,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(phiW,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(phiW,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(phiW,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(phiW,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(phiW,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(phiW,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(phiW,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(phiW,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(phiW,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(phiW,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(phiW,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(phiW,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(phiW,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(phiW,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(phiW,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(phiW,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(phiW,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(phiW,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(phiW,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(phiW,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(phiW,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(phiW,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(phiW,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(phiW,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(phiW,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(phiW,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(phiW,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(phiW,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(phiW,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(phiW,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(phiW,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(phiW,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(phiW,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(phiW,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(phiW,0,0,9),0) + IfThen(tk == 
      8,0.196380615234375*GFOffset(phiW,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(phiW,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(phiW,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(phiW,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(phiW,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(phiW,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(phiW,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(phiW,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(phiW,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(phiW,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(phiW,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(phiW,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(phiW,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(phiW,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(phiW,0,0,7) - 
      0.196380615234375*GFOffset(phiW,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(phiW,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(phiW,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(phiW,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(phiW,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(phiW,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(phiW,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(phiW,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(phiW,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(phiW,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(phiW,0,0,1) - 
      2.630489733142368322167942483*GFOffset(phiW,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(phiW,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(phiW,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(phiW,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(phiW,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(phiW,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(phiW,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(phiW,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(phiW,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(phiW,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(phiW,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(phiW,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(phiW,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(phiW,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(phiW,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(phiW,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(phiW,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(phiW,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(phiW,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(phiW,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(phiW,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(phiW,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(phiW,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(phiW,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(phiW,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(phiW,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(phiW,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(phiW,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(phiW,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(phiW,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(phiW,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(phiW,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(phiW,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(phiW,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(phiW,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(phiW,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(phiW,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(phiW,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(phiW,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(phiW,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(phiW,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(phiW,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(phiW,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(phiW,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(phiW,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(phiW,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(phiW,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(phiW,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(phiW,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(phiW,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(phiW,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(phiW,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(phiW,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(phiW,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(phiW,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(phiW,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(phiW,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(phiW,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(phiW,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(phiW,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(phiW,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(phiW,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(phiW,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(phiW,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(phiW,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(phiW,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(phiW,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(phiW,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(phiW,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(phiW,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(phiW,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(phiW,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(phiW,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(phiW,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(phiW,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(phiW,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(phiW,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(phiW,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(phiW,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(phiW,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(phiW,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(phiW,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(phiW,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(phiW,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(phiW,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(phiW,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(phiW,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(phiW,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(phiW,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(phiW,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(phiW,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(phiW,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(phiW,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(phiW,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(phiW,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(phiW,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(phiW,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(phiW,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(phiW,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(phiW,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(phiW,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(phiW,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(phiW,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(phiW,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(phiW,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(phiW,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(phiW,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(phiW,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(phiW,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(phiW,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(phiW,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(phiW,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(phiW,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(phiW,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(phiW,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(phiW,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(phiW,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(phiW,0,0,-1) + 
      68.*GFOffset(phiW,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(phiW,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(phiW,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt111 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(gt11,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(gt11,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(gt11,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt11,1,0,0) - 
      36.825792804916105238285776948*GFOffset(gt11,2,0,0) + 
      21.042577052349419249515496693*GFOffset(gt11,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(gt11,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(gt11,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(gt11,6,0,0) + 
      6.225793703001792996013745096*GFOffset(gt11,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(gt11,8,0,0) + 
      4.242018040981331373618358215*GFOffset(gt11,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(gt11,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(gt11,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(gt11,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(gt11,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(gt11,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(gt11,15,0,0) - 
      0.5*GFOffset(gt11,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(gt11,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(gt11,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(gt11,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(gt11,3,0,0) - 
      4.376597384264969120661066707*GFOffset(gt11,4,0,0) + 
      3.303076725656509756145262224*GFOffset(gt11,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(gt11,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(gt11,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(gt11,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(gt11,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(gt11,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(gt11,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(gt11,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(gt11,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(gt11,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(gt11,15,0,0),0) + IfThen(ti == 
      2,3.4189873913829435992478256345*GFOffset(gt11,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(gt11,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(gt11,1,0,0) - 
      6.003906719792868453304381935*GFOffset(gt11,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(gt11,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(gt11,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(gt11,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(gt11,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(gt11,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(gt11,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(gt11,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(gt11,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(gt11,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(gt11,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(gt11,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(gt11,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(gt11,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(gt11,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(gt11,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(gt11,1,0,0) - 
      4.390240639181825578641202719*GFOffset(gt11,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(gt11,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(gt11,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(gt11,5,0,0) - 
      1.29435140870084806656280919*GFOffset(gt11,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(gt11,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(gt11,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(gt11,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(gt11,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(gt11,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(gt11,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(gt11,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(gt11,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(gt11,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(gt11,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(gt11,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(gt11,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(gt11,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(gt11,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(gt11,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(gt11,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(gt11,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(gt11,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(gt11,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(gt11,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(gt11,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(gt11,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(gt11,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(gt11,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(gt11,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(gt11,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(gt11,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(gt11,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(gt11,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(gt11,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(gt11,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(gt11,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(gt11,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(gt11,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(gt11,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(gt11,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(gt11,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(gt11,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(gt11,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(gt11,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(gt11,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(gt11,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(gt11,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(gt11,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(gt11,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(gt11,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(gt11,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(gt11,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(gt11,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(gt11,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(gt11,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(gt11,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(gt11,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(gt11,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(gt11,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(gt11,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(gt11,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(gt11,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(gt11,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(gt11,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(gt11,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(gt11,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(gt11,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(gt11,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(gt11,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(gt11,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(gt11,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(gt11,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(gt11,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(gt11,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(gt11,9,0,0),0) + IfThen(ti == 
      8,0.196380615234375*GFOffset(gt11,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(gt11,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(gt11,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(gt11,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(gt11,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(gt11,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(gt11,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(gt11,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(gt11,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(gt11,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(gt11,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(gt11,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(gt11,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(gt11,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(gt11,7,0,0) - 
      0.196380615234375*GFOffset(gt11,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(gt11,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(gt11,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(gt11,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(gt11,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(gt11,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(gt11,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(gt11,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(gt11,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(gt11,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(gt11,1,0,0) - 
      2.630489733142368322167942483*GFOffset(gt11,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(gt11,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(gt11,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(gt11,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(gt11,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(gt11,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(gt11,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(gt11,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(gt11,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(gt11,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(gt11,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(gt11,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(gt11,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(gt11,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(gt11,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(gt11,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(gt11,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(gt11,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(gt11,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(gt11,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(gt11,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(gt11,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(gt11,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(gt11,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(gt11,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(gt11,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(gt11,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(gt11,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(gt11,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(gt11,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(gt11,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(gt11,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(gt11,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(gt11,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(gt11,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(gt11,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(gt11,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(gt11,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(gt11,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(gt11,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(gt11,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(gt11,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(gt11,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(gt11,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(gt11,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(gt11,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(gt11,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(gt11,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(gt11,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(gt11,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(gt11,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(gt11,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(gt11,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(gt11,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(gt11,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(gt11,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(gt11,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(gt11,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(gt11,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(gt11,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(gt11,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(gt11,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(gt11,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(gt11,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(gt11,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(gt11,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(gt11,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(gt11,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(gt11,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(gt11,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(gt11,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(gt11,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(gt11,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(gt11,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(gt11,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(gt11,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(gt11,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(gt11,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(gt11,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(gt11,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(gt11,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(gt11,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(gt11,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(gt11,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(gt11,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(gt11,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(gt11,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(gt11,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(gt11,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(gt11,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(gt11,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(gt11,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(gt11,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(gt11,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(gt11,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(gt11,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(gt11,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(gt11,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(gt11,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(gt11,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(gt11,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(gt11,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(gt11,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(gt11,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(gt11,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(gt11,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(gt11,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(gt11,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(gt11,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(gt11,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(gt11,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(gt11,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(gt11,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(gt11,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(gt11,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(gt11,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(gt11,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(gt11,-1,0,0) + 
      68.*GFOffset(gt11,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(gt11,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(gt11,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt112 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(gt11,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(gt11,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(gt11,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt11,0,1,0) - 
      36.825792804916105238285776948*GFOffset(gt11,0,2,0) + 
      21.042577052349419249515496693*GFOffset(gt11,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(gt11,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(gt11,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(gt11,0,6,0) + 
      6.225793703001792996013745096*GFOffset(gt11,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(gt11,0,8,0) + 
      4.242018040981331373618358215*GFOffset(gt11,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(gt11,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(gt11,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(gt11,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(gt11,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(gt11,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(gt11,0,15,0) - 
      0.5*GFOffset(gt11,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(gt11,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(gt11,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(gt11,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(gt11,0,3,0) - 
      4.376597384264969120661066707*GFOffset(gt11,0,4,0) + 
      3.303076725656509756145262224*GFOffset(gt11,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(gt11,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(gt11,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(gt11,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(gt11,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(gt11,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(gt11,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(gt11,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(gt11,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(gt11,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(gt11,0,15,0),0) + IfThen(tj == 
      2,3.4189873913829435992478256345*GFOffset(gt11,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(gt11,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(gt11,0,1,0) - 
      6.003906719792868453304381935*GFOffset(gt11,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(gt11,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(gt11,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(gt11,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(gt11,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(gt11,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(gt11,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(gt11,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(gt11,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(gt11,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(gt11,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(gt11,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(gt11,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(gt11,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(gt11,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(gt11,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(gt11,0,1,0) - 
      4.390240639181825578641202719*GFOffset(gt11,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(gt11,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(gt11,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(gt11,0,5,0) - 
      1.29435140870084806656280919*GFOffset(gt11,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(gt11,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(gt11,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(gt11,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(gt11,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(gt11,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(gt11,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(gt11,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(gt11,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(gt11,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(gt11,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(gt11,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(gt11,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(gt11,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(gt11,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(gt11,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(gt11,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(gt11,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(gt11,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(gt11,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(gt11,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(gt11,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(gt11,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(gt11,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(gt11,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(gt11,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(gt11,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(gt11,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(gt11,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(gt11,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(gt11,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(gt11,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(gt11,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(gt11,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(gt11,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(gt11,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(gt11,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(gt11,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(gt11,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(gt11,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(gt11,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(gt11,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(gt11,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(gt11,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(gt11,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(gt11,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(gt11,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(gt11,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(gt11,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(gt11,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(gt11,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(gt11,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(gt11,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(gt11,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(gt11,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(gt11,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(gt11,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(gt11,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(gt11,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(gt11,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(gt11,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(gt11,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(gt11,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(gt11,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(gt11,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(gt11,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(gt11,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(gt11,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(gt11,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(gt11,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(gt11,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(gt11,0,9,0),0) + IfThen(tj == 
      8,0.196380615234375*GFOffset(gt11,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(gt11,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(gt11,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(gt11,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(gt11,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(gt11,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(gt11,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(gt11,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(gt11,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(gt11,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(gt11,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(gt11,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(gt11,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(gt11,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(gt11,0,7,0) - 
      0.196380615234375*GFOffset(gt11,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(gt11,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(gt11,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(gt11,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(gt11,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(gt11,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(gt11,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(gt11,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(gt11,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(gt11,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(gt11,0,1,0) - 
      2.630489733142368322167942483*GFOffset(gt11,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(gt11,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(gt11,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(gt11,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(gt11,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(gt11,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(gt11,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(gt11,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(gt11,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(gt11,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(gt11,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(gt11,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(gt11,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(gt11,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(gt11,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(gt11,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(gt11,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(gt11,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(gt11,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(gt11,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(gt11,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(gt11,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(gt11,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(gt11,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(gt11,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(gt11,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(gt11,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(gt11,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(gt11,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(gt11,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(gt11,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(gt11,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(gt11,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(gt11,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(gt11,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(gt11,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(gt11,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(gt11,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(gt11,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(gt11,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(gt11,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(gt11,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(gt11,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(gt11,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(gt11,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(gt11,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(gt11,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(gt11,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(gt11,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(gt11,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(gt11,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(gt11,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(gt11,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(gt11,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(gt11,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(gt11,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(gt11,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(gt11,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(gt11,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(gt11,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(gt11,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(gt11,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(gt11,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(gt11,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(gt11,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(gt11,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(gt11,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(gt11,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(gt11,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(gt11,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(gt11,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(gt11,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(gt11,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(gt11,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(gt11,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(gt11,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(gt11,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(gt11,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(gt11,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(gt11,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(gt11,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(gt11,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(gt11,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(gt11,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(gt11,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(gt11,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(gt11,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(gt11,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(gt11,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(gt11,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(gt11,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(gt11,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(gt11,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(gt11,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(gt11,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(gt11,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(gt11,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(gt11,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(gt11,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(gt11,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(gt11,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(gt11,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(gt11,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(gt11,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(gt11,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(gt11,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(gt11,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(gt11,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(gt11,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(gt11,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(gt11,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(gt11,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(gt11,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(gt11,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(gt11,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(gt11,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(gt11,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(gt11,0,-1,0) + 
      68.*GFOffset(gt11,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(gt11,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(gt11,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt113 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(gt11,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(gt11,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(gt11,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt11,0,0,1) - 
      36.825792804916105238285776948*GFOffset(gt11,0,0,2) + 
      21.042577052349419249515496693*GFOffset(gt11,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(gt11,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(gt11,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(gt11,0,0,6) + 
      6.225793703001792996013745096*GFOffset(gt11,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(gt11,0,0,8) + 
      4.242018040981331373618358215*GFOffset(gt11,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(gt11,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(gt11,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(gt11,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(gt11,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(gt11,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(gt11,0,0,15) - 
      0.5*GFOffset(gt11,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(gt11,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(gt11,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(gt11,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(gt11,0,0,3) - 
      4.376597384264969120661066707*GFOffset(gt11,0,0,4) + 
      3.303076725656509756145262224*GFOffset(gt11,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(gt11,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(gt11,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(gt11,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(gt11,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(gt11,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(gt11,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(gt11,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(gt11,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(gt11,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(gt11,0,0,15),0) + IfThen(tk == 
      2,3.4189873913829435992478256345*GFOffset(gt11,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(gt11,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(gt11,0,0,1) - 
      6.003906719792868453304381935*GFOffset(gt11,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(gt11,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(gt11,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(gt11,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(gt11,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(gt11,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(gt11,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(gt11,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(gt11,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(gt11,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(gt11,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(gt11,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(gt11,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(gt11,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(gt11,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(gt11,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(gt11,0,0,1) - 
      4.390240639181825578641202719*GFOffset(gt11,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(gt11,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(gt11,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(gt11,0,0,5) - 
      1.29435140870084806656280919*GFOffset(gt11,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(gt11,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(gt11,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(gt11,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(gt11,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(gt11,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(gt11,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(gt11,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(gt11,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(gt11,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(gt11,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(gt11,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(gt11,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(gt11,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(gt11,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(gt11,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(gt11,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(gt11,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(gt11,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(gt11,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(gt11,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(gt11,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(gt11,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(gt11,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(gt11,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(gt11,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(gt11,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(gt11,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(gt11,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(gt11,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(gt11,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(gt11,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(gt11,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(gt11,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(gt11,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(gt11,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(gt11,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(gt11,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(gt11,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(gt11,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(gt11,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(gt11,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(gt11,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(gt11,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(gt11,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(gt11,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(gt11,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(gt11,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(gt11,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(gt11,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(gt11,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(gt11,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(gt11,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(gt11,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(gt11,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(gt11,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(gt11,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(gt11,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(gt11,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(gt11,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(gt11,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(gt11,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(gt11,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(gt11,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(gt11,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(gt11,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(gt11,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(gt11,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(gt11,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(gt11,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(gt11,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(gt11,0,0,9),0) + IfThen(tk == 
      8,0.196380615234375*GFOffset(gt11,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(gt11,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(gt11,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(gt11,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(gt11,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(gt11,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(gt11,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(gt11,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(gt11,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(gt11,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(gt11,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(gt11,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(gt11,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(gt11,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(gt11,0,0,7) - 
      0.196380615234375*GFOffset(gt11,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(gt11,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(gt11,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(gt11,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(gt11,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(gt11,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(gt11,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(gt11,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(gt11,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(gt11,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(gt11,0,0,1) - 
      2.630489733142368322167942483*GFOffset(gt11,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(gt11,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(gt11,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(gt11,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(gt11,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(gt11,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(gt11,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(gt11,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(gt11,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(gt11,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(gt11,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(gt11,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(gt11,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(gt11,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(gt11,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(gt11,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(gt11,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(gt11,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(gt11,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(gt11,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(gt11,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(gt11,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(gt11,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(gt11,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(gt11,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(gt11,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(gt11,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(gt11,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(gt11,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(gt11,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(gt11,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(gt11,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(gt11,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(gt11,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(gt11,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(gt11,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(gt11,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(gt11,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(gt11,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(gt11,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(gt11,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(gt11,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(gt11,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(gt11,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(gt11,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(gt11,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(gt11,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(gt11,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(gt11,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(gt11,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(gt11,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(gt11,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(gt11,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(gt11,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(gt11,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(gt11,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(gt11,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(gt11,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(gt11,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(gt11,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(gt11,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(gt11,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(gt11,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(gt11,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(gt11,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(gt11,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(gt11,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(gt11,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(gt11,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(gt11,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(gt11,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(gt11,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(gt11,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(gt11,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(gt11,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(gt11,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(gt11,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(gt11,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(gt11,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(gt11,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(gt11,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(gt11,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(gt11,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(gt11,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(gt11,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(gt11,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(gt11,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(gt11,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(gt11,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(gt11,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(gt11,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(gt11,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(gt11,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(gt11,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(gt11,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(gt11,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(gt11,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(gt11,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(gt11,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(gt11,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(gt11,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(gt11,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(gt11,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(gt11,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(gt11,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(gt11,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(gt11,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(gt11,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(gt11,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(gt11,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(gt11,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(gt11,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(gt11,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(gt11,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(gt11,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(gt11,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(gt11,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(gt11,0,0,-1) + 
      68.*GFOffset(gt11,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(gt11,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(gt11,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt121 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(gt12,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(gt12,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(gt12,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt12,1,0,0) - 
      36.825792804916105238285776948*GFOffset(gt12,2,0,0) + 
      21.042577052349419249515496693*GFOffset(gt12,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(gt12,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(gt12,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(gt12,6,0,0) + 
      6.225793703001792996013745096*GFOffset(gt12,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(gt12,8,0,0) + 
      4.242018040981331373618358215*GFOffset(gt12,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(gt12,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(gt12,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(gt12,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(gt12,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(gt12,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(gt12,15,0,0) - 
      0.5*GFOffset(gt12,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(gt12,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(gt12,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(gt12,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(gt12,3,0,0) - 
      4.376597384264969120661066707*GFOffset(gt12,4,0,0) + 
      3.303076725656509756145262224*GFOffset(gt12,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(gt12,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(gt12,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(gt12,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(gt12,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(gt12,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(gt12,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(gt12,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(gt12,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(gt12,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(gt12,15,0,0),0) + IfThen(ti == 
      2,3.4189873913829435992478256345*GFOffset(gt12,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(gt12,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(gt12,1,0,0) - 
      6.003906719792868453304381935*GFOffset(gt12,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(gt12,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(gt12,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(gt12,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(gt12,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(gt12,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(gt12,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(gt12,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(gt12,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(gt12,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(gt12,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(gt12,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(gt12,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(gt12,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(gt12,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(gt12,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(gt12,1,0,0) - 
      4.390240639181825578641202719*GFOffset(gt12,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(gt12,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(gt12,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(gt12,5,0,0) - 
      1.29435140870084806656280919*GFOffset(gt12,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(gt12,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(gt12,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(gt12,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(gt12,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(gt12,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(gt12,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(gt12,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(gt12,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(gt12,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(gt12,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(gt12,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(gt12,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(gt12,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(gt12,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(gt12,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(gt12,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(gt12,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(gt12,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(gt12,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(gt12,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(gt12,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(gt12,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(gt12,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(gt12,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(gt12,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(gt12,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(gt12,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(gt12,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(gt12,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(gt12,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(gt12,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(gt12,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(gt12,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(gt12,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(gt12,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(gt12,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(gt12,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(gt12,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(gt12,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(gt12,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(gt12,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(gt12,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(gt12,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(gt12,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(gt12,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(gt12,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(gt12,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(gt12,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(gt12,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(gt12,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(gt12,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(gt12,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(gt12,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(gt12,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(gt12,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(gt12,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(gt12,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(gt12,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(gt12,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(gt12,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(gt12,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(gt12,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(gt12,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(gt12,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(gt12,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(gt12,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(gt12,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(gt12,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(gt12,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(gt12,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(gt12,9,0,0),0) + IfThen(ti == 
      8,0.196380615234375*GFOffset(gt12,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(gt12,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(gt12,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(gt12,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(gt12,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(gt12,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(gt12,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(gt12,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(gt12,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(gt12,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(gt12,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(gt12,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(gt12,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(gt12,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(gt12,7,0,0) - 
      0.196380615234375*GFOffset(gt12,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(gt12,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(gt12,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(gt12,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(gt12,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(gt12,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(gt12,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(gt12,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(gt12,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(gt12,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(gt12,1,0,0) - 
      2.630489733142368322167942483*GFOffset(gt12,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(gt12,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(gt12,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(gt12,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(gt12,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(gt12,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(gt12,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(gt12,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(gt12,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(gt12,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(gt12,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(gt12,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(gt12,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(gt12,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(gt12,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(gt12,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(gt12,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(gt12,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(gt12,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(gt12,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(gt12,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(gt12,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(gt12,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(gt12,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(gt12,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(gt12,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(gt12,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(gt12,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(gt12,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(gt12,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(gt12,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(gt12,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(gt12,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(gt12,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(gt12,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(gt12,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(gt12,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(gt12,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(gt12,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(gt12,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(gt12,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(gt12,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(gt12,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(gt12,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(gt12,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(gt12,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(gt12,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(gt12,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(gt12,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(gt12,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(gt12,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(gt12,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(gt12,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(gt12,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(gt12,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(gt12,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(gt12,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(gt12,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(gt12,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(gt12,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(gt12,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(gt12,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(gt12,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(gt12,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(gt12,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(gt12,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(gt12,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(gt12,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(gt12,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(gt12,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(gt12,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(gt12,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(gt12,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(gt12,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(gt12,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(gt12,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(gt12,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(gt12,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(gt12,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(gt12,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(gt12,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(gt12,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(gt12,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(gt12,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(gt12,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(gt12,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(gt12,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(gt12,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(gt12,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(gt12,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(gt12,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(gt12,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(gt12,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(gt12,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(gt12,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(gt12,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(gt12,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(gt12,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(gt12,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(gt12,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(gt12,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(gt12,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(gt12,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(gt12,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(gt12,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(gt12,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(gt12,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(gt12,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(gt12,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(gt12,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(gt12,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(gt12,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(gt12,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(gt12,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(gt12,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(gt12,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(gt12,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(gt12,-1,0,0) + 
      68.*GFOffset(gt12,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(gt12,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(gt12,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt122 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(gt12,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(gt12,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(gt12,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt12,0,1,0) - 
      36.825792804916105238285776948*GFOffset(gt12,0,2,0) + 
      21.042577052349419249515496693*GFOffset(gt12,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(gt12,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(gt12,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(gt12,0,6,0) + 
      6.225793703001792996013745096*GFOffset(gt12,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(gt12,0,8,0) + 
      4.242018040981331373618358215*GFOffset(gt12,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(gt12,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(gt12,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(gt12,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(gt12,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(gt12,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(gt12,0,15,0) - 
      0.5*GFOffset(gt12,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(gt12,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(gt12,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(gt12,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(gt12,0,3,0) - 
      4.376597384264969120661066707*GFOffset(gt12,0,4,0) + 
      3.303076725656509756145262224*GFOffset(gt12,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(gt12,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(gt12,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(gt12,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(gt12,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(gt12,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(gt12,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(gt12,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(gt12,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(gt12,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(gt12,0,15,0),0) + IfThen(tj == 
      2,3.4189873913829435992478256345*GFOffset(gt12,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(gt12,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(gt12,0,1,0) - 
      6.003906719792868453304381935*GFOffset(gt12,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(gt12,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(gt12,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(gt12,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(gt12,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(gt12,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(gt12,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(gt12,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(gt12,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(gt12,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(gt12,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(gt12,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(gt12,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(gt12,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(gt12,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(gt12,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(gt12,0,1,0) - 
      4.390240639181825578641202719*GFOffset(gt12,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(gt12,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(gt12,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(gt12,0,5,0) - 
      1.29435140870084806656280919*GFOffset(gt12,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(gt12,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(gt12,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(gt12,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(gt12,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(gt12,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(gt12,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(gt12,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(gt12,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(gt12,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(gt12,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(gt12,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(gt12,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(gt12,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(gt12,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(gt12,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(gt12,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(gt12,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(gt12,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(gt12,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(gt12,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(gt12,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(gt12,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(gt12,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(gt12,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(gt12,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(gt12,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(gt12,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(gt12,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(gt12,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(gt12,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(gt12,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(gt12,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(gt12,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(gt12,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(gt12,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(gt12,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(gt12,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(gt12,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(gt12,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(gt12,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(gt12,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(gt12,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(gt12,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(gt12,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(gt12,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(gt12,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(gt12,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(gt12,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(gt12,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(gt12,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(gt12,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(gt12,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(gt12,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(gt12,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(gt12,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(gt12,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(gt12,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(gt12,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(gt12,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(gt12,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(gt12,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(gt12,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(gt12,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(gt12,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(gt12,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(gt12,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(gt12,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(gt12,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(gt12,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(gt12,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(gt12,0,9,0),0) + IfThen(tj == 
      8,0.196380615234375*GFOffset(gt12,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(gt12,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(gt12,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(gt12,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(gt12,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(gt12,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(gt12,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(gt12,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(gt12,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(gt12,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(gt12,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(gt12,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(gt12,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(gt12,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(gt12,0,7,0) - 
      0.196380615234375*GFOffset(gt12,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(gt12,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(gt12,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(gt12,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(gt12,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(gt12,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(gt12,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(gt12,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(gt12,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(gt12,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(gt12,0,1,0) - 
      2.630489733142368322167942483*GFOffset(gt12,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(gt12,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(gt12,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(gt12,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(gt12,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(gt12,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(gt12,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(gt12,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(gt12,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(gt12,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(gt12,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(gt12,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(gt12,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(gt12,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(gt12,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(gt12,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(gt12,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(gt12,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(gt12,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(gt12,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(gt12,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(gt12,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(gt12,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(gt12,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(gt12,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(gt12,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(gt12,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(gt12,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(gt12,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(gt12,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(gt12,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(gt12,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(gt12,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(gt12,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(gt12,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(gt12,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(gt12,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(gt12,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(gt12,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(gt12,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(gt12,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(gt12,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(gt12,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(gt12,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(gt12,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(gt12,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(gt12,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(gt12,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(gt12,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(gt12,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(gt12,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(gt12,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(gt12,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(gt12,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(gt12,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(gt12,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(gt12,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(gt12,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(gt12,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(gt12,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(gt12,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(gt12,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(gt12,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(gt12,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(gt12,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(gt12,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(gt12,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(gt12,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(gt12,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(gt12,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(gt12,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(gt12,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(gt12,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(gt12,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(gt12,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(gt12,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(gt12,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(gt12,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(gt12,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(gt12,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(gt12,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(gt12,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(gt12,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(gt12,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(gt12,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(gt12,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(gt12,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(gt12,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(gt12,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(gt12,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(gt12,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(gt12,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(gt12,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(gt12,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(gt12,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(gt12,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(gt12,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(gt12,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(gt12,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(gt12,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(gt12,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(gt12,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(gt12,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(gt12,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(gt12,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(gt12,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(gt12,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(gt12,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(gt12,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(gt12,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(gt12,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(gt12,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(gt12,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(gt12,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(gt12,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(gt12,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(gt12,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(gt12,0,-1,0) + 
      68.*GFOffset(gt12,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(gt12,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(gt12,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt123 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(gt12,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(gt12,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(gt12,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt12,0,0,1) - 
      36.825792804916105238285776948*GFOffset(gt12,0,0,2) + 
      21.042577052349419249515496693*GFOffset(gt12,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(gt12,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(gt12,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(gt12,0,0,6) + 
      6.225793703001792996013745096*GFOffset(gt12,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(gt12,0,0,8) + 
      4.242018040981331373618358215*GFOffset(gt12,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(gt12,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(gt12,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(gt12,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(gt12,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(gt12,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(gt12,0,0,15) - 
      0.5*GFOffset(gt12,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(gt12,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(gt12,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(gt12,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(gt12,0,0,3) - 
      4.376597384264969120661066707*GFOffset(gt12,0,0,4) + 
      3.303076725656509756145262224*GFOffset(gt12,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(gt12,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(gt12,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(gt12,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(gt12,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(gt12,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(gt12,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(gt12,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(gt12,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(gt12,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(gt12,0,0,15),0) + IfThen(tk == 
      2,3.4189873913829435992478256345*GFOffset(gt12,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(gt12,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(gt12,0,0,1) - 
      6.003906719792868453304381935*GFOffset(gt12,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(gt12,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(gt12,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(gt12,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(gt12,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(gt12,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(gt12,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(gt12,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(gt12,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(gt12,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(gt12,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(gt12,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(gt12,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(gt12,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(gt12,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(gt12,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(gt12,0,0,1) - 
      4.390240639181825578641202719*GFOffset(gt12,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(gt12,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(gt12,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(gt12,0,0,5) - 
      1.29435140870084806656280919*GFOffset(gt12,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(gt12,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(gt12,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(gt12,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(gt12,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(gt12,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(gt12,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(gt12,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(gt12,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(gt12,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(gt12,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(gt12,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(gt12,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(gt12,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(gt12,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(gt12,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(gt12,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(gt12,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(gt12,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(gt12,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(gt12,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(gt12,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(gt12,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(gt12,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(gt12,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(gt12,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(gt12,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(gt12,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(gt12,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(gt12,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(gt12,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(gt12,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(gt12,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(gt12,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(gt12,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(gt12,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(gt12,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(gt12,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(gt12,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(gt12,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(gt12,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(gt12,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(gt12,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(gt12,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(gt12,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(gt12,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(gt12,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(gt12,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(gt12,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(gt12,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(gt12,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(gt12,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(gt12,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(gt12,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(gt12,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(gt12,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(gt12,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(gt12,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(gt12,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(gt12,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(gt12,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(gt12,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(gt12,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(gt12,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(gt12,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(gt12,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(gt12,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(gt12,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(gt12,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(gt12,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(gt12,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(gt12,0,0,9),0) + IfThen(tk == 
      8,0.196380615234375*GFOffset(gt12,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(gt12,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(gt12,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(gt12,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(gt12,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(gt12,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(gt12,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(gt12,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(gt12,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(gt12,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(gt12,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(gt12,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(gt12,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(gt12,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(gt12,0,0,7) - 
      0.196380615234375*GFOffset(gt12,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(gt12,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(gt12,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(gt12,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(gt12,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(gt12,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(gt12,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(gt12,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(gt12,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(gt12,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(gt12,0,0,1) - 
      2.630489733142368322167942483*GFOffset(gt12,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(gt12,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(gt12,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(gt12,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(gt12,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(gt12,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(gt12,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(gt12,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(gt12,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(gt12,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(gt12,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(gt12,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(gt12,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(gt12,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(gt12,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(gt12,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(gt12,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(gt12,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(gt12,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(gt12,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(gt12,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(gt12,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(gt12,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(gt12,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(gt12,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(gt12,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(gt12,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(gt12,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(gt12,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(gt12,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(gt12,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(gt12,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(gt12,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(gt12,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(gt12,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(gt12,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(gt12,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(gt12,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(gt12,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(gt12,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(gt12,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(gt12,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(gt12,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(gt12,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(gt12,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(gt12,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(gt12,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(gt12,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(gt12,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(gt12,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(gt12,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(gt12,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(gt12,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(gt12,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(gt12,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(gt12,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(gt12,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(gt12,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(gt12,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(gt12,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(gt12,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(gt12,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(gt12,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(gt12,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(gt12,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(gt12,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(gt12,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(gt12,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(gt12,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(gt12,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(gt12,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(gt12,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(gt12,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(gt12,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(gt12,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(gt12,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(gt12,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(gt12,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(gt12,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(gt12,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(gt12,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(gt12,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(gt12,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(gt12,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(gt12,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(gt12,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(gt12,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(gt12,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(gt12,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(gt12,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(gt12,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(gt12,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(gt12,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(gt12,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(gt12,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(gt12,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(gt12,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(gt12,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(gt12,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(gt12,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(gt12,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(gt12,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(gt12,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(gt12,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(gt12,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(gt12,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(gt12,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(gt12,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(gt12,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(gt12,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(gt12,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(gt12,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(gt12,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(gt12,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(gt12,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(gt12,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(gt12,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(gt12,0,0,-1) + 
      68.*GFOffset(gt12,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(gt12,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(gt12,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt131 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(gt13,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(gt13,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(gt13,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt13,1,0,0) - 
      36.825792804916105238285776948*GFOffset(gt13,2,0,0) + 
      21.042577052349419249515496693*GFOffset(gt13,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(gt13,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(gt13,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(gt13,6,0,0) + 
      6.225793703001792996013745096*GFOffset(gt13,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(gt13,8,0,0) + 
      4.242018040981331373618358215*GFOffset(gt13,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(gt13,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(gt13,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(gt13,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(gt13,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(gt13,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(gt13,15,0,0) - 
      0.5*GFOffset(gt13,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(gt13,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(gt13,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(gt13,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(gt13,3,0,0) - 
      4.376597384264969120661066707*GFOffset(gt13,4,0,0) + 
      3.303076725656509756145262224*GFOffset(gt13,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(gt13,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(gt13,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(gt13,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(gt13,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(gt13,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(gt13,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(gt13,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(gt13,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(gt13,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(gt13,15,0,0),0) + IfThen(ti == 
      2,3.4189873913829435992478256345*GFOffset(gt13,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(gt13,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(gt13,1,0,0) - 
      6.003906719792868453304381935*GFOffset(gt13,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(gt13,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(gt13,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(gt13,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(gt13,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(gt13,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(gt13,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(gt13,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(gt13,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(gt13,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(gt13,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(gt13,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(gt13,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(gt13,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(gt13,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(gt13,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(gt13,1,0,0) - 
      4.390240639181825578641202719*GFOffset(gt13,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(gt13,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(gt13,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(gt13,5,0,0) - 
      1.29435140870084806656280919*GFOffset(gt13,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(gt13,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(gt13,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(gt13,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(gt13,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(gt13,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(gt13,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(gt13,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(gt13,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(gt13,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(gt13,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(gt13,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(gt13,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(gt13,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(gt13,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(gt13,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(gt13,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(gt13,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(gt13,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(gt13,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(gt13,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(gt13,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(gt13,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(gt13,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(gt13,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(gt13,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(gt13,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(gt13,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(gt13,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(gt13,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(gt13,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(gt13,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(gt13,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(gt13,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(gt13,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(gt13,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(gt13,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(gt13,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(gt13,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(gt13,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(gt13,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(gt13,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(gt13,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(gt13,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(gt13,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(gt13,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(gt13,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(gt13,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(gt13,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(gt13,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(gt13,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(gt13,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(gt13,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(gt13,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(gt13,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(gt13,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(gt13,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(gt13,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(gt13,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(gt13,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(gt13,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(gt13,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(gt13,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(gt13,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(gt13,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(gt13,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(gt13,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(gt13,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(gt13,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(gt13,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(gt13,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(gt13,9,0,0),0) + IfThen(ti == 
      8,0.196380615234375*GFOffset(gt13,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(gt13,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(gt13,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(gt13,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(gt13,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(gt13,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(gt13,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(gt13,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(gt13,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(gt13,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(gt13,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(gt13,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(gt13,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(gt13,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(gt13,7,0,0) - 
      0.196380615234375*GFOffset(gt13,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(gt13,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(gt13,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(gt13,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(gt13,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(gt13,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(gt13,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(gt13,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(gt13,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(gt13,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(gt13,1,0,0) - 
      2.630489733142368322167942483*GFOffset(gt13,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(gt13,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(gt13,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(gt13,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(gt13,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(gt13,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(gt13,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(gt13,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(gt13,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(gt13,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(gt13,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(gt13,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(gt13,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(gt13,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(gt13,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(gt13,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(gt13,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(gt13,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(gt13,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(gt13,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(gt13,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(gt13,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(gt13,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(gt13,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(gt13,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(gt13,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(gt13,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(gt13,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(gt13,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(gt13,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(gt13,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(gt13,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(gt13,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(gt13,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(gt13,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(gt13,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(gt13,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(gt13,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(gt13,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(gt13,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(gt13,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(gt13,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(gt13,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(gt13,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(gt13,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(gt13,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(gt13,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(gt13,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(gt13,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(gt13,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(gt13,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(gt13,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(gt13,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(gt13,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(gt13,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(gt13,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(gt13,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(gt13,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(gt13,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(gt13,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(gt13,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(gt13,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(gt13,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(gt13,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(gt13,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(gt13,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(gt13,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(gt13,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(gt13,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(gt13,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(gt13,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(gt13,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(gt13,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(gt13,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(gt13,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(gt13,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(gt13,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(gt13,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(gt13,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(gt13,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(gt13,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(gt13,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(gt13,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(gt13,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(gt13,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(gt13,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(gt13,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(gt13,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(gt13,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(gt13,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(gt13,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(gt13,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(gt13,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(gt13,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(gt13,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(gt13,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(gt13,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(gt13,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(gt13,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(gt13,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(gt13,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(gt13,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(gt13,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(gt13,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(gt13,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(gt13,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(gt13,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(gt13,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(gt13,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(gt13,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(gt13,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(gt13,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(gt13,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(gt13,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(gt13,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(gt13,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(gt13,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(gt13,-1,0,0) + 
      68.*GFOffset(gt13,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(gt13,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(gt13,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt132 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(gt13,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(gt13,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(gt13,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt13,0,1,0) - 
      36.825792804916105238285776948*GFOffset(gt13,0,2,0) + 
      21.042577052349419249515496693*GFOffset(gt13,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(gt13,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(gt13,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(gt13,0,6,0) + 
      6.225793703001792996013745096*GFOffset(gt13,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(gt13,0,8,0) + 
      4.242018040981331373618358215*GFOffset(gt13,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(gt13,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(gt13,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(gt13,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(gt13,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(gt13,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(gt13,0,15,0) - 
      0.5*GFOffset(gt13,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(gt13,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(gt13,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(gt13,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(gt13,0,3,0) - 
      4.376597384264969120661066707*GFOffset(gt13,0,4,0) + 
      3.303076725656509756145262224*GFOffset(gt13,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(gt13,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(gt13,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(gt13,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(gt13,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(gt13,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(gt13,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(gt13,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(gt13,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(gt13,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(gt13,0,15,0),0) + IfThen(tj == 
      2,3.4189873913829435992478256345*GFOffset(gt13,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(gt13,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(gt13,0,1,0) - 
      6.003906719792868453304381935*GFOffset(gt13,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(gt13,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(gt13,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(gt13,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(gt13,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(gt13,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(gt13,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(gt13,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(gt13,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(gt13,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(gt13,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(gt13,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(gt13,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(gt13,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(gt13,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(gt13,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(gt13,0,1,0) - 
      4.390240639181825578641202719*GFOffset(gt13,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(gt13,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(gt13,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(gt13,0,5,0) - 
      1.29435140870084806656280919*GFOffset(gt13,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(gt13,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(gt13,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(gt13,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(gt13,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(gt13,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(gt13,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(gt13,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(gt13,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(gt13,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(gt13,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(gt13,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(gt13,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(gt13,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(gt13,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(gt13,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(gt13,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(gt13,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(gt13,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(gt13,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(gt13,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(gt13,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(gt13,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(gt13,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(gt13,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(gt13,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(gt13,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(gt13,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(gt13,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(gt13,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(gt13,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(gt13,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(gt13,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(gt13,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(gt13,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(gt13,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(gt13,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(gt13,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(gt13,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(gt13,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(gt13,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(gt13,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(gt13,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(gt13,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(gt13,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(gt13,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(gt13,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(gt13,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(gt13,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(gt13,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(gt13,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(gt13,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(gt13,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(gt13,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(gt13,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(gt13,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(gt13,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(gt13,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(gt13,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(gt13,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(gt13,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(gt13,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(gt13,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(gt13,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(gt13,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(gt13,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(gt13,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(gt13,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(gt13,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(gt13,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(gt13,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(gt13,0,9,0),0) + IfThen(tj == 
      8,0.196380615234375*GFOffset(gt13,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(gt13,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(gt13,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(gt13,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(gt13,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(gt13,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(gt13,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(gt13,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(gt13,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(gt13,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(gt13,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(gt13,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(gt13,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(gt13,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(gt13,0,7,0) - 
      0.196380615234375*GFOffset(gt13,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(gt13,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(gt13,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(gt13,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(gt13,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(gt13,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(gt13,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(gt13,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(gt13,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(gt13,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(gt13,0,1,0) - 
      2.630489733142368322167942483*GFOffset(gt13,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(gt13,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(gt13,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(gt13,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(gt13,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(gt13,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(gt13,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(gt13,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(gt13,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(gt13,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(gt13,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(gt13,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(gt13,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(gt13,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(gt13,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(gt13,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(gt13,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(gt13,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(gt13,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(gt13,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(gt13,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(gt13,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(gt13,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(gt13,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(gt13,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(gt13,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(gt13,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(gt13,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(gt13,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(gt13,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(gt13,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(gt13,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(gt13,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(gt13,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(gt13,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(gt13,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(gt13,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(gt13,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(gt13,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(gt13,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(gt13,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(gt13,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(gt13,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(gt13,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(gt13,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(gt13,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(gt13,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(gt13,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(gt13,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(gt13,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(gt13,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(gt13,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(gt13,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(gt13,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(gt13,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(gt13,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(gt13,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(gt13,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(gt13,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(gt13,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(gt13,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(gt13,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(gt13,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(gt13,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(gt13,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(gt13,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(gt13,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(gt13,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(gt13,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(gt13,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(gt13,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(gt13,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(gt13,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(gt13,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(gt13,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(gt13,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(gt13,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(gt13,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(gt13,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(gt13,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(gt13,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(gt13,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(gt13,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(gt13,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(gt13,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(gt13,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(gt13,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(gt13,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(gt13,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(gt13,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(gt13,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(gt13,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(gt13,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(gt13,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(gt13,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(gt13,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(gt13,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(gt13,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(gt13,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(gt13,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(gt13,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(gt13,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(gt13,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(gt13,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(gt13,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(gt13,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(gt13,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(gt13,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(gt13,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(gt13,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(gt13,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(gt13,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(gt13,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(gt13,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(gt13,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(gt13,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(gt13,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(gt13,0,-1,0) + 
      68.*GFOffset(gt13,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(gt13,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(gt13,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt133 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(gt13,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(gt13,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(gt13,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt13,0,0,1) - 
      36.825792804916105238285776948*GFOffset(gt13,0,0,2) + 
      21.042577052349419249515496693*GFOffset(gt13,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(gt13,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(gt13,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(gt13,0,0,6) + 
      6.225793703001792996013745096*GFOffset(gt13,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(gt13,0,0,8) + 
      4.242018040981331373618358215*GFOffset(gt13,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(gt13,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(gt13,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(gt13,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(gt13,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(gt13,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(gt13,0,0,15) - 
      0.5*GFOffset(gt13,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(gt13,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(gt13,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(gt13,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(gt13,0,0,3) - 
      4.376597384264969120661066707*GFOffset(gt13,0,0,4) + 
      3.303076725656509756145262224*GFOffset(gt13,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(gt13,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(gt13,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(gt13,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(gt13,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(gt13,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(gt13,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(gt13,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(gt13,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(gt13,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(gt13,0,0,15),0) + IfThen(tk == 
      2,3.4189873913829435992478256345*GFOffset(gt13,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(gt13,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(gt13,0,0,1) - 
      6.003906719792868453304381935*GFOffset(gt13,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(gt13,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(gt13,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(gt13,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(gt13,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(gt13,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(gt13,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(gt13,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(gt13,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(gt13,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(gt13,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(gt13,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(gt13,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(gt13,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(gt13,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(gt13,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(gt13,0,0,1) - 
      4.390240639181825578641202719*GFOffset(gt13,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(gt13,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(gt13,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(gt13,0,0,5) - 
      1.29435140870084806656280919*GFOffset(gt13,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(gt13,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(gt13,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(gt13,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(gt13,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(gt13,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(gt13,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(gt13,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(gt13,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(gt13,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(gt13,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(gt13,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(gt13,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(gt13,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(gt13,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(gt13,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(gt13,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(gt13,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(gt13,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(gt13,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(gt13,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(gt13,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(gt13,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(gt13,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(gt13,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(gt13,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(gt13,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(gt13,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(gt13,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(gt13,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(gt13,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(gt13,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(gt13,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(gt13,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(gt13,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(gt13,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(gt13,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(gt13,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(gt13,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(gt13,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(gt13,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(gt13,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(gt13,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(gt13,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(gt13,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(gt13,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(gt13,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(gt13,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(gt13,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(gt13,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(gt13,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(gt13,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(gt13,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(gt13,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(gt13,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(gt13,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(gt13,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(gt13,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(gt13,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(gt13,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(gt13,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(gt13,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(gt13,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(gt13,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(gt13,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(gt13,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(gt13,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(gt13,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(gt13,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(gt13,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(gt13,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(gt13,0,0,9),0) + IfThen(tk == 
      8,0.196380615234375*GFOffset(gt13,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(gt13,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(gt13,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(gt13,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(gt13,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(gt13,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(gt13,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(gt13,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(gt13,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(gt13,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(gt13,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(gt13,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(gt13,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(gt13,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(gt13,0,0,7) - 
      0.196380615234375*GFOffset(gt13,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(gt13,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(gt13,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(gt13,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(gt13,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(gt13,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(gt13,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(gt13,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(gt13,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(gt13,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(gt13,0,0,1) - 
      2.630489733142368322167942483*GFOffset(gt13,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(gt13,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(gt13,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(gt13,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(gt13,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(gt13,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(gt13,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(gt13,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(gt13,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(gt13,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(gt13,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(gt13,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(gt13,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(gt13,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(gt13,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(gt13,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(gt13,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(gt13,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(gt13,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(gt13,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(gt13,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(gt13,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(gt13,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(gt13,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(gt13,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(gt13,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(gt13,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(gt13,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(gt13,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(gt13,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(gt13,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(gt13,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(gt13,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(gt13,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(gt13,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(gt13,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(gt13,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(gt13,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(gt13,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(gt13,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(gt13,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(gt13,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(gt13,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(gt13,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(gt13,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(gt13,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(gt13,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(gt13,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(gt13,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(gt13,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(gt13,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(gt13,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(gt13,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(gt13,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(gt13,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(gt13,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(gt13,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(gt13,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(gt13,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(gt13,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(gt13,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(gt13,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(gt13,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(gt13,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(gt13,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(gt13,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(gt13,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(gt13,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(gt13,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(gt13,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(gt13,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(gt13,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(gt13,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(gt13,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(gt13,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(gt13,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(gt13,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(gt13,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(gt13,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(gt13,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(gt13,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(gt13,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(gt13,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(gt13,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(gt13,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(gt13,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(gt13,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(gt13,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(gt13,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(gt13,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(gt13,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(gt13,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(gt13,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(gt13,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(gt13,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(gt13,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(gt13,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(gt13,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(gt13,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(gt13,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(gt13,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(gt13,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(gt13,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(gt13,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(gt13,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(gt13,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(gt13,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(gt13,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(gt13,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(gt13,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(gt13,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(gt13,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(gt13,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(gt13,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(gt13,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(gt13,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(gt13,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(gt13,0,0,-1) + 
      68.*GFOffset(gt13,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(gt13,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(gt13,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt221 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(gt22,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(gt22,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(gt22,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt22,1,0,0) - 
      36.825792804916105238285776948*GFOffset(gt22,2,0,0) + 
      21.042577052349419249515496693*GFOffset(gt22,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(gt22,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(gt22,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(gt22,6,0,0) + 
      6.225793703001792996013745096*GFOffset(gt22,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(gt22,8,0,0) + 
      4.242018040981331373618358215*GFOffset(gt22,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(gt22,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(gt22,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(gt22,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(gt22,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(gt22,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(gt22,15,0,0) - 
      0.5*GFOffset(gt22,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(gt22,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(gt22,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(gt22,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(gt22,3,0,0) - 
      4.376597384264969120661066707*GFOffset(gt22,4,0,0) + 
      3.303076725656509756145262224*GFOffset(gt22,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(gt22,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(gt22,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(gt22,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(gt22,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(gt22,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(gt22,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(gt22,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(gt22,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(gt22,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(gt22,15,0,0),0) + IfThen(ti == 
      2,3.4189873913829435992478256345*GFOffset(gt22,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(gt22,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(gt22,1,0,0) - 
      6.003906719792868453304381935*GFOffset(gt22,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(gt22,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(gt22,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(gt22,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(gt22,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(gt22,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(gt22,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(gt22,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(gt22,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(gt22,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(gt22,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(gt22,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(gt22,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(gt22,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(gt22,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(gt22,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(gt22,1,0,0) - 
      4.390240639181825578641202719*GFOffset(gt22,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(gt22,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(gt22,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(gt22,5,0,0) - 
      1.29435140870084806656280919*GFOffset(gt22,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(gt22,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(gt22,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(gt22,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(gt22,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(gt22,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(gt22,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(gt22,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(gt22,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(gt22,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(gt22,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(gt22,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(gt22,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(gt22,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(gt22,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(gt22,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(gt22,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(gt22,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(gt22,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(gt22,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(gt22,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(gt22,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(gt22,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(gt22,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(gt22,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(gt22,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(gt22,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(gt22,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(gt22,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(gt22,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(gt22,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(gt22,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(gt22,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(gt22,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(gt22,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(gt22,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(gt22,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(gt22,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(gt22,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(gt22,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(gt22,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(gt22,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(gt22,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(gt22,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(gt22,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(gt22,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(gt22,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(gt22,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(gt22,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(gt22,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(gt22,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(gt22,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(gt22,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(gt22,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(gt22,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(gt22,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(gt22,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(gt22,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(gt22,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(gt22,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(gt22,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(gt22,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(gt22,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(gt22,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(gt22,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(gt22,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(gt22,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(gt22,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(gt22,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(gt22,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(gt22,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(gt22,9,0,0),0) + IfThen(ti == 
      8,0.196380615234375*GFOffset(gt22,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(gt22,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(gt22,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(gt22,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(gt22,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(gt22,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(gt22,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(gt22,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(gt22,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(gt22,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(gt22,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(gt22,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(gt22,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(gt22,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(gt22,7,0,0) - 
      0.196380615234375*GFOffset(gt22,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(gt22,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(gt22,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(gt22,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(gt22,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(gt22,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(gt22,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(gt22,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(gt22,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(gt22,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(gt22,1,0,0) - 
      2.630489733142368322167942483*GFOffset(gt22,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(gt22,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(gt22,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(gt22,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(gt22,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(gt22,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(gt22,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(gt22,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(gt22,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(gt22,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(gt22,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(gt22,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(gt22,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(gt22,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(gt22,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(gt22,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(gt22,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(gt22,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(gt22,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(gt22,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(gt22,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(gt22,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(gt22,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(gt22,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(gt22,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(gt22,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(gt22,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(gt22,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(gt22,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(gt22,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(gt22,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(gt22,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(gt22,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(gt22,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(gt22,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(gt22,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(gt22,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(gt22,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(gt22,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(gt22,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(gt22,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(gt22,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(gt22,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(gt22,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(gt22,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(gt22,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(gt22,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(gt22,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(gt22,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(gt22,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(gt22,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(gt22,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(gt22,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(gt22,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(gt22,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(gt22,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(gt22,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(gt22,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(gt22,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(gt22,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(gt22,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(gt22,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(gt22,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(gt22,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(gt22,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(gt22,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(gt22,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(gt22,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(gt22,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(gt22,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(gt22,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(gt22,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(gt22,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(gt22,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(gt22,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(gt22,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(gt22,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(gt22,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(gt22,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(gt22,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(gt22,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(gt22,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(gt22,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(gt22,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(gt22,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(gt22,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(gt22,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(gt22,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(gt22,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(gt22,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(gt22,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(gt22,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(gt22,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(gt22,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(gt22,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(gt22,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(gt22,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(gt22,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(gt22,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(gt22,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(gt22,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(gt22,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(gt22,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(gt22,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(gt22,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(gt22,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(gt22,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(gt22,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(gt22,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(gt22,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(gt22,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(gt22,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(gt22,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(gt22,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(gt22,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(gt22,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(gt22,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(gt22,-1,0,0) + 
      68.*GFOffset(gt22,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(gt22,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(gt22,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt222 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(gt22,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(gt22,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(gt22,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt22,0,1,0) - 
      36.825792804916105238285776948*GFOffset(gt22,0,2,0) + 
      21.042577052349419249515496693*GFOffset(gt22,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(gt22,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(gt22,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(gt22,0,6,0) + 
      6.225793703001792996013745096*GFOffset(gt22,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(gt22,0,8,0) + 
      4.242018040981331373618358215*GFOffset(gt22,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(gt22,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(gt22,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(gt22,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(gt22,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(gt22,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(gt22,0,15,0) - 
      0.5*GFOffset(gt22,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(gt22,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(gt22,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(gt22,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(gt22,0,3,0) - 
      4.376597384264969120661066707*GFOffset(gt22,0,4,0) + 
      3.303076725656509756145262224*GFOffset(gt22,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(gt22,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(gt22,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(gt22,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(gt22,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(gt22,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(gt22,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(gt22,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(gt22,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(gt22,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(gt22,0,15,0),0) + IfThen(tj == 
      2,3.4189873913829435992478256345*GFOffset(gt22,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(gt22,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(gt22,0,1,0) - 
      6.003906719792868453304381935*GFOffset(gt22,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(gt22,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(gt22,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(gt22,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(gt22,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(gt22,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(gt22,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(gt22,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(gt22,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(gt22,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(gt22,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(gt22,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(gt22,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(gt22,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(gt22,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(gt22,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(gt22,0,1,0) - 
      4.390240639181825578641202719*GFOffset(gt22,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(gt22,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(gt22,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(gt22,0,5,0) - 
      1.29435140870084806656280919*GFOffset(gt22,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(gt22,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(gt22,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(gt22,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(gt22,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(gt22,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(gt22,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(gt22,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(gt22,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(gt22,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(gt22,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(gt22,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(gt22,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(gt22,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(gt22,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(gt22,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(gt22,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(gt22,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(gt22,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(gt22,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(gt22,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(gt22,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(gt22,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(gt22,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(gt22,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(gt22,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(gt22,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(gt22,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(gt22,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(gt22,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(gt22,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(gt22,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(gt22,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(gt22,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(gt22,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(gt22,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(gt22,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(gt22,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(gt22,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(gt22,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(gt22,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(gt22,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(gt22,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(gt22,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(gt22,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(gt22,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(gt22,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(gt22,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(gt22,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(gt22,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(gt22,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(gt22,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(gt22,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(gt22,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(gt22,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(gt22,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(gt22,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(gt22,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(gt22,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(gt22,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(gt22,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(gt22,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(gt22,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(gt22,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(gt22,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(gt22,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(gt22,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(gt22,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(gt22,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(gt22,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(gt22,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(gt22,0,9,0),0) + IfThen(tj == 
      8,0.196380615234375*GFOffset(gt22,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(gt22,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(gt22,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(gt22,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(gt22,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(gt22,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(gt22,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(gt22,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(gt22,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(gt22,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(gt22,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(gt22,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(gt22,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(gt22,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(gt22,0,7,0) - 
      0.196380615234375*GFOffset(gt22,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(gt22,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(gt22,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(gt22,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(gt22,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(gt22,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(gt22,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(gt22,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(gt22,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(gt22,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(gt22,0,1,0) - 
      2.630489733142368322167942483*GFOffset(gt22,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(gt22,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(gt22,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(gt22,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(gt22,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(gt22,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(gt22,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(gt22,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(gt22,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(gt22,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(gt22,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(gt22,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(gt22,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(gt22,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(gt22,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(gt22,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(gt22,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(gt22,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(gt22,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(gt22,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(gt22,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(gt22,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(gt22,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(gt22,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(gt22,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(gt22,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(gt22,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(gt22,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(gt22,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(gt22,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(gt22,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(gt22,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(gt22,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(gt22,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(gt22,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(gt22,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(gt22,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(gt22,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(gt22,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(gt22,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(gt22,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(gt22,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(gt22,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(gt22,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(gt22,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(gt22,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(gt22,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(gt22,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(gt22,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(gt22,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(gt22,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(gt22,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(gt22,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(gt22,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(gt22,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(gt22,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(gt22,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(gt22,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(gt22,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(gt22,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(gt22,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(gt22,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(gt22,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(gt22,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(gt22,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(gt22,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(gt22,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(gt22,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(gt22,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(gt22,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(gt22,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(gt22,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(gt22,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(gt22,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(gt22,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(gt22,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(gt22,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(gt22,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(gt22,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(gt22,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(gt22,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(gt22,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(gt22,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(gt22,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(gt22,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(gt22,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(gt22,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(gt22,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(gt22,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(gt22,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(gt22,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(gt22,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(gt22,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(gt22,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(gt22,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(gt22,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(gt22,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(gt22,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(gt22,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(gt22,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(gt22,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(gt22,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(gt22,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(gt22,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(gt22,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(gt22,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(gt22,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(gt22,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(gt22,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(gt22,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(gt22,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(gt22,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(gt22,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(gt22,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(gt22,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(gt22,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(gt22,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(gt22,0,-1,0) + 
      68.*GFOffset(gt22,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(gt22,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(gt22,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt223 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(gt22,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(gt22,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(gt22,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt22,0,0,1) - 
      36.825792804916105238285776948*GFOffset(gt22,0,0,2) + 
      21.042577052349419249515496693*GFOffset(gt22,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(gt22,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(gt22,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(gt22,0,0,6) + 
      6.225793703001792996013745096*GFOffset(gt22,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(gt22,0,0,8) + 
      4.242018040981331373618358215*GFOffset(gt22,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(gt22,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(gt22,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(gt22,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(gt22,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(gt22,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(gt22,0,0,15) - 
      0.5*GFOffset(gt22,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(gt22,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(gt22,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(gt22,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(gt22,0,0,3) - 
      4.376597384264969120661066707*GFOffset(gt22,0,0,4) + 
      3.303076725656509756145262224*GFOffset(gt22,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(gt22,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(gt22,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(gt22,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(gt22,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(gt22,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(gt22,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(gt22,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(gt22,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(gt22,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(gt22,0,0,15),0) + IfThen(tk == 
      2,3.4189873913829435992478256345*GFOffset(gt22,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(gt22,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(gt22,0,0,1) - 
      6.003906719792868453304381935*GFOffset(gt22,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(gt22,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(gt22,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(gt22,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(gt22,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(gt22,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(gt22,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(gt22,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(gt22,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(gt22,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(gt22,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(gt22,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(gt22,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(gt22,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(gt22,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(gt22,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(gt22,0,0,1) - 
      4.390240639181825578641202719*GFOffset(gt22,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(gt22,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(gt22,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(gt22,0,0,5) - 
      1.29435140870084806656280919*GFOffset(gt22,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(gt22,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(gt22,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(gt22,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(gt22,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(gt22,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(gt22,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(gt22,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(gt22,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(gt22,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(gt22,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(gt22,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(gt22,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(gt22,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(gt22,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(gt22,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(gt22,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(gt22,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(gt22,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(gt22,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(gt22,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(gt22,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(gt22,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(gt22,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(gt22,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(gt22,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(gt22,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(gt22,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(gt22,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(gt22,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(gt22,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(gt22,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(gt22,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(gt22,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(gt22,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(gt22,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(gt22,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(gt22,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(gt22,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(gt22,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(gt22,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(gt22,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(gt22,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(gt22,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(gt22,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(gt22,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(gt22,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(gt22,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(gt22,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(gt22,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(gt22,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(gt22,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(gt22,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(gt22,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(gt22,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(gt22,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(gt22,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(gt22,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(gt22,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(gt22,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(gt22,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(gt22,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(gt22,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(gt22,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(gt22,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(gt22,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(gt22,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(gt22,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(gt22,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(gt22,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(gt22,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(gt22,0,0,9),0) + IfThen(tk == 
      8,0.196380615234375*GFOffset(gt22,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(gt22,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(gt22,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(gt22,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(gt22,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(gt22,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(gt22,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(gt22,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(gt22,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(gt22,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(gt22,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(gt22,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(gt22,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(gt22,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(gt22,0,0,7) - 
      0.196380615234375*GFOffset(gt22,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(gt22,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(gt22,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(gt22,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(gt22,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(gt22,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(gt22,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(gt22,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(gt22,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(gt22,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(gt22,0,0,1) - 
      2.630489733142368322167942483*GFOffset(gt22,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(gt22,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(gt22,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(gt22,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(gt22,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(gt22,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(gt22,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(gt22,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(gt22,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(gt22,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(gt22,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(gt22,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(gt22,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(gt22,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(gt22,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(gt22,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(gt22,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(gt22,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(gt22,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(gt22,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(gt22,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(gt22,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(gt22,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(gt22,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(gt22,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(gt22,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(gt22,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(gt22,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(gt22,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(gt22,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(gt22,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(gt22,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(gt22,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(gt22,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(gt22,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(gt22,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(gt22,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(gt22,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(gt22,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(gt22,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(gt22,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(gt22,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(gt22,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(gt22,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(gt22,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(gt22,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(gt22,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(gt22,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(gt22,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(gt22,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(gt22,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(gt22,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(gt22,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(gt22,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(gt22,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(gt22,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(gt22,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(gt22,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(gt22,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(gt22,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(gt22,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(gt22,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(gt22,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(gt22,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(gt22,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(gt22,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(gt22,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(gt22,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(gt22,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(gt22,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(gt22,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(gt22,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(gt22,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(gt22,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(gt22,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(gt22,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(gt22,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(gt22,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(gt22,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(gt22,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(gt22,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(gt22,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(gt22,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(gt22,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(gt22,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(gt22,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(gt22,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(gt22,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(gt22,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(gt22,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(gt22,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(gt22,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(gt22,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(gt22,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(gt22,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(gt22,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(gt22,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(gt22,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(gt22,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(gt22,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(gt22,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(gt22,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(gt22,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(gt22,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(gt22,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(gt22,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(gt22,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(gt22,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(gt22,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(gt22,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(gt22,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(gt22,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(gt22,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(gt22,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(gt22,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(gt22,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(gt22,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(gt22,0,0,-1) + 
      68.*GFOffset(gt22,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(gt22,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(gt22,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt231 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(gt23,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(gt23,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(gt23,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt23,1,0,0) - 
      36.825792804916105238285776948*GFOffset(gt23,2,0,0) + 
      21.042577052349419249515496693*GFOffset(gt23,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(gt23,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(gt23,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(gt23,6,0,0) + 
      6.225793703001792996013745096*GFOffset(gt23,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(gt23,8,0,0) + 
      4.242018040981331373618358215*GFOffset(gt23,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(gt23,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(gt23,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(gt23,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(gt23,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(gt23,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(gt23,15,0,0) - 
      0.5*GFOffset(gt23,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(gt23,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(gt23,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(gt23,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(gt23,3,0,0) - 
      4.376597384264969120661066707*GFOffset(gt23,4,0,0) + 
      3.303076725656509756145262224*GFOffset(gt23,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(gt23,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(gt23,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(gt23,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(gt23,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(gt23,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(gt23,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(gt23,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(gt23,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(gt23,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(gt23,15,0,0),0) + IfThen(ti == 
      2,3.4189873913829435992478256345*GFOffset(gt23,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(gt23,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(gt23,1,0,0) - 
      6.003906719792868453304381935*GFOffset(gt23,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(gt23,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(gt23,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(gt23,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(gt23,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(gt23,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(gt23,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(gt23,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(gt23,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(gt23,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(gt23,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(gt23,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(gt23,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(gt23,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(gt23,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(gt23,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(gt23,1,0,0) - 
      4.390240639181825578641202719*GFOffset(gt23,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(gt23,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(gt23,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(gt23,5,0,0) - 
      1.29435140870084806656280919*GFOffset(gt23,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(gt23,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(gt23,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(gt23,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(gt23,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(gt23,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(gt23,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(gt23,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(gt23,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(gt23,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(gt23,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(gt23,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(gt23,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(gt23,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(gt23,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(gt23,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(gt23,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(gt23,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(gt23,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(gt23,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(gt23,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(gt23,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(gt23,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(gt23,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(gt23,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(gt23,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(gt23,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(gt23,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(gt23,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(gt23,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(gt23,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(gt23,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(gt23,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(gt23,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(gt23,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(gt23,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(gt23,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(gt23,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(gt23,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(gt23,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(gt23,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(gt23,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(gt23,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(gt23,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(gt23,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(gt23,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(gt23,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(gt23,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(gt23,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(gt23,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(gt23,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(gt23,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(gt23,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(gt23,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(gt23,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(gt23,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(gt23,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(gt23,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(gt23,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(gt23,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(gt23,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(gt23,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(gt23,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(gt23,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(gt23,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(gt23,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(gt23,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(gt23,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(gt23,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(gt23,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(gt23,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(gt23,9,0,0),0) + IfThen(ti == 
      8,0.196380615234375*GFOffset(gt23,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(gt23,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(gt23,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(gt23,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(gt23,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(gt23,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(gt23,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(gt23,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(gt23,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(gt23,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(gt23,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(gt23,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(gt23,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(gt23,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(gt23,7,0,0) - 
      0.196380615234375*GFOffset(gt23,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(gt23,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(gt23,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(gt23,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(gt23,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(gt23,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(gt23,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(gt23,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(gt23,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(gt23,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(gt23,1,0,0) - 
      2.630489733142368322167942483*GFOffset(gt23,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(gt23,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(gt23,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(gt23,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(gt23,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(gt23,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(gt23,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(gt23,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(gt23,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(gt23,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(gt23,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(gt23,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(gt23,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(gt23,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(gt23,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(gt23,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(gt23,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(gt23,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(gt23,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(gt23,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(gt23,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(gt23,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(gt23,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(gt23,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(gt23,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(gt23,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(gt23,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(gt23,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(gt23,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(gt23,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(gt23,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(gt23,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(gt23,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(gt23,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(gt23,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(gt23,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(gt23,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(gt23,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(gt23,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(gt23,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(gt23,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(gt23,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(gt23,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(gt23,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(gt23,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(gt23,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(gt23,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(gt23,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(gt23,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(gt23,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(gt23,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(gt23,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(gt23,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(gt23,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(gt23,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(gt23,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(gt23,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(gt23,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(gt23,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(gt23,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(gt23,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(gt23,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(gt23,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(gt23,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(gt23,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(gt23,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(gt23,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(gt23,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(gt23,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(gt23,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(gt23,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(gt23,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(gt23,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(gt23,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(gt23,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(gt23,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(gt23,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(gt23,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(gt23,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(gt23,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(gt23,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(gt23,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(gt23,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(gt23,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(gt23,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(gt23,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(gt23,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(gt23,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(gt23,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(gt23,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(gt23,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(gt23,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(gt23,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(gt23,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(gt23,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(gt23,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(gt23,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(gt23,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(gt23,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(gt23,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(gt23,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(gt23,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(gt23,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(gt23,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(gt23,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(gt23,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(gt23,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(gt23,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(gt23,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(gt23,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(gt23,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(gt23,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(gt23,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(gt23,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(gt23,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(gt23,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(gt23,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(gt23,-1,0,0) + 
      68.*GFOffset(gt23,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(gt23,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(gt23,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt232 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(gt23,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(gt23,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(gt23,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt23,0,1,0) - 
      36.825792804916105238285776948*GFOffset(gt23,0,2,0) + 
      21.042577052349419249515496693*GFOffset(gt23,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(gt23,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(gt23,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(gt23,0,6,0) + 
      6.225793703001792996013745096*GFOffset(gt23,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(gt23,0,8,0) + 
      4.242018040981331373618358215*GFOffset(gt23,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(gt23,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(gt23,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(gt23,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(gt23,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(gt23,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(gt23,0,15,0) - 
      0.5*GFOffset(gt23,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(gt23,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(gt23,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(gt23,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(gt23,0,3,0) - 
      4.376597384264969120661066707*GFOffset(gt23,0,4,0) + 
      3.303076725656509756145262224*GFOffset(gt23,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(gt23,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(gt23,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(gt23,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(gt23,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(gt23,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(gt23,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(gt23,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(gt23,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(gt23,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(gt23,0,15,0),0) + IfThen(tj == 
      2,3.4189873913829435992478256345*GFOffset(gt23,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(gt23,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(gt23,0,1,0) - 
      6.003906719792868453304381935*GFOffset(gt23,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(gt23,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(gt23,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(gt23,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(gt23,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(gt23,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(gt23,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(gt23,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(gt23,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(gt23,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(gt23,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(gt23,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(gt23,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(gt23,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(gt23,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(gt23,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(gt23,0,1,0) - 
      4.390240639181825578641202719*GFOffset(gt23,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(gt23,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(gt23,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(gt23,0,5,0) - 
      1.29435140870084806656280919*GFOffset(gt23,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(gt23,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(gt23,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(gt23,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(gt23,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(gt23,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(gt23,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(gt23,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(gt23,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(gt23,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(gt23,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(gt23,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(gt23,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(gt23,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(gt23,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(gt23,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(gt23,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(gt23,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(gt23,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(gt23,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(gt23,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(gt23,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(gt23,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(gt23,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(gt23,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(gt23,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(gt23,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(gt23,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(gt23,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(gt23,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(gt23,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(gt23,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(gt23,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(gt23,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(gt23,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(gt23,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(gt23,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(gt23,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(gt23,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(gt23,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(gt23,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(gt23,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(gt23,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(gt23,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(gt23,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(gt23,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(gt23,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(gt23,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(gt23,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(gt23,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(gt23,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(gt23,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(gt23,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(gt23,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(gt23,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(gt23,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(gt23,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(gt23,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(gt23,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(gt23,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(gt23,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(gt23,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(gt23,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(gt23,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(gt23,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(gt23,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(gt23,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(gt23,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(gt23,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(gt23,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(gt23,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(gt23,0,9,0),0) + IfThen(tj == 
      8,0.196380615234375*GFOffset(gt23,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(gt23,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(gt23,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(gt23,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(gt23,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(gt23,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(gt23,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(gt23,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(gt23,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(gt23,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(gt23,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(gt23,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(gt23,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(gt23,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(gt23,0,7,0) - 
      0.196380615234375*GFOffset(gt23,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(gt23,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(gt23,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(gt23,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(gt23,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(gt23,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(gt23,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(gt23,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(gt23,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(gt23,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(gt23,0,1,0) - 
      2.630489733142368322167942483*GFOffset(gt23,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(gt23,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(gt23,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(gt23,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(gt23,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(gt23,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(gt23,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(gt23,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(gt23,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(gt23,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(gt23,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(gt23,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(gt23,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(gt23,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(gt23,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(gt23,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(gt23,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(gt23,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(gt23,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(gt23,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(gt23,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(gt23,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(gt23,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(gt23,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(gt23,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(gt23,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(gt23,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(gt23,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(gt23,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(gt23,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(gt23,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(gt23,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(gt23,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(gt23,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(gt23,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(gt23,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(gt23,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(gt23,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(gt23,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(gt23,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(gt23,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(gt23,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(gt23,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(gt23,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(gt23,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(gt23,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(gt23,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(gt23,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(gt23,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(gt23,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(gt23,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(gt23,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(gt23,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(gt23,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(gt23,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(gt23,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(gt23,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(gt23,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(gt23,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(gt23,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(gt23,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(gt23,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(gt23,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(gt23,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(gt23,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(gt23,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(gt23,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(gt23,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(gt23,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(gt23,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(gt23,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(gt23,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(gt23,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(gt23,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(gt23,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(gt23,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(gt23,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(gt23,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(gt23,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(gt23,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(gt23,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(gt23,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(gt23,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(gt23,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(gt23,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(gt23,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(gt23,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(gt23,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(gt23,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(gt23,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(gt23,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(gt23,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(gt23,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(gt23,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(gt23,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(gt23,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(gt23,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(gt23,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(gt23,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(gt23,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(gt23,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(gt23,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(gt23,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(gt23,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(gt23,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(gt23,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(gt23,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(gt23,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(gt23,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(gt23,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(gt23,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(gt23,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(gt23,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(gt23,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(gt23,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(gt23,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(gt23,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(gt23,0,-1,0) + 
      68.*GFOffset(gt23,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(gt23,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(gt23,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt233 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(gt23,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(gt23,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(gt23,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt23,0,0,1) - 
      36.825792804916105238285776948*GFOffset(gt23,0,0,2) + 
      21.042577052349419249515496693*GFOffset(gt23,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(gt23,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(gt23,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(gt23,0,0,6) + 
      6.225793703001792996013745096*GFOffset(gt23,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(gt23,0,0,8) + 
      4.242018040981331373618358215*GFOffset(gt23,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(gt23,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(gt23,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(gt23,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(gt23,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(gt23,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(gt23,0,0,15) - 
      0.5*GFOffset(gt23,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(gt23,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(gt23,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(gt23,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(gt23,0,0,3) - 
      4.376597384264969120661066707*GFOffset(gt23,0,0,4) + 
      3.303076725656509756145262224*GFOffset(gt23,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(gt23,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(gt23,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(gt23,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(gt23,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(gt23,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(gt23,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(gt23,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(gt23,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(gt23,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(gt23,0,0,15),0) + IfThen(tk == 
      2,3.4189873913829435992478256345*GFOffset(gt23,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(gt23,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(gt23,0,0,1) - 
      6.003906719792868453304381935*GFOffset(gt23,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(gt23,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(gt23,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(gt23,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(gt23,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(gt23,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(gt23,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(gt23,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(gt23,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(gt23,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(gt23,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(gt23,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(gt23,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(gt23,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(gt23,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(gt23,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(gt23,0,0,1) - 
      4.390240639181825578641202719*GFOffset(gt23,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(gt23,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(gt23,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(gt23,0,0,5) - 
      1.29435140870084806656280919*GFOffset(gt23,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(gt23,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(gt23,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(gt23,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(gt23,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(gt23,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(gt23,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(gt23,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(gt23,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(gt23,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(gt23,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(gt23,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(gt23,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(gt23,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(gt23,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(gt23,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(gt23,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(gt23,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(gt23,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(gt23,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(gt23,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(gt23,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(gt23,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(gt23,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(gt23,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(gt23,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(gt23,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(gt23,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(gt23,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(gt23,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(gt23,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(gt23,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(gt23,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(gt23,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(gt23,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(gt23,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(gt23,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(gt23,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(gt23,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(gt23,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(gt23,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(gt23,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(gt23,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(gt23,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(gt23,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(gt23,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(gt23,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(gt23,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(gt23,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(gt23,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(gt23,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(gt23,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(gt23,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(gt23,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(gt23,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(gt23,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(gt23,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(gt23,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(gt23,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(gt23,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(gt23,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(gt23,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(gt23,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(gt23,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(gt23,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(gt23,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(gt23,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(gt23,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(gt23,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(gt23,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(gt23,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(gt23,0,0,9),0) + IfThen(tk == 
      8,0.196380615234375*GFOffset(gt23,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(gt23,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(gt23,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(gt23,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(gt23,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(gt23,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(gt23,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(gt23,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(gt23,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(gt23,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(gt23,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(gt23,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(gt23,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(gt23,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(gt23,0,0,7) - 
      0.196380615234375*GFOffset(gt23,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(gt23,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(gt23,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(gt23,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(gt23,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(gt23,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(gt23,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(gt23,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(gt23,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(gt23,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(gt23,0,0,1) - 
      2.630489733142368322167942483*GFOffset(gt23,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(gt23,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(gt23,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(gt23,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(gt23,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(gt23,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(gt23,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(gt23,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(gt23,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(gt23,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(gt23,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(gt23,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(gt23,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(gt23,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(gt23,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(gt23,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(gt23,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(gt23,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(gt23,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(gt23,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(gt23,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(gt23,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(gt23,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(gt23,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(gt23,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(gt23,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(gt23,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(gt23,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(gt23,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(gt23,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(gt23,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(gt23,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(gt23,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(gt23,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(gt23,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(gt23,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(gt23,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(gt23,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(gt23,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(gt23,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(gt23,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(gt23,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(gt23,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(gt23,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(gt23,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(gt23,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(gt23,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(gt23,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(gt23,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(gt23,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(gt23,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(gt23,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(gt23,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(gt23,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(gt23,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(gt23,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(gt23,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(gt23,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(gt23,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(gt23,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(gt23,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(gt23,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(gt23,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(gt23,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(gt23,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(gt23,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(gt23,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(gt23,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(gt23,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(gt23,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(gt23,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(gt23,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(gt23,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(gt23,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(gt23,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(gt23,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(gt23,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(gt23,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(gt23,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(gt23,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(gt23,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(gt23,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(gt23,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(gt23,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(gt23,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(gt23,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(gt23,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(gt23,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(gt23,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(gt23,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(gt23,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(gt23,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(gt23,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(gt23,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(gt23,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(gt23,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(gt23,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(gt23,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(gt23,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(gt23,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(gt23,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(gt23,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(gt23,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(gt23,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(gt23,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(gt23,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(gt23,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(gt23,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(gt23,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(gt23,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(gt23,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(gt23,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(gt23,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(gt23,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(gt23,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(gt23,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(gt23,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(gt23,0,0,-1) + 
      68.*GFOffset(gt23,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(gt23,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(gt23,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDgt331 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(gt33,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(gt33,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(gt33,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt33,1,0,0) - 
      36.825792804916105238285776948*GFOffset(gt33,2,0,0) + 
      21.042577052349419249515496693*GFOffset(gt33,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(gt33,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(gt33,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(gt33,6,0,0) + 
      6.225793703001792996013745096*GFOffset(gt33,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(gt33,8,0,0) + 
      4.242018040981331373618358215*GFOffset(gt33,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(gt33,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(gt33,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(gt33,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(gt33,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(gt33,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(gt33,15,0,0) - 
      0.5*GFOffset(gt33,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(gt33,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(gt33,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(gt33,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(gt33,3,0,0) - 
      4.376597384264969120661066707*GFOffset(gt33,4,0,0) + 
      3.303076725656509756145262224*GFOffset(gt33,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(gt33,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(gt33,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(gt33,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(gt33,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(gt33,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(gt33,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(gt33,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(gt33,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(gt33,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(gt33,15,0,0),0) + IfThen(ti == 
      2,3.4189873913829435992478256345*GFOffset(gt33,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(gt33,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(gt33,1,0,0) - 
      6.003906719792868453304381935*GFOffset(gt33,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(gt33,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(gt33,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(gt33,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(gt33,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(gt33,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(gt33,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(gt33,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(gt33,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(gt33,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(gt33,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(gt33,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(gt33,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(gt33,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(gt33,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(gt33,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(gt33,1,0,0) - 
      4.390240639181825578641202719*GFOffset(gt33,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(gt33,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(gt33,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(gt33,5,0,0) - 
      1.29435140870084806656280919*GFOffset(gt33,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(gt33,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(gt33,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(gt33,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(gt33,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(gt33,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(gt33,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(gt33,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(gt33,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(gt33,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(gt33,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(gt33,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(gt33,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(gt33,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(gt33,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(gt33,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(gt33,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(gt33,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(gt33,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(gt33,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(gt33,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(gt33,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(gt33,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(gt33,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(gt33,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(gt33,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(gt33,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(gt33,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(gt33,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(gt33,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(gt33,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(gt33,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(gt33,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(gt33,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(gt33,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(gt33,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(gt33,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(gt33,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(gt33,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(gt33,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(gt33,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(gt33,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(gt33,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(gt33,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(gt33,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(gt33,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(gt33,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(gt33,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(gt33,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(gt33,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(gt33,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(gt33,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(gt33,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(gt33,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(gt33,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(gt33,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(gt33,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(gt33,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(gt33,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(gt33,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(gt33,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(gt33,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(gt33,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(gt33,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(gt33,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(gt33,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(gt33,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(gt33,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(gt33,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(gt33,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(gt33,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(gt33,9,0,0),0) + IfThen(ti == 
      8,0.196380615234375*GFOffset(gt33,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(gt33,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(gt33,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(gt33,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(gt33,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(gt33,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(gt33,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(gt33,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(gt33,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(gt33,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(gt33,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(gt33,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(gt33,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(gt33,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(gt33,7,0,0) - 
      0.196380615234375*GFOffset(gt33,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(gt33,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(gt33,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(gt33,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(gt33,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(gt33,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(gt33,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(gt33,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(gt33,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(gt33,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(gt33,1,0,0) - 
      2.630489733142368322167942483*GFOffset(gt33,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(gt33,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(gt33,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(gt33,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(gt33,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(gt33,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(gt33,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(gt33,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(gt33,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(gt33,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(gt33,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(gt33,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(gt33,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(gt33,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(gt33,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(gt33,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(gt33,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(gt33,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(gt33,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(gt33,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(gt33,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(gt33,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(gt33,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(gt33,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(gt33,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(gt33,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(gt33,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(gt33,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(gt33,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(gt33,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(gt33,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(gt33,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(gt33,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(gt33,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(gt33,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(gt33,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(gt33,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(gt33,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(gt33,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(gt33,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(gt33,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(gt33,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(gt33,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(gt33,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(gt33,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(gt33,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(gt33,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(gt33,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(gt33,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(gt33,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(gt33,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(gt33,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(gt33,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(gt33,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(gt33,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(gt33,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(gt33,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(gt33,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(gt33,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(gt33,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(gt33,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(gt33,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(gt33,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(gt33,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(gt33,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(gt33,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(gt33,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(gt33,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(gt33,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(gt33,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(gt33,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(gt33,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(gt33,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(gt33,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(gt33,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(gt33,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(gt33,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(gt33,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(gt33,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(gt33,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(gt33,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(gt33,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(gt33,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(gt33,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(gt33,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(gt33,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(gt33,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(gt33,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(gt33,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(gt33,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(gt33,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(gt33,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(gt33,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(gt33,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(gt33,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(gt33,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(gt33,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(gt33,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(gt33,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(gt33,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(gt33,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(gt33,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(gt33,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(gt33,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(gt33,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(gt33,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(gt33,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(gt33,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(gt33,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(gt33,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(gt33,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(gt33,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(gt33,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(gt33,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(gt33,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(gt33,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(gt33,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(gt33,-1,0,0) + 
      68.*GFOffset(gt33,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(gt33,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(gt33,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDgt332 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(gt33,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(gt33,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(gt33,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt33,0,1,0) - 
      36.825792804916105238285776948*GFOffset(gt33,0,2,0) + 
      21.042577052349419249515496693*GFOffset(gt33,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(gt33,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(gt33,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(gt33,0,6,0) + 
      6.225793703001792996013745096*GFOffset(gt33,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(gt33,0,8,0) + 
      4.242018040981331373618358215*GFOffset(gt33,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(gt33,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(gt33,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(gt33,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(gt33,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(gt33,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(gt33,0,15,0) - 
      0.5*GFOffset(gt33,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(gt33,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(gt33,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(gt33,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(gt33,0,3,0) - 
      4.376597384264969120661066707*GFOffset(gt33,0,4,0) + 
      3.303076725656509756145262224*GFOffset(gt33,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(gt33,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(gt33,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(gt33,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(gt33,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(gt33,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(gt33,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(gt33,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(gt33,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(gt33,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(gt33,0,15,0),0) + IfThen(tj == 
      2,3.4189873913829435992478256345*GFOffset(gt33,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(gt33,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(gt33,0,1,0) - 
      6.003906719792868453304381935*GFOffset(gt33,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(gt33,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(gt33,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(gt33,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(gt33,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(gt33,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(gt33,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(gt33,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(gt33,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(gt33,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(gt33,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(gt33,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(gt33,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(gt33,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(gt33,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(gt33,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(gt33,0,1,0) - 
      4.390240639181825578641202719*GFOffset(gt33,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(gt33,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(gt33,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(gt33,0,5,0) - 
      1.29435140870084806656280919*GFOffset(gt33,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(gt33,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(gt33,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(gt33,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(gt33,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(gt33,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(gt33,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(gt33,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(gt33,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(gt33,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(gt33,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(gt33,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(gt33,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(gt33,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(gt33,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(gt33,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(gt33,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(gt33,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(gt33,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(gt33,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(gt33,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(gt33,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(gt33,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(gt33,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(gt33,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(gt33,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(gt33,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(gt33,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(gt33,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(gt33,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(gt33,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(gt33,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(gt33,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(gt33,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(gt33,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(gt33,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(gt33,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(gt33,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(gt33,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(gt33,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(gt33,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(gt33,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(gt33,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(gt33,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(gt33,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(gt33,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(gt33,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(gt33,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(gt33,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(gt33,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(gt33,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(gt33,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(gt33,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(gt33,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(gt33,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(gt33,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(gt33,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(gt33,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(gt33,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(gt33,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(gt33,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(gt33,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(gt33,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(gt33,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(gt33,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(gt33,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(gt33,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(gt33,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(gt33,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(gt33,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(gt33,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(gt33,0,9,0),0) + IfThen(tj == 
      8,0.196380615234375*GFOffset(gt33,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(gt33,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(gt33,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(gt33,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(gt33,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(gt33,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(gt33,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(gt33,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(gt33,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(gt33,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(gt33,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(gt33,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(gt33,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(gt33,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(gt33,0,7,0) - 
      0.196380615234375*GFOffset(gt33,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(gt33,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(gt33,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(gt33,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(gt33,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(gt33,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(gt33,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(gt33,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(gt33,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(gt33,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(gt33,0,1,0) - 
      2.630489733142368322167942483*GFOffset(gt33,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(gt33,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(gt33,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(gt33,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(gt33,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(gt33,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(gt33,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(gt33,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(gt33,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(gt33,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(gt33,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(gt33,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(gt33,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(gt33,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(gt33,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(gt33,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(gt33,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(gt33,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(gt33,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(gt33,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(gt33,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(gt33,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(gt33,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(gt33,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(gt33,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(gt33,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(gt33,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(gt33,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(gt33,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(gt33,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(gt33,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(gt33,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(gt33,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(gt33,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(gt33,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(gt33,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(gt33,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(gt33,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(gt33,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(gt33,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(gt33,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(gt33,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(gt33,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(gt33,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(gt33,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(gt33,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(gt33,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(gt33,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(gt33,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(gt33,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(gt33,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(gt33,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(gt33,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(gt33,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(gt33,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(gt33,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(gt33,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(gt33,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(gt33,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(gt33,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(gt33,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(gt33,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(gt33,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(gt33,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(gt33,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(gt33,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(gt33,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(gt33,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(gt33,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(gt33,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(gt33,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(gt33,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(gt33,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(gt33,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(gt33,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(gt33,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(gt33,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(gt33,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(gt33,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(gt33,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(gt33,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(gt33,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(gt33,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(gt33,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(gt33,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(gt33,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(gt33,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(gt33,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(gt33,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(gt33,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(gt33,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(gt33,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(gt33,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(gt33,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(gt33,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(gt33,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(gt33,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(gt33,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(gt33,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(gt33,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(gt33,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(gt33,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(gt33,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(gt33,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(gt33,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(gt33,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(gt33,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(gt33,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(gt33,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(gt33,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(gt33,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(gt33,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(gt33,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(gt33,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(gt33,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(gt33,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(gt33,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(gt33,0,-1,0) + 
      68.*GFOffset(gt33,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(gt33,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(gt33,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDgt333 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(gt33,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(gt33,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(gt33,0,0,0) + 
      91.995423705517011185543249491*GFOffset(gt33,0,0,1) - 
      36.825792804916105238285776948*GFOffset(gt33,0,0,2) + 
      21.042577052349419249515496693*GFOffset(gt33,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(gt33,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(gt33,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(gt33,0,0,6) + 
      6.225793703001792996013745096*GFOffset(gt33,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(gt33,0,0,8) + 
      4.242018040981331373618358215*GFOffset(gt33,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(gt33,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(gt33,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(gt33,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(gt33,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(gt33,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(gt33,0,0,15) - 
      0.5*GFOffset(gt33,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(gt33,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(gt33,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(gt33,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(gt33,0,0,3) - 
      4.376597384264969120661066707*GFOffset(gt33,0,0,4) + 
      3.303076725656509756145262224*GFOffset(gt33,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(gt33,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(gt33,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(gt33,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(gt33,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(gt33,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(gt33,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(gt33,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(gt33,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(gt33,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(gt33,0,0,15),0) + IfThen(tk == 
      2,3.4189873913829435992478256345*GFOffset(gt33,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(gt33,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(gt33,0,0,1) - 
      6.003906719792868453304381935*GFOffset(gt33,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(gt33,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(gt33,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(gt33,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(gt33,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(gt33,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(gt33,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(gt33,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(gt33,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(gt33,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(gt33,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(gt33,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(gt33,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(gt33,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(gt33,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(gt33,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(gt33,0,0,1) - 
      4.390240639181825578641202719*GFOffset(gt33,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(gt33,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(gt33,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(gt33,0,0,5) - 
      1.29435140870084806656280919*GFOffset(gt33,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(gt33,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(gt33,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(gt33,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(gt33,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(gt33,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(gt33,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(gt33,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(gt33,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(gt33,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(gt33,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(gt33,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(gt33,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(gt33,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(gt33,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(gt33,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(gt33,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(gt33,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(gt33,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(gt33,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(gt33,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(gt33,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(gt33,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(gt33,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(gt33,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(gt33,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(gt33,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(gt33,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(gt33,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(gt33,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(gt33,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(gt33,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(gt33,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(gt33,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(gt33,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(gt33,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(gt33,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(gt33,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(gt33,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(gt33,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(gt33,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(gt33,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(gt33,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(gt33,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(gt33,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(gt33,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(gt33,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(gt33,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(gt33,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(gt33,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(gt33,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(gt33,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(gt33,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(gt33,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(gt33,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(gt33,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(gt33,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(gt33,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(gt33,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(gt33,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(gt33,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(gt33,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(gt33,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(gt33,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(gt33,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(gt33,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(gt33,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(gt33,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(gt33,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(gt33,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(gt33,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(gt33,0,0,9),0) + IfThen(tk == 
      8,0.196380615234375*GFOffset(gt33,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(gt33,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(gt33,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(gt33,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(gt33,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(gt33,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(gt33,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(gt33,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(gt33,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(gt33,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(gt33,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(gt33,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(gt33,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(gt33,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(gt33,0,0,7) - 
      0.196380615234375*GFOffset(gt33,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(gt33,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(gt33,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(gt33,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(gt33,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(gt33,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(gt33,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(gt33,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(gt33,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(gt33,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(gt33,0,0,1) - 
      2.630489733142368322167942483*GFOffset(gt33,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(gt33,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(gt33,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(gt33,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(gt33,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(gt33,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(gt33,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(gt33,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(gt33,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(gt33,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(gt33,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(gt33,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(gt33,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(gt33,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(gt33,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(gt33,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(gt33,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(gt33,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(gt33,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(gt33,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(gt33,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(gt33,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(gt33,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(gt33,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(gt33,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(gt33,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(gt33,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(gt33,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(gt33,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(gt33,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(gt33,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(gt33,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(gt33,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(gt33,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(gt33,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(gt33,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(gt33,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(gt33,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(gt33,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(gt33,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(gt33,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(gt33,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(gt33,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(gt33,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(gt33,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(gt33,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(gt33,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(gt33,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(gt33,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(gt33,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(gt33,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(gt33,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(gt33,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(gt33,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(gt33,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(gt33,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(gt33,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(gt33,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(gt33,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(gt33,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(gt33,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(gt33,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(gt33,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(gt33,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(gt33,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(gt33,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(gt33,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(gt33,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(gt33,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(gt33,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(gt33,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(gt33,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(gt33,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(gt33,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(gt33,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(gt33,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(gt33,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(gt33,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(gt33,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(gt33,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(gt33,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(gt33,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(gt33,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(gt33,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(gt33,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(gt33,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(gt33,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(gt33,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(gt33,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(gt33,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(gt33,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(gt33,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(gt33,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(gt33,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(gt33,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(gt33,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(gt33,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(gt33,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(gt33,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(gt33,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(gt33,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(gt33,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(gt33,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(gt33,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(gt33,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(gt33,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(gt33,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(gt33,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(gt33,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(gt33,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(gt33,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(gt33,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(gt33,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(gt33,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(gt33,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(gt33,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(gt33,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(gt33,0,0,-1) + 
      68.*GFOffset(gt33,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(gt33,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(gt33,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDalpha1 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(alpha,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(alpha,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(alpha,0,0,0) + 
      91.995423705517011185543249491*GFOffset(alpha,1,0,0) - 
      36.825792804916105238285776948*GFOffset(alpha,2,0,0) + 
      21.042577052349419249515496693*GFOffset(alpha,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(alpha,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(alpha,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(alpha,6,0,0) + 
      6.225793703001792996013745096*GFOffset(alpha,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(alpha,8,0,0) + 
      4.242018040981331373618358215*GFOffset(alpha,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(alpha,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(alpha,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(alpha,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(alpha,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(alpha,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(alpha,15,0,0) - 
      0.5*GFOffset(alpha,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(alpha,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(alpha,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(alpha,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(alpha,3,0,0) - 
      4.376597384264969120661066707*GFOffset(alpha,4,0,0) + 
      3.303076725656509756145262224*GFOffset(alpha,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(alpha,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(alpha,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(alpha,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(alpha,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(alpha,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(alpha,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(alpha,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(alpha,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(alpha,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(alpha,15,0,0),0) + IfThen(ti 
      == 2,3.4189873913829435992478256345*GFOffset(alpha,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(alpha,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(alpha,1,0,0) - 
      6.003906719792868453304381935*GFOffset(alpha,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(alpha,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(alpha,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(alpha,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(alpha,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(alpha,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(alpha,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(alpha,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(alpha,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(alpha,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(alpha,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(alpha,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(alpha,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(alpha,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(alpha,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(alpha,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(alpha,1,0,0) - 
      4.390240639181825578641202719*GFOffset(alpha,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(alpha,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(alpha,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(alpha,5,0,0) - 
      1.29435140870084806656280919*GFOffset(alpha,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(alpha,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(alpha,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(alpha,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(alpha,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(alpha,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(alpha,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(alpha,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(alpha,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(alpha,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(alpha,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(alpha,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(alpha,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(alpha,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(alpha,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(alpha,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(alpha,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(alpha,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(alpha,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(alpha,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(alpha,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(alpha,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(alpha,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(alpha,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(alpha,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(alpha,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(alpha,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(alpha,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(alpha,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(alpha,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(alpha,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(alpha,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(alpha,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(alpha,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(alpha,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(alpha,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(alpha,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(alpha,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(alpha,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(alpha,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(alpha,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(alpha,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(alpha,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(alpha,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(alpha,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(alpha,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(alpha,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(alpha,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(alpha,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(alpha,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(alpha,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(alpha,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(alpha,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(alpha,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(alpha,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(alpha,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(alpha,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(alpha,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(alpha,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(alpha,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(alpha,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(alpha,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(alpha,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(alpha,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(alpha,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(alpha,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(alpha,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(alpha,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(alpha,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(alpha,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(alpha,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(alpha,9,0,0),0) + IfThen(ti 
      == 8,0.196380615234375*GFOffset(alpha,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(alpha,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(alpha,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(alpha,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(alpha,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(alpha,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(alpha,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(alpha,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(alpha,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(alpha,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(alpha,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(alpha,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(alpha,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(alpha,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(alpha,7,0,0) - 
      0.196380615234375*GFOffset(alpha,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(alpha,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(alpha,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(alpha,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(alpha,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(alpha,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(alpha,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(alpha,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(alpha,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(alpha,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(alpha,1,0,0) - 
      2.630489733142368322167942483*GFOffset(alpha,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(alpha,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(alpha,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(alpha,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(alpha,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(alpha,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(alpha,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(alpha,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(alpha,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(alpha,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(alpha,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(alpha,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(alpha,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(alpha,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(alpha,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(alpha,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(alpha,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(alpha,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(alpha,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(alpha,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(alpha,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(alpha,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(alpha,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(alpha,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(alpha,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(alpha,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(alpha,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(alpha,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(alpha,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(alpha,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(alpha,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(alpha,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(alpha,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(alpha,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(alpha,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(alpha,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(alpha,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(alpha,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(alpha,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(alpha,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(alpha,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(alpha,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(alpha,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(alpha,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(alpha,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(alpha,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(alpha,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(alpha,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(alpha,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(alpha,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(alpha,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(alpha,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(alpha,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(alpha,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(alpha,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(alpha,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(alpha,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(alpha,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(alpha,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(alpha,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(alpha,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(alpha,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(alpha,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(alpha,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(alpha,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(alpha,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(alpha,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(alpha,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(alpha,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(alpha,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(alpha,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(alpha,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(alpha,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(alpha,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(alpha,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(alpha,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(alpha,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(alpha,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(alpha,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(alpha,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(alpha,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(alpha,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(alpha,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(alpha,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(alpha,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(alpha,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(alpha,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(alpha,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(alpha,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(alpha,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(alpha,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(alpha,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(alpha,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(alpha,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(alpha,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(alpha,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(alpha,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(alpha,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(alpha,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(alpha,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(alpha,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(alpha,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(alpha,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(alpha,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(alpha,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(alpha,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(alpha,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(alpha,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(alpha,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(alpha,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(alpha,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(alpha,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(alpha,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(alpha,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(alpha,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(alpha,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(alpha,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(alpha,-1,0,0) + 
      68.*GFOffset(alpha,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(alpha,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(alpha,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDalpha2 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(alpha,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(alpha,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(alpha,0,0,0) + 
      91.995423705517011185543249491*GFOffset(alpha,0,1,0) - 
      36.825792804916105238285776948*GFOffset(alpha,0,2,0) + 
      21.042577052349419249515496693*GFOffset(alpha,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(alpha,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(alpha,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(alpha,0,6,0) + 
      6.225793703001792996013745096*GFOffset(alpha,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(alpha,0,8,0) + 
      4.242018040981331373618358215*GFOffset(alpha,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(alpha,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(alpha,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(alpha,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(alpha,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(alpha,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(alpha,0,15,0) - 
      0.5*GFOffset(alpha,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(alpha,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(alpha,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(alpha,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(alpha,0,3,0) - 
      4.376597384264969120661066707*GFOffset(alpha,0,4,0) + 
      3.303076725656509756145262224*GFOffset(alpha,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(alpha,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(alpha,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(alpha,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(alpha,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(alpha,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(alpha,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(alpha,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(alpha,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(alpha,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(alpha,0,15,0),0) + IfThen(tj 
      == 2,3.4189873913829435992478256345*GFOffset(alpha,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(alpha,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(alpha,0,1,0) - 
      6.003906719792868453304381935*GFOffset(alpha,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(alpha,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(alpha,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(alpha,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(alpha,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(alpha,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(alpha,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(alpha,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(alpha,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(alpha,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(alpha,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(alpha,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(alpha,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(alpha,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(alpha,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(alpha,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(alpha,0,1,0) - 
      4.390240639181825578641202719*GFOffset(alpha,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(alpha,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(alpha,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(alpha,0,5,0) - 
      1.29435140870084806656280919*GFOffset(alpha,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(alpha,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(alpha,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(alpha,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(alpha,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(alpha,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(alpha,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(alpha,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(alpha,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(alpha,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(alpha,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(alpha,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(alpha,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(alpha,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(alpha,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(alpha,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(alpha,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(alpha,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(alpha,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(alpha,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(alpha,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(alpha,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(alpha,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(alpha,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(alpha,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(alpha,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(alpha,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(alpha,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(alpha,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(alpha,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(alpha,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(alpha,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(alpha,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(alpha,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(alpha,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(alpha,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(alpha,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(alpha,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(alpha,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(alpha,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(alpha,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(alpha,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(alpha,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(alpha,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(alpha,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(alpha,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(alpha,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(alpha,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(alpha,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(alpha,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(alpha,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(alpha,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(alpha,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(alpha,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(alpha,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(alpha,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(alpha,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(alpha,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(alpha,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(alpha,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(alpha,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(alpha,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(alpha,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(alpha,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(alpha,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(alpha,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(alpha,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(alpha,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(alpha,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(alpha,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(alpha,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(alpha,0,9,0),0) + IfThen(tj 
      == 8,0.196380615234375*GFOffset(alpha,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(alpha,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(alpha,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(alpha,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(alpha,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(alpha,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(alpha,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(alpha,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(alpha,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(alpha,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(alpha,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(alpha,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(alpha,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(alpha,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(alpha,0,7,0) - 
      0.196380615234375*GFOffset(alpha,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(alpha,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(alpha,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(alpha,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(alpha,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(alpha,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(alpha,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(alpha,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(alpha,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(alpha,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(alpha,0,1,0) - 
      2.630489733142368322167942483*GFOffset(alpha,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(alpha,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(alpha,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(alpha,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(alpha,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(alpha,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(alpha,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(alpha,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(alpha,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(alpha,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(alpha,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(alpha,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(alpha,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(alpha,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(alpha,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(alpha,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(alpha,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(alpha,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(alpha,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(alpha,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(alpha,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(alpha,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(alpha,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(alpha,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(alpha,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(alpha,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(alpha,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(alpha,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(alpha,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(alpha,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(alpha,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(alpha,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(alpha,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(alpha,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(alpha,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(alpha,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(alpha,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(alpha,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(alpha,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(alpha,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(alpha,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(alpha,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(alpha,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(alpha,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(alpha,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(alpha,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(alpha,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(alpha,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(alpha,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(alpha,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(alpha,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(alpha,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(alpha,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(alpha,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(alpha,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(alpha,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(alpha,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(alpha,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(alpha,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(alpha,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(alpha,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(alpha,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(alpha,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(alpha,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(alpha,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(alpha,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(alpha,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(alpha,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(alpha,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(alpha,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(alpha,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(alpha,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(alpha,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(alpha,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(alpha,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(alpha,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(alpha,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(alpha,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(alpha,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(alpha,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(alpha,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(alpha,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(alpha,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(alpha,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(alpha,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(alpha,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(alpha,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(alpha,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(alpha,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(alpha,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(alpha,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(alpha,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(alpha,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(alpha,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(alpha,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(alpha,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(alpha,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(alpha,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(alpha,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(alpha,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(alpha,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(alpha,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(alpha,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(alpha,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(alpha,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(alpha,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(alpha,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(alpha,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(alpha,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(alpha,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(alpha,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(alpha,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(alpha,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(alpha,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(alpha,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(alpha,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(alpha,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(alpha,0,-1,0) + 
      68.*GFOffset(alpha,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(alpha,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(alpha,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDalpha3 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(alpha,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(alpha,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(alpha,0,0,0) + 
      91.995423705517011185543249491*GFOffset(alpha,0,0,1) - 
      36.825792804916105238285776948*GFOffset(alpha,0,0,2) + 
      21.042577052349419249515496693*GFOffset(alpha,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(alpha,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(alpha,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(alpha,0,0,6) + 
      6.225793703001792996013745096*GFOffset(alpha,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(alpha,0,0,8) + 
      4.242018040981331373618358215*GFOffset(alpha,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(alpha,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(alpha,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(alpha,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(alpha,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(alpha,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(alpha,0,0,15) - 
      0.5*GFOffset(alpha,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(alpha,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(alpha,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(alpha,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(alpha,0,0,3) - 
      4.376597384264969120661066707*GFOffset(alpha,0,0,4) + 
      3.303076725656509756145262224*GFOffset(alpha,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(alpha,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(alpha,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(alpha,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(alpha,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(alpha,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(alpha,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(alpha,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(alpha,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(alpha,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(alpha,0,0,15),0) + IfThen(tk 
      == 2,3.4189873913829435992478256345*GFOffset(alpha,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(alpha,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(alpha,0,0,1) - 
      6.003906719792868453304381935*GFOffset(alpha,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(alpha,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(alpha,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(alpha,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(alpha,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(alpha,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(alpha,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(alpha,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(alpha,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(alpha,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(alpha,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(alpha,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(alpha,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(alpha,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(alpha,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(alpha,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(alpha,0,0,1) - 
      4.390240639181825578641202719*GFOffset(alpha,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(alpha,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(alpha,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(alpha,0,0,5) - 
      1.29435140870084806656280919*GFOffset(alpha,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(alpha,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(alpha,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(alpha,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(alpha,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(alpha,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(alpha,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(alpha,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(alpha,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(alpha,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(alpha,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(alpha,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(alpha,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(alpha,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(alpha,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(alpha,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(alpha,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(alpha,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(alpha,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(alpha,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(alpha,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(alpha,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(alpha,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(alpha,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(alpha,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(alpha,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(alpha,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(alpha,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(alpha,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(alpha,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(alpha,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(alpha,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(alpha,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(alpha,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(alpha,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(alpha,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(alpha,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(alpha,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(alpha,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(alpha,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(alpha,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(alpha,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(alpha,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(alpha,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(alpha,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(alpha,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(alpha,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(alpha,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(alpha,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(alpha,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(alpha,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(alpha,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(alpha,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(alpha,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(alpha,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(alpha,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(alpha,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(alpha,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(alpha,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(alpha,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(alpha,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(alpha,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(alpha,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(alpha,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(alpha,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(alpha,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(alpha,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(alpha,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(alpha,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(alpha,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(alpha,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(alpha,0,0,9),0) + IfThen(tk 
      == 8,0.196380615234375*GFOffset(alpha,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(alpha,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(alpha,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(alpha,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(alpha,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(alpha,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(alpha,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(alpha,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(alpha,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(alpha,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(alpha,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(alpha,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(alpha,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(alpha,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(alpha,0,0,7) - 
      0.196380615234375*GFOffset(alpha,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(alpha,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(alpha,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(alpha,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(alpha,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(alpha,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(alpha,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(alpha,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(alpha,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(alpha,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(alpha,0,0,1) - 
      2.630489733142368322167942483*GFOffset(alpha,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(alpha,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(alpha,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(alpha,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(alpha,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(alpha,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(alpha,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(alpha,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(alpha,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(alpha,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(alpha,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(alpha,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(alpha,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(alpha,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(alpha,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(alpha,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(alpha,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(alpha,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(alpha,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(alpha,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(alpha,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(alpha,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(alpha,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(alpha,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(alpha,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(alpha,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(alpha,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(alpha,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(alpha,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(alpha,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(alpha,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(alpha,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(alpha,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(alpha,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(alpha,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(alpha,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(alpha,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(alpha,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(alpha,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(alpha,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(alpha,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(alpha,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(alpha,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(alpha,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(alpha,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(alpha,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(alpha,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(alpha,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(alpha,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(alpha,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(alpha,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(alpha,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(alpha,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(alpha,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(alpha,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(alpha,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(alpha,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(alpha,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(alpha,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(alpha,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(alpha,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(alpha,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(alpha,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(alpha,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(alpha,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(alpha,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(alpha,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(alpha,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(alpha,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(alpha,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(alpha,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(alpha,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(alpha,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(alpha,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(alpha,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(alpha,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(alpha,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(alpha,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(alpha,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(alpha,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(alpha,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(alpha,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(alpha,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(alpha,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(alpha,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(alpha,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(alpha,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(alpha,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(alpha,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(alpha,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(alpha,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(alpha,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(alpha,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(alpha,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(alpha,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(alpha,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(alpha,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(alpha,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(alpha,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(alpha,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(alpha,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(alpha,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(alpha,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(alpha,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(alpha,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(alpha,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(alpha,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(alpha,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(alpha,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(alpha,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(alpha,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(alpha,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(alpha,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(alpha,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(alpha,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(alpha,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(alpha,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(alpha,0,0,-1) + 
      68.*GFOffset(alpha,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(alpha,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(alpha,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDbeta11 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(beta1,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(beta1,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(beta1,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta1,1,0,0) - 
      36.825792804916105238285776948*GFOffset(beta1,2,0,0) + 
      21.042577052349419249515496693*GFOffset(beta1,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(beta1,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(beta1,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(beta1,6,0,0) + 
      6.225793703001792996013745096*GFOffset(beta1,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(beta1,8,0,0) + 
      4.242018040981331373618358215*GFOffset(beta1,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(beta1,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(beta1,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(beta1,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(beta1,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(beta1,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(beta1,15,0,0) - 
      0.5*GFOffset(beta1,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(beta1,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(beta1,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(beta1,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(beta1,3,0,0) - 
      4.376597384264969120661066707*GFOffset(beta1,4,0,0) + 
      3.303076725656509756145262224*GFOffset(beta1,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(beta1,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(beta1,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(beta1,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(beta1,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(beta1,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(beta1,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(beta1,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(beta1,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(beta1,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(beta1,15,0,0),0) + IfThen(ti 
      == 2,3.4189873913829435992478256345*GFOffset(beta1,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(beta1,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(beta1,1,0,0) - 
      6.003906719792868453304381935*GFOffset(beta1,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(beta1,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(beta1,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(beta1,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(beta1,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(beta1,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(beta1,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(beta1,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(beta1,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(beta1,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(beta1,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(beta1,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(beta1,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(beta1,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(beta1,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(beta1,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(beta1,1,0,0) - 
      4.390240639181825578641202719*GFOffset(beta1,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(beta1,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(beta1,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(beta1,5,0,0) - 
      1.29435140870084806656280919*GFOffset(beta1,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(beta1,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(beta1,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(beta1,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(beta1,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(beta1,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(beta1,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(beta1,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(beta1,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(beta1,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(beta1,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(beta1,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(beta1,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(beta1,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(beta1,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(beta1,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(beta1,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(beta1,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(beta1,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(beta1,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(beta1,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(beta1,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(beta1,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(beta1,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(beta1,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(beta1,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(beta1,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(beta1,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(beta1,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(beta1,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(beta1,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(beta1,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(beta1,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(beta1,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(beta1,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(beta1,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(beta1,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(beta1,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(beta1,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(beta1,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(beta1,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(beta1,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(beta1,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(beta1,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(beta1,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(beta1,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(beta1,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(beta1,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(beta1,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(beta1,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(beta1,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(beta1,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(beta1,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(beta1,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(beta1,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(beta1,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(beta1,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(beta1,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(beta1,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(beta1,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(beta1,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(beta1,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(beta1,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(beta1,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(beta1,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(beta1,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(beta1,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(beta1,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(beta1,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(beta1,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(beta1,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(beta1,9,0,0),0) + IfThen(ti 
      == 8,0.196380615234375*GFOffset(beta1,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(beta1,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(beta1,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(beta1,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(beta1,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(beta1,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(beta1,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(beta1,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(beta1,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(beta1,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(beta1,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(beta1,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(beta1,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(beta1,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(beta1,7,0,0) - 
      0.196380615234375*GFOffset(beta1,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(beta1,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(beta1,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(beta1,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(beta1,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(beta1,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(beta1,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(beta1,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(beta1,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(beta1,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(beta1,1,0,0) - 
      2.630489733142368322167942483*GFOffset(beta1,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(beta1,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(beta1,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(beta1,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(beta1,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(beta1,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(beta1,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(beta1,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(beta1,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(beta1,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(beta1,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(beta1,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(beta1,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(beta1,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(beta1,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(beta1,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(beta1,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(beta1,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(beta1,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(beta1,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(beta1,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(beta1,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(beta1,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(beta1,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(beta1,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(beta1,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(beta1,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(beta1,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(beta1,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(beta1,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(beta1,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(beta1,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(beta1,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(beta1,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(beta1,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(beta1,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(beta1,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(beta1,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(beta1,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(beta1,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(beta1,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(beta1,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(beta1,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(beta1,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(beta1,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(beta1,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(beta1,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(beta1,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(beta1,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(beta1,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(beta1,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(beta1,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(beta1,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(beta1,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(beta1,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(beta1,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(beta1,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(beta1,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(beta1,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(beta1,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(beta1,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(beta1,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(beta1,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(beta1,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(beta1,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(beta1,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(beta1,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(beta1,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(beta1,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(beta1,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(beta1,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(beta1,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(beta1,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(beta1,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(beta1,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(beta1,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(beta1,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(beta1,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(beta1,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(beta1,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(beta1,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(beta1,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(beta1,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(beta1,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(beta1,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(beta1,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(beta1,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(beta1,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(beta1,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(beta1,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(beta1,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(beta1,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(beta1,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(beta1,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(beta1,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(beta1,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(beta1,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(beta1,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(beta1,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(beta1,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(beta1,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(beta1,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(beta1,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(beta1,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(beta1,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(beta1,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(beta1,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(beta1,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(beta1,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(beta1,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(beta1,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(beta1,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(beta1,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(beta1,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(beta1,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(beta1,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(beta1,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(beta1,-1,0,0) + 
      68.*GFOffset(beta1,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(beta1,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(beta1,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDbeta21 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(beta2,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(beta2,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(beta2,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta2,1,0,0) - 
      36.825792804916105238285776948*GFOffset(beta2,2,0,0) + 
      21.042577052349419249515496693*GFOffset(beta2,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(beta2,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(beta2,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(beta2,6,0,0) + 
      6.225793703001792996013745096*GFOffset(beta2,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(beta2,8,0,0) + 
      4.242018040981331373618358215*GFOffset(beta2,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(beta2,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(beta2,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(beta2,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(beta2,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(beta2,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(beta2,15,0,0) - 
      0.5*GFOffset(beta2,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(beta2,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(beta2,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(beta2,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(beta2,3,0,0) - 
      4.376597384264969120661066707*GFOffset(beta2,4,0,0) + 
      3.303076725656509756145262224*GFOffset(beta2,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(beta2,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(beta2,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(beta2,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(beta2,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(beta2,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(beta2,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(beta2,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(beta2,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(beta2,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(beta2,15,0,0),0) + IfThen(ti 
      == 2,3.4189873913829435992478256345*GFOffset(beta2,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(beta2,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(beta2,1,0,0) - 
      6.003906719792868453304381935*GFOffset(beta2,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(beta2,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(beta2,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(beta2,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(beta2,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(beta2,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(beta2,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(beta2,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(beta2,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(beta2,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(beta2,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(beta2,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(beta2,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(beta2,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(beta2,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(beta2,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(beta2,1,0,0) - 
      4.390240639181825578641202719*GFOffset(beta2,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(beta2,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(beta2,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(beta2,5,0,0) - 
      1.29435140870084806656280919*GFOffset(beta2,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(beta2,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(beta2,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(beta2,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(beta2,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(beta2,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(beta2,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(beta2,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(beta2,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(beta2,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(beta2,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(beta2,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(beta2,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(beta2,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(beta2,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(beta2,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(beta2,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(beta2,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(beta2,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(beta2,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(beta2,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(beta2,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(beta2,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(beta2,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(beta2,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(beta2,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(beta2,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(beta2,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(beta2,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(beta2,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(beta2,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(beta2,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(beta2,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(beta2,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(beta2,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(beta2,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(beta2,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(beta2,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(beta2,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(beta2,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(beta2,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(beta2,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(beta2,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(beta2,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(beta2,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(beta2,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(beta2,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(beta2,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(beta2,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(beta2,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(beta2,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(beta2,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(beta2,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(beta2,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(beta2,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(beta2,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(beta2,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(beta2,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(beta2,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(beta2,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(beta2,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(beta2,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(beta2,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(beta2,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(beta2,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(beta2,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(beta2,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(beta2,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(beta2,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(beta2,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(beta2,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(beta2,9,0,0),0) + IfThen(ti 
      == 8,0.196380615234375*GFOffset(beta2,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(beta2,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(beta2,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(beta2,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(beta2,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(beta2,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(beta2,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(beta2,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(beta2,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(beta2,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(beta2,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(beta2,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(beta2,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(beta2,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(beta2,7,0,0) - 
      0.196380615234375*GFOffset(beta2,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(beta2,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(beta2,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(beta2,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(beta2,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(beta2,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(beta2,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(beta2,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(beta2,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(beta2,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(beta2,1,0,0) - 
      2.630489733142368322167942483*GFOffset(beta2,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(beta2,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(beta2,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(beta2,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(beta2,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(beta2,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(beta2,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(beta2,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(beta2,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(beta2,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(beta2,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(beta2,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(beta2,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(beta2,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(beta2,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(beta2,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(beta2,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(beta2,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(beta2,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(beta2,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(beta2,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(beta2,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(beta2,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(beta2,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(beta2,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(beta2,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(beta2,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(beta2,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(beta2,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(beta2,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(beta2,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(beta2,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(beta2,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(beta2,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(beta2,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(beta2,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(beta2,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(beta2,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(beta2,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(beta2,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(beta2,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(beta2,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(beta2,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(beta2,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(beta2,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(beta2,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(beta2,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(beta2,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(beta2,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(beta2,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(beta2,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(beta2,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(beta2,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(beta2,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(beta2,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(beta2,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(beta2,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(beta2,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(beta2,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(beta2,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(beta2,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(beta2,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(beta2,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(beta2,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(beta2,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(beta2,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(beta2,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(beta2,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(beta2,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(beta2,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(beta2,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(beta2,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(beta2,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(beta2,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(beta2,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(beta2,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(beta2,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(beta2,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(beta2,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(beta2,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(beta2,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(beta2,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(beta2,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(beta2,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(beta2,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(beta2,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(beta2,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(beta2,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(beta2,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(beta2,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(beta2,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(beta2,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(beta2,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(beta2,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(beta2,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(beta2,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(beta2,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(beta2,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(beta2,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(beta2,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(beta2,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(beta2,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(beta2,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(beta2,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(beta2,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(beta2,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(beta2,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(beta2,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(beta2,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(beta2,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(beta2,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(beta2,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(beta2,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(beta2,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(beta2,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(beta2,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(beta2,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(beta2,-1,0,0) + 
      68.*GFOffset(beta2,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(beta2,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(beta2,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDbeta31 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(ti == 
      0,-136.*GFOffset(beta3,0,0,0),0) + IfThen(ti == 
      16,136.*GFOffset(beta3,0,0,0),0))*pow(dx,-1) + 
      0.117647058823529411764705882353*(IfThen(ti == 
      0,-68.*GFOffset(beta3,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta3,1,0,0) - 
      36.825792804916105238285776948*GFOffset(beta3,2,0,0) + 
      21.042577052349419249515496693*GFOffset(beta3,3,0,0) - 
      14.0207733572473326733232729469*GFOffset(beta3,4,0,0) + 
      10.1839564276923609087636233103*GFOffset(beta3,5,0,0) - 
      7.8148799060837185155248890235*GFOffset(beta3,6,0,0) + 
      6.225793703001792996013745096*GFOffset(beta3,7,0,0) - 
      5.0921522921522921522921522922*GFOffset(beta3,8,0,0) + 
      4.242018040981331373618358215*GFOffset(beta3,9,0,0) - 
      3.5756251418458313646084276667*GFOffset(beta3,10,0,0) + 
      3.0300735379715023622251472559*GFOffset(beta3,11,0,0) - 
      2.5617613217775424367069428177*GFOffset(beta3,12,0,0) + 
      2.1359441768374612645390986478*GFOffset(beta3,13,0,0) - 
      1.7174887026926442266484643494*GFOffset(beta3,14,0,0) + 
      1.25268688236458726717120733545*GFOffset(beta3,15,0,0) - 
      0.5*GFOffset(beta3,16,0,0),0) + IfThen(ti == 
      1,-15.0580524979732417031816154011*GFOffset(beta3,-1,0,0) + 
      21.329173403460624719650507164*GFOffset(beta3,1,0,0) - 
      9.9662217315544297047431656874*GFOffset(beta3,2,0,0) + 
      6.2127374376796612987841995538*GFOffset(beta3,3,0,0) - 
      4.376597384264969120661066707*GFOffset(beta3,4,0,0) + 
      3.303076725656509756145262224*GFOffset(beta3,5,0,0) - 
      2.6051755661549408690951791049*GFOffset(beta3,6,0,0) + 
      2.1170486703261381185633144345*GFOffset(beta3,7,0,0) - 
      1.7558839529986057597173561381*GFOffset(beta3,8,0,0) + 
      1.47550715887261928380573952732*GFOffset(beta3,9,0,0) - 
      1.24764601361733073935176914511*GFOffset(beta3,10,0,0) + 
      1.05316307826105658459388074163*GFOffset(beta3,11,0,0) - 
      0.87713350073939532514238019381*GFOffset(beta3,12,0,0) + 
      0.70476591212082589044247602143*GFOffset(beta3,13,0,0) - 
      0.51380481707098977744550540087*GFOffset(beta3,14,0,0) + 
      0.20504307799646734735265811118*GFOffset(beta3,15,0,0),0) + IfThen(ti 
      == 2,3.4189873913829435992478256345*GFOffset(beta3,-2,0,0) - 
      12.0980906953316627460912389063*GFOffset(beta3,-1,0,0) + 
      12.4148936988942509765781752778*GFOffset(beta3,1,0,0) - 
      6.003906719792868453304381935*GFOffset(beta3,2,0,0) + 
      3.8514921294753514104486234611*GFOffset(beta3,3,0,0) - 
      2.7751249544430953067946729349*GFOffset(beta3,4,0,0) + 
      2.1313616127677429944468291168*GFOffset(beta3,5,0,0) - 
      1.7033853828073160608298284805*GFOffset(beta3,6,0,0) + 
      1.3972258561707565847560028235*GFOffset(beta3,7,0,0) - 
      1.16516900205061249641024554001*GFOffset(beta3,8,0,0) + 
      0.97992111861336021584554474667*GFOffset(beta3,9,0,0) - 
      0.82399500057024378503865758089*GFOffset(beta3,10,0,0) + 
      0.68441580509143724201083245916*GFOffset(beta3,11,0,0) - 
      0.54891972844065325040182692449*GFOffset(beta3,12,0,0) + 
      0.39974928997635293940119558372*GFOffset(beta3,13,0,0) - 
      0.159455418935743863864176801131*GFOffset(beta3,14,0,0),0) + IfThen(ti 
      == 3,-1.39904838977915305297442065187*GFOffset(beta3,-3,0,0) + 
      4.0481982442230967749977900261*GFOffset(beta3,-2,0,0) - 
      8.8906071670206541962088263737*GFOffset(beta3,-1,0,0) + 
      8.9599207502710419418678125413*GFOffset(beta3,1,0,0) - 
      4.390240639181825578641202719*GFOffset(beta3,2,0,0) + 
      2.8524183528095073388337823645*GFOffset(beta3,3,0,0) - 
      2.0778111620780424940574758157*GFOffset(beta3,4,0,0) + 
      1.60968101634442175130895793491*GFOffset(beta3,5,0,0) - 
      1.29435140870084806656280919*GFOffset(beta3,6,0,0) + 
      1.06502314499059230654638247221*GFOffset(beta3,7,0,0) - 
      0.88741207962958841669287449253*GFOffset(beta3,8,0,0) + 
      0.74134874830795214771393446336*GFOffset(beta3,9,0,0) - 
      0.61297327191474456003014948906*GFOffset(beta3,10,0,0) + 
      0.49012679524675272231094225417*GFOffset(beta3,11,0,0) - 
      0.35628449710286139765470632448*GFOffset(beta3,12,0,0) + 
      0.142011563214352779242862999816*GFOffset(beta3,13,0,0),0) + IfThen(ti 
      == 4,0.74712374527518954001099192507*GFOffset(beta3,-4,0,0) - 
      2.0225580130708640640223348395*GFOffset(beta3,-3,0,0) + 
      3.4459511192916461380881923237*GFOffset(beta3,-2,0,0) - 
      7.1810992462682459840976026061*GFOffset(beta3,-1,0,0) + 
      7.2047116081680621707026720524*GFOffset(beta3,1,0,0) - 
      3.5520492286055812420468337222*GFOffset(beta3,2,0,0) + 
      2.3225546899410544915695827313*GFOffset(beta3,3,0,0) - 
      1.7010434521867990558390611467*GFOffset(beta3,4,0,0) + 
      1.32282396572542287644657467431*GFOffset(beta3,5,0,0) - 
      1.0652590396252522137648987959*GFOffset(beta3,6,0,0) + 
      0.87481845781405758828676845081*GFOffset(beta3,7,0,0) - 
      0.72355865530535824794021258565*GFOffset(beta3,8,0,0) + 
      0.59416808318701907014513742192*GFOffset(beta3,9,0,0) - 
      0.4729331462037957083606828683*GFOffset(beta3,10,0,0) + 
      0.34285746732004547213637755986*GFOffset(beta3,11,0,0) - 
      0.136508355456600831314670575059*GFOffset(beta3,12,0,0),0) + IfThen(ti 
      == 5,-0.46686112632396636747577512626*GFOffset(beta3,-5,0,0) + 
      1.22575929291604642001509669697*GFOffset(beta3,-4,0,0) - 
      1.9017560292469961761829445848*GFOffset(beta3,-3,0,0) + 
      3.0270925321392092857260728001*GFOffset(beta3,-2,0,0) - 
      6.1982232105748690135841729691*GFOffset(beta3,-1,0,0) + 
      6.2082384879303237164867153437*GFOffset(beta3,1,0,0) - 
      3.0703681361027735158780725202*GFOffset(beta3,2,0,0) + 
      2.0138653755273951704351701347*GFOffset(beta3,3,0,0) - 
      1.4781568448432306228464785126*GFOffset(beta3,4,0,0) + 
      1.14989953850113756341678751431*GFOffset(beta3,5,0,0) - 
      0.92355649158379422910974033451*GFOffset(beta3,6,0,0) + 
      0.75260751091203410454863770474*GFOffset(beta3,7,0,0) - 
      0.61187499728431125269744561717*GFOffset(beta3,8,0,0) + 
      0.48385686192828167608039428479*GFOffset(beta3,9,0,0) - 
      0.34942983354132426509071273763*GFOffset(beta3,10,0,0) + 
      0.138907069646837506156467922982*GFOffset(beta3,11,0,0),0) + IfThen(ti 
      == 6,0.32463825647857910692083111049*GFOffset(beta3,-6,0,0) - 
      0.83828842150746799078175057853*GFOffset(beta3,-5,0,0) + 
      1.2416938715208579644505022202*GFOffset(beta3,-4,0,0) - 
      1.7822014842955935859451244093*GFOffset(beta3,-3,0,0) + 
      2.7690818594380164539724232637*GFOffset(beta3,-2,0,0) - 
      5.6256744913992884348000044672*GFOffset(beta3,-1,0,0) + 
      5.6302894370117927868077791211*GFOffset(beta3,1,0,0) - 
      2.7886469957442089235491946144*GFOffset(beta3,2,0,0) + 
      1.8309905783222644495104831588*GFOffset(beta3,3,0,0) - 
      1.34345606496915503717287457821*GFOffset(beta3,4,0,0) + 
      1.04199613368497675695790118199*GFOffset(beta3,5,0,0) - 
      0.83044724112301795463771928881*GFOffset(beta3,6,0,0) + 
      0.66543038048463797319692695175*GFOffset(beta3,7,0,0) - 
      0.52133984338829749335571433254*GFOffset(beta3,8,0,0) + 
      0.3744692206289740714799192084*GFOffset(beta3,9,0,0) - 
      0.148535195143070143054383947497*GFOffset(beta3,10,0,0),0) + IfThen(ti 
      == 7,-0.24451869400844663028521091858*GFOffset(beta3,-7,0,0) + 
      0.62510324733980025532496696346*GFOffset(beta3,-6,0,0) - 
      0.90163152340133989966053775745*GFOffset(beta3,-5,0,0) + 
      1.22740985737237318223498077692*GFOffset(beta3,-4,0,0) - 
      1.7118382276223548370005028254*GFOffset(beta3,-3,0,0) + 
      2.630489733142368322167942483*GFOffset(beta3,-2,0,0) - 
      5.3231741449034573785935861146*GFOffset(beta3,-1,0,0) + 
      5.3250464482630572904207380412*GFOffset(beta3,1,0,0) - 
      2.6383557234797737660003113595*GFOffset(beta3,2,0,0) + 
      1.7311155696570794764334229421*GFOffset(beta3,3,0,0) - 
      1.26638768772191418505470175919*GFOffset(beta3,4,0,0) + 
      0.97498700149069629173161293075*GFOffset(beta3,5,0,0) - 
      0.76460253315530448839544028004*GFOffset(beta3,6,0,0) + 
      0.59106951616673445661478237816*GFOffset(beta3,7,0,0) - 
      0.42131853807890128275765671591*GFOffset(beta3,8,0,0) + 
      0.166605698939383192819501215113*GFOffset(beta3,9,0,0),0) + IfThen(ti 
      == 8,0.196380615234375*GFOffset(beta3,-8,0,0) - 
      0.49879890575153179460488390904*GFOffset(beta3,-7,0,0) + 
      0.70756241379686533842877873719*GFOffset(beta3,-6,0,0) - 
      0.93369115561830415789433778777*GFOffset(beta3,-5,0,0) + 
      1.23109642377273339408357291018*GFOffset(beta3,-4,0,0) - 
      1.6941680481957597967343647471*GFOffset(beta3,-3,0,0) + 
      2.5888887352997334042415596822*GFOffset(beta3,-2,0,0) - 
      5.2288151784208519366049271655*GFOffset(beta3,-1,0,0) + 
      5.2288151784208519366049271655*GFOffset(beta3,1,0,0) - 
      2.5888887352997334042415596822*GFOffset(beta3,2,0,0) + 
      1.6941680481957597967343647471*GFOffset(beta3,3,0,0) - 
      1.23109642377273339408357291018*GFOffset(beta3,4,0,0) + 
      0.93369115561830415789433778777*GFOffset(beta3,5,0,0) - 
      0.70756241379686533842877873719*GFOffset(beta3,6,0,0) + 
      0.49879890575153179460488390904*GFOffset(beta3,7,0,0) - 
      0.196380615234375*GFOffset(beta3,8,0,0),0) + IfThen(ti == 
      9,-0.166605698939383192819501215113*GFOffset(beta3,-9,0,0) + 
      0.42131853807890128275765671591*GFOffset(beta3,-8,0,0) - 
      0.59106951616673445661478237816*GFOffset(beta3,-7,0,0) + 
      0.76460253315530448839544028004*GFOffset(beta3,-6,0,0) - 
      0.97498700149069629173161293075*GFOffset(beta3,-5,0,0) + 
      1.26638768772191418505470175919*GFOffset(beta3,-4,0,0) - 
      1.7311155696570794764334229421*GFOffset(beta3,-3,0,0) + 
      2.6383557234797737660003113595*GFOffset(beta3,-2,0,0) - 
      5.3250464482630572904207380412*GFOffset(beta3,-1,0,0) + 
      5.3231741449034573785935861146*GFOffset(beta3,1,0,0) - 
      2.630489733142368322167942483*GFOffset(beta3,2,0,0) + 
      1.7118382276223548370005028254*GFOffset(beta3,3,0,0) - 
      1.22740985737237318223498077692*GFOffset(beta3,4,0,0) + 
      0.90163152340133989966053775745*GFOffset(beta3,5,0,0) - 
      0.62510324733980025532496696346*GFOffset(beta3,6,0,0) + 
      0.24451869400844663028521091858*GFOffset(beta3,7,0,0),0) + IfThen(ti == 
      10,0.148535195143070143054383947497*GFOffset(beta3,-10,0,0) - 
      0.3744692206289740714799192084*GFOffset(beta3,-9,0,0) + 
      0.52133984338829749335571433254*GFOffset(beta3,-8,0,0) - 
      0.66543038048463797319692695175*GFOffset(beta3,-7,0,0) + 
      0.83044724112301795463771928881*GFOffset(beta3,-6,0,0) - 
      1.04199613368497675695790118199*GFOffset(beta3,-5,0,0) + 
      1.34345606496915503717287457821*GFOffset(beta3,-4,0,0) - 
      1.8309905783222644495104831588*GFOffset(beta3,-3,0,0) + 
      2.7886469957442089235491946144*GFOffset(beta3,-2,0,0) - 
      5.6302894370117927868077791211*GFOffset(beta3,-1,0,0) + 
      5.6256744913992884348000044672*GFOffset(beta3,1,0,0) - 
      2.7690818594380164539724232637*GFOffset(beta3,2,0,0) + 
      1.7822014842955935859451244093*GFOffset(beta3,3,0,0) - 
      1.2416938715208579644505022202*GFOffset(beta3,4,0,0) + 
      0.83828842150746799078175057853*GFOffset(beta3,5,0,0) - 
      0.32463825647857910692083111049*GFOffset(beta3,6,0,0),0) + IfThen(ti == 
      11,-0.138907069646837506156467922982*GFOffset(beta3,-11,0,0) + 
      0.34942983354132426509071273763*GFOffset(beta3,-10,0,0) - 
      0.48385686192828167608039428479*GFOffset(beta3,-9,0,0) + 
      0.61187499728431125269744561717*GFOffset(beta3,-8,0,0) - 
      0.75260751091203410454863770474*GFOffset(beta3,-7,0,0) + 
      0.92355649158379422910974033451*GFOffset(beta3,-6,0,0) - 
      1.14989953850113756341678751431*GFOffset(beta3,-5,0,0) + 
      1.4781568448432306228464785126*GFOffset(beta3,-4,0,0) - 
      2.0138653755273951704351701347*GFOffset(beta3,-3,0,0) + 
      3.0703681361027735158780725202*GFOffset(beta3,-2,0,0) - 
      6.2082384879303237164867153437*GFOffset(beta3,-1,0,0) + 
      6.1982232105748690135841729691*GFOffset(beta3,1,0,0) - 
      3.0270925321392092857260728001*GFOffset(beta3,2,0,0) + 
      1.9017560292469961761829445848*GFOffset(beta3,3,0,0) - 
      1.22575929291604642001509669697*GFOffset(beta3,4,0,0) + 
      0.46686112632396636747577512626*GFOffset(beta3,5,0,0),0) + IfThen(ti == 
      12,0.136508355456600831314670575059*GFOffset(beta3,-12,0,0) - 
      0.34285746732004547213637755986*GFOffset(beta3,-11,0,0) + 
      0.4729331462037957083606828683*GFOffset(beta3,-10,0,0) - 
      0.59416808318701907014513742192*GFOffset(beta3,-9,0,0) + 
      0.72355865530535824794021258565*GFOffset(beta3,-8,0,0) - 
      0.87481845781405758828676845081*GFOffset(beta3,-7,0,0) + 
      1.0652590396252522137648987959*GFOffset(beta3,-6,0,0) - 
      1.32282396572542287644657467431*GFOffset(beta3,-5,0,0) + 
      1.7010434521867990558390611467*GFOffset(beta3,-4,0,0) - 
      2.3225546899410544915695827313*GFOffset(beta3,-3,0,0) + 
      3.5520492286055812420468337222*GFOffset(beta3,-2,0,0) - 
      7.2047116081680621707026720524*GFOffset(beta3,-1,0,0) + 
      7.1810992462682459840976026061*GFOffset(beta3,1,0,0) - 
      3.4459511192916461380881923237*GFOffset(beta3,2,0,0) + 
      2.0225580130708640640223348395*GFOffset(beta3,3,0,0) - 
      0.74712374527518954001099192507*GFOffset(beta3,4,0,0),0) + IfThen(ti == 
      13,-0.142011563214352779242862999816*GFOffset(beta3,-13,0,0) + 
      0.35628449710286139765470632448*GFOffset(beta3,-12,0,0) - 
      0.49012679524675272231094225417*GFOffset(beta3,-11,0,0) + 
      0.61297327191474456003014948906*GFOffset(beta3,-10,0,0) - 
      0.74134874830795214771393446336*GFOffset(beta3,-9,0,0) + 
      0.88741207962958841669287449253*GFOffset(beta3,-8,0,0) - 
      1.06502314499059230654638247221*GFOffset(beta3,-7,0,0) + 
      1.29435140870084806656280919*GFOffset(beta3,-6,0,0) - 
      1.60968101634442175130895793491*GFOffset(beta3,-5,0,0) + 
      2.0778111620780424940574758157*GFOffset(beta3,-4,0,0) - 
      2.8524183528095073388337823645*GFOffset(beta3,-3,0,0) + 
      4.390240639181825578641202719*GFOffset(beta3,-2,0,0) - 
      8.9599207502710419418678125413*GFOffset(beta3,-1,0,0) + 
      8.8906071670206541962088263737*GFOffset(beta3,1,0,0) - 
      4.0481982442230967749977900261*GFOffset(beta3,2,0,0) + 
      1.39904838977915305297442065187*GFOffset(beta3,3,0,0),0) + IfThen(ti == 
      14,0.159455418935743863864176801131*GFOffset(beta3,-14,0,0) - 
      0.39974928997635293940119558372*GFOffset(beta3,-13,0,0) + 
      0.54891972844065325040182692449*GFOffset(beta3,-12,0,0) - 
      0.68441580509143724201083245916*GFOffset(beta3,-11,0,0) + 
      0.82399500057024378503865758089*GFOffset(beta3,-10,0,0) - 
      0.97992111861336021584554474667*GFOffset(beta3,-9,0,0) + 
      1.16516900205061249641024554001*GFOffset(beta3,-8,0,0) - 
      1.3972258561707565847560028235*GFOffset(beta3,-7,0,0) + 
      1.7033853828073160608298284805*GFOffset(beta3,-6,0,0) - 
      2.1313616127677429944468291168*GFOffset(beta3,-5,0,0) + 
      2.7751249544430953067946729349*GFOffset(beta3,-4,0,0) - 
      3.8514921294753514104486234611*GFOffset(beta3,-3,0,0) + 
      6.003906719792868453304381935*GFOffset(beta3,-2,0,0) - 
      12.4148936988942509765781752778*GFOffset(beta3,-1,0,0) + 
      12.0980906953316627460912389063*GFOffset(beta3,1,0,0) - 
      3.4189873913829435992478256345*GFOffset(beta3,2,0,0),0) + IfThen(ti == 
      15,-0.20504307799646734735265811118*GFOffset(beta3,-15,0,0) + 
      0.51380481707098977744550540087*GFOffset(beta3,-14,0,0) - 
      0.70476591212082589044247602143*GFOffset(beta3,-13,0,0) + 
      0.87713350073939532514238019381*GFOffset(beta3,-12,0,0) - 
      1.05316307826105658459388074163*GFOffset(beta3,-11,0,0) + 
      1.24764601361733073935176914511*GFOffset(beta3,-10,0,0) - 
      1.47550715887261928380573952732*GFOffset(beta3,-9,0,0) + 
      1.7558839529986057597173561381*GFOffset(beta3,-8,0,0) - 
      2.1170486703261381185633144345*GFOffset(beta3,-7,0,0) + 
      2.6051755661549408690951791049*GFOffset(beta3,-6,0,0) - 
      3.303076725656509756145262224*GFOffset(beta3,-5,0,0) + 
      4.376597384264969120661066707*GFOffset(beta3,-4,0,0) - 
      6.2127374376796612987841995538*GFOffset(beta3,-3,0,0) + 
      9.9662217315544297047431656874*GFOffset(beta3,-2,0,0) - 
      21.329173403460624719650507164*GFOffset(beta3,-1,0,0) + 
      15.0580524979732417031816154011*GFOffset(beta3,1,0,0),0) + IfThen(ti == 
      16,0.5*GFOffset(beta3,-16,0,0) - 
      1.25268688236458726717120733545*GFOffset(beta3,-15,0,0) + 
      1.7174887026926442266484643494*GFOffset(beta3,-14,0,0) - 
      2.1359441768374612645390986478*GFOffset(beta3,-13,0,0) + 
      2.5617613217775424367069428177*GFOffset(beta3,-12,0,0) - 
      3.0300735379715023622251472559*GFOffset(beta3,-11,0,0) + 
      3.5756251418458313646084276667*GFOffset(beta3,-10,0,0) - 
      4.242018040981331373618358215*GFOffset(beta3,-9,0,0) + 
      5.0921522921522921522921522922*GFOffset(beta3,-8,0,0) - 
      6.225793703001792996013745096*GFOffset(beta3,-7,0,0) + 
      7.8148799060837185155248890235*GFOffset(beta3,-6,0,0) - 
      10.1839564276923609087636233103*GFOffset(beta3,-5,0,0) + 
      14.0207733572473326733232729469*GFOffset(beta3,-4,0,0) - 
      21.042577052349419249515496693*GFOffset(beta3,-3,0,0) + 
      36.825792804916105238285776948*GFOffset(beta3,-2,0,0) - 
      91.995423705517011185543249491*GFOffset(beta3,-1,0,0) + 
      68.*GFOffset(beta3,0,0,0),0))*pow(dx,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(ti == 
      0,136.*GFOffset(beta3,-1,0,0),0) + IfThen(ti == 
      16,-136.*GFOffset(beta3,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDbeta12 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(beta1,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(beta1,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(beta1,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta1,0,1,0) - 
      36.825792804916105238285776948*GFOffset(beta1,0,2,0) + 
      21.042577052349419249515496693*GFOffset(beta1,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(beta1,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(beta1,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(beta1,0,6,0) + 
      6.225793703001792996013745096*GFOffset(beta1,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(beta1,0,8,0) + 
      4.242018040981331373618358215*GFOffset(beta1,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(beta1,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(beta1,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(beta1,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(beta1,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(beta1,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(beta1,0,15,0) - 
      0.5*GFOffset(beta1,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(beta1,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(beta1,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(beta1,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(beta1,0,3,0) - 
      4.376597384264969120661066707*GFOffset(beta1,0,4,0) + 
      3.303076725656509756145262224*GFOffset(beta1,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(beta1,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(beta1,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(beta1,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(beta1,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(beta1,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(beta1,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(beta1,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(beta1,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(beta1,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(beta1,0,15,0),0) + IfThen(tj 
      == 2,3.4189873913829435992478256345*GFOffset(beta1,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(beta1,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(beta1,0,1,0) - 
      6.003906719792868453304381935*GFOffset(beta1,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(beta1,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(beta1,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(beta1,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(beta1,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(beta1,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(beta1,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(beta1,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(beta1,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(beta1,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(beta1,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(beta1,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(beta1,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(beta1,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(beta1,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(beta1,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(beta1,0,1,0) - 
      4.390240639181825578641202719*GFOffset(beta1,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(beta1,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(beta1,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(beta1,0,5,0) - 
      1.29435140870084806656280919*GFOffset(beta1,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(beta1,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(beta1,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(beta1,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(beta1,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(beta1,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(beta1,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(beta1,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(beta1,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(beta1,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(beta1,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(beta1,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(beta1,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(beta1,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(beta1,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(beta1,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(beta1,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(beta1,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(beta1,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(beta1,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(beta1,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(beta1,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(beta1,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(beta1,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(beta1,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(beta1,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(beta1,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(beta1,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(beta1,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(beta1,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(beta1,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(beta1,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(beta1,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(beta1,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(beta1,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(beta1,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(beta1,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(beta1,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(beta1,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(beta1,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(beta1,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(beta1,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(beta1,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(beta1,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(beta1,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(beta1,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(beta1,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(beta1,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(beta1,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(beta1,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(beta1,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(beta1,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(beta1,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(beta1,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(beta1,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(beta1,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(beta1,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(beta1,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(beta1,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(beta1,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(beta1,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(beta1,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(beta1,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(beta1,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(beta1,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(beta1,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(beta1,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(beta1,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(beta1,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(beta1,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(beta1,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(beta1,0,9,0),0) + IfThen(tj 
      == 8,0.196380615234375*GFOffset(beta1,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(beta1,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(beta1,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(beta1,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(beta1,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(beta1,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(beta1,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(beta1,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(beta1,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(beta1,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(beta1,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(beta1,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(beta1,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(beta1,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(beta1,0,7,0) - 
      0.196380615234375*GFOffset(beta1,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(beta1,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(beta1,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(beta1,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(beta1,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(beta1,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(beta1,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(beta1,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(beta1,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(beta1,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(beta1,0,1,0) - 
      2.630489733142368322167942483*GFOffset(beta1,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(beta1,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(beta1,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(beta1,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(beta1,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(beta1,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(beta1,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(beta1,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(beta1,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(beta1,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(beta1,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(beta1,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(beta1,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(beta1,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(beta1,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(beta1,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(beta1,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(beta1,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(beta1,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(beta1,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(beta1,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(beta1,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(beta1,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(beta1,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(beta1,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(beta1,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(beta1,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(beta1,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(beta1,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(beta1,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(beta1,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(beta1,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(beta1,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(beta1,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(beta1,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(beta1,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(beta1,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(beta1,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(beta1,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(beta1,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(beta1,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(beta1,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(beta1,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(beta1,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(beta1,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(beta1,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(beta1,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(beta1,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(beta1,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(beta1,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(beta1,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(beta1,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(beta1,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(beta1,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(beta1,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(beta1,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(beta1,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(beta1,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(beta1,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(beta1,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(beta1,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(beta1,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(beta1,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(beta1,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(beta1,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(beta1,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(beta1,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(beta1,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(beta1,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(beta1,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(beta1,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(beta1,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(beta1,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(beta1,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(beta1,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(beta1,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(beta1,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(beta1,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(beta1,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(beta1,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(beta1,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(beta1,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(beta1,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(beta1,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(beta1,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(beta1,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(beta1,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(beta1,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(beta1,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(beta1,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(beta1,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(beta1,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(beta1,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(beta1,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(beta1,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(beta1,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(beta1,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(beta1,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(beta1,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(beta1,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(beta1,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(beta1,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(beta1,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(beta1,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(beta1,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(beta1,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(beta1,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(beta1,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(beta1,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(beta1,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(beta1,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(beta1,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(beta1,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(beta1,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(beta1,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(beta1,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(beta1,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(beta1,0,-1,0) + 
      68.*GFOffset(beta1,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(beta1,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(beta1,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDbeta22 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(beta2,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(beta2,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(beta2,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta2,0,1,0) - 
      36.825792804916105238285776948*GFOffset(beta2,0,2,0) + 
      21.042577052349419249515496693*GFOffset(beta2,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(beta2,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(beta2,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(beta2,0,6,0) + 
      6.225793703001792996013745096*GFOffset(beta2,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(beta2,0,8,0) + 
      4.242018040981331373618358215*GFOffset(beta2,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(beta2,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(beta2,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(beta2,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(beta2,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(beta2,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(beta2,0,15,0) - 
      0.5*GFOffset(beta2,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(beta2,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(beta2,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(beta2,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(beta2,0,3,0) - 
      4.376597384264969120661066707*GFOffset(beta2,0,4,0) + 
      3.303076725656509756145262224*GFOffset(beta2,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(beta2,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(beta2,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(beta2,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(beta2,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(beta2,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(beta2,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(beta2,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(beta2,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(beta2,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(beta2,0,15,0),0) + IfThen(tj 
      == 2,3.4189873913829435992478256345*GFOffset(beta2,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(beta2,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(beta2,0,1,0) - 
      6.003906719792868453304381935*GFOffset(beta2,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(beta2,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(beta2,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(beta2,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(beta2,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(beta2,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(beta2,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(beta2,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(beta2,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(beta2,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(beta2,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(beta2,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(beta2,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(beta2,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(beta2,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(beta2,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(beta2,0,1,0) - 
      4.390240639181825578641202719*GFOffset(beta2,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(beta2,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(beta2,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(beta2,0,5,0) - 
      1.29435140870084806656280919*GFOffset(beta2,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(beta2,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(beta2,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(beta2,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(beta2,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(beta2,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(beta2,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(beta2,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(beta2,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(beta2,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(beta2,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(beta2,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(beta2,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(beta2,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(beta2,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(beta2,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(beta2,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(beta2,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(beta2,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(beta2,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(beta2,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(beta2,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(beta2,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(beta2,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(beta2,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(beta2,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(beta2,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(beta2,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(beta2,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(beta2,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(beta2,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(beta2,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(beta2,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(beta2,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(beta2,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(beta2,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(beta2,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(beta2,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(beta2,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(beta2,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(beta2,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(beta2,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(beta2,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(beta2,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(beta2,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(beta2,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(beta2,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(beta2,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(beta2,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(beta2,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(beta2,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(beta2,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(beta2,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(beta2,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(beta2,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(beta2,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(beta2,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(beta2,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(beta2,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(beta2,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(beta2,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(beta2,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(beta2,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(beta2,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(beta2,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(beta2,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(beta2,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(beta2,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(beta2,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(beta2,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(beta2,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(beta2,0,9,0),0) + IfThen(tj 
      == 8,0.196380615234375*GFOffset(beta2,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(beta2,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(beta2,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(beta2,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(beta2,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(beta2,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(beta2,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(beta2,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(beta2,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(beta2,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(beta2,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(beta2,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(beta2,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(beta2,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(beta2,0,7,0) - 
      0.196380615234375*GFOffset(beta2,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(beta2,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(beta2,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(beta2,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(beta2,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(beta2,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(beta2,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(beta2,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(beta2,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(beta2,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(beta2,0,1,0) - 
      2.630489733142368322167942483*GFOffset(beta2,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(beta2,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(beta2,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(beta2,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(beta2,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(beta2,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(beta2,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(beta2,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(beta2,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(beta2,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(beta2,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(beta2,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(beta2,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(beta2,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(beta2,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(beta2,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(beta2,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(beta2,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(beta2,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(beta2,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(beta2,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(beta2,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(beta2,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(beta2,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(beta2,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(beta2,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(beta2,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(beta2,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(beta2,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(beta2,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(beta2,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(beta2,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(beta2,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(beta2,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(beta2,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(beta2,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(beta2,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(beta2,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(beta2,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(beta2,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(beta2,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(beta2,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(beta2,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(beta2,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(beta2,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(beta2,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(beta2,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(beta2,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(beta2,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(beta2,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(beta2,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(beta2,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(beta2,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(beta2,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(beta2,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(beta2,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(beta2,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(beta2,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(beta2,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(beta2,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(beta2,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(beta2,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(beta2,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(beta2,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(beta2,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(beta2,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(beta2,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(beta2,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(beta2,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(beta2,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(beta2,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(beta2,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(beta2,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(beta2,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(beta2,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(beta2,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(beta2,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(beta2,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(beta2,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(beta2,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(beta2,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(beta2,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(beta2,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(beta2,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(beta2,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(beta2,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(beta2,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(beta2,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(beta2,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(beta2,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(beta2,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(beta2,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(beta2,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(beta2,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(beta2,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(beta2,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(beta2,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(beta2,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(beta2,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(beta2,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(beta2,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(beta2,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(beta2,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(beta2,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(beta2,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(beta2,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(beta2,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(beta2,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(beta2,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(beta2,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(beta2,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(beta2,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(beta2,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(beta2,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(beta2,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(beta2,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(beta2,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(beta2,0,-1,0) + 
      68.*GFOffset(beta2,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(beta2,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(beta2,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDbeta32 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tj == 
      0,-136.*GFOffset(beta3,0,0,0),0) + IfThen(tj == 
      16,136.*GFOffset(beta3,0,0,0),0))*pow(dy,-1) + 
      0.117647058823529411764705882353*(IfThen(tj == 
      0,-68.*GFOffset(beta3,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta3,0,1,0) - 
      36.825792804916105238285776948*GFOffset(beta3,0,2,0) + 
      21.042577052349419249515496693*GFOffset(beta3,0,3,0) - 
      14.0207733572473326733232729469*GFOffset(beta3,0,4,0) + 
      10.1839564276923609087636233103*GFOffset(beta3,0,5,0) - 
      7.8148799060837185155248890235*GFOffset(beta3,0,6,0) + 
      6.225793703001792996013745096*GFOffset(beta3,0,7,0) - 
      5.0921522921522921522921522922*GFOffset(beta3,0,8,0) + 
      4.242018040981331373618358215*GFOffset(beta3,0,9,0) - 
      3.5756251418458313646084276667*GFOffset(beta3,0,10,0) + 
      3.0300735379715023622251472559*GFOffset(beta3,0,11,0) - 
      2.5617613217775424367069428177*GFOffset(beta3,0,12,0) + 
      2.1359441768374612645390986478*GFOffset(beta3,0,13,0) - 
      1.7174887026926442266484643494*GFOffset(beta3,0,14,0) + 
      1.25268688236458726717120733545*GFOffset(beta3,0,15,0) - 
      0.5*GFOffset(beta3,0,16,0),0) + IfThen(tj == 
      1,-15.0580524979732417031816154011*GFOffset(beta3,0,-1,0) + 
      21.329173403460624719650507164*GFOffset(beta3,0,1,0) - 
      9.9662217315544297047431656874*GFOffset(beta3,0,2,0) + 
      6.2127374376796612987841995538*GFOffset(beta3,0,3,0) - 
      4.376597384264969120661066707*GFOffset(beta3,0,4,0) + 
      3.303076725656509756145262224*GFOffset(beta3,0,5,0) - 
      2.6051755661549408690951791049*GFOffset(beta3,0,6,0) + 
      2.1170486703261381185633144345*GFOffset(beta3,0,7,0) - 
      1.7558839529986057597173561381*GFOffset(beta3,0,8,0) + 
      1.47550715887261928380573952732*GFOffset(beta3,0,9,0) - 
      1.24764601361733073935176914511*GFOffset(beta3,0,10,0) + 
      1.05316307826105658459388074163*GFOffset(beta3,0,11,0) - 
      0.87713350073939532514238019381*GFOffset(beta3,0,12,0) + 
      0.70476591212082589044247602143*GFOffset(beta3,0,13,0) - 
      0.51380481707098977744550540087*GFOffset(beta3,0,14,0) + 
      0.20504307799646734735265811118*GFOffset(beta3,0,15,0),0) + IfThen(tj 
      == 2,3.4189873913829435992478256345*GFOffset(beta3,0,-2,0) - 
      12.0980906953316627460912389063*GFOffset(beta3,0,-1,0) + 
      12.4148936988942509765781752778*GFOffset(beta3,0,1,0) - 
      6.003906719792868453304381935*GFOffset(beta3,0,2,0) + 
      3.8514921294753514104486234611*GFOffset(beta3,0,3,0) - 
      2.7751249544430953067946729349*GFOffset(beta3,0,4,0) + 
      2.1313616127677429944468291168*GFOffset(beta3,0,5,0) - 
      1.7033853828073160608298284805*GFOffset(beta3,0,6,0) + 
      1.3972258561707565847560028235*GFOffset(beta3,0,7,0) - 
      1.16516900205061249641024554001*GFOffset(beta3,0,8,0) + 
      0.97992111861336021584554474667*GFOffset(beta3,0,9,0) - 
      0.82399500057024378503865758089*GFOffset(beta3,0,10,0) + 
      0.68441580509143724201083245916*GFOffset(beta3,0,11,0) - 
      0.54891972844065325040182692449*GFOffset(beta3,0,12,0) + 
      0.39974928997635293940119558372*GFOffset(beta3,0,13,0) - 
      0.159455418935743863864176801131*GFOffset(beta3,0,14,0),0) + IfThen(tj 
      == 3,-1.39904838977915305297442065187*GFOffset(beta3,0,-3,0) + 
      4.0481982442230967749977900261*GFOffset(beta3,0,-2,0) - 
      8.8906071670206541962088263737*GFOffset(beta3,0,-1,0) + 
      8.9599207502710419418678125413*GFOffset(beta3,0,1,0) - 
      4.390240639181825578641202719*GFOffset(beta3,0,2,0) + 
      2.8524183528095073388337823645*GFOffset(beta3,0,3,0) - 
      2.0778111620780424940574758157*GFOffset(beta3,0,4,0) + 
      1.60968101634442175130895793491*GFOffset(beta3,0,5,0) - 
      1.29435140870084806656280919*GFOffset(beta3,0,6,0) + 
      1.06502314499059230654638247221*GFOffset(beta3,0,7,0) - 
      0.88741207962958841669287449253*GFOffset(beta3,0,8,0) + 
      0.74134874830795214771393446336*GFOffset(beta3,0,9,0) - 
      0.61297327191474456003014948906*GFOffset(beta3,0,10,0) + 
      0.49012679524675272231094225417*GFOffset(beta3,0,11,0) - 
      0.35628449710286139765470632448*GFOffset(beta3,0,12,0) + 
      0.142011563214352779242862999816*GFOffset(beta3,0,13,0),0) + IfThen(tj 
      == 4,0.74712374527518954001099192507*GFOffset(beta3,0,-4,0) - 
      2.0225580130708640640223348395*GFOffset(beta3,0,-3,0) + 
      3.4459511192916461380881923237*GFOffset(beta3,0,-2,0) - 
      7.1810992462682459840976026061*GFOffset(beta3,0,-1,0) + 
      7.2047116081680621707026720524*GFOffset(beta3,0,1,0) - 
      3.5520492286055812420468337222*GFOffset(beta3,0,2,0) + 
      2.3225546899410544915695827313*GFOffset(beta3,0,3,0) - 
      1.7010434521867990558390611467*GFOffset(beta3,0,4,0) + 
      1.32282396572542287644657467431*GFOffset(beta3,0,5,0) - 
      1.0652590396252522137648987959*GFOffset(beta3,0,6,0) + 
      0.87481845781405758828676845081*GFOffset(beta3,0,7,0) - 
      0.72355865530535824794021258565*GFOffset(beta3,0,8,0) + 
      0.59416808318701907014513742192*GFOffset(beta3,0,9,0) - 
      0.4729331462037957083606828683*GFOffset(beta3,0,10,0) + 
      0.34285746732004547213637755986*GFOffset(beta3,0,11,0) - 
      0.136508355456600831314670575059*GFOffset(beta3,0,12,0),0) + IfThen(tj 
      == 5,-0.46686112632396636747577512626*GFOffset(beta3,0,-5,0) + 
      1.22575929291604642001509669697*GFOffset(beta3,0,-4,0) - 
      1.9017560292469961761829445848*GFOffset(beta3,0,-3,0) + 
      3.0270925321392092857260728001*GFOffset(beta3,0,-2,0) - 
      6.1982232105748690135841729691*GFOffset(beta3,0,-1,0) + 
      6.2082384879303237164867153437*GFOffset(beta3,0,1,0) - 
      3.0703681361027735158780725202*GFOffset(beta3,0,2,0) + 
      2.0138653755273951704351701347*GFOffset(beta3,0,3,0) - 
      1.4781568448432306228464785126*GFOffset(beta3,0,4,0) + 
      1.14989953850113756341678751431*GFOffset(beta3,0,5,0) - 
      0.92355649158379422910974033451*GFOffset(beta3,0,6,0) + 
      0.75260751091203410454863770474*GFOffset(beta3,0,7,0) - 
      0.61187499728431125269744561717*GFOffset(beta3,0,8,0) + 
      0.48385686192828167608039428479*GFOffset(beta3,0,9,0) - 
      0.34942983354132426509071273763*GFOffset(beta3,0,10,0) + 
      0.138907069646837506156467922982*GFOffset(beta3,0,11,0),0) + IfThen(tj 
      == 6,0.32463825647857910692083111049*GFOffset(beta3,0,-6,0) - 
      0.83828842150746799078175057853*GFOffset(beta3,0,-5,0) + 
      1.2416938715208579644505022202*GFOffset(beta3,0,-4,0) - 
      1.7822014842955935859451244093*GFOffset(beta3,0,-3,0) + 
      2.7690818594380164539724232637*GFOffset(beta3,0,-2,0) - 
      5.6256744913992884348000044672*GFOffset(beta3,0,-1,0) + 
      5.6302894370117927868077791211*GFOffset(beta3,0,1,0) - 
      2.7886469957442089235491946144*GFOffset(beta3,0,2,0) + 
      1.8309905783222644495104831588*GFOffset(beta3,0,3,0) - 
      1.34345606496915503717287457821*GFOffset(beta3,0,4,0) + 
      1.04199613368497675695790118199*GFOffset(beta3,0,5,0) - 
      0.83044724112301795463771928881*GFOffset(beta3,0,6,0) + 
      0.66543038048463797319692695175*GFOffset(beta3,0,7,0) - 
      0.52133984338829749335571433254*GFOffset(beta3,0,8,0) + 
      0.3744692206289740714799192084*GFOffset(beta3,0,9,0) - 
      0.148535195143070143054383947497*GFOffset(beta3,0,10,0),0) + IfThen(tj 
      == 7,-0.24451869400844663028521091858*GFOffset(beta3,0,-7,0) + 
      0.62510324733980025532496696346*GFOffset(beta3,0,-6,0) - 
      0.90163152340133989966053775745*GFOffset(beta3,0,-5,0) + 
      1.22740985737237318223498077692*GFOffset(beta3,0,-4,0) - 
      1.7118382276223548370005028254*GFOffset(beta3,0,-3,0) + 
      2.630489733142368322167942483*GFOffset(beta3,0,-2,0) - 
      5.3231741449034573785935861146*GFOffset(beta3,0,-1,0) + 
      5.3250464482630572904207380412*GFOffset(beta3,0,1,0) - 
      2.6383557234797737660003113595*GFOffset(beta3,0,2,0) + 
      1.7311155696570794764334229421*GFOffset(beta3,0,3,0) - 
      1.26638768772191418505470175919*GFOffset(beta3,0,4,0) + 
      0.97498700149069629173161293075*GFOffset(beta3,0,5,0) - 
      0.76460253315530448839544028004*GFOffset(beta3,0,6,0) + 
      0.59106951616673445661478237816*GFOffset(beta3,0,7,0) - 
      0.42131853807890128275765671591*GFOffset(beta3,0,8,0) + 
      0.166605698939383192819501215113*GFOffset(beta3,0,9,0),0) + IfThen(tj 
      == 8,0.196380615234375*GFOffset(beta3,0,-8,0) - 
      0.49879890575153179460488390904*GFOffset(beta3,0,-7,0) + 
      0.70756241379686533842877873719*GFOffset(beta3,0,-6,0) - 
      0.93369115561830415789433778777*GFOffset(beta3,0,-5,0) + 
      1.23109642377273339408357291018*GFOffset(beta3,0,-4,0) - 
      1.6941680481957597967343647471*GFOffset(beta3,0,-3,0) + 
      2.5888887352997334042415596822*GFOffset(beta3,0,-2,0) - 
      5.2288151784208519366049271655*GFOffset(beta3,0,-1,0) + 
      5.2288151784208519366049271655*GFOffset(beta3,0,1,0) - 
      2.5888887352997334042415596822*GFOffset(beta3,0,2,0) + 
      1.6941680481957597967343647471*GFOffset(beta3,0,3,0) - 
      1.23109642377273339408357291018*GFOffset(beta3,0,4,0) + 
      0.93369115561830415789433778777*GFOffset(beta3,0,5,0) - 
      0.70756241379686533842877873719*GFOffset(beta3,0,6,0) + 
      0.49879890575153179460488390904*GFOffset(beta3,0,7,0) - 
      0.196380615234375*GFOffset(beta3,0,8,0),0) + IfThen(tj == 
      9,-0.166605698939383192819501215113*GFOffset(beta3,0,-9,0) + 
      0.42131853807890128275765671591*GFOffset(beta3,0,-8,0) - 
      0.59106951616673445661478237816*GFOffset(beta3,0,-7,0) + 
      0.76460253315530448839544028004*GFOffset(beta3,0,-6,0) - 
      0.97498700149069629173161293075*GFOffset(beta3,0,-5,0) + 
      1.26638768772191418505470175919*GFOffset(beta3,0,-4,0) - 
      1.7311155696570794764334229421*GFOffset(beta3,0,-3,0) + 
      2.6383557234797737660003113595*GFOffset(beta3,0,-2,0) - 
      5.3250464482630572904207380412*GFOffset(beta3,0,-1,0) + 
      5.3231741449034573785935861146*GFOffset(beta3,0,1,0) - 
      2.630489733142368322167942483*GFOffset(beta3,0,2,0) + 
      1.7118382276223548370005028254*GFOffset(beta3,0,3,0) - 
      1.22740985737237318223498077692*GFOffset(beta3,0,4,0) + 
      0.90163152340133989966053775745*GFOffset(beta3,0,5,0) - 
      0.62510324733980025532496696346*GFOffset(beta3,0,6,0) + 
      0.24451869400844663028521091858*GFOffset(beta3,0,7,0),0) + IfThen(tj == 
      10,0.148535195143070143054383947497*GFOffset(beta3,0,-10,0) - 
      0.3744692206289740714799192084*GFOffset(beta3,0,-9,0) + 
      0.52133984338829749335571433254*GFOffset(beta3,0,-8,0) - 
      0.66543038048463797319692695175*GFOffset(beta3,0,-7,0) + 
      0.83044724112301795463771928881*GFOffset(beta3,0,-6,0) - 
      1.04199613368497675695790118199*GFOffset(beta3,0,-5,0) + 
      1.34345606496915503717287457821*GFOffset(beta3,0,-4,0) - 
      1.8309905783222644495104831588*GFOffset(beta3,0,-3,0) + 
      2.7886469957442089235491946144*GFOffset(beta3,0,-2,0) - 
      5.6302894370117927868077791211*GFOffset(beta3,0,-1,0) + 
      5.6256744913992884348000044672*GFOffset(beta3,0,1,0) - 
      2.7690818594380164539724232637*GFOffset(beta3,0,2,0) + 
      1.7822014842955935859451244093*GFOffset(beta3,0,3,0) - 
      1.2416938715208579644505022202*GFOffset(beta3,0,4,0) + 
      0.83828842150746799078175057853*GFOffset(beta3,0,5,0) - 
      0.32463825647857910692083111049*GFOffset(beta3,0,6,0),0) + IfThen(tj == 
      11,-0.138907069646837506156467922982*GFOffset(beta3,0,-11,0) + 
      0.34942983354132426509071273763*GFOffset(beta3,0,-10,0) - 
      0.48385686192828167608039428479*GFOffset(beta3,0,-9,0) + 
      0.61187499728431125269744561717*GFOffset(beta3,0,-8,0) - 
      0.75260751091203410454863770474*GFOffset(beta3,0,-7,0) + 
      0.92355649158379422910974033451*GFOffset(beta3,0,-6,0) - 
      1.14989953850113756341678751431*GFOffset(beta3,0,-5,0) + 
      1.4781568448432306228464785126*GFOffset(beta3,0,-4,0) - 
      2.0138653755273951704351701347*GFOffset(beta3,0,-3,0) + 
      3.0703681361027735158780725202*GFOffset(beta3,0,-2,0) - 
      6.2082384879303237164867153437*GFOffset(beta3,0,-1,0) + 
      6.1982232105748690135841729691*GFOffset(beta3,0,1,0) - 
      3.0270925321392092857260728001*GFOffset(beta3,0,2,0) + 
      1.9017560292469961761829445848*GFOffset(beta3,0,3,0) - 
      1.22575929291604642001509669697*GFOffset(beta3,0,4,0) + 
      0.46686112632396636747577512626*GFOffset(beta3,0,5,0),0) + IfThen(tj == 
      12,0.136508355456600831314670575059*GFOffset(beta3,0,-12,0) - 
      0.34285746732004547213637755986*GFOffset(beta3,0,-11,0) + 
      0.4729331462037957083606828683*GFOffset(beta3,0,-10,0) - 
      0.59416808318701907014513742192*GFOffset(beta3,0,-9,0) + 
      0.72355865530535824794021258565*GFOffset(beta3,0,-8,0) - 
      0.87481845781405758828676845081*GFOffset(beta3,0,-7,0) + 
      1.0652590396252522137648987959*GFOffset(beta3,0,-6,0) - 
      1.32282396572542287644657467431*GFOffset(beta3,0,-5,0) + 
      1.7010434521867990558390611467*GFOffset(beta3,0,-4,0) - 
      2.3225546899410544915695827313*GFOffset(beta3,0,-3,0) + 
      3.5520492286055812420468337222*GFOffset(beta3,0,-2,0) - 
      7.2047116081680621707026720524*GFOffset(beta3,0,-1,0) + 
      7.1810992462682459840976026061*GFOffset(beta3,0,1,0) - 
      3.4459511192916461380881923237*GFOffset(beta3,0,2,0) + 
      2.0225580130708640640223348395*GFOffset(beta3,0,3,0) - 
      0.74712374527518954001099192507*GFOffset(beta3,0,4,0),0) + IfThen(tj == 
      13,-0.142011563214352779242862999816*GFOffset(beta3,0,-13,0) + 
      0.35628449710286139765470632448*GFOffset(beta3,0,-12,0) - 
      0.49012679524675272231094225417*GFOffset(beta3,0,-11,0) + 
      0.61297327191474456003014948906*GFOffset(beta3,0,-10,0) - 
      0.74134874830795214771393446336*GFOffset(beta3,0,-9,0) + 
      0.88741207962958841669287449253*GFOffset(beta3,0,-8,0) - 
      1.06502314499059230654638247221*GFOffset(beta3,0,-7,0) + 
      1.29435140870084806656280919*GFOffset(beta3,0,-6,0) - 
      1.60968101634442175130895793491*GFOffset(beta3,0,-5,0) + 
      2.0778111620780424940574758157*GFOffset(beta3,0,-4,0) - 
      2.8524183528095073388337823645*GFOffset(beta3,0,-3,0) + 
      4.390240639181825578641202719*GFOffset(beta3,0,-2,0) - 
      8.9599207502710419418678125413*GFOffset(beta3,0,-1,0) + 
      8.8906071670206541962088263737*GFOffset(beta3,0,1,0) - 
      4.0481982442230967749977900261*GFOffset(beta3,0,2,0) + 
      1.39904838977915305297442065187*GFOffset(beta3,0,3,0),0) + IfThen(tj == 
      14,0.159455418935743863864176801131*GFOffset(beta3,0,-14,0) - 
      0.39974928997635293940119558372*GFOffset(beta3,0,-13,0) + 
      0.54891972844065325040182692449*GFOffset(beta3,0,-12,0) - 
      0.68441580509143724201083245916*GFOffset(beta3,0,-11,0) + 
      0.82399500057024378503865758089*GFOffset(beta3,0,-10,0) - 
      0.97992111861336021584554474667*GFOffset(beta3,0,-9,0) + 
      1.16516900205061249641024554001*GFOffset(beta3,0,-8,0) - 
      1.3972258561707565847560028235*GFOffset(beta3,0,-7,0) + 
      1.7033853828073160608298284805*GFOffset(beta3,0,-6,0) - 
      2.1313616127677429944468291168*GFOffset(beta3,0,-5,0) + 
      2.7751249544430953067946729349*GFOffset(beta3,0,-4,0) - 
      3.8514921294753514104486234611*GFOffset(beta3,0,-3,0) + 
      6.003906719792868453304381935*GFOffset(beta3,0,-2,0) - 
      12.4148936988942509765781752778*GFOffset(beta3,0,-1,0) + 
      12.0980906953316627460912389063*GFOffset(beta3,0,1,0) - 
      3.4189873913829435992478256345*GFOffset(beta3,0,2,0),0) + IfThen(tj == 
      15,-0.20504307799646734735265811118*GFOffset(beta3,0,-15,0) + 
      0.51380481707098977744550540087*GFOffset(beta3,0,-14,0) - 
      0.70476591212082589044247602143*GFOffset(beta3,0,-13,0) + 
      0.87713350073939532514238019381*GFOffset(beta3,0,-12,0) - 
      1.05316307826105658459388074163*GFOffset(beta3,0,-11,0) + 
      1.24764601361733073935176914511*GFOffset(beta3,0,-10,0) - 
      1.47550715887261928380573952732*GFOffset(beta3,0,-9,0) + 
      1.7558839529986057597173561381*GFOffset(beta3,0,-8,0) - 
      2.1170486703261381185633144345*GFOffset(beta3,0,-7,0) + 
      2.6051755661549408690951791049*GFOffset(beta3,0,-6,0) - 
      3.303076725656509756145262224*GFOffset(beta3,0,-5,0) + 
      4.376597384264969120661066707*GFOffset(beta3,0,-4,0) - 
      6.2127374376796612987841995538*GFOffset(beta3,0,-3,0) + 
      9.9662217315544297047431656874*GFOffset(beta3,0,-2,0) - 
      21.329173403460624719650507164*GFOffset(beta3,0,-1,0) + 
      15.0580524979732417031816154011*GFOffset(beta3,0,1,0),0) + IfThen(tj == 
      16,0.5*GFOffset(beta3,0,-16,0) - 
      1.25268688236458726717120733545*GFOffset(beta3,0,-15,0) + 
      1.7174887026926442266484643494*GFOffset(beta3,0,-14,0) - 
      2.1359441768374612645390986478*GFOffset(beta3,0,-13,0) + 
      2.5617613217775424367069428177*GFOffset(beta3,0,-12,0) - 
      3.0300735379715023622251472559*GFOffset(beta3,0,-11,0) + 
      3.5756251418458313646084276667*GFOffset(beta3,0,-10,0) - 
      4.242018040981331373618358215*GFOffset(beta3,0,-9,0) + 
      5.0921522921522921522921522922*GFOffset(beta3,0,-8,0) - 
      6.225793703001792996013745096*GFOffset(beta3,0,-7,0) + 
      7.8148799060837185155248890235*GFOffset(beta3,0,-6,0) - 
      10.1839564276923609087636233103*GFOffset(beta3,0,-5,0) + 
      14.0207733572473326733232729469*GFOffset(beta3,0,-4,0) - 
      21.042577052349419249515496693*GFOffset(beta3,0,-3,0) + 
      36.825792804916105238285776948*GFOffset(beta3,0,-2,0) - 
      91.995423705517011185543249491*GFOffset(beta3,0,-1,0) + 
      68.*GFOffset(beta3,0,0,0),0))*pow(dy,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tj == 
      0,136.*GFOffset(beta3,0,-1,0),0) + IfThen(tj == 
      16,-136.*GFOffset(beta3,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDbeta13 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(beta1,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(beta1,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(beta1,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta1,0,0,1) - 
      36.825792804916105238285776948*GFOffset(beta1,0,0,2) + 
      21.042577052349419249515496693*GFOffset(beta1,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(beta1,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(beta1,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(beta1,0,0,6) + 
      6.225793703001792996013745096*GFOffset(beta1,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(beta1,0,0,8) + 
      4.242018040981331373618358215*GFOffset(beta1,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(beta1,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(beta1,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(beta1,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(beta1,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(beta1,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(beta1,0,0,15) - 
      0.5*GFOffset(beta1,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(beta1,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(beta1,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(beta1,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(beta1,0,0,3) - 
      4.376597384264969120661066707*GFOffset(beta1,0,0,4) + 
      3.303076725656509756145262224*GFOffset(beta1,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(beta1,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(beta1,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(beta1,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(beta1,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(beta1,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(beta1,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(beta1,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(beta1,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(beta1,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(beta1,0,0,15),0) + IfThen(tk 
      == 2,3.4189873913829435992478256345*GFOffset(beta1,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(beta1,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(beta1,0,0,1) - 
      6.003906719792868453304381935*GFOffset(beta1,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(beta1,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(beta1,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(beta1,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(beta1,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(beta1,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(beta1,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(beta1,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(beta1,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(beta1,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(beta1,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(beta1,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(beta1,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(beta1,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(beta1,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(beta1,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(beta1,0,0,1) - 
      4.390240639181825578641202719*GFOffset(beta1,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(beta1,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(beta1,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(beta1,0,0,5) - 
      1.29435140870084806656280919*GFOffset(beta1,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(beta1,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(beta1,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(beta1,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(beta1,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(beta1,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(beta1,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(beta1,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(beta1,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(beta1,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(beta1,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(beta1,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(beta1,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(beta1,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(beta1,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(beta1,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(beta1,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(beta1,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(beta1,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(beta1,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(beta1,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(beta1,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(beta1,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(beta1,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(beta1,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(beta1,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(beta1,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(beta1,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(beta1,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(beta1,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(beta1,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(beta1,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(beta1,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(beta1,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(beta1,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(beta1,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(beta1,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(beta1,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(beta1,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(beta1,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(beta1,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(beta1,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(beta1,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(beta1,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(beta1,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(beta1,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(beta1,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(beta1,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(beta1,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(beta1,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(beta1,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(beta1,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(beta1,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(beta1,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(beta1,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(beta1,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(beta1,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(beta1,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(beta1,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(beta1,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(beta1,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(beta1,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(beta1,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(beta1,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(beta1,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(beta1,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(beta1,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(beta1,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(beta1,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(beta1,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(beta1,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(beta1,0,0,9),0) + IfThen(tk 
      == 8,0.196380615234375*GFOffset(beta1,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(beta1,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(beta1,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(beta1,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(beta1,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(beta1,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(beta1,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(beta1,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(beta1,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(beta1,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(beta1,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(beta1,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(beta1,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(beta1,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(beta1,0,0,7) - 
      0.196380615234375*GFOffset(beta1,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(beta1,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(beta1,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(beta1,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(beta1,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(beta1,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(beta1,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(beta1,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(beta1,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(beta1,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(beta1,0,0,1) - 
      2.630489733142368322167942483*GFOffset(beta1,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(beta1,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(beta1,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(beta1,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(beta1,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(beta1,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(beta1,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(beta1,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(beta1,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(beta1,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(beta1,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(beta1,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(beta1,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(beta1,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(beta1,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(beta1,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(beta1,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(beta1,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(beta1,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(beta1,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(beta1,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(beta1,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(beta1,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(beta1,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(beta1,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(beta1,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(beta1,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(beta1,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(beta1,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(beta1,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(beta1,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(beta1,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(beta1,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(beta1,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(beta1,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(beta1,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(beta1,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(beta1,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(beta1,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(beta1,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(beta1,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(beta1,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(beta1,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(beta1,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(beta1,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(beta1,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(beta1,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(beta1,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(beta1,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(beta1,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(beta1,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(beta1,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(beta1,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(beta1,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(beta1,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(beta1,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(beta1,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(beta1,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(beta1,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(beta1,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(beta1,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(beta1,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(beta1,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(beta1,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(beta1,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(beta1,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(beta1,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(beta1,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(beta1,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(beta1,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(beta1,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(beta1,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(beta1,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(beta1,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(beta1,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(beta1,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(beta1,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(beta1,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(beta1,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(beta1,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(beta1,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(beta1,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(beta1,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(beta1,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(beta1,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(beta1,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(beta1,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(beta1,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(beta1,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(beta1,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(beta1,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(beta1,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(beta1,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(beta1,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(beta1,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(beta1,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(beta1,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(beta1,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(beta1,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(beta1,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(beta1,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(beta1,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(beta1,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(beta1,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(beta1,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(beta1,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(beta1,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(beta1,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(beta1,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(beta1,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(beta1,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(beta1,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(beta1,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(beta1,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(beta1,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(beta1,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(beta1,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(beta1,0,0,-1) + 
      68.*GFOffset(beta1,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(beta1,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(beta1,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDbeta23 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(beta2,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(beta2,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(beta2,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta2,0,0,1) - 
      36.825792804916105238285776948*GFOffset(beta2,0,0,2) + 
      21.042577052349419249515496693*GFOffset(beta2,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(beta2,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(beta2,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(beta2,0,0,6) + 
      6.225793703001792996013745096*GFOffset(beta2,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(beta2,0,0,8) + 
      4.242018040981331373618358215*GFOffset(beta2,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(beta2,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(beta2,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(beta2,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(beta2,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(beta2,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(beta2,0,0,15) - 
      0.5*GFOffset(beta2,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(beta2,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(beta2,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(beta2,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(beta2,0,0,3) - 
      4.376597384264969120661066707*GFOffset(beta2,0,0,4) + 
      3.303076725656509756145262224*GFOffset(beta2,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(beta2,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(beta2,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(beta2,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(beta2,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(beta2,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(beta2,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(beta2,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(beta2,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(beta2,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(beta2,0,0,15),0) + IfThen(tk 
      == 2,3.4189873913829435992478256345*GFOffset(beta2,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(beta2,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(beta2,0,0,1) - 
      6.003906719792868453304381935*GFOffset(beta2,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(beta2,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(beta2,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(beta2,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(beta2,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(beta2,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(beta2,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(beta2,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(beta2,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(beta2,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(beta2,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(beta2,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(beta2,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(beta2,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(beta2,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(beta2,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(beta2,0,0,1) - 
      4.390240639181825578641202719*GFOffset(beta2,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(beta2,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(beta2,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(beta2,0,0,5) - 
      1.29435140870084806656280919*GFOffset(beta2,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(beta2,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(beta2,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(beta2,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(beta2,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(beta2,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(beta2,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(beta2,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(beta2,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(beta2,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(beta2,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(beta2,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(beta2,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(beta2,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(beta2,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(beta2,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(beta2,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(beta2,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(beta2,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(beta2,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(beta2,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(beta2,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(beta2,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(beta2,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(beta2,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(beta2,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(beta2,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(beta2,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(beta2,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(beta2,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(beta2,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(beta2,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(beta2,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(beta2,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(beta2,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(beta2,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(beta2,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(beta2,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(beta2,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(beta2,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(beta2,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(beta2,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(beta2,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(beta2,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(beta2,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(beta2,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(beta2,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(beta2,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(beta2,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(beta2,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(beta2,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(beta2,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(beta2,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(beta2,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(beta2,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(beta2,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(beta2,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(beta2,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(beta2,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(beta2,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(beta2,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(beta2,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(beta2,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(beta2,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(beta2,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(beta2,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(beta2,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(beta2,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(beta2,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(beta2,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(beta2,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(beta2,0,0,9),0) + IfThen(tk 
      == 8,0.196380615234375*GFOffset(beta2,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(beta2,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(beta2,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(beta2,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(beta2,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(beta2,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(beta2,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(beta2,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(beta2,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(beta2,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(beta2,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(beta2,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(beta2,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(beta2,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(beta2,0,0,7) - 
      0.196380615234375*GFOffset(beta2,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(beta2,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(beta2,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(beta2,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(beta2,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(beta2,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(beta2,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(beta2,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(beta2,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(beta2,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(beta2,0,0,1) - 
      2.630489733142368322167942483*GFOffset(beta2,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(beta2,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(beta2,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(beta2,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(beta2,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(beta2,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(beta2,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(beta2,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(beta2,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(beta2,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(beta2,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(beta2,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(beta2,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(beta2,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(beta2,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(beta2,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(beta2,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(beta2,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(beta2,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(beta2,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(beta2,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(beta2,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(beta2,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(beta2,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(beta2,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(beta2,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(beta2,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(beta2,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(beta2,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(beta2,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(beta2,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(beta2,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(beta2,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(beta2,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(beta2,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(beta2,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(beta2,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(beta2,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(beta2,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(beta2,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(beta2,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(beta2,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(beta2,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(beta2,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(beta2,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(beta2,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(beta2,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(beta2,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(beta2,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(beta2,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(beta2,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(beta2,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(beta2,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(beta2,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(beta2,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(beta2,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(beta2,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(beta2,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(beta2,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(beta2,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(beta2,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(beta2,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(beta2,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(beta2,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(beta2,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(beta2,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(beta2,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(beta2,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(beta2,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(beta2,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(beta2,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(beta2,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(beta2,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(beta2,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(beta2,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(beta2,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(beta2,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(beta2,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(beta2,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(beta2,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(beta2,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(beta2,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(beta2,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(beta2,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(beta2,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(beta2,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(beta2,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(beta2,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(beta2,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(beta2,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(beta2,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(beta2,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(beta2,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(beta2,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(beta2,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(beta2,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(beta2,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(beta2,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(beta2,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(beta2,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(beta2,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(beta2,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(beta2,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(beta2,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(beta2,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(beta2,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(beta2,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(beta2,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(beta2,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(beta2,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(beta2,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(beta2,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(beta2,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(beta2,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(beta2,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(beta2,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(beta2,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(beta2,0,0,-1) + 
      68.*GFOffset(beta2,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(beta2,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(beta2,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDbeta33 CCTK_ATTRIBUTE_UNUSED = 
      -0.0588235294117647058823529411765*(IfThen(tk == 
      0,-136.*GFOffset(beta3,0,0,0),0) + IfThen(tk == 
      16,136.*GFOffset(beta3,0,0,0),0))*pow(dz,-1) + 
      0.117647058823529411764705882353*(IfThen(tk == 
      0,-68.*GFOffset(beta3,0,0,0) + 
      91.995423705517011185543249491*GFOffset(beta3,0,0,1) - 
      36.825792804916105238285776948*GFOffset(beta3,0,0,2) + 
      21.042577052349419249515496693*GFOffset(beta3,0,0,3) - 
      14.0207733572473326733232729469*GFOffset(beta3,0,0,4) + 
      10.1839564276923609087636233103*GFOffset(beta3,0,0,5) - 
      7.8148799060837185155248890235*GFOffset(beta3,0,0,6) + 
      6.225793703001792996013745096*GFOffset(beta3,0,0,7) - 
      5.0921522921522921522921522922*GFOffset(beta3,0,0,8) + 
      4.242018040981331373618358215*GFOffset(beta3,0,0,9) - 
      3.5756251418458313646084276667*GFOffset(beta3,0,0,10) + 
      3.0300735379715023622251472559*GFOffset(beta3,0,0,11) - 
      2.5617613217775424367069428177*GFOffset(beta3,0,0,12) + 
      2.1359441768374612645390986478*GFOffset(beta3,0,0,13) - 
      1.7174887026926442266484643494*GFOffset(beta3,0,0,14) + 
      1.25268688236458726717120733545*GFOffset(beta3,0,0,15) - 
      0.5*GFOffset(beta3,0,0,16),0) + IfThen(tk == 
      1,-15.0580524979732417031816154011*GFOffset(beta3,0,0,-1) + 
      21.329173403460624719650507164*GFOffset(beta3,0,0,1) - 
      9.9662217315544297047431656874*GFOffset(beta3,0,0,2) + 
      6.2127374376796612987841995538*GFOffset(beta3,0,0,3) - 
      4.376597384264969120661066707*GFOffset(beta3,0,0,4) + 
      3.303076725656509756145262224*GFOffset(beta3,0,0,5) - 
      2.6051755661549408690951791049*GFOffset(beta3,0,0,6) + 
      2.1170486703261381185633144345*GFOffset(beta3,0,0,7) - 
      1.7558839529986057597173561381*GFOffset(beta3,0,0,8) + 
      1.47550715887261928380573952732*GFOffset(beta3,0,0,9) - 
      1.24764601361733073935176914511*GFOffset(beta3,0,0,10) + 
      1.05316307826105658459388074163*GFOffset(beta3,0,0,11) - 
      0.87713350073939532514238019381*GFOffset(beta3,0,0,12) + 
      0.70476591212082589044247602143*GFOffset(beta3,0,0,13) - 
      0.51380481707098977744550540087*GFOffset(beta3,0,0,14) + 
      0.20504307799646734735265811118*GFOffset(beta3,0,0,15),0) + IfThen(tk 
      == 2,3.4189873913829435992478256345*GFOffset(beta3,0,0,-2) - 
      12.0980906953316627460912389063*GFOffset(beta3,0,0,-1) + 
      12.4148936988942509765781752778*GFOffset(beta3,0,0,1) - 
      6.003906719792868453304381935*GFOffset(beta3,0,0,2) + 
      3.8514921294753514104486234611*GFOffset(beta3,0,0,3) - 
      2.7751249544430953067946729349*GFOffset(beta3,0,0,4) + 
      2.1313616127677429944468291168*GFOffset(beta3,0,0,5) - 
      1.7033853828073160608298284805*GFOffset(beta3,0,0,6) + 
      1.3972258561707565847560028235*GFOffset(beta3,0,0,7) - 
      1.16516900205061249641024554001*GFOffset(beta3,0,0,8) + 
      0.97992111861336021584554474667*GFOffset(beta3,0,0,9) - 
      0.82399500057024378503865758089*GFOffset(beta3,0,0,10) + 
      0.68441580509143724201083245916*GFOffset(beta3,0,0,11) - 
      0.54891972844065325040182692449*GFOffset(beta3,0,0,12) + 
      0.39974928997635293940119558372*GFOffset(beta3,0,0,13) - 
      0.159455418935743863864176801131*GFOffset(beta3,0,0,14),0) + IfThen(tk 
      == 3,-1.39904838977915305297442065187*GFOffset(beta3,0,0,-3) + 
      4.0481982442230967749977900261*GFOffset(beta3,0,0,-2) - 
      8.8906071670206541962088263737*GFOffset(beta3,0,0,-1) + 
      8.9599207502710419418678125413*GFOffset(beta3,0,0,1) - 
      4.390240639181825578641202719*GFOffset(beta3,0,0,2) + 
      2.8524183528095073388337823645*GFOffset(beta3,0,0,3) - 
      2.0778111620780424940574758157*GFOffset(beta3,0,0,4) + 
      1.60968101634442175130895793491*GFOffset(beta3,0,0,5) - 
      1.29435140870084806656280919*GFOffset(beta3,0,0,6) + 
      1.06502314499059230654638247221*GFOffset(beta3,0,0,7) - 
      0.88741207962958841669287449253*GFOffset(beta3,0,0,8) + 
      0.74134874830795214771393446336*GFOffset(beta3,0,0,9) - 
      0.61297327191474456003014948906*GFOffset(beta3,0,0,10) + 
      0.49012679524675272231094225417*GFOffset(beta3,0,0,11) - 
      0.35628449710286139765470632448*GFOffset(beta3,0,0,12) + 
      0.142011563214352779242862999816*GFOffset(beta3,0,0,13),0) + IfThen(tk 
      == 4,0.74712374527518954001099192507*GFOffset(beta3,0,0,-4) - 
      2.0225580130708640640223348395*GFOffset(beta3,0,0,-3) + 
      3.4459511192916461380881923237*GFOffset(beta3,0,0,-2) - 
      7.1810992462682459840976026061*GFOffset(beta3,0,0,-1) + 
      7.2047116081680621707026720524*GFOffset(beta3,0,0,1) - 
      3.5520492286055812420468337222*GFOffset(beta3,0,0,2) + 
      2.3225546899410544915695827313*GFOffset(beta3,0,0,3) - 
      1.7010434521867990558390611467*GFOffset(beta3,0,0,4) + 
      1.32282396572542287644657467431*GFOffset(beta3,0,0,5) - 
      1.0652590396252522137648987959*GFOffset(beta3,0,0,6) + 
      0.87481845781405758828676845081*GFOffset(beta3,0,0,7) - 
      0.72355865530535824794021258565*GFOffset(beta3,0,0,8) + 
      0.59416808318701907014513742192*GFOffset(beta3,0,0,9) - 
      0.4729331462037957083606828683*GFOffset(beta3,0,0,10) + 
      0.34285746732004547213637755986*GFOffset(beta3,0,0,11) - 
      0.136508355456600831314670575059*GFOffset(beta3,0,0,12),0) + IfThen(tk 
      == 5,-0.46686112632396636747577512626*GFOffset(beta3,0,0,-5) + 
      1.22575929291604642001509669697*GFOffset(beta3,0,0,-4) - 
      1.9017560292469961761829445848*GFOffset(beta3,0,0,-3) + 
      3.0270925321392092857260728001*GFOffset(beta3,0,0,-2) - 
      6.1982232105748690135841729691*GFOffset(beta3,0,0,-1) + 
      6.2082384879303237164867153437*GFOffset(beta3,0,0,1) - 
      3.0703681361027735158780725202*GFOffset(beta3,0,0,2) + 
      2.0138653755273951704351701347*GFOffset(beta3,0,0,3) - 
      1.4781568448432306228464785126*GFOffset(beta3,0,0,4) + 
      1.14989953850113756341678751431*GFOffset(beta3,0,0,5) - 
      0.92355649158379422910974033451*GFOffset(beta3,0,0,6) + 
      0.75260751091203410454863770474*GFOffset(beta3,0,0,7) - 
      0.61187499728431125269744561717*GFOffset(beta3,0,0,8) + 
      0.48385686192828167608039428479*GFOffset(beta3,0,0,9) - 
      0.34942983354132426509071273763*GFOffset(beta3,0,0,10) + 
      0.138907069646837506156467922982*GFOffset(beta3,0,0,11),0) + IfThen(tk 
      == 6,0.32463825647857910692083111049*GFOffset(beta3,0,0,-6) - 
      0.83828842150746799078175057853*GFOffset(beta3,0,0,-5) + 
      1.2416938715208579644505022202*GFOffset(beta3,0,0,-4) - 
      1.7822014842955935859451244093*GFOffset(beta3,0,0,-3) + 
      2.7690818594380164539724232637*GFOffset(beta3,0,0,-2) - 
      5.6256744913992884348000044672*GFOffset(beta3,0,0,-1) + 
      5.6302894370117927868077791211*GFOffset(beta3,0,0,1) - 
      2.7886469957442089235491946144*GFOffset(beta3,0,0,2) + 
      1.8309905783222644495104831588*GFOffset(beta3,0,0,3) - 
      1.34345606496915503717287457821*GFOffset(beta3,0,0,4) + 
      1.04199613368497675695790118199*GFOffset(beta3,0,0,5) - 
      0.83044724112301795463771928881*GFOffset(beta3,0,0,6) + 
      0.66543038048463797319692695175*GFOffset(beta3,0,0,7) - 
      0.52133984338829749335571433254*GFOffset(beta3,0,0,8) + 
      0.3744692206289740714799192084*GFOffset(beta3,0,0,9) - 
      0.148535195143070143054383947497*GFOffset(beta3,0,0,10),0) + IfThen(tk 
      == 7,-0.24451869400844663028521091858*GFOffset(beta3,0,0,-7) + 
      0.62510324733980025532496696346*GFOffset(beta3,0,0,-6) - 
      0.90163152340133989966053775745*GFOffset(beta3,0,0,-5) + 
      1.22740985737237318223498077692*GFOffset(beta3,0,0,-4) - 
      1.7118382276223548370005028254*GFOffset(beta3,0,0,-3) + 
      2.630489733142368322167942483*GFOffset(beta3,0,0,-2) - 
      5.3231741449034573785935861146*GFOffset(beta3,0,0,-1) + 
      5.3250464482630572904207380412*GFOffset(beta3,0,0,1) - 
      2.6383557234797737660003113595*GFOffset(beta3,0,0,2) + 
      1.7311155696570794764334229421*GFOffset(beta3,0,0,3) - 
      1.26638768772191418505470175919*GFOffset(beta3,0,0,4) + 
      0.97498700149069629173161293075*GFOffset(beta3,0,0,5) - 
      0.76460253315530448839544028004*GFOffset(beta3,0,0,6) + 
      0.59106951616673445661478237816*GFOffset(beta3,0,0,7) - 
      0.42131853807890128275765671591*GFOffset(beta3,0,0,8) + 
      0.166605698939383192819501215113*GFOffset(beta3,0,0,9),0) + IfThen(tk 
      == 8,0.196380615234375*GFOffset(beta3,0,0,-8) - 
      0.49879890575153179460488390904*GFOffset(beta3,0,0,-7) + 
      0.70756241379686533842877873719*GFOffset(beta3,0,0,-6) - 
      0.93369115561830415789433778777*GFOffset(beta3,0,0,-5) + 
      1.23109642377273339408357291018*GFOffset(beta3,0,0,-4) - 
      1.6941680481957597967343647471*GFOffset(beta3,0,0,-3) + 
      2.5888887352997334042415596822*GFOffset(beta3,0,0,-2) - 
      5.2288151784208519366049271655*GFOffset(beta3,0,0,-1) + 
      5.2288151784208519366049271655*GFOffset(beta3,0,0,1) - 
      2.5888887352997334042415596822*GFOffset(beta3,0,0,2) + 
      1.6941680481957597967343647471*GFOffset(beta3,0,0,3) - 
      1.23109642377273339408357291018*GFOffset(beta3,0,0,4) + 
      0.93369115561830415789433778777*GFOffset(beta3,0,0,5) - 
      0.70756241379686533842877873719*GFOffset(beta3,0,0,6) + 
      0.49879890575153179460488390904*GFOffset(beta3,0,0,7) - 
      0.196380615234375*GFOffset(beta3,0,0,8),0) + IfThen(tk == 
      9,-0.166605698939383192819501215113*GFOffset(beta3,0,0,-9) + 
      0.42131853807890128275765671591*GFOffset(beta3,0,0,-8) - 
      0.59106951616673445661478237816*GFOffset(beta3,0,0,-7) + 
      0.76460253315530448839544028004*GFOffset(beta3,0,0,-6) - 
      0.97498700149069629173161293075*GFOffset(beta3,0,0,-5) + 
      1.26638768772191418505470175919*GFOffset(beta3,0,0,-4) - 
      1.7311155696570794764334229421*GFOffset(beta3,0,0,-3) + 
      2.6383557234797737660003113595*GFOffset(beta3,0,0,-2) - 
      5.3250464482630572904207380412*GFOffset(beta3,0,0,-1) + 
      5.3231741449034573785935861146*GFOffset(beta3,0,0,1) - 
      2.630489733142368322167942483*GFOffset(beta3,0,0,2) + 
      1.7118382276223548370005028254*GFOffset(beta3,0,0,3) - 
      1.22740985737237318223498077692*GFOffset(beta3,0,0,4) + 
      0.90163152340133989966053775745*GFOffset(beta3,0,0,5) - 
      0.62510324733980025532496696346*GFOffset(beta3,0,0,6) + 
      0.24451869400844663028521091858*GFOffset(beta3,0,0,7),0) + IfThen(tk == 
      10,0.148535195143070143054383947497*GFOffset(beta3,0,0,-10) - 
      0.3744692206289740714799192084*GFOffset(beta3,0,0,-9) + 
      0.52133984338829749335571433254*GFOffset(beta3,0,0,-8) - 
      0.66543038048463797319692695175*GFOffset(beta3,0,0,-7) + 
      0.83044724112301795463771928881*GFOffset(beta3,0,0,-6) - 
      1.04199613368497675695790118199*GFOffset(beta3,0,0,-5) + 
      1.34345606496915503717287457821*GFOffset(beta3,0,0,-4) - 
      1.8309905783222644495104831588*GFOffset(beta3,0,0,-3) + 
      2.7886469957442089235491946144*GFOffset(beta3,0,0,-2) - 
      5.6302894370117927868077791211*GFOffset(beta3,0,0,-1) + 
      5.6256744913992884348000044672*GFOffset(beta3,0,0,1) - 
      2.7690818594380164539724232637*GFOffset(beta3,0,0,2) + 
      1.7822014842955935859451244093*GFOffset(beta3,0,0,3) - 
      1.2416938715208579644505022202*GFOffset(beta3,0,0,4) + 
      0.83828842150746799078175057853*GFOffset(beta3,0,0,5) - 
      0.32463825647857910692083111049*GFOffset(beta3,0,0,6),0) + IfThen(tk == 
      11,-0.138907069646837506156467922982*GFOffset(beta3,0,0,-11) + 
      0.34942983354132426509071273763*GFOffset(beta3,0,0,-10) - 
      0.48385686192828167608039428479*GFOffset(beta3,0,0,-9) + 
      0.61187499728431125269744561717*GFOffset(beta3,0,0,-8) - 
      0.75260751091203410454863770474*GFOffset(beta3,0,0,-7) + 
      0.92355649158379422910974033451*GFOffset(beta3,0,0,-6) - 
      1.14989953850113756341678751431*GFOffset(beta3,0,0,-5) + 
      1.4781568448432306228464785126*GFOffset(beta3,0,0,-4) - 
      2.0138653755273951704351701347*GFOffset(beta3,0,0,-3) + 
      3.0703681361027735158780725202*GFOffset(beta3,0,0,-2) - 
      6.2082384879303237164867153437*GFOffset(beta3,0,0,-1) + 
      6.1982232105748690135841729691*GFOffset(beta3,0,0,1) - 
      3.0270925321392092857260728001*GFOffset(beta3,0,0,2) + 
      1.9017560292469961761829445848*GFOffset(beta3,0,0,3) - 
      1.22575929291604642001509669697*GFOffset(beta3,0,0,4) + 
      0.46686112632396636747577512626*GFOffset(beta3,0,0,5),0) + IfThen(tk == 
      12,0.136508355456600831314670575059*GFOffset(beta3,0,0,-12) - 
      0.34285746732004547213637755986*GFOffset(beta3,0,0,-11) + 
      0.4729331462037957083606828683*GFOffset(beta3,0,0,-10) - 
      0.59416808318701907014513742192*GFOffset(beta3,0,0,-9) + 
      0.72355865530535824794021258565*GFOffset(beta3,0,0,-8) - 
      0.87481845781405758828676845081*GFOffset(beta3,0,0,-7) + 
      1.0652590396252522137648987959*GFOffset(beta3,0,0,-6) - 
      1.32282396572542287644657467431*GFOffset(beta3,0,0,-5) + 
      1.7010434521867990558390611467*GFOffset(beta3,0,0,-4) - 
      2.3225546899410544915695827313*GFOffset(beta3,0,0,-3) + 
      3.5520492286055812420468337222*GFOffset(beta3,0,0,-2) - 
      7.2047116081680621707026720524*GFOffset(beta3,0,0,-1) + 
      7.1810992462682459840976026061*GFOffset(beta3,0,0,1) - 
      3.4459511192916461380881923237*GFOffset(beta3,0,0,2) + 
      2.0225580130708640640223348395*GFOffset(beta3,0,0,3) - 
      0.74712374527518954001099192507*GFOffset(beta3,0,0,4),0) + IfThen(tk == 
      13,-0.142011563214352779242862999816*GFOffset(beta3,0,0,-13) + 
      0.35628449710286139765470632448*GFOffset(beta3,0,0,-12) - 
      0.49012679524675272231094225417*GFOffset(beta3,0,0,-11) + 
      0.61297327191474456003014948906*GFOffset(beta3,0,0,-10) - 
      0.74134874830795214771393446336*GFOffset(beta3,0,0,-9) + 
      0.88741207962958841669287449253*GFOffset(beta3,0,0,-8) - 
      1.06502314499059230654638247221*GFOffset(beta3,0,0,-7) + 
      1.29435140870084806656280919*GFOffset(beta3,0,0,-6) - 
      1.60968101634442175130895793491*GFOffset(beta3,0,0,-5) + 
      2.0778111620780424940574758157*GFOffset(beta3,0,0,-4) - 
      2.8524183528095073388337823645*GFOffset(beta3,0,0,-3) + 
      4.390240639181825578641202719*GFOffset(beta3,0,0,-2) - 
      8.9599207502710419418678125413*GFOffset(beta3,0,0,-1) + 
      8.8906071670206541962088263737*GFOffset(beta3,0,0,1) - 
      4.0481982442230967749977900261*GFOffset(beta3,0,0,2) + 
      1.39904838977915305297442065187*GFOffset(beta3,0,0,3),0) + IfThen(tk == 
      14,0.159455418935743863864176801131*GFOffset(beta3,0,0,-14) - 
      0.39974928997635293940119558372*GFOffset(beta3,0,0,-13) + 
      0.54891972844065325040182692449*GFOffset(beta3,0,0,-12) - 
      0.68441580509143724201083245916*GFOffset(beta3,0,0,-11) + 
      0.82399500057024378503865758089*GFOffset(beta3,0,0,-10) - 
      0.97992111861336021584554474667*GFOffset(beta3,0,0,-9) + 
      1.16516900205061249641024554001*GFOffset(beta3,0,0,-8) - 
      1.3972258561707565847560028235*GFOffset(beta3,0,0,-7) + 
      1.7033853828073160608298284805*GFOffset(beta3,0,0,-6) - 
      2.1313616127677429944468291168*GFOffset(beta3,0,0,-5) + 
      2.7751249544430953067946729349*GFOffset(beta3,0,0,-4) - 
      3.8514921294753514104486234611*GFOffset(beta3,0,0,-3) + 
      6.003906719792868453304381935*GFOffset(beta3,0,0,-2) - 
      12.4148936988942509765781752778*GFOffset(beta3,0,0,-1) + 
      12.0980906953316627460912389063*GFOffset(beta3,0,0,1) - 
      3.4189873913829435992478256345*GFOffset(beta3,0,0,2),0) + IfThen(tk == 
      15,-0.20504307799646734735265811118*GFOffset(beta3,0,0,-15) + 
      0.51380481707098977744550540087*GFOffset(beta3,0,0,-14) - 
      0.70476591212082589044247602143*GFOffset(beta3,0,0,-13) + 
      0.87713350073939532514238019381*GFOffset(beta3,0,0,-12) - 
      1.05316307826105658459388074163*GFOffset(beta3,0,0,-11) + 
      1.24764601361733073935176914511*GFOffset(beta3,0,0,-10) - 
      1.47550715887261928380573952732*GFOffset(beta3,0,0,-9) + 
      1.7558839529986057597173561381*GFOffset(beta3,0,0,-8) - 
      2.1170486703261381185633144345*GFOffset(beta3,0,0,-7) + 
      2.6051755661549408690951791049*GFOffset(beta3,0,0,-6) - 
      3.303076725656509756145262224*GFOffset(beta3,0,0,-5) + 
      4.376597384264969120661066707*GFOffset(beta3,0,0,-4) - 
      6.2127374376796612987841995538*GFOffset(beta3,0,0,-3) + 
      9.9662217315544297047431656874*GFOffset(beta3,0,0,-2) - 
      21.329173403460624719650507164*GFOffset(beta3,0,0,-1) + 
      15.0580524979732417031816154011*GFOffset(beta3,0,0,1),0) + IfThen(tk == 
      16,0.5*GFOffset(beta3,0,0,-16) - 
      1.25268688236458726717120733545*GFOffset(beta3,0,0,-15) + 
      1.7174887026926442266484643494*GFOffset(beta3,0,0,-14) - 
      2.1359441768374612645390986478*GFOffset(beta3,0,0,-13) + 
      2.5617613217775424367069428177*GFOffset(beta3,0,0,-12) - 
      3.0300735379715023622251472559*GFOffset(beta3,0,0,-11) + 
      3.5756251418458313646084276667*GFOffset(beta3,0,0,-10) - 
      4.242018040981331373618358215*GFOffset(beta3,0,0,-9) + 
      5.0921522921522921522921522922*GFOffset(beta3,0,0,-8) - 
      6.225793703001792996013745096*GFOffset(beta3,0,0,-7) + 
      7.8148799060837185155248890235*GFOffset(beta3,0,0,-6) - 
      10.1839564276923609087636233103*GFOffset(beta3,0,0,-5) + 
      14.0207733572473326733232729469*GFOffset(beta3,0,0,-4) - 
      21.042577052349419249515496693*GFOffset(beta3,0,0,-3) + 
      36.825792804916105238285776948*GFOffset(beta3,0,0,-2) - 
      91.995423705517011185543249491*GFOffset(beta3,0,0,-1) + 
      68.*GFOffset(beta3,0,0,0),0))*pow(dz,-1) - 
      0.117647058823529411764705882353*alphaDeriv*(IfThen(tk == 
      0,136.*GFOffset(beta3,0,0,-1),0) + IfThen(tk == 
      16,-136.*GFOffset(beta3,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDalpha1L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDalpha2L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDalpha3L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta11L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta12L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta13L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta21L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta22L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta23L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta31L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta32L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDbeta33L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt111L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt112L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt113L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt121L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt122L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt123L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt131L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt132L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt133L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt221L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt222L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt223L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt231L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt232L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt233L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt331L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt332L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDgt333L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDphiW1L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDphiW2L CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDphiW3L CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDphiW1L = J11L*LDphiW1 + J21L*LDphiW2 + J31L*LDphiW3;
      
      PDphiW2L = J12L*LDphiW1 + J22L*LDphiW2 + J32L*LDphiW3;
      
      PDphiW3L = J13L*LDphiW1 + J23L*LDphiW2 + J33L*LDphiW3;
      
      PDgt111L = J11L*LDgt111 + J21L*LDgt112 + J31L*LDgt113;
      
      PDgt112L = J12L*LDgt111 + J22L*LDgt112 + J32L*LDgt113;
      
      PDgt113L = J13L*LDgt111 + J23L*LDgt112 + J33L*LDgt113;
      
      PDgt121L = J11L*LDgt121 + J21L*LDgt122 + J31L*LDgt123;
      
      PDgt122L = J12L*LDgt121 + J22L*LDgt122 + J32L*LDgt123;
      
      PDgt123L = J13L*LDgt121 + J23L*LDgt122 + J33L*LDgt123;
      
      PDgt131L = J11L*LDgt131 + J21L*LDgt132 + J31L*LDgt133;
      
      PDgt132L = J12L*LDgt131 + J22L*LDgt132 + J32L*LDgt133;
      
      PDgt133L = J13L*LDgt131 + J23L*LDgt132 + J33L*LDgt133;
      
      PDgt221L = J11L*LDgt221 + J21L*LDgt222 + J31L*LDgt223;
      
      PDgt222L = J12L*LDgt221 + J22L*LDgt222 + J32L*LDgt223;
      
      PDgt223L = J13L*LDgt221 + J23L*LDgt222 + J33L*LDgt223;
      
      PDgt231L = J11L*LDgt231 + J21L*LDgt232 + J31L*LDgt233;
      
      PDgt232L = J12L*LDgt231 + J22L*LDgt232 + J32L*LDgt233;
      
      PDgt233L = J13L*LDgt231 + J23L*LDgt232 + J33L*LDgt233;
      
      PDgt331L = J11L*LDgt331 + J21L*LDgt332 + J31L*LDgt333;
      
      PDgt332L = J12L*LDgt331 + J22L*LDgt332 + J32L*LDgt333;
      
      PDgt333L = J13L*LDgt331 + J23L*LDgt332 + J33L*LDgt333;
      
      PDalpha1L = J11L*LDalpha1 + J21L*LDalpha2 + J31L*LDalpha3;
      
      PDalpha2L = J12L*LDalpha1 + J22L*LDalpha2 + J32L*LDalpha3;
      
      PDalpha3L = J13L*LDalpha1 + J23L*LDalpha2 + J33L*LDalpha3;
      
      PDbeta11L = J11L*LDbeta11 + J21L*LDbeta12 + J31L*LDbeta13;
      
      PDbeta21L = J11L*LDbeta21 + J21L*LDbeta22 + J31L*LDbeta23;
      
      PDbeta31L = J11L*LDbeta31 + J21L*LDbeta32 + J31L*LDbeta33;
      
      PDbeta12L = J12L*LDbeta11 + J22L*LDbeta12 + J32L*LDbeta13;
      
      PDbeta22L = J12L*LDbeta21 + J22L*LDbeta22 + J32L*LDbeta23;
      
      PDbeta32L = J12L*LDbeta31 + J22L*LDbeta32 + J32L*LDbeta33;
      
      PDbeta13L = J13L*LDbeta11 + J23L*LDbeta12 + J33L*LDbeta13;
      
      PDbeta23L = J13L*LDbeta21 + J23L*LDbeta22 + J33L*LDbeta23;
      
      PDbeta33L = J13L*LDbeta31 + J23L*LDbeta32 + J33L*LDbeta33;
    }
    else
    {
      PDphiW1L = LDphiW1;
      
      PDphiW2L = LDphiW2;
      
      PDphiW3L = LDphiW3;
      
      PDgt111L = LDgt111;
      
      PDgt112L = LDgt112;
      
      PDgt113L = LDgt113;
      
      PDgt121L = LDgt121;
      
      PDgt122L = LDgt122;
      
      PDgt123L = LDgt123;
      
      PDgt131L = LDgt131;
      
      PDgt132L = LDgt132;
      
      PDgt133L = LDgt133;
      
      PDgt221L = LDgt221;
      
      PDgt222L = LDgt222;
      
      PDgt223L = LDgt223;
      
      PDgt231L = LDgt231;
      
      PDgt232L = LDgt232;
      
      PDgt233L = LDgt233;
      
      PDgt331L = LDgt331;
      
      PDgt332L = LDgt332;
      
      PDgt333L = LDgt333;
      
      PDalpha1L = LDalpha1;
      
      PDalpha2L = LDalpha2;
      
      PDalpha3L = LDalpha3;
      
      PDbeta11L = LDbeta11;
      
      PDbeta21L = LDbeta21;
      
      PDbeta31L = LDbeta31;
      
      PDbeta12L = LDbeta12;
      
      PDbeta22L = LDbeta22;
      
      PDbeta32L = LDbeta32;
      
      PDbeta13L = LDbeta13;
      
      PDbeta23L = LDbeta23;
      
      PDbeta33L = LDbeta33;
    }
    /* Copy local copies back to grid functions */
    PDalpha1[index] = PDalpha1L;
    PDalpha2[index] = PDalpha2L;
    PDalpha3[index] = PDalpha3L;
    PDbeta11[index] = PDbeta11L;
    PDbeta12[index] = PDbeta12L;
    PDbeta13[index] = PDbeta13L;
    PDbeta21[index] = PDbeta21L;
    PDbeta22[index] = PDbeta22L;
    PDbeta23[index] = PDbeta23L;
    PDbeta31[index] = PDbeta31L;
    PDbeta32[index] = PDbeta32L;
    PDbeta33[index] = PDbeta33L;
    PDgt111[index] = PDgt111L;
    PDgt112[index] = PDgt112L;
    PDgt113[index] = PDgt113L;
    PDgt121[index] = PDgt121L;
    PDgt122[index] = PDgt122L;
    PDgt123[index] = PDgt123L;
    PDgt131[index] = PDgt131L;
    PDgt132[index] = PDgt132L;
    PDgt133[index] = PDgt133L;
    PDgt221[index] = PDgt221L;
    PDgt222[index] = PDgt222L;
    PDgt223[index] = PDgt223L;
    PDgt231[index] = PDgt231L;
    PDgt232[index] = PDgt232L;
    PDgt233[index] = PDgt233L;
    PDgt331[index] = PDgt331L;
    PDgt332[index] = PDgt332L;
    PDgt333[index] = PDgt333L;
    PDphiW1[index] = PDphiW1L;
    PDphiW2[index] = PDphiW2L;
    PDphiW3[index] = PDphiW3L;
  }
  CCTK_ENDLOOP3(ML_BSSN_DG16_DerivativesInterior);
}
extern "C" void ML_BSSN_DG16_DerivativesInterior(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_DG16_DerivativesInterior_Body");
  }
  if (cctk_iteration % ML_BSSN_DG16_DerivativesInterior_calc_every != ML_BSSN_DG16_DerivativesInterior_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_BSSN_DG16::ML_confac",
    "ML_BSSN_DG16::ML_dconfac",
    "ML_BSSN_DG16::ML_dlapse",
    "ML_BSSN_DG16::ML_dmetric",
    "ML_BSSN_DG16::ML_dshift",
    "ML_BSSN_DG16::ML_lapse",
    "ML_BSSN_DG16::ML_metric",
    "ML_BSSN_DG16::ML_shift"};
  AssertGroupStorage(cctkGH, "ML_BSSN_DG16_DerivativesInterior", 8, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_BSSN_DG16_DerivativesInterior_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_DG16_DerivativesInterior_Body");
  }
}

} // namespace ML_BSSN_DG16
