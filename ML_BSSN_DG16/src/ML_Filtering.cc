#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cassert>

extern "C"
void ML_BSSN_DG16_ConjugateFilterSelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int ierr;
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_BSSN_DG16::ML_dtlapse", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_BSSN_DG16::ML_dtshift", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_BSSN_DG16::ML_trace_curv", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_BSSN_DG16::ML_cons_Gamma", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_BSSN_DG16::ML_curv", "none");
  assert(!ierr);
}
