#include <Slicing.h>

#include <cctk.h>

extern "C"
int ML_BSSN_DG16_RegisterSlicing(void)
{
  Einstein_RegisterSlicing("ML_BSSN_DG16");
  return 0;
}
