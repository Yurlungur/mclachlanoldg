/*  File produced by Kranc */

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

extern "C" void ML_BSSN_DG16_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  /* Register all the evolved grid functions with MoL */
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::phiW"),  CCTK_VarIndex("ML_BSSN_DG16::phiWrhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::At11"),  CCTK_VarIndex("ML_BSSN_DG16::At11rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::At12"),  CCTK_VarIndex("ML_BSSN_DG16::At12rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::At13"),  CCTK_VarIndex("ML_BSSN_DG16::At13rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::At22"),  CCTK_VarIndex("ML_BSSN_DG16::At22rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::At23"),  CCTK_VarIndex("ML_BSSN_DG16::At23rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::At33"),  CCTK_VarIndex("ML_BSSN_DG16::At33rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::A"),  CCTK_VarIndex("ML_BSSN_DG16::Arhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::B1"),  CCTK_VarIndex("ML_BSSN_DG16::B1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::B2"),  CCTK_VarIndex("ML_BSSN_DG16::B2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::B3"),  CCTK_VarIndex("ML_BSSN_DG16::B3rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::Xt1"),  CCTK_VarIndex("ML_BSSN_DG16::Xt1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::Xt2"),  CCTK_VarIndex("ML_BSSN_DG16::Xt2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::Xt3"),  CCTK_VarIndex("ML_BSSN_DG16::Xt3rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::alpha"),  CCTK_VarIndex("ML_BSSN_DG16::alpharhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::gt11"),  CCTK_VarIndex("ML_BSSN_DG16::gt11rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::gt12"),  CCTK_VarIndex("ML_BSSN_DG16::gt12rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::gt13"),  CCTK_VarIndex("ML_BSSN_DG16::gt13rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::gt22"),  CCTK_VarIndex("ML_BSSN_DG16::gt22rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::gt23"),  CCTK_VarIndex("ML_BSSN_DG16::gt23rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::gt33"),  CCTK_VarIndex("ML_BSSN_DG16::gt33rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::beta1"),  CCTK_VarIndex("ML_BSSN_DG16::beta1rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::beta2"),  CCTK_VarIndex("ML_BSSN_DG16::beta2rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::beta3"),  CCTK_VarIndex("ML_BSSN_DG16::beta3rhs"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ML_BSSN_DG16::trK"),  CCTK_VarIndex("ML_BSSN_DG16::trKrhs"));
  /* Register all the evolved Array functions with MoL */
  return;
}
