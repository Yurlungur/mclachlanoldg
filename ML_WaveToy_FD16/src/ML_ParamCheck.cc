#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cassert>

extern "C"
void ML_WaveToy_FD16_ParamCheck(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int type;
  const void* parptr = CCTK_ParameterGet("init_RHS_zero", "Mol", &type);
  assert(type == PARAMETER_BOOLEAN);
  assert(parptr);
  CCTK_INT MoL_init_RHS_zero = *static_cast<const CCTK_INT*>(parptr);
  if (MoL_init_RHS_zero) {
    CCTK_PARAMWARN("MoL::init_RHS_zero must not be set since the RHS is pre-evaluated");
  }
}
