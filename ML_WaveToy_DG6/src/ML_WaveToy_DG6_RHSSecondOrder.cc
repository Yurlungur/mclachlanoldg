/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_WaveToy_DG6 {

extern "C" void ML_WaveToy_DG6_RHSSecondOrder_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_WaveToy_DG6_RHSSecondOrder_calc_every != ML_WaveToy_DG6_RHSSecondOrder_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG6::WT_rhorhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG6::WT_rhorhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG6::WT_urhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG6::WT_urhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG6::WT_vrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG6::WT_vrhs.");
  return;
}

static void ML_WaveToy_DG6_RHSSecondOrder_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_WaveToy_DG6_RHSSecondOrder,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL du1L CCTK_ATTRIBUTE_UNUSED = du1[index];
    CCTK_REAL du2L CCTK_ATTRIBUTE_UNUSED = du2[index];
    CCTK_REAL du3L CCTK_ATTRIBUTE_UNUSED = du3[index];
    CCTK_REAL myDetJL CCTK_ATTRIBUTE_UNUSED = myDetJ[index];
    CCTK_REAL rhoL CCTK_ATTRIBUTE_UNUSED = rho[index];
    CCTK_REAL uL CCTK_ATTRIBUTE_UNUSED = u[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDdu11 CCTK_ATTRIBUTE_UNUSED = 
      0.285714285714285714285714285714*(IfThen(ti == 
      0,-1.5*GFOffset(du1,0,0,0) + 
      14.201576602919816167081697726*GFOffset(du1,1,0,0) - 
      5.6689852255455078785754090169*GFOffset(du1,2,0,0) + 
      3.2*GFOffset(du1,3,0,0) - 
      2.0499648130767427769623871824*GFOffset(du1,4,0,0) + 
      1.3173734357024344884560984733*GFOffset(du1,5,0,0) + 
      1.*GFOffset(du1,6,0,0),0) + IfThen(ti == 
      1,-1.8208003236771012201589305835*GFOffset(du1,-1,0,0) + 
      3.4558282142942851326012978916*GFOffset(du1,1,0,0) - 
      1.5986066880983668371678581959*GFOffset(du1,2,0,0) + 
      0.96133979728871166671127688438*GFOffset(du1,3,0,0) - 
      0.60224717963578568466640341307*GFOffset(du1,4,0,0) - 
      0.3955138201717430573193825835*GFOffset(du1,5,0,0),0) + IfThen(ti == 
      2,0.12709791709691405234867282716*GFOffset(du1,-2,0,0) - 
      2.2158042831699713136380303455*GFOffset(du1,-1,0,0) + 
      2.2666980870859990122084630061*GFOffset(du1,1,0,0) - 
      1.06644190400637468973550663446*GFOffset(du1,2,0,0) + 
      0.61639083551757952864611929469*GFOffset(du1,3,0,0) + 
      0.27205934747585341017028185199*GFOffset(du1,4,0,0),0) + IfThen(ti == 
      3,0.15625*GFOffset(du1,-3,0,0) + 
      0.9075444712688209188202794332*GFOffset(du1,-2,0,0) - 
      2.00696924058875308957204950201*GFOffset(du1,-1,0,0) + 
      2.00696924058875308957204950201*GFOffset(du1,1,0,0) - 
      0.9075444712688209188202794332*GFOffset(du1,2,0,0) - 
      0.15625*GFOffset(du1,3,0,0),0) + IfThen(ti == 
      4,-0.27205934747585341017028185199*GFOffset(du1,-4,0,0) - 
      0.61639083551757952864611929469*GFOffset(du1,-3,0,0) + 
      1.06644190400637468973550663446*GFOffset(du1,-2,0,0) - 
      2.2666980870859990122084630061*GFOffset(du1,-1,0,0) + 
      2.2158042831699713136380303455*GFOffset(du1,1,0,0) - 
      0.12709791709691405234867282716*GFOffset(du1,2,0,0),0) + IfThen(ti == 
      5,0.3955138201717430573193825835*GFOffset(du1,-5,0,0) + 
      0.60224717963578568466640341307*GFOffset(du1,-4,0,0) - 
      0.96133979728871166671127688438*GFOffset(du1,-3,0,0) + 
      1.5986066880983668371678581959*GFOffset(du1,-2,0,0) - 
      3.4558282142942851326012978916*GFOffset(du1,-1,0,0) + 
      1.8208003236771012201589305835*GFOffset(du1,1,0,0),0) + IfThen(ti == 
      6,-1.*GFOffset(du1,-6,0,0) - 
      1.3173734357024344884560984733*GFOffset(du1,-5,0,0) + 
      2.0499648130767427769623871824*GFOffset(du1,-4,0,0) - 
      3.2*GFOffset(du1,-3,0,0) + 
      5.6689852255455078785754090169*GFOffset(du1,-2,0,0) - 
      14.201576602919816167081697726*GFOffset(du1,-1,0,0) + 
      1.5*GFOffset(du1,0,0,0),0))*pow(dx,-1) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(ti == 
      0,18.*GFOffset(du1,-1,0,0) + 3.*GFOffset(du1,7,0,0),0) + IfThen(ti == 
      1,1.2442513811343767849692089999*GFOffset(du1,-2,0,0) - 
      1.2442513811343767849692089999*GFOffset(du1,6,0,0),0) + IfThen(ti == 
      2,-0.99631749683685612381218420655*GFOffset(du1,-3,0,0) + 
      0.99631749683685612381218420655*GFOffset(du1,5,0,0),0) + IfThen(ti == 
      3,0.9375*GFOffset(du1,-4,0,0) - 0.9375*GFOffset(du1,4,0,0),0) + 
      IfThen(ti == 4,-0.99631749683685612381218420655*GFOffset(du1,-5,0,0) + 
      0.99631749683685612381218420655*GFOffset(du1,3,0,0),0) + IfThen(ti == 
      5,1.2442513811343767849692089999*GFOffset(du1,-6,0,0) - 
      1.2442513811343767849692089999*GFOffset(du1,2,0,0),0) + IfThen(ti == 
      6,-3.*GFOffset(du1,-7,0,0) - 18.*GFOffset(du1,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDdu12 CCTK_ATTRIBUTE_UNUSED = 
      0.285714285714285714285714285714*(IfThen(tj == 
      0,-1.5*GFOffset(du1,0,0,0) + 
      14.201576602919816167081697726*GFOffset(du1,0,1,0) - 
      5.6689852255455078785754090169*GFOffset(du1,0,2,0) + 
      3.2*GFOffset(du1,0,3,0) - 
      2.0499648130767427769623871824*GFOffset(du1,0,4,0) + 
      1.3173734357024344884560984733*GFOffset(du1,0,5,0) + 
      1.*GFOffset(du1,0,6,0),0) + IfThen(tj == 
      1,-1.8208003236771012201589305835*GFOffset(du1,0,-1,0) + 
      3.4558282142942851326012978916*GFOffset(du1,0,1,0) - 
      1.5986066880983668371678581959*GFOffset(du1,0,2,0) + 
      0.96133979728871166671127688438*GFOffset(du1,0,3,0) - 
      0.60224717963578568466640341307*GFOffset(du1,0,4,0) - 
      0.3955138201717430573193825835*GFOffset(du1,0,5,0),0) + IfThen(tj == 
      2,0.12709791709691405234867282716*GFOffset(du1,0,-2,0) - 
      2.2158042831699713136380303455*GFOffset(du1,0,-1,0) + 
      2.2666980870859990122084630061*GFOffset(du1,0,1,0) - 
      1.06644190400637468973550663446*GFOffset(du1,0,2,0) + 
      0.61639083551757952864611929469*GFOffset(du1,0,3,0) + 
      0.27205934747585341017028185199*GFOffset(du1,0,4,0),0) + IfThen(tj == 
      3,0.15625*GFOffset(du1,0,-3,0) + 
      0.9075444712688209188202794332*GFOffset(du1,0,-2,0) - 
      2.00696924058875308957204950201*GFOffset(du1,0,-1,0) + 
      2.00696924058875308957204950201*GFOffset(du1,0,1,0) - 
      0.9075444712688209188202794332*GFOffset(du1,0,2,0) - 
      0.15625*GFOffset(du1,0,3,0),0) + IfThen(tj == 
      4,-0.27205934747585341017028185199*GFOffset(du1,0,-4,0) - 
      0.61639083551757952864611929469*GFOffset(du1,0,-3,0) + 
      1.06644190400637468973550663446*GFOffset(du1,0,-2,0) - 
      2.2666980870859990122084630061*GFOffset(du1,0,-1,0) + 
      2.2158042831699713136380303455*GFOffset(du1,0,1,0) - 
      0.12709791709691405234867282716*GFOffset(du1,0,2,0),0) + IfThen(tj == 
      5,0.3955138201717430573193825835*GFOffset(du1,0,-5,0) + 
      0.60224717963578568466640341307*GFOffset(du1,0,-4,0) - 
      0.96133979728871166671127688438*GFOffset(du1,0,-3,0) + 
      1.5986066880983668371678581959*GFOffset(du1,0,-2,0) - 
      3.4558282142942851326012978916*GFOffset(du1,0,-1,0) + 
      1.8208003236771012201589305835*GFOffset(du1,0,1,0),0) + IfThen(tj == 
      6,-1.*GFOffset(du1,0,-6,0) - 
      1.3173734357024344884560984733*GFOffset(du1,0,-5,0) + 
      2.0499648130767427769623871824*GFOffset(du1,0,-4,0) - 
      3.2*GFOffset(du1,0,-3,0) + 
      5.6689852255455078785754090169*GFOffset(du1,0,-2,0) - 
      14.201576602919816167081697726*GFOffset(du1,0,-1,0) + 
      1.5*GFOffset(du1,0,0,0),0))*pow(dy,-1) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tj == 
      0,18.*GFOffset(du1,0,-1,0) + 3.*GFOffset(du1,0,7,0),0) + IfThen(tj == 
      1,1.2442513811343767849692089999*GFOffset(du1,0,-2,0) - 
      1.2442513811343767849692089999*GFOffset(du1,0,6,0),0) + IfThen(tj == 
      2,-0.99631749683685612381218420655*GFOffset(du1,0,-3,0) + 
      0.99631749683685612381218420655*GFOffset(du1,0,5,0),0) + IfThen(tj == 
      3,0.9375*GFOffset(du1,0,-4,0) - 0.9375*GFOffset(du1,0,4,0),0) + 
      IfThen(tj == 4,-0.99631749683685612381218420655*GFOffset(du1,0,-5,0) + 
      0.99631749683685612381218420655*GFOffset(du1,0,3,0),0) + IfThen(tj == 
      5,1.2442513811343767849692089999*GFOffset(du1,0,-6,0) - 
      1.2442513811343767849692089999*GFOffset(du1,0,2,0),0) + IfThen(tj == 
      6,-3.*GFOffset(du1,0,-7,0) - 18.*GFOffset(du1,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDdu13 CCTK_ATTRIBUTE_UNUSED = 
      0.285714285714285714285714285714*(IfThen(tk == 
      0,-1.5*GFOffset(du1,0,0,0) + 
      14.201576602919816167081697726*GFOffset(du1,0,0,1) - 
      5.6689852255455078785754090169*GFOffset(du1,0,0,2) + 
      3.2*GFOffset(du1,0,0,3) - 
      2.0499648130767427769623871824*GFOffset(du1,0,0,4) + 
      1.3173734357024344884560984733*GFOffset(du1,0,0,5) + 
      1.*GFOffset(du1,0,0,6),0) + IfThen(tk == 
      1,-1.8208003236771012201589305835*GFOffset(du1,0,0,-1) + 
      3.4558282142942851326012978916*GFOffset(du1,0,0,1) - 
      1.5986066880983668371678581959*GFOffset(du1,0,0,2) + 
      0.96133979728871166671127688438*GFOffset(du1,0,0,3) - 
      0.60224717963578568466640341307*GFOffset(du1,0,0,4) - 
      0.3955138201717430573193825835*GFOffset(du1,0,0,5),0) + IfThen(tk == 
      2,0.12709791709691405234867282716*GFOffset(du1,0,0,-2) - 
      2.2158042831699713136380303455*GFOffset(du1,0,0,-1) + 
      2.2666980870859990122084630061*GFOffset(du1,0,0,1) - 
      1.06644190400637468973550663446*GFOffset(du1,0,0,2) + 
      0.61639083551757952864611929469*GFOffset(du1,0,0,3) + 
      0.27205934747585341017028185199*GFOffset(du1,0,0,4),0) + IfThen(tk == 
      3,0.15625*GFOffset(du1,0,0,-3) + 
      0.9075444712688209188202794332*GFOffset(du1,0,0,-2) - 
      2.00696924058875308957204950201*GFOffset(du1,0,0,-1) + 
      2.00696924058875308957204950201*GFOffset(du1,0,0,1) - 
      0.9075444712688209188202794332*GFOffset(du1,0,0,2) - 
      0.15625*GFOffset(du1,0,0,3),0) + IfThen(tk == 
      4,-0.27205934747585341017028185199*GFOffset(du1,0,0,-4) - 
      0.61639083551757952864611929469*GFOffset(du1,0,0,-3) + 
      1.06644190400637468973550663446*GFOffset(du1,0,0,-2) - 
      2.2666980870859990122084630061*GFOffset(du1,0,0,-1) + 
      2.2158042831699713136380303455*GFOffset(du1,0,0,1) - 
      0.12709791709691405234867282716*GFOffset(du1,0,0,2),0) + IfThen(tk == 
      5,0.3955138201717430573193825835*GFOffset(du1,0,0,-5) + 
      0.60224717963578568466640341307*GFOffset(du1,0,0,-4) - 
      0.96133979728871166671127688438*GFOffset(du1,0,0,-3) + 
      1.5986066880983668371678581959*GFOffset(du1,0,0,-2) - 
      3.4558282142942851326012978916*GFOffset(du1,0,0,-1) + 
      1.8208003236771012201589305835*GFOffset(du1,0,0,1),0) + IfThen(tk == 
      6,-1.*GFOffset(du1,0,0,-6) - 
      1.3173734357024344884560984733*GFOffset(du1,0,0,-5) + 
      2.0499648130767427769623871824*GFOffset(du1,0,0,-4) - 
      3.2*GFOffset(du1,0,0,-3) + 
      5.6689852255455078785754090169*GFOffset(du1,0,0,-2) - 
      14.201576602919816167081697726*GFOffset(du1,0,0,-1) + 
      1.5*GFOffset(du1,0,0,0),0))*pow(dz,-1) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tk == 
      0,18.*GFOffset(du1,0,0,-1) + 3.*GFOffset(du1,0,0,7),0) + IfThen(tk == 
      1,1.2442513811343767849692089999*GFOffset(du1,0,0,-2) - 
      1.2442513811343767849692089999*GFOffset(du1,0,0,6),0) + IfThen(tk == 
      2,-0.99631749683685612381218420655*GFOffset(du1,0,0,-3) + 
      0.99631749683685612381218420655*GFOffset(du1,0,0,5),0) + IfThen(tk == 
      3,0.9375*GFOffset(du1,0,0,-4) - 0.9375*GFOffset(du1,0,0,4),0) + 
      IfThen(tk == 4,-0.99631749683685612381218420655*GFOffset(du1,0,0,-5) + 
      0.99631749683685612381218420655*GFOffset(du1,0,0,3),0) + IfThen(tk == 
      5,1.2442513811343767849692089999*GFOffset(du1,0,0,-6) - 
      1.2442513811343767849692089999*GFOffset(du1,0,0,2),0) + IfThen(tk == 
      6,-3.*GFOffset(du1,0,0,-7) - 18.*GFOffset(du1,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDdu21 CCTK_ATTRIBUTE_UNUSED = 
      0.285714285714285714285714285714*(IfThen(ti == 
      0,-1.5*GFOffset(du2,0,0,0) + 
      14.201576602919816167081697726*GFOffset(du2,1,0,0) - 
      5.6689852255455078785754090169*GFOffset(du2,2,0,0) + 
      3.2*GFOffset(du2,3,0,0) - 
      2.0499648130767427769623871824*GFOffset(du2,4,0,0) + 
      1.3173734357024344884560984733*GFOffset(du2,5,0,0) + 
      1.*GFOffset(du2,6,0,0),0) + IfThen(ti == 
      1,-1.8208003236771012201589305835*GFOffset(du2,-1,0,0) + 
      3.4558282142942851326012978916*GFOffset(du2,1,0,0) - 
      1.5986066880983668371678581959*GFOffset(du2,2,0,0) + 
      0.96133979728871166671127688438*GFOffset(du2,3,0,0) - 
      0.60224717963578568466640341307*GFOffset(du2,4,0,0) - 
      0.3955138201717430573193825835*GFOffset(du2,5,0,0),0) + IfThen(ti == 
      2,0.12709791709691405234867282716*GFOffset(du2,-2,0,0) - 
      2.2158042831699713136380303455*GFOffset(du2,-1,0,0) + 
      2.2666980870859990122084630061*GFOffset(du2,1,0,0) - 
      1.06644190400637468973550663446*GFOffset(du2,2,0,0) + 
      0.61639083551757952864611929469*GFOffset(du2,3,0,0) + 
      0.27205934747585341017028185199*GFOffset(du2,4,0,0),0) + IfThen(ti == 
      3,0.15625*GFOffset(du2,-3,0,0) + 
      0.9075444712688209188202794332*GFOffset(du2,-2,0,0) - 
      2.00696924058875308957204950201*GFOffset(du2,-1,0,0) + 
      2.00696924058875308957204950201*GFOffset(du2,1,0,0) - 
      0.9075444712688209188202794332*GFOffset(du2,2,0,0) - 
      0.15625*GFOffset(du2,3,0,0),0) + IfThen(ti == 
      4,-0.27205934747585341017028185199*GFOffset(du2,-4,0,0) - 
      0.61639083551757952864611929469*GFOffset(du2,-3,0,0) + 
      1.06644190400637468973550663446*GFOffset(du2,-2,0,0) - 
      2.2666980870859990122084630061*GFOffset(du2,-1,0,0) + 
      2.2158042831699713136380303455*GFOffset(du2,1,0,0) - 
      0.12709791709691405234867282716*GFOffset(du2,2,0,0),0) + IfThen(ti == 
      5,0.3955138201717430573193825835*GFOffset(du2,-5,0,0) + 
      0.60224717963578568466640341307*GFOffset(du2,-4,0,0) - 
      0.96133979728871166671127688438*GFOffset(du2,-3,0,0) + 
      1.5986066880983668371678581959*GFOffset(du2,-2,0,0) - 
      3.4558282142942851326012978916*GFOffset(du2,-1,0,0) + 
      1.8208003236771012201589305835*GFOffset(du2,1,0,0),0) + IfThen(ti == 
      6,-1.*GFOffset(du2,-6,0,0) - 
      1.3173734357024344884560984733*GFOffset(du2,-5,0,0) + 
      2.0499648130767427769623871824*GFOffset(du2,-4,0,0) - 
      3.2*GFOffset(du2,-3,0,0) + 
      5.6689852255455078785754090169*GFOffset(du2,-2,0,0) - 
      14.201576602919816167081697726*GFOffset(du2,-1,0,0) + 
      1.5*GFOffset(du2,0,0,0),0))*pow(dx,-1) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(ti == 
      0,18.*GFOffset(du2,-1,0,0) + 3.*GFOffset(du2,7,0,0),0) + IfThen(ti == 
      1,1.2442513811343767849692089999*GFOffset(du2,-2,0,0) - 
      1.2442513811343767849692089999*GFOffset(du2,6,0,0),0) + IfThen(ti == 
      2,-0.99631749683685612381218420655*GFOffset(du2,-3,0,0) + 
      0.99631749683685612381218420655*GFOffset(du2,5,0,0),0) + IfThen(ti == 
      3,0.9375*GFOffset(du2,-4,0,0) - 0.9375*GFOffset(du2,4,0,0),0) + 
      IfThen(ti == 4,-0.99631749683685612381218420655*GFOffset(du2,-5,0,0) + 
      0.99631749683685612381218420655*GFOffset(du2,3,0,0),0) + IfThen(ti == 
      5,1.2442513811343767849692089999*GFOffset(du2,-6,0,0) - 
      1.2442513811343767849692089999*GFOffset(du2,2,0,0),0) + IfThen(ti == 
      6,-3.*GFOffset(du2,-7,0,0) - 18.*GFOffset(du2,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDdu22 CCTK_ATTRIBUTE_UNUSED = 
      0.285714285714285714285714285714*(IfThen(tj == 
      0,-1.5*GFOffset(du2,0,0,0) + 
      14.201576602919816167081697726*GFOffset(du2,0,1,0) - 
      5.6689852255455078785754090169*GFOffset(du2,0,2,0) + 
      3.2*GFOffset(du2,0,3,0) - 
      2.0499648130767427769623871824*GFOffset(du2,0,4,0) + 
      1.3173734357024344884560984733*GFOffset(du2,0,5,0) + 
      1.*GFOffset(du2,0,6,0),0) + IfThen(tj == 
      1,-1.8208003236771012201589305835*GFOffset(du2,0,-1,0) + 
      3.4558282142942851326012978916*GFOffset(du2,0,1,0) - 
      1.5986066880983668371678581959*GFOffset(du2,0,2,0) + 
      0.96133979728871166671127688438*GFOffset(du2,0,3,0) - 
      0.60224717963578568466640341307*GFOffset(du2,0,4,0) - 
      0.3955138201717430573193825835*GFOffset(du2,0,5,0),0) + IfThen(tj == 
      2,0.12709791709691405234867282716*GFOffset(du2,0,-2,0) - 
      2.2158042831699713136380303455*GFOffset(du2,0,-1,0) + 
      2.2666980870859990122084630061*GFOffset(du2,0,1,0) - 
      1.06644190400637468973550663446*GFOffset(du2,0,2,0) + 
      0.61639083551757952864611929469*GFOffset(du2,0,3,0) + 
      0.27205934747585341017028185199*GFOffset(du2,0,4,0),0) + IfThen(tj == 
      3,0.15625*GFOffset(du2,0,-3,0) + 
      0.9075444712688209188202794332*GFOffset(du2,0,-2,0) - 
      2.00696924058875308957204950201*GFOffset(du2,0,-1,0) + 
      2.00696924058875308957204950201*GFOffset(du2,0,1,0) - 
      0.9075444712688209188202794332*GFOffset(du2,0,2,0) - 
      0.15625*GFOffset(du2,0,3,0),0) + IfThen(tj == 
      4,-0.27205934747585341017028185199*GFOffset(du2,0,-4,0) - 
      0.61639083551757952864611929469*GFOffset(du2,0,-3,0) + 
      1.06644190400637468973550663446*GFOffset(du2,0,-2,0) - 
      2.2666980870859990122084630061*GFOffset(du2,0,-1,0) + 
      2.2158042831699713136380303455*GFOffset(du2,0,1,0) - 
      0.12709791709691405234867282716*GFOffset(du2,0,2,0),0) + IfThen(tj == 
      5,0.3955138201717430573193825835*GFOffset(du2,0,-5,0) + 
      0.60224717963578568466640341307*GFOffset(du2,0,-4,0) - 
      0.96133979728871166671127688438*GFOffset(du2,0,-3,0) + 
      1.5986066880983668371678581959*GFOffset(du2,0,-2,0) - 
      3.4558282142942851326012978916*GFOffset(du2,0,-1,0) + 
      1.8208003236771012201589305835*GFOffset(du2,0,1,0),0) + IfThen(tj == 
      6,-1.*GFOffset(du2,0,-6,0) - 
      1.3173734357024344884560984733*GFOffset(du2,0,-5,0) + 
      2.0499648130767427769623871824*GFOffset(du2,0,-4,0) - 
      3.2*GFOffset(du2,0,-3,0) + 
      5.6689852255455078785754090169*GFOffset(du2,0,-2,0) - 
      14.201576602919816167081697726*GFOffset(du2,0,-1,0) + 
      1.5*GFOffset(du2,0,0,0),0))*pow(dy,-1) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tj == 
      0,18.*GFOffset(du2,0,-1,0) + 3.*GFOffset(du2,0,7,0),0) + IfThen(tj == 
      1,1.2442513811343767849692089999*GFOffset(du2,0,-2,0) - 
      1.2442513811343767849692089999*GFOffset(du2,0,6,0),0) + IfThen(tj == 
      2,-0.99631749683685612381218420655*GFOffset(du2,0,-3,0) + 
      0.99631749683685612381218420655*GFOffset(du2,0,5,0),0) + IfThen(tj == 
      3,0.9375*GFOffset(du2,0,-4,0) - 0.9375*GFOffset(du2,0,4,0),0) + 
      IfThen(tj == 4,-0.99631749683685612381218420655*GFOffset(du2,0,-5,0) + 
      0.99631749683685612381218420655*GFOffset(du2,0,3,0),0) + IfThen(tj == 
      5,1.2442513811343767849692089999*GFOffset(du2,0,-6,0) - 
      1.2442513811343767849692089999*GFOffset(du2,0,2,0),0) + IfThen(tj == 
      6,-3.*GFOffset(du2,0,-7,0) - 18.*GFOffset(du2,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDdu23 CCTK_ATTRIBUTE_UNUSED = 
      0.285714285714285714285714285714*(IfThen(tk == 
      0,-1.5*GFOffset(du2,0,0,0) + 
      14.201576602919816167081697726*GFOffset(du2,0,0,1) - 
      5.6689852255455078785754090169*GFOffset(du2,0,0,2) + 
      3.2*GFOffset(du2,0,0,3) - 
      2.0499648130767427769623871824*GFOffset(du2,0,0,4) + 
      1.3173734357024344884560984733*GFOffset(du2,0,0,5) + 
      1.*GFOffset(du2,0,0,6),0) + IfThen(tk == 
      1,-1.8208003236771012201589305835*GFOffset(du2,0,0,-1) + 
      3.4558282142942851326012978916*GFOffset(du2,0,0,1) - 
      1.5986066880983668371678581959*GFOffset(du2,0,0,2) + 
      0.96133979728871166671127688438*GFOffset(du2,0,0,3) - 
      0.60224717963578568466640341307*GFOffset(du2,0,0,4) - 
      0.3955138201717430573193825835*GFOffset(du2,0,0,5),0) + IfThen(tk == 
      2,0.12709791709691405234867282716*GFOffset(du2,0,0,-2) - 
      2.2158042831699713136380303455*GFOffset(du2,0,0,-1) + 
      2.2666980870859990122084630061*GFOffset(du2,0,0,1) - 
      1.06644190400637468973550663446*GFOffset(du2,0,0,2) + 
      0.61639083551757952864611929469*GFOffset(du2,0,0,3) + 
      0.27205934747585341017028185199*GFOffset(du2,0,0,4),0) + IfThen(tk == 
      3,0.15625*GFOffset(du2,0,0,-3) + 
      0.9075444712688209188202794332*GFOffset(du2,0,0,-2) - 
      2.00696924058875308957204950201*GFOffset(du2,0,0,-1) + 
      2.00696924058875308957204950201*GFOffset(du2,0,0,1) - 
      0.9075444712688209188202794332*GFOffset(du2,0,0,2) - 
      0.15625*GFOffset(du2,0,0,3),0) + IfThen(tk == 
      4,-0.27205934747585341017028185199*GFOffset(du2,0,0,-4) - 
      0.61639083551757952864611929469*GFOffset(du2,0,0,-3) + 
      1.06644190400637468973550663446*GFOffset(du2,0,0,-2) - 
      2.2666980870859990122084630061*GFOffset(du2,0,0,-1) + 
      2.2158042831699713136380303455*GFOffset(du2,0,0,1) - 
      0.12709791709691405234867282716*GFOffset(du2,0,0,2),0) + IfThen(tk == 
      5,0.3955138201717430573193825835*GFOffset(du2,0,0,-5) + 
      0.60224717963578568466640341307*GFOffset(du2,0,0,-4) - 
      0.96133979728871166671127688438*GFOffset(du2,0,0,-3) + 
      1.5986066880983668371678581959*GFOffset(du2,0,0,-2) - 
      3.4558282142942851326012978916*GFOffset(du2,0,0,-1) + 
      1.8208003236771012201589305835*GFOffset(du2,0,0,1),0) + IfThen(tk == 
      6,-1.*GFOffset(du2,0,0,-6) - 
      1.3173734357024344884560984733*GFOffset(du2,0,0,-5) + 
      2.0499648130767427769623871824*GFOffset(du2,0,0,-4) - 
      3.2*GFOffset(du2,0,0,-3) + 
      5.6689852255455078785754090169*GFOffset(du2,0,0,-2) - 
      14.201576602919816167081697726*GFOffset(du2,0,0,-1) + 
      1.5*GFOffset(du2,0,0,0),0))*pow(dz,-1) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tk == 
      0,18.*GFOffset(du2,0,0,-1) + 3.*GFOffset(du2,0,0,7),0) + IfThen(tk == 
      1,1.2442513811343767849692089999*GFOffset(du2,0,0,-2) - 
      1.2442513811343767849692089999*GFOffset(du2,0,0,6),0) + IfThen(tk == 
      2,-0.99631749683685612381218420655*GFOffset(du2,0,0,-3) + 
      0.99631749683685612381218420655*GFOffset(du2,0,0,5),0) + IfThen(tk == 
      3,0.9375*GFOffset(du2,0,0,-4) - 0.9375*GFOffset(du2,0,0,4),0) + 
      IfThen(tk == 4,-0.99631749683685612381218420655*GFOffset(du2,0,0,-5) + 
      0.99631749683685612381218420655*GFOffset(du2,0,0,3),0) + IfThen(tk == 
      5,1.2442513811343767849692089999*GFOffset(du2,0,0,-6) - 
      1.2442513811343767849692089999*GFOffset(du2,0,0,2),0) + IfThen(tk == 
      6,-3.*GFOffset(du2,0,0,-7) - 18.*GFOffset(du2,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDdu31 CCTK_ATTRIBUTE_UNUSED = 
      0.285714285714285714285714285714*(IfThen(ti == 
      0,-1.5*GFOffset(du3,0,0,0) + 
      14.201576602919816167081697726*GFOffset(du3,1,0,0) - 
      5.6689852255455078785754090169*GFOffset(du3,2,0,0) + 
      3.2*GFOffset(du3,3,0,0) - 
      2.0499648130767427769623871824*GFOffset(du3,4,0,0) + 
      1.3173734357024344884560984733*GFOffset(du3,5,0,0) + 
      1.*GFOffset(du3,6,0,0),0) + IfThen(ti == 
      1,-1.8208003236771012201589305835*GFOffset(du3,-1,0,0) + 
      3.4558282142942851326012978916*GFOffset(du3,1,0,0) - 
      1.5986066880983668371678581959*GFOffset(du3,2,0,0) + 
      0.96133979728871166671127688438*GFOffset(du3,3,0,0) - 
      0.60224717963578568466640341307*GFOffset(du3,4,0,0) - 
      0.3955138201717430573193825835*GFOffset(du3,5,0,0),0) + IfThen(ti == 
      2,0.12709791709691405234867282716*GFOffset(du3,-2,0,0) - 
      2.2158042831699713136380303455*GFOffset(du3,-1,0,0) + 
      2.2666980870859990122084630061*GFOffset(du3,1,0,0) - 
      1.06644190400637468973550663446*GFOffset(du3,2,0,0) + 
      0.61639083551757952864611929469*GFOffset(du3,3,0,0) + 
      0.27205934747585341017028185199*GFOffset(du3,4,0,0),0) + IfThen(ti == 
      3,0.15625*GFOffset(du3,-3,0,0) + 
      0.9075444712688209188202794332*GFOffset(du3,-2,0,0) - 
      2.00696924058875308957204950201*GFOffset(du3,-1,0,0) + 
      2.00696924058875308957204950201*GFOffset(du3,1,0,0) - 
      0.9075444712688209188202794332*GFOffset(du3,2,0,0) - 
      0.15625*GFOffset(du3,3,0,0),0) + IfThen(ti == 
      4,-0.27205934747585341017028185199*GFOffset(du3,-4,0,0) - 
      0.61639083551757952864611929469*GFOffset(du3,-3,0,0) + 
      1.06644190400637468973550663446*GFOffset(du3,-2,0,0) - 
      2.2666980870859990122084630061*GFOffset(du3,-1,0,0) + 
      2.2158042831699713136380303455*GFOffset(du3,1,0,0) - 
      0.12709791709691405234867282716*GFOffset(du3,2,0,0),0) + IfThen(ti == 
      5,0.3955138201717430573193825835*GFOffset(du3,-5,0,0) + 
      0.60224717963578568466640341307*GFOffset(du3,-4,0,0) - 
      0.96133979728871166671127688438*GFOffset(du3,-3,0,0) + 
      1.5986066880983668371678581959*GFOffset(du3,-2,0,0) - 
      3.4558282142942851326012978916*GFOffset(du3,-1,0,0) + 
      1.8208003236771012201589305835*GFOffset(du3,1,0,0),0) + IfThen(ti == 
      6,-1.*GFOffset(du3,-6,0,0) - 
      1.3173734357024344884560984733*GFOffset(du3,-5,0,0) + 
      2.0499648130767427769623871824*GFOffset(du3,-4,0,0) - 
      3.2*GFOffset(du3,-3,0,0) + 
      5.6689852255455078785754090169*GFOffset(du3,-2,0,0) - 
      14.201576602919816167081697726*GFOffset(du3,-1,0,0) + 
      1.5*GFOffset(du3,0,0,0),0))*pow(dx,-1) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(ti == 
      0,18.*GFOffset(du3,-1,0,0) + 3.*GFOffset(du3,7,0,0),0) + IfThen(ti == 
      1,1.2442513811343767849692089999*GFOffset(du3,-2,0,0) - 
      1.2442513811343767849692089999*GFOffset(du3,6,0,0),0) + IfThen(ti == 
      2,-0.99631749683685612381218420655*GFOffset(du3,-3,0,0) + 
      0.99631749683685612381218420655*GFOffset(du3,5,0,0),0) + IfThen(ti == 
      3,0.9375*GFOffset(du3,-4,0,0) - 0.9375*GFOffset(du3,4,0,0),0) + 
      IfThen(ti == 4,-0.99631749683685612381218420655*GFOffset(du3,-5,0,0) + 
      0.99631749683685612381218420655*GFOffset(du3,3,0,0),0) + IfThen(ti == 
      5,1.2442513811343767849692089999*GFOffset(du3,-6,0,0) - 
      1.2442513811343767849692089999*GFOffset(du3,2,0,0),0) + IfThen(ti == 
      6,-3.*GFOffset(du3,-7,0,0) - 18.*GFOffset(du3,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDdu32 CCTK_ATTRIBUTE_UNUSED = 
      0.285714285714285714285714285714*(IfThen(tj == 
      0,-1.5*GFOffset(du3,0,0,0) + 
      14.201576602919816167081697726*GFOffset(du3,0,1,0) - 
      5.6689852255455078785754090169*GFOffset(du3,0,2,0) + 
      3.2*GFOffset(du3,0,3,0) - 
      2.0499648130767427769623871824*GFOffset(du3,0,4,0) + 
      1.3173734357024344884560984733*GFOffset(du3,0,5,0) + 
      1.*GFOffset(du3,0,6,0),0) + IfThen(tj == 
      1,-1.8208003236771012201589305835*GFOffset(du3,0,-1,0) + 
      3.4558282142942851326012978916*GFOffset(du3,0,1,0) - 
      1.5986066880983668371678581959*GFOffset(du3,0,2,0) + 
      0.96133979728871166671127688438*GFOffset(du3,0,3,0) - 
      0.60224717963578568466640341307*GFOffset(du3,0,4,0) - 
      0.3955138201717430573193825835*GFOffset(du3,0,5,0),0) + IfThen(tj == 
      2,0.12709791709691405234867282716*GFOffset(du3,0,-2,0) - 
      2.2158042831699713136380303455*GFOffset(du3,0,-1,0) + 
      2.2666980870859990122084630061*GFOffset(du3,0,1,0) - 
      1.06644190400637468973550663446*GFOffset(du3,0,2,0) + 
      0.61639083551757952864611929469*GFOffset(du3,0,3,0) + 
      0.27205934747585341017028185199*GFOffset(du3,0,4,0),0) + IfThen(tj == 
      3,0.15625*GFOffset(du3,0,-3,0) + 
      0.9075444712688209188202794332*GFOffset(du3,0,-2,0) - 
      2.00696924058875308957204950201*GFOffset(du3,0,-1,0) + 
      2.00696924058875308957204950201*GFOffset(du3,0,1,0) - 
      0.9075444712688209188202794332*GFOffset(du3,0,2,0) - 
      0.15625*GFOffset(du3,0,3,0),0) + IfThen(tj == 
      4,-0.27205934747585341017028185199*GFOffset(du3,0,-4,0) - 
      0.61639083551757952864611929469*GFOffset(du3,0,-3,0) + 
      1.06644190400637468973550663446*GFOffset(du3,0,-2,0) - 
      2.2666980870859990122084630061*GFOffset(du3,0,-1,0) + 
      2.2158042831699713136380303455*GFOffset(du3,0,1,0) - 
      0.12709791709691405234867282716*GFOffset(du3,0,2,0),0) + IfThen(tj == 
      5,0.3955138201717430573193825835*GFOffset(du3,0,-5,0) + 
      0.60224717963578568466640341307*GFOffset(du3,0,-4,0) - 
      0.96133979728871166671127688438*GFOffset(du3,0,-3,0) + 
      1.5986066880983668371678581959*GFOffset(du3,0,-2,0) - 
      3.4558282142942851326012978916*GFOffset(du3,0,-1,0) + 
      1.8208003236771012201589305835*GFOffset(du3,0,1,0),0) + IfThen(tj == 
      6,-1.*GFOffset(du3,0,-6,0) - 
      1.3173734357024344884560984733*GFOffset(du3,0,-5,0) + 
      2.0499648130767427769623871824*GFOffset(du3,0,-4,0) - 
      3.2*GFOffset(du3,0,-3,0) + 
      5.6689852255455078785754090169*GFOffset(du3,0,-2,0) - 
      14.201576602919816167081697726*GFOffset(du3,0,-1,0) + 
      1.5*GFOffset(du3,0,0,0),0))*pow(dy,-1) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tj == 
      0,18.*GFOffset(du3,0,-1,0) + 3.*GFOffset(du3,0,7,0),0) + IfThen(tj == 
      1,1.2442513811343767849692089999*GFOffset(du3,0,-2,0) - 
      1.2442513811343767849692089999*GFOffset(du3,0,6,0),0) + IfThen(tj == 
      2,-0.99631749683685612381218420655*GFOffset(du3,0,-3,0) + 
      0.99631749683685612381218420655*GFOffset(du3,0,5,0),0) + IfThen(tj == 
      3,0.9375*GFOffset(du3,0,-4,0) - 0.9375*GFOffset(du3,0,4,0),0) + 
      IfThen(tj == 4,-0.99631749683685612381218420655*GFOffset(du3,0,-5,0) + 
      0.99631749683685612381218420655*GFOffset(du3,0,3,0),0) + IfThen(tj == 
      5,1.2442513811343767849692089999*GFOffset(du3,0,-6,0) - 
      1.2442513811343767849692089999*GFOffset(du3,0,2,0),0) + IfThen(tj == 
      6,-3.*GFOffset(du3,0,-7,0) - 18.*GFOffset(du3,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDdu33 CCTK_ATTRIBUTE_UNUSED = 
      0.285714285714285714285714285714*(IfThen(tk == 
      0,-1.5*GFOffset(du3,0,0,0) + 
      14.201576602919816167081697726*GFOffset(du3,0,0,1) - 
      5.6689852255455078785754090169*GFOffset(du3,0,0,2) + 
      3.2*GFOffset(du3,0,0,3) - 
      2.0499648130767427769623871824*GFOffset(du3,0,0,4) + 
      1.3173734357024344884560984733*GFOffset(du3,0,0,5) + 
      1.*GFOffset(du3,0,0,6),0) + IfThen(tk == 
      1,-1.8208003236771012201589305835*GFOffset(du3,0,0,-1) + 
      3.4558282142942851326012978916*GFOffset(du3,0,0,1) - 
      1.5986066880983668371678581959*GFOffset(du3,0,0,2) + 
      0.96133979728871166671127688438*GFOffset(du3,0,0,3) - 
      0.60224717963578568466640341307*GFOffset(du3,0,0,4) - 
      0.3955138201717430573193825835*GFOffset(du3,0,0,5),0) + IfThen(tk == 
      2,0.12709791709691405234867282716*GFOffset(du3,0,0,-2) - 
      2.2158042831699713136380303455*GFOffset(du3,0,0,-1) + 
      2.2666980870859990122084630061*GFOffset(du3,0,0,1) - 
      1.06644190400637468973550663446*GFOffset(du3,0,0,2) + 
      0.61639083551757952864611929469*GFOffset(du3,0,0,3) + 
      0.27205934747585341017028185199*GFOffset(du3,0,0,4),0) + IfThen(tk == 
      3,0.15625*GFOffset(du3,0,0,-3) + 
      0.9075444712688209188202794332*GFOffset(du3,0,0,-2) - 
      2.00696924058875308957204950201*GFOffset(du3,0,0,-1) + 
      2.00696924058875308957204950201*GFOffset(du3,0,0,1) - 
      0.9075444712688209188202794332*GFOffset(du3,0,0,2) - 
      0.15625*GFOffset(du3,0,0,3),0) + IfThen(tk == 
      4,-0.27205934747585341017028185199*GFOffset(du3,0,0,-4) - 
      0.61639083551757952864611929469*GFOffset(du3,0,0,-3) + 
      1.06644190400637468973550663446*GFOffset(du3,0,0,-2) - 
      2.2666980870859990122084630061*GFOffset(du3,0,0,-1) + 
      2.2158042831699713136380303455*GFOffset(du3,0,0,1) - 
      0.12709791709691405234867282716*GFOffset(du3,0,0,2),0) + IfThen(tk == 
      5,0.3955138201717430573193825835*GFOffset(du3,0,0,-5) + 
      0.60224717963578568466640341307*GFOffset(du3,0,0,-4) - 
      0.96133979728871166671127688438*GFOffset(du3,0,0,-3) + 
      1.5986066880983668371678581959*GFOffset(du3,0,0,-2) - 
      3.4558282142942851326012978916*GFOffset(du3,0,0,-1) + 
      1.8208003236771012201589305835*GFOffset(du3,0,0,1),0) + IfThen(tk == 
      6,-1.*GFOffset(du3,0,0,-6) - 
      1.3173734357024344884560984733*GFOffset(du3,0,0,-5) + 
      2.0499648130767427769623871824*GFOffset(du3,0,0,-4) - 
      3.2*GFOffset(du3,0,0,-3) + 
      5.6689852255455078785754090169*GFOffset(du3,0,0,-2) - 
      14.201576602919816167081697726*GFOffset(du3,0,0,-1) + 
      1.5*GFOffset(du3,0,0,0),0))*pow(dz,-1) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tk == 
      0,18.*GFOffset(du3,0,0,-1) + 3.*GFOffset(du3,0,0,7),0) + IfThen(tk == 
      1,1.2442513811343767849692089999*GFOffset(du3,0,0,-2) - 
      1.2442513811343767849692089999*GFOffset(du3,0,0,6),0) + IfThen(tk == 
      2,-0.99631749683685612381218420655*GFOffset(du3,0,0,-3) + 
      0.99631749683685612381218420655*GFOffset(du3,0,0,5),0) + IfThen(tk == 
      3,0.9375*GFOffset(du3,0,0,-4) - 0.9375*GFOffset(du3,0,0,4),0) + 
      IfThen(tk == 4,-0.99631749683685612381218420655*GFOffset(du3,0,0,-5) + 
      0.99631749683685612381218420655*GFOffset(du3,0,0,3),0) + IfThen(tk == 
      5,1.2442513811343767849692089999*GFOffset(du3,0,0,-6) - 
      1.2442513811343767849692089999*GFOffset(du3,0,0,2),0) + IfThen(tk == 
      6,-3.*GFOffset(du3,0,0,-7) - 18.*GFOffset(du3,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDdu11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDdu22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDdu33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDdu11 = J11L*LDdu11 + J21L*LDdu12 + J31L*LDdu13;
      
      PDdu22 = J12L*LDdu21 + J22L*LDdu22 + J32L*LDdu23;
      
      PDdu33 = J13L*LDdu31 + J23L*LDdu32 + J33L*LDdu33;
    }
    else
    {
      PDdu11 = LDdu11;
      
      PDdu22 = LDdu22;
      
      PDdu33 = LDdu33;
    }
    
    CCTK_REAL urhsL CCTK_ATTRIBUTE_UNUSED = rhoL + 
      IfThen(usejacobian,myDetJL*(epsDiss*(IfThen(ti == 
      0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(u,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(u,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(u,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(u,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(u,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(u,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(u,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(u,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(u,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(u,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(u,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(u,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(u,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(u,4,0,0),0) + IfThen(ti == 
      3,0.0446428403421747932239219136542*GFOffset(u,-3,0,0) - 
      0.107637859609505734449271251484*GFOffset(u,-2,0,0) + 
      0.134423622953212481092562120757*GFOffset(u,-1,0,0) - 
      0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(u,1,0,0) - 
      0.107637859609505734449271251484*GFOffset(u,2,0,0) + 
      0.0446428403421747932239219136542*GFOffset(u,3,0,0),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(u,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(u,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(u,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(u,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(u,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(u,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(u,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(u,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(u,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(u,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(u,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(u,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(u,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(u,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(u,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(u,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(u,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(u,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(u,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(u,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(u,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(u,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(u,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(u,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(u,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(u,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(u,0,4,0),0) + IfThen(tj == 
      3,0.0446428403421747932239219136542*GFOffset(u,0,-3,0) - 
      0.107637859609505734449271251484*GFOffset(u,0,-2,0) + 
      0.134423622953212481092562120757*GFOffset(u,0,-1,0) - 
      0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(u,0,1,0) - 
      0.107637859609505734449271251484*GFOffset(u,0,2,0) + 
      0.0446428403421747932239219136542*GFOffset(u,0,3,0),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(u,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(u,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(u,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(u,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(u,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(u,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(u,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(u,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(u,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(u,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(u,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(u,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(u,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(u,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(u,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(u,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(u,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(u,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(u,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(u,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(u,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(u,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(u,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(u,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(u,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(u,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(u,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(u,0,0,4),0) + IfThen(tk == 
      3,0.0446428403421747932239219136542*GFOffset(u,0,0,-3) - 
      0.107637859609505734449271251484*GFOffset(u,0,0,-2) + 
      0.134423622953212481092562120757*GFOffset(u,0,0,-1) - 
      0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(u,0,0,1) - 
      0.107637859609505734449271251484*GFOffset(u,0,0,2) + 
      0.0446428403421747932239219136542*GFOffset(u,0,0,3),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(u,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(u,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(u,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(u,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(u,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(u,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(u,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(u,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(u,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(u,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(u,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(u,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(u,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(u,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(u,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(u,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dz,-1)),epsDiss*(IfThen(ti 
      == 0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(u,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(u,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(u,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(u,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(u,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(u,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(u,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(u,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(u,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(u,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(u,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(u,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(u,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(u,4,0,0),0) + IfThen(ti == 
      3,0.0446428403421747932239219136542*GFOffset(u,-3,0,0) - 
      0.107637859609505734449271251484*GFOffset(u,-2,0,0) + 
      0.134423622953212481092562120757*GFOffset(u,-1,0,0) - 
      0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(u,1,0,0) - 
      0.107637859609505734449271251484*GFOffset(u,2,0,0) + 
      0.0446428403421747932239219136542*GFOffset(u,3,0,0),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(u,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(u,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(u,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(u,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(u,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(u,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(u,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(u,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(u,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(u,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(u,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(u,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(u,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(u,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(u,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(u,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(u,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(u,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(u,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(u,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(u,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(u,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(u,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(u,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(u,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(u,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(u,0,4,0),0) + IfThen(tj == 
      3,0.0446428403421747932239219136542*GFOffset(u,0,-3,0) - 
      0.107637859609505734449271251484*GFOffset(u,0,-2,0) + 
      0.134423622953212481092562120757*GFOffset(u,0,-1,0) - 
      0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(u,0,1,0) - 
      0.107637859609505734449271251484*GFOffset(u,0,2,0) + 
      0.0446428403421747932239219136542*GFOffset(u,0,3,0),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(u,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(u,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(u,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(u,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(u,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(u,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(u,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(u,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(u,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(u,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(u,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(u,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(u,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(u,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(u,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(u,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(u,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(u,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(u,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(u,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(u,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(u,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(u,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(u,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(u,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(u,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(u,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(u,0,0,4),0) + IfThen(tk == 
      3,0.0446428403421747932239219136542*GFOffset(u,0,0,-3) - 
      0.107637859609505734449271251484*GFOffset(u,0,0,-2) + 
      0.134423622953212481092562120757*GFOffset(u,0,0,-1) - 
      0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(u,0,0,1) - 
      0.107637859609505734449271251484*GFOffset(u,0,0,2) + 
      0.0446428403421747932239219136542*GFOffset(u,0,0,3),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(u,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(u,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(u,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(u,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(u,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(u,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(u,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(u,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(u,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(u,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(u,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(u,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(u,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(u,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(u,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(u,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dz,-1));
    
    CCTK_REAL rhorhsL CCTK_ATTRIBUTE_UNUSED = PDdu11 + PDdu22 + PDdu33 + 
      IfThen(usejacobian,myDetJL*(epsDiss*(IfThen(ti == 
      0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(rho,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(rho,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(rho,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(rho,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(rho,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(rho,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(rho,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(rho,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(rho,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(rho,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(rho,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(rho,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      3,0.0446428403421747932239219136542*GFOffset(rho,-3,0,0) - 
      0.107637859609505734449271251484*GFOffset(rho,-2,0,0) + 
      0.134423622953212481092562120757*GFOffset(rho,-1,0,0) - 
      0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(rho,1,0,0) - 
      0.107637859609505734449271251484*GFOffset(rho,2,0,0) + 
      0.0446428403421747932239219136542*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(rho,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(rho,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(rho,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(rho,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(rho,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(rho,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(rho,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(rho,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(rho,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(rho,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(rho,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(rho,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(rho,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(rho,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(rho,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(rho,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(rho,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(rho,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(rho,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(rho,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(rho,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(rho,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(rho,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      3,0.0446428403421747932239219136542*GFOffset(rho,0,-3,0) - 
      0.107637859609505734449271251484*GFOffset(rho,0,-2,0) + 
      0.134423622953212481092562120757*GFOffset(rho,0,-1,0) - 
      0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(rho,0,1,0) - 
      0.107637859609505734449271251484*GFOffset(rho,0,2,0) + 
      0.0446428403421747932239219136542*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(rho,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(rho,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(rho,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(rho,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(rho,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(rho,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(rho,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(rho,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(rho,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(rho,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(rho,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(rho,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(rho,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(rho,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(rho,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(rho,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(rho,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(rho,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(rho,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(rho,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(rho,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(rho,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(rho,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(rho,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      3,0.0446428403421747932239219136542*GFOffset(rho,0,0,-3) - 
      0.107637859609505734449271251484*GFOffset(rho,0,0,-2) + 
      0.134423622953212481092562120757*GFOffset(rho,0,0,-1) - 
      0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(rho,0,0,1) - 
      0.107637859609505734449271251484*GFOffset(rho,0,0,2) + 
      0.0446428403421747932239219136542*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(rho,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(rho,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(rho,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(rho,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(rho,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(rho,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(rho,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(rho,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(rho,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(rho,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(rho,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(rho,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(rho,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(rho,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dz,-1)),epsDiss*(IfThen(ti 
      == 0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(rho,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(rho,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(rho,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(rho,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(rho,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(rho,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(rho,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(rho,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(rho,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(rho,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(rho,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(rho,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      3,0.0446428403421747932239219136542*GFOffset(rho,-3,0,0) - 
      0.107637859609505734449271251484*GFOffset(rho,-2,0,0) + 
      0.134423622953212481092562120757*GFOffset(rho,-1,0,0) - 
      0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(rho,1,0,0) - 
      0.107637859609505734449271251484*GFOffset(rho,2,0,0) + 
      0.0446428403421747932239219136542*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(rho,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(rho,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(rho,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(rho,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(rho,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(rho,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(rho,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(rho,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(rho,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(rho,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(rho,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(rho,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(rho,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(rho,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(rho,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(rho,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(rho,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(rho,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(rho,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(rho,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(rho,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(rho,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(rho,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      3,0.0446428403421747932239219136542*GFOffset(rho,0,-3,0) - 
      0.107637859609505734449271251484*GFOffset(rho,0,-2,0) + 
      0.134423622953212481092562120757*GFOffset(rho,0,-1,0) - 
      0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(rho,0,1,0) - 
      0.107637859609505734449271251484*GFOffset(rho,0,2,0) + 
      0.0446428403421747932239219136542*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(rho,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(rho,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(rho,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(rho,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(rho,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(rho,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(rho,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(rho,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(rho,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(rho,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(rho,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(rho,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(rho,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(rho,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(rho,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(rho,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(rho,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(rho,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(rho,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(rho,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(rho,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(rho,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(rho,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(rho,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      3,0.0446428403421747932239219136542*GFOffset(rho,0,0,-3) - 
      0.107637859609505734449271251484*GFOffset(rho,0,0,-2) + 
      0.134423622953212481092562120757*GFOffset(rho,0,0,-1) - 
      0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*GFOffset(rho,0,0,1) - 
      0.107637859609505734449271251484*GFOffset(rho,0,0,2) + 
      0.0446428403421747932239219136542*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(rho,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(rho,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(rho,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(rho,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(rho,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(rho,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(rho,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(rho,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(rho,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(rho,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(rho,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(rho,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(rho,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(rho,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dz,-1));
    
    CCTK_REAL v1rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL v2rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL v3rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    /* Copy local copies back to grid functions */
    rhorhs[index] = rhorhsL;
    urhs[index] = urhsL;
    v1rhs[index] = v1rhsL;
    v2rhs[index] = v2rhsL;
    v3rhs[index] = v3rhsL;
  }
  CCTK_ENDLOOP3(ML_WaveToy_DG6_RHSSecondOrder);
}
extern "C" void ML_WaveToy_DG6_RHSSecondOrder(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_WaveToy_DG6_RHSSecondOrder_Body");
  }
  if (cctk_iteration % ML_WaveToy_DG6_RHSSecondOrder_calc_every != ML_WaveToy_DG6_RHSSecondOrder_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_WaveToy_DG6::WT_detJ",
    "ML_WaveToy_DG6::WT_du",
    "ML_WaveToy_DG6::WT_rho",
    "ML_WaveToy_DG6::WT_rhorhs",
    "ML_WaveToy_DG6::WT_u",
    "ML_WaveToy_DG6::WT_urhs",
    "ML_WaveToy_DG6::WT_vrhs"};
  AssertGroupStorage(cctkGH, "ML_WaveToy_DG6_RHSSecondOrder", 7, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_WaveToy_DG6_RHSSecondOrder_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_WaveToy_DG6_RHSSecondOrder_Body");
  }
}

} // namespace ML_WaveToy_DG6
