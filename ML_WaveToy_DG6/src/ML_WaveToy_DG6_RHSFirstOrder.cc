/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_WaveToy_DG6 {

extern "C" void ML_WaveToy_DG6_RHSFirstOrder_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_WaveToy_DG6_RHSFirstOrder_calc_every != ML_WaveToy_DG6_RHSFirstOrder_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG6::WT_rhorhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG6::WT_rhorhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG6::WT_urhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG6::WT_urhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG6::WT_vrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG6::WT_vrhs.");
  return;
}

static void ML_WaveToy_DG6_RHSFirstOrder_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_WaveToy_DG6_RHSFirstOrder,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL myDetJL CCTK_ATTRIBUTE_UNUSED = myDetJ[index];
    CCTK_REAL rhoL CCTK_ATTRIBUTE_UNUSED = rho[index];
    CCTK_REAL uL CCTK_ATTRIBUTE_UNUSED = u[index];
    CCTK_REAL v1L CCTK_ATTRIBUTE_UNUSED = v1[index];
    CCTK_REAL v2L CCTK_ATTRIBUTE_UNUSED = v2[index];
    CCTK_REAL v3L CCTK_ATTRIBUTE_UNUSED = v3[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDrho1 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(ti == 
      0,-21.*GFOffset(rho,0,0,0),0) + IfThen(ti == 
      6,21.*GFOffset(rho,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(ti == 
      0,-10.5*GFOffset(rho,0,0,0) + 
      14.201576602919816167081697726*GFOffset(rho,1,0,0) - 
      5.6689852255455078785754090169*GFOffset(rho,2,0,0) + 
      3.2*GFOffset(rho,3,0,0) - 
      2.04996481307674277696238718244*GFOffset(rho,4,0,0) + 
      1.31737343570243448845609847333*GFOffset(rho,5,0,0) - 
      0.5*GFOffset(rho,6,0,0),0) + IfThen(ti == 
      1,-2.44292601424428961264353508344*GFOffset(rho,-1,0,0) + 
      3.45582821429428513260129789155*GFOffset(rho,1,0,0) - 
      1.59860668809836683716785819589*GFOffset(rho,2,0,0) + 
      0.96133979728871166671127688438*GFOffset(rho,3,0,0) - 
      0.60224717963578568466640341307*GFOffset(rho,4,0,0) + 
      0.226611870395445335165221916464*GFOffset(rho,5,0,0),0) + IfThen(ti == 
      2,0.62525666551534211425476493044*GFOffset(rho,-2,0,0) - 
      2.21580428316997131363803034549*GFOffset(rho,-1,0,0) + 
      2.26669808708599901220846300609*GFOffset(rho,1,0,0) - 
      1.06644190400637468973550663446*GFOffset(rho,2,0,0) + 
      0.61639083551757952864611929469*GFOffset(rho,3,0,0) - 
      0.22609940094257465173581025128*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      3,-0.3125*GFOffset(rho,-3,0,0) + 
      0.9075444712688209188202794332*GFOffset(rho,-2,0,0) - 
      2.00696924058875308957204950201*GFOffset(rho,-1,0,0) + 
      2.00696924058875308957204950201*GFOffset(rho,1,0,0) - 
      0.9075444712688209188202794332*GFOffset(rho,2,0,0) + 
      0.3125*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      4,0.22609940094257465173581025128*GFOffset(rho,-4,0,0) - 
      0.61639083551757952864611929469*GFOffset(rho,-3,0,0) + 
      1.06644190400637468973550663446*GFOffset(rho,-2,0,0) - 
      2.26669808708599901220846300609*GFOffset(rho,-1,0,0) + 
      2.21580428316997131363803034549*GFOffset(rho,1,0,0) - 
      0.62525666551534211425476493044*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      5,-0.226611870395445335165221916464*GFOffset(rho,-5,0,0) + 
      0.60224717963578568466640341307*GFOffset(rho,-4,0,0) - 
      0.96133979728871166671127688438*GFOffset(rho,-3,0,0) + 
      1.59860668809836683716785819589*GFOffset(rho,-2,0,0) - 
      3.45582821429428513260129789155*GFOffset(rho,-1,0,0) + 
      2.44292601424428961264353508344*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      6,0.5*GFOffset(rho,-6,0,0) - 
      1.31737343570243448845609847333*GFOffset(rho,-5,0,0) + 
      2.04996481307674277696238718244*GFOffset(rho,-4,0,0) - 
      3.2*GFOffset(rho,-3,0,0) + 
      5.6689852255455078785754090169*GFOffset(rho,-2,0,0) - 
      14.201576602919816167081697726*GFOffset(rho,-1,0,0) + 
      10.5*GFOffset(rho,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(ti == 
      0,21.*GFOffset(rho,-1,0,0),0) + IfThen(ti == 
      6,-21.*GFOffset(rho,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDrho2 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(tj == 
      0,-21.*GFOffset(rho,0,0,0),0) + IfThen(tj == 
      6,21.*GFOffset(rho,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(tj == 
      0,-10.5*GFOffset(rho,0,0,0) + 
      14.201576602919816167081697726*GFOffset(rho,0,1,0) - 
      5.6689852255455078785754090169*GFOffset(rho,0,2,0) + 
      3.2*GFOffset(rho,0,3,0) - 
      2.04996481307674277696238718244*GFOffset(rho,0,4,0) + 
      1.31737343570243448845609847333*GFOffset(rho,0,5,0) - 
      0.5*GFOffset(rho,0,6,0),0) + IfThen(tj == 
      1,-2.44292601424428961264353508344*GFOffset(rho,0,-1,0) + 
      3.45582821429428513260129789155*GFOffset(rho,0,1,0) - 
      1.59860668809836683716785819589*GFOffset(rho,0,2,0) + 
      0.96133979728871166671127688438*GFOffset(rho,0,3,0) - 
      0.60224717963578568466640341307*GFOffset(rho,0,4,0) + 
      0.226611870395445335165221916464*GFOffset(rho,0,5,0),0) + IfThen(tj == 
      2,0.62525666551534211425476493044*GFOffset(rho,0,-2,0) - 
      2.21580428316997131363803034549*GFOffset(rho,0,-1,0) + 
      2.26669808708599901220846300609*GFOffset(rho,0,1,0) - 
      1.06644190400637468973550663446*GFOffset(rho,0,2,0) + 
      0.61639083551757952864611929469*GFOffset(rho,0,3,0) - 
      0.22609940094257465173581025128*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      3,-0.3125*GFOffset(rho,0,-3,0) + 
      0.9075444712688209188202794332*GFOffset(rho,0,-2,0) - 
      2.00696924058875308957204950201*GFOffset(rho,0,-1,0) + 
      2.00696924058875308957204950201*GFOffset(rho,0,1,0) - 
      0.9075444712688209188202794332*GFOffset(rho,0,2,0) + 
      0.3125*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      4,0.22609940094257465173581025128*GFOffset(rho,0,-4,0) - 
      0.61639083551757952864611929469*GFOffset(rho,0,-3,0) + 
      1.06644190400637468973550663446*GFOffset(rho,0,-2,0) - 
      2.26669808708599901220846300609*GFOffset(rho,0,-1,0) + 
      2.21580428316997131363803034549*GFOffset(rho,0,1,0) - 
      0.62525666551534211425476493044*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      5,-0.226611870395445335165221916464*GFOffset(rho,0,-5,0) + 
      0.60224717963578568466640341307*GFOffset(rho,0,-4,0) - 
      0.96133979728871166671127688438*GFOffset(rho,0,-3,0) + 
      1.59860668809836683716785819589*GFOffset(rho,0,-2,0) - 
      3.45582821429428513260129789155*GFOffset(rho,0,-1,0) + 
      2.44292601424428961264353508344*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      6,0.5*GFOffset(rho,0,-6,0) - 
      1.31737343570243448845609847333*GFOffset(rho,0,-5,0) + 
      2.04996481307674277696238718244*GFOffset(rho,0,-4,0) - 
      3.2*GFOffset(rho,0,-3,0) + 
      5.6689852255455078785754090169*GFOffset(rho,0,-2,0) - 
      14.201576602919816167081697726*GFOffset(rho,0,-1,0) + 
      10.5*GFOffset(rho,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tj == 
      0,21.*GFOffset(rho,0,-1,0),0) + IfThen(tj == 
      6,-21.*GFOffset(rho,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDrho3 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(tk == 
      0,-21.*GFOffset(rho,0,0,0),0) + IfThen(tk == 
      6,21.*GFOffset(rho,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(tk == 
      0,-10.5*GFOffset(rho,0,0,0) + 
      14.201576602919816167081697726*GFOffset(rho,0,0,1) - 
      5.6689852255455078785754090169*GFOffset(rho,0,0,2) + 
      3.2*GFOffset(rho,0,0,3) - 
      2.04996481307674277696238718244*GFOffset(rho,0,0,4) + 
      1.31737343570243448845609847333*GFOffset(rho,0,0,5) - 
      0.5*GFOffset(rho,0,0,6),0) + IfThen(tk == 
      1,-2.44292601424428961264353508344*GFOffset(rho,0,0,-1) + 
      3.45582821429428513260129789155*GFOffset(rho,0,0,1) - 
      1.59860668809836683716785819589*GFOffset(rho,0,0,2) + 
      0.96133979728871166671127688438*GFOffset(rho,0,0,3) - 
      0.60224717963578568466640341307*GFOffset(rho,0,0,4) + 
      0.226611870395445335165221916464*GFOffset(rho,0,0,5),0) + IfThen(tk == 
      2,0.62525666551534211425476493044*GFOffset(rho,0,0,-2) - 
      2.21580428316997131363803034549*GFOffset(rho,0,0,-1) + 
      2.26669808708599901220846300609*GFOffset(rho,0,0,1) - 
      1.06644190400637468973550663446*GFOffset(rho,0,0,2) + 
      0.61639083551757952864611929469*GFOffset(rho,0,0,3) - 
      0.22609940094257465173581025128*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      3,-0.3125*GFOffset(rho,0,0,-3) + 
      0.9075444712688209188202794332*GFOffset(rho,0,0,-2) - 
      2.00696924058875308957204950201*GFOffset(rho,0,0,-1) + 
      2.00696924058875308957204950201*GFOffset(rho,0,0,1) - 
      0.9075444712688209188202794332*GFOffset(rho,0,0,2) + 
      0.3125*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      4,0.22609940094257465173581025128*GFOffset(rho,0,0,-4) - 
      0.61639083551757952864611929469*GFOffset(rho,0,0,-3) + 
      1.06644190400637468973550663446*GFOffset(rho,0,0,-2) - 
      2.26669808708599901220846300609*GFOffset(rho,0,0,-1) + 
      2.21580428316997131363803034549*GFOffset(rho,0,0,1) - 
      0.62525666551534211425476493044*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      5,-0.226611870395445335165221916464*GFOffset(rho,0,0,-5) + 
      0.60224717963578568466640341307*GFOffset(rho,0,0,-4) - 
      0.96133979728871166671127688438*GFOffset(rho,0,0,-3) + 
      1.59860668809836683716785819589*GFOffset(rho,0,0,-2) - 
      3.45582821429428513260129789155*GFOffset(rho,0,0,-1) + 
      2.44292601424428961264353508344*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      6,0.5*GFOffset(rho,0,0,-6) - 
      1.31737343570243448845609847333*GFOffset(rho,0,0,-5) + 
      2.04996481307674277696238718244*GFOffset(rho,0,0,-4) - 
      3.2*GFOffset(rho,0,0,-3) + 
      5.6689852255455078785754090169*GFOffset(rho,0,0,-2) - 
      14.201576602919816167081697726*GFOffset(rho,0,0,-1) + 
      10.5*GFOffset(rho,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tk == 
      0,21.*GFOffset(rho,0,0,-1),0) + IfThen(tk == 
      6,-21.*GFOffset(rho,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL LDv11 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(ti == 
      0,-21.*GFOffset(v1,0,0,0),0) + IfThen(ti == 
      6,21.*GFOffset(v1,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(ti == 
      0,-10.5*GFOffset(v1,0,0,0) + 
      14.201576602919816167081697726*GFOffset(v1,1,0,0) - 
      5.6689852255455078785754090169*GFOffset(v1,2,0,0) + 
      3.2*GFOffset(v1,3,0,0) - 
      2.04996481307674277696238718244*GFOffset(v1,4,0,0) + 
      1.31737343570243448845609847333*GFOffset(v1,5,0,0) - 
      0.5*GFOffset(v1,6,0,0),0) + IfThen(ti == 
      1,-2.44292601424428961264353508344*GFOffset(v1,-1,0,0) + 
      3.45582821429428513260129789155*GFOffset(v1,1,0,0) - 
      1.59860668809836683716785819589*GFOffset(v1,2,0,0) + 
      0.96133979728871166671127688438*GFOffset(v1,3,0,0) - 
      0.60224717963578568466640341307*GFOffset(v1,4,0,0) + 
      0.226611870395445335165221916464*GFOffset(v1,5,0,0),0) + IfThen(ti == 
      2,0.62525666551534211425476493044*GFOffset(v1,-2,0,0) - 
      2.21580428316997131363803034549*GFOffset(v1,-1,0,0) + 
      2.26669808708599901220846300609*GFOffset(v1,1,0,0) - 
      1.06644190400637468973550663446*GFOffset(v1,2,0,0) + 
      0.61639083551757952864611929469*GFOffset(v1,3,0,0) - 
      0.22609940094257465173581025128*GFOffset(v1,4,0,0),0) + IfThen(ti == 
      3,-0.3125*GFOffset(v1,-3,0,0) + 
      0.9075444712688209188202794332*GFOffset(v1,-2,0,0) - 
      2.00696924058875308957204950201*GFOffset(v1,-1,0,0) + 
      2.00696924058875308957204950201*GFOffset(v1,1,0,0) - 
      0.9075444712688209188202794332*GFOffset(v1,2,0,0) + 
      0.3125*GFOffset(v1,3,0,0),0) + IfThen(ti == 
      4,0.22609940094257465173581025128*GFOffset(v1,-4,0,0) - 
      0.61639083551757952864611929469*GFOffset(v1,-3,0,0) + 
      1.06644190400637468973550663446*GFOffset(v1,-2,0,0) - 
      2.26669808708599901220846300609*GFOffset(v1,-1,0,0) + 
      2.21580428316997131363803034549*GFOffset(v1,1,0,0) - 
      0.62525666551534211425476493044*GFOffset(v1,2,0,0),0) + IfThen(ti == 
      5,-0.226611870395445335165221916464*GFOffset(v1,-5,0,0) + 
      0.60224717963578568466640341307*GFOffset(v1,-4,0,0) - 
      0.96133979728871166671127688438*GFOffset(v1,-3,0,0) + 
      1.59860668809836683716785819589*GFOffset(v1,-2,0,0) - 
      3.45582821429428513260129789155*GFOffset(v1,-1,0,0) + 
      2.44292601424428961264353508344*GFOffset(v1,1,0,0),0) + IfThen(ti == 
      6,0.5*GFOffset(v1,-6,0,0) - 
      1.31737343570243448845609847333*GFOffset(v1,-5,0,0) + 
      2.04996481307674277696238718244*GFOffset(v1,-4,0,0) - 
      3.2*GFOffset(v1,-3,0,0) + 
      5.6689852255455078785754090169*GFOffset(v1,-2,0,0) - 
      14.201576602919816167081697726*GFOffset(v1,-1,0,0) + 
      10.5*GFOffset(v1,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(ti == 
      0,21.*GFOffset(v1,-1,0,0),0) + IfThen(ti == 
      6,-21.*GFOffset(v1,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDv12 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(tj == 
      0,-21.*GFOffset(v1,0,0,0),0) + IfThen(tj == 
      6,21.*GFOffset(v1,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(tj == 
      0,-10.5*GFOffset(v1,0,0,0) + 
      14.201576602919816167081697726*GFOffset(v1,0,1,0) - 
      5.6689852255455078785754090169*GFOffset(v1,0,2,0) + 
      3.2*GFOffset(v1,0,3,0) - 
      2.04996481307674277696238718244*GFOffset(v1,0,4,0) + 
      1.31737343570243448845609847333*GFOffset(v1,0,5,0) - 
      0.5*GFOffset(v1,0,6,0),0) + IfThen(tj == 
      1,-2.44292601424428961264353508344*GFOffset(v1,0,-1,0) + 
      3.45582821429428513260129789155*GFOffset(v1,0,1,0) - 
      1.59860668809836683716785819589*GFOffset(v1,0,2,0) + 
      0.96133979728871166671127688438*GFOffset(v1,0,3,0) - 
      0.60224717963578568466640341307*GFOffset(v1,0,4,0) + 
      0.226611870395445335165221916464*GFOffset(v1,0,5,0),0) + IfThen(tj == 
      2,0.62525666551534211425476493044*GFOffset(v1,0,-2,0) - 
      2.21580428316997131363803034549*GFOffset(v1,0,-1,0) + 
      2.26669808708599901220846300609*GFOffset(v1,0,1,0) - 
      1.06644190400637468973550663446*GFOffset(v1,0,2,0) + 
      0.61639083551757952864611929469*GFOffset(v1,0,3,0) - 
      0.22609940094257465173581025128*GFOffset(v1,0,4,0),0) + IfThen(tj == 
      3,-0.3125*GFOffset(v1,0,-3,0) + 
      0.9075444712688209188202794332*GFOffset(v1,0,-2,0) - 
      2.00696924058875308957204950201*GFOffset(v1,0,-1,0) + 
      2.00696924058875308957204950201*GFOffset(v1,0,1,0) - 
      0.9075444712688209188202794332*GFOffset(v1,0,2,0) + 
      0.3125*GFOffset(v1,0,3,0),0) + IfThen(tj == 
      4,0.22609940094257465173581025128*GFOffset(v1,0,-4,0) - 
      0.61639083551757952864611929469*GFOffset(v1,0,-3,0) + 
      1.06644190400637468973550663446*GFOffset(v1,0,-2,0) - 
      2.26669808708599901220846300609*GFOffset(v1,0,-1,0) + 
      2.21580428316997131363803034549*GFOffset(v1,0,1,0) - 
      0.62525666551534211425476493044*GFOffset(v1,0,2,0),0) + IfThen(tj == 
      5,-0.226611870395445335165221916464*GFOffset(v1,0,-5,0) + 
      0.60224717963578568466640341307*GFOffset(v1,0,-4,0) - 
      0.96133979728871166671127688438*GFOffset(v1,0,-3,0) + 
      1.59860668809836683716785819589*GFOffset(v1,0,-2,0) - 
      3.45582821429428513260129789155*GFOffset(v1,0,-1,0) + 
      2.44292601424428961264353508344*GFOffset(v1,0,1,0),0) + IfThen(tj == 
      6,0.5*GFOffset(v1,0,-6,0) - 
      1.31737343570243448845609847333*GFOffset(v1,0,-5,0) + 
      2.04996481307674277696238718244*GFOffset(v1,0,-4,0) - 
      3.2*GFOffset(v1,0,-3,0) + 
      5.6689852255455078785754090169*GFOffset(v1,0,-2,0) - 
      14.201576602919816167081697726*GFOffset(v1,0,-1,0) + 
      10.5*GFOffset(v1,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tj == 
      0,21.*GFOffset(v1,0,-1,0),0) + IfThen(tj == 
      6,-21.*GFOffset(v1,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDv13 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(tk == 
      0,-21.*GFOffset(v1,0,0,0),0) + IfThen(tk == 
      6,21.*GFOffset(v1,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(tk == 
      0,-10.5*GFOffset(v1,0,0,0) + 
      14.201576602919816167081697726*GFOffset(v1,0,0,1) - 
      5.6689852255455078785754090169*GFOffset(v1,0,0,2) + 
      3.2*GFOffset(v1,0,0,3) - 
      2.04996481307674277696238718244*GFOffset(v1,0,0,4) + 
      1.31737343570243448845609847333*GFOffset(v1,0,0,5) - 
      0.5*GFOffset(v1,0,0,6),0) + IfThen(tk == 
      1,-2.44292601424428961264353508344*GFOffset(v1,0,0,-1) + 
      3.45582821429428513260129789155*GFOffset(v1,0,0,1) - 
      1.59860668809836683716785819589*GFOffset(v1,0,0,2) + 
      0.96133979728871166671127688438*GFOffset(v1,0,0,3) - 
      0.60224717963578568466640341307*GFOffset(v1,0,0,4) + 
      0.226611870395445335165221916464*GFOffset(v1,0,0,5),0) + IfThen(tk == 
      2,0.62525666551534211425476493044*GFOffset(v1,0,0,-2) - 
      2.21580428316997131363803034549*GFOffset(v1,0,0,-1) + 
      2.26669808708599901220846300609*GFOffset(v1,0,0,1) - 
      1.06644190400637468973550663446*GFOffset(v1,0,0,2) + 
      0.61639083551757952864611929469*GFOffset(v1,0,0,3) - 
      0.22609940094257465173581025128*GFOffset(v1,0,0,4),0) + IfThen(tk == 
      3,-0.3125*GFOffset(v1,0,0,-3) + 
      0.9075444712688209188202794332*GFOffset(v1,0,0,-2) - 
      2.00696924058875308957204950201*GFOffset(v1,0,0,-1) + 
      2.00696924058875308957204950201*GFOffset(v1,0,0,1) - 
      0.9075444712688209188202794332*GFOffset(v1,0,0,2) + 
      0.3125*GFOffset(v1,0,0,3),0) + IfThen(tk == 
      4,0.22609940094257465173581025128*GFOffset(v1,0,0,-4) - 
      0.61639083551757952864611929469*GFOffset(v1,0,0,-3) + 
      1.06644190400637468973550663446*GFOffset(v1,0,0,-2) - 
      2.26669808708599901220846300609*GFOffset(v1,0,0,-1) + 
      2.21580428316997131363803034549*GFOffset(v1,0,0,1) - 
      0.62525666551534211425476493044*GFOffset(v1,0,0,2),0) + IfThen(tk == 
      5,-0.226611870395445335165221916464*GFOffset(v1,0,0,-5) + 
      0.60224717963578568466640341307*GFOffset(v1,0,0,-4) - 
      0.96133979728871166671127688438*GFOffset(v1,0,0,-3) + 
      1.59860668809836683716785819589*GFOffset(v1,0,0,-2) - 
      3.45582821429428513260129789155*GFOffset(v1,0,0,-1) + 
      2.44292601424428961264353508344*GFOffset(v1,0,0,1),0) + IfThen(tk == 
      6,0.5*GFOffset(v1,0,0,-6) - 
      1.31737343570243448845609847333*GFOffset(v1,0,0,-5) + 
      2.04996481307674277696238718244*GFOffset(v1,0,0,-4) - 
      3.2*GFOffset(v1,0,0,-3) + 
      5.6689852255455078785754090169*GFOffset(v1,0,0,-2) - 
      14.201576602919816167081697726*GFOffset(v1,0,0,-1) + 
      10.5*GFOffset(v1,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tk == 
      0,21.*GFOffset(v1,0,0,-1),0) + IfThen(tk == 
      6,-21.*GFOffset(v1,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL LDv21 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(ti == 
      0,-21.*GFOffset(v2,0,0,0),0) + IfThen(ti == 
      6,21.*GFOffset(v2,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(ti == 
      0,-10.5*GFOffset(v2,0,0,0) + 
      14.201576602919816167081697726*GFOffset(v2,1,0,0) - 
      5.6689852255455078785754090169*GFOffset(v2,2,0,0) + 
      3.2*GFOffset(v2,3,0,0) - 
      2.04996481307674277696238718244*GFOffset(v2,4,0,0) + 
      1.31737343570243448845609847333*GFOffset(v2,5,0,0) - 
      0.5*GFOffset(v2,6,0,0),0) + IfThen(ti == 
      1,-2.44292601424428961264353508344*GFOffset(v2,-1,0,0) + 
      3.45582821429428513260129789155*GFOffset(v2,1,0,0) - 
      1.59860668809836683716785819589*GFOffset(v2,2,0,0) + 
      0.96133979728871166671127688438*GFOffset(v2,3,0,0) - 
      0.60224717963578568466640341307*GFOffset(v2,4,0,0) + 
      0.226611870395445335165221916464*GFOffset(v2,5,0,0),0) + IfThen(ti == 
      2,0.62525666551534211425476493044*GFOffset(v2,-2,0,0) - 
      2.21580428316997131363803034549*GFOffset(v2,-1,0,0) + 
      2.26669808708599901220846300609*GFOffset(v2,1,0,0) - 
      1.06644190400637468973550663446*GFOffset(v2,2,0,0) + 
      0.61639083551757952864611929469*GFOffset(v2,3,0,0) - 
      0.22609940094257465173581025128*GFOffset(v2,4,0,0),0) + IfThen(ti == 
      3,-0.3125*GFOffset(v2,-3,0,0) + 
      0.9075444712688209188202794332*GFOffset(v2,-2,0,0) - 
      2.00696924058875308957204950201*GFOffset(v2,-1,0,0) + 
      2.00696924058875308957204950201*GFOffset(v2,1,0,0) - 
      0.9075444712688209188202794332*GFOffset(v2,2,0,0) + 
      0.3125*GFOffset(v2,3,0,0),0) + IfThen(ti == 
      4,0.22609940094257465173581025128*GFOffset(v2,-4,0,0) - 
      0.61639083551757952864611929469*GFOffset(v2,-3,0,0) + 
      1.06644190400637468973550663446*GFOffset(v2,-2,0,0) - 
      2.26669808708599901220846300609*GFOffset(v2,-1,0,0) + 
      2.21580428316997131363803034549*GFOffset(v2,1,0,0) - 
      0.62525666551534211425476493044*GFOffset(v2,2,0,0),0) + IfThen(ti == 
      5,-0.226611870395445335165221916464*GFOffset(v2,-5,0,0) + 
      0.60224717963578568466640341307*GFOffset(v2,-4,0,0) - 
      0.96133979728871166671127688438*GFOffset(v2,-3,0,0) + 
      1.59860668809836683716785819589*GFOffset(v2,-2,0,0) - 
      3.45582821429428513260129789155*GFOffset(v2,-1,0,0) + 
      2.44292601424428961264353508344*GFOffset(v2,1,0,0),0) + IfThen(ti == 
      6,0.5*GFOffset(v2,-6,0,0) - 
      1.31737343570243448845609847333*GFOffset(v2,-5,0,0) + 
      2.04996481307674277696238718244*GFOffset(v2,-4,0,0) - 
      3.2*GFOffset(v2,-3,0,0) + 
      5.6689852255455078785754090169*GFOffset(v2,-2,0,0) - 
      14.201576602919816167081697726*GFOffset(v2,-1,0,0) + 
      10.5*GFOffset(v2,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(ti == 
      0,21.*GFOffset(v2,-1,0,0),0) + IfThen(ti == 
      6,-21.*GFOffset(v2,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDv22 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(tj == 
      0,-21.*GFOffset(v2,0,0,0),0) + IfThen(tj == 
      6,21.*GFOffset(v2,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(tj == 
      0,-10.5*GFOffset(v2,0,0,0) + 
      14.201576602919816167081697726*GFOffset(v2,0,1,0) - 
      5.6689852255455078785754090169*GFOffset(v2,0,2,0) + 
      3.2*GFOffset(v2,0,3,0) - 
      2.04996481307674277696238718244*GFOffset(v2,0,4,0) + 
      1.31737343570243448845609847333*GFOffset(v2,0,5,0) - 
      0.5*GFOffset(v2,0,6,0),0) + IfThen(tj == 
      1,-2.44292601424428961264353508344*GFOffset(v2,0,-1,0) + 
      3.45582821429428513260129789155*GFOffset(v2,0,1,0) - 
      1.59860668809836683716785819589*GFOffset(v2,0,2,0) + 
      0.96133979728871166671127688438*GFOffset(v2,0,3,0) - 
      0.60224717963578568466640341307*GFOffset(v2,0,4,0) + 
      0.226611870395445335165221916464*GFOffset(v2,0,5,0),0) + IfThen(tj == 
      2,0.62525666551534211425476493044*GFOffset(v2,0,-2,0) - 
      2.21580428316997131363803034549*GFOffset(v2,0,-1,0) + 
      2.26669808708599901220846300609*GFOffset(v2,0,1,0) - 
      1.06644190400637468973550663446*GFOffset(v2,0,2,0) + 
      0.61639083551757952864611929469*GFOffset(v2,0,3,0) - 
      0.22609940094257465173581025128*GFOffset(v2,0,4,0),0) + IfThen(tj == 
      3,-0.3125*GFOffset(v2,0,-3,0) + 
      0.9075444712688209188202794332*GFOffset(v2,0,-2,0) - 
      2.00696924058875308957204950201*GFOffset(v2,0,-1,0) + 
      2.00696924058875308957204950201*GFOffset(v2,0,1,0) - 
      0.9075444712688209188202794332*GFOffset(v2,0,2,0) + 
      0.3125*GFOffset(v2,0,3,0),0) + IfThen(tj == 
      4,0.22609940094257465173581025128*GFOffset(v2,0,-4,0) - 
      0.61639083551757952864611929469*GFOffset(v2,0,-3,0) + 
      1.06644190400637468973550663446*GFOffset(v2,0,-2,0) - 
      2.26669808708599901220846300609*GFOffset(v2,0,-1,0) + 
      2.21580428316997131363803034549*GFOffset(v2,0,1,0) - 
      0.62525666551534211425476493044*GFOffset(v2,0,2,0),0) + IfThen(tj == 
      5,-0.226611870395445335165221916464*GFOffset(v2,0,-5,0) + 
      0.60224717963578568466640341307*GFOffset(v2,0,-4,0) - 
      0.96133979728871166671127688438*GFOffset(v2,0,-3,0) + 
      1.59860668809836683716785819589*GFOffset(v2,0,-2,0) - 
      3.45582821429428513260129789155*GFOffset(v2,0,-1,0) + 
      2.44292601424428961264353508344*GFOffset(v2,0,1,0),0) + IfThen(tj == 
      6,0.5*GFOffset(v2,0,-6,0) - 
      1.31737343570243448845609847333*GFOffset(v2,0,-5,0) + 
      2.04996481307674277696238718244*GFOffset(v2,0,-4,0) - 
      3.2*GFOffset(v2,0,-3,0) + 
      5.6689852255455078785754090169*GFOffset(v2,0,-2,0) - 
      14.201576602919816167081697726*GFOffset(v2,0,-1,0) + 
      10.5*GFOffset(v2,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tj == 
      0,21.*GFOffset(v2,0,-1,0),0) + IfThen(tj == 
      6,-21.*GFOffset(v2,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDv23 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(tk == 
      0,-21.*GFOffset(v2,0,0,0),0) + IfThen(tk == 
      6,21.*GFOffset(v2,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(tk == 
      0,-10.5*GFOffset(v2,0,0,0) + 
      14.201576602919816167081697726*GFOffset(v2,0,0,1) - 
      5.6689852255455078785754090169*GFOffset(v2,0,0,2) + 
      3.2*GFOffset(v2,0,0,3) - 
      2.04996481307674277696238718244*GFOffset(v2,0,0,4) + 
      1.31737343570243448845609847333*GFOffset(v2,0,0,5) - 
      0.5*GFOffset(v2,0,0,6),0) + IfThen(tk == 
      1,-2.44292601424428961264353508344*GFOffset(v2,0,0,-1) + 
      3.45582821429428513260129789155*GFOffset(v2,0,0,1) - 
      1.59860668809836683716785819589*GFOffset(v2,0,0,2) + 
      0.96133979728871166671127688438*GFOffset(v2,0,0,3) - 
      0.60224717963578568466640341307*GFOffset(v2,0,0,4) + 
      0.226611870395445335165221916464*GFOffset(v2,0,0,5),0) + IfThen(tk == 
      2,0.62525666551534211425476493044*GFOffset(v2,0,0,-2) - 
      2.21580428316997131363803034549*GFOffset(v2,0,0,-1) + 
      2.26669808708599901220846300609*GFOffset(v2,0,0,1) - 
      1.06644190400637468973550663446*GFOffset(v2,0,0,2) + 
      0.61639083551757952864611929469*GFOffset(v2,0,0,3) - 
      0.22609940094257465173581025128*GFOffset(v2,0,0,4),0) + IfThen(tk == 
      3,-0.3125*GFOffset(v2,0,0,-3) + 
      0.9075444712688209188202794332*GFOffset(v2,0,0,-2) - 
      2.00696924058875308957204950201*GFOffset(v2,0,0,-1) + 
      2.00696924058875308957204950201*GFOffset(v2,0,0,1) - 
      0.9075444712688209188202794332*GFOffset(v2,0,0,2) + 
      0.3125*GFOffset(v2,0,0,3),0) + IfThen(tk == 
      4,0.22609940094257465173581025128*GFOffset(v2,0,0,-4) - 
      0.61639083551757952864611929469*GFOffset(v2,0,0,-3) + 
      1.06644190400637468973550663446*GFOffset(v2,0,0,-2) - 
      2.26669808708599901220846300609*GFOffset(v2,0,0,-1) + 
      2.21580428316997131363803034549*GFOffset(v2,0,0,1) - 
      0.62525666551534211425476493044*GFOffset(v2,0,0,2),0) + IfThen(tk == 
      5,-0.226611870395445335165221916464*GFOffset(v2,0,0,-5) + 
      0.60224717963578568466640341307*GFOffset(v2,0,0,-4) - 
      0.96133979728871166671127688438*GFOffset(v2,0,0,-3) + 
      1.59860668809836683716785819589*GFOffset(v2,0,0,-2) - 
      3.45582821429428513260129789155*GFOffset(v2,0,0,-1) + 
      2.44292601424428961264353508344*GFOffset(v2,0,0,1),0) + IfThen(tk == 
      6,0.5*GFOffset(v2,0,0,-6) - 
      1.31737343570243448845609847333*GFOffset(v2,0,0,-5) + 
      2.04996481307674277696238718244*GFOffset(v2,0,0,-4) - 
      3.2*GFOffset(v2,0,0,-3) + 
      5.6689852255455078785754090169*GFOffset(v2,0,0,-2) - 
      14.201576602919816167081697726*GFOffset(v2,0,0,-1) + 
      10.5*GFOffset(v2,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tk == 
      0,21.*GFOffset(v2,0,0,-1),0) + IfThen(tk == 
      6,-21.*GFOffset(v2,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL LDv31 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(ti == 
      0,-21.*GFOffset(v3,0,0,0),0) + IfThen(ti == 
      6,21.*GFOffset(v3,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(ti == 
      0,-10.5*GFOffset(v3,0,0,0) + 
      14.201576602919816167081697726*GFOffset(v3,1,0,0) - 
      5.6689852255455078785754090169*GFOffset(v3,2,0,0) + 
      3.2*GFOffset(v3,3,0,0) - 
      2.04996481307674277696238718244*GFOffset(v3,4,0,0) + 
      1.31737343570243448845609847333*GFOffset(v3,5,0,0) - 
      0.5*GFOffset(v3,6,0,0),0) + IfThen(ti == 
      1,-2.44292601424428961264353508344*GFOffset(v3,-1,0,0) + 
      3.45582821429428513260129789155*GFOffset(v3,1,0,0) - 
      1.59860668809836683716785819589*GFOffset(v3,2,0,0) + 
      0.96133979728871166671127688438*GFOffset(v3,3,0,0) - 
      0.60224717963578568466640341307*GFOffset(v3,4,0,0) + 
      0.226611870395445335165221916464*GFOffset(v3,5,0,0),0) + IfThen(ti == 
      2,0.62525666551534211425476493044*GFOffset(v3,-2,0,0) - 
      2.21580428316997131363803034549*GFOffset(v3,-1,0,0) + 
      2.26669808708599901220846300609*GFOffset(v3,1,0,0) - 
      1.06644190400637468973550663446*GFOffset(v3,2,0,0) + 
      0.61639083551757952864611929469*GFOffset(v3,3,0,0) - 
      0.22609940094257465173581025128*GFOffset(v3,4,0,0),0) + IfThen(ti == 
      3,-0.3125*GFOffset(v3,-3,0,0) + 
      0.9075444712688209188202794332*GFOffset(v3,-2,0,0) - 
      2.00696924058875308957204950201*GFOffset(v3,-1,0,0) + 
      2.00696924058875308957204950201*GFOffset(v3,1,0,0) - 
      0.9075444712688209188202794332*GFOffset(v3,2,0,0) + 
      0.3125*GFOffset(v3,3,0,0),0) + IfThen(ti == 
      4,0.22609940094257465173581025128*GFOffset(v3,-4,0,0) - 
      0.61639083551757952864611929469*GFOffset(v3,-3,0,0) + 
      1.06644190400637468973550663446*GFOffset(v3,-2,0,0) - 
      2.26669808708599901220846300609*GFOffset(v3,-1,0,0) + 
      2.21580428316997131363803034549*GFOffset(v3,1,0,0) - 
      0.62525666551534211425476493044*GFOffset(v3,2,0,0),0) + IfThen(ti == 
      5,-0.226611870395445335165221916464*GFOffset(v3,-5,0,0) + 
      0.60224717963578568466640341307*GFOffset(v3,-4,0,0) - 
      0.96133979728871166671127688438*GFOffset(v3,-3,0,0) + 
      1.59860668809836683716785819589*GFOffset(v3,-2,0,0) - 
      3.45582821429428513260129789155*GFOffset(v3,-1,0,0) + 
      2.44292601424428961264353508344*GFOffset(v3,1,0,0),0) + IfThen(ti == 
      6,0.5*GFOffset(v3,-6,0,0) - 
      1.31737343570243448845609847333*GFOffset(v3,-5,0,0) + 
      2.04996481307674277696238718244*GFOffset(v3,-4,0,0) - 
      3.2*GFOffset(v3,-3,0,0) + 
      5.6689852255455078785754090169*GFOffset(v3,-2,0,0) - 
      14.201576602919816167081697726*GFOffset(v3,-1,0,0) + 
      10.5*GFOffset(v3,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(ti == 
      0,21.*GFOffset(v3,-1,0,0),0) + IfThen(ti == 
      6,-21.*GFOffset(v3,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDv32 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(tj == 
      0,-21.*GFOffset(v3,0,0,0),0) + IfThen(tj == 
      6,21.*GFOffset(v3,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(tj == 
      0,-10.5*GFOffset(v3,0,0,0) + 
      14.201576602919816167081697726*GFOffset(v3,0,1,0) - 
      5.6689852255455078785754090169*GFOffset(v3,0,2,0) + 
      3.2*GFOffset(v3,0,3,0) - 
      2.04996481307674277696238718244*GFOffset(v3,0,4,0) + 
      1.31737343570243448845609847333*GFOffset(v3,0,5,0) - 
      0.5*GFOffset(v3,0,6,0),0) + IfThen(tj == 
      1,-2.44292601424428961264353508344*GFOffset(v3,0,-1,0) + 
      3.45582821429428513260129789155*GFOffset(v3,0,1,0) - 
      1.59860668809836683716785819589*GFOffset(v3,0,2,0) + 
      0.96133979728871166671127688438*GFOffset(v3,0,3,0) - 
      0.60224717963578568466640341307*GFOffset(v3,0,4,0) + 
      0.226611870395445335165221916464*GFOffset(v3,0,5,0),0) + IfThen(tj == 
      2,0.62525666551534211425476493044*GFOffset(v3,0,-2,0) - 
      2.21580428316997131363803034549*GFOffset(v3,0,-1,0) + 
      2.26669808708599901220846300609*GFOffset(v3,0,1,0) - 
      1.06644190400637468973550663446*GFOffset(v3,0,2,0) + 
      0.61639083551757952864611929469*GFOffset(v3,0,3,0) - 
      0.22609940094257465173581025128*GFOffset(v3,0,4,0),0) + IfThen(tj == 
      3,-0.3125*GFOffset(v3,0,-3,0) + 
      0.9075444712688209188202794332*GFOffset(v3,0,-2,0) - 
      2.00696924058875308957204950201*GFOffset(v3,0,-1,0) + 
      2.00696924058875308957204950201*GFOffset(v3,0,1,0) - 
      0.9075444712688209188202794332*GFOffset(v3,0,2,0) + 
      0.3125*GFOffset(v3,0,3,0),0) + IfThen(tj == 
      4,0.22609940094257465173581025128*GFOffset(v3,0,-4,0) - 
      0.61639083551757952864611929469*GFOffset(v3,0,-3,0) + 
      1.06644190400637468973550663446*GFOffset(v3,0,-2,0) - 
      2.26669808708599901220846300609*GFOffset(v3,0,-1,0) + 
      2.21580428316997131363803034549*GFOffset(v3,0,1,0) - 
      0.62525666551534211425476493044*GFOffset(v3,0,2,0),0) + IfThen(tj == 
      5,-0.226611870395445335165221916464*GFOffset(v3,0,-5,0) + 
      0.60224717963578568466640341307*GFOffset(v3,0,-4,0) - 
      0.96133979728871166671127688438*GFOffset(v3,0,-3,0) + 
      1.59860668809836683716785819589*GFOffset(v3,0,-2,0) - 
      3.45582821429428513260129789155*GFOffset(v3,0,-1,0) + 
      2.44292601424428961264353508344*GFOffset(v3,0,1,0),0) + IfThen(tj == 
      6,0.5*GFOffset(v3,0,-6,0) - 
      1.31737343570243448845609847333*GFOffset(v3,0,-5,0) + 
      2.04996481307674277696238718244*GFOffset(v3,0,-4,0) - 
      3.2*GFOffset(v3,0,-3,0) + 
      5.6689852255455078785754090169*GFOffset(v3,0,-2,0) - 
      14.201576602919816167081697726*GFOffset(v3,0,-1,0) + 
      10.5*GFOffset(v3,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tj == 
      0,21.*GFOffset(v3,0,-1,0),0) + IfThen(tj == 
      6,-21.*GFOffset(v3,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDv33 CCTK_ATTRIBUTE_UNUSED = 
      (-0.142857142857142857142857142857*(IfThen(tk == 
      0,-21.*GFOffset(v3,0,0,0),0) + IfThen(tk == 
      6,21.*GFOffset(v3,0,0,0),0)) + 
      0.285714285714285714285714285714*(IfThen(tk == 
      0,-10.5*GFOffset(v3,0,0,0) + 
      14.201576602919816167081697726*GFOffset(v3,0,0,1) - 
      5.6689852255455078785754090169*GFOffset(v3,0,0,2) + 
      3.2*GFOffset(v3,0,0,3) - 
      2.04996481307674277696238718244*GFOffset(v3,0,0,4) + 
      1.31737343570243448845609847333*GFOffset(v3,0,0,5) - 
      0.5*GFOffset(v3,0,0,6),0) + IfThen(tk == 
      1,-2.44292601424428961264353508344*GFOffset(v3,0,0,-1) + 
      3.45582821429428513260129789155*GFOffset(v3,0,0,1) - 
      1.59860668809836683716785819589*GFOffset(v3,0,0,2) + 
      0.96133979728871166671127688438*GFOffset(v3,0,0,3) - 
      0.60224717963578568466640341307*GFOffset(v3,0,0,4) + 
      0.226611870395445335165221916464*GFOffset(v3,0,0,5),0) + IfThen(tk == 
      2,0.62525666551534211425476493044*GFOffset(v3,0,0,-2) - 
      2.21580428316997131363803034549*GFOffset(v3,0,0,-1) + 
      2.26669808708599901220846300609*GFOffset(v3,0,0,1) - 
      1.06644190400637468973550663446*GFOffset(v3,0,0,2) + 
      0.61639083551757952864611929469*GFOffset(v3,0,0,3) - 
      0.22609940094257465173581025128*GFOffset(v3,0,0,4),0) + IfThen(tk == 
      3,-0.3125*GFOffset(v3,0,0,-3) + 
      0.9075444712688209188202794332*GFOffset(v3,0,0,-2) - 
      2.00696924058875308957204950201*GFOffset(v3,0,0,-1) + 
      2.00696924058875308957204950201*GFOffset(v3,0,0,1) - 
      0.9075444712688209188202794332*GFOffset(v3,0,0,2) + 
      0.3125*GFOffset(v3,0,0,3),0) + IfThen(tk == 
      4,0.22609940094257465173581025128*GFOffset(v3,0,0,-4) - 
      0.61639083551757952864611929469*GFOffset(v3,0,0,-3) + 
      1.06644190400637468973550663446*GFOffset(v3,0,0,-2) - 
      2.26669808708599901220846300609*GFOffset(v3,0,0,-1) + 
      2.21580428316997131363803034549*GFOffset(v3,0,0,1) - 
      0.62525666551534211425476493044*GFOffset(v3,0,0,2),0) + IfThen(tk == 
      5,-0.226611870395445335165221916464*GFOffset(v3,0,0,-5) + 
      0.60224717963578568466640341307*GFOffset(v3,0,0,-4) - 
      0.96133979728871166671127688438*GFOffset(v3,0,0,-3) + 
      1.59860668809836683716785819589*GFOffset(v3,0,0,-2) - 
      3.45582821429428513260129789155*GFOffset(v3,0,0,-1) + 
      2.44292601424428961264353508344*GFOffset(v3,0,0,1),0) + IfThen(tk == 
      6,0.5*GFOffset(v3,0,0,-6) - 
      1.31737343570243448845609847333*GFOffset(v3,0,0,-5) + 
      2.04996481307674277696238718244*GFOffset(v3,0,0,-4) - 
      3.2*GFOffset(v3,0,0,-3) + 
      5.6689852255455078785754090169*GFOffset(v3,0,0,-2) - 
      14.201576602919816167081697726*GFOffset(v3,0,0,-1) + 
      10.5*GFOffset(v3,0,0,0),0)) - 
      0.285714285714285714285714285714*alphaDeriv*(IfThen(tk == 
      0,21.*GFOffset(v3,0,0,-1),0) + IfThen(tk == 
      6,-21.*GFOffset(v3,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL PDrho1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDrho2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDrho3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDv11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDv22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDv33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDrho1 = J11L*LDrho1 + J21L*LDrho2 + J31L*LDrho3;
      
      PDrho2 = J12L*LDrho1 + J22L*LDrho2 + J32L*LDrho3;
      
      PDrho3 = J13L*LDrho1 + J23L*LDrho2 + J33L*LDrho3;
      
      PDv11 = J11L*LDv11 + J21L*LDv12 + J31L*LDv13;
      
      PDv22 = J12L*LDv21 + J22L*LDv22 + J32L*LDv23;
      
      PDv33 = J13L*LDv31 + J23L*LDv32 + J33L*LDv33;
    }
    else
    {
      PDrho1 = LDrho1;
      
      PDrho2 = LDrho2;
      
      PDrho3 = LDrho3;
      
      PDv11 = LDv11;
      
      PDv22 = LDv22;
      
      PDv33 = LDv33;
    }
    
    CCTK_REAL urhsL CCTK_ATTRIBUTE_UNUSED = rhoL + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(u,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(u,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(u,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(u,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(u,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(u,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(u,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(u,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(u,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(u,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(u,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(u,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(u,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(u,4,0,0),0) + IfThen(ti == 
      3,-0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(u,-1,0,0) + 
      GFOffset(u,1,0,0)) - 
      0.107637859609505734449271251484*(GFOffset(u,-2,0,0) + 
      GFOffset(u,2,0,0)) + 
      0.0446428403421747932239219136542*(GFOffset(u,-3,0,0) + 
      GFOffset(u,3,0,0)),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(u,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(u,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(u,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(u,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(u,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(u,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(u,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(u,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(u,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(u,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(u,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(u,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(u,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(u,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(u,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(u,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(u,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(u,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(u,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(u,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(u,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(u,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(u,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(u,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(u,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(u,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(u,0,4,0),0) + IfThen(tj == 
      3,-0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(u,0,-1,0) + 
      GFOffset(u,0,1,0)) - 
      0.107637859609505734449271251484*(GFOffset(u,0,-2,0) + 
      GFOffset(u,0,2,0)) + 
      0.0446428403421747932239219136542*(GFOffset(u,0,-3,0) + 
      GFOffset(u,0,3,0)),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(u,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(u,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(u,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(u,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(u,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(u,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(u,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(u,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(u,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(u,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(u,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(u,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(u,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(u,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(u,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(u,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(u,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(u,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(u,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(u,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(u,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(u,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(u,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(u,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(u,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(u,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(u,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(u,0,0,4),0) + IfThen(tk == 
      3,-0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(u,0,0,-1) + 
      GFOffset(u,0,0,1)) - 
      0.107637859609505734449271251484*(GFOffset(u,0,0,-2) + 
      GFOffset(u,0,0,2)) + 
      0.0446428403421747932239219136542*(GFOffset(u,0,0,-3) + 
      GFOffset(u,0,0,3)),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(u,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(u,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(u,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(u,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(u,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(u,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(u,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(u,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(u,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(u,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(u,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(u,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(u,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(u,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(u,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(u,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(u,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(u,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(u,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(u,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(u,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(u,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(u,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(u,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(u,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(u,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(u,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(u,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(u,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(u,4,0,0),0) + IfThen(ti == 
      3,-0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(u,-1,0,0) + 
      GFOffset(u,1,0,0)) - 
      0.107637859609505734449271251484*(GFOffset(u,-2,0,0) + 
      GFOffset(u,2,0,0)) + 
      0.0446428403421747932239219136542*(GFOffset(u,-3,0,0) + 
      GFOffset(u,3,0,0)),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(u,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(u,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(u,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(u,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(u,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(u,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(u,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(u,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(u,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(u,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(u,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(u,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(u,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(u,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(u,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(u,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(u,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(u,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(u,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(u,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(u,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(u,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(u,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(u,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(u,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(u,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(u,0,4,0),0) + IfThen(tj == 
      3,-0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(u,0,-1,0) + 
      GFOffset(u,0,1,0)) - 
      0.107637859609505734449271251484*(GFOffset(u,0,-2,0) + 
      GFOffset(u,0,2,0)) + 
      0.0446428403421747932239219136542*(GFOffset(u,0,-3,0) + 
      GFOffset(u,0,3,0)),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(u,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(u,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(u,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(u,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(u,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(u,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(u,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(u,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(u,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(u,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(u,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(u,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(u,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.143754339118452914716281675173*GFOffset(u,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(u,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(u,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(u,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(u,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(u,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(u,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(u,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(u,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(u,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(u,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(u,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(u,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(u,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(u,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(u,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(u,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(u,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(u,0,0,4),0) + IfThen(tk == 
      3,-0.142857207371763079734425565854*GFOffset(u,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(u,0,0,-1) + 
      GFOffset(u,0,0,1)) - 
      0.107637859609505734449271251484*(GFOffset(u,0,0,-2) + 
      GFOffset(u,0,0,2)) + 
      0.0446428403421747932239219136542*(GFOffset(u,0,0,-3) + 
      GFOffset(u,0,0,3)),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(u,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(u,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(u,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(u,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(u,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(u,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(u,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(u,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(u,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(u,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(u,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(u,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(u,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(u,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(u,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(u,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(u,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(u,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(u,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(u,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(u,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL rhorhsL CCTK_ATTRIBUTE_UNUSED = PDv11 + PDv22 + PDv33 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(rho,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(rho,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(rho,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(rho,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(rho,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(rho,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(rho,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(rho,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(rho,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(rho,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(rho,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(rho,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      3,-0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(rho,-1,0,0) + 
      GFOffset(rho,1,0,0)) - 
      0.107637859609505734449271251484*(GFOffset(rho,-2,0,0) + 
      GFOffset(rho,2,0,0)) + 
      0.0446428403421747932239219136542*(GFOffset(rho,-3,0,0) + 
      GFOffset(rho,3,0,0)),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(rho,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(rho,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(rho,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(rho,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(rho,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(rho,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(rho,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(rho,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(rho,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(rho,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(rho,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(rho,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(rho,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(rho,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(rho,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(rho,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(rho,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(rho,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(rho,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(rho,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(rho,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(rho,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(rho,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      3,-0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(rho,0,-1,0) + 
      GFOffset(rho,0,1,0)) - 
      0.107637859609505734449271251484*(GFOffset(rho,0,-2,0) + 
      GFOffset(rho,0,2,0)) + 
      0.0446428403421747932239219136542*(GFOffset(rho,0,-3,0) + 
      GFOffset(rho,0,3,0)),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(rho,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(rho,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(rho,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(rho,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(rho,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(rho,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(rho,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(rho,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(rho,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(rho,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(rho,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(rho,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(rho,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(rho,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(rho,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(rho,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(rho,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(rho,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(rho,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(rho,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(rho,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(rho,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(rho,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(rho,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      3,-0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(rho,0,0,-1) + 
      GFOffset(rho,0,0,1)) - 
      0.107637859609505734449271251484*(GFOffset(rho,0,0,-2) + 
      GFOffset(rho,0,0,2)) + 
      0.0446428403421747932239219136542*(GFOffset(rho,0,0,-3) + 
      GFOffset(rho,0,0,3)),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(rho,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(rho,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(rho,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(rho,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(rho,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(rho,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(rho,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(rho,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(rho,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(rho,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(rho,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(rho,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(rho,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(rho,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(rho,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(rho,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(rho,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(rho,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(rho,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(rho,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(rho,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(rho,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(rho,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(rho,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(rho,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(rho,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      3,-0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(rho,-1,0,0) + 
      GFOffset(rho,1,0,0)) - 
      0.107637859609505734449271251484*(GFOffset(rho,-2,0,0) + 
      GFOffset(rho,2,0,0)) + 
      0.0446428403421747932239219136542*(GFOffset(rho,-3,0,0) + 
      GFOffset(rho,3,0,0)),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(rho,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(rho,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(rho,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(rho,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(rho,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(rho,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(rho,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(rho,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(rho,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(rho,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(rho,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(rho,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(rho,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(rho,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(rho,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(rho,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(rho,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(rho,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(rho,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(rho,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(rho,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(rho,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(rho,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      3,-0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(rho,0,-1,0) + 
      GFOffset(rho,0,1,0)) - 
      0.107637859609505734449271251484*(GFOffset(rho,0,-2,0) + 
      GFOffset(rho,0,2,0)) + 
      0.0446428403421747932239219136542*(GFOffset(rho,0,-3,0) + 
      GFOffset(rho,0,3,0)),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(rho,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(rho,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(rho,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(rho,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(rho,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(rho,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(rho,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(rho,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(rho,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(rho,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(rho,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.143754339118452914716281675173*GFOffset(rho,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(rho,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(rho,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(rho,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(rho,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(rho,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(rho,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(rho,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(rho,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(rho,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(rho,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(rho,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(rho,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(rho,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(rho,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(rho,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(rho,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(rho,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      3,-0.142857207371763079734425565854*GFOffset(rho,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(rho,0,0,-1) + 
      GFOffset(rho,0,0,1)) - 
      0.107637859609505734449271251484*(GFOffset(rho,0,0,-2) + 
      GFOffset(rho,0,0,2)) + 
      0.0446428403421747932239219136542*(GFOffset(rho,0,0,-3) + 
      GFOffset(rho,0,0,3)),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(rho,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(rho,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(rho,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(rho,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(rho,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(rho,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(rho,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(rho,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(rho,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(rho,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(rho,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(rho,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(rho,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(rho,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(rho,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(rho,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(rho,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(rho,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(rho,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL v1rhsL CCTK_ATTRIBUTE_UNUSED = PDrho1 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.143754339118452914716281675173*GFOffset(v1,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v1,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(v1,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(v1,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(v1,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(v1,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(v1,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(v1,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v1,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(v1,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(v1,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(v1,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(v1,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(v1,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(v1,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v1,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(v1,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(v1,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(v1,4,0,0),0) + IfThen(ti == 
      3,-0.142857207371763079734425565854*GFOffset(v1,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v1,-1,0,0) + 
      GFOffset(v1,1,0,0)) - 
      0.107637859609505734449271251484*(GFOffset(v1,-2,0,0) + 
      GFOffset(v1,2,0,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v1,-3,0,0) + 
      GFOffset(v1,3,0,0)),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(v1,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(v1,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(v1,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(v1,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v1,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(v1,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(v1,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(v1,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(v1,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(v1,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(v1,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v1,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(v1,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(v1,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(v1,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(v1,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(v1,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(v1,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(v1,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.143754339118452914716281675173*GFOffset(v1,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v1,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(v1,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(v1,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(v1,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(v1,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(v1,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(v1,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v1,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(v1,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(v1,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(v1,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(v1,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(v1,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(v1,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v1,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(v1,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(v1,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(v1,0,4,0),0) + IfThen(tj == 
      3,-0.142857207371763079734425565854*GFOffset(v1,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v1,0,-1,0) + 
      GFOffset(v1,0,1,0)) - 
      0.107637859609505734449271251484*(GFOffset(v1,0,-2,0) + 
      GFOffset(v1,0,2,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v1,0,-3,0) + 
      GFOffset(v1,0,3,0)),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(v1,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(v1,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(v1,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(v1,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v1,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(v1,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(v1,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(v1,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(v1,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(v1,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(v1,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v1,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(v1,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(v1,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(v1,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(v1,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(v1,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(v1,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(v1,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.143754339118452914716281675173*GFOffset(v1,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v1,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(v1,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(v1,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(v1,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(v1,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(v1,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(v1,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v1,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(v1,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(v1,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(v1,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(v1,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(v1,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(v1,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v1,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(v1,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(v1,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(v1,0,0,4),0) + IfThen(tk == 
      3,-0.142857207371763079734425565854*GFOffset(v1,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v1,0,0,-1) + 
      GFOffset(v1,0,0,1)) - 
      0.107637859609505734449271251484*(GFOffset(v1,0,0,-2) + 
      GFOffset(v1,0,0,2)) + 
      0.0446428403421747932239219136542*(GFOffset(v1,0,0,-3) + 
      GFOffset(v1,0,0,3)),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(v1,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(v1,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(v1,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(v1,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v1,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(v1,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(v1,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(v1,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(v1,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(v1,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(v1,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v1,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(v1,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(v1,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(v1,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(v1,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(v1,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(v1,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(v1,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.143754339118452914716281675173*GFOffset(v1,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v1,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(v1,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(v1,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(v1,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(v1,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(v1,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(v1,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v1,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(v1,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(v1,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(v1,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(v1,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(v1,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(v1,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v1,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(v1,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(v1,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(v1,4,0,0),0) + IfThen(ti == 
      3,-0.142857207371763079734425565854*GFOffset(v1,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v1,-1,0,0) + 
      GFOffset(v1,1,0,0)) - 
      0.107637859609505734449271251484*(GFOffset(v1,-2,0,0) + 
      GFOffset(v1,2,0,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v1,-3,0,0) + 
      GFOffset(v1,3,0,0)),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(v1,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(v1,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(v1,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(v1,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v1,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(v1,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(v1,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(v1,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(v1,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(v1,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(v1,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v1,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(v1,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(v1,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(v1,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(v1,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(v1,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(v1,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(v1,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.143754339118452914716281675173*GFOffset(v1,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v1,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(v1,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(v1,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(v1,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(v1,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(v1,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(v1,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v1,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(v1,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(v1,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(v1,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(v1,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(v1,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(v1,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v1,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(v1,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(v1,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(v1,0,4,0),0) + IfThen(tj == 
      3,-0.142857207371763079734425565854*GFOffset(v1,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v1,0,-1,0) + 
      GFOffset(v1,0,1,0)) - 
      0.107637859609505734449271251484*(GFOffset(v1,0,-2,0) + 
      GFOffset(v1,0,2,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v1,0,-3,0) + 
      GFOffset(v1,0,3,0)),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(v1,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(v1,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(v1,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(v1,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v1,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(v1,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(v1,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(v1,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(v1,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(v1,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(v1,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v1,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(v1,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(v1,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(v1,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(v1,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(v1,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(v1,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(v1,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.143754339118452914716281675173*GFOffset(v1,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v1,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(v1,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(v1,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(v1,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(v1,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(v1,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(v1,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v1,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(v1,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(v1,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(v1,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(v1,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(v1,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(v1,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v1,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(v1,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(v1,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(v1,0,0,4),0) + IfThen(tk == 
      3,-0.142857207371763079734425565854*GFOffset(v1,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v1,0,0,-1) + 
      GFOffset(v1,0,0,1)) - 
      0.107637859609505734449271251484*(GFOffset(v1,0,0,-2) + 
      GFOffset(v1,0,0,2)) + 
      0.0446428403421747932239219136542*(GFOffset(v1,0,0,-3) + 
      GFOffset(v1,0,0,3)),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(v1,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(v1,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(v1,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(v1,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v1,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v1,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(v1,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(v1,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(v1,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(v1,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(v1,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(v1,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v1,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v1,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(v1,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(v1,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(v1,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(v1,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(v1,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(v1,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(v1,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL v2rhsL CCTK_ATTRIBUTE_UNUSED = PDrho2 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.143754339118452914716281675173*GFOffset(v2,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v2,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(v2,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(v2,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(v2,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(v2,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(v2,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(v2,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v2,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(v2,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(v2,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(v2,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(v2,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(v2,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(v2,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v2,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(v2,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(v2,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(v2,4,0,0),0) + IfThen(ti == 
      3,-0.142857207371763079734425565854*GFOffset(v2,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v2,-1,0,0) + 
      GFOffset(v2,1,0,0)) - 
      0.107637859609505734449271251484*(GFOffset(v2,-2,0,0) + 
      GFOffset(v2,2,0,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v2,-3,0,0) + 
      GFOffset(v2,3,0,0)),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(v2,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(v2,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(v2,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(v2,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v2,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(v2,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(v2,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(v2,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(v2,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(v2,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(v2,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v2,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(v2,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(v2,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(v2,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(v2,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(v2,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(v2,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(v2,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.143754339118452914716281675173*GFOffset(v2,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v2,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(v2,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(v2,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(v2,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(v2,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(v2,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(v2,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v2,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(v2,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(v2,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(v2,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(v2,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(v2,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(v2,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v2,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(v2,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(v2,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(v2,0,4,0),0) + IfThen(tj == 
      3,-0.142857207371763079734425565854*GFOffset(v2,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v2,0,-1,0) + 
      GFOffset(v2,0,1,0)) - 
      0.107637859609505734449271251484*(GFOffset(v2,0,-2,0) + 
      GFOffset(v2,0,2,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v2,0,-3,0) + 
      GFOffset(v2,0,3,0)),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(v2,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(v2,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(v2,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(v2,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v2,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(v2,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(v2,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(v2,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(v2,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(v2,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(v2,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v2,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(v2,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(v2,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(v2,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(v2,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(v2,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(v2,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(v2,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.143754339118452914716281675173*GFOffset(v2,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v2,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(v2,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(v2,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(v2,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(v2,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(v2,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(v2,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v2,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(v2,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(v2,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(v2,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(v2,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(v2,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(v2,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v2,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(v2,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(v2,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(v2,0,0,4),0) + IfThen(tk == 
      3,-0.142857207371763079734425565854*GFOffset(v2,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v2,0,0,-1) + 
      GFOffset(v2,0,0,1)) - 
      0.107637859609505734449271251484*(GFOffset(v2,0,0,-2) + 
      GFOffset(v2,0,0,2)) + 
      0.0446428403421747932239219136542*(GFOffset(v2,0,0,-3) + 
      GFOffset(v2,0,0,3)),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(v2,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(v2,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(v2,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(v2,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v2,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(v2,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(v2,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(v2,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(v2,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(v2,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(v2,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v2,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(v2,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(v2,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(v2,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(v2,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(v2,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(v2,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(v2,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.143754339118452914716281675173*GFOffset(v2,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v2,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(v2,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(v2,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(v2,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(v2,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(v2,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(v2,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v2,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(v2,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(v2,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(v2,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(v2,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(v2,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(v2,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v2,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(v2,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(v2,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(v2,4,0,0),0) + IfThen(ti == 
      3,-0.142857207371763079734425565854*GFOffset(v2,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v2,-1,0,0) + 
      GFOffset(v2,1,0,0)) - 
      0.107637859609505734449271251484*(GFOffset(v2,-2,0,0) + 
      GFOffset(v2,2,0,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v2,-3,0,0) + 
      GFOffset(v2,3,0,0)),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(v2,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(v2,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(v2,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(v2,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v2,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(v2,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(v2,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(v2,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(v2,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(v2,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(v2,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v2,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(v2,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(v2,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(v2,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(v2,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(v2,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(v2,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(v2,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.143754339118452914716281675173*GFOffset(v2,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v2,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(v2,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(v2,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(v2,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(v2,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(v2,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(v2,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v2,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(v2,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(v2,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(v2,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(v2,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(v2,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(v2,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v2,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(v2,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(v2,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(v2,0,4,0),0) + IfThen(tj == 
      3,-0.142857207371763079734425565854*GFOffset(v2,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v2,0,-1,0) + 
      GFOffset(v2,0,1,0)) - 
      0.107637859609505734449271251484*(GFOffset(v2,0,-2,0) + 
      GFOffset(v2,0,2,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v2,0,-3,0) + 
      GFOffset(v2,0,3,0)),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(v2,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(v2,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(v2,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(v2,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v2,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(v2,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(v2,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(v2,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(v2,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(v2,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(v2,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v2,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(v2,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(v2,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(v2,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(v2,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(v2,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(v2,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(v2,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.143754339118452914716281675173*GFOffset(v2,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v2,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(v2,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(v2,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(v2,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(v2,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(v2,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(v2,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v2,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(v2,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(v2,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(v2,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(v2,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(v2,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(v2,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v2,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(v2,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(v2,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(v2,0,0,4),0) + IfThen(tk == 
      3,-0.142857207371763079734425565854*GFOffset(v2,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v2,0,0,-1) + 
      GFOffset(v2,0,0,1)) - 
      0.107637859609505734449271251484*(GFOffset(v2,0,0,-2) + 
      GFOffset(v2,0,0,2)) + 
      0.0446428403421747932239219136542*(GFOffset(v2,0,0,-3) + 
      GFOffset(v2,0,0,3)),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(v2,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(v2,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(v2,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(v2,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v2,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v2,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(v2,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(v2,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(v2,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(v2,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(v2,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(v2,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v2,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v2,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(v2,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(v2,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(v2,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(v2,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(v2,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(v2,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(v2,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL v3rhsL CCTK_ATTRIBUTE_UNUSED = PDrho3 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.143754339118452914716281675173*GFOffset(v3,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v3,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(v3,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(v3,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(v3,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(v3,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(v3,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(v3,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v3,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(v3,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(v3,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(v3,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(v3,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(v3,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(v3,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v3,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(v3,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(v3,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(v3,4,0,0),0) + IfThen(ti == 
      3,-0.142857207371763079734425565854*GFOffset(v3,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v3,-1,0,0) + 
      GFOffset(v3,1,0,0)) - 
      0.107637859609505734449271251484*(GFOffset(v3,-2,0,0) + 
      GFOffset(v3,2,0,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v3,-3,0,0) + 
      GFOffset(v3,3,0,0)),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(v3,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(v3,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(v3,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(v3,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v3,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(v3,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(v3,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(v3,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(v3,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(v3,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(v3,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v3,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(v3,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(v3,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(v3,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(v3,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(v3,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(v3,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(v3,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.143754339118452914716281675173*GFOffset(v3,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v3,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(v3,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(v3,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(v3,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(v3,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(v3,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(v3,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v3,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(v3,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(v3,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(v3,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(v3,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(v3,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(v3,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v3,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(v3,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(v3,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(v3,0,4,0),0) + IfThen(tj == 
      3,-0.142857207371763079734425565854*GFOffset(v3,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v3,0,-1,0) + 
      GFOffset(v3,0,1,0)) - 
      0.107637859609505734449271251484*(GFOffset(v3,0,-2,0) + 
      GFOffset(v3,0,2,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v3,0,-3,0) + 
      GFOffset(v3,0,3,0)),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(v3,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(v3,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(v3,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(v3,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v3,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(v3,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(v3,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(v3,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(v3,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(v3,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(v3,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v3,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(v3,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(v3,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(v3,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(v3,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(v3,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(v3,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(v3,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.143754339118452914716281675173*GFOffset(v3,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v3,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(v3,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(v3,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(v3,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(v3,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(v3,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(v3,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v3,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(v3,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(v3,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(v3,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(v3,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(v3,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(v3,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v3,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(v3,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(v3,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(v3,0,0,4),0) + IfThen(tk == 
      3,-0.142857207371763079734425565854*GFOffset(v3,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v3,0,0,-1) + 
      GFOffset(v3,0,0,1)) - 
      0.107637859609505734449271251484*(GFOffset(v3,0,0,-2) + 
      GFOffset(v3,0,0,2)) + 
      0.0446428403421747932239219136542*(GFOffset(v3,0,0,-3) + 
      GFOffset(v3,0,0,3)),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(v3,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(v3,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(v3,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(v3,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v3,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(v3,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(v3,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(v3,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(v3,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(v3,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(v3,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v3,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(v3,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(v3,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(v3,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(v3,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(v3,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(v3,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(v3,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.143754339118452914716281675173*GFOffset(v3,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v3,1,0,0) - 
      0.431421930043719746826057811667*GFOffset(v3,2,0,0) + 
      0.457142685103869882612960395819*GFOffset(v3,3,0,0) - 
      0.428888831624723688103151048028*GFOffset(v3,4,0,0) + 
      0.342645359145655785119617106813*GFOffset(v3,5,0,0) - 
      0.141960036199471997613277642481*GFOffset(v3,6,0,0),0) + IfThen(ti == 
      1,0.0595589929620380495473889622657*GFOffset(v3,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v3,1,0,0) - 
      0.189600187882562103735381684601*GFOffset(v3,2,0,0) + 
      0.17797105147260177854463810828*GFOffset(v3,3,0,0) - 
      0.142238766214397370085444289993*GFOffset(v3,4,0,0) + 
      0.0589411503329075949898187754065*GFOffset(v3,5,0,0),0) + IfThen(ti == 
      2,-0.0475833728043227168414005120136*GFOffset(v3,-2,0,0) + 
      0.114670550313455647310066503505*GFOffset(v3,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v3,1,0,0) - 
      0.142659954742437281402047305808*GFOffset(v3,2,0,0) + 
      0.11411129074706755523125092284*GFOffset(v3,3,0,0) - 
      0.0473039865283192804053205511371*GFOffset(v3,4,0,0),0) + IfThen(ti == 
      3,-0.142857207371763079734425565854*GFOffset(v3,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v3,-1,0,0) + 
      GFOffset(v3,1,0,0)) - 
      0.107637859609505734449271251484*(GFOffset(v3,-2,0,0) + 
      GFOffset(v3,2,0,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v3,-3,0,0) + 
      GFOffset(v3,3,0,0)),0) + IfThen(ti == 
      4,-0.0473039865283192804053205511371*GFOffset(v3,-4,0,0) + 
      0.11411129074706755523125092284*GFOffset(v3,-3,0,0) - 
      0.142659954742437281402047305808*GFOffset(v3,-2,0,0) + 
      0.151819849973301989649999216565*GFOffset(v3,-1,0,0) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v3,1,0,0) - 
      0.0475833728043227168414005120136*GFOffset(v3,2,0,0),0) + IfThen(ti == 
      5,0.0589411503329075949898187754065*GFOffset(v3,-5,0,0) - 
      0.142238766214397370085444289993*GFOffset(v3,-4,0,0) + 
      0.17797105147260177854463810828*GFOffset(v3,-3,0,0) - 
      0.189600187882562103735381684601*GFOffset(v3,-2,0,0) + 
      0.178843287799301622413927116574*GFOffset(v3,-1,0,0) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v3,1,0,0),0) + IfThen(ti == 
      6,-0.141960036199471997613277642481*GFOffset(v3,-6,0,0) + 
      0.342645359145655785119617106813*GFOffset(v3,-5,0,0) - 
      0.428888831624723688103151048028*GFOffset(v3,-4,0,0) + 
      0.457142685103869882612960395819*GFOffset(v3,-3,0,0) - 
      0.431421930043719746826057811667*GFOffset(v3,-2,0,0) + 
      0.346237092736842679526190674716*GFOffset(v3,-1,0,0) - 
      0.143754339118452914716281675173*GFOffset(v3,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.143754339118452914716281675173*GFOffset(v3,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v3,0,1,0) - 
      0.431421930043719746826057811667*GFOffset(v3,0,2,0) + 
      0.457142685103869882612960395819*GFOffset(v3,0,3,0) - 
      0.428888831624723688103151048028*GFOffset(v3,0,4,0) + 
      0.342645359145655785119617106813*GFOffset(v3,0,5,0) - 
      0.141960036199471997613277642481*GFOffset(v3,0,6,0),0) + IfThen(tj == 
      1,0.0595589929620380495473889622657*GFOffset(v3,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v3,0,1,0) - 
      0.189600187882562103735381684601*GFOffset(v3,0,2,0) + 
      0.17797105147260177854463810828*GFOffset(v3,0,3,0) - 
      0.142238766214397370085444289993*GFOffset(v3,0,4,0) + 
      0.0589411503329075949898187754065*GFOffset(v3,0,5,0),0) + IfThen(tj == 
      2,-0.0475833728043227168414005120136*GFOffset(v3,0,-2,0) + 
      0.114670550313455647310066503505*GFOffset(v3,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v3,0,1,0) - 
      0.142659954742437281402047305808*GFOffset(v3,0,2,0) + 
      0.11411129074706755523125092284*GFOffset(v3,0,3,0) - 
      0.0473039865283192804053205511371*GFOffset(v3,0,4,0),0) + IfThen(tj == 
      3,-0.142857207371763079734425565854*GFOffset(v3,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v3,0,-1,0) + 
      GFOffset(v3,0,1,0)) - 
      0.107637859609505734449271251484*(GFOffset(v3,0,-2,0) + 
      GFOffset(v3,0,2,0)) + 
      0.0446428403421747932239219136542*(GFOffset(v3,0,-3,0) + 
      GFOffset(v3,0,3,0)),0) + IfThen(tj == 
      4,-0.0473039865283192804053205511371*GFOffset(v3,0,-4,0) + 
      0.11411129074706755523125092284*GFOffset(v3,0,-3,0) - 
      0.142659954742437281402047305808*GFOffset(v3,0,-2,0) + 
      0.151819849973301989649999216565*GFOffset(v3,0,-1,0) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v3,0,1,0) - 
      0.0475833728043227168414005120136*GFOffset(v3,0,2,0),0) + IfThen(tj == 
      5,0.0589411503329075949898187754065*GFOffset(v3,0,-5,0) - 
      0.142238766214397370085444289993*GFOffset(v3,0,-4,0) + 
      0.17797105147260177854463810828*GFOffset(v3,0,-3,0) - 
      0.189600187882562103735381684601*GFOffset(v3,0,-2,0) + 
      0.178843287799301622413927116574*GFOffset(v3,0,-1,0) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v3,0,1,0),0) + IfThen(tj == 
      6,-0.141960036199471997613277642481*GFOffset(v3,0,-6,0) + 
      0.342645359145655785119617106813*GFOffset(v3,0,-5,0) - 
      0.428888831624723688103151048028*GFOffset(v3,0,-4,0) + 
      0.457142685103869882612960395819*GFOffset(v3,0,-3,0) - 
      0.431421930043719746826057811667*GFOffset(v3,0,-2,0) + 
      0.346237092736842679526190674716*GFOffset(v3,0,-1,0) - 
      0.143754339118452914716281675173*GFOffset(v3,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.143754339118452914716281675173*GFOffset(v3,0,0,0) + 
      0.346237092736842679526190674716*GFOffset(v3,0,0,1) - 
      0.431421930043719746826057811667*GFOffset(v3,0,0,2) + 
      0.457142685103869882612960395819*GFOffset(v3,0,0,3) - 
      0.428888831624723688103151048028*GFOffset(v3,0,0,4) + 
      0.342645359145655785119617106813*GFOffset(v3,0,0,5) - 
      0.141960036199471997613277642481*GFOffset(v3,0,0,6),0) + IfThen(tk == 
      1,0.0595589929620380495473889622657*GFOffset(v3,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.178843287799301622413927116574*GFOffset(v3,0,0,1) - 
      0.189600187882562103735381684601*GFOffset(v3,0,0,2) + 
      0.17797105147260177854463810828*GFOffset(v3,0,0,3) - 
      0.142238766214397370085444289993*GFOffset(v3,0,0,4) + 
      0.0589411503329075949898187754065*GFOffset(v3,0,0,5),0) + IfThen(tk == 
      2,-0.0475833728043227168414005120136*GFOffset(v3,0,0,-2) + 
      0.114670550313455647310066503505*GFOffset(v3,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.151819849973301989649999216565*GFOffset(v3,0,0,1) - 
      0.142659954742437281402047305808*GFOffset(v3,0,0,2) + 
      0.11411129074706755523125092284*GFOffset(v3,0,0,3) - 
      0.0473039865283192804053205511371*GFOffset(v3,0,0,4),0) + IfThen(tk == 
      3,-0.142857207371763079734425565854*GFOffset(v3,0,0,0) + 
      0.134423622953212481092562120757*(GFOffset(v3,0,0,-1) + 
      GFOffset(v3,0,0,1)) - 
      0.107637859609505734449271251484*(GFOffset(v3,0,0,-2) + 
      GFOffset(v3,0,0,2)) + 
      0.0446428403421747932239219136542*(GFOffset(v3,0,0,-3) + 
      GFOffset(v3,0,0,3)),0) + IfThen(tk == 
      4,-0.0473039865283192804053205511371*GFOffset(v3,0,0,-4) + 
      0.11411129074706755523125092284*GFOffset(v3,0,0,-3) - 
      0.142659954742437281402047305808*GFOffset(v3,0,0,-2) + 
      0.151819849973301989649999216565*GFOffset(v3,0,0,-1) - 
      0.143054376958745913542548273952*GFOffset(v3,0,0,0) + 
      0.114670550313455647310066503505*GFOffset(v3,0,0,1) - 
      0.0475833728043227168414005120136*GFOffset(v3,0,0,2),0) + IfThen(tk == 
      5,0.0589411503329075949898187754065*GFOffset(v3,0,0,-5) - 
      0.142238766214397370085444289993*GFOffset(v3,0,0,-4) + 
      0.17797105147260177854463810828*GFOffset(v3,0,0,-3) - 
      0.189600187882562103735381684601*GFOffset(v3,0,0,-2) + 
      0.178843287799301622413927116574*GFOffset(v3,0,0,-1) - 
      0.143475528469889571674946987932*GFOffset(v3,0,0,0) + 
      0.0595589929620380495473889622657*GFOffset(v3,0,0,1),0) + IfThen(tk == 
      6,-0.141960036199471997613277642481*GFOffset(v3,0,0,-6) + 
      0.342645359145655785119617106813*GFOffset(v3,0,0,-5) - 
      0.428888831624723688103151048028*GFOffset(v3,0,0,-4) + 
      0.457142685103869882612960395819*GFOffset(v3,0,0,-3) - 
      0.431421930043719746826057811667*GFOffset(v3,0,0,-2) + 
      0.346237092736842679526190674716*GFOffset(v3,0,0,-1) - 
      0.143754339118452914716281675173*GFOffset(v3,0,0,0),0))*pow(dz,-1)));
    /* Copy local copies back to grid functions */
    rhorhs[index] = rhorhsL;
    urhs[index] = urhsL;
    v1rhs[index] = v1rhsL;
    v2rhs[index] = v2rhsL;
    v3rhs[index] = v3rhsL;
  }
  CCTK_ENDLOOP3(ML_WaveToy_DG6_RHSFirstOrder);
}
extern "C" void ML_WaveToy_DG6_RHSFirstOrder(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_WaveToy_DG6_RHSFirstOrder_Body");
  }
  if (cctk_iteration % ML_WaveToy_DG6_RHSFirstOrder_calc_every != ML_WaveToy_DG6_RHSFirstOrder_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_WaveToy_DG6::WT_detJ",
    "ML_WaveToy_DG6::WT_rho",
    "ML_WaveToy_DG6::WT_rhorhs",
    "ML_WaveToy_DG6::WT_u",
    "ML_WaveToy_DG6::WT_urhs",
    "ML_WaveToy_DG6::WT_v",
    "ML_WaveToy_DG6::WT_vrhs"};
  AssertGroupStorage(cctkGH, "ML_WaveToy_DG6_RHSFirstOrder", 7, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_WaveToy_DG6_RHSFirstOrder_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_WaveToy_DG6_RHSFirstOrder_Body");
  }
}

} // namespace ML_WaveToy_DG6
