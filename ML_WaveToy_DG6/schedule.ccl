# File produced by Kranc

STORAGE: WT_u[timelevels]

STORAGE: WT_rho[timelevels]

STORAGE: WT_v[timelevels]

STORAGE: WT_du[other_timelevels]

STORAGE: WT_w[other_timelevels]

STORAGE: WT_erru[other_timelevels]

STORAGE: WT_weight[other_timelevels]

STORAGE: WT_detJ[other_timelevels]

STORAGE: WT_errenergy[other_timelevels]

STORAGE: WT_energy[other_timelevels]

STORAGE: WT_rhorhs[rhs_timelevels]

STORAGE: WT_urhs[rhs_timelevels]

STORAGE: WT_vrhs[rhs_timelevels]
schedule ML_WaveToy_DG6_Startup at STARTUP
{
  LANG: C
  OPTIONS: meta
} "create banner"

schedule ML_WaveToy_DG6_RegisterSymmetries in SymmetryRegister
{
  LANG: C
  OPTIONS: meta
} "register symmetries"

schedule ML_WaveToy_DG6_SetupCoordinates IN ML_WaveToy_DG6_CalcCoordinatesGroup
{
  LANG: C
  WRITES: grid::r(Everywhere)
  WRITES: grid::x(Everywhere)
  WRITES: grid::y(Everywhere)
  WRITES: grid::z(Everywhere)
} "ML_WaveToy_DG6_SetupCoordinates"

schedule ML_WaveToy_DG6_SetupWeights AS DGFE_SetupWeights IN SetupMask
{
  LANG: C
  WRITES: ML_WaveToy_DG6::myDetJ(Everywhere)
  WRITES: ML_WaveToy_DG6::weight(Everywhere)
} "ML_WaveToy_DG6_SetupWeights"

if (CCTK_EQUALS(initial_data, "Gaussian"))
{
  schedule ML_WaveToy_DG6_InitialGaussian IN ML_WaveToy_DG6_CalcInitialGroup
  {
    LANG: C
    READS: grid::x(Everywhere)
    READS: grid::y(Everywhere)
    READS: grid::z(Everywhere)
    READS: grid::r(Everywhere)
    WRITES: ML_WaveToy_DG6::rho(Everywhere)
    WRITES: ML_WaveToy_DG6::u(Everywhere)
    WRITES: ML_WaveToy_DG6::v1(Everywhere)
    WRITES: ML_WaveToy_DG6::v2(Everywhere)
    WRITES: ML_WaveToy_DG6::v3(Everywhere)
  } "ML_WaveToy_DG6_InitialGaussian"
}

if (CCTK_EQUALS(boundary_condition, "Gaussian"))
{
  schedule ML_WaveToy_DG6_BoundaryGaussian IN ML_WaveToy_DG6_CalcBoundaryGroup
  {
    LANG: C
    SYNC: WT_rho
    SYNC: WT_u
    SYNC: WT_v
    READS: grid::x(Everywhere)
    READS: grid::y(Everywhere)
    READS: grid::z(Everywhere)
    READS: grid::r(Everywhere)
    WRITES: ML_WaveToy_DG6::rho(Boundary)
    WRITES: ML_WaveToy_DG6::u(Boundary)
    WRITES: ML_WaveToy_DG6::v1(Boundary)
    WRITES: ML_WaveToy_DG6::v2(Boundary)
    WRITES: ML_WaveToy_DG6::v3(Boundary)
  } "ML_WaveToy_DG6_BoundaryGaussian"
}

if (CCTK_EQUALS(initial_data, "Gaussian"))
{
  schedule ML_WaveToy_DG6_ErrorGaussian AT analysis
  {
    LANG: C
    READS: grid::x(Everywhere)
    READS: grid::y(Everywhere)
    READS: grid::z(Everywhere)
    READS: grid::r(Everywhere)
    READS: ML_WaveToy_DG6::rho(Everywhere)
    READS: ML_WaveToy_DG6::u(Everywhere)
    READS: ML_WaveToy_DG6::v1(Everywhere)
    READS: ML_WaveToy_DG6::v2(Everywhere)
    READS: ML_WaveToy_DG6::v3(Everywhere)
    WRITES: ML_WaveToy_DG6::errenergy(Everywhere)
    WRITES: ML_WaveToy_DG6::erru(Everywhere)
  } "ML_WaveToy_DG6_ErrorGaussian"
}

if (CCTK_EQUALS(initial_data, "standing"))
{
  schedule ML_WaveToy_DG6_InitialStanding IN ML_WaveToy_DG6_CalcInitialGroup
  {
    LANG: C
    READS: grid::x(Everywhere)
    READS: grid::y(Everywhere)
    READS: grid::z(Everywhere)
    WRITES: ML_WaveToy_DG6::rho(Everywhere)
    WRITES: ML_WaveToy_DG6::u(Everywhere)
    WRITES: ML_WaveToy_DG6::v1(Everywhere)
    WRITES: ML_WaveToy_DG6::v2(Everywhere)
    WRITES: ML_WaveToy_DG6::v3(Everywhere)
  } "ML_WaveToy_DG6_InitialStanding"
}

if (CCTK_EQUALS(boundary_condition, "standing"))
{
  schedule ML_WaveToy_DG6_BoundaryStanding IN ML_WaveToy_DG6_CalcBoundaryGroup
  {
    LANG: C
    SYNC: WT_rho
    SYNC: WT_u
    SYNC: WT_v
    READS: grid::x(Everywhere)
    READS: grid::y(Everywhere)
    READS: grid::z(Everywhere)
    WRITES: ML_WaveToy_DG6::rho(Boundary)
    WRITES: ML_WaveToy_DG6::u(Boundary)
    WRITES: ML_WaveToy_DG6::v1(Boundary)
    WRITES: ML_WaveToy_DG6::v2(Boundary)
    WRITES: ML_WaveToy_DG6::v3(Boundary)
  } "ML_WaveToy_DG6_BoundaryStanding"
}

if (CCTK_EQUALS(rhs_boundary_condition, "standing"))
{
  schedule ML_WaveToy_DG6_RHSBoundaryStanding IN ML_WaveToy_DG6_CalcRHSGroup
  {
    LANG: C
    SYNC: WT_rhorhs
    SYNC: WT_urhs
    SYNC: WT_vrhs
    READS: grid::x(Everywhere)
    READS: grid::y(Everywhere)
    READS: grid::z(Everywhere)
    WRITES: ML_WaveToy_DG6::rhorhs(Boundary)
    WRITES: ML_WaveToy_DG6::urhs(Boundary)
    WRITES: ML_WaveToy_DG6::v1rhs(Boundary)
    WRITES: ML_WaveToy_DG6::v2rhs(Boundary)
    WRITES: ML_WaveToy_DG6::v3rhs(Boundary)
  } "ML_WaveToy_DG6_RHSBoundaryStanding"
}

if (CCTK_EQUALS(initial_data, "sine"))
{
  schedule ML_WaveToy_DG6_InitialSine IN ML_WaveToy_DG6_CalcInitialGroup
  {
    LANG: C
    READS: grid::x(Everywhere)
    READS: grid::y(Everywhere)
    READS: grid::z(Everywhere)
    WRITES: ML_WaveToy_DG6::rho(Everywhere)
    WRITES: ML_WaveToy_DG6::u(Everywhere)
    WRITES: ML_WaveToy_DG6::v1(Everywhere)
    WRITES: ML_WaveToy_DG6::v2(Everywhere)
    WRITES: ML_WaveToy_DG6::v3(Everywhere)
  } "ML_WaveToy_DG6_InitialSine"
}

if (CCTK_EQUALS(initial_data, "sinex"))
{
  schedule ML_WaveToy_DG6_InitialSineX IN ML_WaveToy_DG6_CalcInitialGroup
  {
    LANG: C
    READS: grid::x(Everywhere)
    WRITES: ML_WaveToy_DG6::rho(Everywhere)
    WRITES: ML_WaveToy_DG6::u(Everywhere)
    WRITES: ML_WaveToy_DG6::v1(Everywhere)
    WRITES: ML_WaveToy_DG6::v2(Everywhere)
    WRITES: ML_WaveToy_DG6::v3(Everywhere)
  } "ML_WaveToy_DG6_InitialSineX"
}

if (CCTK_EQUALS(boundary_condition, "sinex"))
{
  schedule ML_WaveToy_DG6_BoundarySineX IN ML_WaveToy_DG6_CalcBoundaryGroup
  {
    LANG: C
    SYNC: WT_rho
    SYNC: WT_u
    SYNC: WT_v
    READS: grid::x(Everywhere)
    WRITES: ML_WaveToy_DG6::rho(Boundary)
    WRITES: ML_WaveToy_DG6::u(Boundary)
    WRITES: ML_WaveToy_DG6::v1(Boundary)
    WRITES: ML_WaveToy_DG6::v2(Boundary)
    WRITES: ML_WaveToy_DG6::v3(Boundary)
  } "ML_WaveToy_DG6_BoundarySineX"
}

if (CCTK_EQUALS(dependent_boundary_condition, "sinex"))
{
  schedule ML_WaveToy_DG6_DependentBoundarySineX IN ML_WaveToy_DG6_CalcDependentGroup
  {
    LANG: C
    SYNC: WT_du
    READS: grid::x(Everywhere)
    WRITES: ML_WaveToy_DG6::du1(Boundary)
    WRITES: ML_WaveToy_DG6::du2(Boundary)
    WRITES: ML_WaveToy_DG6::du3(Boundary)
  } "ML_WaveToy_DG6_DependentBoundarySineX"
}

if (CCTK_EQUALS(initial_data, "sinex"))
{
  schedule ML_WaveToy_DG6_ErrorSineX AT analysis
  {
    LANG: C
    READS: grid::x(Everywhere)
    READS: ML_WaveToy_DG6::rho(Everywhere)
    READS: ML_WaveToy_DG6::u(Everywhere)
    READS: ML_WaveToy_DG6::v1(Everywhere)
    READS: ML_WaveToy_DG6::v2(Everywhere)
    READS: ML_WaveToy_DG6::v3(Everywhere)
    WRITES: ML_WaveToy_DG6::errenergy(Everywhere)
    WRITES: ML_WaveToy_DG6::erru(Everywhere)
  } "ML_WaveToy_DG6_ErrorSineX"
}

if (CCTK_EQUALS(boundary_condition, "zero"))
{
  schedule ML_WaveToy_DG6_BoundaryZero IN ML_WaveToy_DG6_CalcBoundaryGroup
  {
    LANG: C
    SYNC: WT_rho
    SYNC: WT_u
    SYNC: WT_v
    WRITES: ML_WaveToy_DG6::rho(Boundary)
    WRITES: ML_WaveToy_DG6::u(Boundary)
    WRITES: ML_WaveToy_DG6::v1(Boundary)
    WRITES: ML_WaveToy_DG6::v2(Boundary)
    WRITES: ML_WaveToy_DG6::v3(Boundary)
  } "ML_WaveToy_DG6_BoundaryZero"
}

if (CCTK_EQUALS(formulation, "first_order"))
{
  schedule ML_WaveToy_DG6_DependentFirstOrder IN ML_WaveToy_DG6_CalcDependentGroup
  {
    LANG: C
    SYNC: WT_du
    WRITES: ML_WaveToy_DG6::du1(Interior)
    WRITES: ML_WaveToy_DG6::du2(Interior)
    WRITES: ML_WaveToy_DG6::du3(Interior)
  } "ML_WaveToy_DG6_DependentFirstOrder"
}

if (CCTK_EQUALS(formulation, "second_order"))
{
  schedule ML_WaveToy_DG6_DependentSecondOrder IN ML_WaveToy_DG6_CalcDependentGroup
  {
    LANG: C
    SYNC: WT_du
    READS: ML_WaveToy_DG6::u(Everywhere)
    WRITES: ML_WaveToy_DG6::du1(Interior)
    WRITES: ML_WaveToy_DG6::du2(Interior)
    WRITES: ML_WaveToy_DG6::du3(Interior)
  } "ML_WaveToy_DG6_DependentSecondOrder"
}

if (CCTK_EQUALS(dependent_boundary_condition, "zero"))
{
  schedule ML_WaveToy_DG6_DependentSecondOrderBoundary IN ML_WaveToy_DG6_CalcDependentGroup
  {
    LANG: C
    SYNC: WT_du
    WRITES: ML_WaveToy_DG6::du1(Boundary)
    WRITES: ML_WaveToy_DG6::du2(Boundary)
    WRITES: ML_WaveToy_DG6::du3(Boundary)
  } "ML_WaveToy_DG6_DependentSecondOrderBoundary"
}

if (CCTK_EQUALS(formulation, "first_order"))
{
  schedule ML_WaveToy_DG6_RHSFirstOrder IN ML_WaveToy_DG6_CalcRHSGroup
  {
    LANG: C
    SYNC: WT_rhorhs
    SYNC: WT_urhs
    SYNC: WT_vrhs
    READS: ML_WaveToy_DG6::myDetJ(Everywhere)
    READS: ML_WaveToy_DG6::rho(Everywhere)
    READS: ML_WaveToy_DG6::u(Everywhere)
    READS: ML_WaveToy_DG6::v1(Everywhere)
    READS: ML_WaveToy_DG6::v2(Everywhere)
    READS: ML_WaveToy_DG6::v3(Everywhere)
    WRITES: ML_WaveToy_DG6::rhorhs(Interior)
    WRITES: ML_WaveToy_DG6::urhs(Interior)
    WRITES: ML_WaveToy_DG6::v1rhs(Interior)
    WRITES: ML_WaveToy_DG6::v2rhs(Interior)
    WRITES: ML_WaveToy_DG6::v3rhs(Interior)
  } "ML_WaveToy_DG6_RHSFirstOrder"
}

if (CCTK_EQUALS(formulation, "second_order"))
{
  schedule ML_WaveToy_DG6_RHSSecondOrder IN ML_WaveToy_DG6_CalcRHSGroup
  {
    LANG: C
    SYNC: WT_rhorhs
    SYNC: WT_urhs
    SYNC: WT_vrhs
    READS: ML_WaveToy_DG6::myDetJ(Everywhere)
    READS: ML_WaveToy_DG6::du1(Everywhere)
    READS: ML_WaveToy_DG6::du2(Everywhere)
    READS: ML_WaveToy_DG6::du3(Everywhere)
    READS: ML_WaveToy_DG6::rho(Everywhere)
    READS: ML_WaveToy_DG6::u(Everywhere)
    WRITES: ML_WaveToy_DG6::rhorhs(Interior)
    WRITES: ML_WaveToy_DG6::urhs(Interior)
    WRITES: ML_WaveToy_DG6::v1rhs(Interior)
    WRITES: ML_WaveToy_DG6::v2rhs(Interior)
    WRITES: ML_WaveToy_DG6::v3rhs(Interior)
  } "ML_WaveToy_DG6_RHSSecondOrder"
}

if (CCTK_EQUALS(rhs_boundary_condition, "zero"))
{
  schedule ML_WaveToy_DG6_RHSBoundaryZero IN ML_WaveToy_DG6_CalcRHSGroup
  {
    LANG: C
    SYNC: WT_rhorhs
    SYNC: WT_urhs
    SYNC: WT_vrhs
    WRITES: ML_WaveToy_DG6::rhorhs(Boundary)
    WRITES: ML_WaveToy_DG6::urhs(Boundary)
    WRITES: ML_WaveToy_DG6::v1rhs(Boundary)
    WRITES: ML_WaveToy_DG6::v2rhs(Boundary)
    WRITES: ML_WaveToy_DG6::v3rhs(Boundary)
  } "ML_WaveToy_DG6_RHSBoundaryZero"
}

if (CCTK_EQUALS(formulation, "first_order"))
{
  schedule ML_WaveToy_DG6_ConstraintsFirstOrder IN ML_WaveToy_DG6_CalcConstraintsGroup
  {
    LANG: C
    SYNC: WT_w
    READS: ML_WaveToy_DG6::v1(Everywhere)
    READS: ML_WaveToy_DG6::v2(Everywhere)
    READS: ML_WaveToy_DG6::v3(Everywhere)
    WRITES: ML_WaveToy_DG6::w1(Interior)
    WRITES: ML_WaveToy_DG6::w2(Interior)
    WRITES: ML_WaveToy_DG6::w3(Interior)
  } "ML_WaveToy_DG6_ConstraintsFirstOrder"
}

if (CCTK_EQUALS(formulation, "second_order"))
{
  schedule ML_WaveToy_DG6_ConstraintsSecondOrder IN ML_WaveToy_DG6_CalcConstraintsGroup
  {
    LANG: C
    SYNC: WT_w
    READS: ML_WaveToy_DG6::du1(Everywhere)
    READS: ML_WaveToy_DG6::du2(Everywhere)
    READS: ML_WaveToy_DG6::du3(Everywhere)
    WRITES: ML_WaveToy_DG6::w1(Interior)
    WRITES: ML_WaveToy_DG6::w2(Interior)
    WRITES: ML_WaveToy_DG6::w3(Interior)
  } "ML_WaveToy_DG6_ConstraintsSecondOrder"
}

schedule ML_WaveToy_DG6_ConstraintsBoundary IN ML_WaveToy_DG6_CalcConstraintsGroup
{
  LANG: C
  SYNC: WT_w
  WRITES: ML_WaveToy_DG6::w1(Boundary)
  WRITES: ML_WaveToy_DG6::w2(Boundary)
  WRITES: ML_WaveToy_DG6::w3(Boundary)
} "ML_WaveToy_DG6_ConstraintsBoundary"

if (CCTK_EQUALS(formulation, "first_order"))
{
  schedule ML_WaveToy_DG6_EnergyFirstOrder IN ML_WaveToy_DG6_CalcEnergyGroup
  {
    LANG: C
    SYNC: WT_energy
    READS: ML_WaveToy_DG6::rho(Everywhere)
    READS: ML_WaveToy_DG6::v1(Everywhere)
    READS: ML_WaveToy_DG6::v2(Everywhere)
    READS: ML_WaveToy_DG6::v3(Everywhere)
    WRITES: ML_WaveToy_DG6::energy(Interior)
  } "ML_WaveToy_DG6_EnergyFirstOrder"
}

if (CCTK_EQUALS(formulation, "second_order"))
{
  schedule ML_WaveToy_DG6_EnergySecondOrder IN ML_WaveToy_DG6_CalcEnergyGroup
  {
    LANG: C
    SYNC: WT_energy
    READS: ML_WaveToy_DG6::du1(Everywhere)
    READS: ML_WaveToy_DG6::du2(Everywhere)
    READS: ML_WaveToy_DG6::du3(Everywhere)
    READS: ML_WaveToy_DG6::rho(Everywhere)
    WRITES: ML_WaveToy_DG6::energy(Interior)
  } "ML_WaveToy_DG6_EnergySecondOrder"
}

schedule ML_WaveToy_DG6_EnergyBoundary IN ML_WaveToy_DG6_CalcEnergyGroup
{
  LANG: C
  SYNC: WT_energy
  WRITES: ML_WaveToy_DG6::energy(Boundary)
} "ML_WaveToy_DG6_EnergyBoundary"

schedule ML_WaveToy_DG6_SelectBoundConds in MoL_PostStep
{
  LANG: C
  OPTIONS: level
  SYNC: WT_u
  SYNC: WT_rho
  SYNC: WT_v
} "select boundary conditions"

schedule ML_WaveToy_DG6_CheckBoundaries at BASEGRID
{
  LANG: C
  OPTIONS: meta
} "check boundaries treatment"

schedule ML_WaveToy_DG6_RegisterVars in MoL_Register
{
  LANG: C
  OPTIONS: meta
} "Register Variables for MoL"
schedule group ApplyBCs as ML_WaveToy_DG6_ApplyBCs in MoL_PostStep after ML_WaveToy_DG6_SelectBoundConds
{
} "Apply boundary conditions controlled by thorn Boundary"



# Parameter checking

SCHEDULE ML_WaveToy_DG6_ParamCheck AT paramcheck
{
  LANG: C
} "Check parameters"



# Coordinates

SCHEDULE GROUP ML_WaveToy_DG6_CalcCoordinatesGroup IN ML_WaveToy_DG6_CoordinatesGroup
{
} "Set up DGFE intra-element coordinates"

SCHEDULE GROUP ML_WaveToy_DG6_CoordinatesGroup AS DGFE_SetupCoordinates AT basegrid AFTER (SpatialCoordinates MultiPatch_SpatialCoordinates Interpolate2Init Interpolate2Test)
{
} "Set up DGFE intra-element coordinates"

SCHEDULE ML_WaveToy_DG6_ConvertCoordinates IN ML_WaveToy_DG6_CoordinatesGroup AFTER ML_WaveToy_DG6_CalcCoordinatesGroup
{
  LANG: C
} "Convert DGFE coordinates to multi-block coordinates"

SCHEDULE ML_WaveToy_DG6_SetupMask AS DGFE_SetupMask AFTER DGFE_SetupWeights IN SetupMask
{
  LANG: C
} "Set up DGFE reduction mask"



# Initial conditions

SCHEDULE GROUP ML_WaveToy_DG6_CalcInitialGroup IN ML_WaveToy_DG6_InitialGroup
{
} "Calculate initial conditions"

SCHEDULE GROUP ML_WaveToy_DG6_InitialGroup AT initial
{
} "Calculate initial conditions"



# Boundary conditions for the state vector

SCHEDULE GROUP ML_WaveToy_DG6_CalcBoundaryGroup IN MoL_PostStep BEFORE ML_WaveToy_DG6_SelectBoundConds
{
} "Apply boundary conditions"

# Note: Kranc applies boundary conditions via two schedule items
# - ML_WaveToy_DG6_SelectBoundConds
# - ML_WaveToy_DG6_ApplyBCs

SCHEDULE GROUP ML_WaveToy_DG6_BoundaryGroup IN MoL_PostStep AFTER ML_WaveToy_DG6_ApplyBCs
{
} "Apply boundary conditions"



# Dependent variables

SCHEDULE GROUP ML_WaveToy_DG6_CalcDependentGroup IN ML_WaveToy_DG6_DependentGroup
{
} "Calculate dependent variables"

SCHEDULE GROUP ML_WaveToy_DG6_DependentGroup IN MoL_PostStep AFTER ML_WaveToy_DG6_BoundaryGroup
{
} "Calculate dependent variables"

SCHEDULE ML_WaveToy_DG6_DependentSelectBCs IN ML_WaveToy_DG6_DependentGroup AFTER ML_WaveToy_DG6_CalcDependentGroup
{
  LANG: C
  OPTIONS: level
  SYNC: WT_du
} "Select boundary conditions for dependent variables"

SCHEDULE GROUP ApplyBCs AS ML_WaveToy_DG6_DependentApplyBCs IN ML_WaveToy_DG6_DependentGroup AFTER ML_WaveToy_DG6_DependentSelectBCs
{
} "Apply boundary conditions to dependent variables"



# RHS

SCHEDULE GROUP ML_WaveToy_DG6_CalcRHSGroup IN ML_WaveToy_DG6_RHSGroup
{
} "Evaluate RHS"

SCHEDULE GROUP ML_WaveToy_DG6_RHSGroup IN MoL_PostStep AFTER ML_WaveToy_DG6_DependentGroup
{
} "Evaluate RHS"

SCHEDULE ML_WaveToy_DG6_RHSSelectBCs IN ML_WaveToy_DG6_RHSGroup AFTER ML_WaveToy_DG6_CalcRHSGroup
{
  LANG: C
  OPTIONS: level
  SYNC: WT_urhs
  SYNC: WT_rhorhs
  SYNC: WT_vrhs
} "Select boundary conditions for RHS variables"

SCHEDULE GROUP ApplyBCs AS ML_WaveToy_DG6_RHSApplyBCs IN ML_WaveToy_DG6_RHSGroup AFTER ML_WaveToy_DG6_RHSSelectBCs
{
} "Apply boundary conditions to RHS variables"



# Constraints

SCHEDULE GROUP ML_WaveToy_DG6_CalcConstraintsGroup IN ML_WaveToy_DG6_ConstraintsGroup
{
} "Evaluate Constraints"

SCHEDULE GROUP ML_WaveToy_DG6_ConstraintsGroup AT analysis
{
} "Evaluate Constraints"

SCHEDULE ML_WaveToy_DG6_ConstraintsSelectBCs IN ML_WaveToy_DG6_ConstraintsGroup AFTER ML_WaveToy_DG6_CalcConstraintsGroup
{
  LANG: C
  OPTIONS: level
  SYNC: WT_w
} "Select boundary conditions for Constraints variables"

SCHEDULE GROUP ApplyBCs AS ML_WaveToy_DG6_ConstraintsApplyBCs IN ML_WaveToy_DG6_ConstraintsGroup AFTER ML_WaveToy_DG6_ConstraintsSelectBCs
{
} "Apply boundary conditions to Constraints variables"



# Energy

SCHEDULE GROUP ML_WaveToy_DG6_CalcEnergyGroup IN ML_WaveToy_DG6_EnergyGroup
{
} "Calculate Energy"

SCHEDULE GROUP ML_WaveToy_DG6_EnergyGroup AT analysis
{
} "Calculate Energy"

SCHEDULE ML_WaveToy_DG6_EnergySelectBCs IN ML_WaveToy_DG6_EnergyGroup AFTER ML_WaveToy_DG6_CalcEnergyGroup
{
  LANG: C
  OPTIONS: level
  SYNC: WT_w
} "Select boundary conditions for Energy variables"

SCHEDULE GROUP ApplyBCs AS ML_WaveToy_DG6_EnergyApplyBCs IN ML_WaveToy_DG6_EnergyGroup AFTER ML_WaveToy_DG6_EnergySelectBCs
{
} "Apply boundary conditions to Energy variables"
