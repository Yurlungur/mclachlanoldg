#include <cassert>
#include <cmath>
#include <iostream>
#include <vector>
using namespace std;

#include <cctk.h>
#include <util_Table.h>



namespace ML_LocalInterp {
  
  template<int order>
  CCTK_REAL interp(const CCTK_REAL* restrict c, CCTK_REAL x, int deriv);
  
  template<>
  CCTK_REAL interp<1>(const CCTK_REAL* restrict c, CCTK_REAL x, int deriv)
  {
    switch (deriv) {
    case 0: return (0.5 - 0.5*x)*c[0] + 0.5*c[1] + 0.5*x*c[1];
    case 1: return -0.5*c[0] + 0.5*c[1];
    case 2: return 0.0;
    }
    assert(0);
  }
  
  template<>
  CCTK_REAL interp<2>(const CCTK_REAL* restrict c, CCTK_REAL x, int deriv)
  {
    switch (deriv) {
    case 0: return
        c[1] + x*(-0.5*c[0] + x*(0.5*c[0] - 1.*c[1] + 0.5*c[2]) + 0.5*c[2]);
    }
    assert(0);
  }
  
  template<>
  CCTK_REAL interp<3>(const CCTK_REAL* restrict c, CCTK_REAL x, int deriv)
  {
    switch (deriv) {
    case 0: return
        -0.125*c[0] + 0.625*c[1] + 0.625*c[2] +
        x*(0.125*c[0] - 1.39754248593736856025573354296*c[1] +
           1.39754248593736856025573354296*c[2] +
           x*(0.625*c[0] - 0.625*c[1] - 0.625*c[2] +
              x*(-0.625*c[0] + 1.39754248593736856025573354296*c[1] -
                 1.39754248593736856025573354296*c[2] + 0.625*c[3]) +
              0.625*c[3]) - 0.125*c[3]) - 0.125*c[3];
    }
    assert(0);
  }
  
  template<>
  CCTK_REAL interp<4>(const CCTK_REAL* restrict c, CCTK_REAL x, int deriv)
  {
    switch (deriv) {
    case 0: return
        1.*c[2] +
        x*(0.375*c[0] - 1.33658457769545333525484709817*c[1] + 
           1.33658457769545333525484709817*c[3] + 
           x*(-0.375*c[0] + 2.04166666666666666666666666667*c[1] - 
              3.33333333333333333333333333333*c[2] + 
              2.04166666666666666666666666667*c[3] + 
              x*(-0.875*c[0] + 1.33658457769545333525484709817*c[1] - 
                 1.33658457769545333525484709817*c[3] + 
                 x*(0.875*c[0] - 2.04166666666666666666666666667*c[1] + 
                    2.33333333333333333333333333333*c[2] - 
                    2.04166666666666666666666666667*c[3] + 0.875*c[4]) + 
                 0.875*c[4]) - 0.375*c[4]) - 0.375*c[4]);
    case 1: return
        0.375*c[0] - 1.33658457769545333525484709817066914262*c[1] + 
        1.33658457769545333525484709817066914262*c[3] - 0.375*c[4] + 
        x*(-0.75*c[0] + 4.0833333333333333333333333333333333333*c[1] - 
           6.6666666666666666666666666666666666667*c[2] + 
           4.0833333333333333333333333333333333333*c[3] - 0.75*c[4] + 
           x*(-2.625*c[0] + 4.0097537330863600057645412945120074279*c[1] - 
              4.0097537330863600057645412945120074279*c[3] + 2.625*c[4] + 
              x*(3.5*c[0] - 8.1666666666666666666666666666666666667*c[1] + 
                 9.3333333333333333333333333333333333333*c[2] - 
                 8.1666666666666666666666666666666666667*c[3] + 3.5*c[4])));
    case 2: return
        -0.75*c[0] + 4.0833333333333333333333333333333333333*c[1] - 
        6.6666666666666666666666666666666666667*c[2] + 
        4.0833333333333333333333333333333333333*c[3] - 0.75*c[4] + 
        x*(-5.25*c[0] + 8.0195074661727200115290825890240148557*c[1] - 
           8.0195074661727200115290825890240148557*c[3] + 5.25*c[4] + 
           x*(10.5*c[0] - 24.5*c[1] + 28.*c[2] - 24.5*c[3] + 10.5*c[4]));
    }
    assert(0);
  }
  
  
  
  // Multi-dimensional interpolation -- recurse to one lower dimension
  template<int order>
  CCTK_REAL interp_multi(const CCTK_REAL* restrict c,
                         const int* restrict di,
                         const CCTK_REAL* restrict x,
                         const int* restrict derivs,
                         int dims)
  {
    if (dims == 0) return *c;
    CCTK_REAL coeffs[order+1];
    for (int n=0; n<=order; ++n) {
      coeffs[n] = interp_multi<order>(c + n*di[dims-1], di, x, derivs, dims-1);
    }
    return interp<order>(coeffs, x[dims-1], derivs[dims-1]);
  }
  
  
  
  const int dim = 3;
  
  const int order = 4;          // TODO
  const CCTK_REAL xmin = -1.0;
  const CCTK_REAL xmax = +1.0;
  
  int interpolate(int num_dims,
                  int table,
                  /***** coordinate system *****/
                  const CCTK_REAL coord_origin[],
                  const CCTK_REAL coord_delta[],
                  /***** interpolation points *****/
                  int num_interp_points,
                  int interp_coords_type_code,
                  const void *const interp_coords[],
                  /***** input arrays *****/
                  int num_input_arrays,
                  const CCTK_INT input_array_dims[],
                  const CCTK_INT input_array_type_codes[],
                  const void *const input_arrays[],
                  /***** output arrays *****/
                  int num_output_arrays,
                  const CCTK_INT output_array_type_codes[],
                  void *const output_arrays[])
  {
    // Check input
    assert(num_dims == dim);
    assert(interp_coords_type_code == CCTK_VARIABLE_REAL);
    
    vector<CCTK_INT> operand_indices(num_output_arrays);
    int iret = Util_TableGetIntArray
      (table, num_output_arrays, &operand_indices[0], "operand_indices");
    if (iret == num_output_arrays) {
      // all is fine
      for (int array=0; array<num_output_arrays; ++array) {
        assert(operand_indices[array]>=0 &&
               operand_indices[array]<num_input_arrays);
      }
    } else if (iret == UTIL_ERROR_TABLE_NO_SUCH_KEY) {
      assert(num_output_arrays == num_input_arrays);
      for (int array=0; array<num_output_arrays; ++array) {
        operand_indices[array] = array;
      }
    } else {
      assert(0);
    }
    
    vector<CCTK_INT> operation_codes(num_output_arrays);
    iret = Util_TableGetIntArray
      (table, num_output_arrays, &operation_codes[0], "operation_codes");
    if (iret == num_output_arrays) {
      // all is fine
    } else if (iret == UTIL_ERROR_TABLE_NO_SUCH_KEY) {
      for (int array=0; array<num_output_arrays; ++array) {
        operation_codes[array] = 0;
      }
    } else {
      assert(0);
    }
    
    for (int array=0; array<num_input_arrays; ++array) {
      // Setting input_array=NULL is possible of that interpolation is
      // suppressed
      if (input_arrays[array]) {
        assert(input_array_type_codes[array] == CCTK_VARIABLE_REAL);
      }
    }
    for (int array=0; array<num_output_arrays; ++array) {
      // Setting output_array=NULL suppresses that interpolation
      if (output_arrays[array]) {
        assert(output_array_type_codes[array] == CCTK_VARIABLE_REAL);
        assert(input_arrays[operand_indices[array]]);
      }
    }
    
    // Input array shape
    int di[dim];
    di[0] = 1;
    di[1] = di[0] * input_array_dims[0];
    di[2] = di[1] * input_array_dims[1];
    
    // Loop over all points
#pragma omp parallel for
    for (int point=0; point<num_interp_points; ++point) {
      
      // Determine interpolation location
      int elem[dim];            // element
      CCTK_REAL x[dim];         // location in element
      for (int d=0; d<dim; ++d) {
        const CCTK_REAL coord =
          static_cast<const CCTK_REAL*>(interp_coords[d])[point];
        const CCTK_REAL eps = 1.0e-12;
        int i = lrint((coord - coord_origin[d]) / coord_delta[d]);
        if (i == 0) {
          i = lrint((coord - coord_origin[d]) / coord_delta[d] + eps);
        } else if (i == input_array_dims[d]-1) {
          i = lrint((coord - coord_origin[d]) / coord_delta[d] - eps);
        }
        assert(i>=1 && i<input_array_dims[d]-1);
        elem[d] = (i-1) / (order+1);
        const CCTK_REAL elem_origin =
          coord_origin[d] + (elem[d] * (order+1) + 0.5) * coord_delta[d];
        x[d] = xmin + ((xmax - xmin) / ((order+1) * coord_delta[d]) *
                       (coord - elem_origin));
        assert(x[d] >= xmin - eps && x[d] <= xmax + eps);
        x[d] = fmax(xmin, fmin(xmax, x[d]));
      }
      
      // Prepare interpolation
      const int idx =
        (elem[0]*(order+1)+1)*di[0] +
        (elem[1]*(order+1)+1)*di[1] +
        (elem[2]*(order+1)+1)*di[2];
      
      // Loop over all variables
      for (int array=0; array<num_output_arrays; ++array) {
        CCTK_REAL *restrict const output_array =
          static_cast<CCTK_REAL*>(output_arrays[array]);
        if (output_array) {
          // Determine operation
          int derivs[dim];
          derivs[0] = derivs[1] = derivs[2] = 0;
          int code = operation_codes[array];
          while (code > 0) {
            const int dir = code % 10;
            assert(dir>=0 && dir<=dim);
            if (dir>0) ++derivs[dir-1];
            code /= 10;
          }
          // Interpolate
          const CCTK_REAL *restrict const input_array =
            static_cast<const CCTK_REAL*>(input_arrays[operand_indices[array]]);
          const CCTK_REAL result =
            interp_multi<order>(input_array+idx, di, x, derivs, dim);
          // Correct derivatives for grid spacing
          const CCTK_REAL result2 = result *
            pow((xmax - xmin) / ((order+1) * coord_delta[0]), derivs[0]) *
            pow((xmax - xmin) / ((order+1) * coord_delta[1]), derivs[1]) *
            pow((xmax - xmin) / ((order+1) * coord_delta[2]), derivs[2]);
          // Store result
          output_array[point] = result2;
        }
      } // array
    }   // point
    
    return 0;
  }
  
  
  
  extern "C"
  int ML_LocalInterp_Startup()
  {
    int iret = CCTK_InterpRegisterOpLocalUniform
      (interpolate, "DGFE", CCTK_THORNSTRING);
    // if (iret <= 0) std::cout << "ierr = " << ierr << std::endl;
    assert(iret >= 0);
    return 0;
  }
  
} // namespace ML_LocalInterp
