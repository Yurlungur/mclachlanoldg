/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_WaveToy_DG8 {

extern "C" void ML_WaveToy_DG8_DependentSecondOrder_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_WaveToy_DG8_DependentSecondOrder_calc_every != ML_WaveToy_DG8_DependentSecondOrder_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG8::WT_du","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG8::WT_du.");
  return;
}

static void ML_WaveToy_DG8_DependentSecondOrder_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_WaveToy_DG8_DependentSecondOrder,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL uL CCTK_ATTRIBUTE_UNUSED = u[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDu1 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(u,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(u,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 0,-18.*GFOffset(u,0,0,0) 
      + 24.3497451715930658264745651379*GFOffset(u,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(u,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(u,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(u,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(u,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(u,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(u,7,0,0) - 
      0.5*GFOffset(u,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(u,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(u,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(u,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(u,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(u,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(u,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(u,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(u,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(u,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(u,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(u,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(u,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(u,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(u,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(u,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(u,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(u,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(u,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(u,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(u,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(u,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(u,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(u,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(u,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(u,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(u,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(u,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(u,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(u,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(u,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(u,3,0,0) - 
      0.2734375*GFOffset(u,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(u,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(u,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(u,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(u,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(u,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(u,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(u,2,0,0) + 
      0.444613449281090634955156033*GFOffset(u,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(u,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(u,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(u,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(u,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(u,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(u,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(u,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(u,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(u,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(u,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(u,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(u,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(u,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(u,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(u,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(u,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(u,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(u,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(u,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(u,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(u,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(u,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(u,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(u,-1,0,0) + 
      18.*GFOffset(u,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(u,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(u,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDu2 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(u,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(u,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 0,-18.*GFOffset(u,0,0,0) 
      + 24.3497451715930658264745651379*GFOffset(u,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(u,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(u,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(u,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(u,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(u,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(u,0,7,0) - 
      0.5*GFOffset(u,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(u,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(u,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(u,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(u,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(u,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(u,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(u,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(u,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(u,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(u,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(u,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(u,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(u,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(u,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(u,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(u,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(u,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(u,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(u,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(u,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(u,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(u,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(u,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(u,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(u,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(u,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(u,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(u,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(u,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(u,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(u,0,3,0) - 
      0.2734375*GFOffset(u,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(u,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(u,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(u,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(u,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(u,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(u,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(u,0,2,0) + 
      0.444613449281090634955156033*GFOffset(u,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(u,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(u,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(u,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(u,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(u,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(u,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(u,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(u,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(u,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(u,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(u,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(u,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(u,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(u,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(u,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(u,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(u,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(u,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(u,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(u,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(u,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(u,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(u,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(u,0,-1,0) + 
      18.*GFOffset(u,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(u,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(u,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDu3 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(u,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(u,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 0,-18.*GFOffset(u,0,0,0) 
      + 24.3497451715930658264745651379*GFOffset(u,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(u,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(u,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(u,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(u,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(u,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(u,0,0,7) - 
      0.5*GFOffset(u,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(u,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(u,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(u,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(u,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(u,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(u,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(u,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(u,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(u,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(u,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(u,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(u,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(u,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(u,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(u,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(u,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(u,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(u,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(u,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(u,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(u,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(u,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(u,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(u,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(u,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(u,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(u,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(u,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(u,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(u,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(u,0,0,3) - 
      0.2734375*GFOffset(u,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(u,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(u,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(u,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(u,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(u,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(u,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(u,0,0,2) + 
      0.444613449281090634955156033*GFOffset(u,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(u,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(u,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(u,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(u,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(u,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(u,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(u,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(u,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(u,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(u,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(u,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(u,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(u,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(u,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(u,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(u,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(u,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(u,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(u,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(u,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(u,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(u,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(u,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(u,0,0,-1) + 
      18.*GFOffset(u,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(u,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(u,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDu1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDu2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDu3 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDu1 = J11L*LDu1 + J21L*LDu2 + J31L*LDu3;
      
      PDu2 = J12L*LDu1 + J22L*LDu2 + J32L*LDu3;
      
      PDu3 = J13L*LDu1 + J23L*LDu2 + J33L*LDu3;
    }
    else
    {
      PDu1 = LDu1;
      
      PDu2 = LDu2;
      
      PDu3 = LDu3;
    }
    
    CCTK_REAL du1L CCTK_ATTRIBUTE_UNUSED = PDu1;
    
    CCTK_REAL du2L CCTK_ATTRIBUTE_UNUSED = PDu2;
    
    CCTK_REAL du3L CCTK_ATTRIBUTE_UNUSED = PDu3;
    /* Copy local copies back to grid functions */
    du1[index] = du1L;
    du2[index] = du2L;
    du3[index] = du3L;
  }
  CCTK_ENDLOOP3(ML_WaveToy_DG8_DependentSecondOrder);
}
extern "C" void ML_WaveToy_DG8_DependentSecondOrder(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_WaveToy_DG8_DependentSecondOrder_Body");
  }
  if (cctk_iteration % ML_WaveToy_DG8_DependentSecondOrder_calc_every != ML_WaveToy_DG8_DependentSecondOrder_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_WaveToy_DG8::WT_du",
    "ML_WaveToy_DG8::WT_u"};
  AssertGroupStorage(cctkGH, "ML_WaveToy_DG8_DependentSecondOrder", 2, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_WaveToy_DG8_DependentSecondOrder_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_WaveToy_DG8_DependentSecondOrder_Body");
  }
}

} // namespace ML_WaveToy_DG8
