/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_WaveToy_DG8 {

extern "C" void ML_WaveToy_DG8_RHSFirstOrder_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_WaveToy_DG8_RHSFirstOrder_calc_every != ML_WaveToy_DG8_RHSFirstOrder_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG8::WT_rhorhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG8::WT_rhorhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG8::WT_urhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG8::WT_urhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG8::WT_vrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG8::WT_vrhs.");
  return;
}

static void ML_WaveToy_DG8_RHSFirstOrder_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_WaveToy_DG8_RHSFirstOrder,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL myDetJL CCTK_ATTRIBUTE_UNUSED = myDetJ[index];
    CCTK_REAL rhoL CCTK_ATTRIBUTE_UNUSED = rho[index];
    CCTK_REAL uL CCTK_ATTRIBUTE_UNUSED = u[index];
    CCTK_REAL v1L CCTK_ATTRIBUTE_UNUSED = v1[index];
    CCTK_REAL v2L CCTK_ATTRIBUTE_UNUSED = v2[index];
    CCTK_REAL v3L CCTK_ATTRIBUTE_UNUSED = v3[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDrho1 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(rho,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(rho,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(rho,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(rho,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(rho,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(rho,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(rho,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(rho,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(rho,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(rho,7,0,0) - 
      0.5*GFOffset(rho,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(rho,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(rho,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(rho,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(rho,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(rho,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(rho,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(rho,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(rho,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(rho,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(rho,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(rho,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(rho,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(rho,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(rho,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(rho,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(rho,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(rho,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(rho,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(rho,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(rho,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(rho,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(rho,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(rho,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(rho,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(rho,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(rho,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(rho,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(rho,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(rho,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(rho,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(rho,3,0,0) - 
      0.2734375*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(rho,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(rho,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(rho,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(rho,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(rho,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(rho,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(rho,2,0,0) + 
      0.444613449281090634955156033*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(rho,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(rho,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(rho,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(rho,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(rho,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(rho,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(rho,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(rho,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(rho,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(rho,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(rho,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(rho,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(rho,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(rho,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(rho,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(rho,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(rho,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(rho,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(rho,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(rho,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(rho,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(rho,-1,0,0) + 
      18.*GFOffset(rho,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(rho,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(rho,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDrho2 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(rho,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(rho,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(rho,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(rho,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(rho,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(rho,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(rho,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(rho,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(rho,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(rho,0,7,0) - 
      0.5*GFOffset(rho,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(rho,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(rho,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(rho,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(rho,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(rho,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(rho,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(rho,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(rho,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(rho,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(rho,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(rho,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(rho,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(rho,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(rho,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(rho,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(rho,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(rho,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(rho,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(rho,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(rho,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(rho,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(rho,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(rho,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(rho,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(rho,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(rho,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(rho,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(rho,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(rho,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(rho,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(rho,0,3,0) - 
      0.2734375*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(rho,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(rho,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(rho,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(rho,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(rho,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(rho,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(rho,0,2,0) + 
      0.444613449281090634955156033*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(rho,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(rho,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(rho,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(rho,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(rho,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(rho,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(rho,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(rho,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(rho,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(rho,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(rho,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(rho,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(rho,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(rho,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(rho,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(rho,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(rho,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(rho,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(rho,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(rho,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(rho,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(rho,0,-1,0) + 
      18.*GFOffset(rho,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(rho,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(rho,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDrho3 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(rho,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(rho,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(rho,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(rho,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(rho,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(rho,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(rho,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(rho,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(rho,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(rho,0,0,7) - 
      0.5*GFOffset(rho,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(rho,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(rho,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(rho,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(rho,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(rho,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(rho,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(rho,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(rho,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(rho,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(rho,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(rho,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(rho,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(rho,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(rho,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(rho,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(rho,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(rho,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(rho,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(rho,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(rho,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(rho,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(rho,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(rho,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(rho,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(rho,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(rho,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(rho,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(rho,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(rho,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(rho,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(rho,0,0,3) - 
      0.2734375*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(rho,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(rho,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(rho,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(rho,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(rho,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(rho,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(rho,0,0,2) + 
      0.444613449281090634955156033*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(rho,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(rho,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(rho,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(rho,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(rho,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(rho,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(rho,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(rho,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(rho,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(rho,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(rho,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(rho,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(rho,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(rho,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(rho,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(rho,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(rho,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(rho,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(rho,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(rho,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(rho,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(rho,0,0,-1) + 
      18.*GFOffset(rho,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(rho,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(rho,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL LDv11 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(v1,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(v1,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(v1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(v1,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(v1,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(v1,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(v1,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(v1,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(v1,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(v1,7,0,0) - 
      0.5*GFOffset(v1,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(v1,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(v1,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(v1,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(v1,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(v1,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(v1,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(v1,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(v1,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(v1,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(v1,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(v1,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(v1,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(v1,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(v1,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(v1,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(v1,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(v1,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(v1,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(v1,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(v1,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(v1,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(v1,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(v1,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(v1,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(v1,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(v1,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(v1,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(v1,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(v1,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(v1,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(v1,3,0,0) - 
      0.2734375*GFOffset(v1,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(v1,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(v1,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(v1,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(v1,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(v1,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(v1,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(v1,2,0,0) + 
      0.444613449281090634955156033*GFOffset(v1,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(v1,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(v1,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(v1,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(v1,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(v1,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(v1,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(v1,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(v1,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(v1,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(v1,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(v1,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(v1,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(v1,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(v1,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(v1,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(v1,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(v1,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(v1,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(v1,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(v1,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(v1,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(v1,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(v1,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(v1,-1,0,0) + 
      18.*GFOffset(v1,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(v1,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(v1,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDv12 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(v1,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(v1,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(v1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(v1,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(v1,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(v1,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(v1,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(v1,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(v1,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(v1,0,7,0) - 
      0.5*GFOffset(v1,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(v1,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(v1,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(v1,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(v1,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(v1,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(v1,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(v1,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(v1,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(v1,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(v1,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(v1,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(v1,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(v1,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(v1,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(v1,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(v1,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(v1,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(v1,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(v1,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(v1,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(v1,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(v1,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(v1,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(v1,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(v1,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(v1,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(v1,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(v1,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(v1,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(v1,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(v1,0,3,0) - 
      0.2734375*GFOffset(v1,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(v1,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(v1,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(v1,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(v1,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(v1,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(v1,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(v1,0,2,0) + 
      0.444613449281090634955156033*GFOffset(v1,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(v1,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(v1,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(v1,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(v1,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(v1,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(v1,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(v1,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(v1,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(v1,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(v1,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(v1,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(v1,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(v1,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(v1,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(v1,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(v1,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(v1,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(v1,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(v1,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(v1,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(v1,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(v1,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(v1,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(v1,0,-1,0) + 
      18.*GFOffset(v1,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(v1,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(v1,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDv13 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(v1,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(v1,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(v1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(v1,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(v1,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(v1,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(v1,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(v1,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(v1,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(v1,0,0,7) - 
      0.5*GFOffset(v1,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(v1,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(v1,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(v1,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(v1,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(v1,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(v1,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(v1,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(v1,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(v1,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(v1,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(v1,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(v1,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(v1,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(v1,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(v1,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(v1,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(v1,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(v1,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(v1,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(v1,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(v1,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(v1,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(v1,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(v1,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(v1,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(v1,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(v1,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(v1,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(v1,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(v1,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(v1,0,0,3) - 
      0.2734375*GFOffset(v1,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(v1,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(v1,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(v1,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(v1,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(v1,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(v1,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(v1,0,0,2) + 
      0.444613449281090634955156033*GFOffset(v1,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(v1,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(v1,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(v1,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(v1,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(v1,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(v1,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(v1,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(v1,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(v1,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(v1,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(v1,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(v1,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(v1,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(v1,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(v1,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(v1,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(v1,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(v1,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(v1,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(v1,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(v1,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(v1,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(v1,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(v1,0,0,-1) + 
      18.*GFOffset(v1,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(v1,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(v1,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL LDv21 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(v2,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(v2,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(v2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(v2,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(v2,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(v2,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(v2,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(v2,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(v2,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(v2,7,0,0) - 
      0.5*GFOffset(v2,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(v2,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(v2,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(v2,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(v2,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(v2,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(v2,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(v2,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(v2,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(v2,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(v2,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(v2,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(v2,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(v2,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(v2,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(v2,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(v2,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(v2,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(v2,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(v2,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(v2,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(v2,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(v2,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(v2,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(v2,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(v2,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(v2,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(v2,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(v2,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(v2,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(v2,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(v2,3,0,0) - 
      0.2734375*GFOffset(v2,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(v2,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(v2,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(v2,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(v2,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(v2,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(v2,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(v2,2,0,0) + 
      0.444613449281090634955156033*GFOffset(v2,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(v2,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(v2,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(v2,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(v2,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(v2,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(v2,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(v2,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(v2,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(v2,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(v2,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(v2,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(v2,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(v2,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(v2,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(v2,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(v2,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(v2,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(v2,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(v2,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(v2,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(v2,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(v2,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(v2,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(v2,-1,0,0) + 
      18.*GFOffset(v2,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(v2,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(v2,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDv22 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(v2,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(v2,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(v2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(v2,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(v2,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(v2,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(v2,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(v2,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(v2,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(v2,0,7,0) - 
      0.5*GFOffset(v2,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(v2,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(v2,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(v2,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(v2,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(v2,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(v2,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(v2,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(v2,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(v2,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(v2,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(v2,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(v2,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(v2,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(v2,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(v2,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(v2,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(v2,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(v2,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(v2,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(v2,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(v2,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(v2,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(v2,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(v2,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(v2,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(v2,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(v2,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(v2,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(v2,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(v2,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(v2,0,3,0) - 
      0.2734375*GFOffset(v2,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(v2,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(v2,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(v2,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(v2,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(v2,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(v2,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(v2,0,2,0) + 
      0.444613449281090634955156033*GFOffset(v2,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(v2,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(v2,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(v2,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(v2,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(v2,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(v2,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(v2,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(v2,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(v2,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(v2,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(v2,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(v2,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(v2,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(v2,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(v2,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(v2,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(v2,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(v2,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(v2,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(v2,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(v2,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(v2,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(v2,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(v2,0,-1,0) + 
      18.*GFOffset(v2,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(v2,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(v2,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDv23 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(v2,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(v2,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(v2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(v2,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(v2,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(v2,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(v2,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(v2,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(v2,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(v2,0,0,7) - 
      0.5*GFOffset(v2,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(v2,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(v2,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(v2,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(v2,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(v2,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(v2,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(v2,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(v2,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(v2,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(v2,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(v2,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(v2,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(v2,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(v2,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(v2,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(v2,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(v2,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(v2,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(v2,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(v2,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(v2,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(v2,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(v2,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(v2,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(v2,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(v2,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(v2,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(v2,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(v2,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(v2,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(v2,0,0,3) - 
      0.2734375*GFOffset(v2,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(v2,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(v2,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(v2,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(v2,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(v2,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(v2,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(v2,0,0,2) + 
      0.444613449281090634955156033*GFOffset(v2,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(v2,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(v2,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(v2,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(v2,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(v2,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(v2,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(v2,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(v2,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(v2,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(v2,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(v2,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(v2,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(v2,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(v2,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(v2,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(v2,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(v2,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(v2,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(v2,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(v2,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(v2,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(v2,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(v2,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(v2,0,0,-1) + 
      18.*GFOffset(v2,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(v2,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(v2,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL LDv31 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(v3,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(v3,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(v3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(v3,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(v3,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(v3,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(v3,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(v3,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(v3,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(v3,7,0,0) - 
      0.5*GFOffset(v3,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(v3,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(v3,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(v3,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(v3,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(v3,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(v3,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(v3,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(v3,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(v3,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(v3,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(v3,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(v3,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(v3,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(v3,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(v3,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(v3,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(v3,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(v3,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(v3,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(v3,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(v3,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(v3,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(v3,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(v3,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(v3,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(v3,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(v3,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(v3,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(v3,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(v3,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(v3,3,0,0) - 
      0.2734375*GFOffset(v3,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(v3,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(v3,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(v3,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(v3,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(v3,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(v3,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(v3,2,0,0) + 
      0.444613449281090634955156033*GFOffset(v3,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(v3,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(v3,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(v3,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(v3,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(v3,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(v3,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(v3,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(v3,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(v3,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(v3,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(v3,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(v3,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(v3,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(v3,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(v3,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(v3,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(v3,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(v3,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(v3,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(v3,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(v3,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(v3,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(v3,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(v3,-1,0,0) + 
      18.*GFOffset(v3,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(v3,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(v3,1,0,0),0)))*pow(dx,-1);
    
    CCTK_REAL LDv32 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(v3,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(v3,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(v3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(v3,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(v3,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(v3,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(v3,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(v3,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(v3,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(v3,0,7,0) - 
      0.5*GFOffset(v3,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(v3,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(v3,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(v3,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(v3,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(v3,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(v3,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(v3,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(v3,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(v3,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(v3,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(v3,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(v3,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(v3,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(v3,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(v3,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(v3,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(v3,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(v3,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(v3,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(v3,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(v3,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(v3,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(v3,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(v3,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(v3,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(v3,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(v3,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(v3,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(v3,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(v3,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(v3,0,3,0) - 
      0.2734375*GFOffset(v3,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(v3,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(v3,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(v3,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(v3,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(v3,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(v3,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(v3,0,2,0) + 
      0.444613449281090634955156033*GFOffset(v3,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(v3,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(v3,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(v3,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(v3,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(v3,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(v3,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(v3,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(v3,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(v3,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(v3,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(v3,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(v3,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(v3,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(v3,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(v3,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(v3,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(v3,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(v3,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(v3,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(v3,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(v3,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(v3,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(v3,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(v3,0,-1,0) + 
      18.*GFOffset(v3,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(v3,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(v3,0,1,0),0)))*pow(dy,-1);
    
    CCTK_REAL LDv33 CCTK_ATTRIBUTE_UNUSED = 
      (-0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(v3,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(v3,0,0,0),0)) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(v3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(v3,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(v3,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(v3,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(v3,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(v3,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(v3,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(v3,0,0,7) - 
      0.5*GFOffset(v3,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(v3,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(v3,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(v3,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(v3,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(v3,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(v3,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(v3,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(v3,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(v3,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(v3,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(v3,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(v3,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(v3,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(v3,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(v3,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(v3,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(v3,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(v3,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(v3,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(v3,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(v3,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(v3,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(v3,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(v3,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(v3,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(v3,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(v3,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(v3,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(v3,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(v3,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(v3,0,0,3) - 
      0.2734375*GFOffset(v3,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(v3,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(v3,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(v3,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(v3,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(v3,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(v3,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(v3,0,0,2) + 
      0.444613449281090634955156033*GFOffset(v3,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(v3,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(v3,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(v3,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(v3,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(v3,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(v3,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(v3,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(v3,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(v3,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(v3,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(v3,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(v3,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(v3,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(v3,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(v3,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(v3,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(v3,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(v3,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(v3,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(v3,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(v3,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(v3,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(v3,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(v3,0,0,-1) + 
      18.*GFOffset(v3,0,0,0),0)) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(v3,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(v3,0,0,1),0)))*pow(dz,-1);
    
    CCTK_REAL PDrho1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDrho2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDrho3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDv11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDv22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDv33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDrho1 = J11L*LDrho1 + J21L*LDrho2 + J31L*LDrho3;
      
      PDrho2 = J12L*LDrho1 + J22L*LDrho2 + J32L*LDrho3;
      
      PDrho3 = J13L*LDrho1 + J23L*LDrho2 + J33L*LDrho3;
      
      PDv11 = J11L*LDv11 + J21L*LDv12 + J31L*LDv13;
      
      PDv22 = J12L*LDv21 + J22L*LDv22 + J32L*LDv23;
      
      PDv33 = J13L*LDv31 + J23L*LDv32 + J33L*LDv33;
    }
    else
    {
      PDrho1 = LDrho1;
      
      PDrho2 = LDrho2;
      
      PDrho3 = LDrho3;
      
      PDv11 = LDv11;
      
      PDv22 = LDv22;
      
      PDv33 = LDv33;
    }
    
    CCTK_REAL urhsL CCTK_ATTRIBUTE_UNUSED = rhoL + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(u,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(u,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(u,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(u,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(u,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(u,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(u,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(u,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(u,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(u,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(u,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(u,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(u,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(u,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(u,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(u,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(u,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(u,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(u,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(u,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(u,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(u,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(u,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(u,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(u,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(u,5,0,0),0) + IfThen(ti == 
      4,-0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(u,-1,0,0) + 
      GFOffset(u,1,0,0)) - 
      0.0955144556251064670745298655398*(GFOffset(u,-2,0,0) + 
      GFOffset(u,2,0,0)) + 
      0.0741579827300490137576365968936*(GFOffset(u,-3,0,0) + 
      GFOffset(u,3,0,0)) - 
      0.0303817292054494222005208333333*(GFOffset(u,-4,0,0) + 
      GFOffset(u,4,0,0)),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(u,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(u,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(u,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(u,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(u,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(u,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(u,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(u,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(u,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(u,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(u,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(u,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(u,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(u,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(u,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(u,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(u,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(u,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(u,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(u,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(u,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(u,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(u,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(u,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(u,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(u,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(u,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(u,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(u,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(u,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(u,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(u,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(u,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(u,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(u,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(u,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(u,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(u,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(u,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(u,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(u,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(u,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(u,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(u,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(u,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(u,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(u,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(u,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(u,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(u,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(u,0,5,0),0) + IfThen(tj == 
      4,-0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(u,0,-1,0) + 
      GFOffset(u,0,1,0)) - 
      0.0955144556251064670745298655398*(GFOffset(u,0,-2,0) + 
      GFOffset(u,0,2,0)) + 
      0.0741579827300490137576365968936*(GFOffset(u,0,-3,0) + 
      GFOffset(u,0,3,0)) - 
      0.0303817292054494222005208333333*(GFOffset(u,0,-4,0) + 
      GFOffset(u,0,4,0)),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(u,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(u,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(u,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(u,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(u,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(u,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(u,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(u,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(u,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(u,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(u,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(u,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(u,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(u,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(u,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(u,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(u,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(u,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(u,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(u,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(u,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(u,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(u,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(u,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(u,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(u,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(u,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(u,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(u,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(u,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(u,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(u,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(u,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(u,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(u,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(u,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(u,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(u,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(u,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(u,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(u,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(u,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(u,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(u,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(u,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(u,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(u,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(u,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(u,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(u,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(u,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(u,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(u,0,0,5),0) + IfThen(tk == 
      4,-0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(u,0,0,-1) + 
      GFOffset(u,0,0,1)) - 
      0.0955144556251064670745298655398*(GFOffset(u,0,0,-2) + 
      GFOffset(u,0,0,2)) + 
      0.0741579827300490137576365968936*(GFOffset(u,0,0,-3) + 
      GFOffset(u,0,0,3)) - 
      0.0303817292054494222005208333333*(GFOffset(u,0,0,-4) + 
      GFOffset(u,0,0,4)),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(u,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(u,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(u,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(u,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(u,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(u,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(u,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(u,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(u,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(u,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(u,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(u,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(u,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(u,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(u,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(u,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(u,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(u,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(u,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(u,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(u,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(u,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(u,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(u,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(u,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(u,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(u,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(u,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(u,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(u,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(u,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(u,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(u,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(u,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(u,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(u,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(u,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(u,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(u,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(u,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(u,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(u,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(u,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(u,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(u,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(u,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(u,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(u,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(u,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(u,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(u,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(u,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(u,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(u,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(u,5,0,0),0) + IfThen(ti == 
      4,-0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(u,-1,0,0) + 
      GFOffset(u,1,0,0)) - 
      0.0955144556251064670745298655398*(GFOffset(u,-2,0,0) + 
      GFOffset(u,2,0,0)) + 
      0.0741579827300490137576365968936*(GFOffset(u,-3,0,0) + 
      GFOffset(u,3,0,0)) - 
      0.0303817292054494222005208333333*(GFOffset(u,-4,0,0) + 
      GFOffset(u,4,0,0)),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(u,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(u,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(u,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(u,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(u,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(u,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(u,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(u,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(u,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(u,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(u,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(u,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(u,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(u,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(u,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(u,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(u,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(u,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(u,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(u,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(u,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(u,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(u,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(u,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(u,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(u,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(u,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(u,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(u,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(u,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(u,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(u,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(u,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(u,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(u,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(u,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(u,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(u,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(u,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(u,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(u,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(u,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(u,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(u,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(u,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(u,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(u,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(u,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(u,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(u,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(u,0,5,0),0) + IfThen(tj == 
      4,-0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(u,0,-1,0) + 
      GFOffset(u,0,1,0)) - 
      0.0955144556251064670745298655398*(GFOffset(u,0,-2,0) + 
      GFOffset(u,0,2,0)) + 
      0.0741579827300490137576365968936*(GFOffset(u,0,-3,0) + 
      GFOffset(u,0,3,0)) - 
      0.0303817292054494222005208333333*(GFOffset(u,0,-4,0) + 
      GFOffset(u,0,4,0)),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(u,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(u,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(u,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(u,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(u,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(u,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(u,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(u,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(u,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(u,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(u,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(u,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(u,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(u,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(u,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(u,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(u,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(u,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(u,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(u,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(u,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(u,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(u,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(u,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(u,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(u,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(u,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(u,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(u,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(u,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(u,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(u,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(u,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(u,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(u,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(u,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(u,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(u,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(u,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(u,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(u,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(u,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(u,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(u,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(u,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(u,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(u,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(u,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(u,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(u,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(u,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(u,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(u,0,0,5),0) + IfThen(tk == 
      4,-0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(u,0,0,-1) + 
      GFOffset(u,0,0,1)) - 
      0.0955144556251064670745298655398*(GFOffset(u,0,0,-2) + 
      GFOffset(u,0,0,2)) + 
      0.0741579827300490137576365968936*(GFOffset(u,0,0,-3) + 
      GFOffset(u,0,0,3)) - 
      0.0303817292054494222005208333333*(GFOffset(u,0,0,-4) + 
      GFOffset(u,0,0,4)),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(u,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(u,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(u,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(u,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(u,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(u,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(u,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(u,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(u,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(u,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(u,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(u,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(u,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(u,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(u,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(u,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(u,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(u,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(u,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(u,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(u,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(u,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(u,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(u,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(u,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(u,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(u,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(u,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(u,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL rhorhsL CCTK_ATTRIBUTE_UNUSED = PDv11 + PDv22 + PDv33 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(rho,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(rho,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(rho,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(rho,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(rho,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(rho,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(rho,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(rho,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(rho,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(rho,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(rho,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(rho,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(rho,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(rho,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(rho,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(rho,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(rho,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(rho,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(rho,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(rho,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(rho,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(rho,5,0,0),0) + IfThen(ti == 
      4,-0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(rho,-1,0,0) + 
      GFOffset(rho,1,0,0)) - 
      0.0955144556251064670745298655398*(GFOffset(rho,-2,0,0) + 
      GFOffset(rho,2,0,0)) + 
      0.0741579827300490137576365968936*(GFOffset(rho,-3,0,0) + 
      GFOffset(rho,3,0,0)) - 
      0.0303817292054494222005208333333*(GFOffset(rho,-4,0,0) + 
      GFOffset(rho,4,0,0)),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(rho,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(rho,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(rho,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(rho,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(rho,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(rho,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(rho,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(rho,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(rho,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(rho,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(rho,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(rho,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(rho,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(rho,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(rho,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(rho,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(rho,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(rho,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(rho,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(rho,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(rho,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(rho,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(rho,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(rho,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(rho,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(rho,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(rho,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(rho,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(rho,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(rho,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(rho,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(rho,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(rho,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(rho,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(rho,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(rho,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(rho,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(rho,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(rho,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(rho,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(rho,0,5,0),0) + IfThen(tj == 
      4,-0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(rho,0,-1,0) + 
      GFOffset(rho,0,1,0)) - 
      0.0955144556251064670745298655398*(GFOffset(rho,0,-2,0) + 
      GFOffset(rho,0,2,0)) + 
      0.0741579827300490137576365968936*(GFOffset(rho,0,-3,0) + 
      GFOffset(rho,0,3,0)) - 
      0.0303817292054494222005208333333*(GFOffset(rho,0,-4,0) + 
      GFOffset(rho,0,4,0)),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(rho,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(rho,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(rho,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(rho,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(rho,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(rho,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(rho,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(rho,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(rho,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(rho,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(rho,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(rho,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(rho,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(rho,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(rho,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(rho,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(rho,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(rho,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(rho,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(rho,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(rho,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(rho,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(rho,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(rho,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(rho,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(rho,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(rho,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(rho,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(rho,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(rho,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(rho,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(rho,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(rho,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(rho,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(rho,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(rho,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(rho,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(rho,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(rho,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(rho,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(rho,0,0,5),0) + IfThen(tk == 
      4,-0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(rho,0,0,-1) + 
      GFOffset(rho,0,0,1)) - 
      0.0955144556251064670745298655398*(GFOffset(rho,0,0,-2) + 
      GFOffset(rho,0,0,2)) + 
      0.0741579827300490137576365968936*(GFOffset(rho,0,0,-3) + 
      GFOffset(rho,0,0,3)) - 
      0.0303817292054494222005208333333*(GFOffset(rho,0,0,-4) + 
      GFOffset(rho,0,0,4)),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(rho,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(rho,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(rho,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(rho,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(rho,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(rho,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(rho,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(rho,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(rho,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(rho,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(rho,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(rho,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(rho,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(rho,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(rho,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(rho,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(rho,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(rho,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(rho,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(rho,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(rho,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(rho,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(rho,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(rho,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(rho,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(rho,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(rho,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(rho,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(rho,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(rho,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(rho,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(rho,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(rho,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(rho,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(rho,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(rho,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(rho,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(rho,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(rho,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(rho,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(rho,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(rho,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(rho,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(rho,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(rho,5,0,0),0) + IfThen(ti == 
      4,-0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(rho,-1,0,0) + 
      GFOffset(rho,1,0,0)) - 
      0.0955144556251064670745298655398*(GFOffset(rho,-2,0,0) + 
      GFOffset(rho,2,0,0)) + 
      0.0741579827300490137576365968936*(GFOffset(rho,-3,0,0) + 
      GFOffset(rho,3,0,0)) - 
      0.0303817292054494222005208333333*(GFOffset(rho,-4,0,0) + 
      GFOffset(rho,4,0,0)),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(rho,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(rho,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(rho,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(rho,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(rho,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(rho,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(rho,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(rho,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(rho,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(rho,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(rho,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(rho,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(rho,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(rho,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(rho,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(rho,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(rho,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(rho,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(rho,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(rho,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(rho,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(rho,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(rho,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(rho,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(rho,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(rho,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(rho,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(rho,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(rho,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(rho,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(rho,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(rho,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(rho,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(rho,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(rho,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(rho,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(rho,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(rho,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(rho,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(rho,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(rho,0,5,0),0) + IfThen(tj == 
      4,-0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(rho,0,-1,0) + 
      GFOffset(rho,0,1,0)) - 
      0.0955144556251064670745298655398*(GFOffset(rho,0,-2,0) + 
      GFOffset(rho,0,2,0)) + 
      0.0741579827300490137576365968936*(GFOffset(rho,0,-3,0) + 
      GFOffset(rho,0,3,0)) - 
      0.0303817292054494222005208333333*(GFOffset(rho,0,-4,0) + 
      GFOffset(rho,0,4,0)),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(rho,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(rho,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(rho,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(rho,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(rho,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(rho,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(rho,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(rho,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(rho,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(rho,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(rho,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(rho,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(rho,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(rho,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(rho,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(rho,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(rho,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(rho,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(rho,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(rho,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(rho,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(rho,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(rho,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(rho,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(rho,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(rho,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(rho,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(rho,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(rho,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(rho,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(rho,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(rho,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(rho,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(rho,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(rho,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(rho,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(rho,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(rho,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(rho,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(rho,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(rho,0,0,5),0) + IfThen(tk == 
      4,-0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(rho,0,0,-1) + 
      GFOffset(rho,0,0,1)) - 
      0.0955144556251064670745298655398*(GFOffset(rho,0,0,-2) + 
      GFOffset(rho,0,0,2)) + 
      0.0741579827300490137576365968936*(GFOffset(rho,0,0,-3) + 
      GFOffset(rho,0,0,3)) - 
      0.0303817292054494222005208333333*(GFOffset(rho,0,0,-4) + 
      GFOffset(rho,0,0,4)),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(rho,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(rho,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(rho,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(rho,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(rho,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(rho,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(rho,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(rho,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(rho,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(rho,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(rho,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(rho,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(rho,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(rho,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(rho,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(rho,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(rho,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(rho,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(rho,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(rho,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(rho,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(rho,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(rho,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL v1rhsL CCTK_ATTRIBUTE_UNUSED = PDrho1 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(v1,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v1,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(v1,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(v1,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(v1,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(v1,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(v1,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(v1,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(v1,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(v1,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v1,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(v1,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(v1,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(v1,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(v1,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(v1,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(v1,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(v1,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v1,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v1,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(v1,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(v1,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(v1,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v1,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(v1,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(v1,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v1,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v1,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v1,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(v1,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v1,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(v1,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(v1,5,0,0),0) + IfThen(ti == 
      4,-0.111112010722257653061224489796*GFOffset(v1,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v1,-1,0,0) + 
      GFOffset(v1,1,0,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v1,-2,0,0) + 
      GFOffset(v1,2,0,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v1,-3,0,0) + 
      GFOffset(v1,3,0,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v1,-4,0,0) + 
      GFOffset(v1,4,0,0)),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(v1,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(v1,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v1,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(v1,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(v1,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v1,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v1,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(v1,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(v1,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v1,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(v1,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(v1,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(v1,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(v1,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v1,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(v1,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(v1,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(v1,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(v1,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(v1,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(v1,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(v1,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(v1,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v1,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(v1,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(v1,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(v1,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(v1,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(v1,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(v1,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(v1,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(v1,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(v1,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.112286322818464314978983667162*GFOffset(v1,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v1,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(v1,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(v1,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(v1,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(v1,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(v1,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(v1,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(v1,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(v1,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v1,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(v1,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(v1,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(v1,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(v1,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(v1,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(v1,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(v1,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(v1,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v1,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(v1,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(v1,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(v1,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(v1,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(v1,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(v1,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(v1,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(v1,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v1,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(v1,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(v1,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(v1,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(v1,0,5,0),0) + IfThen(tj == 
      4,-0.111112010722257653061224489796*GFOffset(v1,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v1,0,-1,0) + 
      GFOffset(v1,0,1,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v1,0,-2,0) + 
      GFOffset(v1,0,2,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v1,0,-3,0) + 
      GFOffset(v1,0,3,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v1,0,-4,0) + 
      GFOffset(v1,0,4,0)),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(v1,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(v1,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(v1,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(v1,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(v1,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v1,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(v1,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(v1,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(v1,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(v1,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(v1,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(v1,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(v1,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(v1,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v1,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(v1,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(v1,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(v1,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(v1,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(v1,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(v1,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(v1,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(v1,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v1,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(v1,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(v1,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(v1,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(v1,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(v1,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(v1,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(v1,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(v1,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(v1,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.112286322818464314978983667162*GFOffset(v1,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v1,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(v1,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(v1,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(v1,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(v1,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(v1,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(v1,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(v1,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(v1,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v1,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(v1,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(v1,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(v1,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(v1,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(v1,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(v1,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(v1,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(v1,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v1,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(v1,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(v1,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(v1,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(v1,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(v1,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(v1,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(v1,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(v1,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v1,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(v1,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(v1,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(v1,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(v1,0,0,5),0) + IfThen(tk == 
      4,-0.111112010722257653061224489796*GFOffset(v1,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v1,0,0,-1) + 
      GFOffset(v1,0,0,1)) - 
      0.0955144556251064670745298655398*(GFOffset(v1,0,0,-2) + 
      GFOffset(v1,0,0,2)) + 
      0.0741579827300490137576365968936*(GFOffset(v1,0,0,-3) + 
      GFOffset(v1,0,0,3)) - 
      0.0303817292054494222005208333333*(GFOffset(v1,0,0,-4) + 
      GFOffset(v1,0,0,4)),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(v1,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(v1,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(v1,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(v1,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(v1,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v1,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(v1,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(v1,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(v1,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(v1,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(v1,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(v1,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(v1,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(v1,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v1,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(v1,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(v1,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(v1,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(v1,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(v1,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(v1,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(v1,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(v1,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v1,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(v1,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(v1,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(v1,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(v1,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(v1,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(v1,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(v1,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(v1,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(v1,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.112286322818464314978983667162*GFOffset(v1,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v1,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(v1,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(v1,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(v1,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(v1,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(v1,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(v1,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(v1,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(v1,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v1,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(v1,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(v1,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(v1,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(v1,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(v1,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(v1,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(v1,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v1,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v1,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(v1,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(v1,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(v1,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v1,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(v1,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(v1,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v1,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v1,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v1,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(v1,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v1,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(v1,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(v1,5,0,0),0) + IfThen(ti == 
      4,-0.111112010722257653061224489796*GFOffset(v1,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v1,-1,0,0) + 
      GFOffset(v1,1,0,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v1,-2,0,0) + 
      GFOffset(v1,2,0,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v1,-3,0,0) + 
      GFOffset(v1,3,0,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v1,-4,0,0) + 
      GFOffset(v1,4,0,0)),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(v1,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(v1,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v1,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(v1,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(v1,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v1,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v1,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(v1,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(v1,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v1,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(v1,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(v1,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(v1,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(v1,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v1,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(v1,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(v1,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(v1,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(v1,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(v1,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(v1,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(v1,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(v1,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v1,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(v1,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(v1,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(v1,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(v1,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(v1,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(v1,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(v1,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(v1,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(v1,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.112286322818464314978983667162*GFOffset(v1,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v1,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(v1,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(v1,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(v1,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(v1,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(v1,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(v1,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(v1,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(v1,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v1,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(v1,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(v1,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(v1,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(v1,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(v1,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(v1,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(v1,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(v1,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v1,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(v1,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(v1,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(v1,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(v1,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(v1,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(v1,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(v1,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(v1,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v1,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(v1,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(v1,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(v1,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(v1,0,5,0),0) + IfThen(tj == 
      4,-0.111112010722257653061224489796*GFOffset(v1,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v1,0,-1,0) + 
      GFOffset(v1,0,1,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v1,0,-2,0) + 
      GFOffset(v1,0,2,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v1,0,-3,0) + 
      GFOffset(v1,0,3,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v1,0,-4,0) + 
      GFOffset(v1,0,4,0)),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(v1,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(v1,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(v1,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(v1,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(v1,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v1,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(v1,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(v1,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(v1,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(v1,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(v1,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(v1,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(v1,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(v1,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v1,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(v1,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(v1,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(v1,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(v1,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(v1,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(v1,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(v1,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(v1,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v1,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(v1,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(v1,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(v1,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(v1,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(v1,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(v1,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(v1,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(v1,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(v1,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.112286322818464314978983667162*GFOffset(v1,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v1,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(v1,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(v1,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(v1,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(v1,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(v1,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(v1,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(v1,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(v1,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v1,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(v1,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(v1,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(v1,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(v1,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(v1,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(v1,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(v1,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(v1,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v1,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(v1,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(v1,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(v1,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(v1,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(v1,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(v1,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(v1,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(v1,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v1,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(v1,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(v1,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(v1,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(v1,0,0,5),0) + IfThen(tk == 
      4,-0.111112010722257653061224489796*GFOffset(v1,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v1,0,0,-1) + 
      GFOffset(v1,0,0,1)) - 
      0.0955144556251064670745298655398*(GFOffset(v1,0,0,-2) + 
      GFOffset(v1,0,0,2)) + 
      0.0741579827300490137576365968936*(GFOffset(v1,0,0,-3) + 
      GFOffset(v1,0,0,3)) - 
      0.0303817292054494222005208333333*(GFOffset(v1,0,0,-4) + 
      GFOffset(v1,0,0,4)),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(v1,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(v1,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(v1,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(v1,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(v1,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v1,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v1,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(v1,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(v1,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(v1,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(v1,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(v1,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(v1,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(v1,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(v1,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v1,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v1,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(v1,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(v1,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(v1,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(v1,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(v1,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(v1,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(v1,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(v1,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v1,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v1,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(v1,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(v1,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(v1,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(v1,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(v1,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(v1,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(v1,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(v1,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(v1,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL v2rhsL CCTK_ATTRIBUTE_UNUSED = PDrho2 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(v2,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v2,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(v2,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(v2,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(v2,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(v2,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(v2,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(v2,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(v2,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(v2,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v2,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(v2,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(v2,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(v2,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(v2,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(v2,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(v2,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(v2,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v2,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v2,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(v2,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(v2,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(v2,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v2,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(v2,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(v2,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v2,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v2,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v2,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(v2,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v2,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(v2,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(v2,5,0,0),0) + IfThen(ti == 
      4,-0.111112010722257653061224489796*GFOffset(v2,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v2,-1,0,0) + 
      GFOffset(v2,1,0,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v2,-2,0,0) + 
      GFOffset(v2,2,0,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v2,-3,0,0) + 
      GFOffset(v2,3,0,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v2,-4,0,0) + 
      GFOffset(v2,4,0,0)),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(v2,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(v2,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v2,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(v2,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(v2,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v2,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v2,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(v2,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(v2,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v2,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(v2,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(v2,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(v2,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(v2,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v2,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(v2,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(v2,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(v2,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(v2,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(v2,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(v2,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(v2,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(v2,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v2,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(v2,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(v2,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(v2,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(v2,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(v2,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(v2,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(v2,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(v2,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(v2,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.112286322818464314978983667162*GFOffset(v2,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v2,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(v2,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(v2,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(v2,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(v2,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(v2,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(v2,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(v2,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(v2,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v2,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(v2,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(v2,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(v2,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(v2,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(v2,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(v2,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(v2,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(v2,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v2,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(v2,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(v2,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(v2,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(v2,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(v2,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(v2,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(v2,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(v2,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v2,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(v2,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(v2,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(v2,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(v2,0,5,0),0) + IfThen(tj == 
      4,-0.111112010722257653061224489796*GFOffset(v2,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v2,0,-1,0) + 
      GFOffset(v2,0,1,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v2,0,-2,0) + 
      GFOffset(v2,0,2,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v2,0,-3,0) + 
      GFOffset(v2,0,3,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v2,0,-4,0) + 
      GFOffset(v2,0,4,0)),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(v2,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(v2,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(v2,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(v2,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(v2,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v2,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(v2,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(v2,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(v2,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(v2,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(v2,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(v2,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(v2,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(v2,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v2,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(v2,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(v2,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(v2,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(v2,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(v2,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(v2,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(v2,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(v2,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v2,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(v2,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(v2,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(v2,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(v2,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(v2,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(v2,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(v2,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(v2,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(v2,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.112286322818464314978983667162*GFOffset(v2,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v2,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(v2,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(v2,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(v2,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(v2,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(v2,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(v2,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(v2,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(v2,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v2,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(v2,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(v2,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(v2,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(v2,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(v2,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(v2,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(v2,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(v2,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v2,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(v2,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(v2,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(v2,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(v2,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(v2,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(v2,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(v2,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(v2,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v2,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(v2,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(v2,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(v2,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(v2,0,0,5),0) + IfThen(tk == 
      4,-0.111112010722257653061224489796*GFOffset(v2,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v2,0,0,-1) + 
      GFOffset(v2,0,0,1)) - 
      0.0955144556251064670745298655398*(GFOffset(v2,0,0,-2) + 
      GFOffset(v2,0,0,2)) + 
      0.0741579827300490137576365968936*(GFOffset(v2,0,0,-3) + 
      GFOffset(v2,0,0,3)) - 
      0.0303817292054494222005208333333*(GFOffset(v2,0,0,-4) + 
      GFOffset(v2,0,0,4)),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(v2,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(v2,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(v2,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(v2,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(v2,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v2,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(v2,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(v2,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(v2,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(v2,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(v2,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(v2,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(v2,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(v2,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v2,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(v2,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(v2,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(v2,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(v2,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(v2,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(v2,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(v2,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(v2,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v2,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(v2,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(v2,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(v2,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(v2,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(v2,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(v2,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(v2,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(v2,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(v2,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.112286322818464314978983667162*GFOffset(v2,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v2,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(v2,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(v2,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(v2,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(v2,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(v2,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(v2,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(v2,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(v2,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v2,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(v2,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(v2,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(v2,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(v2,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(v2,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(v2,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(v2,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v2,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v2,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(v2,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(v2,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(v2,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v2,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(v2,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(v2,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v2,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v2,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v2,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(v2,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v2,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(v2,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(v2,5,0,0),0) + IfThen(ti == 
      4,-0.111112010722257653061224489796*GFOffset(v2,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v2,-1,0,0) + 
      GFOffset(v2,1,0,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v2,-2,0,0) + 
      GFOffset(v2,2,0,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v2,-3,0,0) + 
      GFOffset(v2,3,0,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v2,-4,0,0) + 
      GFOffset(v2,4,0,0)),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(v2,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(v2,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v2,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(v2,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(v2,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v2,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v2,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(v2,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(v2,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v2,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(v2,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(v2,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(v2,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(v2,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v2,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(v2,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(v2,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(v2,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(v2,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(v2,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(v2,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(v2,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(v2,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v2,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(v2,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(v2,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(v2,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(v2,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(v2,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(v2,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(v2,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(v2,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(v2,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.112286322818464314978983667162*GFOffset(v2,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v2,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(v2,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(v2,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(v2,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(v2,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(v2,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(v2,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(v2,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(v2,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v2,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(v2,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(v2,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(v2,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(v2,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(v2,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(v2,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(v2,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(v2,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v2,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(v2,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(v2,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(v2,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(v2,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(v2,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(v2,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(v2,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(v2,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v2,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(v2,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(v2,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(v2,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(v2,0,5,0),0) + IfThen(tj == 
      4,-0.111112010722257653061224489796*GFOffset(v2,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v2,0,-1,0) + 
      GFOffset(v2,0,1,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v2,0,-2,0) + 
      GFOffset(v2,0,2,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v2,0,-3,0) + 
      GFOffset(v2,0,3,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v2,0,-4,0) + 
      GFOffset(v2,0,4,0)),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(v2,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(v2,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(v2,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(v2,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(v2,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v2,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(v2,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(v2,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(v2,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(v2,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(v2,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(v2,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(v2,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(v2,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v2,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(v2,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(v2,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(v2,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(v2,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(v2,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(v2,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(v2,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(v2,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v2,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(v2,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(v2,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(v2,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(v2,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(v2,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(v2,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(v2,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(v2,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(v2,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.112286322818464314978983667162*GFOffset(v2,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v2,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(v2,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(v2,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(v2,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(v2,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(v2,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(v2,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(v2,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(v2,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v2,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(v2,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(v2,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(v2,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(v2,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(v2,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(v2,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(v2,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(v2,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v2,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(v2,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(v2,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(v2,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(v2,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(v2,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(v2,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(v2,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(v2,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v2,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(v2,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(v2,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(v2,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(v2,0,0,5),0) + IfThen(tk == 
      4,-0.111112010722257653061224489796*GFOffset(v2,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v2,0,0,-1) + 
      GFOffset(v2,0,0,1)) - 
      0.0955144556251064670745298655398*(GFOffset(v2,0,0,-2) + 
      GFOffset(v2,0,0,2)) + 
      0.0741579827300490137576365968936*(GFOffset(v2,0,0,-3) + 
      GFOffset(v2,0,0,3)) - 
      0.0303817292054494222005208333333*(GFOffset(v2,0,0,-4) + 
      GFOffset(v2,0,0,4)),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(v2,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(v2,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(v2,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(v2,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(v2,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v2,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v2,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(v2,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(v2,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(v2,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(v2,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(v2,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(v2,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(v2,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(v2,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v2,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v2,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(v2,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(v2,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(v2,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(v2,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(v2,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(v2,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(v2,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(v2,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v2,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v2,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(v2,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(v2,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(v2,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(v2,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(v2,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(v2,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(v2,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(v2,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(v2,0,0,0),0))*pow(dz,-1)));
    
    CCTK_REAL v3rhsL CCTK_ATTRIBUTE_UNUSED = PDrho3 + 
      IfThen(usejacobian,myDetJL*epsDiss*((IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(v3,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v3,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(v3,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(v3,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(v3,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(v3,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(v3,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(v3,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(v3,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(v3,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v3,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(v3,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(v3,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(v3,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(v3,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(v3,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(v3,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(v3,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v3,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v3,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(v3,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(v3,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(v3,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v3,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(v3,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(v3,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v3,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v3,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v3,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(v3,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v3,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(v3,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(v3,5,0,0),0) + IfThen(ti == 
      4,-0.111112010722257653061224489796*GFOffset(v3,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v3,-1,0,0) + 
      GFOffset(v3,1,0,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v3,-2,0,0) + 
      GFOffset(v3,2,0,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v3,-3,0,0) + 
      GFOffset(v3,3,0,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v3,-4,0,0) + 
      GFOffset(v3,4,0,0)),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(v3,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(v3,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v3,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(v3,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(v3,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v3,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v3,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(v3,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(v3,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v3,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(v3,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(v3,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(v3,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(v3,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v3,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(v3,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(v3,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(v3,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(v3,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(v3,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(v3,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(v3,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(v3,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v3,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(v3,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(v3,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(v3,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(v3,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(v3,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(v3,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(v3,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(v3,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(v3,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.112286322818464314978983667162*GFOffset(v3,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v3,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(v3,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(v3,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(v3,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(v3,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(v3,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(v3,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(v3,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(v3,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v3,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(v3,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(v3,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(v3,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(v3,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(v3,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(v3,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(v3,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(v3,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v3,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(v3,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(v3,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(v3,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(v3,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(v3,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(v3,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(v3,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(v3,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v3,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(v3,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(v3,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(v3,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(v3,0,5,0),0) + IfThen(tj == 
      4,-0.111112010722257653061224489796*GFOffset(v3,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v3,0,-1,0) + 
      GFOffset(v3,0,1,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v3,0,-2,0) + 
      GFOffset(v3,0,2,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v3,0,-3,0) + 
      GFOffset(v3,0,3,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v3,0,-4,0) + 
      GFOffset(v3,0,4,0)),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(v3,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(v3,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(v3,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(v3,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(v3,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v3,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(v3,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(v3,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(v3,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(v3,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(v3,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(v3,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(v3,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(v3,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v3,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(v3,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(v3,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(v3,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(v3,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(v3,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(v3,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(v3,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(v3,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v3,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(v3,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(v3,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(v3,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(v3,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(v3,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(v3,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(v3,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(v3,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(v3,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.112286322818464314978983667162*GFOffset(v3,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v3,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(v3,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(v3,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(v3,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(v3,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(v3,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(v3,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(v3,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(v3,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v3,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(v3,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(v3,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(v3,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(v3,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(v3,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(v3,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(v3,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(v3,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v3,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(v3,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(v3,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(v3,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(v3,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(v3,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(v3,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(v3,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(v3,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v3,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(v3,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(v3,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(v3,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(v3,0,0,5),0) + IfThen(tk == 
      4,-0.111112010722257653061224489796*GFOffset(v3,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v3,0,0,-1) + 
      GFOffset(v3,0,0,1)) - 
      0.0955144556251064670745298655398*(GFOffset(v3,0,0,-2) + 
      GFOffset(v3,0,0,2)) + 
      0.0741579827300490137576365968936*(GFOffset(v3,0,0,-3) + 
      GFOffset(v3,0,0,3)) - 
      0.0303817292054494222005208333333*(GFOffset(v3,0,0,-4) + 
      GFOffset(v3,0,0,4)),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(v3,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(v3,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(v3,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(v3,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(v3,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v3,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(v3,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(v3,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(v3,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(v3,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(v3,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(v3,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(v3,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(v3,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v3,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(v3,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(v3,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(v3,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(v3,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(v3,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(v3,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(v3,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(v3,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v3,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(v3,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(v3,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(v3,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(v3,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(v3,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(v3,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(v3,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(v3,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(v3,0,0,0),0))*pow(dz,-1)),epsDiss*((IfThen(ti 
      == 0,-0.112286322818464314978983667162*GFOffset(v3,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v3,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(v3,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(v3,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(v3,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(v3,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(v3,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(v3,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(v3,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(v3,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v3,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(v3,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(v3,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(v3,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(v3,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(v3,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(v3,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(v3,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v3,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v3,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(v3,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(v3,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(v3,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v3,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(v3,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(v3,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v3,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v3,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v3,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(v3,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v3,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(v3,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(v3,5,0,0),0) + IfThen(ti == 
      4,-0.111112010722257653061224489796*GFOffset(v3,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v3,-1,0,0) + 
      GFOffset(v3,1,0,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v3,-2,0,0) + 
      GFOffset(v3,2,0,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v3,-3,0,0) + 
      GFOffset(v3,3,0,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v3,-4,0,0) + 
      GFOffset(v3,4,0,0)),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(v3,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(v3,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(v3,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(v3,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(v3,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v3,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(v3,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(v3,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(v3,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(v3,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(v3,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(v3,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(v3,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(v3,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v3,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(v3,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(v3,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(v3,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(v3,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(v3,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(v3,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(v3,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(v3,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v3,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(v3,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(v3,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(v3,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(v3,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(v3,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(v3,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(v3,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(v3,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(v3,0,0,0),0))*pow(dx,-1) + 
      (IfThen(tj == 0,-0.112286322818464314978983667162*GFOffset(v3,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v3,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(v3,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(v3,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(v3,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(v3,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(v3,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(v3,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(v3,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(v3,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v3,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(v3,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(v3,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(v3,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(v3,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(v3,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(v3,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(v3,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(v3,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v3,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(v3,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(v3,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(v3,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(v3,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(v3,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(v3,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(v3,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(v3,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v3,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(v3,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(v3,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(v3,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(v3,0,5,0),0) + IfThen(tj == 
      4,-0.111112010722257653061224489796*GFOffset(v3,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v3,0,-1,0) + 
      GFOffset(v3,0,1,0)) - 
      0.0955144556251064670745298655398*(GFOffset(v3,0,-2,0) + 
      GFOffset(v3,0,2,0)) + 
      0.0741579827300490137576365968936*(GFOffset(v3,0,-3,0) + 
      GFOffset(v3,0,3,0)) - 
      0.0303817292054494222005208333333*(GFOffset(v3,0,-4,0) + 
      GFOffset(v3,0,4,0)),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(v3,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(v3,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(v3,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(v3,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(v3,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v3,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(v3,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(v3,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(v3,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(v3,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(v3,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(v3,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(v3,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(v3,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v3,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(v3,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(v3,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(v3,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(v3,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(v3,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(v3,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(v3,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(v3,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v3,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(v3,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(v3,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(v3,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(v3,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(v3,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(v3,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(v3,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(v3,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(v3,0,0,0),0))*pow(dy,-1) + 
      (IfThen(tk == 0,-0.112286322818464314978983667162*GFOffset(v3,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(v3,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(v3,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(v3,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(v3,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(v3,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(v3,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(v3,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(v3,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(v3,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(v3,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(v3,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(v3,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(v3,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(v3,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(v3,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(v3,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(v3,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(v3,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(v3,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(v3,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(v3,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(v3,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(v3,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(v3,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(v3,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(v3,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(v3,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(v3,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(v3,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(v3,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(v3,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(v3,0,0,5),0) + IfThen(tk == 
      4,-0.111112010722257653061224489796*GFOffset(v3,0,0,0) + 
      0.107294207461635702048026346877*(GFOffset(v3,0,0,-1) + 
      GFOffset(v3,0,0,1)) - 
      0.0955144556251064670745298655398*(GFOffset(v3,0,0,-2) + 
      GFOffset(v3,0,0,2)) + 
      0.0741579827300490137576365968936*(GFOffset(v3,0,0,-3) + 
      GFOffset(v3,0,0,3)) - 
      0.0303817292054494222005208333333*(GFOffset(v3,0,0,-4) + 
      GFOffset(v3,0,0,4)),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(v3,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(v3,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(v3,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(v3,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(v3,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(v3,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(v3,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(v3,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(v3,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(v3,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(v3,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(v3,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(v3,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(v3,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(v3,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(v3,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(v3,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(v3,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(v3,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(v3,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(v3,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(v3,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(v3,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(v3,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(v3,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(v3,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(v3,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(v3,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(v3,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(v3,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(v3,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(v3,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(v3,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(v3,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(v3,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(v3,0,0,0),0))*pow(dz,-1)));
    /* Copy local copies back to grid functions */
    rhorhs[index] = rhorhsL;
    urhs[index] = urhsL;
    v1rhs[index] = v1rhsL;
    v2rhs[index] = v2rhsL;
    v3rhs[index] = v3rhsL;
  }
  CCTK_ENDLOOP3(ML_WaveToy_DG8_RHSFirstOrder);
}
extern "C" void ML_WaveToy_DG8_RHSFirstOrder(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_WaveToy_DG8_RHSFirstOrder_Body");
  }
  if (cctk_iteration % ML_WaveToy_DG8_RHSFirstOrder_calc_every != ML_WaveToy_DG8_RHSFirstOrder_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_WaveToy_DG8::WT_detJ",
    "ML_WaveToy_DG8::WT_rho",
    "ML_WaveToy_DG8::WT_rhorhs",
    "ML_WaveToy_DG8::WT_u",
    "ML_WaveToy_DG8::WT_urhs",
    "ML_WaveToy_DG8::WT_v",
    "ML_WaveToy_DG8::WT_vrhs"};
  AssertGroupStorage(cctkGH, "ML_WaveToy_DG8_RHSFirstOrder", 7, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_WaveToy_DG8_RHSFirstOrder_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_WaveToy_DG8_RHSFirstOrder_Body");
  }
}

} // namespace ML_WaveToy_DG8
