/*  File produced by Kranc */

#include "cctk.h"

extern "C" int ML_WaveToy_DG8_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "ML_WaveToy_DG8";
  CCTK_RegisterBanner(banner);
  return 0;
}
