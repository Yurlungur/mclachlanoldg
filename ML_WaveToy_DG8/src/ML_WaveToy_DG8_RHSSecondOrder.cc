/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_WaveToy_DG8 {

extern "C" void ML_WaveToy_DG8_RHSSecondOrder_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_WaveToy_DG8_RHSSecondOrder_calc_every != ML_WaveToy_DG8_RHSSecondOrder_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG8::WT_rhorhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG8::WT_rhorhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG8::WT_urhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG8::WT_urhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_WaveToy_DG8::WT_vrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_WaveToy_DG8::WT_vrhs.");
  return;
}

static void ML_WaveToy_DG8_RHSSecondOrder_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_WaveToy_DG8_RHSSecondOrder,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL du1L CCTK_ATTRIBUTE_UNUSED = du1[index];
    CCTK_REAL du2L CCTK_ATTRIBUTE_UNUSED = du2[index];
    CCTK_REAL du3L CCTK_ATTRIBUTE_UNUSED = du3[index];
    CCTK_REAL myDetJL CCTK_ATTRIBUTE_UNUSED = myDetJ[index];
    CCTK_REAL rhoL CCTK_ATTRIBUTE_UNUSED = rho[index];
    CCTK_REAL uL CCTK_ATTRIBUTE_UNUSED = u[index];
    
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDdu11 CCTK_ATTRIBUTE_UNUSED = 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-2.*GFOffset(du1,0,0,0) + 
      24.349745171593065826474565138*GFOffset(du1,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(du1,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(du1,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(du1,4,0,0) + 
      2.5907456765593549921859155848*GFOffset(du1,5,0,0) - 
      1.8744408734469832436758584433*GFOffset(du1,6,0,0) + 
      1.2848306326995883333410042531*GFOffset(du1,7,0,0) + 
      1.5*GFOffset(du1,8,0,0),0) + IfThen(ti == 
      1,-3.2676328094883065529305901673*GFOffset(du1,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(du1,1,0,0) - 
      2.6960654403140560289938204769*GFOffset(du1,2,0,0) + 
      1.6652216450053851807954752347*GFOffset(du1,3,0,0) - 
      1.1456537384551323190125010084*GFOffset(du1,4,0,0) + 
      0.816756381741385878117230629*GFOffset(du1,5,0,0) - 
      0.5557049812837167854026659932*GFOffset(du1,6,0,0) - 
      0.6037268738428710466635005583*GFOffset(du1,7,0,0),0) + IfThen(ti == 
      2,0.3491845766773346366159606025*GFOffset(du1,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(du1,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(du1,1,0,0) - 
      1.7178321571950627779478085413*GFOffset(du1,2,0,0) + 
      1.0798038112826304844454349794*GFOffset(du1,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(du1,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(du1,5,0,0) + 
      0.4465199214188158749748120629*GFOffset(du1,6,0,0),0) + IfThen(ti == 
      3,0.1217196331091731827490296503*GFOffset(du1,-3,0,0) + 
      1.2879607500639065480082640515*GFOffset(du1,-2,0,0) - 
      2.8344589120794203953271610371*GFOffset(du1,-1,0,0) + 
      2.8519159684628953883074916029*GFOffset(du1,1,0,0) - 
      1.3769648937605120893444713842*GFOffset(du1,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(du1,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(du1,4,0,0) - 
      0.3585985703546666387999883421*GFOffset(du1,5,0,0),0) + IfThen(ti == 
      4,-0.2734375*GFOffset(du1,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(du1,-3,0,0) + 
      1.2694130863581495324215740932*GFOffset(du1,-2,0,0) - 
      2.6593102175739179929283553282*GFOffset(du1,-1,0,0) + 
      2.6593102175739179929283553282*GFOffset(du1,1,0,0) - 
      1.2694130863581495324215740932*GFOffset(du1,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(du1,3,0,0) + 
      0.2734375*GFOffset(du1,4,0,0),0) + IfThen(ti == 
      5,0.3585985703546666387999883421*GFOffset(du1,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(du1,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(du1,-3,0,0) + 
      1.3769648937605120893444713842*GFOffset(du1,-2,0,0) - 
      2.8519159684628953883074916029*GFOffset(du1,-1,0,0) + 
      2.8344589120794203953271610371*GFOffset(du1,1,0,0) - 
      1.2879607500639065480082640515*GFOffset(du1,2,0,0) - 
      0.1217196331091731827490296503*GFOffset(du1,3,0,0),0) + IfThen(ti == 
      6,-0.4465199214188158749748120629*GFOffset(du1,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(du1,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(du1,-4,0,0) - 
      1.0798038112826304844454349794*GFOffset(du1,-3,0,0) + 
      1.7178321571950627779478085413*GFOffset(du1,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(du1,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(du1,1,0,0) - 
      0.3491845766773346366159606025*GFOffset(du1,2,0,0),0) + IfThen(ti == 
      7,0.6037268738428710466635005583*GFOffset(du1,-7,0,0) + 
      0.5557049812837167854026659932*GFOffset(du1,-6,0,0) - 
      0.816756381741385878117230629*GFOffset(du1,-5,0,0) + 
      1.1456537384551323190125010084*GFOffset(du1,-4,0,0) - 
      1.6652216450053851807954752347*GFOffset(du1,-3,0,0) + 
      2.6960654403140560289938204769*GFOffset(du1,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(du1,-1,0,0) + 
      3.2676328094883065529305901673*GFOffset(du1,1,0,0),0) + IfThen(ti == 
      8,-1.5*GFOffset(du1,-8,0,0) - 
      1.2848306326995883333410042531*GFOffset(du1,-7,0,0) + 
      1.8744408734469832436758584433*GFOffset(du1,-6,0,0) - 
      2.5907456765593549921859155848*GFOffset(du1,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(du1,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(du1,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(du1,-2,0,0) - 
      24.349745171593065826474565138*GFOffset(du1,-1,0,0) + 
      2.*GFOffset(du1,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,32.*GFOffset(du1,-1,0,0) + 4.*GFOffset(du1,9,0,0),0) + IfThen(ti == 
      1,1.6387617850907400720974393616*GFOffset(du1,-2,0,0) - 
      1.6387617850907400720974393616*GFOffset(du1,8,0,0),0) + IfThen(ti == 
      2,-1.2723510267943446305827338052*GFOffset(du1,-3,0,0) + 
      1.2723510267943446305827338052*GFOffset(du1,7,0,0),0) + IfThen(ti == 
      3,1.1326661647805276354083713666*GFOffset(du1,-4,0,0) - 
      1.1326661647805276354083713666*GFOffset(du1,6,0,0),0) + IfThen(ti == 
      4,-1.09375*GFOffset(du1,-5,0,0) + 1.09375*GFOffset(du1,5,0,0),0) + 
      IfThen(ti == 5,1.1326661647805276354083713666*GFOffset(du1,-6,0,0) - 
      1.1326661647805276354083713666*GFOffset(du1,4,0,0),0) + IfThen(ti == 
      6,-1.2723510267943446305827338052*GFOffset(du1,-7,0,0) + 
      1.2723510267943446305827338052*GFOffset(du1,3,0,0),0) + IfThen(ti == 
      7,1.6387617850907400720974393616*GFOffset(du1,-8,0,0) - 
      1.6387617850907400720974393616*GFOffset(du1,2,0,0),0) + IfThen(ti == 
      8,-4.*GFOffset(du1,-9,0,0) - 32.*GFOffset(du1,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDdu12 CCTK_ATTRIBUTE_UNUSED = 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-2.*GFOffset(du1,0,0,0) + 
      24.349745171593065826474565138*GFOffset(du1,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(du1,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(du1,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(du1,0,4,0) + 
      2.5907456765593549921859155848*GFOffset(du1,0,5,0) - 
      1.8744408734469832436758584433*GFOffset(du1,0,6,0) + 
      1.2848306326995883333410042531*GFOffset(du1,0,7,0) + 
      1.5*GFOffset(du1,0,8,0),0) + IfThen(tj == 
      1,-3.2676328094883065529305901673*GFOffset(du1,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(du1,0,1,0) - 
      2.6960654403140560289938204769*GFOffset(du1,0,2,0) + 
      1.6652216450053851807954752347*GFOffset(du1,0,3,0) - 
      1.1456537384551323190125010084*GFOffset(du1,0,4,0) + 
      0.816756381741385878117230629*GFOffset(du1,0,5,0) - 
      0.5557049812837167854026659932*GFOffset(du1,0,6,0) - 
      0.6037268738428710466635005583*GFOffset(du1,0,7,0),0) + IfThen(tj == 
      2,0.3491845766773346366159606025*GFOffset(du1,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(du1,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(du1,0,1,0) - 
      1.7178321571950627779478085413*GFOffset(du1,0,2,0) + 
      1.0798038112826304844454349794*GFOffset(du1,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(du1,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(du1,0,5,0) + 
      0.4465199214188158749748120629*GFOffset(du1,0,6,0),0) + IfThen(tj == 
      3,0.1217196331091731827490296503*GFOffset(du1,0,-3,0) + 
      1.2879607500639065480082640515*GFOffset(du1,0,-2,0) - 
      2.8344589120794203953271610371*GFOffset(du1,0,-1,0) + 
      2.8519159684628953883074916029*GFOffset(du1,0,1,0) - 
      1.3769648937605120893444713842*GFOffset(du1,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(du1,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(du1,0,4,0) - 
      0.3585985703546666387999883421*GFOffset(du1,0,5,0),0) + IfThen(tj == 
      4,-0.2734375*GFOffset(du1,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(du1,0,-3,0) + 
      1.2694130863581495324215740932*GFOffset(du1,0,-2,0) - 
      2.6593102175739179929283553282*GFOffset(du1,0,-1,0) + 
      2.6593102175739179929283553282*GFOffset(du1,0,1,0) - 
      1.2694130863581495324215740932*GFOffset(du1,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(du1,0,3,0) + 
      0.2734375*GFOffset(du1,0,4,0),0) + IfThen(tj == 
      5,0.3585985703546666387999883421*GFOffset(du1,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(du1,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(du1,0,-3,0) + 
      1.3769648937605120893444713842*GFOffset(du1,0,-2,0) - 
      2.8519159684628953883074916029*GFOffset(du1,0,-1,0) + 
      2.8344589120794203953271610371*GFOffset(du1,0,1,0) - 
      1.2879607500639065480082640515*GFOffset(du1,0,2,0) - 
      0.1217196331091731827490296503*GFOffset(du1,0,3,0),0) + IfThen(tj == 
      6,-0.4465199214188158749748120629*GFOffset(du1,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(du1,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(du1,0,-4,0) - 
      1.0798038112826304844454349794*GFOffset(du1,0,-3,0) + 
      1.7178321571950627779478085413*GFOffset(du1,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(du1,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(du1,0,1,0) - 
      0.3491845766773346366159606025*GFOffset(du1,0,2,0),0) + IfThen(tj == 
      7,0.6037268738428710466635005583*GFOffset(du1,0,-7,0) + 
      0.5557049812837167854026659932*GFOffset(du1,0,-6,0) - 
      0.816756381741385878117230629*GFOffset(du1,0,-5,0) + 
      1.1456537384551323190125010084*GFOffset(du1,0,-4,0) - 
      1.6652216450053851807954752347*GFOffset(du1,0,-3,0) + 
      2.6960654403140560289938204769*GFOffset(du1,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(du1,0,-1,0) + 
      3.2676328094883065529305901673*GFOffset(du1,0,1,0),0) + IfThen(tj == 
      8,-1.5*GFOffset(du1,0,-8,0) - 
      1.2848306326995883333410042531*GFOffset(du1,0,-7,0) + 
      1.8744408734469832436758584433*GFOffset(du1,0,-6,0) - 
      2.5907456765593549921859155848*GFOffset(du1,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(du1,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(du1,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(du1,0,-2,0) - 
      24.349745171593065826474565138*GFOffset(du1,0,-1,0) + 
      2.*GFOffset(du1,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,32.*GFOffset(du1,0,-1,0) + 4.*GFOffset(du1,0,9,0),0) + IfThen(tj == 
      1,1.6387617850907400720974393616*GFOffset(du1,0,-2,0) - 
      1.6387617850907400720974393616*GFOffset(du1,0,8,0),0) + IfThen(tj == 
      2,-1.2723510267943446305827338052*GFOffset(du1,0,-3,0) + 
      1.2723510267943446305827338052*GFOffset(du1,0,7,0),0) + IfThen(tj == 
      3,1.1326661647805276354083713666*GFOffset(du1,0,-4,0) - 
      1.1326661647805276354083713666*GFOffset(du1,0,6,0),0) + IfThen(tj == 
      4,-1.09375*GFOffset(du1,0,-5,0) + 1.09375*GFOffset(du1,0,5,0),0) + 
      IfThen(tj == 5,1.1326661647805276354083713666*GFOffset(du1,0,-6,0) - 
      1.1326661647805276354083713666*GFOffset(du1,0,4,0),0) + IfThen(tj == 
      6,-1.2723510267943446305827338052*GFOffset(du1,0,-7,0) + 
      1.2723510267943446305827338052*GFOffset(du1,0,3,0),0) + IfThen(tj == 
      7,1.6387617850907400720974393616*GFOffset(du1,0,-8,0) - 
      1.6387617850907400720974393616*GFOffset(du1,0,2,0),0) + IfThen(tj == 
      8,-4.*GFOffset(du1,0,-9,0) - 32.*GFOffset(du1,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDdu13 CCTK_ATTRIBUTE_UNUSED = 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-2.*GFOffset(du1,0,0,0) + 
      24.349745171593065826474565138*GFOffset(du1,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(du1,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(du1,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(du1,0,0,4) + 
      2.5907456765593549921859155848*GFOffset(du1,0,0,5) - 
      1.8744408734469832436758584433*GFOffset(du1,0,0,6) + 
      1.2848306326995883333410042531*GFOffset(du1,0,0,7) + 
      1.5*GFOffset(du1,0,0,8),0) + IfThen(tk == 
      1,-3.2676328094883065529305901673*GFOffset(du1,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(du1,0,0,1) - 
      2.6960654403140560289938204769*GFOffset(du1,0,0,2) + 
      1.6652216450053851807954752347*GFOffset(du1,0,0,3) - 
      1.1456537384551323190125010084*GFOffset(du1,0,0,4) + 
      0.816756381741385878117230629*GFOffset(du1,0,0,5) - 
      0.5557049812837167854026659932*GFOffset(du1,0,0,6) - 
      0.6037268738428710466635005583*GFOffset(du1,0,0,7),0) + IfThen(tk == 
      2,0.3491845766773346366159606025*GFOffset(du1,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(du1,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(du1,0,0,1) - 
      1.7178321571950627779478085413*GFOffset(du1,0,0,2) + 
      1.0798038112826304844454349794*GFOffset(du1,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(du1,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(du1,0,0,5) + 
      0.4465199214188158749748120629*GFOffset(du1,0,0,6),0) + IfThen(tk == 
      3,0.1217196331091731827490296503*GFOffset(du1,0,0,-3) + 
      1.2879607500639065480082640515*GFOffset(du1,0,0,-2) - 
      2.8344589120794203953271610371*GFOffset(du1,0,0,-1) + 
      2.8519159684628953883074916029*GFOffset(du1,0,0,1) - 
      1.3769648937605120893444713842*GFOffset(du1,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(du1,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(du1,0,0,4) - 
      0.3585985703546666387999883421*GFOffset(du1,0,0,5),0) + IfThen(tk == 
      4,-0.2734375*GFOffset(du1,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(du1,0,0,-3) + 
      1.2694130863581495324215740932*GFOffset(du1,0,0,-2) - 
      2.6593102175739179929283553282*GFOffset(du1,0,0,-1) + 
      2.6593102175739179929283553282*GFOffset(du1,0,0,1) - 
      1.2694130863581495324215740932*GFOffset(du1,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(du1,0,0,3) + 
      0.2734375*GFOffset(du1,0,0,4),0) + IfThen(tk == 
      5,0.3585985703546666387999883421*GFOffset(du1,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(du1,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(du1,0,0,-3) + 
      1.3769648937605120893444713842*GFOffset(du1,0,0,-2) - 
      2.8519159684628953883074916029*GFOffset(du1,0,0,-1) + 
      2.8344589120794203953271610371*GFOffset(du1,0,0,1) - 
      1.2879607500639065480082640515*GFOffset(du1,0,0,2) - 
      0.1217196331091731827490296503*GFOffset(du1,0,0,3),0) + IfThen(tk == 
      6,-0.4465199214188158749748120629*GFOffset(du1,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(du1,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(du1,0,0,-4) - 
      1.0798038112826304844454349794*GFOffset(du1,0,0,-3) + 
      1.7178321571950627779478085413*GFOffset(du1,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(du1,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(du1,0,0,1) - 
      0.3491845766773346366159606025*GFOffset(du1,0,0,2),0) + IfThen(tk == 
      7,0.6037268738428710466635005583*GFOffset(du1,0,0,-7) + 
      0.5557049812837167854026659932*GFOffset(du1,0,0,-6) - 
      0.816756381741385878117230629*GFOffset(du1,0,0,-5) + 
      1.1456537384551323190125010084*GFOffset(du1,0,0,-4) - 
      1.6652216450053851807954752347*GFOffset(du1,0,0,-3) + 
      2.6960654403140560289938204769*GFOffset(du1,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(du1,0,0,-1) + 
      3.2676328094883065529305901673*GFOffset(du1,0,0,1),0) + IfThen(tk == 
      8,-1.5*GFOffset(du1,0,0,-8) - 
      1.2848306326995883333410042531*GFOffset(du1,0,0,-7) + 
      1.8744408734469832436758584433*GFOffset(du1,0,0,-6) - 
      2.5907456765593549921859155848*GFOffset(du1,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(du1,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(du1,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(du1,0,0,-2) - 
      24.349745171593065826474565138*GFOffset(du1,0,0,-1) + 
      2.*GFOffset(du1,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,32.*GFOffset(du1,0,0,-1) + 4.*GFOffset(du1,0,0,9),0) + IfThen(tk == 
      1,1.6387617850907400720974393616*GFOffset(du1,0,0,-2) - 
      1.6387617850907400720974393616*GFOffset(du1,0,0,8),0) + IfThen(tk == 
      2,-1.2723510267943446305827338052*GFOffset(du1,0,0,-3) + 
      1.2723510267943446305827338052*GFOffset(du1,0,0,7),0) + IfThen(tk == 
      3,1.1326661647805276354083713666*GFOffset(du1,0,0,-4) - 
      1.1326661647805276354083713666*GFOffset(du1,0,0,6),0) + IfThen(tk == 
      4,-1.09375*GFOffset(du1,0,0,-5) + 1.09375*GFOffset(du1,0,0,5),0) + 
      IfThen(tk == 5,1.1326661647805276354083713666*GFOffset(du1,0,0,-6) - 
      1.1326661647805276354083713666*GFOffset(du1,0,0,4),0) + IfThen(tk == 
      6,-1.2723510267943446305827338052*GFOffset(du1,0,0,-7) + 
      1.2723510267943446305827338052*GFOffset(du1,0,0,3),0) + IfThen(tk == 
      7,1.6387617850907400720974393616*GFOffset(du1,0,0,-8) - 
      1.6387617850907400720974393616*GFOffset(du1,0,0,2),0) + IfThen(tk == 
      8,-4.*GFOffset(du1,0,0,-9) - 32.*GFOffset(du1,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDdu21 CCTK_ATTRIBUTE_UNUSED = 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-2.*GFOffset(du2,0,0,0) + 
      24.349745171593065826474565138*GFOffset(du2,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(du2,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(du2,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(du2,4,0,0) + 
      2.5907456765593549921859155848*GFOffset(du2,5,0,0) - 
      1.8744408734469832436758584433*GFOffset(du2,6,0,0) + 
      1.2848306326995883333410042531*GFOffset(du2,7,0,0) + 
      1.5*GFOffset(du2,8,0,0),0) + IfThen(ti == 
      1,-3.2676328094883065529305901673*GFOffset(du2,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(du2,1,0,0) - 
      2.6960654403140560289938204769*GFOffset(du2,2,0,0) + 
      1.6652216450053851807954752347*GFOffset(du2,3,0,0) - 
      1.1456537384551323190125010084*GFOffset(du2,4,0,0) + 
      0.816756381741385878117230629*GFOffset(du2,5,0,0) - 
      0.5557049812837167854026659932*GFOffset(du2,6,0,0) - 
      0.6037268738428710466635005583*GFOffset(du2,7,0,0),0) + IfThen(ti == 
      2,0.3491845766773346366159606025*GFOffset(du2,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(du2,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(du2,1,0,0) - 
      1.7178321571950627779478085413*GFOffset(du2,2,0,0) + 
      1.0798038112826304844454349794*GFOffset(du2,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(du2,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(du2,5,0,0) + 
      0.4465199214188158749748120629*GFOffset(du2,6,0,0),0) + IfThen(ti == 
      3,0.1217196331091731827490296503*GFOffset(du2,-3,0,0) + 
      1.2879607500639065480082640515*GFOffset(du2,-2,0,0) - 
      2.8344589120794203953271610371*GFOffset(du2,-1,0,0) + 
      2.8519159684628953883074916029*GFOffset(du2,1,0,0) - 
      1.3769648937605120893444713842*GFOffset(du2,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(du2,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(du2,4,0,0) - 
      0.3585985703546666387999883421*GFOffset(du2,5,0,0),0) + IfThen(ti == 
      4,-0.2734375*GFOffset(du2,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(du2,-3,0,0) + 
      1.2694130863581495324215740932*GFOffset(du2,-2,0,0) - 
      2.6593102175739179929283553282*GFOffset(du2,-1,0,0) + 
      2.6593102175739179929283553282*GFOffset(du2,1,0,0) - 
      1.2694130863581495324215740932*GFOffset(du2,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(du2,3,0,0) + 
      0.2734375*GFOffset(du2,4,0,0),0) + IfThen(ti == 
      5,0.3585985703546666387999883421*GFOffset(du2,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(du2,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(du2,-3,0,0) + 
      1.3769648937605120893444713842*GFOffset(du2,-2,0,0) - 
      2.8519159684628953883074916029*GFOffset(du2,-1,0,0) + 
      2.8344589120794203953271610371*GFOffset(du2,1,0,0) - 
      1.2879607500639065480082640515*GFOffset(du2,2,0,0) - 
      0.1217196331091731827490296503*GFOffset(du2,3,0,0),0) + IfThen(ti == 
      6,-0.4465199214188158749748120629*GFOffset(du2,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(du2,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(du2,-4,0,0) - 
      1.0798038112826304844454349794*GFOffset(du2,-3,0,0) + 
      1.7178321571950627779478085413*GFOffset(du2,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(du2,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(du2,1,0,0) - 
      0.3491845766773346366159606025*GFOffset(du2,2,0,0),0) + IfThen(ti == 
      7,0.6037268738428710466635005583*GFOffset(du2,-7,0,0) + 
      0.5557049812837167854026659932*GFOffset(du2,-6,0,0) - 
      0.816756381741385878117230629*GFOffset(du2,-5,0,0) + 
      1.1456537384551323190125010084*GFOffset(du2,-4,0,0) - 
      1.6652216450053851807954752347*GFOffset(du2,-3,0,0) + 
      2.6960654403140560289938204769*GFOffset(du2,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(du2,-1,0,0) + 
      3.2676328094883065529305901673*GFOffset(du2,1,0,0),0) + IfThen(ti == 
      8,-1.5*GFOffset(du2,-8,0,0) - 
      1.2848306326995883333410042531*GFOffset(du2,-7,0,0) + 
      1.8744408734469832436758584433*GFOffset(du2,-6,0,0) - 
      2.5907456765593549921859155848*GFOffset(du2,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(du2,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(du2,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(du2,-2,0,0) - 
      24.349745171593065826474565138*GFOffset(du2,-1,0,0) + 
      2.*GFOffset(du2,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,32.*GFOffset(du2,-1,0,0) + 4.*GFOffset(du2,9,0,0),0) + IfThen(ti == 
      1,1.6387617850907400720974393616*GFOffset(du2,-2,0,0) - 
      1.6387617850907400720974393616*GFOffset(du2,8,0,0),0) + IfThen(ti == 
      2,-1.2723510267943446305827338052*GFOffset(du2,-3,0,0) + 
      1.2723510267943446305827338052*GFOffset(du2,7,0,0),0) + IfThen(ti == 
      3,1.1326661647805276354083713666*GFOffset(du2,-4,0,0) - 
      1.1326661647805276354083713666*GFOffset(du2,6,0,0),0) + IfThen(ti == 
      4,-1.09375*GFOffset(du2,-5,0,0) + 1.09375*GFOffset(du2,5,0,0),0) + 
      IfThen(ti == 5,1.1326661647805276354083713666*GFOffset(du2,-6,0,0) - 
      1.1326661647805276354083713666*GFOffset(du2,4,0,0),0) + IfThen(ti == 
      6,-1.2723510267943446305827338052*GFOffset(du2,-7,0,0) + 
      1.2723510267943446305827338052*GFOffset(du2,3,0,0),0) + IfThen(ti == 
      7,1.6387617850907400720974393616*GFOffset(du2,-8,0,0) - 
      1.6387617850907400720974393616*GFOffset(du2,2,0,0),0) + IfThen(ti == 
      8,-4.*GFOffset(du2,-9,0,0) - 32.*GFOffset(du2,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDdu22 CCTK_ATTRIBUTE_UNUSED = 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-2.*GFOffset(du2,0,0,0) + 
      24.349745171593065826474565138*GFOffset(du2,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(du2,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(du2,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(du2,0,4,0) + 
      2.5907456765593549921859155848*GFOffset(du2,0,5,0) - 
      1.8744408734469832436758584433*GFOffset(du2,0,6,0) + 
      1.2848306326995883333410042531*GFOffset(du2,0,7,0) + 
      1.5*GFOffset(du2,0,8,0),0) + IfThen(tj == 
      1,-3.2676328094883065529305901673*GFOffset(du2,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(du2,0,1,0) - 
      2.6960654403140560289938204769*GFOffset(du2,0,2,0) + 
      1.6652216450053851807954752347*GFOffset(du2,0,3,0) - 
      1.1456537384551323190125010084*GFOffset(du2,0,4,0) + 
      0.816756381741385878117230629*GFOffset(du2,0,5,0) - 
      0.5557049812837167854026659932*GFOffset(du2,0,6,0) - 
      0.6037268738428710466635005583*GFOffset(du2,0,7,0),0) + IfThen(tj == 
      2,0.3491845766773346366159606025*GFOffset(du2,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(du2,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(du2,0,1,0) - 
      1.7178321571950627779478085413*GFOffset(du2,0,2,0) + 
      1.0798038112826304844454349794*GFOffset(du2,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(du2,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(du2,0,5,0) + 
      0.4465199214188158749748120629*GFOffset(du2,0,6,0),0) + IfThen(tj == 
      3,0.1217196331091731827490296503*GFOffset(du2,0,-3,0) + 
      1.2879607500639065480082640515*GFOffset(du2,0,-2,0) - 
      2.8344589120794203953271610371*GFOffset(du2,0,-1,0) + 
      2.8519159684628953883074916029*GFOffset(du2,0,1,0) - 
      1.3769648937605120893444713842*GFOffset(du2,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(du2,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(du2,0,4,0) - 
      0.3585985703546666387999883421*GFOffset(du2,0,5,0),0) + IfThen(tj == 
      4,-0.2734375*GFOffset(du2,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(du2,0,-3,0) + 
      1.2694130863581495324215740932*GFOffset(du2,0,-2,0) - 
      2.6593102175739179929283553282*GFOffset(du2,0,-1,0) + 
      2.6593102175739179929283553282*GFOffset(du2,0,1,0) - 
      1.2694130863581495324215740932*GFOffset(du2,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(du2,0,3,0) + 
      0.2734375*GFOffset(du2,0,4,0),0) + IfThen(tj == 
      5,0.3585985703546666387999883421*GFOffset(du2,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(du2,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(du2,0,-3,0) + 
      1.3769648937605120893444713842*GFOffset(du2,0,-2,0) - 
      2.8519159684628953883074916029*GFOffset(du2,0,-1,0) + 
      2.8344589120794203953271610371*GFOffset(du2,0,1,0) - 
      1.2879607500639065480082640515*GFOffset(du2,0,2,0) - 
      0.1217196331091731827490296503*GFOffset(du2,0,3,0),0) + IfThen(tj == 
      6,-0.4465199214188158749748120629*GFOffset(du2,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(du2,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(du2,0,-4,0) - 
      1.0798038112826304844454349794*GFOffset(du2,0,-3,0) + 
      1.7178321571950627779478085413*GFOffset(du2,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(du2,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(du2,0,1,0) - 
      0.3491845766773346366159606025*GFOffset(du2,0,2,0),0) + IfThen(tj == 
      7,0.6037268738428710466635005583*GFOffset(du2,0,-7,0) + 
      0.5557049812837167854026659932*GFOffset(du2,0,-6,0) - 
      0.816756381741385878117230629*GFOffset(du2,0,-5,0) + 
      1.1456537384551323190125010084*GFOffset(du2,0,-4,0) - 
      1.6652216450053851807954752347*GFOffset(du2,0,-3,0) + 
      2.6960654403140560289938204769*GFOffset(du2,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(du2,0,-1,0) + 
      3.2676328094883065529305901673*GFOffset(du2,0,1,0),0) + IfThen(tj == 
      8,-1.5*GFOffset(du2,0,-8,0) - 
      1.2848306326995883333410042531*GFOffset(du2,0,-7,0) + 
      1.8744408734469832436758584433*GFOffset(du2,0,-6,0) - 
      2.5907456765593549921859155848*GFOffset(du2,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(du2,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(du2,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(du2,0,-2,0) - 
      24.349745171593065826474565138*GFOffset(du2,0,-1,0) + 
      2.*GFOffset(du2,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,32.*GFOffset(du2,0,-1,0) + 4.*GFOffset(du2,0,9,0),0) + IfThen(tj == 
      1,1.6387617850907400720974393616*GFOffset(du2,0,-2,0) - 
      1.6387617850907400720974393616*GFOffset(du2,0,8,0),0) + IfThen(tj == 
      2,-1.2723510267943446305827338052*GFOffset(du2,0,-3,0) + 
      1.2723510267943446305827338052*GFOffset(du2,0,7,0),0) + IfThen(tj == 
      3,1.1326661647805276354083713666*GFOffset(du2,0,-4,0) - 
      1.1326661647805276354083713666*GFOffset(du2,0,6,0),0) + IfThen(tj == 
      4,-1.09375*GFOffset(du2,0,-5,0) + 1.09375*GFOffset(du2,0,5,0),0) + 
      IfThen(tj == 5,1.1326661647805276354083713666*GFOffset(du2,0,-6,0) - 
      1.1326661647805276354083713666*GFOffset(du2,0,4,0),0) + IfThen(tj == 
      6,-1.2723510267943446305827338052*GFOffset(du2,0,-7,0) + 
      1.2723510267943446305827338052*GFOffset(du2,0,3,0),0) + IfThen(tj == 
      7,1.6387617850907400720974393616*GFOffset(du2,0,-8,0) - 
      1.6387617850907400720974393616*GFOffset(du2,0,2,0),0) + IfThen(tj == 
      8,-4.*GFOffset(du2,0,-9,0) - 32.*GFOffset(du2,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDdu23 CCTK_ATTRIBUTE_UNUSED = 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-2.*GFOffset(du2,0,0,0) + 
      24.349745171593065826474565138*GFOffset(du2,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(du2,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(du2,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(du2,0,0,4) + 
      2.5907456765593549921859155848*GFOffset(du2,0,0,5) - 
      1.8744408734469832436758584433*GFOffset(du2,0,0,6) + 
      1.2848306326995883333410042531*GFOffset(du2,0,0,7) + 
      1.5*GFOffset(du2,0,0,8),0) + IfThen(tk == 
      1,-3.2676328094883065529305901673*GFOffset(du2,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(du2,0,0,1) - 
      2.6960654403140560289938204769*GFOffset(du2,0,0,2) + 
      1.6652216450053851807954752347*GFOffset(du2,0,0,3) - 
      1.1456537384551323190125010084*GFOffset(du2,0,0,4) + 
      0.816756381741385878117230629*GFOffset(du2,0,0,5) - 
      0.5557049812837167854026659932*GFOffset(du2,0,0,6) - 
      0.6037268738428710466635005583*GFOffset(du2,0,0,7),0) + IfThen(tk == 
      2,0.3491845766773346366159606025*GFOffset(du2,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(du2,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(du2,0,0,1) - 
      1.7178321571950627779478085413*GFOffset(du2,0,0,2) + 
      1.0798038112826304844454349794*GFOffset(du2,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(du2,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(du2,0,0,5) + 
      0.4465199214188158749748120629*GFOffset(du2,0,0,6),0) + IfThen(tk == 
      3,0.1217196331091731827490296503*GFOffset(du2,0,0,-3) + 
      1.2879607500639065480082640515*GFOffset(du2,0,0,-2) - 
      2.8344589120794203953271610371*GFOffset(du2,0,0,-1) + 
      2.8519159684628953883074916029*GFOffset(du2,0,0,1) - 
      1.3769648937605120893444713842*GFOffset(du2,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(du2,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(du2,0,0,4) - 
      0.3585985703546666387999883421*GFOffset(du2,0,0,5),0) + IfThen(tk == 
      4,-0.2734375*GFOffset(du2,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(du2,0,0,-3) + 
      1.2694130863581495324215740932*GFOffset(du2,0,0,-2) - 
      2.6593102175739179929283553282*GFOffset(du2,0,0,-1) + 
      2.6593102175739179929283553282*GFOffset(du2,0,0,1) - 
      1.2694130863581495324215740932*GFOffset(du2,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(du2,0,0,3) + 
      0.2734375*GFOffset(du2,0,0,4),0) + IfThen(tk == 
      5,0.3585985703546666387999883421*GFOffset(du2,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(du2,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(du2,0,0,-3) + 
      1.3769648937605120893444713842*GFOffset(du2,0,0,-2) - 
      2.8519159684628953883074916029*GFOffset(du2,0,0,-1) + 
      2.8344589120794203953271610371*GFOffset(du2,0,0,1) - 
      1.2879607500639065480082640515*GFOffset(du2,0,0,2) - 
      0.1217196331091731827490296503*GFOffset(du2,0,0,3),0) + IfThen(tk == 
      6,-0.4465199214188158749748120629*GFOffset(du2,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(du2,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(du2,0,0,-4) - 
      1.0798038112826304844454349794*GFOffset(du2,0,0,-3) + 
      1.7178321571950627779478085413*GFOffset(du2,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(du2,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(du2,0,0,1) - 
      0.3491845766773346366159606025*GFOffset(du2,0,0,2),0) + IfThen(tk == 
      7,0.6037268738428710466635005583*GFOffset(du2,0,0,-7) + 
      0.5557049812837167854026659932*GFOffset(du2,0,0,-6) - 
      0.816756381741385878117230629*GFOffset(du2,0,0,-5) + 
      1.1456537384551323190125010084*GFOffset(du2,0,0,-4) - 
      1.6652216450053851807954752347*GFOffset(du2,0,0,-3) + 
      2.6960654403140560289938204769*GFOffset(du2,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(du2,0,0,-1) + 
      3.2676328094883065529305901673*GFOffset(du2,0,0,1),0) + IfThen(tk == 
      8,-1.5*GFOffset(du2,0,0,-8) - 
      1.2848306326995883333410042531*GFOffset(du2,0,0,-7) + 
      1.8744408734469832436758584433*GFOffset(du2,0,0,-6) - 
      2.5907456765593549921859155848*GFOffset(du2,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(du2,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(du2,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(du2,0,0,-2) - 
      24.349745171593065826474565138*GFOffset(du2,0,0,-1) + 
      2.*GFOffset(du2,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,32.*GFOffset(du2,0,0,-1) + 4.*GFOffset(du2,0,0,9),0) + IfThen(tk == 
      1,1.6387617850907400720974393616*GFOffset(du2,0,0,-2) - 
      1.6387617850907400720974393616*GFOffset(du2,0,0,8),0) + IfThen(tk == 
      2,-1.2723510267943446305827338052*GFOffset(du2,0,0,-3) + 
      1.2723510267943446305827338052*GFOffset(du2,0,0,7),0) + IfThen(tk == 
      3,1.1326661647805276354083713666*GFOffset(du2,0,0,-4) - 
      1.1326661647805276354083713666*GFOffset(du2,0,0,6),0) + IfThen(tk == 
      4,-1.09375*GFOffset(du2,0,0,-5) + 1.09375*GFOffset(du2,0,0,5),0) + 
      IfThen(tk == 5,1.1326661647805276354083713666*GFOffset(du2,0,0,-6) - 
      1.1326661647805276354083713666*GFOffset(du2,0,0,4),0) + IfThen(tk == 
      6,-1.2723510267943446305827338052*GFOffset(du2,0,0,-7) + 
      1.2723510267943446305827338052*GFOffset(du2,0,0,3),0) + IfThen(tk == 
      7,1.6387617850907400720974393616*GFOffset(du2,0,0,-8) - 
      1.6387617850907400720974393616*GFOffset(du2,0,0,2),0) + IfThen(tk == 
      8,-4.*GFOffset(du2,0,0,-9) - 32.*GFOffset(du2,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDdu31 CCTK_ATTRIBUTE_UNUSED = 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-2.*GFOffset(du3,0,0,0) + 
      24.349745171593065826474565138*GFOffset(du3,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(du3,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(du3,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(du3,4,0,0) + 
      2.5907456765593549921859155848*GFOffset(du3,5,0,0) - 
      1.8744408734469832436758584433*GFOffset(du3,6,0,0) + 
      1.2848306326995883333410042531*GFOffset(du3,7,0,0) + 
      1.5*GFOffset(du3,8,0,0),0) + IfThen(ti == 
      1,-3.2676328094883065529305901673*GFOffset(du3,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(du3,1,0,0) - 
      2.6960654403140560289938204769*GFOffset(du3,2,0,0) + 
      1.6652216450053851807954752347*GFOffset(du3,3,0,0) - 
      1.1456537384551323190125010084*GFOffset(du3,4,0,0) + 
      0.816756381741385878117230629*GFOffset(du3,5,0,0) - 
      0.5557049812837167854026659932*GFOffset(du3,6,0,0) - 
      0.6037268738428710466635005583*GFOffset(du3,7,0,0),0) + IfThen(ti == 
      2,0.3491845766773346366159606025*GFOffset(du3,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(du3,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(du3,1,0,0) - 
      1.7178321571950627779478085413*GFOffset(du3,2,0,0) + 
      1.0798038112826304844454349794*GFOffset(du3,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(du3,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(du3,5,0,0) + 
      0.4465199214188158749748120629*GFOffset(du3,6,0,0),0) + IfThen(ti == 
      3,0.1217196331091731827490296503*GFOffset(du3,-3,0,0) + 
      1.2879607500639065480082640515*GFOffset(du3,-2,0,0) - 
      2.8344589120794203953271610371*GFOffset(du3,-1,0,0) + 
      2.8519159684628953883074916029*GFOffset(du3,1,0,0) - 
      1.3769648937605120893444713842*GFOffset(du3,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(du3,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(du3,4,0,0) - 
      0.3585985703546666387999883421*GFOffset(du3,5,0,0),0) + IfThen(ti == 
      4,-0.2734375*GFOffset(du3,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(du3,-3,0,0) + 
      1.2694130863581495324215740932*GFOffset(du3,-2,0,0) - 
      2.6593102175739179929283553282*GFOffset(du3,-1,0,0) + 
      2.6593102175739179929283553282*GFOffset(du3,1,0,0) - 
      1.2694130863581495324215740932*GFOffset(du3,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(du3,3,0,0) + 
      0.2734375*GFOffset(du3,4,0,0),0) + IfThen(ti == 
      5,0.3585985703546666387999883421*GFOffset(du3,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(du3,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(du3,-3,0,0) + 
      1.3769648937605120893444713842*GFOffset(du3,-2,0,0) - 
      2.8519159684628953883074916029*GFOffset(du3,-1,0,0) + 
      2.8344589120794203953271610371*GFOffset(du3,1,0,0) - 
      1.2879607500639065480082640515*GFOffset(du3,2,0,0) - 
      0.1217196331091731827490296503*GFOffset(du3,3,0,0),0) + IfThen(ti == 
      6,-0.4465199214188158749748120629*GFOffset(du3,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(du3,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(du3,-4,0,0) - 
      1.0798038112826304844454349794*GFOffset(du3,-3,0,0) + 
      1.7178321571950627779478085413*GFOffset(du3,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(du3,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(du3,1,0,0) - 
      0.3491845766773346366159606025*GFOffset(du3,2,0,0),0) + IfThen(ti == 
      7,0.6037268738428710466635005583*GFOffset(du3,-7,0,0) + 
      0.5557049812837167854026659932*GFOffset(du3,-6,0,0) - 
      0.816756381741385878117230629*GFOffset(du3,-5,0,0) + 
      1.1456537384551323190125010084*GFOffset(du3,-4,0,0) - 
      1.6652216450053851807954752347*GFOffset(du3,-3,0,0) + 
      2.6960654403140560289938204769*GFOffset(du3,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(du3,-1,0,0) + 
      3.2676328094883065529305901673*GFOffset(du3,1,0,0),0) + IfThen(ti == 
      8,-1.5*GFOffset(du3,-8,0,0) - 
      1.2848306326995883333410042531*GFOffset(du3,-7,0,0) + 
      1.8744408734469832436758584433*GFOffset(du3,-6,0,0) - 
      2.5907456765593549921859155848*GFOffset(du3,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(du3,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(du3,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(du3,-2,0,0) - 
      24.349745171593065826474565138*GFOffset(du3,-1,0,0) + 
      2.*GFOffset(du3,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,32.*GFOffset(du3,-1,0,0) + 4.*GFOffset(du3,9,0,0),0) + IfThen(ti == 
      1,1.6387617850907400720974393616*GFOffset(du3,-2,0,0) - 
      1.6387617850907400720974393616*GFOffset(du3,8,0,0),0) + IfThen(ti == 
      2,-1.2723510267943446305827338052*GFOffset(du3,-3,0,0) + 
      1.2723510267943446305827338052*GFOffset(du3,7,0,0),0) + IfThen(ti == 
      3,1.1326661647805276354083713666*GFOffset(du3,-4,0,0) - 
      1.1326661647805276354083713666*GFOffset(du3,6,0,0),0) + IfThen(ti == 
      4,-1.09375*GFOffset(du3,-5,0,0) + 1.09375*GFOffset(du3,5,0,0),0) + 
      IfThen(ti == 5,1.1326661647805276354083713666*GFOffset(du3,-6,0,0) - 
      1.1326661647805276354083713666*GFOffset(du3,4,0,0),0) + IfThen(ti == 
      6,-1.2723510267943446305827338052*GFOffset(du3,-7,0,0) + 
      1.2723510267943446305827338052*GFOffset(du3,3,0,0),0) + IfThen(ti == 
      7,1.6387617850907400720974393616*GFOffset(du3,-8,0,0) - 
      1.6387617850907400720974393616*GFOffset(du3,2,0,0),0) + IfThen(ti == 
      8,-4.*GFOffset(du3,-9,0,0) - 32.*GFOffset(du3,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDdu32 CCTK_ATTRIBUTE_UNUSED = 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-2.*GFOffset(du3,0,0,0) + 
      24.349745171593065826474565138*GFOffset(du3,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(du3,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(du3,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(du3,0,4,0) + 
      2.5907456765593549921859155848*GFOffset(du3,0,5,0) - 
      1.8744408734469832436758584433*GFOffset(du3,0,6,0) + 
      1.2848306326995883333410042531*GFOffset(du3,0,7,0) + 
      1.5*GFOffset(du3,0,8,0),0) + IfThen(tj == 
      1,-3.2676328094883065529305901673*GFOffset(du3,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(du3,0,1,0) - 
      2.6960654403140560289938204769*GFOffset(du3,0,2,0) + 
      1.6652216450053851807954752347*GFOffset(du3,0,3,0) - 
      1.1456537384551323190125010084*GFOffset(du3,0,4,0) + 
      0.816756381741385878117230629*GFOffset(du3,0,5,0) - 
      0.5557049812837167854026659932*GFOffset(du3,0,6,0) - 
      0.6037268738428710466635005583*GFOffset(du3,0,7,0),0) + IfThen(tj == 
      2,0.3491845766773346366159606025*GFOffset(du3,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(du3,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(du3,0,1,0) - 
      1.7178321571950627779478085413*GFOffset(du3,0,2,0) + 
      1.0798038112826304844454349794*GFOffset(du3,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(du3,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(du3,0,5,0) + 
      0.4465199214188158749748120629*GFOffset(du3,0,6,0),0) + IfThen(tj == 
      3,0.1217196331091731827490296503*GFOffset(du3,0,-3,0) + 
      1.2879607500639065480082640515*GFOffset(du3,0,-2,0) - 
      2.8344589120794203953271610371*GFOffset(du3,0,-1,0) + 
      2.8519159684628953883074916029*GFOffset(du3,0,1,0) - 
      1.3769648937605120893444713842*GFOffset(du3,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(du3,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(du3,0,4,0) - 
      0.3585985703546666387999883421*GFOffset(du3,0,5,0),0) + IfThen(tj == 
      4,-0.2734375*GFOffset(du3,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(du3,0,-3,0) + 
      1.2694130863581495324215740932*GFOffset(du3,0,-2,0) - 
      2.6593102175739179929283553282*GFOffset(du3,0,-1,0) + 
      2.6593102175739179929283553282*GFOffset(du3,0,1,0) - 
      1.2694130863581495324215740932*GFOffset(du3,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(du3,0,3,0) + 
      0.2734375*GFOffset(du3,0,4,0),0) + IfThen(tj == 
      5,0.3585985703546666387999883421*GFOffset(du3,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(du3,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(du3,0,-3,0) + 
      1.3769648937605120893444713842*GFOffset(du3,0,-2,0) - 
      2.8519159684628953883074916029*GFOffset(du3,0,-1,0) + 
      2.8344589120794203953271610371*GFOffset(du3,0,1,0) - 
      1.2879607500639065480082640515*GFOffset(du3,0,2,0) - 
      0.1217196331091731827490296503*GFOffset(du3,0,3,0),0) + IfThen(tj == 
      6,-0.4465199214188158749748120629*GFOffset(du3,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(du3,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(du3,0,-4,0) - 
      1.0798038112826304844454349794*GFOffset(du3,0,-3,0) + 
      1.7178321571950627779478085413*GFOffset(du3,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(du3,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(du3,0,1,0) - 
      0.3491845766773346366159606025*GFOffset(du3,0,2,0),0) + IfThen(tj == 
      7,0.6037268738428710466635005583*GFOffset(du3,0,-7,0) + 
      0.5557049812837167854026659932*GFOffset(du3,0,-6,0) - 
      0.816756381741385878117230629*GFOffset(du3,0,-5,0) + 
      1.1456537384551323190125010084*GFOffset(du3,0,-4,0) - 
      1.6652216450053851807954752347*GFOffset(du3,0,-3,0) + 
      2.6960654403140560289938204769*GFOffset(du3,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(du3,0,-1,0) + 
      3.2676328094883065529305901673*GFOffset(du3,0,1,0),0) + IfThen(tj == 
      8,-1.5*GFOffset(du3,0,-8,0) - 
      1.2848306326995883333410042531*GFOffset(du3,0,-7,0) + 
      1.8744408734469832436758584433*GFOffset(du3,0,-6,0) - 
      2.5907456765593549921859155848*GFOffset(du3,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(du3,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(du3,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(du3,0,-2,0) - 
      24.349745171593065826474565138*GFOffset(du3,0,-1,0) + 
      2.*GFOffset(du3,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,32.*GFOffset(du3,0,-1,0) + 4.*GFOffset(du3,0,9,0),0) + IfThen(tj == 
      1,1.6387617850907400720974393616*GFOffset(du3,0,-2,0) - 
      1.6387617850907400720974393616*GFOffset(du3,0,8,0),0) + IfThen(tj == 
      2,-1.2723510267943446305827338052*GFOffset(du3,0,-3,0) + 
      1.2723510267943446305827338052*GFOffset(du3,0,7,0),0) + IfThen(tj == 
      3,1.1326661647805276354083713666*GFOffset(du3,0,-4,0) - 
      1.1326661647805276354083713666*GFOffset(du3,0,6,0),0) + IfThen(tj == 
      4,-1.09375*GFOffset(du3,0,-5,0) + 1.09375*GFOffset(du3,0,5,0),0) + 
      IfThen(tj == 5,1.1326661647805276354083713666*GFOffset(du3,0,-6,0) - 
      1.1326661647805276354083713666*GFOffset(du3,0,4,0),0) + IfThen(tj == 
      6,-1.2723510267943446305827338052*GFOffset(du3,0,-7,0) + 
      1.2723510267943446305827338052*GFOffset(du3,0,3,0),0) + IfThen(tj == 
      7,1.6387617850907400720974393616*GFOffset(du3,0,-8,0) - 
      1.6387617850907400720974393616*GFOffset(du3,0,2,0),0) + IfThen(tj == 
      8,-4.*GFOffset(du3,0,-9,0) - 32.*GFOffset(du3,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDdu33 CCTK_ATTRIBUTE_UNUSED = 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-2.*GFOffset(du3,0,0,0) + 
      24.349745171593065826474565138*GFOffset(du3,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(du3,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(du3,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(du3,0,0,4) + 
      2.5907456765593549921859155848*GFOffset(du3,0,0,5) - 
      1.8744408734469832436758584433*GFOffset(du3,0,0,6) + 
      1.2848306326995883333410042531*GFOffset(du3,0,0,7) + 
      1.5*GFOffset(du3,0,0,8),0) + IfThen(tk == 
      1,-3.2676328094883065529305901673*GFOffset(du3,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(du3,0,0,1) - 
      2.6960654403140560289938204769*GFOffset(du3,0,0,2) + 
      1.6652216450053851807954752347*GFOffset(du3,0,0,3) - 
      1.1456537384551323190125010084*GFOffset(du3,0,0,4) + 
      0.816756381741385878117230629*GFOffset(du3,0,0,5) - 
      0.5557049812837167854026659932*GFOffset(du3,0,0,6) - 
      0.6037268738428710466635005583*GFOffset(du3,0,0,7),0) + IfThen(tk == 
      2,0.3491845766773346366159606025*GFOffset(du3,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(du3,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(du3,0,0,1) - 
      1.7178321571950627779478085413*GFOffset(du3,0,0,2) + 
      1.0798038112826304844454349794*GFOffset(du3,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(du3,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(du3,0,0,5) + 
      0.4465199214188158749748120629*GFOffset(du3,0,0,6),0) + IfThen(tk == 
      3,0.1217196331091731827490296503*GFOffset(du3,0,0,-3) + 
      1.2879607500639065480082640515*GFOffset(du3,0,0,-2) - 
      2.8344589120794203953271610371*GFOffset(du3,0,0,-1) + 
      2.8519159684628953883074916029*GFOffset(du3,0,0,1) - 
      1.3769648937605120893444713842*GFOffset(du3,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(du3,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(du3,0,0,4) - 
      0.3585985703546666387999883421*GFOffset(du3,0,0,5),0) + IfThen(tk == 
      4,-0.2734375*GFOffset(du3,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(du3,0,0,-3) + 
      1.2694130863581495324215740932*GFOffset(du3,0,0,-2) - 
      2.6593102175739179929283553282*GFOffset(du3,0,0,-1) + 
      2.6593102175739179929283553282*GFOffset(du3,0,0,1) - 
      1.2694130863581495324215740932*GFOffset(du3,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(du3,0,0,3) + 
      0.2734375*GFOffset(du3,0,0,4),0) + IfThen(tk == 
      5,0.3585985703546666387999883421*GFOffset(du3,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(du3,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(du3,0,0,-3) + 
      1.3769648937605120893444713842*GFOffset(du3,0,0,-2) - 
      2.8519159684628953883074916029*GFOffset(du3,0,0,-1) + 
      2.8344589120794203953271610371*GFOffset(du3,0,0,1) - 
      1.2879607500639065480082640515*GFOffset(du3,0,0,2) - 
      0.1217196331091731827490296503*GFOffset(du3,0,0,3),0) + IfThen(tk == 
      6,-0.4465199214188158749748120629*GFOffset(du3,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(du3,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(du3,0,0,-4) - 
      1.0798038112826304844454349794*GFOffset(du3,0,0,-3) + 
      1.7178321571950627779478085413*GFOffset(du3,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(du3,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(du3,0,0,1) - 
      0.3491845766773346366159606025*GFOffset(du3,0,0,2),0) + IfThen(tk == 
      7,0.6037268738428710466635005583*GFOffset(du3,0,0,-7) + 
      0.5557049812837167854026659932*GFOffset(du3,0,0,-6) - 
      0.816756381741385878117230629*GFOffset(du3,0,0,-5) + 
      1.1456537384551323190125010084*GFOffset(du3,0,0,-4) - 
      1.6652216450053851807954752347*GFOffset(du3,0,0,-3) + 
      2.6960654403140560289938204769*GFOffset(du3,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(du3,0,0,-1) + 
      3.2676328094883065529305901673*GFOffset(du3,0,0,1),0) + IfThen(tk == 
      8,-1.5*GFOffset(du3,0,0,-8) - 
      1.2848306326995883333410042531*GFOffset(du3,0,0,-7) + 
      1.8744408734469832436758584433*GFOffset(du3,0,0,-6) - 
      2.5907456765593549921859155848*GFOffset(du3,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(du3,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(du3,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(du3,0,0,-2) - 
      24.349745171593065826474565138*GFOffset(du3,0,0,-1) + 
      2.*GFOffset(du3,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,32.*GFOffset(du3,0,0,-1) + 4.*GFOffset(du3,0,0,9),0) + IfThen(tk == 
      1,1.6387617850907400720974393616*GFOffset(du3,0,0,-2) - 
      1.6387617850907400720974393616*GFOffset(du3,0,0,8),0) + IfThen(tk == 
      2,-1.2723510267943446305827338052*GFOffset(du3,0,0,-3) + 
      1.2723510267943446305827338052*GFOffset(du3,0,0,7),0) + IfThen(tk == 
      3,1.1326661647805276354083713666*GFOffset(du3,0,0,-4) - 
      1.1326661647805276354083713666*GFOffset(du3,0,0,6),0) + IfThen(tk == 
      4,-1.09375*GFOffset(du3,0,0,-5) + 1.09375*GFOffset(du3,0,0,5),0) + 
      IfThen(tk == 5,1.1326661647805276354083713666*GFOffset(du3,0,0,-6) - 
      1.1326661647805276354083713666*GFOffset(du3,0,0,4),0) + IfThen(tk == 
      6,-1.2723510267943446305827338052*GFOffset(du3,0,0,-7) + 
      1.2723510267943446305827338052*GFOffset(du3,0,0,3),0) + IfThen(tk == 
      7,1.6387617850907400720974393616*GFOffset(du3,0,0,-8) - 
      1.6387617850907400720974393616*GFOffset(du3,0,0,2),0) + IfThen(tk == 
      8,-4.*GFOffset(du3,0,0,-9) - 32.*GFOffset(du3,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDdu11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDdu22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDdu33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDdu11 = J11L*LDdu11 + J21L*LDdu12 + J31L*LDdu13;
      
      PDdu22 = J12L*LDdu21 + J22L*LDdu22 + J32L*LDdu23;
      
      PDdu33 = J13L*LDdu31 + J23L*LDdu32 + J33L*LDdu33;
    }
    else
    {
      PDdu11 = LDdu11;
      
      PDdu22 = LDdu22;
      
      PDdu33 = LDdu33;
    }
    
    CCTK_REAL urhsL CCTK_ATTRIBUTE_UNUSED = rhoL + 
      IfThen(usejacobian,myDetJL*(epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(u,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(u,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(u,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(u,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(u,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(u,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(u,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(u,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(u,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(u,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(u,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(u,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(u,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(u,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(u,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(u,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(u,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(u,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(u,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(u,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(u,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(u,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(u,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(u,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(u,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(u,5,0,0),0) + IfThen(ti == 
      4,-0.0303817292054494222005208333333*GFOffset(u,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(u,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(u,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(u,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(u,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(u,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(u,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(u,4,0,0),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(u,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(u,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(u,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(u,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(u,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(u,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(u,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(u,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(u,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(u,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(u,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(u,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(u,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(u,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(u,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(u,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(u,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(u,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(u,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(u,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(u,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(u,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(u,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(u,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(u,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(u,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(u,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(u,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(u,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(u,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(u,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(u,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(u,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(u,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(u,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(u,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(u,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(u,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(u,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(u,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(u,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(u,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(u,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(u,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(u,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(u,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(u,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(u,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(u,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(u,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(u,0,5,0),0) + IfThen(tj == 
      4,-0.0303817292054494222005208333333*GFOffset(u,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(u,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(u,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(u,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(u,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(u,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(u,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(u,0,4,0),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(u,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(u,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(u,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(u,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(u,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(u,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(u,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(u,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(u,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(u,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(u,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(u,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(u,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(u,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(u,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(u,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(u,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(u,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(u,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(u,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(u,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(u,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(u,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(u,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(u,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(u,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(u,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(u,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(u,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(u,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(u,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(u,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(u,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(u,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(u,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(u,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(u,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(u,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(u,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(u,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(u,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(u,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(u,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(u,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(u,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(u,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(u,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(u,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(u,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(u,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(u,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(u,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(u,0,0,5),0) + IfThen(tk == 
      4,-0.0303817292054494222005208333333*GFOffset(u,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(u,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(u,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(u,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(u,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(u,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(u,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(u,0,0,4),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(u,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(u,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(u,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(u,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(u,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(u,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(u,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(u,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(u,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(u,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(u,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(u,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(u,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(u,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(u,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(u,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(u,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(u,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(u,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(u,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(u,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(u,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(u,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(u,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(u,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(u,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(u,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(u,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(u,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dz,-1)),epsDiss*(IfThen(ti 
      == 0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(u,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(u,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(u,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(u,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(u,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(u,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(u,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(u,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(u,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(u,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(u,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(u,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(u,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(u,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(u,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(u,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(u,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(u,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(u,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(u,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(u,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(u,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(u,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(u,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(u,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(u,5,0,0),0) + IfThen(ti == 
      4,-0.0303817292054494222005208333333*GFOffset(u,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(u,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(u,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(u,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(u,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(u,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(u,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(u,4,0,0),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(u,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(u,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(u,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(u,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(u,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(u,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(u,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(u,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(u,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(u,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(u,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(u,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(u,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(u,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(u,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(u,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(u,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(u,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(u,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(u,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(u,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(u,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(u,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(u,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(u,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(u,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(u,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(u,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(u,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(u,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(u,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(u,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(u,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(u,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(u,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(u,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(u,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(u,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(u,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(u,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(u,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(u,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(u,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(u,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(u,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(u,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(u,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(u,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(u,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(u,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(u,0,5,0),0) + IfThen(tj == 
      4,-0.0303817292054494222005208333333*GFOffset(u,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(u,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(u,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(u,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(u,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(u,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(u,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(u,0,4,0),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(u,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(u,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(u,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(u,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(u,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(u,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(u,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(u,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(u,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(u,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(u,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(u,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(u,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(u,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(u,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(u,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(u,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(u,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(u,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(u,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(u,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(u,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(u,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(u,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(u,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(u,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(u,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(u,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(u,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(u,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(u,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(u,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(u,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(u,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(u,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(u,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(u,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(u,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(u,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(u,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(u,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(u,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(u,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(u,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(u,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(u,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(u,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(u,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(u,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(u,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(u,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(u,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(u,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(u,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(u,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(u,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(u,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(u,0,0,5),0) + IfThen(tk == 
      4,-0.0303817292054494222005208333333*GFOffset(u,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(u,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(u,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(u,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(u,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(u,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(u,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(u,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(u,0,0,4),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(u,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(u,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(u,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(u,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(u,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(u,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(u,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(u,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(u,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(u,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(u,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(u,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(u,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(u,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(u,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(u,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(u,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(u,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(u,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(u,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(u,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(u,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(u,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(u,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(u,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(u,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(u,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(u,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(u,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(u,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(u,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(u,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(u,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(u,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(u,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(u,0,0,0),0))*pow(dz,-1));
    
    CCTK_REAL rhorhsL CCTK_ATTRIBUTE_UNUSED = PDdu11 + PDdu22 + PDdu33 + 
      IfThen(usejacobian,myDetJL*(epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(rho,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(rho,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(rho,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(rho,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(rho,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(rho,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(rho,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(rho,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(rho,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(rho,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(rho,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(rho,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(rho,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(rho,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(rho,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(rho,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(rho,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(rho,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(rho,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(rho,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(rho,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(rho,5,0,0),0) + IfThen(ti == 
      4,-0.0303817292054494222005208333333*GFOffset(rho,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(rho,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(rho,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(rho,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(rho,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(rho,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(rho,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(rho,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(rho,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(rho,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(rho,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(rho,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(rho,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(rho,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(rho,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(rho,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(rho,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(rho,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(rho,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(rho,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(rho,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(rho,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(rho,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(rho,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(rho,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(rho,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(rho,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(rho,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(rho,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(rho,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(rho,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(rho,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(rho,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(rho,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(rho,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(rho,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(rho,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(rho,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(rho,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(rho,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(rho,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(rho,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(rho,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(rho,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(rho,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(rho,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(rho,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(rho,0,5,0),0) + IfThen(tj == 
      4,-0.0303817292054494222005208333333*GFOffset(rho,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(rho,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(rho,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(rho,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(rho,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(rho,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(rho,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(rho,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(rho,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(rho,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(rho,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(rho,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(rho,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(rho,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(rho,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(rho,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(rho,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(rho,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(rho,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(rho,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(rho,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(rho,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(rho,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(rho,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(rho,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(rho,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(rho,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(rho,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(rho,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(rho,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(rho,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(rho,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(rho,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(rho,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(rho,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(rho,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(rho,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(rho,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(rho,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(rho,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(rho,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(rho,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(rho,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(rho,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(rho,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(rho,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(rho,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(rho,0,0,5),0) + IfThen(tk == 
      4,-0.0303817292054494222005208333333*GFOffset(rho,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(rho,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(rho,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(rho,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(rho,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(rho,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(rho,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(rho,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(rho,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(rho,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(rho,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(rho,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(rho,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(rho,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(rho,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(rho,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(rho,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(rho,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(rho,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(rho,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(rho,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(rho,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(rho,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(rho,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(rho,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(rho,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(rho,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(rho,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(rho,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(rho,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dz,-1)),epsDiss*(IfThen(ti 
      == 0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(rho,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(rho,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(rho,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(rho,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(rho,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(rho,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(rho,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(rho,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(rho,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(rho,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(rho,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(rho,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(rho,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(rho,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(rho,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(rho,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(rho,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(rho,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(rho,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(rho,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(rho,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(rho,5,0,0),0) + IfThen(ti == 
      4,-0.0303817292054494222005208333333*GFOffset(rho,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(rho,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(rho,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(rho,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(rho,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(rho,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(rho,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(rho,4,0,0),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(rho,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(rho,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(rho,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(rho,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(rho,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(rho,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(rho,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(rho,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(rho,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(rho,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(rho,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(rho,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(rho,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(rho,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(rho,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(rho,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(rho,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(rho,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(rho,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(rho,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(rho,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(rho,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(rho,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(rho,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(rho,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(rho,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(rho,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(rho,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(rho,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(rho,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(rho,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(rho,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(rho,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(rho,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(rho,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(rho,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(rho,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(rho,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(rho,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(rho,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(rho,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(rho,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(rho,0,5,0),0) + IfThen(tj == 
      4,-0.0303817292054494222005208333333*GFOffset(rho,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(rho,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(rho,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(rho,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(rho,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(rho,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(rho,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(rho,0,4,0),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(rho,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(rho,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(rho,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(rho,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(rho,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(rho,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(rho,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(rho,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(rho,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(rho,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(rho,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(rho,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(rho,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(rho,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(rho,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(rho,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(rho,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(rho,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(rho,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(rho,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(rho,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(rho,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(rho,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(rho,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(rho,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(rho,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(rho,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(rho,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(rho,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(rho,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(rho,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(rho,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(rho,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(rho,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(rho,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(rho,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(rho,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(rho,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(rho,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(rho,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(rho,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(rho,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(rho,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(rho,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(rho,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(rho,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(rho,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(rho,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(rho,0,0,5),0) + IfThen(tk == 
      4,-0.0303817292054494222005208333333*GFOffset(rho,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(rho,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(rho,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(rho,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(rho,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(rho,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(rho,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(rho,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(rho,0,0,4),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(rho,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(rho,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(rho,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(rho,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(rho,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(rho,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(rho,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(rho,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(rho,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(rho,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(rho,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(rho,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(rho,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(rho,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(rho,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(rho,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(rho,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(rho,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(rho,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(rho,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(rho,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(rho,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(rho,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(rho,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(rho,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(rho,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(rho,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(rho,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(rho,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(rho,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(rho,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(rho,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(rho,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(rho,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(rho,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(rho,0,0,0),0))*pow(dz,-1));
    
    CCTK_REAL v1rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL v2rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    
    CCTK_REAL v3rhsL CCTK_ATTRIBUTE_UNUSED = 0;
    /* Copy local copies back to grid functions */
    rhorhs[index] = rhorhsL;
    urhs[index] = urhsL;
    v1rhs[index] = v1rhsL;
    v2rhs[index] = v2rhsL;
    v3rhs[index] = v3rhsL;
  }
  CCTK_ENDLOOP3(ML_WaveToy_DG8_RHSSecondOrder);
}
extern "C" void ML_WaveToy_DG8_RHSSecondOrder(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_WaveToy_DG8_RHSSecondOrder_Body");
  }
  if (cctk_iteration % ML_WaveToy_DG8_RHSSecondOrder_calc_every != ML_WaveToy_DG8_RHSSecondOrder_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_WaveToy_DG8::WT_detJ",
    "ML_WaveToy_DG8::WT_du",
    "ML_WaveToy_DG8::WT_rho",
    "ML_WaveToy_DG8::WT_rhorhs",
    "ML_WaveToy_DG8::WT_u",
    "ML_WaveToy_DG8::WT_urhs",
    "ML_WaveToy_DG8::WT_vrhs"};
  AssertGroupStorage(cctkGH, "ML_WaveToy_DG8_RHSSecondOrder", 7, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_WaveToy_DG8_RHSSecondOrder_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_WaveToy_DG8_RHSSecondOrder_Body");
  }
}

} // namespace ML_WaveToy_DG8
