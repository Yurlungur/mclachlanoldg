#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

extern "C"
void ML_BSSN_DG8_SetupMask(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_REAL *restrict const reduce_weight =
    static_cast<CCTK_REAL*>(CCTK_VarDataPtr(cctkGH, 0, "CarpetReduce::weight"));
  if (!reduce_weight) {
    CCTK_ERROR("Grid function \"CarpetReduce::weight\" does not have storage");
  }
  
#pragma omp parallel
  CCTK_LOOP3_ALL(ML_BSSN_DG8_SetupMask2, cctkGH, i,j,k) {
    int const ind = CCTK_GFINDEX3D(cctkGH, i,j,k);
    reduce_weight[ind] *= weight[ind];
  } CCTK_ENDLOOP3_ALL(ML_BSSN_DG8_SetupMask2);
}
