#include <Slicing.h>

#include <cctk.h>

extern "C"
int ML_BSSN_DG8_RegisterSlicing(void)
{
  Einstein_RegisterSlicing("ML_BSSN_DG8");
  return 0;
}
