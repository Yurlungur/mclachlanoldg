#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cassert>

extern "C"
void ML_BSSN_DG8_DerivativesSelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int ierr;
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_BSSN_DG8::ML_dconfac", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_BSSN_DG8::ML_dmetric", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_BSSN_DG8::ML_dlapse", "none");
  assert(!ierr);
  ierr = Boundary_SelectGroupForBC
    (cctkGH, CCTK_ALL_FACES, 1, -1, "ML_BSSN_DG8::ML_dshift", "none");
  assert(!ierr);
}
