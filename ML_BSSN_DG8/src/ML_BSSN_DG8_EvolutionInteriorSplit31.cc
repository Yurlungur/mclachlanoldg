/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_BSSN_DG8 {

extern "C" void ML_BSSN_DG8_EvolutionInteriorSplit31_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_DG8_EvolutionInteriorSplit31_calc_every != ML_BSSN_DG8_EvolutionInteriorSplit31_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG8::ML_confacrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG8::ML_confacrhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG8::ML_dtlapserhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG8::ML_dtlapserhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG8::ML_lapserhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG8::ML_lapserhs.");
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG8::ML_metricrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG8::ML_metricrhs.");
  return;
}

static void ML_BSSN_DG8_EvolutionInteriorSplit31_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_BSSN_DG8_EvolutionInteriorSplit31,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL AL CCTK_ATTRIBUTE_UNUSED = A[index];
    CCTK_REAL alphaL CCTK_ATTRIBUTE_UNUSED = alpha[index];
    CCTK_REAL At11L CCTK_ATTRIBUTE_UNUSED = At11[index];
    CCTK_REAL At12L CCTK_ATTRIBUTE_UNUSED = At12[index];
    CCTK_REAL At13L CCTK_ATTRIBUTE_UNUSED = At13[index];
    CCTK_REAL At22L CCTK_ATTRIBUTE_UNUSED = At22[index];
    CCTK_REAL At23L CCTK_ATTRIBUTE_UNUSED = At23[index];
    CCTK_REAL At33L CCTK_ATTRIBUTE_UNUSED = At33[index];
    CCTK_REAL beta1L CCTK_ATTRIBUTE_UNUSED = beta1[index];
    CCTK_REAL beta2L CCTK_ATTRIBUTE_UNUSED = beta2[index];
    CCTK_REAL beta3L CCTK_ATTRIBUTE_UNUSED = beta3[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL myDetJL CCTK_ATTRIBUTE_UNUSED = myDetJ[index];
    CCTK_REAL PDalpha1L CCTK_ATTRIBUTE_UNUSED = PDalpha1[index];
    CCTK_REAL PDalpha2L CCTK_ATTRIBUTE_UNUSED = PDalpha2[index];
    CCTK_REAL PDalpha3L CCTK_ATTRIBUTE_UNUSED = PDalpha3[index];
    CCTK_REAL PDbeta11L CCTK_ATTRIBUTE_UNUSED = PDbeta11[index];
    CCTK_REAL PDbeta12L CCTK_ATTRIBUTE_UNUSED = PDbeta12[index];
    CCTK_REAL PDbeta13L CCTK_ATTRIBUTE_UNUSED = PDbeta13[index];
    CCTK_REAL PDbeta21L CCTK_ATTRIBUTE_UNUSED = PDbeta21[index];
    CCTK_REAL PDbeta22L CCTK_ATTRIBUTE_UNUSED = PDbeta22[index];
    CCTK_REAL PDbeta23L CCTK_ATTRIBUTE_UNUSED = PDbeta23[index];
    CCTK_REAL PDbeta31L CCTK_ATTRIBUTE_UNUSED = PDbeta31[index];
    CCTK_REAL PDbeta32L CCTK_ATTRIBUTE_UNUSED = PDbeta32[index];
    CCTK_REAL PDbeta33L CCTK_ATTRIBUTE_UNUSED = PDbeta33[index];
    CCTK_REAL PDgt111L CCTK_ATTRIBUTE_UNUSED = PDgt111[index];
    CCTK_REAL PDgt112L CCTK_ATTRIBUTE_UNUSED = PDgt112[index];
    CCTK_REAL PDgt113L CCTK_ATTRIBUTE_UNUSED = PDgt113[index];
    CCTK_REAL PDgt121L CCTK_ATTRIBUTE_UNUSED = PDgt121[index];
    CCTK_REAL PDgt122L CCTK_ATTRIBUTE_UNUSED = PDgt122[index];
    CCTK_REAL PDgt123L CCTK_ATTRIBUTE_UNUSED = PDgt123[index];
    CCTK_REAL PDgt131L CCTK_ATTRIBUTE_UNUSED = PDgt131[index];
    CCTK_REAL PDgt132L CCTK_ATTRIBUTE_UNUSED = PDgt132[index];
    CCTK_REAL PDgt133L CCTK_ATTRIBUTE_UNUSED = PDgt133[index];
    CCTK_REAL PDgt221L CCTK_ATTRIBUTE_UNUSED = PDgt221[index];
    CCTK_REAL PDgt222L CCTK_ATTRIBUTE_UNUSED = PDgt222[index];
    CCTK_REAL PDgt223L CCTK_ATTRIBUTE_UNUSED = PDgt223[index];
    CCTK_REAL PDgt231L CCTK_ATTRIBUTE_UNUSED = PDgt231[index];
    CCTK_REAL PDgt232L CCTK_ATTRIBUTE_UNUSED = PDgt232[index];
    CCTK_REAL PDgt233L CCTK_ATTRIBUTE_UNUSED = PDgt233[index];
    CCTK_REAL PDgt331L CCTK_ATTRIBUTE_UNUSED = PDgt331[index];
    CCTK_REAL PDgt332L CCTK_ATTRIBUTE_UNUSED = PDgt332[index];
    CCTK_REAL PDgt333L CCTK_ATTRIBUTE_UNUSED = PDgt333[index];
    CCTK_REAL PDphiW1L CCTK_ATTRIBUTE_UNUSED = PDphiW1[index];
    CCTK_REAL PDphiW2L CCTK_ATTRIBUTE_UNUSED = PDphiW2[index];
    CCTK_REAL PDphiW3L CCTK_ATTRIBUTE_UNUSED = PDphiW3[index];
    CCTK_REAL phiWL CCTK_ATTRIBUTE_UNUSED = phiW[index];
    CCTK_REAL trKL CCTK_ATTRIBUTE_UNUSED = trK[index];
    
    CCTK_REAL eTttL, eTtxL, eTtyL, eTtzL, eTxxL, eTxyL, eTxzL, eTyyL, eTyzL, eTzzL CCTK_ATTRIBUTE_UNUSED ;
    
    if (assume_stress_energy_state>=0 ? assume_stress_energy_state : *stress_energy_state)
    {
      eTttL = eTtt[index];
      eTtxL = eTtx[index];
      eTtyL = eTty[index];
      eTtzL = eTtz[index];
      eTxxL = eTxx[index];
      eTxyL = eTxy[index];
      eTxzL = eTxz[index];
      eTyyL = eTyy[index];
      eTyzL = eTyz[index];
      eTzzL = eTzz[index];
    }
    else
    {
      eTttL = 0.;
      eTtxL = 0.;
      eTtyL = 0.;
      eTtzL = 0.;
      eTxxL = 0.;
      eTxyL = 0.;
      eTxzL = 0.;
      eTyyL = 0.;
      eTyzL = 0.;
      eTzzL = 0.;
    }
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDPDalpha11 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDalpha1,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDalpha1,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDalpha1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha1,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha1,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha1,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha1,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha1,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha1,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha1,7,0,0) - 
      0.5*GFOffset(PDalpha1,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha1,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha1,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha1,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha1,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha1,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha1,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha1,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha1,7,0,0),0) + 
      IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha1,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha1,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha1,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha1,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha1,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha1,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha1,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha1,6,0,0),0) + 
      IfThen(ti == 3,-0.444613449281090634955156033*GFOffset(PDalpha1,-3,0,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha1,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha1,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha1,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha1,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha1,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha1,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha1,5,0,0),0) + 
      IfThen(ti == 4,0.2734375*GFOffset(PDalpha1,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha1,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha1,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha1,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha1,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha1,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha1,3,0,0) - 
      0.2734375*GFOffset(PDalpha1,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha1,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha1,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha1,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha1,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha1,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha1,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha1,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha1,3,0,0),0) + IfThen(ti 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha1,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha1,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha1,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha1,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha1,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha1,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha1,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha1,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha1,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha1,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha1,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha1,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha1,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha1,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha1,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha1,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDalpha1,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha1,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha1,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha1,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha1,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha1,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha1,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha1,-1,0,0) + 
      18.*GFOffset(PDalpha1,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDalpha1,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDalpha1,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha12 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDalpha1,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDalpha1,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDalpha1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha1,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha1,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha1,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha1,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha1,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha1,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha1,0,7,0) - 
      0.5*GFOffset(PDalpha1,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha1,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha1,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha1,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha1,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha1,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha1,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha1,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha1,0,7,0),0) + 
      IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha1,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha1,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha1,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha1,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha1,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha1,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha1,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha1,0,6,0),0) + 
      IfThen(tj == 3,-0.444613449281090634955156033*GFOffset(PDalpha1,0,-3,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha1,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha1,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha1,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha1,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha1,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha1,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha1,0,5,0),0) + 
      IfThen(tj == 4,0.2734375*GFOffset(PDalpha1,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha1,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha1,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha1,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha1,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha1,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha1,0,3,0) - 
      0.2734375*GFOffset(PDalpha1,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha1,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha1,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha1,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha1,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha1,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha1,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha1,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha1,0,3,0),0) + IfThen(tj 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha1,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha1,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha1,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha1,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha1,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha1,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha1,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha1,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha1,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha1,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha1,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha1,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha1,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha1,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha1,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha1,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDalpha1,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha1,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha1,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha1,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha1,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha1,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha1,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha1,0,-1,0) + 
      18.*GFOffset(PDalpha1,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDalpha1,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDalpha1,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha13 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDalpha1,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDalpha1,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDalpha1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha1,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDalpha1,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDalpha1,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDalpha1,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDalpha1,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDalpha1,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDalpha1,0,0,7) - 
      0.5*GFOffset(PDalpha1,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha1,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDalpha1,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDalpha1,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDalpha1,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDalpha1,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDalpha1,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDalpha1,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDalpha1,0,0,7),0) + 
      IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha1,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDalpha1,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDalpha1,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDalpha1,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDalpha1,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDalpha1,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDalpha1,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDalpha1,0,0,6),0) + 
      IfThen(tk == 3,-0.444613449281090634955156033*GFOffset(PDalpha1,0,0,-3) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha1,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDalpha1,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDalpha1,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDalpha1,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDalpha1,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDalpha1,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDalpha1,0,0,5),0) + 
      IfThen(tk == 4,0.2734375*GFOffset(PDalpha1,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDalpha1,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDalpha1,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDalpha1,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDalpha1,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDalpha1,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDalpha1,0,0,3) - 
      0.2734375*GFOffset(PDalpha1,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha1,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDalpha1,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDalpha1,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDalpha1,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDalpha1,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDalpha1,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDalpha1,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDalpha1,0,0,3),0) + IfThen(tk 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha1,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDalpha1,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDalpha1,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDalpha1,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDalpha1,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDalpha1,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDalpha1,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDalpha1,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha1,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDalpha1,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDalpha1,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDalpha1,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDalpha1,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDalpha1,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDalpha1,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDalpha1,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDalpha1,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDalpha1,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDalpha1,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDalpha1,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDalpha1,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDalpha1,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDalpha1,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDalpha1,0,0,-1) + 
      18.*GFOffset(PDalpha1,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDalpha1,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDalpha1,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDalpha21 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDalpha2,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDalpha2,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDalpha2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha2,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha2,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha2,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha2,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha2,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha2,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha2,7,0,0) - 
      0.5*GFOffset(PDalpha2,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha2,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha2,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha2,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha2,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha2,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha2,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha2,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha2,7,0,0),0) + 
      IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha2,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha2,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha2,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha2,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha2,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha2,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha2,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha2,6,0,0),0) + 
      IfThen(ti == 3,-0.444613449281090634955156033*GFOffset(PDalpha2,-3,0,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha2,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha2,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha2,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha2,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha2,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha2,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha2,5,0,0),0) + 
      IfThen(ti == 4,0.2734375*GFOffset(PDalpha2,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha2,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha2,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha2,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha2,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha2,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha2,3,0,0) - 
      0.2734375*GFOffset(PDalpha2,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha2,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha2,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha2,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha2,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha2,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha2,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha2,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha2,3,0,0),0) + IfThen(ti 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha2,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha2,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha2,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha2,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha2,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha2,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha2,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha2,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha2,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha2,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha2,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha2,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha2,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha2,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha2,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha2,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDalpha2,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha2,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha2,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha2,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha2,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha2,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha2,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha2,-1,0,0) + 
      18.*GFOffset(PDalpha2,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDalpha2,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDalpha2,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha22 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDalpha2,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDalpha2,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDalpha2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha2,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha2,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha2,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha2,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha2,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha2,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha2,0,7,0) - 
      0.5*GFOffset(PDalpha2,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha2,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha2,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha2,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha2,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha2,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha2,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha2,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha2,0,7,0),0) + 
      IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha2,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha2,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha2,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha2,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha2,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha2,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha2,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha2,0,6,0),0) + 
      IfThen(tj == 3,-0.444613449281090634955156033*GFOffset(PDalpha2,0,-3,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha2,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha2,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha2,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha2,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha2,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha2,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha2,0,5,0),0) + 
      IfThen(tj == 4,0.2734375*GFOffset(PDalpha2,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha2,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha2,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha2,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha2,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha2,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha2,0,3,0) - 
      0.2734375*GFOffset(PDalpha2,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha2,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha2,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha2,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha2,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha2,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha2,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha2,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha2,0,3,0),0) + IfThen(tj 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha2,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha2,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha2,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha2,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha2,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha2,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha2,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha2,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha2,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha2,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha2,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha2,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha2,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha2,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha2,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha2,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDalpha2,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha2,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha2,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha2,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha2,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha2,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha2,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha2,0,-1,0) + 
      18.*GFOffset(PDalpha2,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDalpha2,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDalpha2,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha23 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDalpha2,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDalpha2,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDalpha2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha2,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDalpha2,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDalpha2,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDalpha2,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDalpha2,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDalpha2,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDalpha2,0,0,7) - 
      0.5*GFOffset(PDalpha2,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha2,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDalpha2,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDalpha2,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDalpha2,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDalpha2,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDalpha2,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDalpha2,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDalpha2,0,0,7),0) + 
      IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha2,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDalpha2,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDalpha2,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDalpha2,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDalpha2,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDalpha2,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDalpha2,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDalpha2,0,0,6),0) + 
      IfThen(tk == 3,-0.444613449281090634955156033*GFOffset(PDalpha2,0,0,-3) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha2,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDalpha2,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDalpha2,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDalpha2,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDalpha2,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDalpha2,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDalpha2,0,0,5),0) + 
      IfThen(tk == 4,0.2734375*GFOffset(PDalpha2,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDalpha2,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDalpha2,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDalpha2,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDalpha2,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDalpha2,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDalpha2,0,0,3) - 
      0.2734375*GFOffset(PDalpha2,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha2,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDalpha2,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDalpha2,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDalpha2,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDalpha2,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDalpha2,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDalpha2,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDalpha2,0,0,3),0) + IfThen(tk 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha2,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDalpha2,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDalpha2,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDalpha2,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDalpha2,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDalpha2,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDalpha2,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDalpha2,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha2,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDalpha2,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDalpha2,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDalpha2,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDalpha2,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDalpha2,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDalpha2,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDalpha2,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDalpha2,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDalpha2,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDalpha2,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDalpha2,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDalpha2,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDalpha2,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDalpha2,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDalpha2,0,0,-1) + 
      18.*GFOffset(PDalpha2,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDalpha2,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDalpha2,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDalpha31 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDalpha3,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDalpha3,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDalpha3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha3,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha3,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha3,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha3,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha3,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha3,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha3,7,0,0) - 
      0.5*GFOffset(PDalpha3,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha3,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha3,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha3,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha3,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha3,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha3,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha3,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha3,7,0,0),0) + 
      IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha3,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha3,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha3,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha3,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha3,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha3,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha3,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha3,6,0,0),0) + 
      IfThen(ti == 3,-0.444613449281090634955156033*GFOffset(PDalpha3,-3,0,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha3,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha3,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha3,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha3,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha3,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha3,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha3,5,0,0),0) + 
      IfThen(ti == 4,0.2734375*GFOffset(PDalpha3,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha3,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha3,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha3,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha3,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha3,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha3,3,0,0) - 
      0.2734375*GFOffset(PDalpha3,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha3,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha3,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha3,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha3,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha3,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha3,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha3,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha3,3,0,0),0) + IfThen(ti 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha3,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha3,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha3,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha3,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha3,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha3,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha3,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha3,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha3,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha3,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha3,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha3,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha3,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha3,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha3,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha3,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDalpha3,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha3,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha3,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha3,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha3,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha3,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha3,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha3,-1,0,0) + 
      18.*GFOffset(PDalpha3,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDalpha3,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDalpha3,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha32 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDalpha3,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDalpha3,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDalpha3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha3,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha3,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha3,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha3,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha3,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha3,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha3,0,7,0) - 
      0.5*GFOffset(PDalpha3,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha3,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha3,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha3,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha3,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha3,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha3,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha3,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha3,0,7,0),0) + 
      IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha3,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha3,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha3,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha3,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha3,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha3,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha3,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha3,0,6,0),0) + 
      IfThen(tj == 3,-0.444613449281090634955156033*GFOffset(PDalpha3,0,-3,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha3,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha3,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha3,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha3,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha3,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha3,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha3,0,5,0),0) + 
      IfThen(tj == 4,0.2734375*GFOffset(PDalpha3,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha3,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha3,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha3,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha3,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha3,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha3,0,3,0) - 
      0.2734375*GFOffset(PDalpha3,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha3,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha3,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha3,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha3,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha3,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha3,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha3,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha3,0,3,0),0) + IfThen(tj 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha3,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha3,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha3,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha3,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha3,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha3,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha3,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha3,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha3,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha3,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha3,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha3,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha3,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha3,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha3,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha3,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDalpha3,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha3,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha3,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha3,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha3,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha3,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha3,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha3,0,-1,0) + 
      18.*GFOffset(PDalpha3,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDalpha3,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDalpha3,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha33 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDalpha3,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDalpha3,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDalpha3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha3,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDalpha3,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDalpha3,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDalpha3,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDalpha3,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDalpha3,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDalpha3,0,0,7) - 
      0.5*GFOffset(PDalpha3,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha3,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDalpha3,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDalpha3,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDalpha3,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDalpha3,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDalpha3,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDalpha3,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDalpha3,0,0,7),0) + 
      IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha3,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDalpha3,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDalpha3,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDalpha3,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDalpha3,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDalpha3,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDalpha3,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDalpha3,0,0,6),0) + 
      IfThen(tk == 3,-0.444613449281090634955156033*GFOffset(PDalpha3,0,0,-3) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha3,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDalpha3,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDalpha3,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDalpha3,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDalpha3,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDalpha3,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDalpha3,0,0,5),0) + 
      IfThen(tk == 4,0.2734375*GFOffset(PDalpha3,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDalpha3,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDalpha3,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDalpha3,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDalpha3,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDalpha3,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDalpha3,0,0,3) - 
      0.2734375*GFOffset(PDalpha3,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha3,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDalpha3,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDalpha3,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDalpha3,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDalpha3,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDalpha3,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDalpha3,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDalpha3,0,0,3),0) + IfThen(tk 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha3,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDalpha3,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDalpha3,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDalpha3,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDalpha3,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDalpha3,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDalpha3,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDalpha3,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha3,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDalpha3,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDalpha3,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDalpha3,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDalpha3,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDalpha3,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDalpha3,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDalpha3,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDalpha3,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDalpha3,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDalpha3,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDalpha3,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDalpha3,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDalpha3,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDalpha3,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDalpha3,0,0,-1) + 
      18.*GFOffset(PDalpha3,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDalpha3,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDalpha3,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDPDalpha11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDPDalpha11 = J11L*LDPDalpha11 + J21L*LDPDalpha12 + J31L*LDPDalpha13;
      
      PDPDalpha12 = J12L*LDPDalpha11 + J22L*LDPDalpha12 + J32L*LDPDalpha13;
      
      PDPDalpha13 = J13L*LDPDalpha11 + J23L*LDPDalpha12 + J33L*LDPDalpha13;
      
      PDPDalpha21 = J11L*LDPDalpha21 + J21L*LDPDalpha22 + J31L*LDPDalpha23;
      
      PDPDalpha22 = J12L*LDPDalpha21 + J22L*LDPDalpha22 + J32L*LDPDalpha23;
      
      PDPDalpha23 = J13L*LDPDalpha21 + J23L*LDPDalpha22 + J33L*LDPDalpha23;
      
      PDPDalpha31 = J11L*LDPDalpha31 + J21L*LDPDalpha32 + J31L*LDPDalpha33;
      
      PDPDalpha32 = J12L*LDPDalpha31 + J22L*LDPDalpha32 + J32L*LDPDalpha33;
      
      PDPDalpha33 = J13L*LDPDalpha31 + J23L*LDPDalpha32 + J33L*LDPDalpha33;
    }
    else
    {
      PDPDalpha11 = LDPDalpha11;
      
      PDPDalpha12 = LDPDalpha12;
      
      PDPDalpha13 = LDPDalpha13;
      
      PDPDalpha21 = LDPDalpha21;
      
      PDPDalpha22 = LDPDalpha22;
      
      PDPDalpha23 = LDPDalpha23;
      
      PDPDalpha31 = LDPDalpha31;
      
      PDPDalpha32 = LDPDalpha32;
      
      PDPDalpha33 = LDPDalpha33;
    }
    
    CCTK_REAL LDuphiW1 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(phiW,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(phiW,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(phiW,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(phiW,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(phiW,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(phiW,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(phiW,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(phiW,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(phiW,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(phiW,7,0,0) - 
      0.5*GFOffset(phiW,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(phiW,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(phiW,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(phiW,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(phiW,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(phiW,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(phiW,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(phiW,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(phiW,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(phiW,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(phiW,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(phiW,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(phiW,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(phiW,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(phiW,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(phiW,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(phiW,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(phiW,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(phiW,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(phiW,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(phiW,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(phiW,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(phiW,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(phiW,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(phiW,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(phiW,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(phiW,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(phiW,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(phiW,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(phiW,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(phiW,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(phiW,3,0,0) - 
      0.2734375*GFOffset(phiW,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(phiW,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(phiW,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(phiW,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(phiW,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(phiW,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(phiW,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(phiW,2,0,0) + 
      0.444613449281090634955156033*GFOffset(phiW,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(phiW,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(phiW,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(phiW,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(phiW,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(phiW,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(phiW,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(phiW,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(phiW,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(phiW,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(phiW,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(phiW,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(phiW,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(phiW,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(phiW,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(phiW,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(phiW,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(phiW,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(phiW,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(phiW,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(phiW,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(phiW,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(phiW,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(phiW,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(phiW,-1,0,0) + 
      18.*GFOffset(phiW,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(phiW,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(phiW,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuphiW2 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(phiW,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(phiW,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(phiW,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(phiW,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(phiW,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(phiW,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(phiW,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(phiW,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(phiW,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(phiW,0,7,0) - 
      0.5*GFOffset(phiW,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(phiW,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(phiW,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(phiW,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(phiW,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(phiW,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(phiW,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(phiW,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(phiW,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(phiW,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(phiW,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(phiW,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(phiW,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(phiW,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(phiW,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(phiW,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(phiW,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(phiW,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(phiW,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(phiW,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(phiW,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(phiW,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(phiW,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(phiW,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(phiW,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(phiW,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(phiW,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(phiW,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(phiW,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(phiW,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(phiW,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(phiW,0,3,0) - 
      0.2734375*GFOffset(phiW,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(phiW,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(phiW,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(phiW,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(phiW,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(phiW,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(phiW,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(phiW,0,2,0) + 
      0.444613449281090634955156033*GFOffset(phiW,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(phiW,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(phiW,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(phiW,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(phiW,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(phiW,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(phiW,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(phiW,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(phiW,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(phiW,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(phiW,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(phiW,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(phiW,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(phiW,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(phiW,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(phiW,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(phiW,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(phiW,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(phiW,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(phiW,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(phiW,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(phiW,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(phiW,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(phiW,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(phiW,0,-1,0) + 
      18.*GFOffset(phiW,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(phiW,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(phiW,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuphiW3 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(phiW,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(phiW,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(phiW,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(phiW,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(phiW,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(phiW,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(phiW,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(phiW,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(phiW,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(phiW,0,0,7) - 
      0.5*GFOffset(phiW,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(phiW,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(phiW,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(phiW,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(phiW,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(phiW,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(phiW,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(phiW,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(phiW,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(phiW,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(phiW,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(phiW,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(phiW,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(phiW,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(phiW,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(phiW,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(phiW,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(phiW,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(phiW,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(phiW,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(phiW,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(phiW,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(phiW,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(phiW,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(phiW,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(phiW,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(phiW,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(phiW,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(phiW,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(phiW,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(phiW,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(phiW,0,0,3) - 
      0.2734375*GFOffset(phiW,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(phiW,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(phiW,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(phiW,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(phiW,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(phiW,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(phiW,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(phiW,0,0,2) + 
      0.444613449281090634955156033*GFOffset(phiW,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(phiW,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(phiW,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(phiW,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(phiW,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(phiW,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(phiW,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(phiW,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(phiW,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(phiW,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(phiW,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(phiW,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(phiW,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(phiW,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(phiW,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(phiW,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(phiW,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(phiW,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(phiW,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(phiW,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(phiW,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(phiW,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(phiW,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(phiW,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(phiW,0,0,-1) + 
      18.*GFOffset(phiW,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(phiW,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(phiW,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDugt111 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(gt11,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(gt11,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(gt11,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt11,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(gt11,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(gt11,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(gt11,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(gt11,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(gt11,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(gt11,7,0,0) - 
      0.5*GFOffset(gt11,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(gt11,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(gt11,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(gt11,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(gt11,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(gt11,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(gt11,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(gt11,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(gt11,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(gt11,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(gt11,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(gt11,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(gt11,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(gt11,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(gt11,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(gt11,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(gt11,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(gt11,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(gt11,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(gt11,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(gt11,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(gt11,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(gt11,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(gt11,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(gt11,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(gt11,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(gt11,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(gt11,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(gt11,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(gt11,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(gt11,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(gt11,3,0,0) - 
      0.2734375*GFOffset(gt11,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(gt11,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(gt11,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(gt11,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(gt11,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(gt11,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(gt11,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(gt11,2,0,0) + 
      0.444613449281090634955156033*GFOffset(gt11,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(gt11,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(gt11,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(gt11,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(gt11,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(gt11,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(gt11,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(gt11,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(gt11,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(gt11,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(gt11,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(gt11,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(gt11,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(gt11,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(gt11,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(gt11,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(gt11,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(gt11,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(gt11,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(gt11,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(gt11,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(gt11,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(gt11,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(gt11,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(gt11,-1,0,0) + 
      18.*GFOffset(gt11,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(gt11,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(gt11,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDugt112 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(gt11,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(gt11,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(gt11,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt11,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(gt11,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(gt11,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(gt11,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(gt11,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(gt11,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(gt11,0,7,0) - 
      0.5*GFOffset(gt11,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(gt11,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(gt11,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(gt11,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(gt11,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(gt11,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(gt11,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(gt11,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(gt11,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(gt11,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(gt11,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(gt11,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(gt11,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(gt11,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(gt11,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(gt11,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(gt11,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(gt11,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(gt11,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(gt11,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(gt11,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(gt11,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(gt11,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(gt11,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(gt11,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(gt11,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(gt11,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(gt11,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(gt11,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(gt11,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(gt11,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(gt11,0,3,0) - 
      0.2734375*GFOffset(gt11,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(gt11,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(gt11,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(gt11,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(gt11,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(gt11,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(gt11,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(gt11,0,2,0) + 
      0.444613449281090634955156033*GFOffset(gt11,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(gt11,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(gt11,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(gt11,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(gt11,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(gt11,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(gt11,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(gt11,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(gt11,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(gt11,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(gt11,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(gt11,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(gt11,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(gt11,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(gt11,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(gt11,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(gt11,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(gt11,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(gt11,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(gt11,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(gt11,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(gt11,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(gt11,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(gt11,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(gt11,0,-1,0) + 
      18.*GFOffset(gt11,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(gt11,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(gt11,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDugt113 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(gt11,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(gt11,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(gt11,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt11,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(gt11,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(gt11,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(gt11,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(gt11,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(gt11,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(gt11,0,0,7) - 
      0.5*GFOffset(gt11,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(gt11,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(gt11,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(gt11,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(gt11,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(gt11,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(gt11,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(gt11,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(gt11,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(gt11,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(gt11,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(gt11,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(gt11,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(gt11,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(gt11,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(gt11,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(gt11,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(gt11,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(gt11,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(gt11,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(gt11,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(gt11,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(gt11,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(gt11,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(gt11,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(gt11,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(gt11,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(gt11,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(gt11,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(gt11,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(gt11,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(gt11,0,0,3) - 
      0.2734375*GFOffset(gt11,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(gt11,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(gt11,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(gt11,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(gt11,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(gt11,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(gt11,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(gt11,0,0,2) + 
      0.444613449281090634955156033*GFOffset(gt11,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(gt11,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(gt11,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(gt11,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(gt11,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(gt11,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(gt11,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(gt11,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(gt11,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(gt11,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(gt11,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(gt11,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(gt11,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(gt11,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(gt11,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(gt11,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(gt11,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(gt11,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(gt11,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(gt11,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(gt11,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(gt11,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(gt11,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(gt11,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(gt11,0,0,-1) + 
      18.*GFOffset(gt11,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(gt11,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(gt11,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDugt121 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(gt12,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(gt12,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(gt12,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt12,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(gt12,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(gt12,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(gt12,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(gt12,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(gt12,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(gt12,7,0,0) - 
      0.5*GFOffset(gt12,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(gt12,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(gt12,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(gt12,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(gt12,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(gt12,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(gt12,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(gt12,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(gt12,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(gt12,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(gt12,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(gt12,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(gt12,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(gt12,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(gt12,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(gt12,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(gt12,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(gt12,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(gt12,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(gt12,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(gt12,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(gt12,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(gt12,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(gt12,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(gt12,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(gt12,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(gt12,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(gt12,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(gt12,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(gt12,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(gt12,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(gt12,3,0,0) - 
      0.2734375*GFOffset(gt12,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(gt12,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(gt12,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(gt12,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(gt12,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(gt12,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(gt12,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(gt12,2,0,0) + 
      0.444613449281090634955156033*GFOffset(gt12,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(gt12,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(gt12,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(gt12,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(gt12,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(gt12,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(gt12,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(gt12,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(gt12,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(gt12,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(gt12,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(gt12,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(gt12,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(gt12,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(gt12,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(gt12,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(gt12,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(gt12,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(gt12,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(gt12,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(gt12,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(gt12,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(gt12,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(gt12,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(gt12,-1,0,0) + 
      18.*GFOffset(gt12,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(gt12,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(gt12,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDugt122 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(gt12,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(gt12,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(gt12,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt12,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(gt12,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(gt12,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(gt12,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(gt12,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(gt12,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(gt12,0,7,0) - 
      0.5*GFOffset(gt12,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(gt12,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(gt12,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(gt12,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(gt12,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(gt12,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(gt12,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(gt12,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(gt12,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(gt12,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(gt12,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(gt12,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(gt12,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(gt12,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(gt12,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(gt12,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(gt12,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(gt12,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(gt12,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(gt12,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(gt12,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(gt12,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(gt12,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(gt12,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(gt12,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(gt12,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(gt12,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(gt12,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(gt12,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(gt12,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(gt12,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(gt12,0,3,0) - 
      0.2734375*GFOffset(gt12,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(gt12,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(gt12,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(gt12,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(gt12,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(gt12,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(gt12,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(gt12,0,2,0) + 
      0.444613449281090634955156033*GFOffset(gt12,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(gt12,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(gt12,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(gt12,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(gt12,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(gt12,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(gt12,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(gt12,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(gt12,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(gt12,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(gt12,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(gt12,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(gt12,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(gt12,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(gt12,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(gt12,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(gt12,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(gt12,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(gt12,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(gt12,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(gt12,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(gt12,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(gt12,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(gt12,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(gt12,0,-1,0) + 
      18.*GFOffset(gt12,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(gt12,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(gt12,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDugt123 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(gt12,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(gt12,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(gt12,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt12,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(gt12,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(gt12,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(gt12,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(gt12,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(gt12,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(gt12,0,0,7) - 
      0.5*GFOffset(gt12,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(gt12,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(gt12,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(gt12,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(gt12,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(gt12,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(gt12,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(gt12,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(gt12,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(gt12,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(gt12,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(gt12,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(gt12,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(gt12,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(gt12,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(gt12,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(gt12,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(gt12,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(gt12,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(gt12,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(gt12,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(gt12,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(gt12,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(gt12,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(gt12,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(gt12,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(gt12,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(gt12,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(gt12,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(gt12,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(gt12,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(gt12,0,0,3) - 
      0.2734375*GFOffset(gt12,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(gt12,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(gt12,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(gt12,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(gt12,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(gt12,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(gt12,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(gt12,0,0,2) + 
      0.444613449281090634955156033*GFOffset(gt12,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(gt12,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(gt12,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(gt12,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(gt12,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(gt12,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(gt12,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(gt12,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(gt12,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(gt12,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(gt12,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(gt12,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(gt12,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(gt12,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(gt12,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(gt12,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(gt12,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(gt12,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(gt12,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(gt12,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(gt12,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(gt12,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(gt12,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(gt12,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(gt12,0,0,-1) + 
      18.*GFOffset(gt12,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(gt12,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(gt12,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDugt131 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(gt13,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(gt13,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(gt13,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt13,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(gt13,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(gt13,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(gt13,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(gt13,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(gt13,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(gt13,7,0,0) - 
      0.5*GFOffset(gt13,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(gt13,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(gt13,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(gt13,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(gt13,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(gt13,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(gt13,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(gt13,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(gt13,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(gt13,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(gt13,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(gt13,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(gt13,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(gt13,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(gt13,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(gt13,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(gt13,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(gt13,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(gt13,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(gt13,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(gt13,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(gt13,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(gt13,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(gt13,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(gt13,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(gt13,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(gt13,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(gt13,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(gt13,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(gt13,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(gt13,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(gt13,3,0,0) - 
      0.2734375*GFOffset(gt13,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(gt13,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(gt13,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(gt13,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(gt13,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(gt13,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(gt13,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(gt13,2,0,0) + 
      0.444613449281090634955156033*GFOffset(gt13,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(gt13,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(gt13,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(gt13,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(gt13,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(gt13,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(gt13,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(gt13,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(gt13,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(gt13,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(gt13,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(gt13,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(gt13,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(gt13,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(gt13,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(gt13,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(gt13,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(gt13,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(gt13,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(gt13,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(gt13,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(gt13,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(gt13,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(gt13,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(gt13,-1,0,0) + 
      18.*GFOffset(gt13,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(gt13,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(gt13,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDugt132 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(gt13,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(gt13,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(gt13,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt13,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(gt13,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(gt13,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(gt13,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(gt13,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(gt13,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(gt13,0,7,0) - 
      0.5*GFOffset(gt13,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(gt13,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(gt13,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(gt13,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(gt13,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(gt13,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(gt13,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(gt13,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(gt13,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(gt13,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(gt13,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(gt13,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(gt13,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(gt13,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(gt13,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(gt13,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(gt13,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(gt13,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(gt13,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(gt13,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(gt13,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(gt13,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(gt13,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(gt13,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(gt13,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(gt13,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(gt13,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(gt13,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(gt13,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(gt13,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(gt13,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(gt13,0,3,0) - 
      0.2734375*GFOffset(gt13,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(gt13,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(gt13,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(gt13,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(gt13,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(gt13,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(gt13,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(gt13,0,2,0) + 
      0.444613449281090634955156033*GFOffset(gt13,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(gt13,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(gt13,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(gt13,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(gt13,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(gt13,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(gt13,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(gt13,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(gt13,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(gt13,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(gt13,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(gt13,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(gt13,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(gt13,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(gt13,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(gt13,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(gt13,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(gt13,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(gt13,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(gt13,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(gt13,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(gt13,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(gt13,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(gt13,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(gt13,0,-1,0) + 
      18.*GFOffset(gt13,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(gt13,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(gt13,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDugt133 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(gt13,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(gt13,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(gt13,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt13,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(gt13,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(gt13,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(gt13,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(gt13,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(gt13,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(gt13,0,0,7) - 
      0.5*GFOffset(gt13,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(gt13,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(gt13,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(gt13,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(gt13,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(gt13,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(gt13,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(gt13,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(gt13,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(gt13,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(gt13,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(gt13,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(gt13,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(gt13,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(gt13,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(gt13,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(gt13,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(gt13,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(gt13,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(gt13,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(gt13,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(gt13,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(gt13,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(gt13,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(gt13,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(gt13,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(gt13,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(gt13,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(gt13,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(gt13,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(gt13,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(gt13,0,0,3) - 
      0.2734375*GFOffset(gt13,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(gt13,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(gt13,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(gt13,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(gt13,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(gt13,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(gt13,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(gt13,0,0,2) + 
      0.444613449281090634955156033*GFOffset(gt13,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(gt13,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(gt13,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(gt13,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(gt13,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(gt13,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(gt13,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(gt13,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(gt13,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(gt13,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(gt13,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(gt13,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(gt13,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(gt13,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(gt13,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(gt13,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(gt13,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(gt13,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(gt13,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(gt13,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(gt13,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(gt13,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(gt13,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(gt13,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(gt13,0,0,-1) + 
      18.*GFOffset(gt13,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(gt13,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(gt13,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDugt221 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(gt22,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(gt22,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(gt22,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt22,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(gt22,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(gt22,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(gt22,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(gt22,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(gt22,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(gt22,7,0,0) - 
      0.5*GFOffset(gt22,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(gt22,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(gt22,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(gt22,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(gt22,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(gt22,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(gt22,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(gt22,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(gt22,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(gt22,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(gt22,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(gt22,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(gt22,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(gt22,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(gt22,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(gt22,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(gt22,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(gt22,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(gt22,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(gt22,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(gt22,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(gt22,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(gt22,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(gt22,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(gt22,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(gt22,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(gt22,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(gt22,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(gt22,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(gt22,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(gt22,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(gt22,3,0,0) - 
      0.2734375*GFOffset(gt22,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(gt22,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(gt22,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(gt22,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(gt22,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(gt22,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(gt22,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(gt22,2,0,0) + 
      0.444613449281090634955156033*GFOffset(gt22,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(gt22,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(gt22,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(gt22,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(gt22,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(gt22,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(gt22,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(gt22,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(gt22,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(gt22,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(gt22,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(gt22,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(gt22,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(gt22,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(gt22,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(gt22,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(gt22,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(gt22,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(gt22,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(gt22,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(gt22,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(gt22,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(gt22,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(gt22,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(gt22,-1,0,0) + 
      18.*GFOffset(gt22,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(gt22,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(gt22,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDugt222 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(gt22,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(gt22,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(gt22,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt22,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(gt22,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(gt22,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(gt22,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(gt22,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(gt22,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(gt22,0,7,0) - 
      0.5*GFOffset(gt22,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(gt22,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(gt22,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(gt22,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(gt22,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(gt22,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(gt22,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(gt22,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(gt22,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(gt22,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(gt22,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(gt22,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(gt22,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(gt22,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(gt22,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(gt22,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(gt22,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(gt22,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(gt22,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(gt22,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(gt22,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(gt22,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(gt22,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(gt22,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(gt22,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(gt22,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(gt22,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(gt22,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(gt22,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(gt22,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(gt22,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(gt22,0,3,0) - 
      0.2734375*GFOffset(gt22,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(gt22,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(gt22,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(gt22,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(gt22,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(gt22,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(gt22,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(gt22,0,2,0) + 
      0.444613449281090634955156033*GFOffset(gt22,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(gt22,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(gt22,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(gt22,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(gt22,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(gt22,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(gt22,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(gt22,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(gt22,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(gt22,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(gt22,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(gt22,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(gt22,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(gt22,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(gt22,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(gt22,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(gt22,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(gt22,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(gt22,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(gt22,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(gt22,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(gt22,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(gt22,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(gt22,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(gt22,0,-1,0) + 
      18.*GFOffset(gt22,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(gt22,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(gt22,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDugt223 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(gt22,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(gt22,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(gt22,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt22,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(gt22,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(gt22,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(gt22,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(gt22,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(gt22,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(gt22,0,0,7) - 
      0.5*GFOffset(gt22,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(gt22,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(gt22,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(gt22,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(gt22,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(gt22,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(gt22,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(gt22,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(gt22,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(gt22,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(gt22,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(gt22,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(gt22,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(gt22,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(gt22,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(gt22,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(gt22,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(gt22,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(gt22,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(gt22,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(gt22,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(gt22,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(gt22,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(gt22,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(gt22,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(gt22,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(gt22,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(gt22,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(gt22,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(gt22,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(gt22,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(gt22,0,0,3) - 
      0.2734375*GFOffset(gt22,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(gt22,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(gt22,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(gt22,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(gt22,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(gt22,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(gt22,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(gt22,0,0,2) + 
      0.444613449281090634955156033*GFOffset(gt22,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(gt22,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(gt22,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(gt22,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(gt22,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(gt22,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(gt22,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(gt22,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(gt22,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(gt22,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(gt22,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(gt22,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(gt22,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(gt22,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(gt22,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(gt22,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(gt22,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(gt22,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(gt22,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(gt22,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(gt22,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(gt22,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(gt22,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(gt22,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(gt22,0,0,-1) + 
      18.*GFOffset(gt22,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(gt22,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(gt22,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDugt231 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(gt23,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(gt23,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(gt23,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt23,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(gt23,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(gt23,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(gt23,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(gt23,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(gt23,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(gt23,7,0,0) - 
      0.5*GFOffset(gt23,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(gt23,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(gt23,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(gt23,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(gt23,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(gt23,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(gt23,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(gt23,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(gt23,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(gt23,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(gt23,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(gt23,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(gt23,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(gt23,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(gt23,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(gt23,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(gt23,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(gt23,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(gt23,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(gt23,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(gt23,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(gt23,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(gt23,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(gt23,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(gt23,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(gt23,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(gt23,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(gt23,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(gt23,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(gt23,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(gt23,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(gt23,3,0,0) - 
      0.2734375*GFOffset(gt23,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(gt23,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(gt23,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(gt23,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(gt23,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(gt23,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(gt23,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(gt23,2,0,0) + 
      0.444613449281090634955156033*GFOffset(gt23,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(gt23,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(gt23,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(gt23,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(gt23,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(gt23,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(gt23,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(gt23,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(gt23,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(gt23,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(gt23,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(gt23,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(gt23,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(gt23,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(gt23,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(gt23,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(gt23,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(gt23,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(gt23,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(gt23,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(gt23,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(gt23,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(gt23,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(gt23,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(gt23,-1,0,0) + 
      18.*GFOffset(gt23,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(gt23,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(gt23,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDugt232 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(gt23,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(gt23,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(gt23,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt23,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(gt23,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(gt23,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(gt23,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(gt23,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(gt23,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(gt23,0,7,0) - 
      0.5*GFOffset(gt23,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(gt23,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(gt23,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(gt23,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(gt23,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(gt23,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(gt23,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(gt23,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(gt23,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(gt23,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(gt23,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(gt23,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(gt23,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(gt23,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(gt23,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(gt23,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(gt23,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(gt23,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(gt23,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(gt23,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(gt23,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(gt23,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(gt23,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(gt23,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(gt23,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(gt23,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(gt23,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(gt23,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(gt23,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(gt23,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(gt23,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(gt23,0,3,0) - 
      0.2734375*GFOffset(gt23,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(gt23,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(gt23,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(gt23,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(gt23,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(gt23,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(gt23,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(gt23,0,2,0) + 
      0.444613449281090634955156033*GFOffset(gt23,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(gt23,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(gt23,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(gt23,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(gt23,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(gt23,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(gt23,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(gt23,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(gt23,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(gt23,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(gt23,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(gt23,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(gt23,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(gt23,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(gt23,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(gt23,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(gt23,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(gt23,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(gt23,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(gt23,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(gt23,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(gt23,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(gt23,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(gt23,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(gt23,0,-1,0) + 
      18.*GFOffset(gt23,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(gt23,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(gt23,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDugt233 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(gt23,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(gt23,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(gt23,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt23,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(gt23,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(gt23,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(gt23,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(gt23,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(gt23,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(gt23,0,0,7) - 
      0.5*GFOffset(gt23,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(gt23,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(gt23,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(gt23,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(gt23,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(gt23,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(gt23,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(gt23,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(gt23,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(gt23,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(gt23,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(gt23,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(gt23,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(gt23,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(gt23,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(gt23,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(gt23,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(gt23,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(gt23,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(gt23,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(gt23,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(gt23,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(gt23,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(gt23,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(gt23,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(gt23,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(gt23,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(gt23,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(gt23,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(gt23,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(gt23,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(gt23,0,0,3) - 
      0.2734375*GFOffset(gt23,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(gt23,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(gt23,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(gt23,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(gt23,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(gt23,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(gt23,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(gt23,0,0,2) + 
      0.444613449281090634955156033*GFOffset(gt23,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(gt23,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(gt23,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(gt23,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(gt23,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(gt23,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(gt23,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(gt23,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(gt23,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(gt23,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(gt23,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(gt23,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(gt23,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(gt23,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(gt23,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(gt23,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(gt23,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(gt23,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(gt23,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(gt23,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(gt23,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(gt23,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(gt23,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(gt23,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(gt23,0,0,-1) + 
      18.*GFOffset(gt23,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(gt23,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(gt23,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDugt331 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(gt33,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(gt33,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(gt33,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt33,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(gt33,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(gt33,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(gt33,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(gt33,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(gt33,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(gt33,7,0,0) - 
      0.5*GFOffset(gt33,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(gt33,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(gt33,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(gt33,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(gt33,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(gt33,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(gt33,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(gt33,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(gt33,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(gt33,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(gt33,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(gt33,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(gt33,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(gt33,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(gt33,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(gt33,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(gt33,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(gt33,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(gt33,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(gt33,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(gt33,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(gt33,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(gt33,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(gt33,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(gt33,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(gt33,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(gt33,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(gt33,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(gt33,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(gt33,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(gt33,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(gt33,3,0,0) - 
      0.2734375*GFOffset(gt33,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(gt33,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(gt33,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(gt33,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(gt33,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(gt33,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(gt33,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(gt33,2,0,0) + 
      0.444613449281090634955156033*GFOffset(gt33,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(gt33,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(gt33,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(gt33,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(gt33,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(gt33,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(gt33,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(gt33,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(gt33,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(gt33,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(gt33,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(gt33,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(gt33,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(gt33,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(gt33,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(gt33,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(gt33,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(gt33,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(gt33,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(gt33,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(gt33,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(gt33,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(gt33,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(gt33,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(gt33,-1,0,0) + 
      18.*GFOffset(gt33,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(gt33,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(gt33,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDugt332 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(gt33,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(gt33,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(gt33,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt33,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(gt33,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(gt33,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(gt33,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(gt33,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(gt33,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(gt33,0,7,0) - 
      0.5*GFOffset(gt33,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(gt33,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(gt33,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(gt33,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(gt33,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(gt33,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(gt33,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(gt33,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(gt33,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(gt33,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(gt33,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(gt33,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(gt33,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(gt33,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(gt33,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(gt33,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(gt33,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(gt33,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(gt33,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(gt33,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(gt33,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(gt33,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(gt33,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(gt33,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(gt33,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(gt33,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(gt33,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(gt33,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(gt33,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(gt33,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(gt33,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(gt33,0,3,0) - 
      0.2734375*GFOffset(gt33,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(gt33,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(gt33,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(gt33,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(gt33,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(gt33,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(gt33,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(gt33,0,2,0) + 
      0.444613449281090634955156033*GFOffset(gt33,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(gt33,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(gt33,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(gt33,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(gt33,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(gt33,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(gt33,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(gt33,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(gt33,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(gt33,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(gt33,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(gt33,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(gt33,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(gt33,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(gt33,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(gt33,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(gt33,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(gt33,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(gt33,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(gt33,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(gt33,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(gt33,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(gt33,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(gt33,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(gt33,0,-1,0) + 
      18.*GFOffset(gt33,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(gt33,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(gt33,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDugt333 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(gt33,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(gt33,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(gt33,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(gt33,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(gt33,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(gt33,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(gt33,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(gt33,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(gt33,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(gt33,0,0,7) - 
      0.5*GFOffset(gt33,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(gt33,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(gt33,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(gt33,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(gt33,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(gt33,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(gt33,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(gt33,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(gt33,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(gt33,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(gt33,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(gt33,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(gt33,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(gt33,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(gt33,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(gt33,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(gt33,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(gt33,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(gt33,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(gt33,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(gt33,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(gt33,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(gt33,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(gt33,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(gt33,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(gt33,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(gt33,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(gt33,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(gt33,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(gt33,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(gt33,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(gt33,0,0,3) - 
      0.2734375*GFOffset(gt33,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(gt33,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(gt33,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(gt33,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(gt33,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(gt33,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(gt33,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(gt33,0,0,2) + 
      0.444613449281090634955156033*GFOffset(gt33,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(gt33,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(gt33,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(gt33,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(gt33,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(gt33,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(gt33,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(gt33,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(gt33,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(gt33,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(gt33,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(gt33,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(gt33,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(gt33,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(gt33,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(gt33,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(gt33,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(gt33,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(gt33,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(gt33,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(gt33,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(gt33,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(gt33,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(gt33,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(gt33,0,0,-1) + 
      18.*GFOffset(gt33,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(gt33,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(gt33,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDutrK1 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(trK,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(trK,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(trK,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(trK,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(trK,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(trK,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(trK,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(trK,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(trK,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(trK,7,0,0) - 
      0.5*GFOffset(trK,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(trK,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(trK,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(trK,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(trK,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(trK,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(trK,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(trK,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(trK,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(trK,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(trK,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(trK,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(trK,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(trK,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(trK,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(trK,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(trK,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(trK,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(trK,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(trK,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(trK,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(trK,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(trK,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(trK,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(trK,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(trK,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(trK,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(trK,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(trK,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(trK,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(trK,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(trK,3,0,0) - 
      0.2734375*GFOffset(trK,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(trK,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(trK,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(trK,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(trK,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(trK,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(trK,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(trK,2,0,0) + 
      0.444613449281090634955156033*GFOffset(trK,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(trK,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(trK,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(trK,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(trK,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(trK,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(trK,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(trK,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(trK,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(trK,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(trK,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(trK,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(trK,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(trK,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(trK,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(trK,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(trK,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(trK,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(trK,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(trK,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(trK,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(trK,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(trK,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(trK,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(trK,-1,0,0) + 
      18.*GFOffset(trK,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(trK,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(trK,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDutrK2 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(trK,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(trK,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(trK,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(trK,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(trK,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(trK,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(trK,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(trK,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(trK,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(trK,0,7,0) - 
      0.5*GFOffset(trK,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(trK,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(trK,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(trK,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(trK,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(trK,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(trK,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(trK,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(trK,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(trK,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(trK,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(trK,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(trK,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(trK,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(trK,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(trK,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(trK,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(trK,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(trK,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(trK,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(trK,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(trK,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(trK,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(trK,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(trK,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(trK,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(trK,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(trK,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(trK,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(trK,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(trK,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(trK,0,3,0) - 
      0.2734375*GFOffset(trK,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(trK,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(trK,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(trK,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(trK,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(trK,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(trK,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(trK,0,2,0) + 
      0.444613449281090634955156033*GFOffset(trK,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(trK,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(trK,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(trK,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(trK,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(trK,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(trK,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(trK,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(trK,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(trK,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(trK,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(trK,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(trK,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(trK,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(trK,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(trK,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(trK,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(trK,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(trK,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(trK,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(trK,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(trK,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(trK,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(trK,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(trK,0,-1,0) + 
      18.*GFOffset(trK,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(trK,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(trK,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDutrK3 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(trK,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(trK,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(trK,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(trK,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(trK,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(trK,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(trK,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(trK,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(trK,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(trK,0,0,7) - 
      0.5*GFOffset(trK,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(trK,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(trK,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(trK,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(trK,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(trK,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(trK,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(trK,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(trK,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(trK,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(trK,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(trK,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(trK,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(trK,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(trK,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(trK,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(trK,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(trK,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(trK,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(trK,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(trK,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(trK,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(trK,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(trK,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(trK,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(trK,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(trK,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(trK,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(trK,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(trK,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(trK,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(trK,0,0,3) - 
      0.2734375*GFOffset(trK,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(trK,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(trK,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(trK,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(trK,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(trK,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(trK,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(trK,0,0,2) + 
      0.444613449281090634955156033*GFOffset(trK,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(trK,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(trK,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(trK,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(trK,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(trK,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(trK,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(trK,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(trK,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(trK,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(trK,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(trK,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(trK,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(trK,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(trK,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(trK,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(trK,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(trK,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(trK,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(trK,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(trK,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(trK,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(trK,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(trK,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(trK,0,0,-1) + 
      18.*GFOffset(trK,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(trK,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(trK,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDualpha1 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(alpha,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(alpha,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(alpha,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(alpha,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(alpha,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(alpha,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(alpha,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(alpha,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(alpha,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(alpha,7,0,0) - 
      0.5*GFOffset(alpha,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(alpha,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(alpha,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(alpha,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(alpha,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(alpha,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(alpha,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(alpha,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(alpha,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(alpha,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(alpha,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(alpha,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(alpha,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(alpha,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(alpha,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(alpha,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(alpha,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(alpha,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(alpha,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(alpha,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(alpha,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(alpha,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(alpha,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(alpha,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(alpha,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(alpha,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(alpha,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(alpha,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(alpha,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(alpha,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(alpha,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(alpha,3,0,0) - 
      0.2734375*GFOffset(alpha,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(alpha,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(alpha,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(alpha,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(alpha,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(alpha,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(alpha,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(alpha,2,0,0) + 
      0.444613449281090634955156033*GFOffset(alpha,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(alpha,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(alpha,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(alpha,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(alpha,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(alpha,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(alpha,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(alpha,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(alpha,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(alpha,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(alpha,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(alpha,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(alpha,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(alpha,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(alpha,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(alpha,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(alpha,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(alpha,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(alpha,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(alpha,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(alpha,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(alpha,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(alpha,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(alpha,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(alpha,-1,0,0) + 
      18.*GFOffset(alpha,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(alpha,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(alpha,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDualpha2 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(alpha,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(alpha,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(alpha,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(alpha,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(alpha,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(alpha,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(alpha,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(alpha,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(alpha,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(alpha,0,7,0) - 
      0.5*GFOffset(alpha,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(alpha,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(alpha,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(alpha,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(alpha,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(alpha,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(alpha,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(alpha,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(alpha,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(alpha,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(alpha,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(alpha,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(alpha,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(alpha,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(alpha,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(alpha,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(alpha,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(alpha,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(alpha,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(alpha,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(alpha,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(alpha,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(alpha,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(alpha,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(alpha,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(alpha,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(alpha,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(alpha,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(alpha,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(alpha,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(alpha,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(alpha,0,3,0) - 
      0.2734375*GFOffset(alpha,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(alpha,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(alpha,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(alpha,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(alpha,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(alpha,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(alpha,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(alpha,0,2,0) + 
      0.444613449281090634955156033*GFOffset(alpha,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(alpha,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(alpha,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(alpha,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(alpha,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(alpha,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(alpha,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(alpha,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(alpha,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(alpha,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(alpha,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(alpha,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(alpha,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(alpha,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(alpha,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(alpha,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(alpha,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(alpha,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(alpha,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(alpha,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(alpha,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(alpha,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(alpha,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(alpha,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(alpha,0,-1,0) + 
      18.*GFOffset(alpha,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(alpha,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(alpha,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDualpha3 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(alpha,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(alpha,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(alpha,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(alpha,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(alpha,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(alpha,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(alpha,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(alpha,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(alpha,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(alpha,0,0,7) - 
      0.5*GFOffset(alpha,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(alpha,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(alpha,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(alpha,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(alpha,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(alpha,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(alpha,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(alpha,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(alpha,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(alpha,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(alpha,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(alpha,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(alpha,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(alpha,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(alpha,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(alpha,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(alpha,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(alpha,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(alpha,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(alpha,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(alpha,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(alpha,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(alpha,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(alpha,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(alpha,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(alpha,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(alpha,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(alpha,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(alpha,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(alpha,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(alpha,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(alpha,0,0,3) - 
      0.2734375*GFOffset(alpha,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(alpha,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(alpha,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(alpha,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(alpha,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(alpha,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(alpha,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(alpha,0,0,2) + 
      0.444613449281090634955156033*GFOffset(alpha,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(alpha,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(alpha,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(alpha,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(alpha,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(alpha,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(alpha,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(alpha,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(alpha,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(alpha,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(alpha,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(alpha,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(alpha,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(alpha,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(alpha,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(alpha,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(alpha,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(alpha,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(alpha,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(alpha,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(alpha,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(alpha,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(alpha,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(alpha,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(alpha,0,0,-1) + 
      18.*GFOffset(alpha,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(alpha,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(alpha,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDuA1 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(A,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(A,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 0,-18.*GFOffset(A,0,0,0) 
      + 24.3497451715930658264745651379*GFOffset(A,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(A,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(A,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(A,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(A,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(A,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(A,7,0,0) - 
      0.5*GFOffset(A,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(A,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(A,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(A,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(A,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(A,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(A,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(A,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(A,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(A,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(A,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(A,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(A,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(A,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(A,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(A,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(A,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(A,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(A,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(A,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(A,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(A,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(A,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(A,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(A,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(A,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(A,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(A,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(A,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(A,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(A,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(A,3,0,0) - 
      0.2734375*GFOffset(A,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(A,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(A,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(A,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(A,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(A,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(A,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(A,2,0,0) + 
      0.444613449281090634955156033*GFOffset(A,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(A,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(A,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(A,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(A,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(A,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(A,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(A,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(A,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(A,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(A,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(A,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(A,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(A,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(A,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(A,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(A,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(A,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(A,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(A,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(A,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(A,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(A,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(A,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(A,-1,0,0) + 
      18.*GFOffset(A,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(A,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(A,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuA2 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(A,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(A,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 0,-18.*GFOffset(A,0,0,0) 
      + 24.3497451715930658264745651379*GFOffset(A,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(A,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(A,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(A,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(A,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(A,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(A,0,7,0) - 
      0.5*GFOffset(A,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(A,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(A,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(A,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(A,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(A,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(A,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(A,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(A,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(A,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(A,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(A,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(A,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(A,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(A,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(A,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(A,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(A,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(A,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(A,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(A,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(A,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(A,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(A,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(A,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(A,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(A,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(A,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(A,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(A,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(A,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(A,0,3,0) - 
      0.2734375*GFOffset(A,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(A,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(A,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(A,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(A,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(A,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(A,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(A,0,2,0) + 
      0.444613449281090634955156033*GFOffset(A,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(A,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(A,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(A,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(A,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(A,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(A,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(A,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(A,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(A,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(A,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(A,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(A,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(A,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(A,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(A,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(A,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(A,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(A,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(A,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(A,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(A,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(A,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(A,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(A,0,-1,0) + 
      18.*GFOffset(A,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(A,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(A,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuA3 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(A,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(A,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 0,-18.*GFOffset(A,0,0,0) 
      + 24.3497451715930658264745651379*GFOffset(A,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(A,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(A,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(A,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(A,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(A,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(A,0,0,7) - 
      0.5*GFOffset(A,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(A,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(A,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(A,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(A,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(A,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(A,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(A,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(A,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(A,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(A,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(A,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(A,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(A,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(A,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(A,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(A,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(A,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(A,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(A,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(A,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(A,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(A,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(A,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(A,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(A,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(A,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(A,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(A,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(A,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(A,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(A,0,0,3) - 
      0.2734375*GFOffset(A,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(A,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(A,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(A,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(A,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(A,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(A,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(A,0,0,2) + 
      0.444613449281090634955156033*GFOffset(A,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(A,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(A,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(A,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(A,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(A,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(A,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(A,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(A,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(A,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(A,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(A,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(A,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(A,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(A,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(A,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(A,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(A,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(A,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(A,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(A,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(A,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(A,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(A,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(A,0,0,-1) + 
      18.*GFOffset(A,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(A,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(A,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDuA1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuA2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuA3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualpha1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualpha2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDualpha3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt111 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt112 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt113 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt121 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt122 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt123 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt131 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt132 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt133 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDugt333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuphiW1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuphiW2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuphiW3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDutrK1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDutrK2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDutrK3 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDuphiW1 = J11L*LDuphiW1 + J21L*LDuphiW2 + J31L*LDuphiW3;
      
      PDuphiW2 = J12L*LDuphiW1 + J22L*LDuphiW2 + J32L*LDuphiW3;
      
      PDuphiW3 = J13L*LDuphiW1 + J23L*LDuphiW2 + J33L*LDuphiW3;
      
      PDugt111 = J11L*LDugt111 + J21L*LDugt112 + J31L*LDugt113;
      
      PDugt112 = J12L*LDugt111 + J22L*LDugt112 + J32L*LDugt113;
      
      PDugt113 = J13L*LDugt111 + J23L*LDugt112 + J33L*LDugt113;
      
      PDugt121 = J11L*LDugt121 + J21L*LDugt122 + J31L*LDugt123;
      
      PDugt122 = J12L*LDugt121 + J22L*LDugt122 + J32L*LDugt123;
      
      PDugt123 = J13L*LDugt121 + J23L*LDugt122 + J33L*LDugt123;
      
      PDugt131 = J11L*LDugt131 + J21L*LDugt132 + J31L*LDugt133;
      
      PDugt132 = J12L*LDugt131 + J22L*LDugt132 + J32L*LDugt133;
      
      PDugt133 = J13L*LDugt131 + J23L*LDugt132 + J33L*LDugt133;
      
      PDugt221 = J11L*LDugt221 + J21L*LDugt222 + J31L*LDugt223;
      
      PDugt222 = J12L*LDugt221 + J22L*LDugt222 + J32L*LDugt223;
      
      PDugt223 = J13L*LDugt221 + J23L*LDugt222 + J33L*LDugt223;
      
      PDugt231 = J11L*LDugt231 + J21L*LDugt232 + J31L*LDugt233;
      
      PDugt232 = J12L*LDugt231 + J22L*LDugt232 + J32L*LDugt233;
      
      PDugt233 = J13L*LDugt231 + J23L*LDugt232 + J33L*LDugt233;
      
      PDugt331 = J11L*LDugt331 + J21L*LDugt332 + J31L*LDugt333;
      
      PDugt332 = J12L*LDugt331 + J22L*LDugt332 + J32L*LDugt333;
      
      PDugt333 = J13L*LDugt331 + J23L*LDugt332 + J33L*LDugt333;
      
      PDutrK1 = J11L*LDutrK1 + J21L*LDutrK2 + J31L*LDutrK3;
      
      PDutrK2 = J12L*LDutrK1 + J22L*LDutrK2 + J32L*LDutrK3;
      
      PDutrK3 = J13L*LDutrK1 + J23L*LDutrK2 + J33L*LDutrK3;
      
      PDualpha1 = J11L*LDualpha1 + J21L*LDualpha2 + J31L*LDualpha3;
      
      PDualpha2 = J12L*LDualpha1 + J22L*LDualpha2 + J32L*LDualpha3;
      
      PDualpha3 = J13L*LDualpha1 + J23L*LDualpha2 + J33L*LDualpha3;
      
      PDuA1 = J11L*LDuA1 + J21L*LDuA2 + J31L*LDuA3;
      
      PDuA2 = J12L*LDuA1 + J22L*LDuA2 + J32L*LDuA3;
      
      PDuA3 = J13L*LDuA1 + J23L*LDuA2 + J33L*LDuA3;
    }
    else
    {
      PDuphiW1 = LDuphiW1;
      
      PDuphiW2 = LDuphiW2;
      
      PDuphiW3 = LDuphiW3;
      
      PDugt111 = LDugt111;
      
      PDugt112 = LDugt112;
      
      PDugt113 = LDugt113;
      
      PDugt121 = LDugt121;
      
      PDugt122 = LDugt122;
      
      PDugt123 = LDugt123;
      
      PDugt131 = LDugt131;
      
      PDugt132 = LDugt132;
      
      PDugt133 = LDugt133;
      
      PDugt221 = LDugt221;
      
      PDugt222 = LDugt222;
      
      PDugt223 = LDugt223;
      
      PDugt231 = LDugt231;
      
      PDugt232 = LDugt232;
      
      PDugt233 = LDugt233;
      
      PDugt331 = LDugt331;
      
      PDugt332 = LDugt332;
      
      PDugt333 = LDugt333;
      
      PDutrK1 = LDutrK1;
      
      PDutrK2 = LDutrK2;
      
      PDutrK3 = LDutrK3;
      
      PDualpha1 = LDualpha1;
      
      PDualpha2 = LDualpha2;
      
      PDualpha3 = LDualpha3;
      
      PDuA1 = LDuA1;
      
      PDuA2 = LDuA2;
      
      PDuA3 = LDuA3;
    }
    
    CCTK_REAL LDissphiW CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(phiW,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(phiW,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(phiW,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(phiW,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(phiW,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(phiW,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(phiW,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(phiW,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(phiW,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(phiW,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(phiW,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(phiW,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(phiW,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(phiW,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(phiW,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(phiW,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(phiW,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(phiW,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(phiW,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(phiW,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(phiW,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(phiW,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(phiW,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(phiW,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(phiW,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(phiW,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(phiW,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(phiW,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(phiW,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(phiW,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(phiW,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(phiW,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(phiW,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(phiW,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(phiW,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(phiW,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(phiW,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(phiW,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(phiW,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(phiW,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(phiW,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(phiW,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(phiW,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(phiW,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(phiW,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(phiW,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(phiW,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(phiW,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(phiW,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(phiW,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(phiW,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(phiW,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(phiW,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(phiW,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(phiW,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(phiW,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(phiW,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(phiW,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(phiW,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(phiW,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(phiW,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(phiW,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(phiW,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(phiW,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(phiW,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(phiW,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(phiW,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(phiW,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(phiW,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(phiW,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(phiW,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(phiW,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(phiW,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(phiW,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(phiW,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(phiW,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(phiW,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(phiW,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(phiW,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(phiW,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(phiW,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(phiW,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(phiW,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(phiW,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(phiW,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(phiW,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(phiW,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(phiW,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(phiW,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(phiW,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(phiW,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(phiW,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(phiW,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(phiW,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(phiW,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(phiW,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(phiW,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(phiW,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(phiW,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(phiW,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(phiW,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(phiW,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(phiW,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(phiW,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(phiW,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(phiW,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(phiW,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(phiW,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(phiW,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(phiW,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(phiW,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(phiW,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(phiW,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(phiW,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(phiW,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(phiW,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(phiW,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(phiW,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(phiW,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(phiW,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(phiW,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(phiW,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(phiW,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(phiW,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(phiW,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(phiW,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(phiW,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(phiW,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(phiW,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(phiW,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(phiW,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(phiW,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(phiW,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(phiW,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(phiW,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(phiW,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(phiW,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(phiW,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(phiW,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(phiW,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(phiW,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(phiW,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(phiW,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(phiW,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(phiW,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(phiW,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(phiW,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(phiW,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(phiW,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(phiW,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(phiW,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(phiW,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(phiW,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(phiW,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(phiW,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(phiW,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(phiW,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(phiW,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(phiW,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(phiW,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(phiW,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(phiW,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(phiW,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(phiW,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(phiW,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(phiW,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(phiW,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(phiW,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(phiW,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(phiW,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(phiW,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(phiW,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(phiW,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(phiW,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(phiW,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(phiW,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(phiW,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(phiW,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(phiW,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(phiW,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(phiW,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(phiW,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(phiW,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(phiW,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(phiW,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(phiW,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(phiW,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(phiW,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(phiW,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(phiW,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(phiW,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(phiW,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(phiW,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(phiW,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(phiW,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(phiW,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(phiW,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(phiW,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(phiW,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(phiW,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(phiW,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(phiW,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(phiW,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(phiW,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(phiW,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(phiW,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(phiW,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(phiW,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(phiW,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(phiW,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(phiW,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(phiW,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(phiW,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(phiW,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(phiW,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(phiW,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(phiW,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(phiW,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(phiW,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(phiW,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(phiW,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(phiW,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(phiW,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(phiW,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(phiW,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(phiW,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(phiW,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(phiW,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(phiW,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(phiW,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(phiW,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(phiW,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(phiW,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(phiW,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(phiW,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(phiW,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(phiW,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(phiW,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(phiW,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(phiW,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(phiW,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(phiW,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(phiW,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL GDissphiW CCTK_ATTRIBUTE_UNUSED = 
      IfThen(usejacobian,myDetJL*LDissphiW,LDissphiW);
    
    CCTK_REAL LDissgt11 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(gt11,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt11,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt11,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt11,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt11,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt11,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt11,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt11,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(gt11,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(gt11,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt11,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt11,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt11,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt11,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt11,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt11,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt11,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(gt11,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt11,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt11,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt11,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt11,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt11,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt11,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt11,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt11,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(gt11,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(gt11,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt11,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt11,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt11,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt11,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt11,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt11,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt11,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(gt11,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt11,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt11,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt11,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt11,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(gt11,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt11,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt11,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt11,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(gt11,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(gt11,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt11,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt11,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt11,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt11,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt11,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt11,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt11,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(gt11,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt11,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt11,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt11,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt11,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt11,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt11,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt11,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt11,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(gt11,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(gt11,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt11,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt11,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt11,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt11,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt11,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt11,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt11,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt11,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(gt11,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt11,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt11,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt11,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt11,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt11,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt11,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt11,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(gt11,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(gt11,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt11,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(gt11,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(gt11,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(gt11,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(gt11,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(gt11,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(gt11,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(gt11,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(gt11,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt11,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt11,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(gt11,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(gt11,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(gt11,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(gt11,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(gt11,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(gt11,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt11,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(gt11,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt11,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt11,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(gt11,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(gt11,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(gt11,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(gt11,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(gt11,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(gt11,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(gt11,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(gt11,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt11,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt11,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(gt11,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(gt11,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(gt11,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(gt11,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt11,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(gt11,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(gt11,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(gt11,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(gt11,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt11,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(gt11,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(gt11,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(gt11,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(gt11,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(gt11,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(gt11,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(gt11,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(gt11,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt11,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt11,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(gt11,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(gt11,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt11,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(gt11,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(gt11,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(gt11,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(gt11,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(gt11,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt11,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt11,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(gt11,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(gt11,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(gt11,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(gt11,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(gt11,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(gt11,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(gt11,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(gt11,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt11,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt11,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(gt11,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(gt11,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(gt11,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(gt11,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(gt11,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(gt11,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(gt11,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(gt11,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(gt11,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(gt11,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt11,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(gt11,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(gt11,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(gt11,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(gt11,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(gt11,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(gt11,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(gt11,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(gt11,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt11,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt11,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(gt11,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(gt11,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(gt11,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(gt11,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(gt11,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(gt11,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt11,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(gt11,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt11,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt11,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(gt11,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(gt11,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(gt11,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(gt11,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(gt11,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(gt11,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(gt11,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(gt11,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt11,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt11,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(gt11,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(gt11,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(gt11,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(gt11,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt11,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(gt11,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(gt11,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(gt11,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(gt11,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt11,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(gt11,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(gt11,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(gt11,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(gt11,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(gt11,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(gt11,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(gt11,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(gt11,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt11,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt11,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(gt11,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(gt11,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt11,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(gt11,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(gt11,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(gt11,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(gt11,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(gt11,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt11,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt11,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(gt11,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(gt11,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(gt11,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(gt11,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(gt11,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(gt11,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(gt11,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(gt11,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt11,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt11,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(gt11,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(gt11,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(gt11,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(gt11,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(gt11,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(gt11,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(gt11,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(gt11,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(gt11,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL LDissgt12 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(gt12,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt12,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt12,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt12,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt12,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt12,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt12,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt12,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(gt12,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(gt12,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt12,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt12,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt12,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt12,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt12,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt12,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt12,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(gt12,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt12,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt12,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt12,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt12,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt12,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt12,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt12,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt12,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(gt12,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(gt12,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt12,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt12,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt12,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt12,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt12,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt12,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt12,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(gt12,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt12,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt12,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt12,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt12,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(gt12,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt12,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt12,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt12,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(gt12,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(gt12,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt12,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt12,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt12,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt12,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt12,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt12,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt12,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(gt12,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt12,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt12,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt12,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt12,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt12,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt12,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt12,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt12,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(gt12,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(gt12,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt12,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt12,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt12,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt12,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt12,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt12,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt12,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt12,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(gt12,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt12,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt12,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt12,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt12,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt12,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt12,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt12,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(gt12,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(gt12,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt12,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(gt12,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(gt12,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(gt12,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(gt12,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(gt12,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(gt12,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(gt12,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(gt12,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt12,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt12,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(gt12,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(gt12,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(gt12,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(gt12,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(gt12,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(gt12,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt12,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(gt12,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt12,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt12,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(gt12,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(gt12,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(gt12,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(gt12,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(gt12,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(gt12,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(gt12,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(gt12,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt12,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt12,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(gt12,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(gt12,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(gt12,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(gt12,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt12,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(gt12,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(gt12,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(gt12,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(gt12,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt12,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(gt12,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(gt12,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(gt12,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(gt12,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(gt12,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(gt12,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(gt12,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(gt12,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt12,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt12,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(gt12,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(gt12,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt12,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(gt12,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(gt12,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(gt12,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(gt12,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(gt12,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt12,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt12,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(gt12,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(gt12,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(gt12,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(gt12,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(gt12,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(gt12,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(gt12,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(gt12,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt12,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt12,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(gt12,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(gt12,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(gt12,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(gt12,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(gt12,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(gt12,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(gt12,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(gt12,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(gt12,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(gt12,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt12,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(gt12,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(gt12,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(gt12,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(gt12,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(gt12,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(gt12,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(gt12,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(gt12,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt12,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt12,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(gt12,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(gt12,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(gt12,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(gt12,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(gt12,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(gt12,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt12,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(gt12,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt12,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt12,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(gt12,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(gt12,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(gt12,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(gt12,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(gt12,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(gt12,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(gt12,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(gt12,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt12,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt12,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(gt12,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(gt12,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(gt12,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(gt12,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt12,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(gt12,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(gt12,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(gt12,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(gt12,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt12,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(gt12,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(gt12,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(gt12,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(gt12,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(gt12,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(gt12,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(gt12,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(gt12,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt12,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt12,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(gt12,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(gt12,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt12,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(gt12,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(gt12,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(gt12,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(gt12,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(gt12,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt12,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt12,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(gt12,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(gt12,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(gt12,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(gt12,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(gt12,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(gt12,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(gt12,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(gt12,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt12,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt12,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(gt12,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(gt12,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(gt12,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(gt12,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(gt12,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(gt12,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(gt12,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(gt12,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(gt12,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL LDissgt13 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(gt13,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt13,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt13,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt13,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt13,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt13,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt13,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt13,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(gt13,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(gt13,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt13,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt13,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt13,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt13,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt13,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt13,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt13,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(gt13,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt13,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt13,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt13,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt13,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt13,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt13,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt13,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt13,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(gt13,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(gt13,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt13,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt13,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt13,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt13,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt13,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt13,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt13,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(gt13,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt13,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt13,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt13,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt13,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(gt13,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt13,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt13,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt13,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(gt13,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(gt13,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt13,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt13,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt13,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt13,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt13,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt13,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt13,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(gt13,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt13,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt13,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt13,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt13,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt13,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt13,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt13,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt13,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(gt13,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(gt13,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt13,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt13,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt13,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt13,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt13,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt13,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt13,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt13,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(gt13,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt13,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt13,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt13,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt13,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt13,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt13,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt13,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(gt13,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(gt13,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt13,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(gt13,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(gt13,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(gt13,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(gt13,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(gt13,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(gt13,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(gt13,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(gt13,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt13,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt13,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(gt13,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(gt13,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(gt13,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(gt13,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(gt13,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(gt13,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt13,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(gt13,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt13,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt13,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(gt13,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(gt13,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(gt13,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(gt13,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(gt13,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(gt13,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(gt13,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(gt13,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt13,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt13,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(gt13,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(gt13,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(gt13,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(gt13,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt13,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(gt13,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(gt13,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(gt13,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(gt13,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt13,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(gt13,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(gt13,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(gt13,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(gt13,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(gt13,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(gt13,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(gt13,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(gt13,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt13,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt13,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(gt13,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(gt13,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt13,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(gt13,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(gt13,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(gt13,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(gt13,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(gt13,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt13,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt13,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(gt13,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(gt13,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(gt13,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(gt13,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(gt13,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(gt13,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(gt13,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(gt13,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt13,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt13,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(gt13,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(gt13,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(gt13,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(gt13,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(gt13,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(gt13,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(gt13,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(gt13,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(gt13,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(gt13,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt13,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(gt13,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(gt13,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(gt13,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(gt13,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(gt13,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(gt13,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(gt13,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(gt13,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt13,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt13,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(gt13,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(gt13,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(gt13,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(gt13,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(gt13,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(gt13,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt13,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(gt13,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt13,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt13,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(gt13,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(gt13,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(gt13,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(gt13,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(gt13,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(gt13,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(gt13,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(gt13,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt13,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt13,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(gt13,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(gt13,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(gt13,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(gt13,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt13,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(gt13,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(gt13,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(gt13,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(gt13,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt13,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(gt13,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(gt13,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(gt13,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(gt13,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(gt13,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(gt13,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(gt13,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(gt13,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt13,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt13,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(gt13,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(gt13,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt13,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(gt13,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(gt13,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(gt13,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(gt13,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(gt13,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt13,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt13,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(gt13,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(gt13,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(gt13,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(gt13,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(gt13,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(gt13,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(gt13,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(gt13,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt13,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt13,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(gt13,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(gt13,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(gt13,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(gt13,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(gt13,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(gt13,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(gt13,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(gt13,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(gt13,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL LDissgt22 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(gt22,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt22,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt22,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt22,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt22,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt22,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt22,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt22,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(gt22,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(gt22,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt22,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt22,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt22,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt22,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt22,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt22,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt22,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(gt22,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt22,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt22,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt22,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt22,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt22,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt22,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt22,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt22,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(gt22,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(gt22,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt22,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt22,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt22,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt22,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt22,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt22,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt22,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(gt22,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt22,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt22,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt22,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt22,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(gt22,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt22,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt22,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt22,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(gt22,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(gt22,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt22,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt22,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt22,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt22,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt22,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt22,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt22,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(gt22,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt22,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt22,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt22,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt22,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt22,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt22,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt22,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt22,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(gt22,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(gt22,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt22,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt22,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt22,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt22,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt22,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt22,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt22,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt22,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(gt22,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt22,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt22,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt22,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt22,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt22,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt22,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt22,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(gt22,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(gt22,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt22,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(gt22,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(gt22,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(gt22,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(gt22,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(gt22,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(gt22,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(gt22,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(gt22,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt22,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt22,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(gt22,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(gt22,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(gt22,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(gt22,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(gt22,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(gt22,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt22,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(gt22,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt22,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt22,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(gt22,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(gt22,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(gt22,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(gt22,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(gt22,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(gt22,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(gt22,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(gt22,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt22,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt22,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(gt22,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(gt22,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(gt22,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(gt22,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt22,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(gt22,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(gt22,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(gt22,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(gt22,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt22,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(gt22,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(gt22,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(gt22,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(gt22,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(gt22,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(gt22,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(gt22,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(gt22,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt22,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt22,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(gt22,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(gt22,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt22,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(gt22,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(gt22,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(gt22,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(gt22,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(gt22,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt22,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt22,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(gt22,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(gt22,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(gt22,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(gt22,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(gt22,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(gt22,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(gt22,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(gt22,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt22,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt22,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(gt22,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(gt22,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(gt22,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(gt22,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(gt22,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(gt22,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(gt22,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(gt22,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(gt22,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(gt22,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt22,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(gt22,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(gt22,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(gt22,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(gt22,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(gt22,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(gt22,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(gt22,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(gt22,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt22,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt22,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(gt22,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(gt22,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(gt22,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(gt22,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(gt22,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(gt22,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt22,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(gt22,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt22,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt22,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(gt22,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(gt22,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(gt22,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(gt22,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(gt22,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(gt22,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(gt22,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(gt22,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt22,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt22,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(gt22,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(gt22,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(gt22,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(gt22,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt22,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(gt22,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(gt22,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(gt22,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(gt22,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt22,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(gt22,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(gt22,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(gt22,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(gt22,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(gt22,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(gt22,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(gt22,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(gt22,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt22,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt22,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(gt22,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(gt22,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt22,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(gt22,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(gt22,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(gt22,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(gt22,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(gt22,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt22,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt22,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(gt22,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(gt22,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(gt22,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(gt22,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(gt22,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(gt22,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(gt22,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(gt22,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt22,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt22,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(gt22,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(gt22,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(gt22,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(gt22,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(gt22,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(gt22,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(gt22,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(gt22,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(gt22,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL LDissgt23 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(gt23,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt23,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt23,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt23,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt23,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt23,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt23,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt23,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(gt23,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(gt23,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt23,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt23,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt23,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt23,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt23,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt23,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt23,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(gt23,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt23,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt23,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt23,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt23,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt23,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt23,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt23,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt23,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(gt23,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(gt23,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt23,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt23,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt23,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt23,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt23,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt23,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt23,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(gt23,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt23,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt23,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt23,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt23,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(gt23,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt23,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt23,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt23,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(gt23,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(gt23,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt23,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt23,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt23,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt23,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt23,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt23,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt23,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(gt23,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt23,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt23,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt23,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt23,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt23,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt23,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt23,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt23,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(gt23,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(gt23,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt23,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt23,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt23,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt23,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt23,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt23,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt23,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt23,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(gt23,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt23,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt23,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt23,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt23,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt23,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt23,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt23,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(gt23,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(gt23,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt23,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(gt23,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(gt23,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(gt23,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(gt23,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(gt23,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(gt23,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(gt23,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(gt23,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt23,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt23,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(gt23,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(gt23,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(gt23,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(gt23,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(gt23,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(gt23,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt23,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(gt23,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt23,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt23,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(gt23,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(gt23,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(gt23,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(gt23,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(gt23,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(gt23,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(gt23,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(gt23,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt23,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt23,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(gt23,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(gt23,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(gt23,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(gt23,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt23,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(gt23,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(gt23,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(gt23,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(gt23,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt23,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(gt23,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(gt23,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(gt23,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(gt23,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(gt23,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(gt23,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(gt23,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(gt23,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt23,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt23,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(gt23,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(gt23,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt23,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(gt23,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(gt23,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(gt23,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(gt23,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(gt23,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt23,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt23,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(gt23,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(gt23,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(gt23,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(gt23,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(gt23,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(gt23,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(gt23,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(gt23,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt23,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt23,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(gt23,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(gt23,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(gt23,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(gt23,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(gt23,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(gt23,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(gt23,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(gt23,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(gt23,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(gt23,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt23,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(gt23,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(gt23,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(gt23,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(gt23,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(gt23,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(gt23,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(gt23,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(gt23,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt23,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt23,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(gt23,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(gt23,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(gt23,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(gt23,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(gt23,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(gt23,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt23,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(gt23,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt23,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt23,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(gt23,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(gt23,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(gt23,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(gt23,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(gt23,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(gt23,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(gt23,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(gt23,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt23,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt23,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(gt23,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(gt23,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(gt23,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(gt23,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt23,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(gt23,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(gt23,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(gt23,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(gt23,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt23,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(gt23,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(gt23,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(gt23,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(gt23,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(gt23,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(gt23,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(gt23,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(gt23,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt23,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt23,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(gt23,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(gt23,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt23,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(gt23,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(gt23,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(gt23,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(gt23,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(gt23,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt23,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt23,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(gt23,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(gt23,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(gt23,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(gt23,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(gt23,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(gt23,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(gt23,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(gt23,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt23,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt23,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(gt23,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(gt23,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(gt23,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(gt23,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(gt23,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(gt23,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(gt23,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(gt23,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(gt23,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL LDissgt33 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(gt33,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt33,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt33,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt33,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt33,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt33,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt33,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt33,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(gt33,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(gt33,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt33,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt33,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt33,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt33,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt33,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt33,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt33,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(gt33,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt33,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt33,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt33,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt33,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt33,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt33,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt33,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt33,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(gt33,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(gt33,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt33,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt33,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt33,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt33,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt33,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt33,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt33,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(gt33,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt33,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt33,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt33,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt33,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(gt33,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt33,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(gt33,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(gt33,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(gt33,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(gt33,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(gt33,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(gt33,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(gt33,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt33,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(gt33,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt33,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(gt33,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(gt33,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt33,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(gt33,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(gt33,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(gt33,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(gt33,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt33,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(gt33,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt33,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(gt33,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(gt33,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(gt33,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(gt33,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(gt33,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(gt33,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(gt33,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt33,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(gt33,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt33,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(gt33,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(gt33,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(gt33,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(gt33,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(gt33,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(gt33,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(gt33,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt33,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(gt33,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(gt33,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt33,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(gt33,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(gt33,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(gt33,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(gt33,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(gt33,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(gt33,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(gt33,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(gt33,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt33,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt33,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(gt33,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(gt33,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(gt33,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(gt33,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(gt33,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(gt33,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt33,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(gt33,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt33,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt33,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(gt33,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(gt33,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(gt33,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(gt33,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(gt33,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(gt33,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(gt33,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(gt33,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt33,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt33,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(gt33,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(gt33,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(gt33,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(gt33,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt33,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(gt33,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(gt33,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(gt33,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(gt33,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt33,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(gt33,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(gt33,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(gt33,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(gt33,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(gt33,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(gt33,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(gt33,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(gt33,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(gt33,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt33,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(gt33,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(gt33,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt33,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(gt33,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(gt33,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(gt33,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(gt33,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(gt33,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(gt33,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt33,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(gt33,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(gt33,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(gt33,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(gt33,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(gt33,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(gt33,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(gt33,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(gt33,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(gt33,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt33,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(gt33,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(gt33,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(gt33,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(gt33,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(gt33,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(gt33,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(gt33,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(gt33,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(gt33,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(gt33,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(gt33,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(gt33,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(gt33,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(gt33,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(gt33,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(gt33,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(gt33,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(gt33,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(gt33,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt33,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(gt33,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(gt33,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(gt33,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(gt33,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(gt33,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(gt33,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(gt33,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(gt33,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(gt33,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt33,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(gt33,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(gt33,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(gt33,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(gt33,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(gt33,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(gt33,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(gt33,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(gt33,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(gt33,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt33,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(gt33,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(gt33,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(gt33,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(gt33,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(gt33,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(gt33,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(gt33,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(gt33,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(gt33,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(gt33,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(gt33,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(gt33,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(gt33,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(gt33,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(gt33,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(gt33,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(gt33,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(gt33,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(gt33,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(gt33,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(gt33,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(gt33,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(gt33,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(gt33,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(gt33,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(gt33,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(gt33,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(gt33,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(gt33,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(gt33,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(gt33,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(gt33,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(gt33,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(gt33,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(gt33,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(gt33,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(gt33,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(gt33,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(gt33,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(gt33,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(gt33,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(gt33,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(gt33,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(gt33,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(gt33,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(gt33,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(gt33,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(gt33,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(gt33,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(gt33,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL GDissgt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissgt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissgt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissgt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissgt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissgt33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      GDissgt11 = myDetJL*LDissgt11;
      
      GDissgt12 = myDetJL*LDissgt12;
      
      GDissgt13 = myDetJL*LDissgt13;
      
      GDissgt22 = myDetJL*LDissgt22;
      
      GDissgt23 = myDetJL*LDissgt23;
      
      GDissgt33 = myDetJL*LDissgt33;
    }
    else
    {
      GDissgt11 = LDissgt11;
      
      GDissgt12 = LDissgt12;
      
      GDissgt13 = LDissgt13;
      
      GDissgt22 = LDissgt22;
      
      GDissgt23 = LDissgt23;
      
      GDissgt33 = LDissgt33;
    }
    
    CCTK_REAL LDissalpha CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(alpha,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(alpha,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(alpha,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(alpha,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(alpha,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(alpha,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(alpha,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(alpha,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(alpha,8,0,0),0) + IfThen(ti 
      == 1,0.0459542830207730700182429562579*GFOffset(alpha,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(alpha,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(alpha,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(alpha,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(alpha,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(alpha,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(alpha,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(alpha,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(alpha,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(alpha,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(alpha,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(alpha,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(alpha,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(alpha,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(alpha,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(alpha,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(alpha,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(alpha,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(alpha,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(alpha,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(alpha,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(alpha,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(alpha,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(alpha,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(alpha,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(alpha,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(alpha,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(alpha,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(alpha,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(alpha,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(alpha,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(alpha,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(alpha,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(alpha,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(alpha,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(alpha,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(alpha,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(alpha,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(alpha,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(alpha,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(alpha,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(alpha,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(alpha,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(alpha,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(alpha,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(alpha,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(alpha,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(alpha,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(alpha,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(alpha,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(alpha,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(alpha,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(alpha,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(alpha,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(alpha,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(alpha,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(alpha,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(alpha,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(alpha,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(alpha,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(alpha,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(alpha,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(alpha,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(alpha,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(alpha,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(alpha,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(alpha,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(alpha,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(alpha,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(alpha,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(alpha,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(alpha,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(alpha,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(alpha,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(alpha,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(alpha,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(alpha,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(alpha,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(alpha,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(alpha,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(alpha,0,8,0),0) + IfThen(tj 
      == 1,0.0459542830207730700182429562579*GFOffset(alpha,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(alpha,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(alpha,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(alpha,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(alpha,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(alpha,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(alpha,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(alpha,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(alpha,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(alpha,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(alpha,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(alpha,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(alpha,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(alpha,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(alpha,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(alpha,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(alpha,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(alpha,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(alpha,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(alpha,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(alpha,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(alpha,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(alpha,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(alpha,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(alpha,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(alpha,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(alpha,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(alpha,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(alpha,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(alpha,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(alpha,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(alpha,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(alpha,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(alpha,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(alpha,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(alpha,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(alpha,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(alpha,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(alpha,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(alpha,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(alpha,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(alpha,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(alpha,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(alpha,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(alpha,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(alpha,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(alpha,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(alpha,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(alpha,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(alpha,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(alpha,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(alpha,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(alpha,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(alpha,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(alpha,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(alpha,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(alpha,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(alpha,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(alpha,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(alpha,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(alpha,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(alpha,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(alpha,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(alpha,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(alpha,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(alpha,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(alpha,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(alpha,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(alpha,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(alpha,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(alpha,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(alpha,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(alpha,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(alpha,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(alpha,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(alpha,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(alpha,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(alpha,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(alpha,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(alpha,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(alpha,0,0,8),0) + IfThen(tk 
      == 1,0.0459542830207730700182429562579*GFOffset(alpha,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(alpha,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(alpha,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(alpha,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(alpha,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(alpha,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(alpha,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(alpha,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(alpha,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(alpha,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(alpha,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(alpha,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(alpha,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(alpha,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(alpha,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(alpha,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(alpha,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(alpha,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(alpha,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(alpha,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(alpha,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(alpha,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(alpha,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(alpha,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(alpha,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(alpha,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(alpha,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(alpha,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(alpha,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(alpha,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(alpha,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(alpha,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(alpha,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(alpha,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(alpha,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(alpha,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(alpha,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(alpha,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(alpha,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(alpha,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(alpha,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(alpha,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(alpha,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(alpha,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(alpha,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(alpha,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(alpha,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(alpha,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(alpha,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(alpha,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(alpha,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(alpha,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(alpha,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(alpha,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(alpha,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(alpha,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(alpha,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(alpha,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(alpha,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(alpha,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(alpha,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(alpha,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(alpha,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(alpha,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(alpha,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(alpha,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(alpha,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(alpha,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(alpha,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(alpha,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(alpha,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(alpha,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL GDissalpha CCTK_ATTRIBUTE_UNUSED = 
      IfThen(usejacobian,myDetJL*LDissalpha,LDissalpha);
    
    CCTK_REAL LDissA CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(A,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(A,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(A,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(A,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(A,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(A,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(A,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(A,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(A,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(A,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(A,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(A,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(A,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(A,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(A,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(A,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(A,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(A,7,0,0),0) + IfThen(ti == 
      2,-0.0355960467027073641809745493856*GFOffset(A,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(A,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(A,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(A,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(A,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(A,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(A,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(A,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(A,6,0,0),0) + IfThen(ti == 
      3,0.0315835488689294754585658103387*GFOffset(A,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(A,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(A,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(A,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(A,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(A,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(A,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(A,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(A,5,0,0),0) + IfThen(ti == 
      4,-0.0303817292054494222005208333333*GFOffset(A,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(A,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(A,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(A,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(A,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(A,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(A,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(A,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(A,4,0,0),0) + IfThen(ti == 
      5,0.0313420135921978606262758413179*GFOffset(A,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(A,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(A,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(A,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(A,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(A,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(A,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(A,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(A,3,0,0),0) + IfThen(ti == 
      6,-0.0350900512642925789585489836954*GFOffset(A,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(A,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(A,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(A,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(A,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(A,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(A,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(A,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(A,2,0,0),0) + IfThen(ti == 
      7,0.0450883724317016003377209764486*GFOffset(A,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(A,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(A,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(A,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(A,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(A,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(A,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(A,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(A,1,0,0),0) + IfThen(ti == 
      8,-0.109937276933326049604349666172*GFOffset(A,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(A,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(A,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(A,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(A,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(A,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(A,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(A,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(A,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(A,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(A,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(A,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(A,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(A,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(A,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(A,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(A,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(A,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(A,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(A,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(A,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(A,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(A,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(A,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(A,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(A,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(A,0,7,0),0) + IfThen(tj == 
      2,-0.0355960467027073641809745493856*GFOffset(A,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(A,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(A,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(A,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(A,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(A,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(A,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(A,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(A,0,6,0),0) + IfThen(tj == 
      3,0.0315835488689294754585658103387*GFOffset(A,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(A,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(A,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(A,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(A,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(A,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(A,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(A,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(A,0,5,0),0) + IfThen(tj == 
      4,-0.0303817292054494222005208333333*GFOffset(A,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(A,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(A,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(A,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(A,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(A,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(A,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(A,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(A,0,4,0),0) + IfThen(tj == 
      5,0.0313420135921978606262758413179*GFOffset(A,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(A,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(A,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(A,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(A,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(A,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(A,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(A,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(A,0,3,0),0) + IfThen(tj == 
      6,-0.0350900512642925789585489836954*GFOffset(A,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(A,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(A,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(A,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(A,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(A,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(A,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(A,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(A,0,2,0),0) + IfThen(tj == 
      7,0.0450883724317016003377209764486*GFOffset(A,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(A,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(A,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(A,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(A,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(A,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(A,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(A,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(A,0,1,0),0) + IfThen(tj == 
      8,-0.109937276933326049604349666172*GFOffset(A,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(A,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(A,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(A,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(A,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(A,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(A,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(A,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(A,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(A,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(A,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(A,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(A,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(A,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(A,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(A,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(A,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(A,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(A,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(A,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(A,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(A,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(A,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(A,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(A,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(A,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(A,0,0,7),0) + IfThen(tk == 
      2,-0.0355960467027073641809745493856*GFOffset(A,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(A,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(A,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(A,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(A,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(A,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(A,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(A,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(A,0,0,6),0) + IfThen(tk == 
      3,0.0315835488689294754585658103387*GFOffset(A,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(A,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(A,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(A,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(A,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(A,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(A,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(A,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(A,0,0,5),0) + IfThen(tk == 
      4,-0.0303817292054494222005208333333*GFOffset(A,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(A,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(A,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(A,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(A,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(A,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(A,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(A,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(A,0,0,4),0) + IfThen(tk == 
      5,0.0313420135921978606262758413179*GFOffset(A,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(A,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(A,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(A,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(A,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(A,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(A,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(A,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(A,0,0,3),0) + IfThen(tk == 
      6,-0.0350900512642925789585489836954*GFOffset(A,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(A,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(A,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(A,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(A,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(A,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(A,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(A,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(A,0,0,2),0) + IfThen(tk == 
      7,0.0450883724317016003377209764486*GFOffset(A,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(A,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(A,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(A,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(A,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(A,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(A,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(A,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(A,0,0,1),0) + IfThen(tk == 
      8,-0.109937276933326049604349666172*GFOffset(A,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(A,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(A,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(A,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(A,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(A,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(A,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(A,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(A,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL GDissA CCTK_ATTRIBUTE_UNUSED = 
      IfThen(usejacobian,myDetJL*LDissA,LDissA);
    
    CCTK_REAL detgt CCTK_ATTRIBUTE_UNUSED = 1;
    
    CCTK_REAL gtu11 CCTK_ATTRIBUTE_UNUSED = (gt22L*gt33L - 
      pow(gt23L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu12 CCTK_ATTRIBUTE_UNUSED = (gt13L*gt23L - 
      gt12L*gt33L)*pow(detgt,-1);
    
    CCTK_REAL gtu13 CCTK_ATTRIBUTE_UNUSED = (-(gt13L*gt22L) + 
      gt12L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu22 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt33L - 
      pow(gt13L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu23 CCTK_ATTRIBUTE_UNUSED = (gt12L*gt13L - 
      gt11L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu33 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt22L - 
      pow(gt12L,2))*pow(detgt,-1);
    
    CCTK_REAL Gtl111 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt111L;
    
    CCTK_REAL Gtl112 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt112L;
    
    CCTK_REAL Gtl113 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt113L;
    
    CCTK_REAL Gtl122 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt122L - PDgt221L);
    
    CCTK_REAL Gtl123 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt123L + PDgt132L - 
      PDgt231L);
    
    CCTK_REAL Gtl133 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt133L - PDgt331L);
    
    CCTK_REAL Gtl211 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt112L + 2*PDgt121L);
    
    CCTK_REAL Gtl212 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt221L;
    
    CCTK_REAL Gtl213 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt123L - PDgt132L + 
      PDgt231L);
    
    CCTK_REAL Gtl222 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt222L;
    
    CCTK_REAL Gtl223 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt223L;
    
    CCTK_REAL Gtl233 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt233L - PDgt332L);
    
    CCTK_REAL Gtl311 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt113L + 2*PDgt131L);
    
    CCTK_REAL Gtl312 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt123L + PDgt132L + 
      PDgt231L);
    
    CCTK_REAL Gtl313 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt331L;
    
    CCTK_REAL Gtl322 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt223L + 2*PDgt232L);
    
    CCTK_REAL Gtl323 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt332L;
    
    CCTK_REAL Gtl333 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt333L;
    
    CCTK_REAL Gt111 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu11 + Gtl211*gtu12 + 
      Gtl311*gtu13;
    
    CCTK_REAL Gt211 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu12 + Gtl211*gtu22 + 
      Gtl311*gtu23;
    
    CCTK_REAL Gt311 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu13 + Gtl211*gtu23 + 
      Gtl311*gtu33;
    
    CCTK_REAL Gt112 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu11 + Gtl212*gtu12 + 
      Gtl312*gtu13;
    
    CCTK_REAL Gt212 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu12 + Gtl212*gtu22 + 
      Gtl312*gtu23;
    
    CCTK_REAL Gt312 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu13 + Gtl212*gtu23 + 
      Gtl312*gtu33;
    
    CCTK_REAL Gt113 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu11 + Gtl213*gtu12 + 
      Gtl313*gtu13;
    
    CCTK_REAL Gt213 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu12 + Gtl213*gtu22 + 
      Gtl313*gtu23;
    
    CCTK_REAL Gt313 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu13 + Gtl213*gtu23 + 
      Gtl313*gtu33;
    
    CCTK_REAL Gt122 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu11 + Gtl222*gtu12 + 
      Gtl322*gtu13;
    
    CCTK_REAL Gt222 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu12 + Gtl222*gtu22 + 
      Gtl322*gtu23;
    
    CCTK_REAL Gt322 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu13 + Gtl222*gtu23 + 
      Gtl322*gtu33;
    
    CCTK_REAL Gt123 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu11 + Gtl223*gtu12 + 
      Gtl323*gtu13;
    
    CCTK_REAL Gt223 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu12 + Gtl223*gtu22 + 
      Gtl323*gtu23;
    
    CCTK_REAL Gt323 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu13 + Gtl223*gtu23 + 
      Gtl323*gtu33;
    
    CCTK_REAL Gt133 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu11 + Gtl233*gtu12 + 
      Gtl333*gtu13;
    
    CCTK_REAL Gt233 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu12 + Gtl233*gtu22 + 
      Gtl333*gtu23;
    
    CCTK_REAL Gt333 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu13 + Gtl233*gtu23 + 
      Gtl333*gtu33;
    
    CCTK_REAL em4phi CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,pow(phiWL,2),exp(-4*phiWL));
    
    CCTK_REAL gu11 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu11;
    
    CCTK_REAL gu12 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu12;
    
    CCTK_REAL gu13 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu13;
    
    CCTK_REAL gu22 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu22;
    
    CCTK_REAL gu23 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu23;
    
    CCTK_REAL gu33 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu33;
    
    CCTK_REAL Xtn1 CCTK_ATTRIBUTE_UNUSED = Gt111*gtu11 + 2*Gt112*gtu12 + 
      2*Gt113*gtu13 + Gt122*gtu22 + 2*Gt123*gtu23 + Gt133*gtu33;
    
    CCTK_REAL Xtn2 CCTK_ATTRIBUTE_UNUSED = Gt211*gtu11 + 2*Gt212*gtu12 + 
      2*Gt213*gtu13 + Gt222*gtu22 + 2*Gt223*gtu23 + Gt233*gtu33;
    
    CCTK_REAL Xtn3 CCTK_ATTRIBUTE_UNUSED = Gt311*gtu11 + 2*Gt312*gtu12 + 
      2*Gt313*gtu13 + Gt322*gtu22 + 2*Gt323*gtu23 + Gt333*gtu33;
    
    CCTK_REAL fac1 CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,-0.5*pow(phiWL,-1),1);
    
    CCTK_REAL cdphi1 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*fac1;
    
    CCTK_REAL cdphi2 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*fac1;
    
    CCTK_REAL cdphi3 CCTK_ATTRIBUTE_UNUSED = PDphiW3L*fac1;
    
    CCTK_REAL Atm11 CCTK_ATTRIBUTE_UNUSED = At11L*gtu11 + At12L*gtu12 + 
      At13L*gtu13;
    
    CCTK_REAL Atm21 CCTK_ATTRIBUTE_UNUSED = At11L*gtu12 + At12L*gtu22 + 
      At13L*gtu23;
    
    CCTK_REAL Atm31 CCTK_ATTRIBUTE_UNUSED = At11L*gtu13 + At12L*gtu23 + 
      At13L*gtu33;
    
    CCTK_REAL Atm12 CCTK_ATTRIBUTE_UNUSED = At12L*gtu11 + At22L*gtu12 + 
      At23L*gtu13;
    
    CCTK_REAL Atm22 CCTK_ATTRIBUTE_UNUSED = At12L*gtu12 + At22L*gtu22 + 
      At23L*gtu23;
    
    CCTK_REAL Atm32 CCTK_ATTRIBUTE_UNUSED = At12L*gtu13 + At22L*gtu23 + 
      At23L*gtu33;
    
    CCTK_REAL Atm13 CCTK_ATTRIBUTE_UNUSED = At13L*gtu11 + At23L*gtu12 + 
      At33L*gtu13;
    
    CCTK_REAL Atm23 CCTK_ATTRIBUTE_UNUSED = At13L*gtu12 + At23L*gtu22 + 
      At33L*gtu23;
    
    CCTK_REAL Atm33 CCTK_ATTRIBUTE_UNUSED = At13L*gtu13 + At23L*gtu23 + 
      At33L*gtu33;
    
    CCTK_REAL rho CCTK_ATTRIBUTE_UNUSED = (eTttL - 2*(beta1L*eTtxL + 
      beta2L*eTtyL + beta3L*eTtzL) + beta1L*(beta1L*eTxxL + beta2L*eTxyL + 
      beta3L*eTxzL) + beta2L*(beta1L*eTxyL + beta2L*eTyyL + beta3L*eTyzL) + 
      beta3L*(beta1L*eTxzL + beta2L*eTyzL + beta3L*eTzzL))*pow(alphaL,-2);
    
    CCTK_REAL trS CCTK_ATTRIBUTE_UNUSED = eTxxL*gu11 + 2*eTxyL*gu12 + 
      2*eTxzL*gu13 + eTyyL*gu22 + 2*eTyzL*gu23 + eTzzL*gu33;
    
    CCTK_REAL phiWrhsL CCTK_ATTRIBUTE_UNUSED = GDissphiW + beta1L*PDuphiW1 
      + beta2L*PDuphiW2 + beta3L*PDuphiW3 + (-PDbeta11L - PDbeta22L - 
      PDbeta33L + alphaL*trKL)*IfThen(conformalMethod != 
      0,0.333333333333333333333333333333*phiWL,-0.166666666666666666666666666667);
    
    CCTK_REAL gt11rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At11L + 
      2*gt11L*PDbeta11L + 2*gt12L*PDbeta21L + 2*gt13L*PDbeta31L - 
      0.666666666666666666666666666667*gt11L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt11 + beta1L*PDugt111 + beta2L*PDugt112 + 
      beta3L*PDugt113;
    
    CCTK_REAL gt12rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At12L + 
      gt12L*PDbeta11L + gt11L*PDbeta12L + gt22L*PDbeta21L + gt12L*PDbeta22L + 
      gt23L*PDbeta31L + gt13L*PDbeta32L - 
      0.666666666666666666666666666667*gt12L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt12 + beta1L*PDugt121 + beta2L*PDugt122 + 
      beta3L*PDugt123;
    
    CCTK_REAL gt13rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At13L + 
      gt13L*PDbeta11L + gt11L*PDbeta13L + gt23L*PDbeta21L + gt12L*PDbeta23L + 
      gt33L*PDbeta31L + gt13L*PDbeta33L - 
      0.666666666666666666666666666667*gt13L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt13 + beta1L*PDugt131 + beta2L*PDugt132 + 
      beta3L*PDugt133;
    
    CCTK_REAL gt22rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At22L + 
      2*gt12L*PDbeta12L + 2*gt22L*PDbeta22L + 2*gt23L*PDbeta32L - 
      0.666666666666666666666666666667*gt22L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt22 + beta1L*PDugt221 + beta2L*PDugt222 + 
      beta3L*PDugt223;
    
    CCTK_REAL gt23rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At23L + 
      gt13L*PDbeta12L + gt12L*PDbeta13L + gt23L*PDbeta22L + gt22L*PDbeta23L + 
      gt33L*PDbeta32L + gt23L*PDbeta33L - 
      0.666666666666666666666666666667*gt23L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt23 + beta1L*PDugt231 + beta2L*PDugt232 + 
      beta3L*PDugt233;
    
    CCTK_REAL gt33rhsL CCTK_ATTRIBUTE_UNUSED = -2*alphaL*At33L + 
      2*gt13L*PDbeta13L + 2*gt23L*PDbeta23L + 2*gt33L*PDbeta33L - 
      0.666666666666666666666666666667*gt33L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + GDissgt33 + beta1L*PDugt331 + beta2L*PDugt332 + 
      beta3L*PDugt333;
    
    CCTK_REAL dottrK CCTK_ATTRIBUTE_UNUSED = 4*alphaL*Pi*(rho + trS) - 
      em4phi*(gtu11*(2*PDalpha1L*cdphi1 + PDPDalpha11) + 
      gtu12*(2*PDalpha2L*cdphi1 + PDPDalpha12) + gtu13*(2*PDalpha3L*cdphi1 + 
      PDPDalpha13) + gtu12*(2*PDalpha1L*cdphi2 + PDPDalpha21) + 
      gtu22*(2*PDalpha2L*cdphi2 + PDPDalpha22) + gtu23*(2*PDalpha3L*cdphi2 + 
      PDPDalpha23) + gtu13*(2*PDalpha1L*cdphi3 + PDPDalpha31) + 
      gtu23*(2*PDalpha2L*cdphi3 + PDPDalpha32) + gtu33*(2*PDalpha3L*cdphi3 + 
      PDPDalpha33) - PDalpha1L*Xtn1 - PDalpha2L*Xtn2 - PDalpha3L*Xtn3) + 
      alphaL*(2*Atm12*Atm21 + 2*Atm13*Atm31 + 2*Atm23*Atm32 + 
      0.333333333333333333333333333333*pow(trKL,2) + pow(Atm11,2) + 
      pow(Atm22,2) + pow(Atm33,2));
    
    CCTK_REAL dotalpha CCTK_ATTRIBUTE_UNUSED = -(harmonicF*IfThen(evolveA 
      != 0,AL,trKL + (-1 + alphaL)*alphaDriver)*pow(alphaL,harmonicN));
    
    CCTK_REAL alpharhsL CCTK_ATTRIBUTE_UNUSED = dotalpha + GDissalpha + 
      IfThen(advectLapse != 0,beta1L*PDualpha1 + beta2L*PDualpha2 + 
      beta3L*PDualpha3,0);
    
    CCTK_REAL ArhsL CCTK_ATTRIBUTE_UNUSED = IfThen(evolveA != 0,dottrK + 
      GDissA + IfThen(fixAdvectionTerms == 0 && advectLapse != 0,beta1L*PDuA1 
      + beta2L*PDuA2 + beta3L*PDuA3,0) - alphaDriver*(AL + 
      IfThen(fixAdvectionTerms != 0 && advectLapse != 0,-((beta1L*PDualpha1 + 
      beta2L*PDualpha2 + 
      beta3L*PDualpha3)*pow(alphaL,-harmonicN)*pow(harmonicF,-1)),0)) + 
      IfThen(fixAdvectionTerms != 0,beta1L*PDutrK1 + beta2L*PDutrK2 + 
      beta3L*PDutrK3,0),0);
    /* Copy local copies back to grid functions */
    alpharhs[index] = alpharhsL;
    Arhs[index] = ArhsL;
    gt11rhs[index] = gt11rhsL;
    gt12rhs[index] = gt12rhsL;
    gt13rhs[index] = gt13rhsL;
    gt22rhs[index] = gt22rhsL;
    gt23rhs[index] = gt23rhsL;
    gt33rhs[index] = gt33rhsL;
    phiWrhs[index] = phiWrhsL;
  }
  CCTK_ENDLOOP3(ML_BSSN_DG8_EvolutionInteriorSplit31);
}
extern "C" void ML_BSSN_DG8_EvolutionInteriorSplit31(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_DG8_EvolutionInteriorSplit31_Body");
  }
  if (cctk_iteration % ML_BSSN_DG8_EvolutionInteriorSplit31_calc_every != ML_BSSN_DG8_EvolutionInteriorSplit31_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_BSSN_DG8::ML_confac",
    "ML_BSSN_DG8::ML_confacrhs",
    "ML_BSSN_DG8::ML_curv",
    "ML_BSSN_DG8::ML_dconfac",
    "ML_BSSN_DG8::ML_dlapse",
    "ML_BSSN_DG8::ML_dmetric",
    "ML_BSSN_DG8::ML_dshift",
    "ML_BSSN_DG8::ML_dtlapse",
    "ML_BSSN_DG8::ML_dtlapserhs",
    "ML_BSSN_DG8::ML_lapse",
    "ML_BSSN_DG8::ML_lapserhs",
    "ML_BSSN_DG8::ML_metric",
    "ML_BSSN_DG8::ML_metricrhs",
    "ML_BSSN_DG8::ML_shift",
    "ML_BSSN_DG8::ML_trace_curv",
    "ML_BSSN_DG8::ML_volume_form"};
  AssertGroupStorage(cctkGH, "ML_BSSN_DG8_EvolutionInteriorSplit31", 16, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_BSSN_DG8_EvolutionInteriorSplit31_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_DG8_EvolutionInteriorSplit31_Body");
  }
}

} // namespace ML_BSSN_DG8
