/*  File produced by Kranc */

#define KRANC_C

#include <algorithm>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Kranc.hh"
#include "Differencing.h"

namespace ML_BSSN_DG8 {

extern "C" void ML_BSSN_DG8_EvolutionInteriorSplit33_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_BSSN_DG8_EvolutionInteriorSplit33_calc_every != ML_BSSN_DG8_EvolutionInteriorSplit33_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_BSSN_DG8::ML_curvrhs","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_BSSN_DG8::ML_curvrhs.");
  return;
}

static void ML_BSSN_DG8_EvolutionInteriorSplit33_Body(const cGH* restrict const cctkGH, const KrancData & restrict kd)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  const int dir CCTK_ATTRIBUTE_UNUSED = kd.dir;
  const int face CCTK_ATTRIBUTE_UNUSED = kd.face;
  const int imin[3] = {std::max(kd.imin[0], kd.tile_imin[0]),
                       std::max(kd.imin[1], kd.tile_imin[1]),
                       std::max(kd.imin[2], kd.tile_imin[2])};
  const int imax[3] = {std::min(kd.imax[0], kd.tile_imax[0]),
                       std::min(kd.imax[1], kd.tile_imax[1]),
                       std::min(kd.imax[2], kd.tile_imax[2])};
  /* Include user-supplied include files */
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = 
    CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const ptrdiff_t cctkLbnd1 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[0];
  const ptrdiff_t cctkLbnd2 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[1];
  const ptrdiff_t cctkLbnd3 CCTK_ATTRIBUTE_UNUSED = cctk_lbnd[2];
  const CCTK_REAL t CCTK_ATTRIBUTE_UNUSED = cctk_time;
  const CCTK_REAL cctkOriginSpace1 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(0);
  const CCTK_REAL cctkOriginSpace2 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(1);
  const CCTK_REAL cctkOriginSpace3 CCTK_ATTRIBUTE_UNUSED = 
    CCTK_ORIGIN_SPACE(2);
  const CCTK_REAL dt CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_TIME;
  const CCTK_REAL dx CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz CCTK_ATTRIBUTE_UNUSED = CCTK_DELTA_SPACE(2);
  const CCTK_REAL dxi CCTK_ATTRIBUTE_UNUSED = pow(dx,-1);
  const CCTK_REAL dyi CCTK_ATTRIBUTE_UNUSED = pow(dy,-1);
  const CCTK_REAL dzi CCTK_ATTRIBUTE_UNUSED = pow(dz,-1);
  const CCTK_REAL khalf CCTK_ATTRIBUTE_UNUSED = 0.5;
  const CCTK_REAL kthird CCTK_ATTRIBUTE_UNUSED = 
    0.333333333333333333333333333333;
  const CCTK_REAL ktwothird CCTK_ATTRIBUTE_UNUSED = 
    0.666666666666666666666666666667;
  const CCTK_REAL kfourthird CCTK_ATTRIBUTE_UNUSED = 
    1.33333333333333333333333333333;
  const CCTK_REAL hdxi CCTK_ATTRIBUTE_UNUSED = 0.5*dxi;
  const CCTK_REAL hdyi CCTK_ATTRIBUTE_UNUSED = 0.5*dyi;
  const CCTK_REAL hdzi CCTK_ATTRIBUTE_UNUSED = 0.5*dzi;
  /* Initialize predefined quantities */
  /* Jacobian variable pointers */
  const bool usejacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool usejacobian = assume_use_jacobian>=0 ? assume_use_jacobian : usejacobian1;
  if (usejacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_determinant_ptrs[1] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_determinant_group) > 0) GroupDataPointers(cctkGH, jacobian_determinant_group,
                                                1, jacobian_determinant_ptrs);
  
  const CCTK_REAL* restrict const detJ CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_determinant_ptrs[0] : 0;
  
  const CCTK_REAL* restrict jacobian_inverse_ptrs[9] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian && strlen(jacobian_inverse_group) > 0) GroupDataPointers(cctkGH, jacobian_inverse_group,
                                                9, jacobian_inverse_ptrs);
  
  const CCTK_REAL* restrict const iJ11 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[0] : 0;
  const CCTK_REAL* restrict const iJ12 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[1] : 0;
  const CCTK_REAL* restrict const iJ13 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[2] : 0;
  const CCTK_REAL* restrict const iJ21 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[3] : 0;
  const CCTK_REAL* restrict const iJ22 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[4] : 0;
  const CCTK_REAL* restrict const iJ23 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[5] : 0;
  const CCTK_REAL* restrict const iJ31 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[6] : 0;
  const CCTK_REAL* restrict const iJ32 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[7] : 0;
  const CCTK_REAL* restrict const iJ33 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_inverse_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (usejacobian) GroupDataPointers(cctkGH, jacobian_derivative_group,
                                      18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = usejacobian ? jacobian_derivative_ptrs[17] : 0;
  /* Assign local copies of arrays functions */
  
  
  /* Calculate temporaries and arrays functions */
  /* Copy local copies back to grid functions */
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel
  CCTK_LOOP3(ML_BSSN_DG8_EvolutionInteriorSplit33,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2])
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    const int ti CCTK_ATTRIBUTE_UNUSED = i - kd.tile_imin[0];
    const int tj CCTK_ATTRIBUTE_UNUSED = j - kd.tile_imin[1];
    const int tk CCTK_ATTRIBUTE_UNUSED = k - kd.tile_imin[2];
    /* Assign local copies of grid functions */
    
    CCTK_REAL alphaL CCTK_ATTRIBUTE_UNUSED = alpha[index];
    CCTK_REAL At11L CCTK_ATTRIBUTE_UNUSED = At11[index];
    CCTK_REAL At12L CCTK_ATTRIBUTE_UNUSED = At12[index];
    CCTK_REAL At13L CCTK_ATTRIBUTE_UNUSED = At13[index];
    CCTK_REAL At22L CCTK_ATTRIBUTE_UNUSED = At22[index];
    CCTK_REAL At23L CCTK_ATTRIBUTE_UNUSED = At23[index];
    CCTK_REAL At33L CCTK_ATTRIBUTE_UNUSED = At33[index];
    CCTK_REAL beta1L CCTK_ATTRIBUTE_UNUSED = beta1[index];
    CCTK_REAL beta2L CCTK_ATTRIBUTE_UNUSED = beta2[index];
    CCTK_REAL beta3L CCTK_ATTRIBUTE_UNUSED = beta3[index];
    CCTK_REAL gt11L CCTK_ATTRIBUTE_UNUSED = gt11[index];
    CCTK_REAL gt12L CCTK_ATTRIBUTE_UNUSED = gt12[index];
    CCTK_REAL gt13L CCTK_ATTRIBUTE_UNUSED = gt13[index];
    CCTK_REAL gt22L CCTK_ATTRIBUTE_UNUSED = gt22[index];
    CCTK_REAL gt23L CCTK_ATTRIBUTE_UNUSED = gt23[index];
    CCTK_REAL gt33L CCTK_ATTRIBUTE_UNUSED = gt33[index];
    CCTK_REAL myDetJL CCTK_ATTRIBUTE_UNUSED = myDetJ[index];
    CCTK_REAL PDalpha1L CCTK_ATTRIBUTE_UNUSED = PDalpha1[index];
    CCTK_REAL PDalpha2L CCTK_ATTRIBUTE_UNUSED = PDalpha2[index];
    CCTK_REAL PDalpha3L CCTK_ATTRIBUTE_UNUSED = PDalpha3[index];
    CCTK_REAL PDbeta11L CCTK_ATTRIBUTE_UNUSED = PDbeta11[index];
    CCTK_REAL PDbeta12L CCTK_ATTRIBUTE_UNUSED = PDbeta12[index];
    CCTK_REAL PDbeta13L CCTK_ATTRIBUTE_UNUSED = PDbeta13[index];
    CCTK_REAL PDbeta21L CCTK_ATTRIBUTE_UNUSED = PDbeta21[index];
    CCTK_REAL PDbeta22L CCTK_ATTRIBUTE_UNUSED = PDbeta22[index];
    CCTK_REAL PDbeta23L CCTK_ATTRIBUTE_UNUSED = PDbeta23[index];
    CCTK_REAL PDbeta31L CCTK_ATTRIBUTE_UNUSED = PDbeta31[index];
    CCTK_REAL PDbeta32L CCTK_ATTRIBUTE_UNUSED = PDbeta32[index];
    CCTK_REAL PDbeta33L CCTK_ATTRIBUTE_UNUSED = PDbeta33[index];
    CCTK_REAL PDgt111L CCTK_ATTRIBUTE_UNUSED = PDgt111[index];
    CCTK_REAL PDgt112L CCTK_ATTRIBUTE_UNUSED = PDgt112[index];
    CCTK_REAL PDgt113L CCTK_ATTRIBUTE_UNUSED = PDgt113[index];
    CCTK_REAL PDgt121L CCTK_ATTRIBUTE_UNUSED = PDgt121[index];
    CCTK_REAL PDgt122L CCTK_ATTRIBUTE_UNUSED = PDgt122[index];
    CCTK_REAL PDgt123L CCTK_ATTRIBUTE_UNUSED = PDgt123[index];
    CCTK_REAL PDgt131L CCTK_ATTRIBUTE_UNUSED = PDgt131[index];
    CCTK_REAL PDgt132L CCTK_ATTRIBUTE_UNUSED = PDgt132[index];
    CCTK_REAL PDgt133L CCTK_ATTRIBUTE_UNUSED = PDgt133[index];
    CCTK_REAL PDgt221L CCTK_ATTRIBUTE_UNUSED = PDgt221[index];
    CCTK_REAL PDgt222L CCTK_ATTRIBUTE_UNUSED = PDgt222[index];
    CCTK_REAL PDgt223L CCTK_ATTRIBUTE_UNUSED = PDgt223[index];
    CCTK_REAL PDgt231L CCTK_ATTRIBUTE_UNUSED = PDgt231[index];
    CCTK_REAL PDgt232L CCTK_ATTRIBUTE_UNUSED = PDgt232[index];
    CCTK_REAL PDgt233L CCTK_ATTRIBUTE_UNUSED = PDgt233[index];
    CCTK_REAL PDgt331L CCTK_ATTRIBUTE_UNUSED = PDgt331[index];
    CCTK_REAL PDgt332L CCTK_ATTRIBUTE_UNUSED = PDgt332[index];
    CCTK_REAL PDgt333L CCTK_ATTRIBUTE_UNUSED = PDgt333[index];
    CCTK_REAL PDphiW1L CCTK_ATTRIBUTE_UNUSED = PDphiW1[index];
    CCTK_REAL PDphiW2L CCTK_ATTRIBUTE_UNUSED = PDphiW2[index];
    CCTK_REAL PDphiW3L CCTK_ATTRIBUTE_UNUSED = PDphiW3[index];
    CCTK_REAL phiWL CCTK_ATTRIBUTE_UNUSED = phiW[index];
    CCTK_REAL trKL CCTK_ATTRIBUTE_UNUSED = trK[index];
    CCTK_REAL Xt1L CCTK_ATTRIBUTE_UNUSED = Xt1[index];
    CCTK_REAL Xt2L CCTK_ATTRIBUTE_UNUSED = Xt2[index];
    CCTK_REAL Xt3L CCTK_ATTRIBUTE_UNUSED = Xt3[index];
    
    CCTK_REAL eTxxL, eTxyL, eTxzL, eTyyL, eTyzL, eTzzL CCTK_ATTRIBUTE_UNUSED ;
    
    if (assume_stress_energy_state>=0 ? assume_stress_energy_state : *stress_energy_state)
    {
      eTxxL = eTxx[index];
      eTxyL = eTxy[index];
      eTxzL = eTxz[index];
      eTyyL = eTyy[index];
      eTyzL = eTyz[index];
      eTzzL = eTzz[index];
    }
    else
    {
      eTxxL = 0.;
      eTxyL = 0.;
      eTxzL = 0.;
      eTyyL = 0.;
      eTyzL = 0.;
      eTzzL = 0.;
    }
    
    CCTK_REAL J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (usejacobian)
    {
      J11L = J11[index];
      J12L = J12[index];
      J13L = J13[index];
      J21L = J21[index];
      J22L = J22[index];
      J23L = J23[index];
      J31L = J31[index];
      J32L = J32[index];
      J33L = J33[index];
    }
    /* Include user supplied include files */
    /* Precompute derivatives */
    /* Calculate temporaries and grid functions */
    CCTK_REAL LDXt11 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(Xt1,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(Xt1,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(Xt1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(Xt1,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(Xt1,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(Xt1,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(Xt1,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(Xt1,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(Xt1,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(Xt1,7,0,0) - 
      0.5*GFOffset(Xt1,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(Xt1,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(Xt1,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(Xt1,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(Xt1,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(Xt1,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(Xt1,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(Xt1,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(Xt1,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(Xt1,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(Xt1,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(Xt1,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(Xt1,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(Xt1,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(Xt1,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(Xt1,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(Xt1,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(Xt1,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(Xt1,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(Xt1,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(Xt1,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(Xt1,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(Xt1,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(Xt1,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(Xt1,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(Xt1,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(Xt1,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(Xt1,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(Xt1,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(Xt1,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(Xt1,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(Xt1,3,0,0) - 
      0.2734375*GFOffset(Xt1,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(Xt1,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(Xt1,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(Xt1,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(Xt1,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(Xt1,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(Xt1,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(Xt1,2,0,0) + 
      0.444613449281090634955156033*GFOffset(Xt1,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(Xt1,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(Xt1,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(Xt1,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(Xt1,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(Xt1,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(Xt1,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(Xt1,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(Xt1,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(Xt1,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(Xt1,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(Xt1,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(Xt1,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(Xt1,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(Xt1,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(Xt1,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(Xt1,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(Xt1,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(Xt1,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(Xt1,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(Xt1,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(Xt1,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(Xt1,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(Xt1,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(Xt1,-1,0,0) + 
      18.*GFOffset(Xt1,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(Xt1,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(Xt1,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDXt21 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(Xt2,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(Xt2,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(Xt2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(Xt2,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(Xt2,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(Xt2,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(Xt2,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(Xt2,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(Xt2,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(Xt2,7,0,0) - 
      0.5*GFOffset(Xt2,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(Xt2,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(Xt2,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(Xt2,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(Xt2,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(Xt2,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(Xt2,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(Xt2,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(Xt2,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(Xt2,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(Xt2,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(Xt2,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(Xt2,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(Xt2,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(Xt2,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(Xt2,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(Xt2,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(Xt2,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(Xt2,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(Xt2,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(Xt2,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(Xt2,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(Xt2,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(Xt2,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(Xt2,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(Xt2,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(Xt2,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(Xt2,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(Xt2,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(Xt2,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(Xt2,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(Xt2,3,0,0) - 
      0.2734375*GFOffset(Xt2,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(Xt2,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(Xt2,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(Xt2,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(Xt2,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(Xt2,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(Xt2,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(Xt2,2,0,0) + 
      0.444613449281090634955156033*GFOffset(Xt2,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(Xt2,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(Xt2,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(Xt2,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(Xt2,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(Xt2,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(Xt2,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(Xt2,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(Xt2,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(Xt2,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(Xt2,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(Xt2,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(Xt2,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(Xt2,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(Xt2,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(Xt2,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(Xt2,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(Xt2,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(Xt2,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(Xt2,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(Xt2,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(Xt2,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(Xt2,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(Xt2,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(Xt2,-1,0,0) + 
      18.*GFOffset(Xt2,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(Xt2,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(Xt2,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDXt31 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(Xt3,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(Xt3,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(Xt3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(Xt3,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(Xt3,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(Xt3,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(Xt3,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(Xt3,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(Xt3,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(Xt3,7,0,0) - 
      0.5*GFOffset(Xt3,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(Xt3,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(Xt3,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(Xt3,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(Xt3,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(Xt3,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(Xt3,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(Xt3,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(Xt3,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(Xt3,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(Xt3,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(Xt3,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(Xt3,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(Xt3,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(Xt3,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(Xt3,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(Xt3,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(Xt3,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(Xt3,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(Xt3,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(Xt3,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(Xt3,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(Xt3,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(Xt3,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(Xt3,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(Xt3,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(Xt3,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(Xt3,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(Xt3,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(Xt3,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(Xt3,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(Xt3,3,0,0) - 
      0.2734375*GFOffset(Xt3,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(Xt3,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(Xt3,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(Xt3,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(Xt3,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(Xt3,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(Xt3,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(Xt3,2,0,0) + 
      0.444613449281090634955156033*GFOffset(Xt3,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(Xt3,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(Xt3,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(Xt3,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(Xt3,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(Xt3,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(Xt3,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(Xt3,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(Xt3,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(Xt3,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(Xt3,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(Xt3,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(Xt3,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(Xt3,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(Xt3,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(Xt3,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(Xt3,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(Xt3,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(Xt3,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(Xt3,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(Xt3,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(Xt3,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(Xt3,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(Xt3,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(Xt3,-1,0,0) + 
      18.*GFOffset(Xt3,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(Xt3,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(Xt3,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDXt12 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(Xt1,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(Xt1,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(Xt1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(Xt1,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(Xt1,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(Xt1,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(Xt1,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(Xt1,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(Xt1,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(Xt1,0,7,0) - 
      0.5*GFOffset(Xt1,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(Xt1,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(Xt1,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(Xt1,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(Xt1,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(Xt1,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(Xt1,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(Xt1,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(Xt1,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(Xt1,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(Xt1,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(Xt1,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(Xt1,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(Xt1,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(Xt1,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(Xt1,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(Xt1,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(Xt1,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(Xt1,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(Xt1,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(Xt1,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(Xt1,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(Xt1,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(Xt1,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(Xt1,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(Xt1,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(Xt1,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(Xt1,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(Xt1,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(Xt1,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(Xt1,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(Xt1,0,3,0) - 
      0.2734375*GFOffset(Xt1,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(Xt1,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(Xt1,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(Xt1,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(Xt1,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(Xt1,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(Xt1,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(Xt1,0,2,0) + 
      0.444613449281090634955156033*GFOffset(Xt1,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(Xt1,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(Xt1,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(Xt1,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(Xt1,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(Xt1,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(Xt1,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(Xt1,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(Xt1,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(Xt1,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(Xt1,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(Xt1,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(Xt1,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(Xt1,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(Xt1,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(Xt1,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(Xt1,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(Xt1,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(Xt1,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(Xt1,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(Xt1,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(Xt1,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(Xt1,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(Xt1,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(Xt1,0,-1,0) + 
      18.*GFOffset(Xt1,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(Xt1,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(Xt1,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDXt22 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(Xt2,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(Xt2,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(Xt2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(Xt2,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(Xt2,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(Xt2,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(Xt2,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(Xt2,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(Xt2,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(Xt2,0,7,0) - 
      0.5*GFOffset(Xt2,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(Xt2,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(Xt2,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(Xt2,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(Xt2,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(Xt2,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(Xt2,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(Xt2,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(Xt2,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(Xt2,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(Xt2,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(Xt2,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(Xt2,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(Xt2,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(Xt2,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(Xt2,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(Xt2,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(Xt2,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(Xt2,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(Xt2,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(Xt2,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(Xt2,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(Xt2,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(Xt2,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(Xt2,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(Xt2,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(Xt2,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(Xt2,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(Xt2,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(Xt2,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(Xt2,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(Xt2,0,3,0) - 
      0.2734375*GFOffset(Xt2,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(Xt2,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(Xt2,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(Xt2,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(Xt2,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(Xt2,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(Xt2,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(Xt2,0,2,0) + 
      0.444613449281090634955156033*GFOffset(Xt2,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(Xt2,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(Xt2,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(Xt2,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(Xt2,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(Xt2,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(Xt2,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(Xt2,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(Xt2,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(Xt2,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(Xt2,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(Xt2,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(Xt2,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(Xt2,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(Xt2,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(Xt2,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(Xt2,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(Xt2,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(Xt2,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(Xt2,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(Xt2,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(Xt2,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(Xt2,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(Xt2,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(Xt2,0,-1,0) + 
      18.*GFOffset(Xt2,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(Xt2,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(Xt2,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDXt32 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(Xt3,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(Xt3,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(Xt3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(Xt3,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(Xt3,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(Xt3,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(Xt3,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(Xt3,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(Xt3,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(Xt3,0,7,0) - 
      0.5*GFOffset(Xt3,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(Xt3,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(Xt3,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(Xt3,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(Xt3,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(Xt3,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(Xt3,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(Xt3,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(Xt3,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(Xt3,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(Xt3,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(Xt3,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(Xt3,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(Xt3,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(Xt3,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(Xt3,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(Xt3,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(Xt3,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(Xt3,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(Xt3,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(Xt3,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(Xt3,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(Xt3,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(Xt3,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(Xt3,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(Xt3,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(Xt3,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(Xt3,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(Xt3,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(Xt3,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(Xt3,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(Xt3,0,3,0) - 
      0.2734375*GFOffset(Xt3,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(Xt3,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(Xt3,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(Xt3,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(Xt3,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(Xt3,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(Xt3,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(Xt3,0,2,0) + 
      0.444613449281090634955156033*GFOffset(Xt3,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(Xt3,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(Xt3,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(Xt3,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(Xt3,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(Xt3,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(Xt3,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(Xt3,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(Xt3,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(Xt3,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(Xt3,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(Xt3,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(Xt3,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(Xt3,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(Xt3,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(Xt3,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(Xt3,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(Xt3,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(Xt3,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(Xt3,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(Xt3,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(Xt3,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(Xt3,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(Xt3,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(Xt3,0,-1,0) + 
      18.*GFOffset(Xt3,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(Xt3,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(Xt3,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDXt13 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(Xt1,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(Xt1,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(Xt1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(Xt1,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(Xt1,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(Xt1,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(Xt1,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(Xt1,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(Xt1,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(Xt1,0,0,7) - 
      0.5*GFOffset(Xt1,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(Xt1,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(Xt1,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(Xt1,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(Xt1,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(Xt1,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(Xt1,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(Xt1,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(Xt1,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(Xt1,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(Xt1,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(Xt1,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(Xt1,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(Xt1,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(Xt1,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(Xt1,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(Xt1,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(Xt1,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(Xt1,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(Xt1,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(Xt1,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(Xt1,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(Xt1,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(Xt1,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(Xt1,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(Xt1,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(Xt1,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(Xt1,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(Xt1,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(Xt1,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(Xt1,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(Xt1,0,0,3) - 
      0.2734375*GFOffset(Xt1,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(Xt1,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(Xt1,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(Xt1,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(Xt1,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(Xt1,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(Xt1,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(Xt1,0,0,2) + 
      0.444613449281090634955156033*GFOffset(Xt1,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(Xt1,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(Xt1,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(Xt1,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(Xt1,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(Xt1,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(Xt1,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(Xt1,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(Xt1,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(Xt1,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(Xt1,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(Xt1,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(Xt1,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(Xt1,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(Xt1,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(Xt1,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(Xt1,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(Xt1,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(Xt1,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(Xt1,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(Xt1,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(Xt1,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(Xt1,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(Xt1,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(Xt1,0,0,-1) + 
      18.*GFOffset(Xt1,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(Xt1,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(Xt1,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDXt23 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(Xt2,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(Xt2,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(Xt2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(Xt2,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(Xt2,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(Xt2,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(Xt2,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(Xt2,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(Xt2,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(Xt2,0,0,7) - 
      0.5*GFOffset(Xt2,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(Xt2,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(Xt2,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(Xt2,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(Xt2,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(Xt2,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(Xt2,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(Xt2,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(Xt2,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(Xt2,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(Xt2,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(Xt2,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(Xt2,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(Xt2,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(Xt2,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(Xt2,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(Xt2,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(Xt2,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(Xt2,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(Xt2,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(Xt2,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(Xt2,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(Xt2,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(Xt2,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(Xt2,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(Xt2,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(Xt2,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(Xt2,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(Xt2,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(Xt2,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(Xt2,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(Xt2,0,0,3) - 
      0.2734375*GFOffset(Xt2,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(Xt2,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(Xt2,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(Xt2,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(Xt2,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(Xt2,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(Xt2,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(Xt2,0,0,2) + 
      0.444613449281090634955156033*GFOffset(Xt2,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(Xt2,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(Xt2,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(Xt2,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(Xt2,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(Xt2,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(Xt2,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(Xt2,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(Xt2,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(Xt2,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(Xt2,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(Xt2,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(Xt2,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(Xt2,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(Xt2,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(Xt2,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(Xt2,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(Xt2,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(Xt2,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(Xt2,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(Xt2,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(Xt2,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(Xt2,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(Xt2,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(Xt2,0,0,-1) + 
      18.*GFOffset(Xt2,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(Xt2,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(Xt2,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDXt33 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(Xt3,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(Xt3,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(Xt3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(Xt3,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(Xt3,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(Xt3,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(Xt3,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(Xt3,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(Xt3,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(Xt3,0,0,7) - 
      0.5*GFOffset(Xt3,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(Xt3,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(Xt3,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(Xt3,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(Xt3,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(Xt3,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(Xt3,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(Xt3,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(Xt3,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(Xt3,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(Xt3,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(Xt3,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(Xt3,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(Xt3,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(Xt3,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(Xt3,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(Xt3,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(Xt3,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(Xt3,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(Xt3,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(Xt3,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(Xt3,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(Xt3,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(Xt3,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(Xt3,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(Xt3,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(Xt3,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(Xt3,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(Xt3,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(Xt3,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(Xt3,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(Xt3,0,0,3) - 
      0.2734375*GFOffset(Xt3,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(Xt3,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(Xt3,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(Xt3,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(Xt3,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(Xt3,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(Xt3,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(Xt3,0,0,2) + 
      0.444613449281090634955156033*GFOffset(Xt3,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(Xt3,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(Xt3,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(Xt3,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(Xt3,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(Xt3,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(Xt3,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(Xt3,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(Xt3,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(Xt3,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(Xt3,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(Xt3,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(Xt3,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(Xt3,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(Xt3,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(Xt3,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(Xt3,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(Xt3,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(Xt3,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(Xt3,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(Xt3,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(Xt3,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(Xt3,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(Xt3,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(Xt3,0,0,-1) + 
      18.*GFOffset(Xt3,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(Xt3,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(Xt3,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDXt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDXt33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDXt11 = J11L*LDXt11 + J21L*LDXt12 + J31L*LDXt13;
      
      PDXt21 = J11L*LDXt21 + J21L*LDXt22 + J31L*LDXt23;
      
      PDXt31 = J11L*LDXt31 + J21L*LDXt32 + J31L*LDXt33;
      
      PDXt12 = J12L*LDXt11 + J22L*LDXt12 + J32L*LDXt13;
      
      PDXt22 = J12L*LDXt21 + J22L*LDXt22 + J32L*LDXt23;
      
      PDXt32 = J12L*LDXt31 + J22L*LDXt32 + J32L*LDXt33;
      
      PDXt13 = J13L*LDXt11 + J23L*LDXt12 + J33L*LDXt13;
      
      PDXt23 = J13L*LDXt21 + J23L*LDXt22 + J33L*LDXt23;
      
      PDXt33 = J13L*LDXt31 + J23L*LDXt32 + J33L*LDXt33;
    }
    else
    {
      PDXt11 = LDXt11;
      
      PDXt21 = LDXt21;
      
      PDXt31 = LDXt31;
      
      PDXt12 = LDXt12;
      
      PDXt22 = LDXt22;
      
      PDXt32 = LDXt32;
      
      PDXt13 = LDXt13;
      
      PDXt23 = LDXt23;
      
      PDXt33 = LDXt33;
    }
    
    CCTK_REAL LDPDphiW11 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDphiW1,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDphiW1,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDphiW1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDphiW1,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDphiW1,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDphiW1,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDphiW1,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDphiW1,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDphiW1,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDphiW1,7,0,0) - 
      0.5*GFOffset(PDphiW1,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDphiW1,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDphiW1,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDphiW1,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDphiW1,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDphiW1,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDphiW1,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDphiW1,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDphiW1,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDphiW1,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDphiW1,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDphiW1,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDphiW1,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDphiW1,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDphiW1,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDphiW1,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDphiW1,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDphiW1,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDphiW1,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDphiW1,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDphiW1,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDphiW1,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDphiW1,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDphiW1,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDphiW1,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDphiW1,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDphiW1,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDphiW1,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDphiW1,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDphiW1,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDphiW1,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDphiW1,3,0,0) - 
      0.2734375*GFOffset(PDphiW1,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDphiW1,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDphiW1,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDphiW1,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDphiW1,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDphiW1,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDphiW1,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDphiW1,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDphiW1,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDphiW1,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDphiW1,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDphiW1,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDphiW1,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDphiW1,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDphiW1,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDphiW1,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDphiW1,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDphiW1,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDphiW1,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDphiW1,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDphiW1,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDphiW1,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDphiW1,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDphiW1,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDphiW1,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDphiW1,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDphiW1,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDphiW1,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDphiW1,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDphiW1,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDphiW1,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDphiW1,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDphiW1,-1,0,0) + 
      18.*GFOffset(PDphiW1,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDphiW1,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDphiW1,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDphiW12 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDphiW1,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDphiW1,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDphiW1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDphiW1,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDphiW1,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDphiW1,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDphiW1,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDphiW1,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDphiW1,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDphiW1,0,7,0) - 
      0.5*GFOffset(PDphiW1,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDphiW1,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDphiW1,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDphiW1,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDphiW1,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDphiW1,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDphiW1,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDphiW1,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDphiW1,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDphiW1,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDphiW1,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDphiW1,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDphiW1,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDphiW1,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDphiW1,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDphiW1,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDphiW1,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDphiW1,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDphiW1,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDphiW1,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDphiW1,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDphiW1,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDphiW1,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDphiW1,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDphiW1,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDphiW1,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDphiW1,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDphiW1,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDphiW1,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDphiW1,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDphiW1,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDphiW1,0,3,0) - 
      0.2734375*GFOffset(PDphiW1,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDphiW1,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDphiW1,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDphiW1,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDphiW1,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDphiW1,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDphiW1,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDphiW1,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDphiW1,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDphiW1,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDphiW1,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDphiW1,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDphiW1,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDphiW1,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDphiW1,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDphiW1,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDphiW1,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDphiW1,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDphiW1,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDphiW1,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDphiW1,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDphiW1,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDphiW1,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDphiW1,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDphiW1,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDphiW1,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDphiW1,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDphiW1,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDphiW1,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDphiW1,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDphiW1,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDphiW1,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDphiW1,0,-1,0) + 
      18.*GFOffset(PDphiW1,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDphiW1,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDphiW1,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDphiW13 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDphiW1,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDphiW1,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDphiW1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDphiW1,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDphiW1,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDphiW1,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDphiW1,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDphiW1,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDphiW1,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDphiW1,0,0,7) - 
      0.5*GFOffset(PDphiW1,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDphiW1,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDphiW1,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDphiW1,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDphiW1,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDphiW1,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDphiW1,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDphiW1,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDphiW1,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDphiW1,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDphiW1,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDphiW1,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDphiW1,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDphiW1,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDphiW1,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDphiW1,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDphiW1,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDphiW1,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDphiW1,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDphiW1,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDphiW1,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDphiW1,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDphiW1,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDphiW1,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDphiW1,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDphiW1,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDphiW1,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDphiW1,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDphiW1,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDphiW1,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDphiW1,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDphiW1,0,0,3) - 
      0.2734375*GFOffset(PDphiW1,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDphiW1,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDphiW1,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDphiW1,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDphiW1,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDphiW1,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDphiW1,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDphiW1,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDphiW1,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDphiW1,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDphiW1,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDphiW1,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDphiW1,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDphiW1,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDphiW1,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDphiW1,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDphiW1,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDphiW1,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDphiW1,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDphiW1,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDphiW1,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDphiW1,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDphiW1,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDphiW1,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDphiW1,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDphiW1,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDphiW1,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDphiW1,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDphiW1,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDphiW1,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDphiW1,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDphiW1,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDphiW1,0,0,-1) + 
      18.*GFOffset(PDphiW1,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDphiW1,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDphiW1,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDphiW21 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDphiW2,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDphiW2,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDphiW2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDphiW2,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDphiW2,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDphiW2,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDphiW2,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDphiW2,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDphiW2,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDphiW2,7,0,0) - 
      0.5*GFOffset(PDphiW2,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDphiW2,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDphiW2,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDphiW2,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDphiW2,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDphiW2,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDphiW2,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDphiW2,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDphiW2,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDphiW2,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDphiW2,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDphiW2,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDphiW2,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDphiW2,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDphiW2,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDphiW2,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDphiW2,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDphiW2,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDphiW2,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDphiW2,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDphiW2,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDphiW2,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDphiW2,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDphiW2,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDphiW2,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDphiW2,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDphiW2,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDphiW2,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDphiW2,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDphiW2,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDphiW2,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDphiW2,3,0,0) - 
      0.2734375*GFOffset(PDphiW2,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDphiW2,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDphiW2,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDphiW2,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDphiW2,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDphiW2,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDphiW2,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDphiW2,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDphiW2,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDphiW2,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDphiW2,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDphiW2,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDphiW2,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDphiW2,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDphiW2,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDphiW2,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDphiW2,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDphiW2,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDphiW2,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDphiW2,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDphiW2,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDphiW2,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDphiW2,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDphiW2,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDphiW2,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDphiW2,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDphiW2,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDphiW2,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDphiW2,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDphiW2,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDphiW2,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDphiW2,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDphiW2,-1,0,0) + 
      18.*GFOffset(PDphiW2,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDphiW2,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDphiW2,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDphiW22 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDphiW2,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDphiW2,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDphiW2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDphiW2,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDphiW2,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDphiW2,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDphiW2,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDphiW2,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDphiW2,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDphiW2,0,7,0) - 
      0.5*GFOffset(PDphiW2,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDphiW2,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDphiW2,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDphiW2,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDphiW2,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDphiW2,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDphiW2,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDphiW2,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDphiW2,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDphiW2,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDphiW2,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDphiW2,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDphiW2,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDphiW2,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDphiW2,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDphiW2,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDphiW2,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDphiW2,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDphiW2,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDphiW2,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDphiW2,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDphiW2,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDphiW2,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDphiW2,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDphiW2,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDphiW2,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDphiW2,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDphiW2,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDphiW2,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDphiW2,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDphiW2,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDphiW2,0,3,0) - 
      0.2734375*GFOffset(PDphiW2,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDphiW2,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDphiW2,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDphiW2,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDphiW2,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDphiW2,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDphiW2,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDphiW2,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDphiW2,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDphiW2,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDphiW2,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDphiW2,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDphiW2,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDphiW2,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDphiW2,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDphiW2,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDphiW2,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDphiW2,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDphiW2,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDphiW2,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDphiW2,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDphiW2,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDphiW2,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDphiW2,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDphiW2,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDphiW2,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDphiW2,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDphiW2,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDphiW2,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDphiW2,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDphiW2,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDphiW2,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDphiW2,0,-1,0) + 
      18.*GFOffset(PDphiW2,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDphiW2,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDphiW2,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDphiW23 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDphiW2,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDphiW2,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDphiW2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDphiW2,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDphiW2,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDphiW2,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDphiW2,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDphiW2,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDphiW2,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDphiW2,0,0,7) - 
      0.5*GFOffset(PDphiW2,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDphiW2,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDphiW2,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDphiW2,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDphiW2,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDphiW2,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDphiW2,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDphiW2,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDphiW2,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDphiW2,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDphiW2,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDphiW2,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDphiW2,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDphiW2,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDphiW2,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDphiW2,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDphiW2,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDphiW2,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDphiW2,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDphiW2,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDphiW2,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDphiW2,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDphiW2,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDphiW2,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDphiW2,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDphiW2,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDphiW2,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDphiW2,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDphiW2,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDphiW2,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDphiW2,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDphiW2,0,0,3) - 
      0.2734375*GFOffset(PDphiW2,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDphiW2,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDphiW2,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDphiW2,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDphiW2,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDphiW2,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDphiW2,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDphiW2,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDphiW2,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDphiW2,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDphiW2,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDphiW2,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDphiW2,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDphiW2,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDphiW2,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDphiW2,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDphiW2,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDphiW2,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDphiW2,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDphiW2,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDphiW2,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDphiW2,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDphiW2,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDphiW2,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDphiW2,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDphiW2,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDphiW2,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDphiW2,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDphiW2,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDphiW2,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDphiW2,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDphiW2,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDphiW2,0,0,-1) + 
      18.*GFOffset(PDphiW2,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDphiW2,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDphiW2,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDphiW31 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDphiW3,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDphiW3,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDphiW3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDphiW3,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDphiW3,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDphiW3,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDphiW3,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDphiW3,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDphiW3,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDphiW3,7,0,0) - 
      0.5*GFOffset(PDphiW3,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDphiW3,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDphiW3,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDphiW3,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDphiW3,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDphiW3,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDphiW3,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDphiW3,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDphiW3,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDphiW3,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDphiW3,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDphiW3,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDphiW3,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDphiW3,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDphiW3,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDphiW3,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDphiW3,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDphiW3,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDphiW3,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDphiW3,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDphiW3,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDphiW3,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDphiW3,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDphiW3,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDphiW3,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDphiW3,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDphiW3,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDphiW3,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDphiW3,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDphiW3,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDphiW3,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDphiW3,3,0,0) - 
      0.2734375*GFOffset(PDphiW3,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDphiW3,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDphiW3,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDphiW3,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDphiW3,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDphiW3,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDphiW3,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDphiW3,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDphiW3,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDphiW3,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDphiW3,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDphiW3,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDphiW3,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDphiW3,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDphiW3,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDphiW3,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDphiW3,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDphiW3,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDphiW3,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDphiW3,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDphiW3,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDphiW3,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDphiW3,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDphiW3,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDphiW3,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDphiW3,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDphiW3,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDphiW3,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDphiW3,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDphiW3,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDphiW3,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDphiW3,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDphiW3,-1,0,0) + 
      18.*GFOffset(PDphiW3,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDphiW3,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDphiW3,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDphiW32 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDphiW3,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDphiW3,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDphiW3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDphiW3,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDphiW3,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDphiW3,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDphiW3,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDphiW3,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDphiW3,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDphiW3,0,7,0) - 
      0.5*GFOffset(PDphiW3,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDphiW3,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDphiW3,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDphiW3,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDphiW3,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDphiW3,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDphiW3,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDphiW3,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDphiW3,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDphiW3,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDphiW3,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDphiW3,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDphiW3,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDphiW3,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDphiW3,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDphiW3,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDphiW3,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDphiW3,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDphiW3,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDphiW3,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDphiW3,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDphiW3,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDphiW3,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDphiW3,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDphiW3,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDphiW3,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDphiW3,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDphiW3,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDphiW3,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDphiW3,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDphiW3,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDphiW3,0,3,0) - 
      0.2734375*GFOffset(PDphiW3,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDphiW3,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDphiW3,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDphiW3,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDphiW3,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDphiW3,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDphiW3,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDphiW3,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDphiW3,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDphiW3,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDphiW3,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDphiW3,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDphiW3,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDphiW3,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDphiW3,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDphiW3,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDphiW3,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDphiW3,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDphiW3,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDphiW3,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDphiW3,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDphiW3,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDphiW3,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDphiW3,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDphiW3,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDphiW3,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDphiW3,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDphiW3,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDphiW3,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDphiW3,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDphiW3,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDphiW3,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDphiW3,0,-1,0) + 
      18.*GFOffset(PDphiW3,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDphiW3,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDphiW3,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDphiW33 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDphiW3,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDphiW3,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDphiW3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDphiW3,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDphiW3,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDphiW3,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDphiW3,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDphiW3,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDphiW3,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDphiW3,0,0,7) - 
      0.5*GFOffset(PDphiW3,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDphiW3,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDphiW3,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDphiW3,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDphiW3,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDphiW3,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDphiW3,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDphiW3,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDphiW3,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDphiW3,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDphiW3,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDphiW3,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDphiW3,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDphiW3,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDphiW3,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDphiW3,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDphiW3,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDphiW3,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDphiW3,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDphiW3,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDphiW3,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDphiW3,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDphiW3,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDphiW3,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDphiW3,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDphiW3,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDphiW3,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDphiW3,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDphiW3,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDphiW3,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDphiW3,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDphiW3,0,0,3) - 
      0.2734375*GFOffset(PDphiW3,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDphiW3,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDphiW3,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDphiW3,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDphiW3,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDphiW3,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDphiW3,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDphiW3,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDphiW3,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDphiW3,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDphiW3,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDphiW3,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDphiW3,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDphiW3,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDphiW3,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDphiW3,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDphiW3,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDphiW3,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDphiW3,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDphiW3,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDphiW3,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDphiW3,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDphiW3,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDphiW3,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDphiW3,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDphiW3,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDphiW3,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDphiW3,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDphiW3,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDphiW3,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDphiW3,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDphiW3,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDphiW3,0,0,-1) + 
      18.*GFOffset(PDphiW3,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDphiW3,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDphiW3,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1111 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt111,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt111,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt111,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt111,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt111,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt111,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt111,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt111,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt111,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt111,7,0,0) - 
      0.5*GFOffset(PDgt111,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt111,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt111,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt111,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt111,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt111,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt111,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt111,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt111,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt111,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt111,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt111,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt111,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt111,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt111,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt111,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt111,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt111,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt111,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt111,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt111,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt111,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt111,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt111,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt111,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt111,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt111,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt111,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt111,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt111,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt111,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt111,3,0,0) - 
      0.2734375*GFOffset(PDgt111,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt111,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt111,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt111,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt111,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt111,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt111,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt111,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt111,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt111,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt111,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt111,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt111,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt111,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt111,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt111,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt111,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt111,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt111,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt111,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt111,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt111,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt111,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt111,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt111,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt111,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt111,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt111,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt111,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt111,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt111,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt111,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt111,-1,0,0) + 
      18.*GFOffset(PDgt111,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt111,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt111,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1112 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt111,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt111,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt111,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt111,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt111,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt111,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt111,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt111,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt111,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt111,0,7,0) - 
      0.5*GFOffset(PDgt111,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt111,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt111,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt111,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt111,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt111,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt111,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt111,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt111,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt111,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt111,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt111,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt111,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt111,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt111,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt111,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt111,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt111,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt111,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt111,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt111,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt111,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt111,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt111,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt111,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt111,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt111,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt111,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt111,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt111,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt111,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt111,0,3,0) - 
      0.2734375*GFOffset(PDgt111,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt111,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt111,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt111,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt111,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt111,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt111,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt111,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt111,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt111,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt111,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt111,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt111,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt111,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt111,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt111,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt111,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt111,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt111,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt111,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt111,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt111,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt111,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt111,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt111,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt111,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt111,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt111,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt111,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt111,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt111,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt111,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt111,0,-1,0) + 
      18.*GFOffset(PDgt111,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt111,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt111,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1113 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt111,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt111,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt111,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt111,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt111,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt111,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt111,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt111,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt111,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt111,0,0,7) - 
      0.5*GFOffset(PDgt111,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt111,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt111,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt111,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt111,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt111,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt111,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt111,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt111,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt111,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt111,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt111,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt111,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt111,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt111,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt111,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt111,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt111,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt111,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt111,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt111,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt111,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt111,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt111,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt111,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt111,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt111,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt111,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt111,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt111,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt111,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt111,0,0,3) - 
      0.2734375*GFOffset(PDgt111,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt111,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt111,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt111,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt111,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt111,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt111,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt111,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt111,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt111,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt111,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt111,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt111,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt111,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt111,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt111,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt111,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt111,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt111,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt111,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt111,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt111,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt111,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt111,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt111,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt111,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt111,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt111,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt111,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt111,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt111,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt111,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt111,0,0,-1) + 
      18.*GFOffset(PDgt111,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt111,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt111,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1121 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt112,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt112,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt112,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt112,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt112,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt112,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt112,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt112,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt112,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt112,7,0,0) - 
      0.5*GFOffset(PDgt112,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt112,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt112,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt112,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt112,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt112,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt112,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt112,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt112,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt112,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt112,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt112,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt112,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt112,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt112,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt112,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt112,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt112,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt112,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt112,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt112,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt112,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt112,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt112,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt112,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt112,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt112,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt112,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt112,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt112,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt112,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt112,3,0,0) - 
      0.2734375*GFOffset(PDgt112,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt112,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt112,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt112,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt112,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt112,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt112,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt112,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt112,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt112,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt112,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt112,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt112,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt112,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt112,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt112,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt112,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt112,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt112,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt112,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt112,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt112,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt112,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt112,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt112,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt112,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt112,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt112,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt112,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt112,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt112,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt112,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt112,-1,0,0) + 
      18.*GFOffset(PDgt112,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt112,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt112,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1122 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt112,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt112,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt112,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt112,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt112,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt112,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt112,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt112,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt112,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt112,0,7,0) - 
      0.5*GFOffset(PDgt112,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt112,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt112,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt112,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt112,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt112,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt112,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt112,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt112,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt112,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt112,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt112,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt112,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt112,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt112,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt112,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt112,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt112,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt112,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt112,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt112,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt112,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt112,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt112,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt112,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt112,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt112,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt112,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt112,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt112,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt112,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt112,0,3,0) - 
      0.2734375*GFOffset(PDgt112,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt112,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt112,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt112,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt112,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt112,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt112,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt112,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt112,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt112,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt112,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt112,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt112,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt112,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt112,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt112,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt112,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt112,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt112,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt112,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt112,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt112,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt112,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt112,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt112,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt112,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt112,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt112,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt112,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt112,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt112,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt112,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt112,0,-1,0) + 
      18.*GFOffset(PDgt112,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt112,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt112,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1123 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt112,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt112,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt112,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt112,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt112,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt112,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt112,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt112,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt112,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt112,0,0,7) - 
      0.5*GFOffset(PDgt112,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt112,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt112,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt112,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt112,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt112,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt112,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt112,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt112,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt112,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt112,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt112,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt112,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt112,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt112,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt112,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt112,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt112,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt112,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt112,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt112,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt112,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt112,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt112,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt112,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt112,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt112,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt112,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt112,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt112,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt112,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt112,0,0,3) - 
      0.2734375*GFOffset(PDgt112,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt112,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt112,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt112,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt112,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt112,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt112,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt112,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt112,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt112,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt112,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt112,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt112,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt112,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt112,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt112,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt112,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt112,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt112,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt112,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt112,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt112,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt112,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt112,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt112,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt112,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt112,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt112,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt112,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt112,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt112,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt112,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt112,0,0,-1) + 
      18.*GFOffset(PDgt112,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt112,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt112,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1131 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt113,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt113,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt113,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt113,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt113,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt113,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt113,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt113,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt113,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt113,7,0,0) - 
      0.5*GFOffset(PDgt113,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt113,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt113,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt113,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt113,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt113,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt113,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt113,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt113,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt113,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt113,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt113,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt113,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt113,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt113,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt113,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt113,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt113,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt113,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt113,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt113,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt113,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt113,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt113,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt113,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt113,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt113,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt113,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt113,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt113,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt113,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt113,3,0,0) - 
      0.2734375*GFOffset(PDgt113,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt113,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt113,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt113,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt113,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt113,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt113,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt113,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt113,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt113,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt113,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt113,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt113,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt113,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt113,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt113,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt113,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt113,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt113,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt113,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt113,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt113,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt113,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt113,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt113,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt113,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt113,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt113,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt113,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt113,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt113,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt113,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt113,-1,0,0) + 
      18.*GFOffset(PDgt113,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt113,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt113,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1132 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt113,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt113,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt113,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt113,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt113,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt113,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt113,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt113,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt113,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt113,0,7,0) - 
      0.5*GFOffset(PDgt113,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt113,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt113,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt113,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt113,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt113,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt113,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt113,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt113,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt113,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt113,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt113,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt113,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt113,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt113,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt113,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt113,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt113,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt113,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt113,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt113,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt113,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt113,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt113,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt113,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt113,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt113,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt113,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt113,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt113,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt113,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt113,0,3,0) - 
      0.2734375*GFOffset(PDgt113,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt113,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt113,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt113,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt113,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt113,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt113,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt113,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt113,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt113,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt113,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt113,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt113,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt113,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt113,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt113,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt113,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt113,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt113,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt113,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt113,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt113,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt113,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt113,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt113,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt113,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt113,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt113,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt113,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt113,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt113,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt113,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt113,0,-1,0) + 
      18.*GFOffset(PDgt113,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt113,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt113,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1133 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt113,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt113,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt113,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt113,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt113,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt113,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt113,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt113,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt113,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt113,0,0,7) - 
      0.5*GFOffset(PDgt113,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt113,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt113,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt113,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt113,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt113,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt113,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt113,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt113,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt113,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt113,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt113,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt113,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt113,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt113,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt113,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt113,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt113,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt113,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt113,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt113,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt113,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt113,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt113,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt113,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt113,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt113,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt113,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt113,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt113,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt113,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt113,0,0,3) - 
      0.2734375*GFOffset(PDgt113,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt113,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt113,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt113,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt113,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt113,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt113,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt113,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt113,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt113,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt113,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt113,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt113,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt113,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt113,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt113,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt113,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt113,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt113,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt113,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt113,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt113,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt113,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt113,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt113,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt113,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt113,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt113,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt113,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt113,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt113,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt113,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt113,0,0,-1) + 
      18.*GFOffset(PDgt113,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt113,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt113,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1211 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt121,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt121,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt121,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt121,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt121,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt121,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt121,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt121,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt121,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt121,7,0,0) - 
      0.5*GFOffset(PDgt121,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt121,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt121,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt121,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt121,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt121,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt121,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt121,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt121,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt121,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt121,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt121,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt121,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt121,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt121,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt121,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt121,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt121,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt121,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt121,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt121,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt121,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt121,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt121,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt121,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt121,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt121,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt121,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt121,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt121,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt121,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt121,3,0,0) - 
      0.2734375*GFOffset(PDgt121,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt121,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt121,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt121,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt121,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt121,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt121,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt121,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt121,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt121,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt121,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt121,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt121,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt121,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt121,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt121,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt121,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt121,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt121,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt121,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt121,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt121,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt121,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt121,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt121,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt121,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt121,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt121,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt121,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt121,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt121,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt121,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt121,-1,0,0) + 
      18.*GFOffset(PDgt121,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt121,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt121,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1212 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt121,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt121,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt121,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt121,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt121,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt121,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt121,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt121,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt121,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt121,0,7,0) - 
      0.5*GFOffset(PDgt121,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt121,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt121,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt121,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt121,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt121,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt121,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt121,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt121,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt121,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt121,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt121,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt121,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt121,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt121,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt121,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt121,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt121,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt121,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt121,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt121,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt121,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt121,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt121,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt121,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt121,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt121,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt121,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt121,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt121,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt121,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt121,0,3,0) - 
      0.2734375*GFOffset(PDgt121,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt121,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt121,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt121,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt121,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt121,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt121,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt121,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt121,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt121,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt121,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt121,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt121,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt121,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt121,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt121,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt121,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt121,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt121,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt121,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt121,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt121,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt121,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt121,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt121,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt121,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt121,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt121,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt121,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt121,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt121,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt121,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt121,0,-1,0) + 
      18.*GFOffset(PDgt121,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt121,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt121,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1213 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt121,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt121,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt121,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt121,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt121,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt121,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt121,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt121,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt121,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt121,0,0,7) - 
      0.5*GFOffset(PDgt121,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt121,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt121,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt121,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt121,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt121,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt121,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt121,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt121,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt121,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt121,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt121,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt121,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt121,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt121,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt121,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt121,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt121,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt121,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt121,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt121,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt121,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt121,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt121,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt121,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt121,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt121,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt121,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt121,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt121,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt121,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt121,0,0,3) - 
      0.2734375*GFOffset(PDgt121,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt121,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt121,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt121,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt121,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt121,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt121,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt121,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt121,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt121,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt121,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt121,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt121,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt121,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt121,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt121,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt121,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt121,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt121,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt121,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt121,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt121,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt121,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt121,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt121,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt121,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt121,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt121,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt121,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt121,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt121,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt121,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt121,0,0,-1) + 
      18.*GFOffset(PDgt121,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt121,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt121,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1221 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt122,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt122,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt122,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt122,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt122,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt122,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt122,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt122,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt122,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt122,7,0,0) - 
      0.5*GFOffset(PDgt122,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt122,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt122,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt122,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt122,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt122,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt122,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt122,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt122,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt122,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt122,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt122,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt122,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt122,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt122,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt122,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt122,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt122,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt122,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt122,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt122,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt122,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt122,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt122,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt122,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt122,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt122,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt122,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt122,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt122,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt122,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt122,3,0,0) - 
      0.2734375*GFOffset(PDgt122,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt122,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt122,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt122,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt122,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt122,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt122,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt122,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt122,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt122,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt122,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt122,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt122,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt122,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt122,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt122,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt122,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt122,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt122,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt122,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt122,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt122,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt122,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt122,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt122,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt122,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt122,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt122,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt122,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt122,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt122,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt122,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt122,-1,0,0) + 
      18.*GFOffset(PDgt122,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt122,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt122,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1222 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt122,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt122,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt122,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt122,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt122,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt122,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt122,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt122,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt122,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt122,0,7,0) - 
      0.5*GFOffset(PDgt122,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt122,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt122,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt122,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt122,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt122,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt122,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt122,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt122,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt122,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt122,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt122,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt122,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt122,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt122,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt122,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt122,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt122,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt122,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt122,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt122,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt122,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt122,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt122,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt122,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt122,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt122,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt122,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt122,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt122,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt122,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt122,0,3,0) - 
      0.2734375*GFOffset(PDgt122,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt122,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt122,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt122,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt122,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt122,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt122,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt122,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt122,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt122,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt122,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt122,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt122,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt122,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt122,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt122,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt122,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt122,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt122,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt122,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt122,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt122,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt122,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt122,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt122,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt122,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt122,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt122,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt122,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt122,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt122,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt122,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt122,0,-1,0) + 
      18.*GFOffset(PDgt122,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt122,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt122,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1223 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt122,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt122,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt122,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt122,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt122,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt122,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt122,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt122,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt122,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt122,0,0,7) - 
      0.5*GFOffset(PDgt122,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt122,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt122,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt122,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt122,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt122,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt122,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt122,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt122,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt122,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt122,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt122,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt122,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt122,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt122,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt122,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt122,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt122,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt122,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt122,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt122,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt122,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt122,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt122,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt122,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt122,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt122,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt122,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt122,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt122,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt122,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt122,0,0,3) - 
      0.2734375*GFOffset(PDgt122,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt122,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt122,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt122,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt122,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt122,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt122,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt122,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt122,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt122,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt122,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt122,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt122,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt122,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt122,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt122,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt122,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt122,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt122,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt122,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt122,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt122,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt122,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt122,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt122,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt122,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt122,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt122,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt122,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt122,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt122,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt122,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt122,0,0,-1) + 
      18.*GFOffset(PDgt122,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt122,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt122,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1231 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt123,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt123,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt123,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt123,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt123,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt123,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt123,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt123,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt123,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt123,7,0,0) - 
      0.5*GFOffset(PDgt123,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt123,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt123,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt123,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt123,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt123,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt123,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt123,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt123,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt123,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt123,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt123,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt123,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt123,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt123,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt123,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt123,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt123,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt123,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt123,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt123,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt123,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt123,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt123,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt123,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt123,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt123,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt123,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt123,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt123,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt123,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt123,3,0,0) - 
      0.2734375*GFOffset(PDgt123,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt123,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt123,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt123,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt123,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt123,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt123,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt123,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt123,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt123,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt123,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt123,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt123,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt123,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt123,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt123,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt123,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt123,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt123,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt123,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt123,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt123,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt123,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt123,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt123,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt123,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt123,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt123,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt123,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt123,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt123,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt123,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt123,-1,0,0) + 
      18.*GFOffset(PDgt123,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt123,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt123,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1232 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt123,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt123,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt123,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt123,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt123,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt123,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt123,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt123,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt123,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt123,0,7,0) - 
      0.5*GFOffset(PDgt123,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt123,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt123,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt123,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt123,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt123,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt123,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt123,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt123,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt123,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt123,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt123,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt123,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt123,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt123,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt123,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt123,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt123,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt123,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt123,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt123,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt123,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt123,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt123,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt123,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt123,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt123,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt123,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt123,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt123,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt123,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt123,0,3,0) - 
      0.2734375*GFOffset(PDgt123,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt123,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt123,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt123,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt123,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt123,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt123,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt123,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt123,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt123,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt123,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt123,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt123,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt123,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt123,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt123,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt123,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt123,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt123,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt123,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt123,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt123,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt123,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt123,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt123,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt123,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt123,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt123,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt123,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt123,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt123,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt123,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt123,0,-1,0) + 
      18.*GFOffset(PDgt123,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt123,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt123,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1233 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt123,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt123,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt123,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt123,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt123,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt123,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt123,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt123,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt123,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt123,0,0,7) - 
      0.5*GFOffset(PDgt123,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt123,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt123,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt123,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt123,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt123,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt123,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt123,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt123,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt123,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt123,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt123,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt123,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt123,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt123,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt123,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt123,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt123,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt123,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt123,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt123,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt123,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt123,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt123,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt123,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt123,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt123,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt123,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt123,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt123,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt123,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt123,0,0,3) - 
      0.2734375*GFOffset(PDgt123,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt123,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt123,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt123,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt123,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt123,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt123,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt123,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt123,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt123,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt123,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt123,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt123,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt123,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt123,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt123,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt123,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt123,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt123,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt123,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt123,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt123,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt123,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt123,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt123,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt123,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt123,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt123,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt123,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt123,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt123,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt123,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt123,0,0,-1) + 
      18.*GFOffset(PDgt123,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt123,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt123,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1311 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt131,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt131,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt131,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt131,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt131,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt131,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt131,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt131,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt131,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt131,7,0,0) - 
      0.5*GFOffset(PDgt131,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt131,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt131,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt131,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt131,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt131,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt131,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt131,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt131,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt131,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt131,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt131,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt131,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt131,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt131,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt131,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt131,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt131,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt131,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt131,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt131,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt131,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt131,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt131,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt131,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt131,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt131,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt131,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt131,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt131,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt131,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt131,3,0,0) - 
      0.2734375*GFOffset(PDgt131,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt131,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt131,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt131,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt131,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt131,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt131,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt131,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt131,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt131,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt131,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt131,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt131,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt131,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt131,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt131,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt131,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt131,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt131,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt131,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt131,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt131,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt131,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt131,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt131,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt131,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt131,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt131,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt131,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt131,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt131,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt131,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt131,-1,0,0) + 
      18.*GFOffset(PDgt131,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt131,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt131,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1312 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt131,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt131,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt131,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt131,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt131,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt131,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt131,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt131,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt131,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt131,0,7,0) - 
      0.5*GFOffset(PDgt131,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt131,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt131,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt131,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt131,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt131,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt131,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt131,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt131,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt131,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt131,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt131,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt131,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt131,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt131,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt131,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt131,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt131,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt131,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt131,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt131,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt131,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt131,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt131,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt131,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt131,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt131,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt131,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt131,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt131,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt131,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt131,0,3,0) - 
      0.2734375*GFOffset(PDgt131,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt131,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt131,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt131,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt131,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt131,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt131,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt131,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt131,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt131,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt131,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt131,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt131,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt131,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt131,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt131,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt131,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt131,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt131,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt131,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt131,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt131,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt131,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt131,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt131,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt131,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt131,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt131,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt131,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt131,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt131,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt131,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt131,0,-1,0) + 
      18.*GFOffset(PDgt131,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt131,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt131,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1313 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt131,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt131,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt131,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt131,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt131,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt131,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt131,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt131,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt131,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt131,0,0,7) - 
      0.5*GFOffset(PDgt131,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt131,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt131,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt131,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt131,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt131,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt131,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt131,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt131,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt131,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt131,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt131,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt131,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt131,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt131,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt131,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt131,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt131,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt131,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt131,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt131,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt131,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt131,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt131,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt131,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt131,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt131,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt131,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt131,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt131,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt131,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt131,0,0,3) - 
      0.2734375*GFOffset(PDgt131,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt131,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt131,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt131,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt131,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt131,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt131,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt131,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt131,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt131,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt131,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt131,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt131,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt131,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt131,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt131,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt131,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt131,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt131,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt131,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt131,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt131,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt131,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt131,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt131,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt131,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt131,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt131,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt131,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt131,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt131,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt131,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt131,0,0,-1) + 
      18.*GFOffset(PDgt131,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt131,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt131,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1321 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt132,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt132,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt132,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt132,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt132,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt132,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt132,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt132,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt132,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt132,7,0,0) - 
      0.5*GFOffset(PDgt132,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt132,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt132,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt132,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt132,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt132,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt132,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt132,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt132,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt132,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt132,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt132,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt132,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt132,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt132,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt132,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt132,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt132,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt132,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt132,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt132,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt132,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt132,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt132,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt132,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt132,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt132,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt132,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt132,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt132,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt132,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt132,3,0,0) - 
      0.2734375*GFOffset(PDgt132,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt132,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt132,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt132,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt132,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt132,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt132,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt132,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt132,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt132,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt132,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt132,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt132,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt132,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt132,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt132,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt132,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt132,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt132,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt132,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt132,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt132,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt132,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt132,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt132,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt132,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt132,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt132,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt132,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt132,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt132,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt132,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt132,-1,0,0) + 
      18.*GFOffset(PDgt132,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt132,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt132,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1322 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt132,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt132,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt132,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt132,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt132,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt132,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt132,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt132,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt132,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt132,0,7,0) - 
      0.5*GFOffset(PDgt132,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt132,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt132,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt132,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt132,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt132,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt132,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt132,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt132,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt132,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt132,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt132,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt132,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt132,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt132,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt132,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt132,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt132,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt132,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt132,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt132,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt132,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt132,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt132,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt132,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt132,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt132,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt132,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt132,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt132,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt132,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt132,0,3,0) - 
      0.2734375*GFOffset(PDgt132,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt132,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt132,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt132,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt132,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt132,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt132,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt132,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt132,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt132,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt132,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt132,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt132,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt132,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt132,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt132,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt132,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt132,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt132,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt132,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt132,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt132,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt132,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt132,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt132,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt132,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt132,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt132,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt132,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt132,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt132,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt132,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt132,0,-1,0) + 
      18.*GFOffset(PDgt132,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt132,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt132,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1323 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt132,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt132,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt132,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt132,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt132,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt132,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt132,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt132,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt132,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt132,0,0,7) - 
      0.5*GFOffset(PDgt132,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt132,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt132,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt132,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt132,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt132,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt132,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt132,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt132,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt132,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt132,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt132,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt132,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt132,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt132,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt132,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt132,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt132,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt132,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt132,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt132,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt132,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt132,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt132,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt132,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt132,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt132,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt132,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt132,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt132,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt132,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt132,0,0,3) - 
      0.2734375*GFOffset(PDgt132,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt132,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt132,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt132,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt132,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt132,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt132,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt132,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt132,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt132,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt132,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt132,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt132,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt132,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt132,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt132,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt132,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt132,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt132,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt132,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt132,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt132,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt132,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt132,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt132,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt132,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt132,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt132,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt132,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt132,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt132,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt132,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt132,0,0,-1) + 
      18.*GFOffset(PDgt132,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt132,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt132,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt1331 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt133,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt133,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt133,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt133,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt133,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt133,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt133,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt133,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt133,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt133,7,0,0) - 
      0.5*GFOffset(PDgt133,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt133,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt133,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt133,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt133,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt133,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt133,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt133,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt133,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt133,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt133,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt133,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt133,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt133,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt133,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt133,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt133,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt133,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt133,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt133,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt133,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt133,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt133,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt133,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt133,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt133,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt133,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt133,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt133,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt133,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt133,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt133,3,0,0) - 
      0.2734375*GFOffset(PDgt133,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt133,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt133,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt133,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt133,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt133,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt133,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt133,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt133,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt133,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt133,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt133,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt133,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt133,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt133,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt133,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt133,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt133,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt133,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt133,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt133,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt133,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt133,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt133,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt133,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt133,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt133,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt133,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt133,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt133,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt133,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt133,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt133,-1,0,0) + 
      18.*GFOffset(PDgt133,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt133,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt133,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt1332 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt133,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt133,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt133,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt133,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt133,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt133,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt133,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt133,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt133,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt133,0,7,0) - 
      0.5*GFOffset(PDgt133,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt133,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt133,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt133,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt133,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt133,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt133,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt133,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt133,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt133,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt133,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt133,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt133,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt133,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt133,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt133,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt133,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt133,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt133,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt133,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt133,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt133,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt133,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt133,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt133,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt133,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt133,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt133,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt133,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt133,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt133,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt133,0,3,0) - 
      0.2734375*GFOffset(PDgt133,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt133,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt133,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt133,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt133,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt133,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt133,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt133,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt133,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt133,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt133,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt133,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt133,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt133,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt133,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt133,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt133,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt133,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt133,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt133,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt133,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt133,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt133,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt133,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt133,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt133,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt133,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt133,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt133,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt133,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt133,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt133,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt133,0,-1,0) + 
      18.*GFOffset(PDgt133,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt133,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt133,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt1333 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt133,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt133,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt133,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt133,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt133,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt133,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt133,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt133,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt133,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt133,0,0,7) - 
      0.5*GFOffset(PDgt133,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt133,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt133,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt133,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt133,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt133,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt133,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt133,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt133,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt133,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt133,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt133,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt133,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt133,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt133,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt133,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt133,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt133,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt133,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt133,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt133,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt133,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt133,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt133,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt133,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt133,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt133,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt133,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt133,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt133,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt133,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt133,0,0,3) - 
      0.2734375*GFOffset(PDgt133,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt133,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt133,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt133,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt133,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt133,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt133,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt133,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt133,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt133,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt133,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt133,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt133,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt133,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt133,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt133,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt133,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt133,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt133,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt133,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt133,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt133,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt133,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt133,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt133,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt133,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt133,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt133,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt133,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt133,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt133,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt133,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt133,0,0,-1) + 
      18.*GFOffset(PDgt133,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt133,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt133,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2211 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt221,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt221,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt221,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt221,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt221,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt221,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt221,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt221,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt221,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt221,7,0,0) - 
      0.5*GFOffset(PDgt221,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt221,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt221,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt221,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt221,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt221,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt221,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt221,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt221,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt221,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt221,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt221,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt221,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt221,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt221,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt221,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt221,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt221,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt221,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt221,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt221,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt221,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt221,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt221,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt221,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt221,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt221,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt221,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt221,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt221,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt221,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt221,3,0,0) - 
      0.2734375*GFOffset(PDgt221,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt221,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt221,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt221,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt221,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt221,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt221,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt221,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt221,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt221,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt221,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt221,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt221,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt221,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt221,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt221,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt221,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt221,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt221,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt221,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt221,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt221,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt221,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt221,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt221,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt221,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt221,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt221,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt221,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt221,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt221,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt221,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt221,-1,0,0) + 
      18.*GFOffset(PDgt221,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt221,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt221,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2212 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt221,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt221,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt221,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt221,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt221,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt221,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt221,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt221,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt221,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt221,0,7,0) - 
      0.5*GFOffset(PDgt221,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt221,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt221,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt221,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt221,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt221,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt221,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt221,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt221,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt221,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt221,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt221,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt221,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt221,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt221,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt221,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt221,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt221,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt221,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt221,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt221,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt221,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt221,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt221,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt221,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt221,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt221,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt221,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt221,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt221,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt221,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt221,0,3,0) - 
      0.2734375*GFOffset(PDgt221,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt221,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt221,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt221,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt221,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt221,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt221,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt221,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt221,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt221,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt221,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt221,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt221,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt221,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt221,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt221,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt221,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt221,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt221,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt221,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt221,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt221,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt221,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt221,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt221,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt221,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt221,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt221,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt221,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt221,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt221,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt221,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt221,0,-1,0) + 
      18.*GFOffset(PDgt221,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt221,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt221,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2213 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt221,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt221,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt221,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt221,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt221,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt221,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt221,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt221,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt221,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt221,0,0,7) - 
      0.5*GFOffset(PDgt221,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt221,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt221,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt221,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt221,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt221,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt221,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt221,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt221,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt221,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt221,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt221,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt221,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt221,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt221,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt221,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt221,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt221,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt221,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt221,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt221,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt221,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt221,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt221,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt221,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt221,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt221,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt221,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt221,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt221,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt221,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt221,0,0,3) - 
      0.2734375*GFOffset(PDgt221,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt221,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt221,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt221,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt221,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt221,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt221,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt221,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt221,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt221,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt221,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt221,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt221,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt221,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt221,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt221,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt221,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt221,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt221,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt221,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt221,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt221,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt221,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt221,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt221,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt221,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt221,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt221,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt221,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt221,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt221,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt221,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt221,0,0,-1) + 
      18.*GFOffset(PDgt221,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt221,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt221,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2221 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt222,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt222,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt222,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt222,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt222,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt222,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt222,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt222,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt222,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt222,7,0,0) - 
      0.5*GFOffset(PDgt222,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt222,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt222,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt222,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt222,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt222,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt222,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt222,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt222,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt222,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt222,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt222,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt222,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt222,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt222,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt222,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt222,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt222,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt222,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt222,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt222,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt222,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt222,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt222,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt222,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt222,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt222,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt222,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt222,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt222,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt222,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt222,3,0,0) - 
      0.2734375*GFOffset(PDgt222,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt222,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt222,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt222,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt222,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt222,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt222,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt222,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt222,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt222,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt222,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt222,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt222,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt222,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt222,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt222,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt222,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt222,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt222,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt222,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt222,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt222,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt222,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt222,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt222,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt222,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt222,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt222,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt222,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt222,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt222,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt222,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt222,-1,0,0) + 
      18.*GFOffset(PDgt222,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt222,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt222,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2222 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt222,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt222,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt222,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt222,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt222,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt222,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt222,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt222,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt222,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt222,0,7,0) - 
      0.5*GFOffset(PDgt222,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt222,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt222,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt222,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt222,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt222,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt222,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt222,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt222,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt222,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt222,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt222,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt222,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt222,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt222,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt222,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt222,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt222,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt222,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt222,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt222,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt222,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt222,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt222,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt222,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt222,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt222,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt222,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt222,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt222,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt222,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt222,0,3,0) - 
      0.2734375*GFOffset(PDgt222,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt222,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt222,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt222,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt222,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt222,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt222,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt222,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt222,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt222,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt222,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt222,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt222,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt222,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt222,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt222,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt222,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt222,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt222,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt222,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt222,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt222,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt222,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt222,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt222,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt222,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt222,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt222,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt222,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt222,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt222,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt222,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt222,0,-1,0) + 
      18.*GFOffset(PDgt222,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt222,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt222,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2223 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt222,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt222,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt222,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt222,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt222,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt222,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt222,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt222,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt222,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt222,0,0,7) - 
      0.5*GFOffset(PDgt222,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt222,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt222,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt222,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt222,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt222,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt222,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt222,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt222,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt222,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt222,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt222,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt222,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt222,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt222,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt222,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt222,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt222,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt222,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt222,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt222,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt222,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt222,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt222,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt222,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt222,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt222,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt222,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt222,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt222,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt222,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt222,0,0,3) - 
      0.2734375*GFOffset(PDgt222,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt222,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt222,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt222,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt222,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt222,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt222,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt222,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt222,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt222,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt222,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt222,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt222,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt222,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt222,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt222,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt222,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt222,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt222,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt222,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt222,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt222,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt222,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt222,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt222,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt222,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt222,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt222,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt222,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt222,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt222,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt222,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt222,0,0,-1) + 
      18.*GFOffset(PDgt222,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt222,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt222,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2231 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt223,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt223,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt223,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt223,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt223,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt223,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt223,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt223,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt223,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt223,7,0,0) - 
      0.5*GFOffset(PDgt223,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt223,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt223,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt223,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt223,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt223,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt223,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt223,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt223,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt223,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt223,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt223,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt223,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt223,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt223,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt223,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt223,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt223,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt223,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt223,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt223,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt223,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt223,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt223,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt223,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt223,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt223,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt223,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt223,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt223,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt223,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt223,3,0,0) - 
      0.2734375*GFOffset(PDgt223,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt223,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt223,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt223,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt223,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt223,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt223,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt223,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt223,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt223,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt223,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt223,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt223,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt223,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt223,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt223,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt223,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt223,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt223,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt223,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt223,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt223,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt223,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt223,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt223,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt223,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt223,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt223,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt223,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt223,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt223,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt223,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt223,-1,0,0) + 
      18.*GFOffset(PDgt223,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt223,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt223,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2232 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt223,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt223,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt223,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt223,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt223,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt223,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt223,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt223,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt223,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt223,0,7,0) - 
      0.5*GFOffset(PDgt223,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt223,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt223,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt223,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt223,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt223,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt223,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt223,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt223,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt223,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt223,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt223,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt223,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt223,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt223,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt223,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt223,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt223,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt223,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt223,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt223,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt223,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt223,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt223,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt223,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt223,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt223,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt223,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt223,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt223,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt223,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt223,0,3,0) - 
      0.2734375*GFOffset(PDgt223,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt223,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt223,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt223,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt223,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt223,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt223,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt223,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt223,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt223,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt223,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt223,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt223,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt223,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt223,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt223,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt223,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt223,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt223,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt223,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt223,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt223,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt223,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt223,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt223,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt223,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt223,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt223,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt223,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt223,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt223,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt223,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt223,0,-1,0) + 
      18.*GFOffset(PDgt223,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt223,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt223,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2233 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt223,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt223,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt223,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt223,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt223,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt223,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt223,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt223,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt223,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt223,0,0,7) - 
      0.5*GFOffset(PDgt223,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt223,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt223,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt223,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt223,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt223,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt223,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt223,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt223,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt223,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt223,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt223,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt223,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt223,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt223,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt223,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt223,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt223,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt223,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt223,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt223,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt223,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt223,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt223,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt223,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt223,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt223,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt223,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt223,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt223,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt223,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt223,0,0,3) - 
      0.2734375*GFOffset(PDgt223,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt223,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt223,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt223,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt223,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt223,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt223,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt223,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt223,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt223,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt223,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt223,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt223,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt223,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt223,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt223,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt223,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt223,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt223,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt223,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt223,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt223,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt223,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt223,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt223,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt223,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt223,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt223,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt223,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt223,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt223,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt223,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt223,0,0,-1) + 
      18.*GFOffset(PDgt223,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt223,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt223,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2311 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt231,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt231,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt231,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt231,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt231,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt231,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt231,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt231,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt231,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt231,7,0,0) - 
      0.5*GFOffset(PDgt231,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt231,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt231,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt231,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt231,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt231,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt231,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt231,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt231,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt231,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt231,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt231,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt231,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt231,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt231,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt231,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt231,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt231,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt231,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt231,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt231,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt231,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt231,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt231,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt231,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt231,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt231,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt231,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt231,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt231,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt231,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt231,3,0,0) - 
      0.2734375*GFOffset(PDgt231,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt231,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt231,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt231,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt231,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt231,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt231,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt231,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt231,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt231,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt231,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt231,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt231,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt231,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt231,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt231,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt231,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt231,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt231,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt231,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt231,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt231,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt231,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt231,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt231,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt231,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt231,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt231,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt231,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt231,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt231,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt231,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt231,-1,0,0) + 
      18.*GFOffset(PDgt231,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt231,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt231,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2312 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt231,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt231,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt231,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt231,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt231,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt231,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt231,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt231,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt231,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt231,0,7,0) - 
      0.5*GFOffset(PDgt231,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt231,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt231,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt231,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt231,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt231,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt231,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt231,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt231,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt231,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt231,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt231,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt231,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt231,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt231,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt231,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt231,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt231,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt231,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt231,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt231,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt231,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt231,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt231,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt231,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt231,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt231,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt231,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt231,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt231,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt231,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt231,0,3,0) - 
      0.2734375*GFOffset(PDgt231,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt231,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt231,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt231,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt231,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt231,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt231,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt231,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt231,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt231,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt231,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt231,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt231,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt231,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt231,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt231,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt231,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt231,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt231,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt231,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt231,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt231,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt231,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt231,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt231,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt231,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt231,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt231,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt231,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt231,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt231,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt231,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt231,0,-1,0) + 
      18.*GFOffset(PDgt231,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt231,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt231,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2313 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt231,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt231,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt231,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt231,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt231,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt231,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt231,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt231,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt231,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt231,0,0,7) - 
      0.5*GFOffset(PDgt231,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt231,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt231,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt231,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt231,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt231,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt231,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt231,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt231,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt231,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt231,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt231,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt231,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt231,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt231,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt231,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt231,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt231,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt231,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt231,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt231,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt231,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt231,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt231,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt231,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt231,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt231,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt231,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt231,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt231,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt231,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt231,0,0,3) - 
      0.2734375*GFOffset(PDgt231,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt231,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt231,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt231,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt231,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt231,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt231,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt231,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt231,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt231,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt231,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt231,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt231,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt231,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt231,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt231,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt231,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt231,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt231,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt231,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt231,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt231,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt231,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt231,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt231,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt231,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt231,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt231,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt231,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt231,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt231,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt231,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt231,0,0,-1) + 
      18.*GFOffset(PDgt231,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt231,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt231,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2321 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt232,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt232,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt232,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt232,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt232,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt232,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt232,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt232,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt232,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt232,7,0,0) - 
      0.5*GFOffset(PDgt232,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt232,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt232,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt232,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt232,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt232,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt232,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt232,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt232,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt232,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt232,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt232,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt232,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt232,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt232,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt232,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt232,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt232,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt232,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt232,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt232,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt232,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt232,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt232,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt232,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt232,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt232,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt232,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt232,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt232,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt232,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt232,3,0,0) - 
      0.2734375*GFOffset(PDgt232,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt232,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt232,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt232,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt232,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt232,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt232,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt232,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt232,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt232,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt232,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt232,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt232,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt232,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt232,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt232,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt232,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt232,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt232,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt232,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt232,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt232,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt232,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt232,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt232,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt232,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt232,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt232,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt232,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt232,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt232,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt232,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt232,-1,0,0) + 
      18.*GFOffset(PDgt232,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt232,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt232,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2322 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt232,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt232,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt232,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt232,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt232,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt232,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt232,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt232,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt232,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt232,0,7,0) - 
      0.5*GFOffset(PDgt232,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt232,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt232,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt232,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt232,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt232,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt232,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt232,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt232,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt232,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt232,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt232,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt232,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt232,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt232,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt232,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt232,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt232,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt232,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt232,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt232,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt232,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt232,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt232,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt232,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt232,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt232,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt232,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt232,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt232,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt232,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt232,0,3,0) - 
      0.2734375*GFOffset(PDgt232,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt232,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt232,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt232,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt232,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt232,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt232,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt232,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt232,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt232,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt232,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt232,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt232,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt232,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt232,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt232,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt232,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt232,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt232,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt232,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt232,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt232,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt232,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt232,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt232,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt232,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt232,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt232,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt232,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt232,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt232,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt232,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt232,0,-1,0) + 
      18.*GFOffset(PDgt232,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt232,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt232,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2323 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt232,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt232,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt232,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt232,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt232,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt232,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt232,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt232,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt232,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt232,0,0,7) - 
      0.5*GFOffset(PDgt232,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt232,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt232,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt232,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt232,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt232,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt232,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt232,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt232,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt232,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt232,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt232,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt232,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt232,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt232,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt232,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt232,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt232,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt232,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt232,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt232,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt232,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt232,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt232,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt232,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt232,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt232,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt232,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt232,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt232,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt232,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt232,0,0,3) - 
      0.2734375*GFOffset(PDgt232,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt232,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt232,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt232,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt232,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt232,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt232,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt232,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt232,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt232,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt232,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt232,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt232,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt232,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt232,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt232,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt232,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt232,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt232,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt232,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt232,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt232,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt232,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt232,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt232,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt232,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt232,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt232,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt232,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt232,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt232,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt232,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt232,0,0,-1) + 
      18.*GFOffset(PDgt232,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt232,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt232,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt2331 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt233,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt233,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt233,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt233,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt233,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt233,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt233,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt233,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt233,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt233,7,0,0) - 
      0.5*GFOffset(PDgt233,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt233,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt233,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt233,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt233,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt233,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt233,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt233,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt233,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt233,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt233,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt233,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt233,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt233,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt233,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt233,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt233,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt233,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt233,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt233,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt233,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt233,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt233,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt233,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt233,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt233,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt233,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt233,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt233,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt233,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt233,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt233,3,0,0) - 
      0.2734375*GFOffset(PDgt233,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt233,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt233,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt233,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt233,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt233,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt233,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt233,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt233,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt233,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt233,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt233,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt233,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt233,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt233,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt233,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt233,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt233,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt233,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt233,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt233,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt233,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt233,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt233,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt233,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt233,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt233,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt233,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt233,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt233,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt233,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt233,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt233,-1,0,0) + 
      18.*GFOffset(PDgt233,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt233,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt233,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt2332 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt233,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt233,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt233,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt233,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt233,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt233,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt233,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt233,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt233,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt233,0,7,0) - 
      0.5*GFOffset(PDgt233,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt233,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt233,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt233,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt233,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt233,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt233,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt233,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt233,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt233,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt233,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt233,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt233,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt233,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt233,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt233,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt233,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt233,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt233,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt233,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt233,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt233,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt233,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt233,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt233,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt233,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt233,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt233,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt233,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt233,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt233,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt233,0,3,0) - 
      0.2734375*GFOffset(PDgt233,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt233,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt233,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt233,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt233,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt233,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt233,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt233,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt233,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt233,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt233,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt233,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt233,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt233,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt233,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt233,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt233,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt233,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt233,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt233,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt233,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt233,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt233,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt233,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt233,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt233,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt233,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt233,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt233,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt233,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt233,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt233,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt233,0,-1,0) + 
      18.*GFOffset(PDgt233,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt233,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt233,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt2333 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt233,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt233,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt233,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt233,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt233,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt233,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt233,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt233,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt233,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt233,0,0,7) - 
      0.5*GFOffset(PDgt233,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt233,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt233,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt233,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt233,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt233,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt233,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt233,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt233,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt233,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt233,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt233,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt233,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt233,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt233,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt233,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt233,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt233,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt233,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt233,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt233,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt233,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt233,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt233,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt233,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt233,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt233,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt233,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt233,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt233,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt233,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt233,0,0,3) - 
      0.2734375*GFOffset(PDgt233,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt233,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt233,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt233,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt233,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt233,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt233,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt233,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt233,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt233,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt233,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt233,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt233,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt233,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt233,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt233,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt233,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt233,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt233,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt233,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt233,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt233,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt233,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt233,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt233,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt233,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt233,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt233,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt233,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt233,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt233,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt233,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt233,0,0,-1) + 
      18.*GFOffset(PDgt233,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt233,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt233,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt3311 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt331,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt331,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt331,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt331,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt331,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt331,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt331,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt331,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt331,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt331,7,0,0) - 
      0.5*GFOffset(PDgt331,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt331,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt331,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt331,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt331,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt331,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt331,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt331,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt331,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt331,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt331,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt331,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt331,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt331,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt331,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt331,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt331,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt331,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt331,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt331,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt331,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt331,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt331,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt331,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt331,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt331,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt331,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt331,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt331,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt331,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt331,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt331,3,0,0) - 
      0.2734375*GFOffset(PDgt331,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt331,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt331,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt331,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt331,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt331,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt331,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt331,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt331,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt331,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt331,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt331,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt331,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt331,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt331,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt331,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt331,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt331,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt331,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt331,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt331,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt331,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt331,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt331,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt331,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt331,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt331,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt331,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt331,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt331,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt331,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt331,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt331,-1,0,0) + 
      18.*GFOffset(PDgt331,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt331,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt331,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt3312 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt331,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt331,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt331,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt331,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt331,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt331,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt331,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt331,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt331,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt331,0,7,0) - 
      0.5*GFOffset(PDgt331,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt331,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt331,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt331,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt331,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt331,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt331,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt331,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt331,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt331,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt331,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt331,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt331,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt331,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt331,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt331,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt331,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt331,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt331,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt331,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt331,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt331,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt331,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt331,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt331,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt331,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt331,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt331,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt331,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt331,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt331,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt331,0,3,0) - 
      0.2734375*GFOffset(PDgt331,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt331,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt331,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt331,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt331,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt331,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt331,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt331,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt331,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt331,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt331,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt331,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt331,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt331,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt331,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt331,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt331,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt331,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt331,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt331,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt331,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt331,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt331,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt331,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt331,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt331,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt331,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt331,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt331,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt331,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt331,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt331,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt331,0,-1,0) + 
      18.*GFOffset(PDgt331,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt331,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt331,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt3313 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt331,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt331,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt331,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt331,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt331,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt331,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt331,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt331,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt331,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt331,0,0,7) - 
      0.5*GFOffset(PDgt331,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt331,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt331,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt331,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt331,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt331,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt331,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt331,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt331,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt331,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt331,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt331,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt331,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt331,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt331,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt331,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt331,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt331,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt331,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt331,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt331,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt331,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt331,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt331,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt331,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt331,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt331,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt331,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt331,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt331,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt331,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt331,0,0,3) - 
      0.2734375*GFOffset(PDgt331,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt331,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt331,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt331,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt331,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt331,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt331,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt331,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt331,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt331,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt331,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt331,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt331,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt331,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt331,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt331,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt331,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt331,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt331,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt331,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt331,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt331,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt331,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt331,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt331,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt331,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt331,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt331,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt331,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt331,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt331,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt331,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt331,0,0,-1) + 
      18.*GFOffset(PDgt331,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt331,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt331,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt3321 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt332,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt332,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt332,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt332,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt332,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt332,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt332,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt332,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt332,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt332,7,0,0) - 
      0.5*GFOffset(PDgt332,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt332,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt332,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt332,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt332,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt332,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt332,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt332,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt332,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt332,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt332,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt332,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt332,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt332,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt332,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt332,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt332,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt332,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt332,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt332,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt332,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt332,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt332,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt332,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt332,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt332,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt332,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt332,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt332,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt332,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt332,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt332,3,0,0) - 
      0.2734375*GFOffset(PDgt332,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt332,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt332,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt332,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt332,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt332,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt332,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt332,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt332,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt332,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt332,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt332,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt332,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt332,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt332,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt332,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt332,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt332,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt332,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt332,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt332,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt332,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt332,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt332,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt332,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt332,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt332,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt332,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt332,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt332,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt332,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt332,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt332,-1,0,0) + 
      18.*GFOffset(PDgt332,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt332,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt332,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt3322 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt332,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt332,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt332,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt332,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt332,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt332,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt332,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt332,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt332,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt332,0,7,0) - 
      0.5*GFOffset(PDgt332,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt332,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt332,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt332,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt332,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt332,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt332,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt332,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt332,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt332,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt332,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt332,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt332,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt332,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt332,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt332,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt332,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt332,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt332,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt332,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt332,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt332,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt332,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt332,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt332,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt332,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt332,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt332,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt332,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt332,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt332,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt332,0,3,0) - 
      0.2734375*GFOffset(PDgt332,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt332,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt332,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt332,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt332,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt332,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt332,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt332,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt332,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt332,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt332,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt332,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt332,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt332,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt332,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt332,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt332,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt332,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt332,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt332,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt332,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt332,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt332,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt332,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt332,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt332,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt332,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt332,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt332,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt332,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt332,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt332,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt332,0,-1,0) + 
      18.*GFOffset(PDgt332,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt332,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt332,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt3323 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt332,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt332,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt332,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt332,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt332,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt332,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt332,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt332,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt332,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt332,0,0,7) - 
      0.5*GFOffset(PDgt332,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt332,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt332,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt332,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt332,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt332,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt332,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt332,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt332,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt332,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt332,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt332,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt332,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt332,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt332,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt332,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt332,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt332,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt332,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt332,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt332,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt332,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt332,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt332,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt332,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt332,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt332,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt332,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt332,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt332,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt332,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt332,0,0,3) - 
      0.2734375*GFOffset(PDgt332,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt332,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt332,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt332,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt332,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt332,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt332,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt332,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt332,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt332,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt332,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt332,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt332,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt332,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt332,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt332,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt332,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt332,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt332,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt332,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt332,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt332,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt332,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt332,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt332,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt332,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt332,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt332,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt332,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt332,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt332,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt332,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt332,0,0,-1) + 
      18.*GFOffset(PDgt332,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt332,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt332,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDgt3331 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDgt333,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDgt333,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDgt333,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt333,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt333,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt333,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt333,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt333,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt333,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt333,7,0,0) - 
      0.5*GFOffset(PDgt333,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt333,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt333,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt333,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt333,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt333,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt333,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt333,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt333,7,0,0),0) + IfThen(ti 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt333,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt333,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt333,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt333,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt333,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt333,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt333,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt333,6,0,0),0) + IfThen(ti 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt333,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt333,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt333,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt333,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt333,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt333,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt333,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt333,5,0,0),0) + IfThen(ti 
      == 4,0.2734375*GFOffset(PDgt333,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt333,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt333,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt333,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt333,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt333,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt333,3,0,0) - 
      0.2734375*GFOffset(PDgt333,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt333,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt333,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt333,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt333,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt333,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt333,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt333,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDgt333,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(PDgt333,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt333,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt333,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt333,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt333,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt333,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt333,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt333,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt333,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt333,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt333,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt333,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt333,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt333,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt333,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt333,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDgt333,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt333,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt333,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt333,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt333,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt333,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt333,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt333,-1,0,0) + 
      18.*GFOffset(PDgt333,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDgt333,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDgt333,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDgt3332 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDgt333,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDgt333,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDgt333,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt333,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDgt333,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDgt333,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDgt333,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDgt333,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDgt333,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDgt333,0,7,0) - 
      0.5*GFOffset(PDgt333,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt333,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDgt333,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDgt333,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDgt333,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDgt333,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDgt333,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDgt333,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDgt333,0,7,0),0) + IfThen(tj 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt333,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDgt333,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDgt333,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDgt333,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDgt333,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDgt333,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDgt333,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDgt333,0,6,0),0) + IfThen(tj 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt333,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(PDgt333,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDgt333,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDgt333,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDgt333,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDgt333,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDgt333,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDgt333,0,5,0),0) + IfThen(tj 
      == 4,0.2734375*GFOffset(PDgt333,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDgt333,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDgt333,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDgt333,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDgt333,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDgt333,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDgt333,0,3,0) - 
      0.2734375*GFOffset(PDgt333,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt333,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDgt333,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDgt333,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDgt333,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDgt333,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDgt333,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDgt333,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDgt333,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(PDgt333,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDgt333,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDgt333,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDgt333,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDgt333,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDgt333,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDgt333,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDgt333,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt333,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDgt333,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDgt333,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDgt333,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDgt333,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDgt333,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDgt333,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDgt333,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDgt333,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDgt333,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDgt333,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDgt333,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDgt333,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDgt333,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDgt333,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDgt333,0,-1,0) + 
      18.*GFOffset(PDgt333,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDgt333,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDgt333,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDgt3333 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDgt333,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDgt333,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDgt333,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDgt333,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDgt333,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDgt333,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDgt333,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDgt333,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDgt333,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDgt333,0,0,7) - 
      0.5*GFOffset(PDgt333,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDgt333,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDgt333,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDgt333,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDgt333,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDgt333,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDgt333,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDgt333,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDgt333,0,0,7),0) + IfThen(tk 
      == 2,0.98536009007450695190732750513*GFOffset(PDgt333,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDgt333,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDgt333,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDgt333,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDgt333,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDgt333,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDgt333,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDgt333,0,0,6),0) + IfThen(tk 
      == 3,-0.444613449281090634955156033*GFOffset(PDgt333,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(PDgt333,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDgt333,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDgt333,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDgt333,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDgt333,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDgt333,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDgt333,0,0,5),0) + IfThen(tk 
      == 4,0.2734375*GFOffset(PDgt333,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDgt333,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDgt333,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDgt333,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDgt333,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDgt333,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDgt333,0,0,3) - 
      0.2734375*GFOffset(PDgt333,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDgt333,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDgt333,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDgt333,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDgt333,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDgt333,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDgt333,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDgt333,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDgt333,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(PDgt333,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDgt333,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDgt333,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDgt333,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDgt333,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDgt333,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDgt333,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDgt333,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDgt333,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDgt333,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDgt333,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDgt333,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDgt333,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDgt333,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDgt333,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDgt333,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDgt333,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDgt333,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDgt333,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDgt333,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDgt333,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDgt333,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDgt333,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDgt333,0,0,-1) + 
      18.*GFOffset(PDgt333,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDgt333,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDgt333,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDalpha11 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDalpha1,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDalpha1,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDalpha1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha1,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha1,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha1,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha1,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha1,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha1,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha1,7,0,0) - 
      0.5*GFOffset(PDalpha1,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha1,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha1,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha1,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha1,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha1,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha1,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha1,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha1,7,0,0),0) + 
      IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha1,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha1,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha1,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha1,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha1,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha1,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha1,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha1,6,0,0),0) + 
      IfThen(ti == 3,-0.444613449281090634955156033*GFOffset(PDalpha1,-3,0,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha1,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha1,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha1,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha1,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha1,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha1,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha1,5,0,0),0) + 
      IfThen(ti == 4,0.2734375*GFOffset(PDalpha1,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha1,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha1,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha1,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha1,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha1,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha1,3,0,0) - 
      0.2734375*GFOffset(PDalpha1,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha1,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha1,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha1,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha1,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha1,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha1,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha1,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha1,3,0,0),0) + IfThen(ti 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha1,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha1,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha1,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha1,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha1,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha1,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha1,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha1,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha1,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha1,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha1,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha1,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha1,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha1,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha1,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha1,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDalpha1,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha1,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha1,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha1,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha1,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha1,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha1,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha1,-1,0,0) + 
      18.*GFOffset(PDalpha1,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDalpha1,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDalpha1,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha12 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDalpha1,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDalpha1,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDalpha1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha1,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha1,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha1,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha1,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha1,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha1,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha1,0,7,0) - 
      0.5*GFOffset(PDalpha1,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha1,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha1,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha1,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha1,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha1,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha1,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha1,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha1,0,7,0),0) + 
      IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha1,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha1,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha1,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha1,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha1,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha1,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha1,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha1,0,6,0),0) + 
      IfThen(tj == 3,-0.444613449281090634955156033*GFOffset(PDalpha1,0,-3,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha1,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha1,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha1,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha1,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha1,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha1,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha1,0,5,0),0) + 
      IfThen(tj == 4,0.2734375*GFOffset(PDalpha1,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha1,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha1,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha1,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha1,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha1,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha1,0,3,0) - 
      0.2734375*GFOffset(PDalpha1,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha1,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha1,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha1,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha1,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha1,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha1,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha1,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha1,0,3,0),0) + IfThen(tj 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha1,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha1,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha1,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha1,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha1,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha1,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha1,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha1,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha1,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha1,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha1,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha1,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha1,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha1,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha1,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha1,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDalpha1,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha1,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha1,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha1,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha1,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha1,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha1,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha1,0,-1,0) + 
      18.*GFOffset(PDalpha1,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDalpha1,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDalpha1,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha13 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDalpha1,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDalpha1,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDalpha1,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha1,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDalpha1,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDalpha1,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDalpha1,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDalpha1,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDalpha1,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDalpha1,0,0,7) - 
      0.5*GFOffset(PDalpha1,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha1,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDalpha1,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDalpha1,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDalpha1,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDalpha1,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDalpha1,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDalpha1,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDalpha1,0,0,7),0) + 
      IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha1,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDalpha1,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDalpha1,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDalpha1,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDalpha1,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDalpha1,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDalpha1,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDalpha1,0,0,6),0) + 
      IfThen(tk == 3,-0.444613449281090634955156033*GFOffset(PDalpha1,0,0,-3) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha1,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDalpha1,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDalpha1,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDalpha1,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDalpha1,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDalpha1,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDalpha1,0,0,5),0) + 
      IfThen(tk == 4,0.2734375*GFOffset(PDalpha1,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDalpha1,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDalpha1,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDalpha1,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDalpha1,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDalpha1,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDalpha1,0,0,3) - 
      0.2734375*GFOffset(PDalpha1,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha1,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDalpha1,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDalpha1,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDalpha1,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDalpha1,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDalpha1,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDalpha1,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDalpha1,0,0,3),0) + IfThen(tk 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha1,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDalpha1,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDalpha1,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDalpha1,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDalpha1,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDalpha1,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDalpha1,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDalpha1,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha1,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDalpha1,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDalpha1,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDalpha1,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDalpha1,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDalpha1,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDalpha1,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDalpha1,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDalpha1,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDalpha1,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDalpha1,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDalpha1,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDalpha1,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDalpha1,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDalpha1,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDalpha1,0,0,-1) + 
      18.*GFOffset(PDalpha1,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDalpha1,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDalpha1,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDalpha21 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDalpha2,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDalpha2,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDalpha2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha2,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha2,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha2,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha2,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha2,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha2,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha2,7,0,0) - 
      0.5*GFOffset(PDalpha2,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha2,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha2,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha2,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha2,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha2,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha2,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha2,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha2,7,0,0),0) + 
      IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha2,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha2,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha2,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha2,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha2,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha2,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha2,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha2,6,0,0),0) + 
      IfThen(ti == 3,-0.444613449281090634955156033*GFOffset(PDalpha2,-3,0,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha2,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha2,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha2,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha2,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha2,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha2,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha2,5,0,0),0) + 
      IfThen(ti == 4,0.2734375*GFOffset(PDalpha2,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha2,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha2,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha2,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha2,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha2,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha2,3,0,0) - 
      0.2734375*GFOffset(PDalpha2,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha2,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha2,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha2,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha2,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha2,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha2,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha2,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha2,3,0,0),0) + IfThen(ti 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha2,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha2,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha2,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha2,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha2,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha2,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha2,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha2,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha2,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha2,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha2,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha2,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha2,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha2,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha2,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha2,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDalpha2,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha2,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha2,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha2,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha2,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha2,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha2,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha2,-1,0,0) + 
      18.*GFOffset(PDalpha2,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDalpha2,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDalpha2,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha22 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDalpha2,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDalpha2,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDalpha2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha2,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha2,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha2,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha2,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha2,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha2,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha2,0,7,0) - 
      0.5*GFOffset(PDalpha2,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha2,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha2,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha2,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha2,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha2,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha2,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha2,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha2,0,7,0),0) + 
      IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha2,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha2,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha2,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha2,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha2,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha2,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha2,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha2,0,6,0),0) + 
      IfThen(tj == 3,-0.444613449281090634955156033*GFOffset(PDalpha2,0,-3,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha2,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha2,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha2,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha2,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha2,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha2,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha2,0,5,0),0) + 
      IfThen(tj == 4,0.2734375*GFOffset(PDalpha2,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha2,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha2,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha2,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha2,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha2,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha2,0,3,0) - 
      0.2734375*GFOffset(PDalpha2,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha2,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha2,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha2,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha2,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha2,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha2,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha2,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha2,0,3,0),0) + IfThen(tj 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha2,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha2,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha2,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha2,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha2,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha2,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha2,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha2,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha2,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha2,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha2,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha2,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha2,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha2,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha2,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha2,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDalpha2,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha2,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha2,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha2,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha2,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha2,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha2,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha2,0,-1,0) + 
      18.*GFOffset(PDalpha2,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDalpha2,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDalpha2,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha23 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDalpha2,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDalpha2,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDalpha2,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha2,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDalpha2,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDalpha2,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDalpha2,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDalpha2,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDalpha2,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDalpha2,0,0,7) - 
      0.5*GFOffset(PDalpha2,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha2,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDalpha2,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDalpha2,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDalpha2,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDalpha2,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDalpha2,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDalpha2,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDalpha2,0,0,7),0) + 
      IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha2,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDalpha2,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDalpha2,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDalpha2,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDalpha2,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDalpha2,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDalpha2,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDalpha2,0,0,6),0) + 
      IfThen(tk == 3,-0.444613449281090634955156033*GFOffset(PDalpha2,0,0,-3) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha2,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDalpha2,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDalpha2,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDalpha2,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDalpha2,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDalpha2,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDalpha2,0,0,5),0) + 
      IfThen(tk == 4,0.2734375*GFOffset(PDalpha2,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDalpha2,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDalpha2,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDalpha2,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDalpha2,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDalpha2,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDalpha2,0,0,3) - 
      0.2734375*GFOffset(PDalpha2,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha2,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDalpha2,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDalpha2,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDalpha2,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDalpha2,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDalpha2,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDalpha2,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDalpha2,0,0,3),0) + IfThen(tk 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha2,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDalpha2,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDalpha2,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDalpha2,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDalpha2,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDalpha2,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDalpha2,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDalpha2,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha2,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDalpha2,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDalpha2,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDalpha2,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDalpha2,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDalpha2,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDalpha2,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDalpha2,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDalpha2,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDalpha2,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDalpha2,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDalpha2,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDalpha2,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDalpha2,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDalpha2,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDalpha2,0,0,-1) + 
      18.*GFOffset(PDalpha2,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDalpha2,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDalpha2,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDPDalpha31 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(PDalpha3,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(PDalpha3,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(PDalpha3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha3,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha3,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha3,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha3,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha3,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha3,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha3,7,0,0) - 
      0.5*GFOffset(PDalpha3,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha3,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha3,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha3,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha3,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha3,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha3,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha3,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha3,7,0,0),0) + 
      IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha3,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha3,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha3,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha3,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha3,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha3,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha3,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha3,6,0,0),0) + 
      IfThen(ti == 3,-0.444613449281090634955156033*GFOffset(PDalpha3,-3,0,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha3,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha3,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha3,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha3,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha3,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha3,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha3,5,0,0),0) + 
      IfThen(ti == 4,0.2734375*GFOffset(PDalpha3,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha3,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha3,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha3,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha3,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha3,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha3,3,0,0) - 
      0.2734375*GFOffset(PDalpha3,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha3,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha3,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha3,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha3,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha3,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha3,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha3,2,0,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha3,3,0,0),0) + IfThen(ti 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha3,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha3,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha3,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha3,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha3,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha3,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha3,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha3,2,0,0),0) + IfThen(ti 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha3,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha3,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha3,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha3,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha3,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha3,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha3,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha3,1,0,0),0) + IfThen(ti 
      == 8,0.5*GFOffset(PDalpha3,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha3,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha3,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha3,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha3,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha3,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha3,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha3,-1,0,0) + 
      18.*GFOffset(PDalpha3,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(PDalpha3,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(PDalpha3,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDPDalpha32 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(PDalpha3,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(PDalpha3,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(PDalpha3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha3,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(PDalpha3,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(PDalpha3,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(PDalpha3,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(PDalpha3,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(PDalpha3,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(PDalpha3,0,7,0) - 
      0.5*GFOffset(PDalpha3,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha3,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(PDalpha3,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(PDalpha3,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(PDalpha3,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(PDalpha3,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(PDalpha3,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(PDalpha3,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(PDalpha3,0,7,0),0) + 
      IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha3,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(PDalpha3,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(PDalpha3,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(PDalpha3,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(PDalpha3,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(PDalpha3,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(PDalpha3,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(PDalpha3,0,6,0),0) + 
      IfThen(tj == 3,-0.444613449281090634955156033*GFOffset(PDalpha3,0,-3,0) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha3,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(PDalpha3,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(PDalpha3,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(PDalpha3,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(PDalpha3,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(PDalpha3,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(PDalpha3,0,5,0),0) + 
      IfThen(tj == 4,0.2734375*GFOffset(PDalpha3,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(PDalpha3,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(PDalpha3,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(PDalpha3,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(PDalpha3,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(PDalpha3,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(PDalpha3,0,3,0) - 
      0.2734375*GFOffset(PDalpha3,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha3,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(PDalpha3,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(PDalpha3,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(PDalpha3,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(PDalpha3,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(PDalpha3,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(PDalpha3,0,2,0) + 
      0.444613449281090634955156033*GFOffset(PDalpha3,0,3,0),0) + IfThen(tj 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha3,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(PDalpha3,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(PDalpha3,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(PDalpha3,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(PDalpha3,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(PDalpha3,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(PDalpha3,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(PDalpha3,0,2,0),0) + IfThen(tj 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha3,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(PDalpha3,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(PDalpha3,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(PDalpha3,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(PDalpha3,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(PDalpha3,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(PDalpha3,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(PDalpha3,0,1,0),0) + IfThen(tj 
      == 8,0.5*GFOffset(PDalpha3,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(PDalpha3,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(PDalpha3,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(PDalpha3,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(PDalpha3,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(PDalpha3,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(PDalpha3,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(PDalpha3,0,-1,0) + 
      18.*GFOffset(PDalpha3,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(PDalpha3,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(PDalpha3,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDPDalpha33 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(PDalpha3,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(PDalpha3,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(PDalpha3,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(PDalpha3,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(PDalpha3,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(PDalpha3,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(PDalpha3,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(PDalpha3,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(PDalpha3,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(PDalpha3,0,0,7) - 
      0.5*GFOffset(PDalpha3,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(PDalpha3,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(PDalpha3,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(PDalpha3,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(PDalpha3,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(PDalpha3,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(PDalpha3,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(PDalpha3,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(PDalpha3,0,0,7),0) + 
      IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(PDalpha3,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(PDalpha3,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(PDalpha3,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(PDalpha3,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(PDalpha3,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(PDalpha3,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(PDalpha3,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(PDalpha3,0,0,6),0) + 
      IfThen(tk == 3,-0.444613449281090634955156033*GFOffset(PDalpha3,0,0,-3) 
      + 1.28796075006390654800826405148*GFOffset(PDalpha3,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(PDalpha3,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(PDalpha3,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(PDalpha3,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(PDalpha3,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(PDalpha3,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(PDalpha3,0,0,5),0) + 
      IfThen(tk == 4,0.2734375*GFOffset(PDalpha3,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(PDalpha3,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(PDalpha3,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(PDalpha3,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(PDalpha3,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(PDalpha3,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(PDalpha3,0,0,3) - 
      0.2734375*GFOffset(PDalpha3,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(PDalpha3,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(PDalpha3,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(PDalpha3,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(PDalpha3,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(PDalpha3,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(PDalpha3,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(PDalpha3,0,0,2) + 
      0.444613449281090634955156033*GFOffset(PDalpha3,0,0,3),0) + IfThen(tk 
      == 6,0.189655591978356440316554839705*GFOffset(PDalpha3,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(PDalpha3,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(PDalpha3,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(PDalpha3,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(PDalpha3,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(PDalpha3,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(PDalpha3,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(PDalpha3,0,0,2),0) + IfThen(tk 
      == 7,-0.215654018702498989385219122479*GFOffset(PDalpha3,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(PDalpha3,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(PDalpha3,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(PDalpha3,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(PDalpha3,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(PDalpha3,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(PDalpha3,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(PDalpha3,0,0,1),0) + IfThen(tk 
      == 8,0.5*GFOffset(PDalpha3,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(PDalpha3,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(PDalpha3,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(PDalpha3,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(PDalpha3,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(PDalpha3,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(PDalpha3,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(PDalpha3,0,0,-1) + 
      18.*GFOffset(PDalpha3,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(PDalpha3,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(PDalpha3,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDPDalpha11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDalpha33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1111 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1112 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1113 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1121 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1122 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1123 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1131 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1132 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1133 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1211 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1212 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1213 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt1333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2211 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2212 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2213 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt2333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3311 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3312 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3313 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3321 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3322 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3323 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDgt3333 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW21 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW31 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW32 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDPDphiW33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDPDphiW11 = J11L*LDPDphiW11 + J21L*LDPDphiW12 + J31L*LDPDphiW13;
      
      PDPDphiW12 = J12L*LDPDphiW11 + J22L*LDPDphiW12 + J32L*LDPDphiW13;
      
      PDPDphiW13 = J13L*LDPDphiW11 + J23L*LDPDphiW12 + J33L*LDPDphiW13;
      
      PDPDphiW21 = J11L*LDPDphiW21 + J21L*LDPDphiW22 + J31L*LDPDphiW23;
      
      PDPDphiW22 = J12L*LDPDphiW21 + J22L*LDPDphiW22 + J32L*LDPDphiW23;
      
      PDPDphiW23 = J13L*LDPDphiW21 + J23L*LDPDphiW22 + J33L*LDPDphiW23;
      
      PDPDphiW31 = J11L*LDPDphiW31 + J21L*LDPDphiW32 + J31L*LDPDphiW33;
      
      PDPDphiW32 = J12L*LDPDphiW31 + J22L*LDPDphiW32 + J32L*LDPDphiW33;
      
      PDPDphiW33 = J13L*LDPDphiW31 + J23L*LDPDphiW32 + J33L*LDPDphiW33;
      
      PDPDgt1111 = J11L*LDPDgt1111 + J21L*LDPDgt1112 + J31L*LDPDgt1113;
      
      PDPDgt1112 = J12L*LDPDgt1111 + J22L*LDPDgt1112 + J32L*LDPDgt1113;
      
      PDPDgt1113 = J13L*LDPDgt1111 + J23L*LDPDgt1112 + J33L*LDPDgt1113;
      
      PDPDgt1121 = J11L*LDPDgt1121 + J21L*LDPDgt1122 + J31L*LDPDgt1123;
      
      PDPDgt1122 = J12L*LDPDgt1121 + J22L*LDPDgt1122 + J32L*LDPDgt1123;
      
      PDPDgt1123 = J13L*LDPDgt1121 + J23L*LDPDgt1122 + J33L*LDPDgt1123;
      
      PDPDgt1131 = J11L*LDPDgt1131 + J21L*LDPDgt1132 + J31L*LDPDgt1133;
      
      PDPDgt1132 = J12L*LDPDgt1131 + J22L*LDPDgt1132 + J32L*LDPDgt1133;
      
      PDPDgt1133 = J13L*LDPDgt1131 + J23L*LDPDgt1132 + J33L*LDPDgt1133;
      
      PDPDgt1211 = J11L*LDPDgt1211 + J21L*LDPDgt1212 + J31L*LDPDgt1213;
      
      PDPDgt1212 = J12L*LDPDgt1211 + J22L*LDPDgt1212 + J32L*LDPDgt1213;
      
      PDPDgt1213 = J13L*LDPDgt1211 + J23L*LDPDgt1212 + J33L*LDPDgt1213;
      
      PDPDgt1221 = J11L*LDPDgt1221 + J21L*LDPDgt1222 + J31L*LDPDgt1223;
      
      PDPDgt1222 = J12L*LDPDgt1221 + J22L*LDPDgt1222 + J32L*LDPDgt1223;
      
      PDPDgt1223 = J13L*LDPDgt1221 + J23L*LDPDgt1222 + J33L*LDPDgt1223;
      
      PDPDgt1231 = J11L*LDPDgt1231 + J21L*LDPDgt1232 + J31L*LDPDgt1233;
      
      PDPDgt1232 = J12L*LDPDgt1231 + J22L*LDPDgt1232 + J32L*LDPDgt1233;
      
      PDPDgt1233 = J13L*LDPDgt1231 + J23L*LDPDgt1232 + J33L*LDPDgt1233;
      
      PDPDgt1311 = J11L*LDPDgt1311 + J21L*LDPDgt1312 + J31L*LDPDgt1313;
      
      PDPDgt1312 = J12L*LDPDgt1311 + J22L*LDPDgt1312 + J32L*LDPDgt1313;
      
      PDPDgt1313 = J13L*LDPDgt1311 + J23L*LDPDgt1312 + J33L*LDPDgt1313;
      
      PDPDgt1321 = J11L*LDPDgt1321 + J21L*LDPDgt1322 + J31L*LDPDgt1323;
      
      PDPDgt1322 = J12L*LDPDgt1321 + J22L*LDPDgt1322 + J32L*LDPDgt1323;
      
      PDPDgt1323 = J13L*LDPDgt1321 + J23L*LDPDgt1322 + J33L*LDPDgt1323;
      
      PDPDgt1331 = J11L*LDPDgt1331 + J21L*LDPDgt1332 + J31L*LDPDgt1333;
      
      PDPDgt1332 = J12L*LDPDgt1331 + J22L*LDPDgt1332 + J32L*LDPDgt1333;
      
      PDPDgt1333 = J13L*LDPDgt1331 + J23L*LDPDgt1332 + J33L*LDPDgt1333;
      
      PDPDgt2211 = J11L*LDPDgt2211 + J21L*LDPDgt2212 + J31L*LDPDgt2213;
      
      PDPDgt2212 = J12L*LDPDgt2211 + J22L*LDPDgt2212 + J32L*LDPDgt2213;
      
      PDPDgt2213 = J13L*LDPDgt2211 + J23L*LDPDgt2212 + J33L*LDPDgt2213;
      
      PDPDgt2221 = J11L*LDPDgt2221 + J21L*LDPDgt2222 + J31L*LDPDgt2223;
      
      PDPDgt2222 = J12L*LDPDgt2221 + J22L*LDPDgt2222 + J32L*LDPDgt2223;
      
      PDPDgt2223 = J13L*LDPDgt2221 + J23L*LDPDgt2222 + J33L*LDPDgt2223;
      
      PDPDgt2231 = J11L*LDPDgt2231 + J21L*LDPDgt2232 + J31L*LDPDgt2233;
      
      PDPDgt2232 = J12L*LDPDgt2231 + J22L*LDPDgt2232 + J32L*LDPDgt2233;
      
      PDPDgt2233 = J13L*LDPDgt2231 + J23L*LDPDgt2232 + J33L*LDPDgt2233;
      
      PDPDgt2311 = J11L*LDPDgt2311 + J21L*LDPDgt2312 + J31L*LDPDgt2313;
      
      PDPDgt2312 = J12L*LDPDgt2311 + J22L*LDPDgt2312 + J32L*LDPDgt2313;
      
      PDPDgt2313 = J13L*LDPDgt2311 + J23L*LDPDgt2312 + J33L*LDPDgt2313;
      
      PDPDgt2321 = J11L*LDPDgt2321 + J21L*LDPDgt2322 + J31L*LDPDgt2323;
      
      PDPDgt2322 = J12L*LDPDgt2321 + J22L*LDPDgt2322 + J32L*LDPDgt2323;
      
      PDPDgt2323 = J13L*LDPDgt2321 + J23L*LDPDgt2322 + J33L*LDPDgt2323;
      
      PDPDgt2331 = J11L*LDPDgt2331 + J21L*LDPDgt2332 + J31L*LDPDgt2333;
      
      PDPDgt2332 = J12L*LDPDgt2331 + J22L*LDPDgt2332 + J32L*LDPDgt2333;
      
      PDPDgt2333 = J13L*LDPDgt2331 + J23L*LDPDgt2332 + J33L*LDPDgt2333;
      
      PDPDgt3311 = J11L*LDPDgt3311 + J21L*LDPDgt3312 + J31L*LDPDgt3313;
      
      PDPDgt3312 = J12L*LDPDgt3311 + J22L*LDPDgt3312 + J32L*LDPDgt3313;
      
      PDPDgt3313 = J13L*LDPDgt3311 + J23L*LDPDgt3312 + J33L*LDPDgt3313;
      
      PDPDgt3321 = J11L*LDPDgt3321 + J21L*LDPDgt3322 + J31L*LDPDgt3323;
      
      PDPDgt3322 = J12L*LDPDgt3321 + J22L*LDPDgt3322 + J32L*LDPDgt3323;
      
      PDPDgt3323 = J13L*LDPDgt3321 + J23L*LDPDgt3322 + J33L*LDPDgt3323;
      
      PDPDgt3331 = J11L*LDPDgt3331 + J21L*LDPDgt3332 + J31L*LDPDgt3333;
      
      PDPDgt3332 = J12L*LDPDgt3331 + J22L*LDPDgt3332 + J32L*LDPDgt3333;
      
      PDPDgt3333 = J13L*LDPDgt3331 + J23L*LDPDgt3332 + J33L*LDPDgt3333;
      
      PDPDalpha11 = J11L*LDPDalpha11 + J21L*LDPDalpha12 + J31L*LDPDalpha13;
      
      PDPDalpha12 = J12L*LDPDalpha11 + J22L*LDPDalpha12 + J32L*LDPDalpha13;
      
      PDPDalpha13 = J13L*LDPDalpha11 + J23L*LDPDalpha12 + J33L*LDPDalpha13;
      
      PDPDalpha22 = J12L*LDPDalpha21 + J22L*LDPDalpha22 + J32L*LDPDalpha23;
      
      PDPDalpha23 = J13L*LDPDalpha21 + J23L*LDPDalpha22 + J33L*LDPDalpha23;
      
      PDPDalpha33 = J13L*LDPDalpha31 + J23L*LDPDalpha32 + J33L*LDPDalpha33;
    }
    else
    {
      PDPDphiW11 = LDPDphiW11;
      
      PDPDphiW12 = LDPDphiW12;
      
      PDPDphiW13 = LDPDphiW13;
      
      PDPDphiW21 = LDPDphiW21;
      
      PDPDphiW22 = LDPDphiW22;
      
      PDPDphiW23 = LDPDphiW23;
      
      PDPDphiW31 = LDPDphiW31;
      
      PDPDphiW32 = LDPDphiW32;
      
      PDPDphiW33 = LDPDphiW33;
      
      PDPDgt1111 = LDPDgt1111;
      
      PDPDgt1112 = LDPDgt1112;
      
      PDPDgt1113 = LDPDgt1113;
      
      PDPDgt1121 = LDPDgt1121;
      
      PDPDgt1122 = LDPDgt1122;
      
      PDPDgt1123 = LDPDgt1123;
      
      PDPDgt1131 = LDPDgt1131;
      
      PDPDgt1132 = LDPDgt1132;
      
      PDPDgt1133 = LDPDgt1133;
      
      PDPDgt1211 = LDPDgt1211;
      
      PDPDgt1212 = LDPDgt1212;
      
      PDPDgt1213 = LDPDgt1213;
      
      PDPDgt1221 = LDPDgt1221;
      
      PDPDgt1222 = LDPDgt1222;
      
      PDPDgt1223 = LDPDgt1223;
      
      PDPDgt1231 = LDPDgt1231;
      
      PDPDgt1232 = LDPDgt1232;
      
      PDPDgt1233 = LDPDgt1233;
      
      PDPDgt1311 = LDPDgt1311;
      
      PDPDgt1312 = LDPDgt1312;
      
      PDPDgt1313 = LDPDgt1313;
      
      PDPDgt1321 = LDPDgt1321;
      
      PDPDgt1322 = LDPDgt1322;
      
      PDPDgt1323 = LDPDgt1323;
      
      PDPDgt1331 = LDPDgt1331;
      
      PDPDgt1332 = LDPDgt1332;
      
      PDPDgt1333 = LDPDgt1333;
      
      PDPDgt2211 = LDPDgt2211;
      
      PDPDgt2212 = LDPDgt2212;
      
      PDPDgt2213 = LDPDgt2213;
      
      PDPDgt2221 = LDPDgt2221;
      
      PDPDgt2222 = LDPDgt2222;
      
      PDPDgt2223 = LDPDgt2223;
      
      PDPDgt2231 = LDPDgt2231;
      
      PDPDgt2232 = LDPDgt2232;
      
      PDPDgt2233 = LDPDgt2233;
      
      PDPDgt2311 = LDPDgt2311;
      
      PDPDgt2312 = LDPDgt2312;
      
      PDPDgt2313 = LDPDgt2313;
      
      PDPDgt2321 = LDPDgt2321;
      
      PDPDgt2322 = LDPDgt2322;
      
      PDPDgt2323 = LDPDgt2323;
      
      PDPDgt2331 = LDPDgt2331;
      
      PDPDgt2332 = LDPDgt2332;
      
      PDPDgt2333 = LDPDgt2333;
      
      PDPDgt3311 = LDPDgt3311;
      
      PDPDgt3312 = LDPDgt3312;
      
      PDPDgt3313 = LDPDgt3313;
      
      PDPDgt3321 = LDPDgt3321;
      
      PDPDgt3322 = LDPDgt3322;
      
      PDPDgt3323 = LDPDgt3323;
      
      PDPDgt3331 = LDPDgt3331;
      
      PDPDgt3332 = LDPDgt3332;
      
      PDPDgt3333 = LDPDgt3333;
      
      PDPDalpha11 = LDPDalpha11;
      
      PDPDalpha12 = LDPDalpha12;
      
      PDPDalpha13 = LDPDalpha13;
      
      PDPDalpha22 = LDPDalpha22;
      
      PDPDalpha23 = LDPDalpha23;
      
      PDPDalpha33 = LDPDalpha33;
    }
    
    CCTK_REAL LDuAt111 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(At11,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(At11,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(At11,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At11,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(At11,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(At11,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(At11,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(At11,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(At11,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(At11,7,0,0) - 
      0.5*GFOffset(At11,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(At11,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(At11,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(At11,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(At11,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(At11,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(At11,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(At11,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(At11,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(At11,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(At11,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(At11,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(At11,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(At11,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(At11,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(At11,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(At11,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(At11,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(At11,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(At11,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(At11,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(At11,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(At11,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(At11,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(At11,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(At11,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(At11,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(At11,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(At11,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(At11,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(At11,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(At11,3,0,0) - 
      0.2734375*GFOffset(At11,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(At11,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(At11,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(At11,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(At11,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(At11,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(At11,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(At11,2,0,0) + 
      0.444613449281090634955156033*GFOffset(At11,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(At11,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(At11,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(At11,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(At11,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(At11,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(At11,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(At11,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(At11,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(At11,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(At11,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(At11,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(At11,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(At11,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(At11,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(At11,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(At11,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(At11,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(At11,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(At11,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(At11,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(At11,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(At11,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(At11,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(At11,-1,0,0) + 
      18.*GFOffset(At11,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(At11,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(At11,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuAt112 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(At11,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(At11,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(At11,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At11,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(At11,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(At11,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(At11,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(At11,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(At11,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(At11,0,7,0) - 
      0.5*GFOffset(At11,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(At11,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(At11,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(At11,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(At11,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(At11,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(At11,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(At11,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(At11,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(At11,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(At11,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(At11,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(At11,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(At11,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(At11,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(At11,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(At11,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(At11,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(At11,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(At11,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(At11,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(At11,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(At11,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(At11,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(At11,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(At11,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(At11,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(At11,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(At11,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(At11,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(At11,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(At11,0,3,0) - 
      0.2734375*GFOffset(At11,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(At11,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(At11,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(At11,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(At11,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(At11,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(At11,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(At11,0,2,0) + 
      0.444613449281090634955156033*GFOffset(At11,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(At11,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(At11,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(At11,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(At11,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(At11,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(At11,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(At11,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(At11,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(At11,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(At11,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(At11,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(At11,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(At11,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(At11,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(At11,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(At11,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(At11,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(At11,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(At11,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(At11,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(At11,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(At11,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(At11,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(At11,0,-1,0) + 
      18.*GFOffset(At11,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(At11,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(At11,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuAt113 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(At11,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(At11,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(At11,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At11,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(At11,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(At11,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(At11,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(At11,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(At11,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(At11,0,0,7) - 
      0.5*GFOffset(At11,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(At11,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(At11,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(At11,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(At11,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(At11,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(At11,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(At11,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(At11,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(At11,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(At11,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(At11,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(At11,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(At11,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(At11,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(At11,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(At11,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(At11,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(At11,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(At11,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(At11,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(At11,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(At11,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(At11,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(At11,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(At11,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(At11,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(At11,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(At11,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(At11,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(At11,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(At11,0,0,3) - 
      0.2734375*GFOffset(At11,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(At11,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(At11,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(At11,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(At11,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(At11,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(At11,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(At11,0,0,2) + 
      0.444613449281090634955156033*GFOffset(At11,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(At11,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(At11,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(At11,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(At11,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(At11,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(At11,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(At11,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(At11,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(At11,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(At11,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(At11,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(At11,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(At11,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(At11,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(At11,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(At11,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(At11,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(At11,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(At11,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(At11,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(At11,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(At11,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(At11,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(At11,0,0,-1) + 
      18.*GFOffset(At11,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(At11,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(At11,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDuAt121 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(At12,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(At12,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(At12,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At12,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(At12,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(At12,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(At12,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(At12,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(At12,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(At12,7,0,0) - 
      0.5*GFOffset(At12,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(At12,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(At12,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(At12,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(At12,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(At12,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(At12,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(At12,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(At12,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(At12,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(At12,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(At12,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(At12,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(At12,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(At12,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(At12,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(At12,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(At12,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(At12,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(At12,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(At12,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(At12,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(At12,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(At12,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(At12,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(At12,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(At12,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(At12,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(At12,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(At12,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(At12,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(At12,3,0,0) - 
      0.2734375*GFOffset(At12,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(At12,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(At12,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(At12,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(At12,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(At12,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(At12,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(At12,2,0,0) + 
      0.444613449281090634955156033*GFOffset(At12,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(At12,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(At12,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(At12,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(At12,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(At12,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(At12,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(At12,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(At12,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(At12,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(At12,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(At12,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(At12,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(At12,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(At12,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(At12,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(At12,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(At12,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(At12,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(At12,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(At12,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(At12,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(At12,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(At12,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(At12,-1,0,0) + 
      18.*GFOffset(At12,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(At12,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(At12,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuAt122 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(At12,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(At12,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(At12,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At12,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(At12,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(At12,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(At12,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(At12,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(At12,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(At12,0,7,0) - 
      0.5*GFOffset(At12,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(At12,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(At12,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(At12,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(At12,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(At12,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(At12,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(At12,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(At12,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(At12,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(At12,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(At12,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(At12,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(At12,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(At12,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(At12,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(At12,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(At12,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(At12,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(At12,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(At12,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(At12,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(At12,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(At12,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(At12,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(At12,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(At12,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(At12,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(At12,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(At12,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(At12,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(At12,0,3,0) - 
      0.2734375*GFOffset(At12,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(At12,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(At12,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(At12,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(At12,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(At12,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(At12,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(At12,0,2,0) + 
      0.444613449281090634955156033*GFOffset(At12,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(At12,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(At12,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(At12,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(At12,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(At12,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(At12,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(At12,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(At12,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(At12,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(At12,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(At12,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(At12,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(At12,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(At12,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(At12,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(At12,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(At12,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(At12,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(At12,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(At12,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(At12,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(At12,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(At12,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(At12,0,-1,0) + 
      18.*GFOffset(At12,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(At12,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(At12,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuAt123 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(At12,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(At12,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(At12,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At12,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(At12,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(At12,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(At12,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(At12,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(At12,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(At12,0,0,7) - 
      0.5*GFOffset(At12,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(At12,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(At12,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(At12,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(At12,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(At12,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(At12,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(At12,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(At12,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(At12,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(At12,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(At12,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(At12,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(At12,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(At12,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(At12,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(At12,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(At12,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(At12,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(At12,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(At12,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(At12,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(At12,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(At12,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(At12,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(At12,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(At12,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(At12,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(At12,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(At12,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(At12,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(At12,0,0,3) - 
      0.2734375*GFOffset(At12,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(At12,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(At12,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(At12,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(At12,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(At12,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(At12,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(At12,0,0,2) + 
      0.444613449281090634955156033*GFOffset(At12,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(At12,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(At12,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(At12,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(At12,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(At12,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(At12,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(At12,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(At12,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(At12,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(At12,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(At12,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(At12,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(At12,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(At12,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(At12,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(At12,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(At12,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(At12,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(At12,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(At12,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(At12,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(At12,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(At12,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(At12,0,0,-1) + 
      18.*GFOffset(At12,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(At12,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(At12,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDuAt131 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(At13,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(At13,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(At13,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At13,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(At13,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(At13,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(At13,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(At13,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(At13,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(At13,7,0,0) - 
      0.5*GFOffset(At13,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(At13,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(At13,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(At13,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(At13,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(At13,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(At13,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(At13,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(At13,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(At13,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(At13,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(At13,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(At13,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(At13,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(At13,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(At13,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(At13,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(At13,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(At13,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(At13,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(At13,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(At13,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(At13,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(At13,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(At13,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(At13,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(At13,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(At13,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(At13,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(At13,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(At13,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(At13,3,0,0) - 
      0.2734375*GFOffset(At13,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(At13,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(At13,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(At13,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(At13,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(At13,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(At13,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(At13,2,0,0) + 
      0.444613449281090634955156033*GFOffset(At13,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(At13,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(At13,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(At13,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(At13,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(At13,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(At13,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(At13,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(At13,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(At13,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(At13,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(At13,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(At13,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(At13,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(At13,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(At13,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(At13,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(At13,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(At13,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(At13,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(At13,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(At13,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(At13,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(At13,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(At13,-1,0,0) + 
      18.*GFOffset(At13,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(At13,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(At13,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuAt132 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(At13,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(At13,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(At13,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At13,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(At13,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(At13,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(At13,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(At13,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(At13,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(At13,0,7,0) - 
      0.5*GFOffset(At13,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(At13,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(At13,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(At13,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(At13,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(At13,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(At13,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(At13,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(At13,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(At13,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(At13,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(At13,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(At13,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(At13,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(At13,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(At13,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(At13,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(At13,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(At13,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(At13,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(At13,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(At13,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(At13,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(At13,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(At13,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(At13,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(At13,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(At13,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(At13,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(At13,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(At13,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(At13,0,3,0) - 
      0.2734375*GFOffset(At13,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(At13,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(At13,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(At13,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(At13,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(At13,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(At13,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(At13,0,2,0) + 
      0.444613449281090634955156033*GFOffset(At13,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(At13,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(At13,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(At13,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(At13,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(At13,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(At13,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(At13,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(At13,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(At13,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(At13,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(At13,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(At13,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(At13,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(At13,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(At13,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(At13,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(At13,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(At13,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(At13,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(At13,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(At13,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(At13,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(At13,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(At13,0,-1,0) + 
      18.*GFOffset(At13,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(At13,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(At13,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuAt133 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(At13,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(At13,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(At13,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At13,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(At13,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(At13,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(At13,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(At13,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(At13,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(At13,0,0,7) - 
      0.5*GFOffset(At13,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(At13,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(At13,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(At13,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(At13,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(At13,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(At13,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(At13,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(At13,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(At13,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(At13,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(At13,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(At13,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(At13,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(At13,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(At13,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(At13,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(At13,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(At13,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(At13,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(At13,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(At13,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(At13,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(At13,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(At13,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(At13,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(At13,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(At13,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(At13,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(At13,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(At13,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(At13,0,0,3) - 
      0.2734375*GFOffset(At13,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(At13,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(At13,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(At13,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(At13,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(At13,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(At13,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(At13,0,0,2) + 
      0.444613449281090634955156033*GFOffset(At13,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(At13,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(At13,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(At13,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(At13,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(At13,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(At13,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(At13,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(At13,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(At13,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(At13,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(At13,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(At13,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(At13,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(At13,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(At13,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(At13,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(At13,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(At13,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(At13,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(At13,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(At13,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(At13,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(At13,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(At13,0,0,-1) + 
      18.*GFOffset(At13,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(At13,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(At13,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDuAt221 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(At22,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(At22,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(At22,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At22,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(At22,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(At22,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(At22,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(At22,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(At22,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(At22,7,0,0) - 
      0.5*GFOffset(At22,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(At22,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(At22,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(At22,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(At22,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(At22,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(At22,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(At22,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(At22,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(At22,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(At22,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(At22,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(At22,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(At22,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(At22,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(At22,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(At22,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(At22,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(At22,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(At22,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(At22,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(At22,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(At22,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(At22,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(At22,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(At22,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(At22,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(At22,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(At22,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(At22,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(At22,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(At22,3,0,0) - 
      0.2734375*GFOffset(At22,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(At22,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(At22,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(At22,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(At22,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(At22,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(At22,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(At22,2,0,0) + 
      0.444613449281090634955156033*GFOffset(At22,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(At22,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(At22,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(At22,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(At22,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(At22,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(At22,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(At22,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(At22,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(At22,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(At22,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(At22,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(At22,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(At22,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(At22,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(At22,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(At22,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(At22,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(At22,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(At22,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(At22,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(At22,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(At22,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(At22,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(At22,-1,0,0) + 
      18.*GFOffset(At22,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(At22,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(At22,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuAt222 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(At22,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(At22,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(At22,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At22,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(At22,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(At22,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(At22,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(At22,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(At22,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(At22,0,7,0) - 
      0.5*GFOffset(At22,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(At22,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(At22,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(At22,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(At22,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(At22,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(At22,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(At22,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(At22,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(At22,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(At22,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(At22,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(At22,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(At22,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(At22,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(At22,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(At22,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(At22,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(At22,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(At22,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(At22,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(At22,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(At22,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(At22,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(At22,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(At22,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(At22,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(At22,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(At22,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(At22,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(At22,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(At22,0,3,0) - 
      0.2734375*GFOffset(At22,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(At22,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(At22,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(At22,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(At22,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(At22,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(At22,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(At22,0,2,0) + 
      0.444613449281090634955156033*GFOffset(At22,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(At22,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(At22,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(At22,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(At22,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(At22,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(At22,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(At22,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(At22,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(At22,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(At22,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(At22,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(At22,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(At22,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(At22,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(At22,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(At22,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(At22,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(At22,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(At22,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(At22,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(At22,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(At22,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(At22,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(At22,0,-1,0) + 
      18.*GFOffset(At22,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(At22,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(At22,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuAt223 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(At22,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(At22,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(At22,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At22,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(At22,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(At22,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(At22,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(At22,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(At22,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(At22,0,0,7) - 
      0.5*GFOffset(At22,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(At22,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(At22,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(At22,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(At22,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(At22,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(At22,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(At22,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(At22,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(At22,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(At22,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(At22,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(At22,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(At22,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(At22,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(At22,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(At22,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(At22,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(At22,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(At22,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(At22,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(At22,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(At22,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(At22,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(At22,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(At22,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(At22,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(At22,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(At22,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(At22,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(At22,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(At22,0,0,3) - 
      0.2734375*GFOffset(At22,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(At22,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(At22,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(At22,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(At22,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(At22,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(At22,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(At22,0,0,2) + 
      0.444613449281090634955156033*GFOffset(At22,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(At22,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(At22,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(At22,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(At22,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(At22,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(At22,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(At22,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(At22,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(At22,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(At22,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(At22,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(At22,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(At22,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(At22,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(At22,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(At22,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(At22,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(At22,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(At22,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(At22,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(At22,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(At22,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(At22,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(At22,0,0,-1) + 
      18.*GFOffset(At22,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(At22,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(At22,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDuAt231 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(At23,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(At23,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(At23,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At23,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(At23,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(At23,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(At23,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(At23,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(At23,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(At23,7,0,0) - 
      0.5*GFOffset(At23,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(At23,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(At23,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(At23,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(At23,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(At23,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(At23,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(At23,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(At23,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(At23,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(At23,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(At23,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(At23,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(At23,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(At23,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(At23,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(At23,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(At23,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(At23,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(At23,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(At23,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(At23,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(At23,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(At23,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(At23,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(At23,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(At23,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(At23,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(At23,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(At23,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(At23,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(At23,3,0,0) - 
      0.2734375*GFOffset(At23,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(At23,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(At23,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(At23,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(At23,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(At23,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(At23,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(At23,2,0,0) + 
      0.444613449281090634955156033*GFOffset(At23,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(At23,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(At23,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(At23,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(At23,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(At23,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(At23,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(At23,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(At23,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(At23,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(At23,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(At23,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(At23,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(At23,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(At23,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(At23,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(At23,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(At23,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(At23,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(At23,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(At23,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(At23,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(At23,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(At23,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(At23,-1,0,0) + 
      18.*GFOffset(At23,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(At23,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(At23,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuAt232 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(At23,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(At23,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(At23,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At23,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(At23,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(At23,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(At23,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(At23,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(At23,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(At23,0,7,0) - 
      0.5*GFOffset(At23,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(At23,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(At23,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(At23,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(At23,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(At23,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(At23,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(At23,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(At23,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(At23,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(At23,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(At23,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(At23,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(At23,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(At23,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(At23,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(At23,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(At23,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(At23,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(At23,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(At23,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(At23,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(At23,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(At23,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(At23,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(At23,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(At23,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(At23,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(At23,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(At23,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(At23,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(At23,0,3,0) - 
      0.2734375*GFOffset(At23,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(At23,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(At23,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(At23,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(At23,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(At23,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(At23,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(At23,0,2,0) + 
      0.444613449281090634955156033*GFOffset(At23,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(At23,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(At23,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(At23,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(At23,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(At23,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(At23,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(At23,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(At23,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(At23,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(At23,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(At23,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(At23,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(At23,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(At23,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(At23,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(At23,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(At23,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(At23,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(At23,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(At23,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(At23,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(At23,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(At23,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(At23,0,-1,0) + 
      18.*GFOffset(At23,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(At23,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(At23,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuAt233 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(At23,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(At23,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(At23,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At23,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(At23,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(At23,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(At23,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(At23,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(At23,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(At23,0,0,7) - 
      0.5*GFOffset(At23,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(At23,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(At23,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(At23,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(At23,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(At23,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(At23,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(At23,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(At23,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(At23,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(At23,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(At23,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(At23,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(At23,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(At23,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(At23,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(At23,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(At23,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(At23,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(At23,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(At23,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(At23,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(At23,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(At23,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(At23,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(At23,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(At23,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(At23,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(At23,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(At23,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(At23,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(At23,0,0,3) - 
      0.2734375*GFOffset(At23,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(At23,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(At23,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(At23,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(At23,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(At23,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(At23,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(At23,0,0,2) + 
      0.444613449281090634955156033*GFOffset(At23,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(At23,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(At23,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(At23,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(At23,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(At23,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(At23,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(At23,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(At23,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(At23,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(At23,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(At23,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(At23,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(At23,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(At23,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(At23,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(At23,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(At23,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(At23,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(At23,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(At23,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(At23,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(At23,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(At23,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(At23,0,0,-1) + 
      18.*GFOffset(At23,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(At23,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(At23,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL LDuAt331 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(ti == 
      0,-36.*GFOffset(At33,0,0,0),0) + IfThen(ti == 
      8,36.*GFOffset(At33,0,0,0),0))*pow(dx,-1) + 
      0.222222222222222222222222222222*(IfThen(ti == 
      0,-18.*GFOffset(At33,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At33,1,0,0) - 
      9.7387016572115472650292513563*GFOffset(At33,2,0,0) + 
      5.5449639069493784995607676809*GFOffset(At33,3,0,0) - 
      3.6571428571428571428571428571*GFOffset(At33,4,0,0) + 
      2.59074567655935499218591558478*GFOffset(At33,5,0,0) - 
      1.87444087344698324367585844334*GFOffset(At33,6,0,0) + 
      1.28483063269958833334100425314*GFOffset(At33,7,0,0) - 
      0.5*GFOffset(At33,8,0,0),0) + IfThen(ti == 
      1,-4.0870137020336765889793098481*GFOffset(At33,-1,0,0) + 
      5.7868058166373116740903723405*GFOffset(At33,1,0,0) - 
      2.69606544031405602899382047687*GFOffset(At33,2,0,0) + 
      1.66522164500538518079547523473*GFOffset(At33,3,0,0) - 
      1.14565373845513231901250100842*GFOffset(At33,4,0,0) + 
      0.81675638174138587811723062895*GFOffset(At33,5,0,0) - 
      0.55570498128371678540266599324*GFOffset(At33,6,0,0) + 
      0.215654018702498989385219122479*GFOffset(At33,7,0,0),0) + IfThen(ti == 
      2,0.98536009007450695190732750513*GFOffset(At33,-2,0,0) - 
      3.4883587534344548411598315601*GFOffset(At33,-1,0,0) + 
      3.5766809401256153210044855557*GFOffset(At33,1,0,0) - 
      1.71783215719506277794780854135*GFOffset(At33,2,0,0) + 
      1.07980381128263048444543497939*GFOffset(At33,3,0,0) - 
      0.73834927719038611665282848824*GFOffset(At33,4,0,0) + 
      0.49235093831550741871977538918*GFOffset(At33,5,0,0) - 
      0.189655591978356440316554839705*GFOffset(At33,6,0,0),0) + IfThen(ti == 
      3,-0.444613449281090634955156033*GFOffset(At33,-3,0,0) + 
      1.28796075006390654800826405148*GFOffset(At33,-2,0,0) - 
      2.83445891207942039532716103713*GFOffset(At33,-1,0,0) + 
      2.85191596846289538830749160288*GFOffset(At33,1,0,0) - 
      1.37696489376051208934447138421*GFOffset(At33,2,0,0) + 
      0.85572618509267540469369404148*GFOffset(At33,3,0,0) - 
      0.5473001605340514002868585827*GFOffset(At33,4,0,0) + 
      0.207734512035597178904197341203*GFOffset(At33,5,0,0),0) + IfThen(ti == 
      4,0.2734375*GFOffset(At33,-4,0,0) - 
      0.74178239791625424057639927034*GFOffset(At33,-3,0,0) + 
      1.26941308635814953242157409323*GFOffset(At33,-2,0,0) - 
      2.65931021757391799292835532816*GFOffset(At33,-1,0,0) + 
      2.65931021757391799292835532816*GFOffset(At33,1,0,0) - 
      1.26941308635814953242157409323*GFOffset(At33,2,0,0) + 
      0.74178239791625424057639927034*GFOffset(At33,3,0,0) - 
      0.2734375*GFOffset(At33,4,0,0),0) + IfThen(ti == 
      5,-0.207734512035597178904197341203*GFOffset(At33,-5,0,0) + 
      0.5473001605340514002868585827*GFOffset(At33,-4,0,0) - 
      0.85572618509267540469369404148*GFOffset(At33,-3,0,0) + 
      1.37696489376051208934447138421*GFOffset(At33,-2,0,0) - 
      2.85191596846289538830749160288*GFOffset(At33,-1,0,0) + 
      2.83445891207942039532716103713*GFOffset(At33,1,0,0) - 
      1.28796075006390654800826405148*GFOffset(At33,2,0,0) + 
      0.444613449281090634955156033*GFOffset(At33,3,0,0),0) + IfThen(ti == 
      6,0.189655591978356440316554839705*GFOffset(At33,-6,0,0) - 
      0.49235093831550741871977538918*GFOffset(At33,-5,0,0) + 
      0.73834927719038611665282848824*GFOffset(At33,-4,0,0) - 
      1.07980381128263048444543497939*GFOffset(At33,-3,0,0) + 
      1.71783215719506277794780854135*GFOffset(At33,-2,0,0) - 
      3.5766809401256153210044855557*GFOffset(At33,-1,0,0) + 
      3.4883587534344548411598315601*GFOffset(At33,1,0,0) - 
      0.98536009007450695190732750513*GFOffset(At33,2,0,0),0) + IfThen(ti == 
      7,-0.215654018702498989385219122479*GFOffset(At33,-7,0,0) + 
      0.55570498128371678540266599324*GFOffset(At33,-6,0,0) - 
      0.81675638174138587811723062895*GFOffset(At33,-5,0,0) + 
      1.14565373845513231901250100842*GFOffset(At33,-4,0,0) - 
      1.66522164500538518079547523473*GFOffset(At33,-3,0,0) + 
      2.69606544031405602899382047687*GFOffset(At33,-2,0,0) - 
      5.7868058166373116740903723405*GFOffset(At33,-1,0,0) + 
      4.0870137020336765889793098481*GFOffset(At33,1,0,0),0) + IfThen(ti == 
      8,0.5*GFOffset(At33,-8,0,0) - 
      1.28483063269958833334100425314*GFOffset(At33,-7,0,0) + 
      1.87444087344698324367585844334*GFOffset(At33,-6,0,0) - 
      2.59074567655935499218591558478*GFOffset(At33,-5,0,0) + 
      3.6571428571428571428571428571*GFOffset(At33,-4,0,0) - 
      5.5449639069493784995607676809*GFOffset(At33,-3,0,0) + 
      9.7387016572115472650292513563*GFOffset(At33,-2,0,0) - 
      24.3497451715930658264745651379*GFOffset(At33,-1,0,0) + 
      18.*GFOffset(At33,0,0,0),0))*pow(dx,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(ti == 
      0,36.*GFOffset(At33,-1,0,0),0) + IfThen(ti == 
      8,-36.*GFOffset(At33,1,0,0),0))*pow(dx,-1);
    
    CCTK_REAL LDuAt332 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tj == 
      0,-36.*GFOffset(At33,0,0,0),0) + IfThen(tj == 
      8,36.*GFOffset(At33,0,0,0),0))*pow(dy,-1) + 
      0.222222222222222222222222222222*(IfThen(tj == 
      0,-18.*GFOffset(At33,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At33,0,1,0) - 
      9.7387016572115472650292513563*GFOffset(At33,0,2,0) + 
      5.5449639069493784995607676809*GFOffset(At33,0,3,0) - 
      3.6571428571428571428571428571*GFOffset(At33,0,4,0) + 
      2.59074567655935499218591558478*GFOffset(At33,0,5,0) - 
      1.87444087344698324367585844334*GFOffset(At33,0,6,0) + 
      1.28483063269958833334100425314*GFOffset(At33,0,7,0) - 
      0.5*GFOffset(At33,0,8,0),0) + IfThen(tj == 
      1,-4.0870137020336765889793098481*GFOffset(At33,0,-1,0) + 
      5.7868058166373116740903723405*GFOffset(At33,0,1,0) - 
      2.69606544031405602899382047687*GFOffset(At33,0,2,0) + 
      1.66522164500538518079547523473*GFOffset(At33,0,3,0) - 
      1.14565373845513231901250100842*GFOffset(At33,0,4,0) + 
      0.81675638174138587811723062895*GFOffset(At33,0,5,0) - 
      0.55570498128371678540266599324*GFOffset(At33,0,6,0) + 
      0.215654018702498989385219122479*GFOffset(At33,0,7,0),0) + IfThen(tj == 
      2,0.98536009007450695190732750513*GFOffset(At33,0,-2,0) - 
      3.4883587534344548411598315601*GFOffset(At33,0,-1,0) + 
      3.5766809401256153210044855557*GFOffset(At33,0,1,0) - 
      1.71783215719506277794780854135*GFOffset(At33,0,2,0) + 
      1.07980381128263048444543497939*GFOffset(At33,0,3,0) - 
      0.73834927719038611665282848824*GFOffset(At33,0,4,0) + 
      0.49235093831550741871977538918*GFOffset(At33,0,5,0) - 
      0.189655591978356440316554839705*GFOffset(At33,0,6,0),0) + IfThen(tj == 
      3,-0.444613449281090634955156033*GFOffset(At33,0,-3,0) + 
      1.28796075006390654800826405148*GFOffset(At33,0,-2,0) - 
      2.83445891207942039532716103713*GFOffset(At33,0,-1,0) + 
      2.85191596846289538830749160288*GFOffset(At33,0,1,0) - 
      1.37696489376051208934447138421*GFOffset(At33,0,2,0) + 
      0.85572618509267540469369404148*GFOffset(At33,0,3,0) - 
      0.5473001605340514002868585827*GFOffset(At33,0,4,0) + 
      0.207734512035597178904197341203*GFOffset(At33,0,5,0),0) + IfThen(tj == 
      4,0.2734375*GFOffset(At33,0,-4,0) - 
      0.74178239791625424057639927034*GFOffset(At33,0,-3,0) + 
      1.26941308635814953242157409323*GFOffset(At33,0,-2,0) - 
      2.65931021757391799292835532816*GFOffset(At33,0,-1,0) + 
      2.65931021757391799292835532816*GFOffset(At33,0,1,0) - 
      1.26941308635814953242157409323*GFOffset(At33,0,2,0) + 
      0.74178239791625424057639927034*GFOffset(At33,0,3,0) - 
      0.2734375*GFOffset(At33,0,4,0),0) + IfThen(tj == 
      5,-0.207734512035597178904197341203*GFOffset(At33,0,-5,0) + 
      0.5473001605340514002868585827*GFOffset(At33,0,-4,0) - 
      0.85572618509267540469369404148*GFOffset(At33,0,-3,0) + 
      1.37696489376051208934447138421*GFOffset(At33,0,-2,0) - 
      2.85191596846289538830749160288*GFOffset(At33,0,-1,0) + 
      2.83445891207942039532716103713*GFOffset(At33,0,1,0) - 
      1.28796075006390654800826405148*GFOffset(At33,0,2,0) + 
      0.444613449281090634955156033*GFOffset(At33,0,3,0),0) + IfThen(tj == 
      6,0.189655591978356440316554839705*GFOffset(At33,0,-6,0) - 
      0.49235093831550741871977538918*GFOffset(At33,0,-5,0) + 
      0.73834927719038611665282848824*GFOffset(At33,0,-4,0) - 
      1.07980381128263048444543497939*GFOffset(At33,0,-3,0) + 
      1.71783215719506277794780854135*GFOffset(At33,0,-2,0) - 
      3.5766809401256153210044855557*GFOffset(At33,0,-1,0) + 
      3.4883587534344548411598315601*GFOffset(At33,0,1,0) - 
      0.98536009007450695190732750513*GFOffset(At33,0,2,0),0) + IfThen(tj == 
      7,-0.215654018702498989385219122479*GFOffset(At33,0,-7,0) + 
      0.55570498128371678540266599324*GFOffset(At33,0,-6,0) - 
      0.81675638174138587811723062895*GFOffset(At33,0,-5,0) + 
      1.14565373845513231901250100842*GFOffset(At33,0,-4,0) - 
      1.66522164500538518079547523473*GFOffset(At33,0,-3,0) + 
      2.69606544031405602899382047687*GFOffset(At33,0,-2,0) - 
      5.7868058166373116740903723405*GFOffset(At33,0,-1,0) + 
      4.0870137020336765889793098481*GFOffset(At33,0,1,0),0) + IfThen(tj == 
      8,0.5*GFOffset(At33,0,-8,0) - 
      1.28483063269958833334100425314*GFOffset(At33,0,-7,0) + 
      1.87444087344698324367585844334*GFOffset(At33,0,-6,0) - 
      2.59074567655935499218591558478*GFOffset(At33,0,-5,0) + 
      3.6571428571428571428571428571*GFOffset(At33,0,-4,0) - 
      5.5449639069493784995607676809*GFOffset(At33,0,-3,0) + 
      9.7387016572115472650292513563*GFOffset(At33,0,-2,0) - 
      24.3497451715930658264745651379*GFOffset(At33,0,-1,0) + 
      18.*GFOffset(At33,0,0,0),0))*pow(dy,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tj == 
      0,36.*GFOffset(At33,0,-1,0),0) + IfThen(tj == 
      8,-36.*GFOffset(At33,0,1,0),0))*pow(dy,-1);
    
    CCTK_REAL LDuAt333 CCTK_ATTRIBUTE_UNUSED = 
      -0.111111111111111111111111111111*(IfThen(tk == 
      0,-36.*GFOffset(At33,0,0,0),0) + IfThen(tk == 
      8,36.*GFOffset(At33,0,0,0),0))*pow(dz,-1) + 
      0.222222222222222222222222222222*(IfThen(tk == 
      0,-18.*GFOffset(At33,0,0,0) + 
      24.3497451715930658264745651379*GFOffset(At33,0,0,1) - 
      9.7387016572115472650292513563*GFOffset(At33,0,0,2) + 
      5.5449639069493784995607676809*GFOffset(At33,0,0,3) - 
      3.6571428571428571428571428571*GFOffset(At33,0,0,4) + 
      2.59074567655935499218591558478*GFOffset(At33,0,0,5) - 
      1.87444087344698324367585844334*GFOffset(At33,0,0,6) + 
      1.28483063269958833334100425314*GFOffset(At33,0,0,7) - 
      0.5*GFOffset(At33,0,0,8),0) + IfThen(tk == 
      1,-4.0870137020336765889793098481*GFOffset(At33,0,0,-1) + 
      5.7868058166373116740903723405*GFOffset(At33,0,0,1) - 
      2.69606544031405602899382047687*GFOffset(At33,0,0,2) + 
      1.66522164500538518079547523473*GFOffset(At33,0,0,3) - 
      1.14565373845513231901250100842*GFOffset(At33,0,0,4) + 
      0.81675638174138587811723062895*GFOffset(At33,0,0,5) - 
      0.55570498128371678540266599324*GFOffset(At33,0,0,6) + 
      0.215654018702498989385219122479*GFOffset(At33,0,0,7),0) + IfThen(tk == 
      2,0.98536009007450695190732750513*GFOffset(At33,0,0,-2) - 
      3.4883587534344548411598315601*GFOffset(At33,0,0,-1) + 
      3.5766809401256153210044855557*GFOffset(At33,0,0,1) - 
      1.71783215719506277794780854135*GFOffset(At33,0,0,2) + 
      1.07980381128263048444543497939*GFOffset(At33,0,0,3) - 
      0.73834927719038611665282848824*GFOffset(At33,0,0,4) + 
      0.49235093831550741871977538918*GFOffset(At33,0,0,5) - 
      0.189655591978356440316554839705*GFOffset(At33,0,0,6),0) + IfThen(tk == 
      3,-0.444613449281090634955156033*GFOffset(At33,0,0,-3) + 
      1.28796075006390654800826405148*GFOffset(At33,0,0,-2) - 
      2.83445891207942039532716103713*GFOffset(At33,0,0,-1) + 
      2.85191596846289538830749160288*GFOffset(At33,0,0,1) - 
      1.37696489376051208934447138421*GFOffset(At33,0,0,2) + 
      0.85572618509267540469369404148*GFOffset(At33,0,0,3) - 
      0.5473001605340514002868585827*GFOffset(At33,0,0,4) + 
      0.207734512035597178904197341203*GFOffset(At33,0,0,5),0) + IfThen(tk == 
      4,0.2734375*GFOffset(At33,0,0,-4) - 
      0.74178239791625424057639927034*GFOffset(At33,0,0,-3) + 
      1.26941308635814953242157409323*GFOffset(At33,0,0,-2) - 
      2.65931021757391799292835532816*GFOffset(At33,0,0,-1) + 
      2.65931021757391799292835532816*GFOffset(At33,0,0,1) - 
      1.26941308635814953242157409323*GFOffset(At33,0,0,2) + 
      0.74178239791625424057639927034*GFOffset(At33,0,0,3) - 
      0.2734375*GFOffset(At33,0,0,4),0) + IfThen(tk == 
      5,-0.207734512035597178904197341203*GFOffset(At33,0,0,-5) + 
      0.5473001605340514002868585827*GFOffset(At33,0,0,-4) - 
      0.85572618509267540469369404148*GFOffset(At33,0,0,-3) + 
      1.37696489376051208934447138421*GFOffset(At33,0,0,-2) - 
      2.85191596846289538830749160288*GFOffset(At33,0,0,-1) + 
      2.83445891207942039532716103713*GFOffset(At33,0,0,1) - 
      1.28796075006390654800826405148*GFOffset(At33,0,0,2) + 
      0.444613449281090634955156033*GFOffset(At33,0,0,3),0) + IfThen(tk == 
      6,0.189655591978356440316554839705*GFOffset(At33,0,0,-6) - 
      0.49235093831550741871977538918*GFOffset(At33,0,0,-5) + 
      0.73834927719038611665282848824*GFOffset(At33,0,0,-4) - 
      1.07980381128263048444543497939*GFOffset(At33,0,0,-3) + 
      1.71783215719506277794780854135*GFOffset(At33,0,0,-2) - 
      3.5766809401256153210044855557*GFOffset(At33,0,0,-1) + 
      3.4883587534344548411598315601*GFOffset(At33,0,0,1) - 
      0.98536009007450695190732750513*GFOffset(At33,0,0,2),0) + IfThen(tk == 
      7,-0.215654018702498989385219122479*GFOffset(At33,0,0,-7) + 
      0.55570498128371678540266599324*GFOffset(At33,0,0,-6) - 
      0.81675638174138587811723062895*GFOffset(At33,0,0,-5) + 
      1.14565373845513231901250100842*GFOffset(At33,0,0,-4) - 
      1.66522164500538518079547523473*GFOffset(At33,0,0,-3) + 
      2.69606544031405602899382047687*GFOffset(At33,0,0,-2) - 
      5.7868058166373116740903723405*GFOffset(At33,0,0,-1) + 
      4.0870137020336765889793098481*GFOffset(At33,0,0,1),0) + IfThen(tk == 
      8,0.5*GFOffset(At33,0,0,-8) - 
      1.28483063269958833334100425314*GFOffset(At33,0,0,-7) + 
      1.87444087344698324367585844334*GFOffset(At33,0,0,-6) - 
      2.59074567655935499218591558478*GFOffset(At33,0,0,-5) + 
      3.6571428571428571428571428571*GFOffset(At33,0,0,-4) - 
      5.5449639069493784995607676809*GFOffset(At33,0,0,-3) + 
      9.7387016572115472650292513563*GFOffset(At33,0,0,-2) - 
      24.3497451715930658264745651379*GFOffset(At33,0,0,-1) + 
      18.*GFOffset(At33,0,0,0),0))*pow(dz,-1) - 
      0.222222222222222222222222222222*alphaDeriv*(IfThen(tk == 
      0,36.*GFOffset(At33,0,0,-1),0) + IfThen(tk == 
      8,-36.*GFOffset(At33,0,0,1),0))*pow(dz,-1);
    
    CCTK_REAL PDuAt111 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt112 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt113 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt121 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt122 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt123 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt131 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt132 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt133 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt221 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt222 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt223 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt231 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt232 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt233 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt331 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt332 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL PDuAt333 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      PDuAt111 = J11L*LDuAt111 + J21L*LDuAt112 + J31L*LDuAt113;
      
      PDuAt112 = J12L*LDuAt111 + J22L*LDuAt112 + J32L*LDuAt113;
      
      PDuAt113 = J13L*LDuAt111 + J23L*LDuAt112 + J33L*LDuAt113;
      
      PDuAt121 = J11L*LDuAt121 + J21L*LDuAt122 + J31L*LDuAt123;
      
      PDuAt122 = J12L*LDuAt121 + J22L*LDuAt122 + J32L*LDuAt123;
      
      PDuAt123 = J13L*LDuAt121 + J23L*LDuAt122 + J33L*LDuAt123;
      
      PDuAt131 = J11L*LDuAt131 + J21L*LDuAt132 + J31L*LDuAt133;
      
      PDuAt132 = J12L*LDuAt131 + J22L*LDuAt132 + J32L*LDuAt133;
      
      PDuAt133 = J13L*LDuAt131 + J23L*LDuAt132 + J33L*LDuAt133;
      
      PDuAt221 = J11L*LDuAt221 + J21L*LDuAt222 + J31L*LDuAt223;
      
      PDuAt222 = J12L*LDuAt221 + J22L*LDuAt222 + J32L*LDuAt223;
      
      PDuAt223 = J13L*LDuAt221 + J23L*LDuAt222 + J33L*LDuAt223;
      
      PDuAt231 = J11L*LDuAt231 + J21L*LDuAt232 + J31L*LDuAt233;
      
      PDuAt232 = J12L*LDuAt231 + J22L*LDuAt232 + J32L*LDuAt233;
      
      PDuAt233 = J13L*LDuAt231 + J23L*LDuAt232 + J33L*LDuAt233;
      
      PDuAt331 = J11L*LDuAt331 + J21L*LDuAt332 + J31L*LDuAt333;
      
      PDuAt332 = J12L*LDuAt331 + J22L*LDuAt332 + J32L*LDuAt333;
      
      PDuAt333 = J13L*LDuAt331 + J23L*LDuAt332 + J33L*LDuAt333;
    }
    else
    {
      PDuAt111 = LDuAt111;
      
      PDuAt112 = LDuAt112;
      
      PDuAt113 = LDuAt113;
      
      PDuAt121 = LDuAt121;
      
      PDuAt122 = LDuAt122;
      
      PDuAt123 = LDuAt123;
      
      PDuAt131 = LDuAt131;
      
      PDuAt132 = LDuAt132;
      
      PDuAt133 = LDuAt133;
      
      PDuAt221 = LDuAt221;
      
      PDuAt222 = LDuAt222;
      
      PDuAt223 = LDuAt223;
      
      PDuAt231 = LDuAt231;
      
      PDuAt232 = LDuAt232;
      
      PDuAt233 = LDuAt233;
      
      PDuAt331 = LDuAt331;
      
      PDuAt332 = LDuAt332;
      
      PDuAt333 = LDuAt333;
    }
    
    CCTK_REAL LDissAt11 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(At11,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At11,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(At11,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(At11,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(At11,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(At11,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(At11,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(At11,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(At11,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(At11,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At11,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At11,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(At11,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(At11,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(At11,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(At11,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(At11,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(At11,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(At11,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At11,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At11,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At11,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(At11,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(At11,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(At11,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At11,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(At11,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(At11,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At11,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At11,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At11,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At11,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(At11,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At11,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(At11,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(At11,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(At11,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At11,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At11,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(At11,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(At11,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At11,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At11,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At11,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(At11,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(At11,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(At11,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At11,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(At11,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(At11,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At11,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At11,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At11,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(At11,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(At11,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At11,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(At11,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(At11,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(At11,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(At11,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At11,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At11,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(At11,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(At11,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(At11,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(At11,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(At11,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(At11,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(At11,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(At11,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At11,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At11,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(At11,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(At11,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(At11,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(At11,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(At11,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(At11,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(At11,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(At11,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(At11,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(At11,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At11,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(At11,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(At11,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(At11,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(At11,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(At11,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(At11,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(At11,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(At11,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At11,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At11,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(At11,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(At11,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(At11,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(At11,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(At11,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(At11,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(At11,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(At11,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At11,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At11,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(At11,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(At11,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(At11,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(At11,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(At11,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(At11,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(At11,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(At11,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At11,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At11,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(At11,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(At11,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(At11,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(At11,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(At11,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(At11,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(At11,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(At11,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(At11,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At11,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(At11,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(At11,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(At11,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(At11,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(At11,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(At11,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(At11,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(At11,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At11,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At11,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(At11,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(At11,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(At11,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(At11,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(At11,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(At11,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(At11,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(At11,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At11,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At11,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(At11,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(At11,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(At11,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(At11,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(At11,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(At11,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(At11,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(At11,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At11,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At11,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(At11,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(At11,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(At11,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(At11,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(At11,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(At11,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(At11,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(At11,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(At11,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(At11,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At11,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(At11,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(At11,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(At11,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(At11,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(At11,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(At11,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(At11,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(At11,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At11,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At11,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(At11,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(At11,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(At11,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(At11,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(At11,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(At11,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(At11,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(At11,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At11,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At11,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(At11,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(At11,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(At11,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(At11,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(At11,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(At11,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(At11,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(At11,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At11,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At11,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(At11,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(At11,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(At11,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(At11,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(At11,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(At11,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(At11,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(At11,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(At11,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At11,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(At11,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(At11,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(At11,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(At11,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(At11,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(At11,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(At11,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(At11,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At11,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At11,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(At11,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(At11,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(At11,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(At11,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(At11,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(At11,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(At11,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(At11,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At11,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At11,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(At11,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(At11,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(At11,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(At11,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(At11,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(At11,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(At11,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(At11,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At11,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At11,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(At11,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(At11,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(At11,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(At11,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(At11,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(At11,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(At11,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(At11,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(At11,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL LDissAt12 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(At12,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At12,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(At12,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(At12,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(At12,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(At12,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(At12,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(At12,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(At12,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(At12,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At12,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At12,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(At12,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(At12,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(At12,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(At12,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(At12,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(At12,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(At12,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At12,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At12,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At12,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(At12,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(At12,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(At12,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At12,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(At12,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(At12,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At12,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At12,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At12,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At12,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(At12,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At12,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(At12,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(At12,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(At12,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At12,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At12,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(At12,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(At12,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At12,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At12,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At12,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(At12,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(At12,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(At12,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At12,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(At12,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(At12,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At12,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At12,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At12,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(At12,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(At12,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At12,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(At12,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(At12,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(At12,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(At12,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At12,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At12,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(At12,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(At12,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(At12,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(At12,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(At12,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(At12,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(At12,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(At12,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At12,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At12,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(At12,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(At12,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(At12,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(At12,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(At12,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(At12,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(At12,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(At12,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(At12,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(At12,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At12,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(At12,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(At12,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(At12,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(At12,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(At12,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(At12,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(At12,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(At12,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At12,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At12,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(At12,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(At12,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(At12,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(At12,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(At12,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(At12,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(At12,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(At12,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At12,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At12,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(At12,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(At12,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(At12,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(At12,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(At12,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(At12,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(At12,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(At12,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At12,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At12,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(At12,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(At12,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(At12,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(At12,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(At12,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(At12,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(At12,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(At12,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(At12,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At12,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(At12,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(At12,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(At12,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(At12,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(At12,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(At12,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(At12,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(At12,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At12,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At12,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(At12,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(At12,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(At12,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(At12,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(At12,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(At12,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(At12,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(At12,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At12,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At12,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(At12,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(At12,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(At12,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(At12,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(At12,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(At12,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(At12,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(At12,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At12,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At12,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(At12,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(At12,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(At12,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(At12,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(At12,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(At12,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(At12,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(At12,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(At12,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(At12,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At12,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(At12,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(At12,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(At12,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(At12,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(At12,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(At12,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(At12,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(At12,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At12,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At12,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(At12,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(At12,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(At12,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(At12,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(At12,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(At12,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(At12,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(At12,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At12,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At12,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(At12,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(At12,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(At12,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(At12,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(At12,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(At12,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(At12,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(At12,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At12,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At12,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(At12,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(At12,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(At12,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(At12,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(At12,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(At12,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(At12,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(At12,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(At12,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At12,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(At12,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(At12,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(At12,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(At12,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(At12,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(At12,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(At12,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(At12,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At12,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At12,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(At12,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(At12,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(At12,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(At12,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(At12,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(At12,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(At12,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(At12,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At12,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At12,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(At12,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(At12,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(At12,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(At12,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(At12,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(At12,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(At12,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(At12,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At12,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At12,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(At12,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(At12,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(At12,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(At12,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(At12,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(At12,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(At12,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(At12,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(At12,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL LDissAt13 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(At13,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At13,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(At13,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(At13,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(At13,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(At13,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(At13,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(At13,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(At13,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(At13,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At13,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At13,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(At13,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(At13,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(At13,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(At13,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(At13,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(At13,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(At13,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At13,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At13,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At13,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(At13,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(At13,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(At13,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At13,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(At13,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(At13,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At13,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At13,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At13,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At13,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(At13,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At13,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(At13,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(At13,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(At13,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At13,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At13,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(At13,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(At13,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At13,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At13,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At13,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(At13,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(At13,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(At13,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At13,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(At13,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(At13,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At13,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At13,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At13,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(At13,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(At13,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At13,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(At13,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(At13,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(At13,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(At13,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At13,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At13,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(At13,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(At13,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(At13,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(At13,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(At13,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(At13,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(At13,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(At13,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At13,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At13,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(At13,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(At13,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(At13,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(At13,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(At13,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(At13,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(At13,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(At13,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(At13,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(At13,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At13,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(At13,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(At13,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(At13,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(At13,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(At13,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(At13,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(At13,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(At13,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At13,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At13,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(At13,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(At13,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(At13,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(At13,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(At13,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(At13,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(At13,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(At13,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At13,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At13,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(At13,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(At13,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(At13,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(At13,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(At13,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(At13,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(At13,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(At13,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At13,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At13,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(At13,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(At13,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(At13,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(At13,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(At13,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(At13,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(At13,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(At13,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(At13,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At13,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(At13,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(At13,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(At13,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(At13,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(At13,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(At13,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(At13,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(At13,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At13,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At13,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(At13,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(At13,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(At13,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(At13,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(At13,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(At13,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(At13,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(At13,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At13,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At13,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(At13,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(At13,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(At13,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(At13,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(At13,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(At13,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(At13,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(At13,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At13,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At13,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(At13,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(At13,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(At13,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(At13,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(At13,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(At13,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(At13,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(At13,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(At13,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(At13,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At13,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(At13,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(At13,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(At13,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(At13,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(At13,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(At13,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(At13,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(At13,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At13,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At13,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(At13,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(At13,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(At13,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(At13,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(At13,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(At13,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(At13,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(At13,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At13,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At13,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(At13,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(At13,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(At13,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(At13,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(At13,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(At13,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(At13,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(At13,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At13,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At13,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(At13,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(At13,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(At13,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(At13,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(At13,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(At13,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(At13,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(At13,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(At13,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At13,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(At13,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(At13,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(At13,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(At13,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(At13,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(At13,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(At13,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(At13,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At13,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At13,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(At13,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(At13,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(At13,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(At13,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(At13,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(At13,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(At13,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(At13,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At13,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At13,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(At13,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(At13,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(At13,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(At13,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(At13,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(At13,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(At13,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(At13,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At13,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At13,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(At13,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(At13,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(At13,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(At13,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(At13,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(At13,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(At13,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(At13,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(At13,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL LDissAt22 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(At22,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At22,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(At22,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(At22,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(At22,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(At22,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(At22,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(At22,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(At22,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(At22,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At22,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At22,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(At22,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(At22,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(At22,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(At22,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(At22,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(At22,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(At22,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At22,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At22,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At22,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(At22,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(At22,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(At22,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At22,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(At22,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(At22,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At22,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At22,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At22,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At22,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(At22,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At22,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(At22,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(At22,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(At22,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At22,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At22,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(At22,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(At22,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At22,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At22,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At22,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(At22,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(At22,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(At22,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At22,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(At22,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(At22,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At22,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At22,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At22,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(At22,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(At22,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At22,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(At22,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(At22,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(At22,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(At22,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At22,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At22,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(At22,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(At22,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(At22,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(At22,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(At22,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(At22,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(At22,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(At22,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At22,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At22,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(At22,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(At22,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(At22,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(At22,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(At22,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(At22,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(At22,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(At22,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(At22,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(At22,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At22,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(At22,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(At22,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(At22,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(At22,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(At22,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(At22,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(At22,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(At22,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At22,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At22,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(At22,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(At22,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(At22,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(At22,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(At22,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(At22,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(At22,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(At22,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At22,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At22,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(At22,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(At22,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(At22,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(At22,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(At22,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(At22,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(At22,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(At22,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At22,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At22,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(At22,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(At22,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(At22,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(At22,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(At22,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(At22,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(At22,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(At22,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(At22,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At22,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(At22,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(At22,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(At22,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(At22,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(At22,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(At22,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(At22,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(At22,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At22,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At22,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(At22,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(At22,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(At22,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(At22,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(At22,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(At22,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(At22,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(At22,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At22,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At22,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(At22,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(At22,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(At22,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(At22,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(At22,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(At22,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(At22,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(At22,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At22,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At22,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(At22,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(At22,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(At22,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(At22,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(At22,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(At22,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(At22,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(At22,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(At22,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(At22,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At22,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(At22,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(At22,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(At22,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(At22,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(At22,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(At22,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(At22,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(At22,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At22,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At22,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(At22,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(At22,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(At22,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(At22,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(At22,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(At22,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(At22,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(At22,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At22,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At22,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(At22,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(At22,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(At22,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(At22,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(At22,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(At22,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(At22,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(At22,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At22,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At22,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(At22,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(At22,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(At22,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(At22,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(At22,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(At22,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(At22,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(At22,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(At22,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At22,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(At22,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(At22,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(At22,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(At22,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(At22,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(At22,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(At22,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(At22,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At22,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At22,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(At22,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(At22,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(At22,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(At22,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(At22,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(At22,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(At22,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(At22,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At22,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At22,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(At22,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(At22,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(At22,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(At22,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(At22,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(At22,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(At22,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(At22,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At22,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At22,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(At22,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(At22,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(At22,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(At22,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(At22,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(At22,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(At22,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(At22,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(At22,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL LDissAt23 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(At23,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At23,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(At23,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(At23,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(At23,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(At23,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(At23,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(At23,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(At23,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(At23,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At23,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At23,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(At23,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(At23,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(At23,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(At23,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(At23,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(At23,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(At23,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At23,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At23,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At23,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(At23,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(At23,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(At23,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At23,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(At23,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(At23,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At23,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At23,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At23,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At23,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(At23,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At23,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(At23,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(At23,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(At23,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At23,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At23,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(At23,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(At23,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At23,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At23,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At23,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(At23,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(At23,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(At23,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At23,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(At23,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(At23,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At23,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At23,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At23,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(At23,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(At23,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At23,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(At23,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(At23,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(At23,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(At23,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At23,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At23,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(At23,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(At23,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(At23,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(At23,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(At23,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(At23,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(At23,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(At23,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At23,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At23,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(At23,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(At23,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(At23,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(At23,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(At23,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(At23,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(At23,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(At23,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(At23,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(At23,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At23,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(At23,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(At23,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(At23,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(At23,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(At23,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(At23,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(At23,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(At23,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At23,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At23,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(At23,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(At23,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(At23,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(At23,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(At23,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(At23,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(At23,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(At23,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At23,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At23,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(At23,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(At23,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(At23,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(At23,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(At23,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(At23,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(At23,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(At23,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At23,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At23,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(At23,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(At23,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(At23,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(At23,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(At23,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(At23,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(At23,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(At23,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(At23,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At23,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(At23,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(At23,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(At23,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(At23,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(At23,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(At23,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(At23,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(At23,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At23,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At23,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(At23,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(At23,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(At23,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(At23,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(At23,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(At23,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(At23,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(At23,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At23,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At23,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(At23,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(At23,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(At23,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(At23,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(At23,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(At23,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(At23,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(At23,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At23,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At23,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(At23,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(At23,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(At23,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(At23,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(At23,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(At23,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(At23,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(At23,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(At23,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(At23,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At23,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(At23,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(At23,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(At23,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(At23,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(At23,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(At23,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(At23,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(At23,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At23,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At23,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(At23,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(At23,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(At23,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(At23,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(At23,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(At23,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(At23,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(At23,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At23,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At23,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(At23,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(At23,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(At23,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(At23,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(At23,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(At23,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(At23,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(At23,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At23,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At23,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(At23,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(At23,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(At23,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(At23,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(At23,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(At23,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(At23,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(At23,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(At23,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At23,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(At23,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(At23,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(At23,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(At23,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(At23,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(At23,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(At23,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(At23,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At23,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At23,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(At23,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(At23,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(At23,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(At23,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(At23,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(At23,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(At23,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(At23,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At23,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At23,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(At23,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(At23,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(At23,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(At23,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(At23,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(At23,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(At23,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(At23,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At23,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At23,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(At23,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(At23,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(At23,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(At23,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(At23,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(At23,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(At23,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(At23,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(At23,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL LDissAt33 CCTK_ATTRIBUTE_UNUSED = epsDiss*(IfThen(ti == 
      0,-0.112286322818464314978983667162*GFOffset(At33,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At33,1,0,0) - 
      0.351809741946848657872278328841*GFOffset(At33,2,0,0) + 
      0.393891905012676489751301745911*GFOffset(At33,3,0,0) - 
      0.406346327593537414965986394558*GFOffset(At33,4,0,0) + 
      0.390879615587115033229871138443*GFOffset(At33,5,0,0) - 
      0.346808789843888057395530015299*GFOffset(At33,6,0,0) + 
      0.268628993919817652419710483255*GFOffset(At33,7,0,0) - 
      0.109937276933326049604349666172*GFOffset(At33,8,0,0),0) + IfThen(ti == 
      1,0.0459542830207730700182429562579*GFOffset(At33,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At33,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At33,1,0,0) - 
      0.161312245561261507517930325854*GFOffset(At33,2,0,0) + 
      0.166476689577574582134462173089*GFOffset(At33,3,0,0) - 
      0.160201848635837643277811678136*GFOffset(At33,4,0,0) + 
      0.142186995897579492008129425834*GFOffset(At33,5,0,0) - 
      0.110160500417655412786574230511*GFOffset(At33,6,0,0) + 
      0.0450883724317016003377209764486*GFOffset(At33,7,0,0),0) + IfThen(ti 
      == 2,-0.0355960467027073641809745493856*GFOffset(At33,-2,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At33,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At33,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At33,1,0,0) - 
      0.129254854162970378545481989137*GFOffset(At33,2,0,0) + 
      0.12448944790414592254506540358*GFOffset(At33,3,0,0) - 
      0.110572514574970832140194239567*GFOffset(At33,4,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At33,5,0,0) - 
      0.0350900512642925789585489836954*GFOffset(At33,6,0,0),0) + IfThen(ti 
      == 3,0.0315835488689294754585658103387*GFOffset(At33,-3,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At33,-2,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At33,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At33,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At33,1,0,0) - 
      0.110956754997445517624926570756*GFOffset(At33,2,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At33,3,0,0) - 
      0.076531411309735391258141470829*GFOffset(At33,4,0,0) + 
      0.0313420135921978606262758413179*GFOffset(At33,5,0,0),0) + IfThen(ti 
      == 4,-0.0303817292054494222005208333333*GFOffset(At33,-4,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At33,-3,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At33,-2,0,0) + 
      0.107294207461635702048026346877*GFOffset(At33,-1,0,0) - 
      0.111112010722257653061224489796*GFOffset(At33,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At33,1,0,0) - 
      0.0955144556251064670745298655398*GFOffset(At33,2,0,0) + 
      0.0741579827300490137576365968936*GFOffset(At33,3,0,0) - 
      0.0303817292054494222005208333333*GFOffset(At33,4,0,0),0) + IfThen(ti 
      == 5,0.0313420135921978606262758413179*GFOffset(At33,-5,0,0) - 
      0.076531411309735391258141470829*GFOffset(At33,-4,0,0) + 
      0.0986557736009185254313191297842*GFOffset(At33,-3,0,0) - 
      0.110956754997445517624926570756*GFOffset(At33,-2,0,0) + 
      0.115065200577677324406833526147*GFOffset(At33,-1,0,0) - 
      0.111266486785562678762026781539*GFOffset(At33,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At33,1,0,0) - 
      0.0770618686330453857546084388367*GFOffset(At33,2,0,0) + 
      0.0315835488689294754585658103387*GFOffset(At33,3,0,0),0) + IfThen(ti 
      == 6,-0.0350900512642925789585489836954*GFOffset(At33,-6,0,0) + 
      0.0857120953217147008926440158253*GFOffset(At33,-5,0,0) - 
      0.110572514574970832140194239567*GFOffset(At33,-4,0,0) + 
      0.12448944790414592254506540358*GFOffset(At33,-3,0,0) - 
      0.129254854162970378545481989137*GFOffset(At33,-2,0,0) + 
      0.12513830910664281916562485039*GFOffset(At33,-1,0,0) - 
      0.111649742992731590588719699141*GFOffset(At33,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At33,1,0,0) - 
      0.0355960467027073641809745493856*GFOffset(At33,2,0,0),0) + IfThen(ti 
      == 7,0.0450883724317016003377209764486*GFOffset(At33,-7,0,0) - 
      0.110160500417655412786574230511*GFOffset(At33,-6,0,0) + 
      0.142186995897579492008129425834*GFOffset(At33,-5,0,0) - 
      0.160201848635837643277811678136*GFOffset(At33,-4,0,0) + 
      0.166476689577574582134462173089*GFOffset(At33,-3,0,0) - 
      0.161312245561261507517930325854*GFOffset(At33,-2,0,0) + 
      0.144030458141977394536761358228*GFOffset(At33,-1,0,0) - 
      0.112062204454851575453000655356*GFOffset(At33,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At33,1,0,0),0) + IfThen(ti 
      == 8,-0.109937276933326049604349666172*GFOffset(At33,-8,0,0) + 
      0.268628993919817652419710483255*GFOffset(At33,-7,0,0) - 
      0.346808789843888057395530015299*GFOffset(At33,-6,0,0) + 
      0.390879615587115033229871138443*GFOffset(At33,-5,0,0) - 
      0.406346327593537414965986394558*GFOffset(At33,-4,0,0) + 
      0.393891905012676489751301745911*GFOffset(At33,-3,0,0) - 
      0.351809741946848657872278328841*GFOffset(At33,-2,0,0) + 
      0.273787944616455319416244704422*GFOffset(At33,-1,0,0) - 
      0.112286322818464314978983667162*GFOffset(At33,0,0,0),0))*pow(dx,-1) + 
      epsDiss*(IfThen(tj == 
      0,-0.112286322818464314978983667162*GFOffset(At33,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At33,0,1,0) - 
      0.351809741946848657872278328841*GFOffset(At33,0,2,0) + 
      0.393891905012676489751301745911*GFOffset(At33,0,3,0) - 
      0.406346327593537414965986394558*GFOffset(At33,0,4,0) + 
      0.390879615587115033229871138443*GFOffset(At33,0,5,0) - 
      0.346808789843888057395530015299*GFOffset(At33,0,6,0) + 
      0.268628993919817652419710483255*GFOffset(At33,0,7,0) - 
      0.109937276933326049604349666172*GFOffset(At33,0,8,0),0) + IfThen(tj == 
      1,0.0459542830207730700182429562579*GFOffset(At33,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At33,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At33,0,1,0) - 
      0.161312245561261507517930325854*GFOffset(At33,0,2,0) + 
      0.166476689577574582134462173089*GFOffset(At33,0,3,0) - 
      0.160201848635837643277811678136*GFOffset(At33,0,4,0) + 
      0.142186995897579492008129425834*GFOffset(At33,0,5,0) - 
      0.110160500417655412786574230511*GFOffset(At33,0,6,0) + 
      0.0450883724317016003377209764486*GFOffset(At33,0,7,0),0) + IfThen(tj 
      == 2,-0.0355960467027073641809745493856*GFOffset(At33,0,-2,0) + 
      0.0868233573651693018105851911312*GFOffset(At33,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At33,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At33,0,1,0) - 
      0.129254854162970378545481989137*GFOffset(At33,0,2,0) + 
      0.12448944790414592254506540358*GFOffset(At33,0,3,0) - 
      0.110572514574970832140194239567*GFOffset(At33,0,4,0) + 
      0.0857120953217147008926440158253*GFOffset(At33,0,5,0) - 
      0.0350900512642925789585489836954*GFOffset(At33,0,6,0),0) + IfThen(tj 
      == 3,0.0315835488689294754585658103387*GFOffset(At33,0,-3,0) - 
      0.0770618686330453857546084388367*GFOffset(At33,0,-2,0) + 
      0.0991699850860657874767089543731*GFOffset(At33,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At33,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At33,0,1,0) - 
      0.110956754997445517624926570756*GFOffset(At33,0,2,0) + 
      0.0986557736009185254313191297842*GFOffset(At33,0,3,0) - 
      0.076531411309735391258141470829*GFOffset(At33,0,4,0) + 
      0.0313420135921978606262758413179*GFOffset(At33,0,5,0),0) + IfThen(tj 
      == 4,-0.0303817292054494222005208333333*GFOffset(At33,0,-4,0) + 
      0.0741579827300490137576365968936*GFOffset(At33,0,-3,0) - 
      0.0955144556251064670745298655398*GFOffset(At33,0,-2,0) + 
      0.107294207461635702048026346877*GFOffset(At33,0,-1,0) - 
      0.111112010722257653061224489796*GFOffset(At33,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At33,0,1,0) - 
      0.0955144556251064670745298655398*GFOffset(At33,0,2,0) + 
      0.0741579827300490137576365968936*GFOffset(At33,0,3,0) - 
      0.0303817292054494222005208333333*GFOffset(At33,0,4,0),0) + IfThen(tj 
      == 5,0.0313420135921978606262758413179*GFOffset(At33,0,-5,0) - 
      0.076531411309735391258141470829*GFOffset(At33,0,-4,0) + 
      0.0986557736009185254313191297842*GFOffset(At33,0,-3,0) - 
      0.110956754997445517624926570756*GFOffset(At33,0,-2,0) + 
      0.115065200577677324406833526147*GFOffset(At33,0,-1,0) - 
      0.111266486785562678762026781539*GFOffset(At33,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At33,0,1,0) - 
      0.0770618686330453857546084388367*GFOffset(At33,0,2,0) + 
      0.0315835488689294754585658103387*GFOffset(At33,0,3,0),0) + IfThen(tj 
      == 6,-0.0350900512642925789585489836954*GFOffset(At33,0,-6,0) + 
      0.0857120953217147008926440158253*GFOffset(At33,0,-5,0) - 
      0.110572514574970832140194239567*GFOffset(At33,0,-4,0) + 
      0.12448944790414592254506540358*GFOffset(At33,0,-3,0) - 
      0.129254854162970378545481989137*GFOffset(At33,0,-2,0) + 
      0.12513830910664281916562485039*GFOffset(At33,0,-1,0) - 
      0.111649742992731590588719699141*GFOffset(At33,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At33,0,1,0) - 
      0.0355960467027073641809745493856*GFOffset(At33,0,2,0),0) + IfThen(tj 
      == 7,0.0450883724317016003377209764486*GFOffset(At33,0,-7,0) - 
      0.110160500417655412786574230511*GFOffset(At33,0,-6,0) + 
      0.142186995897579492008129425834*GFOffset(At33,0,-5,0) - 
      0.160201848635837643277811678136*GFOffset(At33,0,-4,0) + 
      0.166476689577574582134462173089*GFOffset(At33,0,-3,0) - 
      0.161312245561261507517930325854*GFOffset(At33,0,-2,0) + 
      0.144030458141977394536761358228*GFOffset(At33,0,-1,0) - 
      0.112062204454851575453000655356*GFOffset(At33,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At33,0,1,0),0) + IfThen(tj 
      == 8,-0.109937276933326049604349666172*GFOffset(At33,0,-8,0) + 
      0.268628993919817652419710483255*GFOffset(At33,0,-7,0) - 
      0.346808789843888057395530015299*GFOffset(At33,0,-6,0) + 
      0.390879615587115033229871138443*GFOffset(At33,0,-5,0) - 
      0.406346327593537414965986394558*GFOffset(At33,0,-4,0) + 
      0.393891905012676489751301745911*GFOffset(At33,0,-3,0) - 
      0.351809741946848657872278328841*GFOffset(At33,0,-2,0) + 
      0.273787944616455319416244704422*GFOffset(At33,0,-1,0) - 
      0.112286322818464314978983667162*GFOffset(At33,0,0,0),0))*pow(dy,-1) + 
      epsDiss*(IfThen(tk == 
      0,-0.112286322818464314978983667162*GFOffset(At33,0,0,0) + 
      0.273787944616455319416244704422*GFOffset(At33,0,0,1) - 
      0.351809741946848657872278328841*GFOffset(At33,0,0,2) + 
      0.393891905012676489751301745911*GFOffset(At33,0,0,3) - 
      0.406346327593537414965986394558*GFOffset(At33,0,0,4) + 
      0.390879615587115033229871138443*GFOffset(At33,0,0,5) - 
      0.346808789843888057395530015299*GFOffset(At33,0,0,6) + 
      0.268628993919817652419710483255*GFOffset(At33,0,0,7) - 
      0.109937276933326049604349666172*GFOffset(At33,0,0,8),0) + IfThen(tk == 
      1,0.0459542830207730700182429562579*GFOffset(At33,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At33,0,0,0) + 
      0.144030458141977394536761358228*GFOffset(At33,0,0,1) - 
      0.161312245561261507517930325854*GFOffset(At33,0,0,2) + 
      0.166476689577574582134462173089*GFOffset(At33,0,0,3) - 
      0.160201848635837643277811678136*GFOffset(At33,0,0,4) + 
      0.142186995897579492008129425834*GFOffset(At33,0,0,5) - 
      0.110160500417655412786574230511*GFOffset(At33,0,0,6) + 
      0.0450883724317016003377209764486*GFOffset(At33,0,0,7),0) + IfThen(tk 
      == 2,-0.0355960467027073641809745493856*GFOffset(At33,0,0,-2) + 
      0.0868233573651693018105851911312*GFOffset(At33,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At33,0,0,0) + 
      0.12513830910664281916562485039*GFOffset(At33,0,0,1) - 
      0.129254854162970378545481989137*GFOffset(At33,0,0,2) + 
      0.12448944790414592254506540358*GFOffset(At33,0,0,3) - 
      0.110572514574970832140194239567*GFOffset(At33,0,0,4) + 
      0.0857120953217147008926440158253*GFOffset(At33,0,0,5) - 
      0.0350900512642925789585489836954*GFOffset(At33,0,0,6),0) + IfThen(tk 
      == 3,0.0315835488689294754585658103387*GFOffset(At33,0,0,-3) - 
      0.0770618686330453857546084388367*GFOffset(At33,0,0,-2) + 
      0.0991699850860657874767089543731*GFOffset(At33,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At33,0,0,0) + 
      0.115065200577677324406833526147*GFOffset(At33,0,0,1) - 
      0.110956754997445517624926570756*GFOffset(At33,0,0,2) + 
      0.0986557736009185254313191297842*GFOffset(At33,0,0,3) - 
      0.076531411309735391258141470829*GFOffset(At33,0,0,4) + 
      0.0313420135921978606262758413179*GFOffset(At33,0,0,5),0) + IfThen(tk 
      == 4,-0.0303817292054494222005208333333*GFOffset(At33,0,0,-4) + 
      0.0741579827300490137576365968936*GFOffset(At33,0,0,-3) - 
      0.0955144556251064670745298655398*GFOffset(At33,0,0,-2) + 
      0.107294207461635702048026346877*GFOffset(At33,0,0,-1) - 
      0.111112010722257653061224489796*GFOffset(At33,0,0,0) + 
      0.107294207461635702048026346877*GFOffset(At33,0,0,1) - 
      0.0955144556251064670745298655398*GFOffset(At33,0,0,2) + 
      0.0741579827300490137576365968936*GFOffset(At33,0,0,3) - 
      0.0303817292054494222005208333333*GFOffset(At33,0,0,4),0) + IfThen(tk 
      == 5,0.0313420135921978606262758413179*GFOffset(At33,0,0,-5) - 
      0.076531411309735391258141470829*GFOffset(At33,0,0,-4) + 
      0.0986557736009185254313191297842*GFOffset(At33,0,0,-3) - 
      0.110956754997445517624926570756*GFOffset(At33,0,0,-2) + 
      0.115065200577677324406833526147*GFOffset(At33,0,0,-1) - 
      0.111266486785562678762026781539*GFOffset(At33,0,0,0) + 
      0.0991699850860657874767089543731*GFOffset(At33,0,0,1) - 
      0.0770618686330453857546084388367*GFOffset(At33,0,0,2) + 
      0.0315835488689294754585658103387*GFOffset(At33,0,0,3),0) + IfThen(tk 
      == 6,-0.0350900512642925789585489836954*GFOffset(At33,0,0,-6) + 
      0.0857120953217147008926440158253*GFOffset(At33,0,0,-5) - 
      0.110572514574970832140194239567*GFOffset(At33,0,0,-4) + 
      0.12448944790414592254506540358*GFOffset(At33,0,0,-3) - 
      0.129254854162970378545481989137*GFOffset(At33,0,0,-2) + 
      0.12513830910664281916562485039*GFOffset(At33,0,0,-1) - 
      0.111649742992731590588719699141*GFOffset(At33,0,0,0) + 
      0.0868233573651693018105851911312*GFOffset(At33,0,0,1) - 
      0.0355960467027073641809745493856*GFOffset(At33,0,0,2),0) + IfThen(tk 
      == 7,0.0450883724317016003377209764486*GFOffset(At33,0,0,-7) - 
      0.110160500417655412786574230511*GFOffset(At33,0,0,-6) + 
      0.142186995897579492008129425834*GFOffset(At33,0,0,-5) - 
      0.160201848635837643277811678136*GFOffset(At33,0,0,-4) + 
      0.166476689577574582134462173089*GFOffset(At33,0,0,-3) - 
      0.161312245561261507517930325854*GFOffset(At33,0,0,-2) + 
      0.144030458141977394536761358228*GFOffset(At33,0,0,-1) - 
      0.112062204454851575453000655356*GFOffset(At33,0,0,0) + 
      0.0459542830207730700182429562579*GFOffset(At33,0,0,1),0) + IfThen(tk 
      == 8,-0.109937276933326049604349666172*GFOffset(At33,0,0,-8) + 
      0.268628993919817652419710483255*GFOffset(At33,0,0,-7) - 
      0.346808789843888057395530015299*GFOffset(At33,0,0,-6) + 
      0.390879615587115033229871138443*GFOffset(At33,0,0,-5) - 
      0.406346327593537414965986394558*GFOffset(At33,0,0,-4) + 
      0.393891905012676489751301745911*GFOffset(At33,0,0,-3) - 
      0.351809741946848657872278328841*GFOffset(At33,0,0,-2) + 
      0.273787944616455319416244704422*GFOffset(At33,0,0,-1) - 
      0.112286322818464314978983667162*GFOffset(At33,0,0,0),0))*pow(dz,-1);
    
    CCTK_REAL GDissAt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissAt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissAt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissAt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissAt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL GDissAt33 CCTK_ATTRIBUTE_UNUSED;
    
    if (usejacobian)
    {
      GDissAt11 = myDetJL*LDissAt11;
      
      GDissAt12 = myDetJL*LDissAt12;
      
      GDissAt13 = myDetJL*LDissAt13;
      
      GDissAt22 = myDetJL*LDissAt22;
      
      GDissAt23 = myDetJL*LDissAt23;
      
      GDissAt33 = myDetJL*LDissAt33;
    }
    else
    {
      GDissAt11 = LDissAt11;
      
      GDissAt12 = LDissAt12;
      
      GDissAt13 = LDissAt13;
      
      GDissAt22 = LDissAt22;
      
      GDissAt23 = LDissAt23;
      
      GDissAt33 = LDissAt33;
    }
    
    CCTK_REAL detgt CCTK_ATTRIBUTE_UNUSED = 1;
    
    CCTK_REAL gtu11 CCTK_ATTRIBUTE_UNUSED = (gt22L*gt33L - 
      pow(gt23L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu12 CCTK_ATTRIBUTE_UNUSED = (gt13L*gt23L - 
      gt12L*gt33L)*pow(detgt,-1);
    
    CCTK_REAL gtu13 CCTK_ATTRIBUTE_UNUSED = (-(gt13L*gt22L) + 
      gt12L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu22 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt33L - 
      pow(gt13L,2))*pow(detgt,-1);
    
    CCTK_REAL gtu23 CCTK_ATTRIBUTE_UNUSED = (gt12L*gt13L - 
      gt11L*gt23L)*pow(detgt,-1);
    
    CCTK_REAL gtu33 CCTK_ATTRIBUTE_UNUSED = (gt11L*gt22L - 
      pow(gt12L,2))*pow(detgt,-1);
    
    CCTK_REAL Gtl111 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt111L;
    
    CCTK_REAL Gtl112 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt112L;
    
    CCTK_REAL Gtl113 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt113L;
    
    CCTK_REAL Gtl122 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt122L - PDgt221L);
    
    CCTK_REAL Gtl123 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt123L + PDgt132L - 
      PDgt231L);
    
    CCTK_REAL Gtl133 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt133L - PDgt331L);
    
    CCTK_REAL Gtl211 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt112L + 2*PDgt121L);
    
    CCTK_REAL Gtl212 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt221L;
    
    CCTK_REAL Gtl213 CCTK_ATTRIBUTE_UNUSED = 0.5*(PDgt123L - PDgt132L + 
      PDgt231L);
    
    CCTK_REAL Gtl222 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt222L;
    
    CCTK_REAL Gtl223 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt223L;
    
    CCTK_REAL Gtl233 CCTK_ATTRIBUTE_UNUSED = 0.5*(2*PDgt233L - PDgt332L);
    
    CCTK_REAL Gtl311 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt113L + 2*PDgt131L);
    
    CCTK_REAL Gtl312 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt123L + PDgt132L + 
      PDgt231L);
    
    CCTK_REAL Gtl313 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt331L;
    
    CCTK_REAL Gtl322 CCTK_ATTRIBUTE_UNUSED = 0.5*(-PDgt223L + 2*PDgt232L);
    
    CCTK_REAL Gtl323 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt332L;
    
    CCTK_REAL Gtl333 CCTK_ATTRIBUTE_UNUSED = 0.5*PDgt333L;
    
    CCTK_REAL Gtlu111 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu11 + Gtl112*gtu12 
      + Gtl113*gtu13;
    
    CCTK_REAL Gtlu112 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu12 + Gtl112*gtu22 
      + Gtl113*gtu23;
    
    CCTK_REAL Gtlu113 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu13 + Gtl112*gtu23 
      + Gtl113*gtu33;
    
    CCTK_REAL Gtlu121 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu11 + Gtl122*gtu12 
      + Gtl123*gtu13;
    
    CCTK_REAL Gtlu122 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu12 + Gtl122*gtu22 
      + Gtl123*gtu23;
    
    CCTK_REAL Gtlu123 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu13 + Gtl122*gtu23 
      + Gtl123*gtu33;
    
    CCTK_REAL Gtlu131 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu11 + Gtl123*gtu12 
      + Gtl133*gtu13;
    
    CCTK_REAL Gtlu132 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu12 + Gtl123*gtu22 
      + Gtl133*gtu23;
    
    CCTK_REAL Gtlu133 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu13 + Gtl123*gtu23 
      + Gtl133*gtu33;
    
    CCTK_REAL Gtlu211 CCTK_ATTRIBUTE_UNUSED = Gtl211*gtu11 + Gtl212*gtu12 
      + Gtl213*gtu13;
    
    CCTK_REAL Gtlu212 CCTK_ATTRIBUTE_UNUSED = Gtl211*gtu12 + Gtl212*gtu22 
      + Gtl213*gtu23;
    
    CCTK_REAL Gtlu213 CCTK_ATTRIBUTE_UNUSED = Gtl211*gtu13 + Gtl212*gtu23 
      + Gtl213*gtu33;
    
    CCTK_REAL Gtlu221 CCTK_ATTRIBUTE_UNUSED = Gtl212*gtu11 + Gtl222*gtu12 
      + Gtl223*gtu13;
    
    CCTK_REAL Gtlu222 CCTK_ATTRIBUTE_UNUSED = Gtl212*gtu12 + Gtl222*gtu22 
      + Gtl223*gtu23;
    
    CCTK_REAL Gtlu223 CCTK_ATTRIBUTE_UNUSED = Gtl212*gtu13 + Gtl222*gtu23 
      + Gtl223*gtu33;
    
    CCTK_REAL Gtlu231 CCTK_ATTRIBUTE_UNUSED = Gtl213*gtu11 + Gtl223*gtu12 
      + Gtl233*gtu13;
    
    CCTK_REAL Gtlu232 CCTK_ATTRIBUTE_UNUSED = Gtl213*gtu12 + Gtl223*gtu22 
      + Gtl233*gtu23;
    
    CCTK_REAL Gtlu233 CCTK_ATTRIBUTE_UNUSED = Gtl213*gtu13 + Gtl223*gtu23 
      + Gtl233*gtu33;
    
    CCTK_REAL Gtlu311 CCTK_ATTRIBUTE_UNUSED = Gtl311*gtu11 + Gtl312*gtu12 
      + Gtl313*gtu13;
    
    CCTK_REAL Gtlu312 CCTK_ATTRIBUTE_UNUSED = Gtl311*gtu12 + Gtl312*gtu22 
      + Gtl313*gtu23;
    
    CCTK_REAL Gtlu313 CCTK_ATTRIBUTE_UNUSED = Gtl311*gtu13 + Gtl312*gtu23 
      + Gtl313*gtu33;
    
    CCTK_REAL Gtlu321 CCTK_ATTRIBUTE_UNUSED = Gtl312*gtu11 + Gtl322*gtu12 
      + Gtl323*gtu13;
    
    CCTK_REAL Gtlu322 CCTK_ATTRIBUTE_UNUSED = Gtl312*gtu12 + Gtl322*gtu22 
      + Gtl323*gtu23;
    
    CCTK_REAL Gtlu323 CCTK_ATTRIBUTE_UNUSED = Gtl312*gtu13 + Gtl322*gtu23 
      + Gtl323*gtu33;
    
    CCTK_REAL Gtlu331 CCTK_ATTRIBUTE_UNUSED = Gtl313*gtu11 + Gtl323*gtu12 
      + Gtl333*gtu13;
    
    CCTK_REAL Gtlu332 CCTK_ATTRIBUTE_UNUSED = Gtl313*gtu12 + Gtl323*gtu22 
      + Gtl333*gtu23;
    
    CCTK_REAL Gtlu333 CCTK_ATTRIBUTE_UNUSED = Gtl313*gtu13 + Gtl323*gtu23 
      + Gtl333*gtu33;
    
    CCTK_REAL Gt111 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu11 + Gtl211*gtu12 + 
      Gtl311*gtu13;
    
    CCTK_REAL Gt211 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu12 + Gtl211*gtu22 + 
      Gtl311*gtu23;
    
    CCTK_REAL Gt311 CCTK_ATTRIBUTE_UNUSED = Gtl111*gtu13 + Gtl211*gtu23 + 
      Gtl311*gtu33;
    
    CCTK_REAL Gt112 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu11 + Gtl212*gtu12 + 
      Gtl312*gtu13;
    
    CCTK_REAL Gt212 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu12 + Gtl212*gtu22 + 
      Gtl312*gtu23;
    
    CCTK_REAL Gt312 CCTK_ATTRIBUTE_UNUSED = Gtl112*gtu13 + Gtl212*gtu23 + 
      Gtl312*gtu33;
    
    CCTK_REAL Gt113 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu11 + Gtl213*gtu12 + 
      Gtl313*gtu13;
    
    CCTK_REAL Gt213 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu12 + Gtl213*gtu22 + 
      Gtl313*gtu23;
    
    CCTK_REAL Gt313 CCTK_ATTRIBUTE_UNUSED = Gtl113*gtu13 + Gtl213*gtu23 + 
      Gtl313*gtu33;
    
    CCTK_REAL Gt122 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu11 + Gtl222*gtu12 + 
      Gtl322*gtu13;
    
    CCTK_REAL Gt222 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu12 + Gtl222*gtu22 + 
      Gtl322*gtu23;
    
    CCTK_REAL Gt322 CCTK_ATTRIBUTE_UNUSED = Gtl122*gtu13 + Gtl222*gtu23 + 
      Gtl322*gtu33;
    
    CCTK_REAL Gt123 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu11 + Gtl223*gtu12 + 
      Gtl323*gtu13;
    
    CCTK_REAL Gt223 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu12 + Gtl223*gtu22 + 
      Gtl323*gtu23;
    
    CCTK_REAL Gt323 CCTK_ATTRIBUTE_UNUSED = Gtl123*gtu13 + Gtl223*gtu23 + 
      Gtl323*gtu33;
    
    CCTK_REAL Gt133 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu11 + Gtl233*gtu12 + 
      Gtl333*gtu13;
    
    CCTK_REAL Gt233 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu12 + Gtl233*gtu22 + 
      Gtl333*gtu23;
    
    CCTK_REAL Gt333 CCTK_ATTRIBUTE_UNUSED = Gtl133*gtu13 + Gtl233*gtu23 + 
      Gtl333*gtu33;
    
    CCTK_REAL em4phi CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,pow(phiWL,2),exp(-4*phiWL));
    
    CCTK_REAL e4phi CCTK_ATTRIBUTE_UNUSED = pow(em4phi,-1);
    
    CCTK_REAL g11 CCTK_ATTRIBUTE_UNUSED = gt11L*e4phi;
    
    CCTK_REAL g12 CCTK_ATTRIBUTE_UNUSED = gt12L*e4phi;
    
    CCTK_REAL g13 CCTK_ATTRIBUTE_UNUSED = gt13L*e4phi;
    
    CCTK_REAL g22 CCTK_ATTRIBUTE_UNUSED = gt22L*e4phi;
    
    CCTK_REAL g23 CCTK_ATTRIBUTE_UNUSED = gt23L*e4phi;
    
    CCTK_REAL g33 CCTK_ATTRIBUTE_UNUSED = gt33L*e4phi;
    
    CCTK_REAL gu11 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu11;
    
    CCTK_REAL gu12 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu12;
    
    CCTK_REAL gu13 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu13;
    
    CCTK_REAL gu22 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu22;
    
    CCTK_REAL gu23 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu23;
    
    CCTK_REAL gu33 CCTK_ATTRIBUTE_UNUSED = em4phi*gtu33;
    
    CCTK_REAL Xtn1 CCTK_ATTRIBUTE_UNUSED = Gt111*gtu11 + 2*Gt112*gtu12 + 
      2*Gt113*gtu13 + Gt122*gtu22 + 2*Gt123*gtu23 + Gt133*gtu33;
    
    CCTK_REAL Xtn2 CCTK_ATTRIBUTE_UNUSED = Gt211*gtu11 + 2*Gt212*gtu12 + 
      2*Gt213*gtu13 + Gt222*gtu22 + 2*Gt223*gtu23 + Gt233*gtu33;
    
    CCTK_REAL Xtn3 CCTK_ATTRIBUTE_UNUSED = Gt311*gtu11 + 2*Gt312*gtu12 + 
      2*Gt313*gtu13 + Gt322*gtu22 + 2*Gt323*gtu23 + Gt333*gtu33;
    
    CCTK_REAL Rt11 CCTK_ATTRIBUTE_UNUSED = 3*Gt111*Gtlu111 + 
      3*Gt112*Gtlu112 + 3*Gt113*Gtlu113 + 2*Gt211*Gtlu121 + 2*Gt212*Gtlu122 + 
      2*Gt213*Gtlu123 + 2*Gt311*Gtlu131 + 2*Gt312*Gtlu132 + 2*Gt313*Gtlu133 + 
      Gt211*Gtlu211 + Gt212*Gtlu212 + Gt213*Gtlu213 + Gt311*Gtlu311 + 
      Gt312*Gtlu312 + Gt313*Gtlu313 + 0.5*(-(gtu11*PDPDgt1111) - 
      gtu12*PDPDgt1112 - gtu13*PDPDgt1113 - gtu12*PDPDgt1121 - 
      gtu22*PDPDgt1122 - gtu23*PDPDgt1123 - gtu13*PDPDgt1131 - 
      gtu23*PDPDgt1132 - gtu33*PDPDgt1133) + gt11L*PDXt11 + gt12L*PDXt21 + 
      gt13L*PDXt31 + Gtl111*Xtn1 + Gtl112*Xtn2 + Gtl113*Xtn3;
    
    CCTK_REAL Rt12 CCTK_ATTRIBUTE_UNUSED = Gt112*Gtlu111 + Gt122*Gtlu112 + 
      Gt123*Gtlu113 + Gt111*Gtlu121 + Gt212*Gtlu121 + Gt112*Gtlu122 + 
      Gt222*Gtlu122 + Gt113*Gtlu123 + Gt223*Gtlu123 + Gt312*Gtlu131 + 
      Gt322*Gtlu132 + Gt323*Gtlu133 + Gt111*Gtlu211 + Gt112*Gtlu212 + 
      Gt113*Gtlu213 + 2*Gt211*Gtlu221 + 2*Gt212*Gtlu222 + 2*Gt213*Gtlu223 + 
      Gt311*Gtlu231 + Gt312*Gtlu232 + Gt313*Gtlu233 + Gt311*Gtlu321 + 
      Gt312*Gtlu322 + Gt313*Gtlu323 + 0.5*(-(gtu11*PDPDgt1211) - 
      gtu12*PDPDgt1212 - gtu13*PDPDgt1213 - gtu12*PDPDgt1221 - 
      gtu22*PDPDgt1222 - gtu23*PDPDgt1223 - gtu13*PDPDgt1231 - 
      gtu23*PDPDgt1232 - gtu33*PDPDgt1233) + 0.5*(gt12L*PDXt11 + gt22L*PDXt21 
      + gt23L*PDXt31) + 0.5*(gt11L*PDXt12 + gt12L*PDXt22 + gt13L*PDXt32) + 
      0.5*(Gtl112*Xtn1 + Gtl122*Xtn2 + Gtl123*Xtn3) + 0.5*(Gtl211*Xtn1 + 
      Gtl212*Xtn2 + Gtl213*Xtn3);
    
    CCTK_REAL Rt13 CCTK_ATTRIBUTE_UNUSED = Gt113*Gtlu111 + Gt123*Gtlu112 + 
      Gt133*Gtlu113 + Gt213*Gtlu121 + Gt223*Gtlu122 + Gt233*Gtlu123 + 
      Gt111*Gtlu131 + Gt313*Gtlu131 + Gt112*Gtlu132 + Gt323*Gtlu132 + 
      Gt113*Gtlu133 + Gt333*Gtlu133 + Gt211*Gtlu231 + Gt212*Gtlu232 + 
      Gt213*Gtlu233 + Gt111*Gtlu311 + Gt112*Gtlu312 + Gt113*Gtlu313 + 
      Gt211*Gtlu321 + Gt212*Gtlu322 + Gt213*Gtlu323 + 2*Gt311*Gtlu331 + 
      2*Gt312*Gtlu332 + 2*Gt313*Gtlu333 + 0.5*(-(gtu11*PDPDgt1311) - 
      gtu12*PDPDgt1312 - gtu13*PDPDgt1313 - gtu12*PDPDgt1321 - 
      gtu22*PDPDgt1322 - gtu23*PDPDgt1323 - gtu13*PDPDgt1331 - 
      gtu23*PDPDgt1332 - gtu33*PDPDgt1333) + 0.5*(gt13L*PDXt11 + gt23L*PDXt21 
      + gt33L*PDXt31) + 0.5*(gt11L*PDXt13 + gt12L*PDXt23 + gt13L*PDXt33) + 
      0.5*(Gtl113*Xtn1 + Gtl123*Xtn2 + Gtl133*Xtn3) + 0.5*(Gtl311*Xtn1 + 
      Gtl312*Xtn2 + Gtl313*Xtn3);
    
    CCTK_REAL Rt22 CCTK_ATTRIBUTE_UNUSED = Gt112*Gtlu121 + Gt122*Gtlu122 + 
      Gt123*Gtlu123 + 2*Gt112*Gtlu211 + 2*Gt122*Gtlu212 + 2*Gt123*Gtlu213 + 
      3*Gt212*Gtlu221 + 3*Gt222*Gtlu222 + 3*Gt223*Gtlu223 + 2*Gt312*Gtlu231 + 
      2*Gt322*Gtlu232 + 2*Gt323*Gtlu233 + Gt312*Gtlu321 + Gt322*Gtlu322 + 
      Gt323*Gtlu323 + 0.5*(-(gtu11*PDPDgt2211) - gtu12*PDPDgt2212 - 
      gtu13*PDPDgt2213 - gtu12*PDPDgt2221 - gtu22*PDPDgt2222 - 
      gtu23*PDPDgt2223 - gtu13*PDPDgt2231 - gtu23*PDPDgt2232 - 
      gtu33*PDPDgt2233) + gt12L*PDXt12 + gt22L*PDXt22 + gt23L*PDXt32 + 
      Gtl212*Xtn1 + Gtl222*Xtn2 + Gtl223*Xtn3;
    
    CCTK_REAL Rt23 CCTK_ATTRIBUTE_UNUSED = Gt112*Gtlu131 + Gt122*Gtlu132 + 
      Gt123*Gtlu133 + Gt113*Gtlu211 + Gt123*Gtlu212 + Gt133*Gtlu213 + 
      Gt213*Gtlu221 + Gt223*Gtlu222 + Gt233*Gtlu223 + Gt212*Gtlu231 + 
      Gt313*Gtlu231 + Gt222*Gtlu232 + Gt323*Gtlu232 + Gt223*Gtlu233 + 
      Gt333*Gtlu233 + Gt112*Gtlu311 + Gt122*Gtlu312 + Gt123*Gtlu313 + 
      Gt212*Gtlu321 + Gt222*Gtlu322 + Gt223*Gtlu323 + 2*Gt312*Gtlu331 + 
      2*Gt322*Gtlu332 + 2*Gt323*Gtlu333 + 0.5*(-(gtu11*PDPDgt2311) - 
      gtu12*PDPDgt2312 - gtu13*PDPDgt2313 - gtu12*PDPDgt2321 - 
      gtu22*PDPDgt2322 - gtu23*PDPDgt2323 - gtu13*PDPDgt2331 - 
      gtu23*PDPDgt2332 - gtu33*PDPDgt2333) + 0.5*(gt13L*PDXt12 + gt23L*PDXt22 
      + gt33L*PDXt32) + 0.5*(gt12L*PDXt13 + gt22L*PDXt23 + gt23L*PDXt33) + 
      0.5*(Gtl213*Xtn1 + Gtl223*Xtn2 + Gtl233*Xtn3) + 0.5*(Gtl312*Xtn1 + 
      Gtl322*Xtn2 + Gtl323*Xtn3);
    
    CCTK_REAL Rt33 CCTK_ATTRIBUTE_UNUSED = Gt113*Gtlu131 + Gt123*Gtlu132 + 
      Gt133*Gtlu133 + Gt213*Gtlu231 + Gt223*Gtlu232 + Gt233*Gtlu233 + 
      2*Gt113*Gtlu311 + 2*Gt123*Gtlu312 + 2*Gt133*Gtlu313 + 2*Gt213*Gtlu321 + 
      2*Gt223*Gtlu322 + 2*Gt233*Gtlu323 + 3*Gt313*Gtlu331 + 3*Gt323*Gtlu332 + 
      3*Gt333*Gtlu333 + 0.5*(-(gtu11*PDPDgt3311) - gtu12*PDPDgt3312 - 
      gtu13*PDPDgt3313 - gtu12*PDPDgt3321 - gtu22*PDPDgt3322 - 
      gtu23*PDPDgt3323 - gtu13*PDPDgt3331 - gtu23*PDPDgt3332 - 
      gtu33*PDPDgt3333) + gt13L*PDXt13 + gt23L*PDXt23 + gt33L*PDXt33 + 
      Gtl313*Xtn1 + Gtl323*Xtn2 + Gtl333*Xtn3;
    
    CCTK_REAL fac1 CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,-0.5*pow(phiWL,-1),1);
    
    CCTK_REAL cdphi1 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*fac1;
    
    CCTK_REAL cdphi2 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*fac1;
    
    CCTK_REAL cdphi3 CCTK_ATTRIBUTE_UNUSED = PDphiW3L*fac1;
    
    CCTK_REAL fac2 CCTK_ATTRIBUTE_UNUSED = IfThen(conformalMethod != 
      0,0.5*pow(phiWL,-2),0);
    
    CCTK_REAL cdphi211 CCTK_ATTRIBUTE_UNUSED = fac1*(-(PDphiW1L*Gt111) - 
      PDphiW2L*Gt211 - PDphiW3L*Gt311 + PDPDphiW11) + fac2*pow(PDphiW1L,2);
    
    CCTK_REAL cdphi212 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW2L*fac2 + 
      fac1*(-(PDphiW1L*Gt112) - PDphiW2L*Gt212 - PDphiW3L*Gt312 + 
      PDPDphiW12);
    
    CCTK_REAL cdphi213 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt113) - PDphiW2L*Gt213 - PDphiW3L*Gt313 + 
      PDPDphiW13);
    
    CCTK_REAL cdphi221 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW2L*fac2 + 
      fac1*(-(PDphiW1L*Gt112) - PDphiW2L*Gt212 - PDphiW3L*Gt312 + 
      PDPDphiW21);
    
    CCTK_REAL cdphi222 CCTK_ATTRIBUTE_UNUSED = fac1*(-(PDphiW1L*Gt122) - 
      PDphiW2L*Gt222 - PDphiW3L*Gt322 + PDPDphiW22) + fac2*pow(PDphiW2L,2);
    
    CCTK_REAL cdphi223 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt123) - PDphiW2L*Gt223 - PDphiW3L*Gt323 + 
      PDPDphiW23);
    
    CCTK_REAL cdphi231 CCTK_ATTRIBUTE_UNUSED = PDphiW1L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt113) - PDphiW2L*Gt213 - PDphiW3L*Gt313 + 
      PDPDphiW31);
    
    CCTK_REAL cdphi232 CCTK_ATTRIBUTE_UNUSED = PDphiW2L*PDphiW3L*fac2 + 
      fac1*(-(PDphiW1L*Gt123) - PDphiW2L*Gt223 - PDphiW3L*Gt323 + 
      PDPDphiW32);
    
    CCTK_REAL cdphi233 CCTK_ATTRIBUTE_UNUSED = fac1*(-(PDphiW1L*Gt133) - 
      PDphiW2L*Gt233 - PDphiW3L*Gt333 + PDPDphiW33) + fac2*pow(PDphiW3L,2);
    
    CCTK_REAL Rphi11 CCTK_ATTRIBUTE_UNUSED = -2*cdphi211 - 
      2*gt11L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt11L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33)) + 
      4*pow(cdphi1,2);
    
    CCTK_REAL Rphi12 CCTK_ATTRIBUTE_UNUSED = 4*cdphi1*cdphi2 - 2*cdphi221 
      - 2*gt12L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt12L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33));
    
    CCTK_REAL Rphi13 CCTK_ATTRIBUTE_UNUSED = -2*cdphi231 + 4*cdphi1*cdphi3 
      - 2*gt13L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt13L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33));
    
    CCTK_REAL Rphi22 CCTK_ATTRIBUTE_UNUSED = -2*cdphi222 - 
      2*gt22L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt22L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33)) + 
      4*pow(cdphi2,2);
    
    CCTK_REAL Rphi23 CCTK_ATTRIBUTE_UNUSED = -2*cdphi232 + 4*cdphi2*cdphi3 
      - 2*gt23L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt23L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33));
    
    CCTK_REAL Rphi33 CCTK_ATTRIBUTE_UNUSED = -2*cdphi233 - 
      2*gt33L*(cdphi211*gtu11 + cdphi212*gtu12 + cdphi221*gtu12 + 
      cdphi213*gtu13 + cdphi231*gtu13 + cdphi222*gtu22 + cdphi223*gtu23 + 
      cdphi232*gtu23 + cdphi233*gtu33) - 4*gt33L*(cdphi1*(cdphi1*gtu11 + 
      cdphi2*gtu12 + cdphi3*gtu13) + cdphi2*(cdphi1*gtu12 + cdphi2*gtu22 + 
      cdphi3*gtu23) + cdphi3*(cdphi1*gtu13 + cdphi2*gtu23 + cdphi3*gtu33)) + 
      4*pow(cdphi3,2);
    
    CCTK_REAL Atm11 CCTK_ATTRIBUTE_UNUSED = At11L*gtu11 + At12L*gtu12 + 
      At13L*gtu13;
    
    CCTK_REAL Atm21 CCTK_ATTRIBUTE_UNUSED = At11L*gtu12 + At12L*gtu22 + 
      At13L*gtu23;
    
    CCTK_REAL Atm31 CCTK_ATTRIBUTE_UNUSED = At11L*gtu13 + At12L*gtu23 + 
      At13L*gtu33;
    
    CCTK_REAL Atm12 CCTK_ATTRIBUTE_UNUSED = At12L*gtu11 + At22L*gtu12 + 
      At23L*gtu13;
    
    CCTK_REAL Atm22 CCTK_ATTRIBUTE_UNUSED = At12L*gtu12 + At22L*gtu22 + 
      At23L*gtu23;
    
    CCTK_REAL Atm32 CCTK_ATTRIBUTE_UNUSED = At12L*gtu13 + At22L*gtu23 + 
      At23L*gtu33;
    
    CCTK_REAL Atm13 CCTK_ATTRIBUTE_UNUSED = At13L*gtu11 + At23L*gtu12 + 
      At33L*gtu13;
    
    CCTK_REAL Atm23 CCTK_ATTRIBUTE_UNUSED = At13L*gtu12 + At23L*gtu22 + 
      At33L*gtu23;
    
    CCTK_REAL Atm33 CCTK_ATTRIBUTE_UNUSED = At13L*gtu13 + At23L*gtu23 + 
      At33L*gtu33;
    
    CCTK_REAL R11 CCTK_ATTRIBUTE_UNUSED = Rphi11 + Rt11;
    
    CCTK_REAL R12 CCTK_ATTRIBUTE_UNUSED = Rphi12 + Rt12;
    
    CCTK_REAL R13 CCTK_ATTRIBUTE_UNUSED = Rphi13 + Rt13;
    
    CCTK_REAL R22 CCTK_ATTRIBUTE_UNUSED = Rphi22 + Rt22;
    
    CCTK_REAL R23 CCTK_ATTRIBUTE_UNUSED = Rphi23 + Rt23;
    
    CCTK_REAL R33 CCTK_ATTRIBUTE_UNUSED = Rphi33 + Rt33;
    
    CCTK_REAL trS CCTK_ATTRIBUTE_UNUSED = eTxxL*gu11 + 2*eTxyL*gu12 + 
      2*eTxzL*gu13 + eTyyL*gu22 + 2*eTyzL*gu23 + eTzzL*gu33;
    
    CCTK_REAL Ats11 CCTK_ATTRIBUTE_UNUSED = 4*PDalpha1L*cdphi1 + 
      PDalpha1L*Gt111 + PDalpha2L*Gt211 + PDalpha3L*Gt311 - PDPDalpha11 + 
      alphaL*R11;
    
    CCTK_REAL Ats12 CCTK_ATTRIBUTE_UNUSED = 2*(PDalpha2L*cdphi1 + 
      PDalpha1L*cdphi2) + PDalpha1L*Gt112 + PDalpha2L*Gt212 + PDalpha3L*Gt312 
      - PDPDalpha12 + alphaL*R12;
    
    CCTK_REAL Ats13 CCTK_ATTRIBUTE_UNUSED = 2*(PDalpha3L*cdphi1 + 
      PDalpha1L*cdphi3) + PDalpha1L*Gt113 + PDalpha2L*Gt213 + PDalpha3L*Gt313 
      - PDPDalpha13 + alphaL*R13;
    
    CCTK_REAL Ats22 CCTK_ATTRIBUTE_UNUSED = 4*PDalpha2L*cdphi2 + 
      PDalpha1L*Gt122 + PDalpha2L*Gt222 + PDalpha3L*Gt322 - PDPDalpha22 + 
      alphaL*R22;
    
    CCTK_REAL Ats23 CCTK_ATTRIBUTE_UNUSED = 2*(PDalpha3L*cdphi2 + 
      PDalpha2L*cdphi3) + PDalpha1L*Gt123 + PDalpha2L*Gt223 + PDalpha3L*Gt323 
      - PDPDalpha23 + alphaL*R23;
    
    CCTK_REAL Ats33 CCTK_ATTRIBUTE_UNUSED = 4*PDalpha3L*cdphi3 + 
      PDalpha1L*Gt133 + PDalpha2L*Gt233 + PDalpha3L*Gt333 - PDPDalpha33 + 
      alphaL*R33;
    
    CCTK_REAL trAts CCTK_ATTRIBUTE_UNUSED = Ats11*gu11 + 2*Ats12*gu12 + 
      2*Ats13*gu13 + Ats22*gu22 + 2*Ats23*gu23 + Ats33*gu33;
    
    CCTK_REAL At11rhsL CCTK_ATTRIBUTE_UNUSED = 2*At11L*PDbeta11L + 
      2*At12L*PDbeta21L + 2*At13L*PDbeta31L - 
      0.666666666666666666666666666667*At11L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + alphaL*(At11L*trKL - 2*(At11L*Atm11 + At12L*Atm21 + 
      At13L*Atm31)) + GDissAt11 + beta1L*PDuAt111 + beta2L*PDuAt112 + 
      beta3L*PDuAt113 + em4phi*(Ats11 - 
      0.333333333333333333333333333333*g11*trAts) - 8*alphaL*em4phi*Pi*(eTxxL 
      - 0.333333333333333333333333333333*g11*trS);
    
    CCTK_REAL At12rhsL CCTK_ATTRIBUTE_UNUSED = At12L*PDbeta11L + 
      At11L*PDbeta12L + At22L*PDbeta21L + At12L*PDbeta22L + At23L*PDbeta31L + 
      At13L*PDbeta32L - 0.666666666666666666666666666667*At12L*(PDbeta11L + 
      PDbeta22L + PDbeta33L) + alphaL*(At12L*trKL - 2*(At11L*Atm12 + 
      At12L*Atm22 + At13L*Atm32)) + GDissAt12 + beta1L*PDuAt121 + 
      beta2L*PDuAt122 + beta3L*PDuAt123 + em4phi*(Ats12 - 
      0.333333333333333333333333333333*g12*trAts) - 8*alphaL*em4phi*Pi*(eTxyL 
      - 0.333333333333333333333333333333*g12*trS);
    
    CCTK_REAL At13rhsL CCTK_ATTRIBUTE_UNUSED = At13L*PDbeta11L + 
      At11L*PDbeta13L + At23L*PDbeta21L + At12L*PDbeta23L + At33L*PDbeta31L + 
      At13L*PDbeta33L - 0.666666666666666666666666666667*At13L*(PDbeta11L + 
      PDbeta22L + PDbeta33L) + alphaL*(At13L*trKL - 2*(At11L*Atm13 + 
      At12L*Atm23 + At13L*Atm33)) + GDissAt13 + beta1L*PDuAt131 + 
      beta2L*PDuAt132 + beta3L*PDuAt133 + em4phi*(Ats13 - 
      0.333333333333333333333333333333*g13*trAts) - 8*alphaL*em4phi*Pi*(eTxzL 
      - 0.333333333333333333333333333333*g13*trS);
    
    CCTK_REAL At22rhsL CCTK_ATTRIBUTE_UNUSED = 2*At12L*PDbeta12L + 
      2*At22L*PDbeta22L + 2*At23L*PDbeta32L - 
      0.666666666666666666666666666667*At22L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + alphaL*(At22L*trKL - 2*(At12L*Atm12 + At22L*Atm22 + 
      At23L*Atm32)) + GDissAt22 + beta1L*PDuAt221 + beta2L*PDuAt222 + 
      beta3L*PDuAt223 + em4phi*(Ats22 - 
      0.333333333333333333333333333333*g22*trAts) - 8*alphaL*em4phi*Pi*(eTyyL 
      - 0.333333333333333333333333333333*g22*trS);
    
    CCTK_REAL At23rhsL CCTK_ATTRIBUTE_UNUSED = At13L*PDbeta12L + 
      At12L*PDbeta13L + At23L*PDbeta22L + At22L*PDbeta23L + At33L*PDbeta32L + 
      At23L*PDbeta33L - 0.666666666666666666666666666667*At23L*(PDbeta11L + 
      PDbeta22L + PDbeta33L) + alphaL*(At23L*trKL - 2*(At12L*Atm13 + 
      At22L*Atm23 + At23L*Atm33)) + GDissAt23 + beta1L*PDuAt231 + 
      beta2L*PDuAt232 + beta3L*PDuAt233 + em4phi*(Ats23 - 
      0.333333333333333333333333333333*g23*trAts) - 8*alphaL*em4phi*Pi*(eTyzL 
      - 0.333333333333333333333333333333*g23*trS);
    
    CCTK_REAL At33rhsL CCTK_ATTRIBUTE_UNUSED = 2*At13L*PDbeta13L + 
      2*At23L*PDbeta23L + 2*At33L*PDbeta33L - 
      0.666666666666666666666666666667*At33L*(PDbeta11L + PDbeta22L + 
      PDbeta33L) + alphaL*(At33L*trKL - 2*(At13L*Atm13 + At23L*Atm23 + 
      At33L*Atm33)) + GDissAt33 + beta1L*PDuAt331 + beta2L*PDuAt332 + 
      beta3L*PDuAt333 + em4phi*(Ats33 - 
      0.333333333333333333333333333333*g33*trAts) - 8*alphaL*em4phi*Pi*(eTzzL 
      - 0.333333333333333333333333333333*g33*trS);
    /* Copy local copies back to grid functions */
    At11rhs[index] = At11rhsL;
    At12rhs[index] = At12rhsL;
    At13rhs[index] = At13rhsL;
    At22rhs[index] = At22rhsL;
    At23rhs[index] = At23rhsL;
    At33rhs[index] = At33rhsL;
  }
  CCTK_ENDLOOP3(ML_BSSN_DG8_EvolutionInteriorSplit33);
}
extern "C" void ML_BSSN_DG8_EvolutionInteriorSplit33(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_BSSN_DG8_EvolutionInteriorSplit33_Body");
  }
  if (cctk_iteration % ML_BSSN_DG8_EvolutionInteriorSplit33_calc_every != ML_BSSN_DG8_EvolutionInteriorSplit33_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_BSSN_DG8::ML_confac",
    "ML_BSSN_DG8::ML_curv",
    "ML_BSSN_DG8::ML_curvrhs",
    "ML_BSSN_DG8::ML_dconfac",
    "ML_BSSN_DG8::ML_dlapse",
    "ML_BSSN_DG8::ML_dmetric",
    "ML_BSSN_DG8::ML_dshift",
    "ML_BSSN_DG8::ML_Gamma",
    "ML_BSSN_DG8::ML_lapse",
    "ML_BSSN_DG8::ML_metric",
    "ML_BSSN_DG8::ML_shift",
    "ML_BSSN_DG8::ML_trace_curv",
    "ML_BSSN_DG8::ML_volume_form"};
  AssertGroupStorage(cctkGH, "ML_BSSN_DG8_EvolutionInteriorSplit33", 13, groups);
  
  
  TiledLoopOverInterior(cctkGH, ML_BSSN_DG8_EvolutionInteriorSplit33_Body);
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_BSSN_DG8_EvolutionInteriorSplit33_Body");
  }
}

} // namespace ML_BSSN_DG8
